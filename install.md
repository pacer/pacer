# Installation

## Frontend and backend

### Host setup
- Install Ubuntu 16.04
- Install vanilla Xen: https://help.ubuntu.com/community/Xen
- Install Pacer-enabled xen from one of the following submodules:
	- Frontend: [repo](https://gitlab.mpi-sws.org/pacer/xen-frontend-4.10.0) ([detailed instructions](https://gitlab.mpi-sws.org/pacer/pacer/-/tree/main/frontend/hypace))
  - Backend: [repo](https://gitlab.mpi-sws.org/pacer/xen-backend-4.10.0) ([detailed instructions](https://gitlab.mpi-sws.org/pacer/pacer/-/tree/main/backend/hypace))

### Guest VM setup
- Disable DVFS on host: `xenpm set-max-cstate 0`
- Run `vfsetup.sh` to setup SR-IOV VFs.
- Determine the PCI IDs of the configured VFs and set them in the config files for xen guests. Example config files for `xl create`:
  - frontend/hypace/configs/vm1-mw.cfg
  - frontend/hypace/configs/vm2-attack.cfg
  - backend/hypace/configs/vm2-memcache.cfg

## Client host setup
- Install Ubuntu 16.04
- Install Pacer-enabled Linux kernel from [repo](https://gitlab.mpi-sws.org/pacer/linux-client-4.9.5) ([detailed instructions](https://gitlab.mpi-sws.org/pacer/pacer/-/tree/main/client/gpace))
- Disable DVFS
- Run `irqsetup.sh`

## Applications
Detailed installation instructions for each application in the links below.
- [OpenSSL](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/apps/README.md#openssl)
- [Apache](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/apps/README.md#apache)
- [Mediawiki](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/apps/README.md#mediawiki)
- [Video service](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/apps/README.md#video-server)
- [Memcache](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/apps/README.md#memcache)
- [wrk2](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/apps/README.md#wrk2)

## Other tools
- tcpdump
- kedr
- wondershaper
- tc
- ethtool
