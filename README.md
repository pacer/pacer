# Pacer

## Publications
Pacer: Comprehensive Network Side-Channel Mitigation in the Cloud<br>
Aastha Mehta, Mohamed Alzayat, Roberta De Viti, Björn B. Brandenburg, Peter Druschel, Deepak Garg<br>
https://arxiv.org/pdf/1908.11568.pdf

If you use this code in your work, please cite the paper above.

## Contact
Aastha Mehta: <aasthakm@cs.ubc.ca>
