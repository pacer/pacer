## Bandwidth overhead

### HTML
```
cd html
./html_bw_clustering.sh 14257493 enwiki_clustering_dataset final wiki
./html_bw_clustering.sh 5027344 wiktionary_clustering_dataset final wikt
./html_bw_clustering.sh 6879 medical_dataset final med
```
Output directory: `wiki`, `wikt`, `med`<br>
Pacer output file: `$outdir/ovhstats.csv`<br>
Baseline output file: `$outdir/roundup.output`

### Video
```
cd video
./video_bw_clustering.sh
```
Output directory: `out`<br>
Pacer output file: `$outdir/ovhstats.csv`<br>
Baseline output file: `$outdir/roundup.output`
