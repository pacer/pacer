#!/usr/bin/python

######################################################
## video_roundup_padding.py
##
## author: aasthakm
##
## implement round up to pow2 and next multiple of 100
## (approaches presented in tamaraw, cs-buflo)
######################################################


import math
import pandas as pd
import numpy as np
import os
import sys
import collections
import itertools
import csv
import time
import argparse

from pacer_video_clustering import *

#### global parameters

debug = False
MTU = 1448
L = 100

data_path = "video_clustering_dataset"

np.set_printoptions(suppress=True)

def np_round_pow2(videos):
#    videos_round = np.power(2, np.ceil(np.where(videos>0, np.log2(videos), 0)))
    videos_round = np.power(2, np.ceil(np.log2(videos, where=(videos!=0))))
    return videos_round


def np_round_mul_L(videos, L):
    videos_round = np.ceil(videos/L) * L
    return videos_round


def np_rel_ovh_arr(pervid_overheads_arr_np, videos_np):
#    rel_np = np.average(pervid_overheads_arr_np/videos_np)
#    return rel_np

    nvids = len(videos_np)
#    rel_arr_np = pervid_overheads_arr_np/videos_np
#    rel_sum_arr_np = np.zeros(nvids)
#    for i in range(nvids):
#        rel_sum_arr_np[i] = np.sum(rel_arr_np[i])

    pervid_overheads_sum_np = np.zeros(nvids)
    pervid_sizes_sum_np = np.zeros(nvids)
    for i in range(nvids):
        pervid_overheads_sum_np[i] = np.sum(pervid_overheads_arr_np[i])
        pervid_sizes_sum_np[i] = np.sum(videos_np[i])

    rel_sum_arr_np = pervid_overheads_sum_np/pervid_sizes_sum_np

    if (debug):
        print("\nrelative overheads:")
        print(rel_sum_arr_np)

    return rel_sum_arr_np


def gen_clusters(videos_round_np):
    clusters = []
    nvids = len(videos_round_np)

    for i in range(0, nvids):
        grouped = 0
        if (len(clusters) == 0):
            clusters.append([i])
            continue

        for clist_idx in range(len(clusters)):
            #for cidx in clusters[clist_idx]:
            cidx = clusters[clist_idx][0]
            check_equal = np.not_equal(videos_round_np[cidx], videos_round_np[i])
#            print(check_equal)
#            print((True in check_equal))
            if (True in check_equal):
                continue

            clusters[clist_idx].append(i)
            grouped = 1
            break

        if (grouped == 0):
            clusters.append([i])

    return clusters


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="roundup padding algorithm")
    parser.add_argument("-L", "--roundfact", dest="L", default=100,
        help="rounding factor for tamaraw style padding")
    parser.add_argument("-i", "--input_dataset", dest="data_path", action="store",
        help="input file", required=True)
    args = parser.parse_args()

    data_path = args.data_path
    L = int(args.L)

    print("input: %s" % (data_path))

    videos_with_tag = load_video_dataset(data_path)
    print("#vids %d" % (len(videos_with_tag)))

    videos = videos_with_tag.drop(videos_with_tag.columns[0], axis=1)
#    print(videos)

#    videos_np = np.loadtxt(data_path, delimiter='\t', skiprows=1, dtype=str)
    videos_np = videos.to_numpy()
    if (debug):
        print("==== orig ====")
        print(videos_np)

    print("\n==== round pow2 ====")
    videos_round_np = np_round_pow2(videos_np)

    if (debug):
        print("segment sizes after pow2 rounding:")
        print(videos_round_np)

    pervid_overheads_arr_np = videos_round_np - videos_np
    rel_overheads_arr_np = np_rel_ovh_arr(pervid_overheads_arr_np, videos_np)
    if (debug):
        print("overheads on individual video segments:")
        print(pervid_overheads_arr_np)

    print("\nrelative padding overhead avg: %f max %f" %
        (np.average(rel_overheads_arr_np), np.max(rel_overheads_arr_np)))

    clusters = gen_clusters(videos_round_np)
#    print(clusters)
    print("number of clusters: %d" % len(clusters))

    print("clusters with more than 1 element (video ids):")
    i = 0
    for c in clusters:
        if (len(c) > 1):
            print("%d. %s" % (i, c))

        i += 1


    print("\n==== round %d ====" % L)
    videos_round_mul_L_np = np_round_mul_L(videos_np, L)
    if (debug):
        print("segment sizes after rounding to multiple of %d" % L)
        print(videos_round_mul_L_np)

    pervid_overheads_arr_np = videos_round_mul_L_np - videos_np
    rel_overheads_arr_np = np_rel_ovh_arr(pervid_overheads_arr_np, videos_np)
    if (debug):
        print("overheads on individual video segments:")
        print(pervid_overheads_arr_np)

    print("\nrelative padding overhead avg: %f max %f" %
        (np.average(rel_overheads_arr_np), np.max(rel_overheads_arr_np)))

    clusters = gen_clusters(videos_round_mul_L_np)
    print("number of clusters: %d" % len(clusters))

    print("clusters with more than 1 element (video ids):")
    i = 0
    for c in clusters:
        if (len(c) > 1):
            print("%d. %s" % (i, c))

        i += 1
