#!/usr/bin/python

###############################################
## pacer_video_clustering.py
##
## author: aasthakm
##
## implement Pacer's video clustering algorithm
###############################################


# Floating-point division
from __future__ import division
from collections import defaultdict
from collections import OrderedDict
from operator import itemgetter
from operator import mul
from random import randint

import numpy as np
import pandas as pd
import time
import pickle
import sys
import os
import csv
import argparse

#### Global parameters

MTU = 1448

## maximum number of segments across all videos in the corpus
## determine from MPD files of videos in the corpus
max_n_segments = 3005

## debug prints
debug = True

## save detailed stats breakdown to files
save_files = True

## merge last cluster if smaller than min cluster size
merge_last = True

max_ss_only = True

## random seed
choose_random = False

#### Functions

def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()


def save_obj(obj, name):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)


def load_file(filename):
    _this_dir = os.path.dirname(os.path.abspath(__file__))
    if filename is None:
        sys.exit("[load_file] Error. File path not given.")
    with open(os.path.join(_this_dir, filename)) as f:
        columns = list(range(0,max_n_segments+1))
        columns.insert(0, "videotag")
        datanan = pd.read_csv(filename, delimiter='\t', header=None, names=columns)
        data = datanan.fillna(0)
    return data


def load_video_dataset(video_path=None):
    if (os.path.exists("videos.pkl")):
        videos = load_obj("videos")
    else:
        videos = load_file(video_path)
        save_obj(videos, "videos")
    return videos


def pad_segments_to_mtu(videos):
    videos_mtu = np.ceil(videos/MTU) * MTU
    return videos_mtu


def video_lengths(videos):
    return np.count_nonzero(videos, axis=1)


def video_max_segment(videos):
    return videos.max(axis=1)


def get_L(lengths):
    return np.unique(lengths)


def get_S(videos):
    if max_ss_only == True:
        max_ss = video_max_segment(videos)
        return np.sort(pd.unique(max_ss))

    return np.delete(np.sort(pd.unique(videos[videos.columns].values.ravel('K'))), 0)


def get_V(videos, lengths):
    max_ss = video_max_segment(videos)
    video_map = [(lengths[i], max_ss[i]) for i in range(len(max_ss))]
    return video_map


def dominance(l1, l2, s1, s2):
    # true if (l2, s2) is dominated by (l1, s1)
    return (l2 <= l1 and s2 <= s1)


def compute_dominance_set(L, S, V):
    d_set = defaultdict(set)
    ls_values = [(l, s) for l in L for s in S]
    i = 0
    totaltime = time.time()
    for ls in ls_values:
        starttime = time.time()
        for v_idx in range(len(V)):
            if dominance(ls[0], V[v_idx][0], ls[1], V[v_idx][1]):
                d_set[ls].add(v_idx)
        i = i + 1

        if debug:
            print("%d out of %d, time %s, time for inner loop %s"
                % (i, (len(ls_values)), (time.time()), (time.time()-starttime)))

    if debug:
        print("compute_dominance_set time: %s s" % (time.time() - totaltime))

    return d_set


def compute_cardinality_set(d_set):
    c_set = defaultdict(list)
    for ls in d_set:
        c = len(d_set[ls])
        c_set[c].append(ls)

    for c in c_set:
        c_set[c].sort(key=lambda tup: reduce(mul, tup))

    return c_set


def get_cardinality_set(d_set):
    starttime = time.time()
    if (os.path.exists("c_set.pkl")):
        c_set = load_obj("c_set")
    else:
        c_set = compute_cardinality_set(d_set)
        save_obj(c_set, "c_set")

    if debug:
        print("get_cardinality_set time: %s s" % (time.time() - starttime))

    return c_set


def get_dominance_set(L, S, V):
    starttime = time.time()
    if (os.path.exists("d_set.pkl")):
        d_set = load_obj("d_set")
    else:
        d_set = compute_dominance_set(L, S, V)
        save_obj(d_set, "d_set")

    if debug:
        print("get_dominance_set time: %s s" % (time.time() - starttime))

    return d_set


def update_cardinality_set(d_set, cov_videos):
    starttime = time.time()
    c_set = defaultdict(list)
    for ls in d_set:
        d_set[ls] = d_set[ls] - cov_videos
        c = len(d_set[ls])
        c_set[c].append(ls)

    for c in c_set:
        c_set[c].sort(key=lambda tup: reduce(mul, tup))

    if (debug):
        print("update_cardinality_set time: %s s" % (time.time() - starttime))

    return c_set, d_set


# pick all the (l, s) dominating at least c videos
# returns a list of couples (l, s)
def get_ceiling_candidates(c_set, c):
    if (debug):
        print("Checking candidates with cardinality in [%d, %d]" % (c, c+c_thr))

    candidates = []
    candidate_lists = [v for k,v in c_set.items() if (k >= c and k <= c+c_thr)]
    for item in candidate_lists:
        for couple in item:
            candidates.append(couple)

    return candidates


def compute_ceiling_optimizing_overhead(c_set, c, cov_videos, d_set, ceilings, videos):
    candidates = get_ceiling_candidates(c_set, c)
    if (debug):
        print("Got list of candidates (%d)" % len(candidates))

    if not candidates:
#        print "Candidate list is empty"
        return False, -1

    new_ceiling, idx = optimize_ceilings(candidates, videos, cov_videos, d_set)
    return new_ceiling, idx


def optimize_ceilings(candidates, videos, cov_videos, d_set):
    clusters = defaultdict(list)
    i = 0
    total = len(candidates)
    for candidate in candidates:
        padding = []
        videoset_idx = list(d_set[candidate] - cov_videos)
        videoset = videos.iloc[videoset_idx]
        max_segment_sizes = videoset.max(axis=0)
        for video_idx in videoset_idx:
            padding.append(sum(max_segment_sizes - videoset.ix[video_idx]))

        clusters[sum(padding)/len(videoset_idx)].append([candidate, len(videoset_idx)])
        #progress(i, total, status='Doing very long job (optimize_ceilings)')
        i = i+1

    if choose_random == False:
        candidates_minimizing_padding = clusters[min(clusters)]
        if (debug):
            print("Candidates minimising padding: %s" %
                (sorted(candidates_minimizing_padding, key=itemgetter(1))))

        new_ceiling = sorted(candidates_minimizing_padding, key = itemgetter(1))[0][0]
        return new_ceiling, 0

    # select a random candidate among the first 10
    sorted_clusters = OrderedDict(sorted(clusters.items()))
    random_candidates = sorted_clusters.items()[0:10]
    found_idx = False
    rand_len = 10
    while found_idx == False:
        #rand_idx = np.random.choice(10,
        #    p=[0.4, 0.2, 0.2, 0.05, 0.05, 0.04, 0.03, 0.01, 0.01, 0.01])
        rand_idx = np.random.choice(rand_len)

        if (debug):
            print "Random index: %d " % rand_idx

        try:
            candidates_minimizing_padding = sorted_clusters.items()[rand_idx]
            found_idx = True
        except:
            rand_len = rand_len - 1

    new_ceiling = candidates_minimizing_padding[1][0][0]
    return new_ceiling, rand_idx


def update_cov_videos(ceiling, cov_videos, d_set, clusters, videos_with_tag):
    # map: (l,s) -> set(videos_in_cluster)
    clusters[ceiling] = d_set[ceiling] - cov_videos
    # update list of covered videos
    cov_videos.update(d_set[ceiling])

    videolist = []
    for video_idx in clusters[ceiling]:
        videolist.append(videos_with_tag.ix[video_idx]['videotag'])

    if (debug):
        print("Videos covered by ceiling (%d):\n\t%s" % (len(cov_videos), videolist))

    return cov_videos, clusters


def get_super_sequence(clusters, videos, ceilings):
    new_ceilings = defaultdict(list)
    map_ceilings = defaultdict(list)
    map_clusters = defaultdict(list)
    i = 0
    for ceiling in ceilings:
        videoset = videos.iloc[list(clusters[ceiling])]
        max_segment_sizes = videoset.max(axis=0)
        new_ceilings[ceiling].append(max_segment_sizes[max_segment_sizes > 0])
        map_ceilings[i].append(max_segment_sizes[max_segment_sizes > 0])
        map_clusters[i].append(list(clusters[ceiling]))
        i = i + 1

    return new_ceilings, map_ceilings, map_clusters


def compute_ceilings(d_set, c, N, n_videos, c_set, videos, videos_with_tag):
    # list of tuples (l,s) selected as initial cluster ceilings
    ceilings = []
    # list of covered videos (i.e., that have been clustered)
    cov_videos = set()
    # map: (l,s) -> set(videos)
    clusters = defaultdict(set)
    # sum of the sizes of all clusters 1 to i-1
    cc = 0
    # used only if choose_random is true, otherwise returns list of 0s
    rand_idx = []
    # until the desired number of clusters is reached
    for i in range(1, N+1):
        starttime = time.time()
        if (debug):
            print("\n[round %d] c: %d, cc: %d, cc+c: %d" % (i, c, cc, cc+c))

        new_ceiling, idx = compute_ceiling_optimizing_overhead(c_set, c,
                cov_videos, d_set, ceilings, videos)
        if new_ceiling == False:
            if (debug):
                print("The max number of clusters with at least %d videos is %d" %
                        (c, len(ceilings)))
                print("Checking ceilings with smaller cardinality...")

            k = 0
            while new_ceiling == False:
                k = k + 1
                new_ceiling, idx = compute_ceiling_optimizing_overhead(c_set,
                        c-k, cov_videos, d_set, ceilings, videos)

        rand_idx.append([idx, new_ceiling])
        ceilings.append(new_ceiling)

        if (debug):
            print("ceilings (%d): %s" % (len(ceilings), ceilings))

        # update the list of videos that have been clustered
        cov_videos, clusters = update_cov_videos(new_ceiling, cov_videos,
                d_set, clusters, videos_with_tag)

        # update cardinality set and remove cov videos from dominance set
        del c_set
        c_set, d_set = update_cardinality_set(d_set, cov_videos)
        cc = len(cov_videos)

        if (debug):
            print("videos covered: %d of %d" % (cc, n_videos))

        # break if all videos have already been clustered
        if cc >= n_videos:
            print("All videos have been clustered!")
            break

    if (debug):
        print("\ncompute_ceilings_loop time: %s s" % (time.time() - starttime))
    return ceilings, clusters, rand_idx


def merge_last_cluster(ceilings, clusters, c):
    # check if last cluster has cardinality less than the desired one
    last_ceiling = ceilings[len(ceilings)-1]
    # if so, merge the second-to-last cluster to the last one
    if len(clusters[last_ceiling]) < c:
        if (debug):
            print("\nLast cluster is too small")
        ceiling_to_merge = ceilings[len(ceilings)-2]
        ceilings.pop(len(ceilings)-2)
        cluster_to_merge = clusters.pop(ceiling_to_merge)
        clusters[last_ceiling].update(cluster_to_merge)
        print("merged clusters: %s and %s" %
                (str(ceiling_to_merge), str(last_ceiling)))
        return True, ceilings, clusters
    return False, ceilings, clusters


def compute_statistics(clusters, videos, new_ceilings):
    video_overheads = np.zeros((videos.shape[0], 6))
    video_sizes = np.zeros(videos.shape[0])
    for ceiling in clusters:
        n_videos = len(clusters[ceiling])
        segments = (new_ceilings[ceiling])[0].values
        l = len(segments)
        for video_idx in clusters[ceiling]:
            video = videos.ix[video_idx].values
            n_segments = np.count_nonzero(video)
            # segment overhead in bytes
            overhead = segments[0:l] - video[0:l]
            # segment overhead normalized to video size
            with np.errstate(divide='ignore'):
                overhead_nrm = np.divide(overhead, video[0:l])

            overhead[np.isinf(overhead)] = 0
            overhead_nrm[np.isinf(overhead_nrm)] = 0
            # total size of a video
	    video_size = sum(video)
            # video overhead in bytes
	    video_overhead = sum(overhead)
            # video overhead normalized to its size
	    video_overhead_nrm = np.divide(video_overhead, video_size)

            # store stats
            video_overheads[video_idx, 0] = video_overhead
	    video_overheads[video_idx, 1] = video_overhead_nrm
	    video_overheads[video_idx, 2] = np.average(overhead_nrm)
	    video_overheads[video_idx, 3] = np.max(overhead_nrm)
	    video_overheads[video_idx, 4] = np.percentile(overhead_nrm, 90,
                    interpolation='nearest')
            video_overheads[video_idx, 5] = l - n_segments
	    video_sizes[video_idx] = video_size

            # compute video overhead nrm statistics
            column = 1
            data = np.zeros(8)
            data = get_column_stats(video_overheads, column, data)

    return video_overheads, video_sizes, data


def get_column_stats(stats, column, data):
    vector = stats[:, column]
    data[0] = len(vector)
    data[1] = min(vector)
    data[2] = np.average(vector)
    data[3] = max(vector)
    data[4] = np.percentile(vector, 50, interpolation='nearest')
    data[5] = np.percentile(vector, 75, interpolation='nearest')
    data[6] = np.percentile(vector, 90, interpolation='nearest')
    data[7] = np.percentile(vector, 99, interpolation='nearest')
    return data


#### Do all the things
def main():

    starttime = time.time()

    # load video dataset
    videos_with_tag = load_video_dataset(dataset_path)
    # create video dataset without videotags
    # contains only segment sizes, 0 (init) to 3005
    videos = videos_with_tag.drop(videos_with_tag.columns[0], axis=1)
    # pad segments to mtu size
    videos_mtu = pad_segments_to_mtu(videos)

    # desired cluster size
    c = min_cluster_size
    N = int(np.floor(videos.shape[0]/c))
#    c = np.ceil(videos.shape[0]/N)

    # distinct video lengths (# segments)
    lengths = video_lengths(videos)
    L = get_L(lengths)
    # distinct segment sizes (bytes)
    S = get_S(videos_mtu)
    # map (video_i) -> (l_i, smax_i)
    V = get_V(videos_mtu, lengths)

    # get initial dominance sets
    d_set = get_dominance_set(L, S, V)

    # compute cardinality of each set
    c_set = get_cardinality_set(d_set)

    # compute clusters
    # ceilings: list of (l,s) tuples
    # clusters: map (l,s) -> set(videos)
    ceilings, clusters, rand_idx = compute_ceilings(d_set, c, N, len(V), c_set,
            videos_mtu, videos_with_tag)

    # merge clusters if the last one is smaller than the desired size
    if merge_last == True:
        merged, ceilings, clusters = merge_last_cluster(ceilings, clusters, c)

    # compute the supersequence of each cluster
    # new_ceilings: list of segment (maximal size of the i-th segment
    # of any video in the cluster)
    # map_ceilings and map_clusters map with a cluster id in [0, n_clusters]
    new_ceilings, map_ceilings, map_clusters = get_super_sequence(clusters,
            videos_mtu, ceilings)

    # compute statistics
    video_overheads, video_sizes, data = compute_statistics(clusters,
            videos_mtu, new_ceilings)

    print("clustering time: %s s" % (time.time() - starttime))
    print("avg video overhead nrm: %s" % (str(data[2])))

    # save stats to csv file
    if save_files:
        fname_prefix = ("%s/n%s.cthr%s" % (output, str(c), str(c_thr)))
        fname = ("%s.padding.csv" % (fname_prefix))
        with open(fname,'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["videotag", "video size", "segments to add",
                "video overhead", "video overhead nrm",
                "average segment overhead nrm", "max segment overhead nrm",
                "90 pct segment overhead nrm"])
	    for row in range(videos.shape[0]):
	        writer.writerow([videos_with_tag.ix[row]['videotag'],
                    video_sizes[row], video_overheads[row, 5],
                    video_overheads[row, 0], video_overheads[row, 1],
                    video_overheads[row, 2], video_overheads[row, 3],
                    video_overheads[row, 4]])

        fname = ("%s.u_clusters.csv" % (fname_prefix))
        with open(fname,'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["videoid", "videotag", "cluster"])
            for k, v in clusters.items():
                for item in v:
                    writer.writerow([item,videos_with_tag.ix[item]['videotag'],k])

        to_sort = pd.read_csv(fname)
        cs = to_sort.sort_values(by='videoid')
        fname = ("%s.s_clusters.csv" % (fname_prefix))
        cs.to_csv(fname, index=False)

        fname = ("%s.u_clusters_with_cluster_idx.csv" % (fname_prefix))
        with open(fname,'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["videoid", "videotag", "cluster"])
            for k, v in map_clusters.items():
                for item in v[0]:
                    writer.writerow([item,videos_with_tag.ix[item]['videotag'],k])

        to_sort = pd.read_csv(fname)
        cs = to_sort.sort_values(by='videoid')
        fname = ("%s.s_clusters_with_cluster_idx.csv" % (fname_prefix))
        cs.to_csv(fname, index=False)

        fname = ("%s.ceilings.csv" % (fname_prefix))
        with open(fname,'w') as csvfile:
            writer = csv.writer(csvfile)
            for k,v in new_ceilings.items():
                segments = (new_ceilings[k])[0].values
                writer.writerow([k])
                writer.writerow([str(segment) for segment in segments])

        fname = ("%s.ceilings_with_cluster_idx.csv" % (fname_prefix))
        with open(fname,'w') as csvfile:
            writer = csv.writer(csvfile)
            for k,v in map_ceilings.items():
                segments = (map_ceilings[k])[0].values
                writer.writerow([k])
                writer.writerow([len(segments)])
                writer.writerow([str(segment) for segment in segments])

        fname = ("%s.ovhstats.csv" % (fname_prefix))
        with open(fname,'a') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["num-actual-clusters", "avg-overhead", "max-overhead",
                "input-min-cluster-size", "actual-min-cluster-size"])
            actual_min_csize = min([ len(v[0]) for k,v in map_clusters.items() ])
            writer.writerow([int(N), data[2], data[3], c, actual_min_csize])
#            writer.writerow(["n_clusters", "c_thr", "min_size", #"n_elem",
#                "min", "avg", "max", "50pct", "75pct", "90pct", "99pct"])
#            writer.writerow([int(N), c_thr, c, data[0], data[1], data[2], data[3],
#                data[4], data[5], data[6], data[7]])

        if choose_random == True:
            fname = ("%s.rand_choices.csv" % (fname_prefix))
            with open(fname,'w') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(["List of random choices ("+str(len(rand_idx))+")"])
                for i in range(len(rand_idx)):
                    writer.writerow([str(i) + ") " + str(rand_idx[i])])
                writer.writerow(["Last cluster merged: " + str(merged)])
                writer.writerow(["Avg video overhead nrm: " + str(data[2])])

            with open(str(filecpu)+'.csv','a+') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow([output + '\t' + str(data[2])])


if __name__ == "__main__":

    #### Parsing global variables
    parser = argparse.ArgumentParser(description="video clustering algorithm")
    parser.add_argument("-i", "--ifile", dest="dataset_path", action="store",
        help="dataset file in input", required=True)
#    parser.add_argument("-n", "--nclusters", dest="N", action="store",
#        help="desired number of clusters", default=1)
    parser.add_argument("-m", "--minsize", dest="min_cluster_size", action="store",
        help="min number of elements in cluster", default=2)
    parser.add_argument("-c", "--cthreshold", dest="c_thr", action="store",
        help="cardinality threshold (different from min cluster size)", default=10)
    parser.add_argument("-o", "--odir", dest="output", action="store",
        help="output folder", default=".")
    parser.add_argument("-f", "--filecpu", dest="filecpu", action="store",
        help="output file relative to the cpu in use (use only when parallelizing)")
    args = parser.parse_args()
    print args

    dataset_path = args.dataset_path
#    min_cluster_size = 10
#    N = int(args.N)
    min_cluster_size = int(args.min_cluster_size)
    c_thr = int(args.c_thr)
    output = args.output
    if args.filecpu:
        filecpu = args.filecpu
    else:
        filecpu = 0

    main()

