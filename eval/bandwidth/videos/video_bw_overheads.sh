#!/bin/bash 

ifile="video_clustering_dataset"
n_files=1218
#minsize=( 1218 512 256 128 64 32 16 8 4 2 1 )
minsize=( 256 )

outdir="out"
rm -rf $outdir
mkdir $outdir

##########################
## remove old output files
##########################

function cleanup()
{
  cmd="rm -f $outdir/*.csv; rm -f $outdir/roundup.output"
  echo "$cmd"
  eval "$cmd"

  for i in "${minsize[@]}";
  do
    cmd="rm -f $outdir/output_$i"
    echo "$cmd"
    eval "$cmd"
  done
}

###########################
## pacer's video clustering
###########################

function process_wrap()
{
  n=$1
  m=$2
  ifile=$3
  outfile=""
  cmd="python pacer_video_clustering.py -m $i -c 10 -i $ifile  \
    -o $outdir -f \"overhead-list-$i\" > $outdir/output_$i"
  echo "$cmd"
  eval "$cmd"
}

ncores=`nproc`
count=0
maxclustersize=$n_files

##########################
## parallelized clustering
##########################

date

cleanup

echo "== PADDING OVERHEADS FROM PACER'S CLUSTERING =="
for i in "${minsize[@]}";
do
#  date
  n=$(( $n_files / $i ))
  process_wrap $n $i $ifile &
  echo "[$!] Clustering with minsize $i, n_clusters = $n..."
  count=$(( $count + 1 ))

  while [ $count -ge $ncores ]; do
    joblist=`jobs -p`
    while read -r job; do
      if ! [[ -e /proc/$job ]]; then
        count=$(( $count - 1 ))
      fi
    done <<< "$joblist"
  done
#  python pacer_video_clustering.py -m $i -c 10 -i $ifile  \
#    -o . -f "overhead-list-"$i > output_$i
#  date
done

FAIL=0
joblist=`jobs -p`
while read -r job; do
  echo "Waiting for $job to complete..."
  wait $job || let "FAIL+=1"
done <<< "$joblist"

if [[ "$FAIL" == "0" ]]; then
  echo "Success!"
else
  echo "Fail ($FAIL)"
fi

date

outfile="ovhstats.csv"
head -n1 "$outdir/n${minsize[0]}.cthr10.$outfile" >> $outdir/$outfile
for i in "${minsize[@]}"; do
  n=$(( $n_files / $i ))
  tail -n1 "$outdir/n$i.cthr10.$outfile" >> $outdir/$outfile
done

#tail -n1 "$outdir/n$n_files.$outfile" >> $outdir/$outfile

#######################################
## padding mechanisms from related work
#######################################

echo "== PADDING OVERHEADS FROM PRIOR TECHNIQUES =="
cmd="python video_roundup_padding.py -i $ifile > $outdir/roundup.output 2>&1"
echo "$cmd"
eval "$cmd"
