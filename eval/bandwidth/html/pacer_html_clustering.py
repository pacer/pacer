
import pandas as pd
import numpy as np
import sys
import collections
import itertools
import csv
import time
import argparse

#### Global parameters
MTU = 1448

## debug prints
debug = True

## save detailed stats breakdown to files
save_files = True

## do not do clustering, just padding
no_cluster = False

## merge last cluster if smaller than min cluster size
merge_last = True

## round data to MTU
def pad_to_mtu(sizes):
    return np.ceil(sizes/MTU) * MTU


## no clustering: worst case scenario
def pad_everything_to_the_max(sizes, original_sizes):

    pages = np.zeros(shape=(len(sizes),7))
    max_value = max(sizes)

    # for every page in the dataset
    for i in range(len(sizes)):
        # get page size value
        size = sizes[i]
        original_size = original_sizes[i]
        # get padding and overhead
        padding = max_value - size
        overhead = padding/float(size)
        tot_overhead = (max_value - original_size)/float(original_size)
        # store results
        pages[i, 0] = i
        pages[i, 1] = size
        pages[i, 2] = max_value
        pages[i, 3] = padding
        pages[i, 4] = overhead
        pages[i, 5] = original_size
        pages[i, 6] = tot_overhead

    return pages


## get number of elements in each cluster
## (cluster_frequency = cluster_value, n_elements)
def get_cluster_frequency(pages):
    cluster_frequency = np.unique(pages[:,2], return_counts=True)
    return cluster_frequency


## check if there is a cluster with less than min_cluster_size elements
def get_small_clusters(cluster_frequency, min_cluster_size):
    # get indexes of small clusters
    small_clusters_ix = np.array(np.where(cluster_frequency[1] < min_cluster_size))
    # remove empty subarray
    small_clusters_ix = small_clusters_ix[0]
    return small_clusters_ix


## merge clusters with a number of elements less than min_cluster_size
def merge_small_clusters(small_clusters_ix, pages, cluster_frequency):
    for sc_ix in small_clusters_ix:
        # get additional padding
        try:
            bytes_to_add = cluster_frequency[0][sc_ix + 1] - cluster_frequency[0][sc_ix]
            if (debug):
                print("Merging the following clusters: ")
                print(cluster_frequency[0][sc_ix])
                print(cluster_frequency[0][sc_ix+1])
        except:
            if merge_last == False:
                if (debug):
                    print("Last cluster cannot be merged!")
                # this was the last cluster
                return False
            # merge second-to-last cluster to last cluster
            if (debug):
                print("Merging last cluster backwards...")
            bytes_to_add = cluster_frequency[0][sc_ix] - cluster_frequency[0][sc_ix - 1]
            # merge clusters updating results
            for i in range(len(pages)):
                # if an element is in the second-to-last cluster
                if pages[i, 2] == cluster_frequency[0][sc_ix - 1]:
                    # compute new results
                    # cluster
                    pages[i, 2] = cluster_frequency[0][sc_ix]
                    # padding = cluster - size
                    pages[i, 3] = pages[i, 2] - pages[i, 1]
                    # overhead (nrm) = padding / size
                    pages[i, 4] = pages[i, 3] / float(pages[i, 1])
                    # total overhead (nrm) = cluster - original size / original size
                    pages[i, 6] = (pages[i, 2] - pages[i, 5])/ float(pages[i, 5])
            # this was the last cluster to merge
            return False
        # merge clusters updating results
        for i in range(len(pages)):
            # if an element is in a small cluster
            if pages[i, 2] == cluster_frequency[0][sc_ix]:
                # compute new results
                # cluster
                pages[i, 2] = cluster_frequency[0][sc_ix + 1]
                # padding = cluster - size
                pages[i, 3] = pages[i, 2] - pages[i, 1]
                # overhead (nrm) = padding / size
                pages[i, 4] = pages[i, 3] / float(pages[i, 1])
                # total overhead (nrm) = cluster - original size / original size
                pages[i, 6] = (pages[i, 2] - pages[i, 5])/ float(pages[i, 5])
        break
    # there could be more clusters to merge
    return True


## get distribution stats
def get_column_stats(column):
    stats = np.zeros(8)
    stats[0] = len(column)
    stats[1] = min(column)
    stats[2] = np.average(column)
    stats[3] = max(column)
    stats[4] = np.percentile(column, 50, interpolation='linear')
    stats[5] = np.percentile(column, 75, interpolation='linear')
    stats[6] = np.percentile(column, 90, interpolation='linear')
    stats[7] = np.percentile(column, 99, interpolation='linear')
    return stats


## print distribution stats
def print_column_stats(stats):
    print("Number of elements:")
    print(stats[0])
    print("Min:")
    print(stats[1])
    print("Avg:")
    print(stats[2])
    print("Max:")
    print(stats[3])
    print("50th percentile:")
    print(stats[4])
    print("75th percentile:")
    print(stats[5])
    print("90th percentile:")
    print(stats[6])
    print("99th percentile:")
    print(stats[7])


def print_clusters(pages):
    cluster_frequency = get_cluster_frequency(pages)
    print("Clusters (cluster_id, n_elements): %s\n" % (str(cluster_frequency)))


## print statistics
def print_statistics(pages, cluster_frequency):
    print("")
    print("Number of clusters:")
    print((len(cluster_frequency[0])))
    print("Min number of elements in a cluster:")
    print((min(cluster_frequency[1])))
    print("Avg number of elements in a cluster:")
    print((np.average(cluster_frequency[1])))
    print("Median number of elements in a cluster:")
    print((np.percentile(cluster_frequency[1], 50, interpolation='linear')))
    print("Max number of elements in a cluster:")
    print((max(cluster_frequency[1])))
    overhead_stats = get_column_stats(pages[:,4])
    tot_overhead_stats = get_column_stats(pages[:,6])
    print("Stats: overhead (nrm)")
    print_column_stats(overhead_stats)
    print("Stats: total overhead (nrm)")
    print_column_stats(tot_overhead_stats)
    print_worst_case_details(pages)
    print("")
    return (overhead_stats[2], overhead_stats[3],
            tot_overhead_stats[2], tot_overhead_stats[3])


def print_worst_case_details(pages):
    max_ix = np.argmax(pages[:,4])
    print("Statistics of the element with maximum overhead...")
    print("Size")
    print((pages[max_ix, 1]))
    print("Ceiling size")
    print((pages[max_ix, 2]))
    print("Padding (bytes)")
    print((pages[max_ix, 3]))
    print("Overhead")
    print((pages[max_ix, 4]))
    print("Original size (not MTU-rounded)")
    print((pages[max_ix, 5]))
    print("Total overhead")
    print((pages[max_ix, 6]))


def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()


def main():

    # load dataset
    data = pd.read_csv(dataset_path, delimiter='\t')

    # convert integer type to float, because in some versions of python/pandas/numpy
    # ceiling operation on integer fractions yields incorrect results
    original_sizes = (data['size'].values).astype(np.float64)
    sizes = pad_to_mtu(original_sizes)

    # get size distribution stats
    dataset_stats = get_column_stats(original_sizes)
    dataset_mtu_stats = get_column_stats(sizes)
    print("\nSize distribution (original, not rounded at MTU size)")
    print("")
    print_column_stats(dataset_stats)
    print("\nSize distribution (rounded at MTU size)")
    print("")
    print_column_stats(dataset_mtu_stats)

    # no clustering: pad to max size in dataset
    if no_cluster == True:
        pages = pad_everything_to_the_max(sizes, original_sizes)
        cluster_frequency = get_cluster_frequency(pages)
        print_statistics(pages, cluster_frequency)
        sys.exit(1)

    # Note: the percentiles are in a range(1, 100), thus if we want more than
    # 100 clusters, we need also other points between percentiles. Since the
    # vector is ordered, we can have X clusters picking vector indexes equally
    # spaced with a step of vector_length / n_clusters.
    # Percentiles can be computed as:
    # percentiles = np.percentile(sizes, range(1,101), interpolation='nearest')
    # where interpolation='nearest' makes sure we pick a point in the vector.

    # get equally-spaced indexes
    n_clusters = int(np.floor(len(sizes)/min_cluster_size))
    step = int(len(sizes)/n_clusters)
    percentiles_ix = np.arange(0 + step, len(sizes), step)
    # add maximum value
    percentiles_ix= np.append(percentiles_ix, len(sizes)-1)
    # get equally-spaced size values
    ceilings = sizes[percentiles_ix]
    # preallocate results variable
    pages = np.zeros(shape=(len(sizes),7))

    # for every page in the dataset
    for i in range(len(sizes)):
        starttime = time.time()
        # get page values
        size = sizes[i]
        original_size = original_sizes[i]
        # find index of first greater element
        cluster_id = np.searchsorted(ceilings, size)
        # get padding and overheads
        padding = ceilings[cluster_id] - size
        overhead = padding/float(size)
        # store results
        # page index
        pages[i, 0] = i
        # page size (bytes)
        pages[i, 1] = size
        # size of the related cluster ceiling (bytes)
        pages[i, 2] = ceilings[cluster_id]
        # bytes to add to the page size
        pages[i, 3] = padding
        # padding normalized to the page size
        pages[i, 4] = overhead
        # original size (not MTU-rounded)
        pages[i, 5] = original_size
        # total overhead (nrm)
        pages[i, 6] = (ceilings[cluster_id] - original_size)/float(original_size)
        # progress bar
        # progress(i, len(sizes), status=' clustering')
    print("compute_clusters_loop time: %s s" % (time.time() - starttime))

    # get number of elements in each cluster
    # (cluster_frequency = cluster_value, n_elements)
    cluster_frequency = get_cluster_frequency(pages)

    # check if there is a cluster with less than min_cluster_size elements
    small_clusters_ix = get_small_clusters(cluster_frequency, min_cluster_size)
    print("\nThere are %d small clusters to merge..." % small_clusters_ix.size)
    print(cluster_frequency)
    print(small_clusters_ix)

    # until there are no small clusters left
    while small_clusters_ix.size != 0:
        # merge clusters with a number of elements less than min_cluster_size
        keep_merging = merge_small_clusters(small_clusters_ix, pages, cluster_frequency)
        # if this is the last cluster and we are not merging it, break now
        # (no need to recompute the cluster frequency)
        if keep_merging == False and merge_last == False:
            break
        # get the number of elements in each cluster
        cluster_frequency = get_cluster_frequency(pages)
        # check if there are small clusters
        small_clusters_ix = get_small_clusters(cluster_frequency, min_cluster_size)
        print("There are %d small clusters to merge..." % small_clusters_ix.size)
        print(cluster_frequency)
        print(small_clusters_ix)
        # if this is the last cluster and we have merged it, break now
        # (we have also recomputed the cluster frequency)
        if keep_merging == False:
            break

    avg_ovh, max_ovh, avg_tovh, max_tovh = print_statistics(pages, cluster_frequency)
    print_clusters(pages)

    n_actual_clusters = len(cluster_frequency[0])
    actual_min_cluster_size = min(cluster_frequency[1])

    if save_files == True:
        with open(output, 'a') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["num-actual-clusters", "avg-overhead", "max-overhead",
                "input-min-cluster-size", "actual-min-cluster-size"])
            writer.writerow([n_actual_clusters, avg_ovh, max_ovh,
                min_cluster_size, actual_min_cluster_size])
#            writer.writerow([n_clusters, n_actual_clusters, avg_ovh, max_ovh,
#                avg_tovh, max_tovh, min_cluster_size, actual_min_cluster_size])


if __name__ == "__main__":

    #### Parsing global variables

    parser = argparse.ArgumentParser(description="1-D clustering algorithm")
    parser.add_argument("-i", "--ifile", dest="dataset_path", action="store",
        help="dataset file in input", required=True)
    parser.add_argument("-m", "--minsize", dest="min_cluster_size", action="store",
        help="minimum cluster size", default=2)
    parser.add_argument("-o", "--ofile", dest="output", action="store",
        help="output file", required=True)
    args = parser.parse_args()

    print(args)

    dataset_path = args.dataset_path
    min_cluster_size = int(args.min_cluster_size)
    output = args.output

    main()
