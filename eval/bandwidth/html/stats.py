########################################################
## cluster-stats.py
##
## author: aasthakm
##   date: 04 Jan 2021
##
## common code related to computing clustering overheads
########################################################

import math
import pandas as pd
import numpy as np
import sys
import collections
import itertools
import csv
import time
import argparse

# get distribution stats
def get_column_stats(column):
    stats = np.zeros(8)
    stats[0] = len(column)
    stats[1] = min(column)
    stats[2] = np.average(column)
    stats[3] = max(column)
    stats[4] = np.percentile(column, 50, interpolation='linear')
    stats[5] = np.percentile(column, 75, interpolation='linear')
    stats[6] = np.percentile(column, 90, interpolation='linear')
    stats[7] = np.percentile(column, 99, interpolation='linear')
    return stats


# print distribution stats
def print_column_stats(stats):
    print("Number of elements:")
    print(stats[0])
    print("Min:")
    print(stats[1])
    print("Avg:")
    print(stats[2])
    print("Max:")
    print(stats[3])
    print("50th percentile:")
    print(stats[4])
    print("75th percentile:")
    print(stats[5])
    print("90th percentile:")
    print(stats[6])
    print("99th percentile:")
    print(stats[7])


"""
input: array of [padded size, original size] tuples
stats for round up padding scheme (from related work)
"""
def get_roundup_padding_stats(padded_sizes):
    ## find number of elements padded to same size
    padded_size_set = set([ ps[0] for ps in padded_sizes ])
#    print(set(padded_sizes))

    padded_size_list = [ ps[0] for ps in padded_sizes ]
    padded_size_freq_set = []
    for key, group in itertools.groupby(padded_size_list):
        padded_size_freq_set.append([key, len(list(group)) ])

    min_cluster_size = 99999999999
    max_cluster_size = 0
    min_cluster_id = -1
    max_cluster_id = -1
    count_min_cluster_size = 0
    count_max_cluster_size = 0
    for i in range(len(padded_size_freq_set)):
        ps = padded_size_freq_set[i]
        if (ps[1] < min_cluster_size):
            min_cluster_size = ps[1]
            min_cluster_id = ps[0]
            # reset count when new min found
            count_min_cluster_size = 0
        if (ps[1] == min_cluster_size):
            count_min_cluster_size += 1
        if (ps[1] > max_cluster_size):
            max_cluster_size = ps[1]
            max_cluster_id = ps[0]
            # reset count when new max found
            count_max_cluster_size = 0
        if (ps[1] == max_cluster_size):
            count_max_cluster_size += 1

#    for ps in padded_size_freq_set:
#        print("%d\t%d" % (ps[0], ps[1]))

    return [ padded_size_set, min_cluster_id, min_cluster_size, count_min_cluster_size,
            max_cluster_id, max_cluster_size, count_max_cluster_size ]


"""
input: array of [padded size, original size] tuples
"""
def compute_padding_ovh(padded_sizes):
    tot_ovh = 0
    avg_ovh = 0
    tot_rel_ovh = 0
    avg_rel_ovh = 0
    max_ovh = 0
    max_rel_ovh = 0.0
    for ps in padded_sizes:
        ovh = ps[0] - ps[1]
        rel_ovh = float(ovh)/ps[1]
        tot_ovh += ovh
        tot_rel_ovh += rel_ovh
        if (max_ovh < ovh):
            max_ovh = ovh
        if (max_rel_ovh < rel_ovh):
            max_rel_ovh = rel_ovh

    avg_ovh = tot_ovh/len(padded_sizes)
    avg_rel_ovh = tot_rel_ovh/len(padded_sizes)

    return [avg_ovh, max_ovh, avg_rel_ovh, max_rel_ovh]


