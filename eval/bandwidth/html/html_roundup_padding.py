######################################################
## roundup-padding.py
##
## author: aasthakm
##   date: 04 Jan 2021
##
## implement round up to pow2 and next multiple of 100
## (approaches presented in tamaraw, cs-buflo)
######################################################

import math
import pandas as pd
import numpy as np
import sys
import collections
import itertools
import csv
import time
import argparse

from stats import *

#### global parameters

debug = True
MTU = 1448
L = 100

data_path = "medical-datasort"

## three variants of the function to compute next power of 2
def power_bit_length(x):
    return 2**(x-1).bit_length()

def shift_bit_length(x):
    if (x == 0):
        return x

    return (1 << ((x-1).bit_length()))

def power_log(x):
    return 2**(math.ceil(math.log(x, 2)))

def round_pow2(sizes):
    rsizes = []
    for os in sizes:
        rs = shift_bit_length(os)
        rsizes.append([rs, os])

#    print(np.array(rsizes))
    return np.array(rsizes)

def round_mul_L(sizes, L):
    rsizes = []
    for os in sizes:
        rs = (int((os-1)/L) + 1) * L
        rsizes.append([rs, os])

#    print(np.array(rsizes))
    return np.array(rsizes)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="roundup padding algorithm")
    parser.add_argument("-L", "--roundfact", dest="L", default=100,
            help="rounding factor for tamaraw style padding")
    parser.add_argument("-i", "--ifile", dest="data_path", action="store",
            help="data_path", required=True)
    parser.add_argument("-o", "--ofile", dest="output", action="store",
            help="data_path", required=True)
    args = parser.parse_args()

    data_path = args.data_path
    output = args.output
    L = int(args.L)

    print("input: %s" % (data_path))

    ## load dataset
    data = pd.read_csv(data_path, delimiter='\t')
    original_sizes = data['size'].values

    padded_sizes = round_pow2(original_sizes)

#    ## get size distribution stats
#    dataset_stats = get_column_stats(original_sizes)
#    dataset_mtu_stats = get_column_stats(padded_sizes)
#    print("\nSize distribution (original, without padding)")
#    print_column_stats(dataset_stats)
#    print("\nSize distribution (rounded at MTU size)")
#    print_column_stats(dataset_mtu_stats)

    (padded_size_set, min_cluster_id, min_cluster_size, count_min_cluster_size,  \
        max_cluster_id, max_cluster_size, count_max_cluster_size) = \
        get_roundup_padding_stats(padded_sizes)
    (avg_ovh, max_ovh, avg_rel_ovh, max_rel_ovh) = compute_padding_ovh(padded_sizes)

    print("========== round pow2 ============")
    print("# uniq clusters %d, min cluster %d size %d freq %d, "
        "max cluster %d size %d freq %d" % (len(padded_size_set),
        min_cluster_id, min_cluster_size, count_min_cluster_size,
        max_cluster_id, max_cluster_size, count_max_cluster_size))
    print("avg-ovh\tmax-ovh\tavg-rel-ovh\tmax-rel-ovh")
    print("%f\t%d\t%f\t%f" % (avg_ovh, max_ovh, avg_rel_ovh, max_rel_ovh))

    f = open("%s-round2.csv" % output, "w")
    f.write("size\torig\n")
    for ps in padded_sizes:
        f.write("%d\t%d\n" % (ps[0], ps[1]))
    f.close()

    padded_sizes = round_mul_L(original_sizes, L)
    (padded_size_set, min_cluster_id, min_cluster_size, count_min_cluster_size,  \
        max_cluster_id, max_cluster_size, count_max_cluster_size) = \
        get_roundup_padding_stats(padded_sizes)
    (avg_ovh, max_ovh, avg_rel_ovh, max_rel_ovh) = compute_padding_ovh(padded_sizes)

    print("========== round %d ==========" % L)
    print("# uniq clusters %d, min cluster %d size %d freq %d, "
        "max cluster %d size %d freq %d" % (len(padded_size_set),
        min_cluster_id, min_cluster_size, count_min_cluster_size,
        max_cluster_id, max_cluster_size, count_max_cluster_size))
    print("avg-ovh\tmax-ovh\tavg-rel-ovh\tmax-rel-ovh")
    print("%f\t%d\t%f\t%f" % (avg_ovh, max_ovh, avg_rel_ovh, max_rel_ovh))

    f = open("%s-round%d.csv" % (output, L), "w")
    f.write("size\torig\n")
    for ps in padded_sizes:
        f.write("%d\t%d\n" % (ps[0], ps[1]))
    f.close()


