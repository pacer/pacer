#!/bin/bash

#######################################################
# html_bw_clustering.sh
#
# author: aasthakm
#
# wrapper over clustering script
# can be used for wiki, wiktionary, and medical dataset
# known values for datasets:
# medical-datasort  6879
# wiki-datasort 14257493
# wikt-datasort 5027344
#######################################################

n_files=$1
ifile=$2
ofile=$3
outdir=$4

##########################
## remove old output files
##########################

#outdir="out"
rm -rf $outdir
mkdir $outdir

function cleanup()
{
  cmd="rm -f $outdir/*.csv $outdir/*.csv.*; rm -f $outdir/roundup.output"
  echo "$cmd"
  eval "$cmd"

  for i in "${minsize[@]}";
  do
    cmd="rm -f output_$i"
    echo "$cmd"
    eval "$cmd"
  done
}

function process_wrap()
{
  n=$1
  m=$2
  ifile=$3
  outfile="$outdir/n$m.ovhstats.csv"
  cmd="python pacer_html_clustering.py -m $m -i $ifile -o \"$outfile\" > $outdir/output_$m"
  echo "$cmd"
  eval "$cmd"
}

logres=$( echo "l($n_files)/l(2)" | bc -l )
floores=$( echo "$logres/1" | bc )
expres=$( echo "2^$floores" | bc )

echo $n_files $logres $floores $expres

ncores=`nproc`
count=0
maxclustersize=$n_files

##########################
## parallelized clustering
##########################

date

cleanup

echo "== PADDING OVERHEADS FROM PACER'S CLUSTERING =="
for (( i = 1; i < $maxclustersize; i = $(($i * 2)) )); do
  m=$i
  n=$(( $n_files / $m ))
  process_wrap $n $m $ifile &
  echo "[$!] Clustering with min_sizes = $m, n_clusters = $n ..."
  count=$(( $count + 1 ))

  while [ $count -ge $ncores ]; do
    joblist=`jobs -p`
    while read -r job; do
      if ! [[ -e /proc/$job ]]; then
        count=$(( $count - 1 ))
      fi
    done <<< "$joblist"
  done
done

m=$n_files
n=$(( $n_files / $m ))
process_wrap $n $m $ifile &
echo "[$!] Clustering with min_sizes = $m, n_clusters = $n..."

FAIL=0
joblist=`jobs -p`
while read -r job; do
  echo "Waiting for $job to complete..."
  wait $job || let "FAIL+=1"
done <<< "$joblist"

if [[ "$FAIL" == "0" ]]; then
  echo "Success!"
else
  echo "Fail ($FAIL)"
fi

date

outfile="ovhstats.csv"
head -n1 "$outdir/n1.$outfile" >> $outdir/$outfile
for (( i = 1; i < $maxclustersize; i = $(($i * 2)) )); do
  m=$i
  n=$(( $n_files / $m ))
  tail -n1 "$outdir/n$m.$outfile" >> $outdir/$outfile
done

tail -n1 "$outdir/n$n_files.$outfile" >> $outdir/$outfile

#######################################
## padding mechanisms from related work
#######################################

echo "== PADDING OVERHEADS FROM PRIOR TECHNIQUES =="
cmd="python html_roundup_padding.py -i $ifile -o $outdir/$ofile > $outdir/roundup.output 2>&1"
echo "$cmd"
eval "$cmd"
