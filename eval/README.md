## Instructions

### Expected directory structure for code
* `rootdir=/local/sme`
* `workdir=$rootdir/workspace`
* `expdir=$rootdir/exp`
* all code is stored in `$workdir/pacer`
* all experiment output from `run` scripts of attack, video, and medical are stored in `$expdir/attack`, `$expdir/video`, `$expdir/mw_micro`, respectively
* transmit schedule directories should be copied or linked into `$rootdir/daemon_profiler/` in the guest VMs.
* video dataset is hosted at `/videos` in the frontend VM as well as the backend VM
* pre-generated cache of html files for mediawiki is hosted at `/wiktionary_cache/wiktionary_cache` in the frontend VM


### Steps to set up from scratch

#### Cloning repo:
```
git clone --recurse-submodules https://gitlab.mpi-sws.org/pacer/pacer.git
cd pacer
git submodule foreach --recursive git pull origin master
```

#### Frontend host
- Set up HyPace on the frontend host. Follow the [README](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/frontend/hypace/README.md).
- Configure SR-IOV VFs using the following script: `$workdir/pacer/frontend/hypace/scripts/vfsetup.sh eno1 mac-ip-map.txt 1`. The script takes three arguments: the name of the physical NIC interface, a file input, and number of VFs to configure as argument. The file input format is one <MAC address,IPv4 address> tuple per line (e.g., 00:16:3e:11:22:33,123.45.678.90) and there are as many lines as the number of VFs to configure.
- Create the guest VM using: `xl create /etc/xen/vm1-mw.cfg`
- Once the guest boots up, `ssh root@139.19.218.101`. Inside the guest:
  - Follow the [README](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/frontend/gpace/README.md), if required.
  - Setup Apache server: Follow the [README](https://gitlab.mpi-sws.org/pacer/pacer/-/tree/main/apps)
  - Start Apache server: `/usr/local/apache2/bin/apachectl -k start`
  - Start MySQL server: `/etc/init.d/mysql start`
  - Setup ProfPace: `cd $workdir/pacer/frontend/profpace/proflib; make`
  - Copy schedules for experiments to `/local/sme/daemon_profiler`.

#### Backend host
- Set up HyPace on the frontend host. Follow the [README](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/backend/hypace/README.md).
- Run `$workdir/pacer/frontend/hypace/scripts/vfsetup.sh eno1 mac-ip-map.txt 1`
- Create the guest VM using: `xl create /etc/xen/vm2-attack.cfg`
- Once the guest boots up, `ssh root@139.19.218.103` or `ssh root@139.19.218.104`. Inside the guest:
  - Follow the [README](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/backend/gpace/README.md), if required.
  - Setup ProfPace: `cd $workdir/pacer/backend/profpace/proflib; make`
  - Init and load memcache server: `$workdir/pacer/apps/memcached-1.6.9/pacer_memcached.sh`

#### Client host
- Once the client boots up:
  - Follow the [README](https://gitlab.mpi-sws.org/pacer/pacer/-/blob/main/backend/gpace/README.md), if required.
  - Setup ProfPace: `cd $workdir/pacer/client/profpace/proflib; make`
  - Setup wrk2: `cd $workdir/pacer/apps/wrk2; make`


### Scripts to reproduce results from the paper
#### section 6.2
  - cd $workdir/pacer/eval/bandwidth/videos; ./video_bw_overheads.sh
  - cd $workdir/pacer/eval/bandwidth/html; ./html_bw_overheads.sh medical_clustering_dataset 6879
  - cd $workdir/pacer/eval/bandwidth/html; ./html_bw_overheads.sh enwiki_clustering_dataset 14257493
  - cd $workdir/pacer/eval/bandwidth/html; ./html_bw_overheads.sh wiktionary_clustering_dataset 5027344

#### section 6.3, video service
  - Copy schedules for the video experiment to the frontend and backend guest VMs:
    ```
    scp -rq $workdir/pacer/eval/video/experiment/data/schedules_video_1tier root@<frontend-vm-ip>:/local/sme/daemon_profiler/
    scp -rq $workdir/pacer/eval/video/experiment/data/schedules_video_2tier_frontend root@<frontend-vm-ip>:/local/sme/daemon_profiler/
    scp -rq $workdir/pacer/eval/video/experiment/data/schedules_video_2tier_backend root@<backend-vm-ip-1>:/local/sme/daemon_profiler/
    scp -rq $workdir/pacer/eval/video/experiment/data/schedules_video_2tier_backend root@<backend-vm-ip-2>:/local/sme/daemon_profiler/
    ```
  - cd $workdir/pacer/eval/video/experiment; ./run_video_exp.sh
  - for the baseline with a single-tier setup, set the configuration flags in the script as follows:
    ```
    cfg=$cfg_base
    cfg_enf_on_client=0
    send_ascii_profiles_for_enf=0
    play_sleep=5
    play_dur=150
    bufgoal_s=60
    rebufgoal_s=30
    num_videos=1
    do_multitier=0
    ```
  - for the baseline with a two-tier setup, set the configuration flags in the script as follows:
    ```
    cfg=$cfg_base
    cfg_enf_on_client=0
    send_ascii_profiles_for_enf=0
    play_sleep=5
    play_dur=150
    bufgoal_s=60
    rebufgoal_s=30
    num_videos=1
    do_multitier=1
    ```
  - for pacer with a single-tier setup, set the configuration flags in the script as follows:
    ```
    cfg=$cfg_enf
    cfg_enf_on_client=1
    send_ascii_profiles_for_enf=1
    play_sleep=5
    play_dur=150
    bufgoal_s=60
    rebufgoal_s=30
    num_videos=1
    do_multitier=0
    ```
  - for pacer with a two-tier setup, set the configuration flags in the script as follows:
    ```
    cfg=$cfg_enf
    cfg_enf_on_client=1
    send_ascii_profiles_for_enf=1
    play_sleep=5
    play_dur=150
    bufgoal_s=60
    rebufgoal_s=30
    num_videos=1
    do_multitier=1
    ```
  - vary the number of concurrent clients as 1, 2, 4, 8, 16, 32, 64, 128 at line 858
  - for N repetitions, set iter as 1, 2, ..., N at line 861
  - no other configs should require changes

#### section 6.3, medical service
  - Copy schedules for the medical experiment to the frontend guest VM:
    ```
    scp -rq $workdir/pacer/eval/medical/experiment/data/schedules_med_100pct root@<frontend-vm-ip>:/local/sme/daemon_profiler/
    scp -rq $workdir/pacer/eval/medical/experiment/data/schedules_med_80pct root@<frontend-vm-ip>:/local/sme/daemon_profiler/
    ```
  - cd $workdir/pacer/eval/medical/experiment; ./run_med_exp.sh
  - for the baseline with trace workload, set the configuration flags in the script as follows:
    ```
    set "for cfg in $cfg_base" at line 900
    select workload_trace_M65_L30_XL5 in cdataset_arr at lin 883 (keep others commented)
    set minimum_experiment_time to 120 at line 898
    set base_rate=9 at line 932
    vary number of concurrent clients as 1, 2, 4, 8, 16, 32, 64, 128, 152, 160, 200 at line 934
    ```
  - for the baseline with largest file workload, set the configuration flags in the script as follows:
    ```
    set "for cfg in $cfg_base" at line 900
    select medical_xlarge_ceiling.cdataset in cdataset_arr at lin 883 (keep others commented)
    set minimum_experiment_time to 120 at line 898
    set base_rate=3 at line 955
    vary number of concurrent clients as 1, 2, 4, 8, 16, 32, 64, 128, 250, 300, 384 at line 953
    ```
  - for pacer with trace workload with 100th percentile latency schedules, set the configuration flags in the script as follows:
    ```
    set "for cfg in $cfg_enf" at line 900
    select workload_trace_M65_L30_XL5 in cdataset_arr at lin 883 (keep others commented)
    set minimum_experiment_time to 120 at line 898
    set base_rate=5 at line 938
    vary number of concurrent clients as 1, 2, 4, 8, 16, 32, 64, 115, 128, 160, 200, 220, 230, 240 at line 939
    select "schedules_med_100pct" in profiles_dir_arr at lin 890 (keep others commented)
    ```
  - for pacer with trace workload with 80th percentile latency schedules, set the configuration flags in the script as follows:
    ```
    set "for cfg in $cfg_enf" at line 900
    select workload_trace_M65_L30_XL5 in cdataset_arr at lin 883 (keep others commented)
    set minimum_experiment_time to 120 at line 898
    set base_rate=5 at line 938
    vary number of concurrent clients as 1, 2, 4, 8, 16, 32, 64, 115, 128, 160, 200 at line 939
    select "schedules_med_80pct" in profiles_dir_arr at lin 890 (keep others commented)
    ```
  - for pacer with largest file with 100th percentile latency schedules, set the configuration flags in the script as follows:
    ```
    set "for cfg in $cfg_enf" at line 900
    select medical_xlarge_ceiling.cdataset in cdataset_arr at lin 883 (keep others commented)
    set minimum_experiment_time to 120 at line 898
    set base_rate=3 at line 938
    vary number of concurrent clients as 1, 2, 4, 8, 16, 32, 64, 128, 250 at line 939
    select "schedules_med_100pct" in profiles_dir_arr at lin 890 (keep others commented)
    ```
  - for pacer with largest file with 80th percentile latency schedules, set the configuration flags in the script as follows:
    ```
    set "for cfg in $cfg_enf" at line 900
    select medical_xlarge_ceiling.cdataset in cdataset_arr at lin 883 (keep others commented)
    set minimum_experiment_time to 120 at line 898
    set base_rate=3 at line 938
    vary number of concurrent clients as 1, 2, 4, 8, 16, 32, 64, 100, 160 at line 939
    select "schedules_med_80pct" in profiles_dir_arr at lin 890 (keep others commented)
    ```
