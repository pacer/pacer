#!/usr/local/bin/python
import sys,os
import math
import numpy as np
import matplotlib
if os.environ.get('DISPLAY','') == '':
    matplotlib.use('Agg')

from matplotlib import pyplot as plt
from matplotlib import rcParams

import warnings

colors = [ 'red', 'blue', 'green']
markers =  [ 's', 'o', '^' ]

rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Calibri']
#rcParams['font.size'] = 14

annotate_font_size = 11

def main():
    ifile = "plots/xputlat-all.csv"
    ofile = "plots/xputlat-subplots.pdf"
    data = np.loadtxt(ifile, delimiter='\t', comments='#', dtype=str)

    b_trace_load = [ int(y) for y in filter(None, data[0].split(' ')) ]
    b_trace_lat_av = [ float(y) for y in filter(None, data[1].split(' ')) ]
    b_trace_lat_std = [ float(x) for x in filter(None, data[2].split(' ')) ]
    b_trace_xput = [ float(x) for x in filter(None, data[3].split(' ')) ]

    b_trace_static_load = [ int(y) for y in filter(None, data[4].split(' ')) ]
    b_trace_static_lat_av = [ float(y) for y in filter(None, data[5].split(' ')) ]
    b_trace_static_lat_std = [ float(x) for x in filter(None, data[6].split(' ')) ]
    b_trace_static_xput = [ float(x) for x in filter(None, data[7].split(' ')) ]

    p_trace_load = [ int(y) for y in filter(None, data[8].split(' ')) ]
    p_trace_lat_av = [ float(y) for y in filter(None, data[9].split(' ')) ]
    p_trace_lat_std = [ float(x) for x in filter(None, data[10].split(' ')) ]
    p_trace_xput = [ float(x) for x in filter(None, data[11].split(' ')) ]

    p_trace80_load = [ int(y) for y in filter(None, data[12].split(' ')) ]
    p_trace80_lat_av = [ float(y) for y in filter(None, data[13].split(' ')) ]
    p_trace80_lat_std = [ float(x) for x in filter(None, data[14].split(' ')) ]
    p_trace80_xput = [ float(x) for x in filter(None, data[15].split(' ')) ]

    b_xl_load = [ int(y) for y in filter(None, data[16].split(' ')) ]
    b_xl_lat_av = [ float(y) for y in filter(None, data[17].split(' ')) ]
    b_xl_lat_std = [ float(x) for x in filter(None, data[18].split(' ')) ]
    b_xl_xput = [ float(x) for x in filter(None, data[19].split(' ')) ]

    b_xl_static_load = [ int(y) for y in filter(None, data[20].split(' ')) ]
    b_xl_static_lat_av = [ float(y) for y in filter(None, data[21].split(' ')) ]
    b_xl_static_lat_std = [ float(x) for x in filter(None, data[22].split(' ')) ]
    b_xl_static_xput = [ float(x) for x in filter(None, data[23].split(' ')) ]

    p_xl_load = [ int(y) for y in filter(None, data[24].split(' ')) ]
    p_xl_lat_av = [ float(y) for y in filter(None, data[25].split(' ')) ]
    p_xl_lat_std = [ float(x) for x in filter(None, data[26].split(' ')) ]
    p_xl_xput = [ float(x) for x in filter(None, data[27].split(' ')) ]

    p_xl80_load = [ int(y) for y in filter(None, data[28].split(' ')) ]
    p_xl80_lat_av = [ float(y) for y in filter(None, data[29].split(' ')) ]
    p_xl80_lat_std = [ float(x) for x in filter(None, data[30].split(' ')) ]
    p_xl80_xput = [ float(x) for x in filter(None, data[31].split(' ')) ]

    plt_index = 0

    fig = plt.figure(figsize=(7.5, 2.0))
    plt.subplots_adjust(wspace=0.12)

    plt.text(0.13, 0.88, "(a) Trace workload")
    plt.text(0.66, 0.88, "(b) Largest file")
    plt.text(0.3, -0.4, "Throughput (requests/s)")
    plt.text(-0.12, 0.8, "Latency (ms)", rotation="vertical")
    plt.axis('off')
    plt.xticks([])
    plt.yticks([])

    Xticks = range(0, 1600, 200)

    print(b_trace_load)

    plt_index += 1
    ax = fig.add_subplot(1, 2, plt_index)
    ax.errorbar( b_trace_xput, b_trace_lat_av, b_trace_lat_std,
        marker='o', markersize=8, markeredgecolor='none',
        label="${\\rm \\bf Base}$", linestyle='-', linewidth=1.5,
        capsize=4, capthick=1.5,
        color="red" )

    ax.errorbar( b_trace_static_xput, b_trace_static_lat_av, b_trace_static_lat_std,
        marker='o', markersize=5, markeredgecolor='none',
        label="${\\rm \\bf Static}$", linestyle='-', linewidth=1.5,
        capsize=4, capthick=1.3,
        color="orange" )

    ax.errorbar( p_trace_xput, p_trace_lat_av, p_trace_lat_std,
        marker='^', markersize=7, markeredgecolor='none',
        label="${\\rm \\bf Pacer}$", linestyle='--', linewidth=2,
        capsize=4, capthick=1.3,
        color="#0066cc" )

    ax.errorbar( p_trace80_xput, p_trace80_lat_av, p_trace80_lat_std,
        marker='d', markersize=7, markeredgecolor='none',
        label="${\\rm \\bf Pacer}$-${\\rm \\bf lowlat}$", linestyle=':', linewidth=2,
        capsize=4, capthick=1.3,
        color="green" )

    base_trace_annotateA = [ 2.15, 17.0, 73.0, 153.0, 161.0, 170.0 ]
    base_trace_annotateB = [ 25.25, 23.25, 25.25, 5.25, 33.75, 86.25 ]
    idx = 0
    for xy in zip(base_trace_annotateA, base_trace_annotateB):
        val = b_trace_load[idx]
        idx += 1
        ax.annotate('%d' % val, xy=xy, size=annotate_font_size,
            xycoords='axes points', textcoords='axes points')

    p_trace_annotateA = [ 0.15, 20.0, 71.0, 141.0 ]
    p_trace_annotateB = [ 78.0, 78.0, 78.0, 78.0 ]
    idx = 0
    for xy in zip(p_trace_annotateA, p_trace_annotateB):
        val = p_trace_load[idx]
        idx += 1
        ax.annotate('%d' % val, xy=xy, size=annotate_font_size,
            xycoords='axes points', textcoords='axes points')

    #p_trace80_annotateA = [ 0.15, 20.0, 73.0, 125.0 ]
    #p_trace80_annotateB = [ 55.0, 55.0, 55.0, 55.0 ]
    p_trace80_annotateA = [ 125.0 ]
    p_trace80_annotateB = [ 55.0 ]
    idx = 0
    for xy in zip(p_trace80_annotateA, p_trace80_annotateB):
        val = p_trace80_load[-1]
        idx += 1
        ax.annotate('%d' % val, xy=xy, size=annotate_font_size,
            xycoords='axes points', textcoords='axes points')

    ax.set_yscale("log")
    ax.set_ylim(ymin=2, ymax=1000)
    ax.set_xlim(xmin=0, xmax=1400)
    ax.set_xticklabels(Xticks, rotation=45)

    handles, labels = ax.get_legend_handles_labels()
    handles = [ h[0] for h in handles ]
    leg = ax.legend(handles, labels,
            bbox_to_anchor=(2.18,1.3),
            ncol=4, numpoints=1, handlelength=2.0, framealpha=0,
            prop={'size' : 14}
            )
    ax.grid()

    plt_index += 1
    ax2 = fig.add_subplot(1, 2, plt_index)
    ax2.errorbar( b_xl_xput, b_xl_lat_av, b_xl_lat_std,
        marker='o', markersize=8, markeredgecolor='none',
        label="${\\rm Large_{Base}}$", linestyle='-', linewidth=1.5,
        capsize=4, capthick=1.5,
        color="red" )

    ax2.errorbar( b_xl_static_xput, b_xl_static_lat_av, b_xl_static_lat_std,
        marker='o', markersize=5, markeredgecolor='none',
        label="${\\rm Large_{Static}}$", linestyle='-', linewidth=1.5,
        capsize=4, capthick=1.3,
        color="orange" )

    ax2.errorbar( p_xl_xput, p_xl_lat_av, p_xl_lat_std,
        marker='^', markersize=7, markeredgecolor='none',
        label="${\\rm Large_{Pacer}}$", linestyle='--', linewidth=2,
        capsize=4, capthick=1.3,
        color="#0066cc" )

    ax2.errorbar( p_xl80_xput, p_xl80_lat_av, p_xl80_lat_std,
        marker='d', markersize=7, markeredgecolor='none',
        label="${\\rm Large_{Pacer80}}$", linestyle=':', linewidth=2,
        capsize=4, capthick=1.3,
        color="green" )

    base_xl_annotateA = [ 1.15, 4.0, 18.0, 44.0, 94.0, 134.0, 153.0 ]
    base_xl_annotateB = [ 5.25, 27.25, 5.25, 33.25, 35.75, 17.25, 75.5 ]
    print(b_xl_load)
    idx = 0
    for xy in zip(base_xl_annotateA, base_xl_annotateB):
        val = b_xl_load[idx]
        idx += 1
        ax2.annotate('%d' % val, xy=xy, size=annotate_font_size,
            xycoords='axes points', textcoords='axes points')

    p_xl_annotateA = [ 0.15, 2.0, 19.0, 50.0, 100.0 ]
    p_xl_annotateB = [ 54.0, 75.0, 75.0, 78.0, 78.0 ]
    idx = 0
    for xy in zip(p_xl_annotateA, p_xl_annotateB):
        val = p_xl_load[idx]
        idx += 1
        ax2.annotate('%d' % val, xy=xy, size=annotate_font_size,
            xycoords='axes points', textcoords='axes points')

    p_xl80_annotateA = [ 0.15, 2.0, 19.0, 35.0, 65.0 ]
    p_xl80_annotateB = [ 54.0, 75.0, 75.0, 54.0, 54.0 ]
    idx = 0
    for xy in zip(p_xl80_annotateA, p_xl80_annotateB):
        val = p_xl80_load[idx]
        idx += 1
        ax2.annotate('%d' % val, xy=xy, size=annotate_font_size,
            xycoords='axes points', textcoords='axes points')

    ax2.set_yscale("log")
    ax2.set_ylim(ymin=2, ymax=1000)
#    ax2.set_yticks([])
    ax2.set_yticklabels([])
    ax2.set_xlim(xmin=0, xmax=1400)
    ax2.set_xticklabels(Xticks, rotation=45)

#    handles, labels = ax2.get_legend_handles_labels()
#    handles = [ h[0] for h in handles ]
#    leg = ax2.legend(handles, labels,
#            #loc='upper left',
#            bbox_to_anchor=(1.0,1.3),
#            ncol=3, numpoints=1, handlelength=2.0, framealpha=0,
#            prop={'size' : 15}
#            )
    ax2.grid()

    fig.savefig(ofile, bbox_inches='tight', transparent=True)


if __name__ == "__main__":
#    warnings.simplefilter('error', UserWarning)
    main()

