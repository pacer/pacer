-- adapted from https://gist.github.com/Zayat/5cb4bd3f852e3c5f591439fbcee793b5

-- Load URL paths from the file
function load_url_paths_from_file(file)
  lines = {}
  print(file)
  -- Check if the file exists
  -- Resource: http://stackoverflow.com/a/4991602/325852
  local f=io.open(file,"r")
  if f~=nil then
    io.close(f)
  else
    -- Return the empty array
    return lines
  end

  -- If the file exists loop through all its lines
  -- and add them into the lines array
  for line in io.lines(file) do
    if not (line == '') then
      lines[#lines+1] = line
      --if #lines == 400 then
      --  return lines
      --end
    end
  end
  print("File loaded successfully")
  return lines
end



local threads = {}
local thread_counter = 0
local total_clients_count = 1

local paths = nil
local number_of_paths = nil

function setup(thread)   
   thread:set("id", thread_counter)
   table.insert(threads, thread)
   local id = thread:get("id")
   print("thread " .. id .. " created")
   thread_counter = thread_counter + 1
end


function init(args)
  dir_path = args[1]
  print(dir_path .. ".00" .. id .. ".shuff")
  if id < 10 then
    paths = load_url_paths_from_file(dir_path .. ".00" .. id .. ".shuff")
  elseif id < 100 then
    paths = load_url_paths_from_file(dir_path .. ".0" .. id  .. ".shuff")
  else
    paths = load_url_paths_from_file(dir_path .. "." .. id .. ".shuff")
  end
  number_of_paths = #paths
  counter = 1
end


function response(status, headers, body)
  if status > 200 then
    print(paths[counter % number_of_paths ])
    print(status)
  end
end

request = function()
--  url_path = "/mediawiki/index.php?title=" .. paths[(counter % number_of_paths)+1]
  url_path = "/mediawiki/index.php/" .. paths[(counter % number_of_paths)+1]
  counter = counter + 1
  return wrk.format(nil, url_path)
end


--function response(status, headers, body)
--     responses = responses + 1
--end

--function done(summary, latency, requests)
--  for i = 1, #latency do
--    print(latency[i])
--  end
--  -- print(latency[1])
--  -- print(requests[1])
--  -- for index in summary.requests do
--  --   print latency[index]
--  -- end
--end



--function print_paths(dataset, first, last)
--  for i=first,last do
--    print(dataset[i])
--  end
--end

--
--function done(summary, latency, requests)
--   print("Total clients count: " .. total_clients_count);
--   for index, thread in ipairs(threads) do
--      local id = thread:get("id")
--			print("Workload for thread: ".. id )
--      paths = _G.pages
--      total_clients_count = #threads
--      print("Total clients count: " .. total_clients_count);
--      number_of_paths = #paths
--      chard_size = number_of_paths / total_clients_count
--
--  		chard_start =  id * chard_size
--      print(chard_start)
--			chard_end = ((id+1)* chard_size) - 1
--      print(chard_end)
--      print_paths(paths, chard_start, chard_end)
--			--subrange(paths,chard_start,chard_end)
--   end
--end
--

