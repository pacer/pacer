function do_cmd() {
  do_bg=0 ## default value
  ruser=$1
  host=$2
  cmd=$3
  do_bg=$4
  out_null=$5
#  echo "1. ssh $ruser@$host \"$cmd\" $do_bg $out_null"
  if [[ $dryrun -eq 0 ]]; then
    if [[ $do_bg -eq 1 ]]; then
      if [[ $out_null -eq 1 ]]; then
        #echo "ssh -f $ruser@$host \"$cmd &\" > /dev/null 2>&1"
        ssh -f $ruser@$host "$cmd &" > /dev/null 2>&1
      else
        #echo "ssh -f $ruser@$host \"$cmd\""
        ssh -f $ruser@$host "$cmd" #> /dev/null 2>&1
      fi
    else
      if [[ $out_null -eq 1 ]]; then
        ssh $ruser@$host "$cmd" > /dev/null 2>&1
      else
        ssh $ruser@$host "$cmd"
      fi
    fi
  fi
  echo "ssh $ruser@$host \"$cmd\" $do_bg $out_null"
}

function run_once_setup() {
  ruser=$1
  host=$2
  kdir=$3
  cmd=""
  cmd="echo  > /var/log/syslog; "
  cmd+="echo > /var/log/kern.log; "
  cmd=$cmd"cd $kdir; ./irqsetup.sh 1; sleep 1; ./irqsetup.sh 1;"
  do_cmd $ruser $host "$cmd" 0 1
  sleep 1
}

function run_host_setup() {
  ruser=$1
  host=$2
  kdir=${12}
  cmd=""
  if [[ $cfg -ge $cfg_mod_irq ]]; then
    if [[ "x$6" != "x" ]]; then
      cmd=$cmd"cd $kdir; ./ethdev_enable.sh; "
      cmd=$cmd"insmod sme_lsm.ko my_ip='$3' my_port=$4 host_type='$5' bknd_ip='$6' bknd_port=$7"
      cmd=$cmd" epoch=$8 spin_thresh=$9 max_pkts_per_epoch=${10}"
      cmd=$cmd" max_other_per_epoch=${11}"
    else
      cmd=$cmd"cd $kdir; ./ethdev_enable.sh; "
      cmd=$cmd"insmod sme_lsm.ko my_ip='$3' my_port=$4 host_type='$5'"
    fi
  else
    cmd=$cmd"cd $kdir; ./ethdev_enable.sh; "
  fi
  do_cmd $ruser $host "$cmd" 0 1
}

function run_host_shutdown() {
  ruser=$1
  host=$2
  kdir=$3
  local=$(hostname -I | cut -d' ' -f1)
  edev=$(ssh $ruser@$host "ifconfig | grep -B1 \"$host\" | head -1 | cut -d' ' -f1")
  if [[ $cfg -ge $cfg_mod_irq ]]; then
    cmd="cd $kdir; rmmod sme_lsm; ./ethdev_disable.sh; "
    if [[ $host != $local ]]; then
      cmd=$cmd"sleep 10; ifdown $edev && ifup $edev; "
    fi
  else
    cmd="cd $kdir; ./ethdev_disable.sh; "
  fi
  do_cmd $ruser $host "$cmd" 0 1
}

function run_dom0_setup()
{
  ruser=$1
  host=$2
  cmd="xenpm set-max-cstate 0"
  do_cmd $ruser $host "$cmd" 0 1
}

function run_ktracer_setup() {
  ruser=$1
  host=$2
  tracer=$3
  if [[ $do_ktrace -eq 1 ]]; then
    cmd="echo 0 > /sys/kernel/debug/tracing/tracing_on; "
    cmd=$cmd"echo $tracer > /sys/kernel/debug/tracing/current_tracer; "
    cmd=$cmd"echo bnx2x_msix_fp_int > /sys/kernel/debug/tracing/set_ftrace_filter; "
    cmd=$cmd"echo bnx2x_poll >> /sys/kernel/debug/tracing/set_ftrace_filter; "
    #cmd=$cmd"echo __bnx2x_rx_int >> /sys/kernel/debug/tracing/set_ftrace_filter; "
    #cmd=$cmd"echo bnx2x_start_xmit >> /sys/kernel/debug/tracing/set_ftrace_filter; "
    do_cmd $ruser $host "$cmd"
  fi
}

function start_ktracer() {
  ruser=$1
  host=$2
  if [[ $do_ktrace -eq 1 ]]; then
    cmd="echo 1 > /sys/kernel/debug/tracing/tracing_on; "
    do_cmd $ruser $host "$cmd"
  fi
}

function stop_ktracer() {
  ruser=$1
  host=$2
  if [[ $do_ktrace -eq 1 ]]; then
    cmd="echo 0 > /sys/kernel/debug/tracing/tracing_on; "
    do_cmd $ruser $host "$cmd"
  fi
}

function get_ktracer_output() {
  ruser=$1
  host=$2
  outfile=$3
  if [[ $do_ktrace -eq 1 ]]; then
    cmd="cat /sys/kernel/debug/tracing/trace > \"$outfile\""
#    cmd=$cmd"/local/sme/syslogs/ktrace/bnx2x.ktrace.$now"
    do_cmd $ruser $host "$cmd"
  fi
}

function get_intr_count() {
  ruser=$1
  host=$2
  outf=$3
  cmd="cat /proc/interrupts | grep \"CPU\\|eno\" > \"$outf\""
  do_cmd $ruser $host "$cmd"
}

function get_softirq_count() {
  ruser=$1
  host=$2
  outf=$3
  cmd="cat /proc/softirqs | grep \"CPU\\|NET_RX\" > \"$outf\""
  do_cmd $ruser $host "$cmd"
}

function get_softirq_count() {
  ruser=$1
  host=$2
  outf=$3
  cmd="cat /proc/softirqs | grep \"CPU\\|NET_RX\" > \"$outf\""
  do_cmd $ruser $host "$cmd"
}

function get_remote_stats() {
  ruser=$1
  host=$2
  outdir=$3
  scp $ruser@$host:"/local/sme/syslogs/ktrace/intr.cnt.start" $outdir/
  scp $ruser@$host:"/local/sme/syslogs/ktrace/intr.cnt.stop" $outdir/
  scp $ruser@$host:"/local/sme/syslogs/ktrace/sirq.cnt.start" $outdir/
  scp $ruser@$host:"/local/sme/syslogs/ktrace/sirq.cnt.stop" $outdir/
}

function run_xenpm_setup() {
  ruser=$1
  host=$2
  outdir=$3
  if [[ $do_ktrace -eq 1 ]]; then
    cmd="xenpm start 600 > $outdir/xpm.stat"
    do_cmd $ruser $host "$cmd" 1
  fi
}

function run_xenpm_shutdown() {
  ruser=$1
  host=$2
  if [[ $do_ktrace -eq 1 ]]; then
    cmd="pkill -2 xenpm"
    do_cmd $ruser $host "$cmd"
  fi
}
