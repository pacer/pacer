#!/bin/bash

#########################################################
# Script for running mediawiki workload (medical dataset)
#########################################################

############
## constants
############
cfg_base=0
cfg_mod_irq=1
cfg_enf=2

set_setup=1
set_shutdown=0

user="sme"
ruser="root"
rootdir="/local/sme"
workdir="$rootdir/workspace/pacer"
appdir="$workdir/apps/mediawiki-1.27.1"
clientdir="$workdir/apps/wrk2"
wrk2bin="$clientdir/wrk"

fe_gpacedir="$workdir/frontend/gpace/linux-frontend-4.9.5"
be_gpacedir="$workdir/backend/gpace/linux-backend-4.9.5"
cl_gpacedir="$workdir/client/gpace/linux-client-4.9.5"

fe_hypacedir="$workdir/frontend/hypace/xen-frontend-4.10.0"
be_hypacedir="$workdir/backend/hypace/xen-backend-4.10.0"

fe_gpacekmod="$workdir/frontend/gpace/kmod-frontend"
be_gpacekmod="$workdir/backend/gpace/kmod-backend"
cl_gpacekmod="$workdir/client/gpace/kmod-client"

fe_gpacescripts="$workdir/frontend/gpace/scripts"
be_gpacescripts="$workdir/backend/gpace/scripts"
cl_gpacescripts="$workdir/client/gpace/scripts"

fe_profpacedir="$workdir/frontend/profpace"
be_profpacedir="$workdir/backend/profpace"
cl_profpacedir="$workdir/client/profpace"

fe_profpacelib="$workdir/frontend/profpace/proflib"
be_profpacelib="$workdir/backend/profpace/proflib"
cl_profpacelib="$workdir/client/profpace/proflib"

datadir="$workdir/eval/medical/experiment/data"
webappdir="/var/www/html/mediawiki"
apachedir="/usr/local/apache2"

daemondir="$rootdir/daemon_profiler"

############
## variables
############

#apphost_ip="139.19.218.12"
#dbhost_ip="139.19.218.11"

#appvm_ip="139.19.218.13"

appvm_ip="139.19.218.101"
dbvm_ip="139.19.218.103"
dummy_ip="139.19.218.173"
client_ip=$(hostname -I | cut -d' ' -f1)
#db_app_port=3306
db_app_port=11211 ## memcached
dummy_port=2231

#maxwell_ip="139.19.218.10"
maxwell_ip="139.19.218.13"

wrk2flags="--latency --u_latency "

do_stat=0
do_mw_stat=0
do_ktrace=0
do_kmemcheck=0
do_tcpdump_remote=0
do_tcpdump_local=0

. "cmd.sh"

function get_gitinfo()
{
  host=$1
  dir=$2
  cmd="cd $dir; git show --name-status > $dir/gitinfo; "
  cmd=$cmd"echo "" >> $dir/gitinfo; "
  cmd=$cmd"git diff >> $dir/gitinfo; "
  do_cmd $ruser $host "$cmd"
}

function copy_stats()
{
  if [[ $1 == "FE" ]]; then
    if [[ $cfg -gt $cfg_mod_irq ]]; then
      scp $ruser@$appvm_ip:/var/log/syslog "$outdir/$sysfile.FE-gpace"

      dcmd="xl dmesg -c > /local/sme/xenstat; "
      do_cmd $ruser $maxwell_ip "$dcmd" 0 0
      scp $ruser@$maxwell_ip:/local/sme/xenstat "$outdir/$xenstat.FE-xen"
    fi

#    if [[ $do_stat -eq 1 ]]; then
      #scp $ruser@$appvm_ip:/local/sme/exp/vmstat.out "$outdir/$vmstatfile.FE-gpace"
#      scp $ruser@$appvm_ip:/local/sme/exp/pidstat.out "$outdir/$pidstatfile.FE-gpace"
#      scp $ruser@$appvm_ip:/local/sme/exp/mpstat.out "$outdir/$mpstatfile.FE-gpace"
#    fi
  elif [[ $1 == "MW" ]]; then
    webserver_copy_logs $ruser $appvm_ip
    if [[ $do_mw_stat -eq 1 ]]; then
      scp $ruser@$appvm_ip:/local/sme/stats_5_tuple.txt $mw5tuplestats
      scp $ruser@$appvm_ip:/local/sme/stats_marker_sync.txt $mwmarkersyncstats
    fi

  elif [[ $1 == "CLIENT" ]]; then
    if [[ $cfg_enf_on_client -eq 1 ]]; then
      cp /var/log/syslog "$outdir/$sysfile.CL-gpace"
    fi
    if [[ $do_stat -eq 1 ]]; then
      scp $ruser@$client_ip:/local/sme/exp/mpstat.out "$outdir/$mpstatfile.CL-gpace"
      scp $ruser@$client_ip:/local/sme/exp/pidstat.out "$outdir/$pidstatfile.CL-gpace"
    fi
  fi

}
trap copy_stats EXIT

function start_vmstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="vmstat 1 > /local/sme/exp/vmstat.out"
    do_cmd $ruser $host "$cmd" 1 1
  fi
}

function stop_vmstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="pkill -9 vmstat"
    do_cmd $ruser $host "$cmd"
  fi
}

function start_pidstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    if [[ "$host" == "$client_ip" ]]; then
      cmd="pidstat -C \"wrk\""
    else
      cmd="pidstat -C \"apache2|httpd|sme_logger|sme_profiler\""
    fi
    cmd=$cmd" -u -w -r -h 1 > /local/sme/exp/pidstat.out"
    do_cmd $ruser $host "$cmd" 1 1
  fi
}

function stop_pidstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="pkill -9 pidstat"
    do_cmd $ruser $host "$cmd"
  fi
}

function start_mpstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="mpstat -u -P ALL 1 > /local/sme/exp/mpstat.out"
    do_cmd $ruser $host "$cmd" 1 1
  fi
}

function stop_mpstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="pkill -9 mpstat"
    do_cmd $ruser $host "$cmd"
  fi
}

function start_kedr()
{
  if [[ $do_kmemcheck -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="kedr start sme_lsm"
    do_cmd $ruser $host "$cmd"
  fi
}

function stop_kedr()
{
  if [[ $do_kmemcheck -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="kedr stop"
    do_cmd $ruser $host "$cmd"
  fi
}

function get_kedr_info()
{
  if [[ $do_kmemcheck -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="cat /sys/kernel/debug/kedr_leak_check/info"
    cmd=$cmd" > /local/sme/exp/kedr.out;"
    cmd=$cmd" cat /sys/kernel/debug/kedr_leak_check/possible_leaks"
    cmd=$cmd" > /local/sme/exp/kedr_leaks.out;"
    do_cmd $ruser $host "$cmd"
  fi
}

function create_vm()
{
  echo "ssh $ruser@$1 \"xl list $2 | tail -n1 | cut -d' ' -f1\""
  vm=`ssh $ruser@$1 "xl list $2 | tail -n1 | cut -d' ' -f1"`
  if [[ "x$vm" != "x" ]]; then
    echo -e "VM $2 already running\n"
  else
    echo "Init VM $2"
    cmd="xl create /etc/xen/$2.cfg"
    do_cmd $ruser $1 "$cmd"
  fi
}

function run_appvm_setup2()
{
  if [[ $cfg -eq $cfg_enf ]]; then
    cmd="cd $fe_profpacedir/proflib; ./sme_mgmt_process"
    cmd=$cmd" --dummy_ip \"$dummy_ip\" --dummy_port $dummy_port"
    cmd=$cmd" --ep_ip1 \"0.0.0.0\" --ep_port1 0 --ep_ip2 \"$2\" --ep_port2 $3"
    cmd=$cmd" --ep_ip3 \"$2\" --ep_port3 0 --ep_ip4 \"$4\" --ep_port4 $5"
    cmd=$cmd" --rw_prof 0 --n_prof 1 --prof_file \"hello.txt\" --ntype $6"
    do_cmd $ruser $1 "$cmd"
  fi
}

function trunc_logs()
{
  ruser=$1
  host=$2
  cmd=""
  cmd=$cmd"echo \"\" > /var/log/apache2/responsetime.log; "
  cmd=$cmd"echo \"\" > $apachedir/logs/responsetime.log; "
  cmd=$cmd"echo \"\" > /var/log/apache2/access.log; "
  cmd=$cmd"echo \"\" > /local/sme/msg_sent_to_kernel.txt; "
  cmd=$cmd"echo \"\" > /local/sme/stats_5_tuple.txt; "
  cmd=$cmd"echo \"\" > /local/sme/stats_marker_sync.txt; "

  do_cmd $ruser $host "$cmd" 0 0
}

function apache_copy_configs()
{
  ruser=$1
  rhost=$2

  cmd="scp $ruser@$rhost:\"$apachedir/conf/httpd.conf\" $outdir/"
  echo "$cmd"
  eval "$cmd"

  cmd="scp $ruser@$rhost:\"$apachedir/conf/extra/httpd-mpm.conf\" $outdir/"
  echo "$cmd"
  eval "$cmd"
}

function apache_copy_logs()
{
  ruser=$1
  rhost=$2

  cmd=""
  cmd=$cmd"scp $ruser@$rhost:/var/log/apache2/access.log $webservlog.FE-gpace; "
  echo "$cmd"
  eval "$cmd"
}

function apache_rmpid()
{
  cmd=""
  cmd=$cmd"rm -f $apachedir/logs/httpd.pid; sleep 1; "
  do_cmd $ruser $rhost "$cmd" 0 0
}

function apache_start()
{
  cmd=""
  cmd=$cmd"sleep 1; $apachedir/bin/apachectl -k start; "
  do_cmd $ruser $rhost "$cmd" 0 0
}

function apache_stop()
{
  cmd=""
  cmd=$cmd"sleep 1; $apachedir/bin/apachectl -k stop; sleep 2; "
  do_cmd $ruser $rhost "$cmd" 0 0
}

function webserver_copy_configs()
{
  ruser=$1
  rhost=$2

  apache_copy_configs $ruser $rhost
}

function webserver_copy_logs()
{
  ruser=$1
  rhost=$2

  apache_copy_logs $ruser $rhost
}

function webserver_rmpid()
{
  apache_rmpid
}

function webserver_start()
{
  apache_start
}

function webserver_stop()
{
  apache_stop
}

function webserver_copy_ssl_mod()
{
  cmd=""
  if [[ $cfg -eq $cfg_enf ]]; then
    cmd=$cmd"cp $apachedir/modules/mod_ssl_enf.so $apachedir/modules/mod_ssl.so; "
  else
    cmd=$cmd"cp $apachedir/modules/mod_ssl_base.so $apachedir/modules/mod_ssl.so; "
  fi
  do_cmd $ruser $host "$cmd" 0 0
}

function webserver_restart()
{
  webserver_rmpid
  webserver_stop

  webserver_copy_ssl_mod

  cmd=""

  if [[ $cfg -eq $cfg_enf ]]; then
    cmd=$cmd"cp $webappdir/LocalSettings.php.enf $webappdir/LocalSettings.php; "
  else
    cmd=$cmd"cp $webappdir/LocalSettings.php.no_enf $webappdir/LocalSettings.php; "
  fi

  do_cmd $ruser $host "$cmd" 0 0

  webserver_start
}

function setup_mysql()
{
  ruser=$1
  host=$2
  cmd=""
  cmd=$cmd"ps aux | grep mysqld | wc -l"
  x=$(do_cmd $ruser $host "$cmd" 0 0)
  read x <<< "$x"
  echo "MYSQL running : $x"
  if [[ $x -lt 3 ]]; then
    cmd="/etc/init.d/mysql start"
    do_cmd $ruser $host "$cmd" 0 1
  fi
}

function run_profile_logger()
{
  # runtype:
  # 0 - use_log_dir and set profile
  # 1 - run as a daemon (make sure to invoke stop_profile_logger)
  # 2 - run for n requests (set prof_for_n_req flag)
  # 3 - send_ascii_profiles_dir and set profile

  ruser=$1
  host=$2
  runtype=$7
  totalreq=$8
  scheddir=$9
  profdir=${10}

  echo "Run profile logger $cfg runtype $runtype"

  if [[ $cfg -ne $cfg_enf ]]; then
    return
  fi

  cmd="cd $profdir; python sme_profiler.py"
  cmd=$cmd" --dummy_ip $dummy_ip --dummy_port $dummy_port"
  cmd=$cmd" --ep_ip1 \"0.0.0.0\" --ep_port1 0 --ep_ip2 \"$3\" --ep_port2 $4"
  cmd=$cmd" --ep_ip3 \"$3\" --ep_port3 0 --ep_ip4 \"$5\" --ep_port4 $6"
  if [[ $runtype -eq 0 ]]; then
    cmd=$cmd" --update_prof_freq 1"
    cmd=$cmd" --gen_prof_freq $totalreq"
    cmd=$cmd" --prof_for_n_req $totalreq"
    cmd=$cmd" --use_logs_dir \"$daemondir/kernel_logs\""
    cmd=$cmd" --keep_logs 0"
    do_cmd $ruser $host "$cmd" 0
  elif [[ $runtype -eq 1 ]]; then
    cmd=$cmd" --update_prof_freq 0"
    do_cmd $ruser $host "$cmd" 1
  elif [[ $runtype -eq 2 ]]; then
    cmd=$cmd" --update_prof_freq 0"
    #cmd=$cmd" --only_dump_no_profiling 0"
    #cmd=$cmd" --pace_log 1"
    #cmd=$cmd" --prof_for_n_req $totalreq"
    #cmd=$cmd" --gen_prof_freq $totalreq"
    #cmd=$cmd" --send_ascii_profiles_dir \"$daemondir/enf_profile\""
    echo "ssh $ruser@$host \"$cmd\""
    ssh $ruser@$host "$cmd" &
    echo "REMOTE LOGGER PID: $!"
  elif [[ $runtype -eq 3 ]]; then
    cmd=$cmd" --update_prof_freq 1"
    #cmd=$cmd" --prof_for_n_req $totalreq"
    #cmd=$cmd" --gen_prof_freq $totalreq"
    cmd=$cmd" --send_ascii_profiles_dir \"$daemondir/$scheddir\""
    do_cmd $ruser $host "$cmd" 0
  fi

}

function stop_profile_logger()
{
  ruser=$1
  host=$2
  if [[ $cfg -eq $cfg_enf ]]; then
    cmd="pkill -15 python"
    do_cmd $ruser $host "$cmd"
  fi
}

function run_tcpdump()
{
  ruser=$1
  host=$2
  appvm_ip=$3
  client_ip=$4
  edev=$5
  profdir=$8
  cmd=""
  cmd=$cmd"mkdir -p $rootdir/tdump; "
  cmd=$cmd"python $profdir/network_profiler.py"
  cmd=$cmd" --server-ip $appvm_ip"
  cmd=$cmd" --client-ip $client_ip"
  cmd=$cmd" --iface $edev"
  cmd=$cmd" --transport $6"
  cmd=$cmd" --server-port $7"
  cmd=$cmd" --dumpfile $rootdir/tdump/$cfgstr.$appvm_ip-$client_ip.tdump"
  cmd=$cmd" --outfile $rootdir/tdump/$cfgstr.$appvm_ip-$client_ip"
#  cmd=$cmd" --multi-tier False"
  cmd=$cmd" --action 0; "
  tcpdump_pid=$(ssh -f $ruser@$host "$cmd")
  echo "$tcpdump_pid"
  echo "ssh $ruser@$host \"$cmd\""
}

function stop_tcpdump()
{
  ruser=$1
  host=$2
  appvm_ip=$3
  client_ip=$4
  tcpdump_pid=$5
  profdir=$8
  echo $tcpdump_pid
  cmd=""
  cmd=$cmd"python $profdir/network_profiler.py"
  cmd=$cmd" --server-ip $appvm_ip"
  cmd=$cmd" --client-ip $client_ip"
  cmd=$cmd" --transport $6"
  cmd=$cmd" --server-port $7"
  cmd=$cmd" --dumpfile $rootdir/tdump/$cfgstr.$appvm_ip-$client_ip.tdump"
  cmd=$cmd" --outfile $rootdir/tdump/$cfgstr.$appvm_ip-$client_ip"
  cmd=$cmd" --action $tcpdump_pid"
  do_cmd $ruser $host "$cmd" 0 1
}

function copy_profiles()
{
  ruser=$1
  rhost=$2
  cmd="scp -rq $ruser@$rhost:\"$daemondir/$profiles_to_enf_dir\" $outdir/"
  echo "$cmd"
  eval "$cmd"
}

function copy_other_configs()
{
  ruser=$1
  rhost=$2

  webserver_copy_configs $ruser $rhost

  cmd="scp $ruser@$rhost:\"/etc/php/7.0/apache2/php.ini\" $outdir/php.ini.apache"
  echo "$cmd"
  eval "$cmd"

  cmd="scp $ruser@$rhost:\"/etc/sysctl.conf\" $outdir/sysctl.conf.server"
  echo "$cmd"
  eval "$cmd"

  cmd="cp /etc/sysctl.conf $outdir/sysctl.conf.client"
  echo "$cmd"
  eval "$cmd"

  cmd="sysctl -p"
  do_cmd $ruser $rhost "$cmd"
  do_cmd $ruser $client_ip "$cmd"
}

function run_dbg_logger()
{
  ruser=$1
  host=$2
  proflibdir=$3

  cmd="$proflibdir/pacer_dbg_logger"
  do_cmd $ruser $host "$cmd" 0
}

function do_setup()
{
  get_gitinfo $appvm_ip $workdir
  scp $ruser@$appvm_ip:$workdir/gitinfo "$outdir/$gitinfo.FE-gpace"

  get_gitinfo $appvm_ip $appdir
  scp $ruser@$appvm_ip:$appdir/gitinfo "$outdir/$gitinfo.FE-mw"

  get_gitinfo $appvm_ip $fe_gpacedir
  scp $ruser@$appvm_ip:$fe_gpacedir/gitinfo "$outdir/$gitinfo.FE-linux"

  get_gitinfo $maxwell_ip $workdir
  scp $ruser@$maxwell_ip:$workdir/gitinfo "$outdir/$gitinfo.FE-hypace"

  get_gitinfo $maxwell_ip $fe_hypacedir
  scp $ruser@$maxwell_ip:$fe_hypacedir/gitinfo "$outdir/$gitinfo.FE-xen"

  get_gitinfo $client_ip $workdir
  cp $workdir/gitinfo "$outdir/$gitinfo.CL-gpace"

  get_gitinfo $client_ip $cl_gpacedir
  cp $cl_gpacedir/gitinfo "$outdir/$gitinfo.CL-linux"

  get_gitinfo $client_ip $clientdir
  cp $clientdir/gitinfo "$outdir/$gitinfo.CL-wrk2"


  copy_profiles $ruser $appvm_ip
  copy_other_configs $ruser $appvm_ip

  do_cmd $ruser $maxwell_ip "xl vcpu-list; "

  do_cmd $ruser $client_ip "pkill -9 tcpdump; pkill -9 tshark; pkill -9 network_profiler.py"

  ## ==== apache setup ====
  trunc_logs $ruser $appvm_ip
#  webserver_restart $ruser $appvm_ip

  ## ==== mysql setup ====
  setup_mysql $ruser $appvm_ip

  sleep 2

  if [[ $warmup -eq 1 ]]; then
    tmp_wrk2_time=$minimum_experiment_time
    tmp_num_requests=$nreq
    ## for wrk2
    minimum_experiment_time=10
    ## for ab
    nreq=1000
    echo "==== WARMUP ===="
    run_client_driver
    ## for wrk2
    minimum_experiment_time=$tmp_wrk2_time
    ## for ab
    nreq=$tmp_num_requests
    warmup=0
    sleep 5
  fi

  run_once_setup $ruser $appvm_ip $fe_gpacescripts

  ## ==== perf and mem monitoring ====
  start_kedr $ruser $appvm_ip

#  start_vmstat $ruser $appvm_ip
#  start_pidstat $ruser $appvm_ip
#  start_mpstat $ruser $appvm_ip
  start_mpstat $ruser $client_ip
  start_pidstat $ruser $client_ip
#  start_mpstat $ruser $appvm_ip

  ## ==== disable C-states on server ====
  run_dom0_setup $ruser "$maxwell_ip"

  ## ==== insmod ====

  if [[ $cfg -ne $cfg_base ]]; then
    run_host_setup $ruser $appvm_ip $appvm_ip $app_client_port "FE" \
      $dbvm_ip $db_app_port \
      $serv_epoch $serv_spin_thresh $serv_max_pkts_per_epoch  \
      $serv_max_other_per_epoch $fe_gpacekmod

    if [[ $cfg_enf_on_client -eq 1 ]]; then
      run_once_setup $ruser $client_ip $cl_gpacescripts
      run_host_setup $ruser $client_ip $client_ip $app_client_port "CLIENT" \
        $client_ip $app_client_port \
        $client_epoch $client_spin_thresh $client_max_pkts_per_epoch  \
        $client_max_other_per_epoch $cl_gpacekmod
    fi
  fi

  sleep 2

  ## ==== run profiler to set custom profile in kernels ====
  if [[ $send_ascii_profile_for_enf -eq 1 ]]; then
    run_profile_logger $ruser $appvm_ip $appvm_ip $app_client_port  \
      $dbvm_ip $db_app_port 3 $totalreq $profiles_to_enf_dir $fe_profpacedir
  fi


  ## ==== generate profile from kernel logs and set profile in kernels ====
  #run_profile_logger $ruser $appvm_ip $appvm_ip $app_client_port  \
  #  $dbvm_ip $db_app_port 0 $totalreq $profiles_to_enf_dir $fe_profpacedir

  ## ==== without custom prof enabled, run profiler for n reqs ====
  if [[ $run_profiler_during_exp -eq 1 ]]; then
    echo "Running profiler"
    run_profile_logger $ruser $appvm_ip $appvm_ip $app_client_port \
      $dbvm_ip $db_app_port 2 $totalreq $profiles_to_enf_dir $fe_profpacedir
  fi

  ## ==== with custom prof enabled, run profiler in daemon mode ====
#  run_profile_logger $ruser $appvm_ip $appvm_ip $app_client_port  \
#    $dbvm_ip $db_app_port 1 $totalreq $profiles_to_enf_dir $fe_profpacedir

}

function do_shutdown()
{
  ## ==== stop exp ====

  ## ==== stop kernel tracers ====
#  stop_ktracer $ruser $appvm_ip
#  sleep 5

  ## ==== stop profiler daemon in FE dom0 userspace ====
# stop_profile_logger $ruser $appvm_ip

#  webserver_stop

  sleep 2

  ## ==== remove kernel modules on FE and BE ====
  if [[ $cfg -ne $cfg_base ]]; then
    run_host_shutdown $ruser $appvm_ip $fe_gpacescripts

    if [[ $cfg_enf_on_client -eq 1 ]]; then
      run_host_shutdown $ruser "$client_ip" $cl_gpacescripts
      cp /var/log/kern.log $sysfile.client
    fi
  fi

  ## ==== get kmem leak info ====
  get_kedr_info $ruser $appvm_ip
  stop_kedr $ruser $appvm_ip

  ## ==== stop stat tools ====
#  stop_vmstat $ruser $appvm_ip
#  stop_pidstat $ruser $appvm_ip
#  stop_mpstat $ruser $appvm_ip
  stop_mpstat $ruser $client_ip
  stop_pidstat $ruser $client_ip
#  stop_mpstat $ruser $appvm_ip

}

function run_client_driver()
{
  if [[ $mw_trace -eq 1 ]]; then
    wrk2cmd="$wrk2bin -t$nthreads -c$(( $threads_per_client*$nthreads )) \
      -d${minimum_experiment_time}s -R${wrk2_load_rate} \
      -H 'accept-encoding: gzip, deflate, br' \
      -s many_url_paths.lua $wrk2flags \
      $proto://$appvm_ip:$app_client_port -- $trace_dataset \
      > $abfile.$warmup 2>&1 &"

    echo "$wrk2cmd"
    eval "$wrk2cmd"

  else
    if [[ $trace_name != "index" ]]; then
      request_name=`head -1 $trace_dataset`
      url="$proto://$appvm_ip:$app_client_port/mediawiki/index.php/$request_name"
    fi

    for p in $(seq -s, 0 $(( $nproc - 1 ))); do
      wrk2cmd="$wrk2bin -t$nthreads -c$(( $threads_per_client*$nthreads ))  \
        -d${minimum_experiment_time}s -H 'accept-encoding: gzip, deflate, br' \
        -R${wrk2_load_rate} $wrk2flags \"$url\" > $abfile-p$p.$warmup 2>&1 &"

      echo "$wrk2cmd"
      eval "$wrk2cmd"
      echo "WRK2 CLIENT $p PID: $!"

    done

  fi # $mw_trace

  ## ==== wait for client to finish ====
  FAIL=0
  joblist=`jobs -p`
  while read -r job; do
    echo "Waiting for $job to complete.."
    wait $job || let "FAIL+=1"
  done <<< "$joblist";

  if [[ "$FAIL" == "0" ]]; then
    echo "Success!"
  else
    echo "Fail ($FAIL)"
  fi;
}

function run_bench()
{
  nthreads=$1
  nreq=$2

  suffix="d$minimum_experiment_time""-c$num_clients"".$cfgstr.$iter"

  abfile="$outdir/ab.$suffix"
  outfile="$outdir/out.$suffix"

  gitinfo="gitinfo.$suffix"
  sysfile="syslog.$suffix"
  xenstat="xenstat.$suffix"
  vmstatfile="vmstat.$suffix"
  pidstatfile="pidstat.$suffix"
  mpstatfile="mpstat.$suffix"
  tsharkfile="$outdir/tshark.$suffix"

  webservlog="$outdir/apache.$suffix"
  mwmarkersyncstats="$outdir/apacheMWmarkerstats.$suffix"
  mw5tuplestats="$outdir/apacheMW5tuplestats.$suffix"

  echostr="[$(date)] engine: $webserver cfg: $cfgstr trace: $trace_name"
  echostr=$echostr" wrk2 dur: $minimum_experiment_time rate/proc: $wrk2_load_rate"
  if [[ $cfg -eq $cfg_enf ]]; then
    echostr=$echostr" epoch: $serv_epoch spinthresh: $serv_spin_thresh"
    echostr=$echostr" pkts/epoch: $serv_max_pkts_per_epoch"
    echostr=$echostr" other/epoch: $serv_max_other_per_epoch"
  fi
  echostr=$echostr" clients/proc: $num_clients #procs: $nproc iter: $iter"
  echo $echostr

  (

    echo $echostr
    do_setup

    ## ==== start tcpdump ====
    if [[ $do_tcpdump_remote -eq 1 ]]; then
      echo "Start tcpdump"
      edev=$(ssh $appvm_ip "ifconfig | grep -B1 \"$appvm_ip\" | head -1 | cut -d' ' -f1")
      tcpdump_pid=$(run_tcpdump $ruser "$appvm_ip" "$appvm_ip" "$client_ip" \
        "$edev" "tcp" $app_client_port $fe_profpacedir)
      echo "REMOTE TCPDUMP PID: $tcpdump_pid"
      ## this is needed, because run_tcpdump echoes the PID followed by
      ## the tcpdump command string. we do not want to accidentally pass
      ## the whole string returned from the function as an argument to
      ## stop_tcpdump later.
      read tcpdump_pid <<< "$tcpdump_pid"
#      echo "CORRECT TCPDUMP PID: $tcpdump_pid"
    fi

    if [[ $do_tcpdump_local -eq 1 ]]; then
      edev=`ifconfig | grep -B1 $client_ip | head -1 | cut -d' ' -f1`
      local_tcpdump_pid=$(run_tcpdump $ruser "$client_ip" "$appvm_ip" "$client_ip"  \
        "$edev" "tcp" $app_client_port $cl_profpacelib)
      echo "LOCAL TCPDUMP PID: $local_tcpdump_pid"
      read local_tcpdump_pid <<< "$local_tcpdump_pid"

      sleep 5

    fi

    ## ==== client driver ====

    date;
    run_client_driver
    date;

    sleep 60

#    date;
#    run_dbg_logger $ruser $appvm_ip $fe_profpacelib
#
#    date;
#    run_dbg_logger $ruser $client_ip $cl_profpacelib

    date;

#    sleep 2
    do_shutdown

    ## ==== copy files ====
    copy_stats "FE"
    copy_stats "MW"
    copy_stats "CLIENT"

    ## ==== stop tcpdump ====
    if [[ $do_tcpdump_remote -eq 1 ]]; then
      echo "Stop tcpdump"
      stop_tcpdump $ruser "$appvm_ip" "$appvm_ip" "$client_ip" $tcpdump_pid \
        "tcp" $app_client_port $fe_profpacedir

      scp $ruser@$appvm_ip:"$rootdir/tdump/$cfgstr.$appvm_ip-$client_ip.tshark" \
        $tsharkfile
    fi

    if [[ $do_tcpdump_local -eq 1 ]]; then
      echo "Stop local tcpdump"
      stop_tcpdump $ruser "$client_ip" "$appvm_ip" "$client_ip" $local_tcpdump_pid  \
        "tcp" $app_client_port $cl_profpacedir

      cp "$rootdir/tdump/$cfgstr.$appvm_ip-$client_ip.tshark" "$tsharkfile.client"
    fi
  ) > $outfile

}


threads_per_client=1

driver="wrk2"
app_client_port=443
run_profiler_during_exp=0
prof_scale_w_load=0
client_epoch=0
client_spin_thresh=0
client_max_pkts_per_epoch=0
client_max_other_per_epoch=0

totalreq=0
nproc=1
webserver="apache"

warmup=1

if [[ $app_client_port -eq 80 ]]; then
  proto="http"
elif [[ $app_client_port -eq 443 ]]; then
  proto="https"
fi

declare -a cdataset_arr=( \
#  "medical_xlarge_ceiling.cdataset"  \
  "workload_trace_M65_L30_XL5" \
  )

declare -a profiles_dir_arr=( \
#  "schedules_med_100pct"  \
  "schedules_med_80pct"  \
  )

spin_epoch_set=(  \
    35000,120000,38,0 \
  )

for minimum_experiment_time in 120
do
for cfg in $cfg_base;
do
if [[ $cfg -eq $cfg_base ]]; then
  cfgstr="base"
  rcfgid=0
  cfg_enf_on_client=0
  send_ascii_profile_for_enf=0
else
  cfgstr="sme"
  rcfgid=1
  cfg_enf_on_client=1
  send_ascii_profile_for_enf=1
fi

nreq=10000
for trace_name in "${cdataset_arr[@]}";
do
  for profiles_to_enf_dir in "${profiles_dir_arr[@]}";
  do
    for spin_epoch in "${spin_epoch_set[@]}";
    do
      IFS=","
      set -- $spin_epoch
      serv_spin_thresh=$1
      serv_epoch=$2
      serv_max_pkts_per_epoch=$3
      serv_max_other_per_epoch=$4
      exptime=$(date +"%y%m%d-%H%M")
      outdir="$rootdir/exp/mw_micro/$exptime"
      mkdir -p $outdir

      if [[ $trace_name != "medical_xlarge_ceiling.cdataset" ]]; then
        base_rate=9
        if [[ $cfg -eq $cfg_base ]]; then
          declare -a client_seq=(1 2 4 8 16 32 64 128 152 160 200)
          nproc=1
        else
          base_rate=5
          declare -a client_seq=(1 2 4 8 16 28 32 64 115 128 160 200 220 230 240 250)
          nproc=1
        fi
        if [[ $trace_name == "workload_trace_M65_L30_XL5" ]]; then
          mw_trace=1
        else
          mw_trace=0
        fi
      else
        mw_trace=0
        if [[ $cfg -eq $cfg_base ]]; then
          declare -a client_seq=(1 2 4 8 16 32 64 128 250 300 384)
          base_rate=3
          nproc=1
        else
          declare -a client_seq=(1 2 4 8 16 32 64 128 250)
          base_rate=3
          nproc=1
        fi
      fi

      for num_clients in ${client_seq[@]}
      do
        if [[ $mw_trace -eq 1 ]]; then
          workloadsdir="$datadir/$trace_name"
        else
          workloadsdir="$datadir/workload_cluster_ceilings"
        fi
        trace_dataset="$workloadsdir/$trace_name"

        for iter in $(seq -s, 0 2); do
          wrk2_load_rate=$(($num_clients*$base_rate))
          echo "Running for the dataset: $trace_dataset"

          nthreads=$num_clients
          if [[ $minimum_experiment_time -ge 30 ]]; then
            profduration=$(( $minimum_experiment_time - 30 ))
            totalreq=$(( $base_rate * $num_clients * $profduration ))
          fi

          run_bench $nthreads $nreq

  #        sleep 5
        done ## iter

      done ## num_clients
    done ## serv_spin_thresh, serv_epoch, serv_max_pkts, serv_max_other
  done ## profiles_to_enf_dir
done ## trace/req
done ## cfg
done ## minimum_experiment_time
date;
