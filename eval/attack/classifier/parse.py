#!/usr/bin/python

from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import backend as K
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv1D, Flatten, MaxPooling1D, Dropout
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Activation
from tensorflow.keras.utils import to_categorical

from sklearn.preprocessing import MinMaxScaler

#import theano
import os
import numpy as np
import datetime
import time

from parallel_parse import *

c_index = 0
c_timestamp = 1
c_interval = 2
c_currseq = 3
c_seqdiff = 4
c_drift = 5
c_rtt = 6
c_sleep = 7

def get_tstamps_files(directory, datatype="v", sstr="client", sel_vid_list=None, suffixstr=""):
    files = sorted_alphanumeric(os.listdir(directory))
    filelist = []
    for vid in sel_vid_list[0]:
        for it in sel_vid_list[1]:
            filestr = "tstamps.%s%d.%s.%d" % (datatype, vid, sstr, it)
            if (suffixstr != ""):
                filestr = "%s.%s" % (filestr, suffixstr)
            if filestr in files:
                filelist.append(filestr)

    return filelist


def select_iters(label_data_arr3d, start_label, end_label, incr_label,
    per_label_iter, start_iter, end_iter):
    train_data_arr3d = []
    print_str = ""
    num_label = int((end_label - start_label)/incr_label) + 1
    if (start_label == end_label):
        print_str = "%sLabels: %d" % (print_str, start_label)
        if (start_iter == end_iter):
            print_str = "%s, Iters: %d" % (print_str, start_iter)
        else:
            print_str = "%s, Iters: %d-%d" % (print_str, start_iter, end_iter)
    else:
        print_str = "%sLabels: %d-%d" % (print_str, start_label, end_label)
        if (start_iter == end_iter):
            print_str = "%s, Iters: %d" % (print_str, start_iter)
        else:
            print_str = "%s, Iters: %d-%d" % (print_str, start_iter, end_iter)
    print(print_str)

    start = datetime.datetime.now().timestamp()

#    for key in range(start_label, end_label+1, incr_label):
    for key in range(num_label):
        start_index = key * per_label_iter + start_iter
        end_index = key * per_label_iter + end_iter
#        print(key, start_index, end_index)
        for iter in range(start_index, end_index+1):
            train_data_arr3d.append(label_data_arr3d[iter])

    end = datetime.datetime.now().timestamp()
    print("Time taken (us): %f" % (end - start))
    return train_data_arr3d


def gen_train_data_numpy2(train_data_arr3d, ydim_max=0):

    if (ydim_max > 0):
        max_y_dim = ydim_max
    else:
        max_y_dim = 0
        for pps_arr2d in train_data_arr3d:
            if len(pps_arr2d[1][0]) > max_y_dim:
                max_y_dim = len(pps_arr2d[1][0])
        print(max_y_dim, max_y_dim-1)

        ## eliminate last bucket at min_y_dim also, as it may
        ## also be only partially filled in some iterations
#        max_y_dim -= 1

    ## Generate numpy array from input data
    x_train_arr3d = np.empty((len(train_data_arr3d), 1))
    y_train_arr3d = np.empty((len(train_data_arr3d), 1, max_y_dim))
    count = 0
    for pps_arr2d in train_data_arr3d:
        x_train_arr3d[count] = pps_arr2d[0]
        if (len(pps_arr2d[1][0]) < max_y_dim):
            for i in range(len(pps_arr2d[1][0]), max_y_dim):
                pps_arr2d[1][1].append(-1000.0) ## fake data, use invalid values for padding
#                pps_arr2d[1][1].append(pps_arr2d[1][1][-1]) ## generate fake data
        y_train_arr3d[count] = pps_arr2d[1][1][:max_y_dim]

        count += 1

    print(x_train_arr3d.shape)
    print(y_train_arr3d.shape)

    ## Re-shape arrays to format required for the training model
    y_train2_arr3d = np.reshape(y_train_arr3d,
                                (y_train_arr3d.shape[0], y_train_arr3d.shape[1],
                                 y_train_arr3d.shape[2], 1))

    x_train2_arr3d = np.reshape(x_train_arr3d, (x_train_arr3d.shape[0], 1))
    print(x_train2_arr3d.shape)
    print(y_train2_arr3d.shape)

    ## Convert label array to a binary based representation
    ## for categorical crossentropy loss function
#    x_train3_arr3d = to_categorical(x_train2_arr3d)
#    print(x_train3_arr3d.shape)

#    return (x_train3_arr3d, y_train2_arr3d, max_y_dim)

    return (x_train2_arr3d, y_train2_arr3d, max_y_dim)


def gen_train_data_numpy_multidim(train_data_arr3d_1, train_data_arr3d_2, ydim_max=0):
    if (ydim_max > 0):
        max_y_dim = ydim_max
    else:
        max_y_dim = 0
        for pps_arr2d in train_data_arr3d_1:
            if (len(pps_arr2d[1][0]) > max_y_dim):
                max_y_dim = len(pps_arr2d[1][0])

        for pps_arr2d in train_data_arr3d_2:
            if (len(pps_arr2d[1][0]) > max_y_dim):
                max_y_dim = len(pps_arr2d[1][0])

        print(max_y_dim, max_y_dim-1)

    ## each elem in train_data_arr3d:
    ## [ vid-label, [ [tstamps-arr], [ [avg1-arr], [avg2-arr], ..., [avgn-arr] ] ] ]
    ## num_channels = n in avgn-arr
    chan1 = len(train_data_arr3d_1[0][1][1])
    chan2 = len(train_data_arr3d_2[0][1][1])
    num_channels = chan1 + chan2

    ## Generate numpy array from input data
    x_train_arr3d = np.empty((len(train_data_arr3d_1), 1))
    y_train_arr3d = np.empty((len(train_data_arr3d_1), num_channels, max_y_dim))
    count = 0
    for e in range(len(train_data_arr3d_1)):
        pps_arr2d_1 = train_data_arr3d_1[e]
        pps_arr2d_2 = train_data_arr3d_2[e]
        x_train_arr3d[count] = pps_arr2d_1[0]
        if (len(pps_arr2d_1[1][0]) < max_y_dim):
            for pps_arr1d in pps_arr2d_1[1][1]:
                for i in range(len(pps_arr2d_1[1][0]), max_y_dim):
                    pps_arr1d.append(-1000.0)
        if (len(pps_arr2d_2[1][0]) < max_y_dim):
            for pps_arr1d in pps_arr2d_2[1][1]:
                for i in range(len(pps_arr2d_2[1][0]), max_y_dim):
                    pps_arr1d.append(-1000.0)

        for c in range(chan1):
            y_train_arr3d[count][c] = pps_arr2d_1[1][1][c][:max_y_dim]
        for c in range(chan2):
            y_train_arr3d[count][chan1+c] = pps_arr2d_2[1][1][c][:max_y_dim]

        count += 1

    print(x_train_arr3d.shape)
    print(y_train_arr3d.shape)

    ## Re-shape arrays to format required for the training model
    y_train2_arr3d = np.reshape(y_train_arr3d,
                                (y_train_arr3d.shape[0], 1,
                                 y_train_arr3d.shape[2], y_train_arr3d.shape[1]))

    x_train2_arr3d = np.reshape(x_train_arr3d, (x_train_arr3d.shape[0], 1))
    print(x_train2_arr3d.shape)
    print(y_train2_arr3d.shape)

    return (x_train2_arr3d, y_train2_arr3d, max_y_dim)



def get_avgs_internal(op_col, baseindex, i, period, count, avgtype):
    if (avgtype == "ts"):
        avg = float(sum(op_col[baseindex:i]))/period
    elif (avgtype == "cnt"):
        avg = float(sum(op_col[baseindex:i]))/count
    elif (avgtype == "sum"):
        avg = float(sum(op_col[baseindex:i]))
    elif (avgtype == "pkt"):
        avg = count

    return avg


def get_avgs(data_arr3d, basets=0, period=0.025, base_col=1, avg_col=5, avgtype="ts"):
    rawlen = len(data_arr3d)

    base = basets
    base_end = base + period
#    base = data_arr3d[baseindex][base_col]
    count = 0
    avg = 0
    avg_col_arr = []
    avg_ts = []
    if (avg_col >= 0):
        op_col = [ float(r[avg_col]) for r in data_arr3d ]

#    print("basets %f base col %d avg col %d datatypes %s %s" % (
#        basets, base_col, avg_col,
#        type(data_arr3d[0][base_col]), type(data_arr3d[0][avg_col])))

    startidx = 0
    i = 0
    while i < rawlen and float(data_arr3d[i][base_col]) < basets:
        i += 1

    if (i >= rawlen):
        print("i %d rawlen %d basets %f first ts %f last ts %f" % (
            i, rawlen, basets, float(data_arr3d[0][base_col]),
            float(data_arr3d[rawlen-1][base_col])))
        return None

    startidx = i
    baseindex = startidx
#    print("basets %.9f startidx %d len %d" % (basets, startidx, rawlen))

    for i in range(startidx, rawlen):
        ## timestamp does not belong to this range, skip ranges till
        ## enclosing range for the starting timestamp is found
        while (float(data_arr3d[baseindex][base_col]) >= base_end):
#            avg_ts.append(base_end)
#            avg_col_arr.append(0.0)
            base += period
            base_end += period

        if ((float(data_arr3d[baseindex][base_col]) >= base
             and float(data_arr3d[baseindex][base_col]) < base_end) == False):
            print(i, rawlen, baseindex, base_col, base, base_end, data_arr3d[baseindex][base_col])

        assert(float(data_arr3d[baseindex][base_col]) >= base
            and float(data_arr3d[baseindex][base_col]) < base_end)

        ## keep iterating through timestamps till one found that is
        ## greater than the end of the current timestamp range
        if (float(data_arr3d[i][base_col]) < base_end):
            continue

#        avg_ts.append(base)
#        avg_ts.append(float(data_arr3d[i-1][base_col]))
        avg_ts.append(base_end)
        count = i - baseindex
        avg = get_avgs_internal(op_col, baseindex, i, period, count, avgtype)
        avg_col_arr.append(avg)
        ## base need not be appended here, since it would have been
        ## appended in the while loop above itself

#        print("#elems %d base %f base_end %f i %d itime %f count %d"    \
#            " baseindex %d basetime %f" % (len(avg_col_arr), base, base_end,
#            i, data_arr3d[i][base_col], count, baseindex,
#            data_arr3d[baseindex][base_col]))

        ## update starting index for next averaging range
        ## only if previous range contained any elements
        baseindex = i

        count = 0
        base += period
        base_end += period

    count = (i - baseindex + 1)
    if count != 0:
        avg = get_avgs_internal(op_col, baseindex, baseindex+count, period, count, avgtype)
        avg_col_arr.append(avg)
#        avg_ts.append(base)
#        avg_ts.append(float(data_arr3d[rawlen-1][base_col]))
        avg_ts.append(base_end)

#        print("#elems %d base %f base_end %f i %d itime %f count %d"    \
#            " baseindex %d basetime %f" % (len(avg_col_arr), base, base_end,
#            i, data_arr3d[i][base_col], count, baseindex,
#            data_arr3d[baseindex][base_col]))

#    print("Averages %d %d" % (len(avg_ts), len(avg_col_arr)))
    return [ avg_ts, avg_col_arr ]


def get_avgs2(data_arr3d, basets=0, period=0.025, base_col=1, avg_col_list=[], avgtype="ts"):
    rawlen = len(data_arr3d)

    base = basets
    base_end = base + period
    count = 0
    avg = 0
    avg_col_arr2d =  [ [] for avg_col in avg_col_list ]
    avg_ts = []
    op_col_arr2d = []
#    print(avg_col_list, len(data_arr3d), len(data_arr3d[0]))
    for avg_col in avg_col_list:
        op_col = [ float(r[avg_col]) for r in data_arr3d ]
        op_col_arr2d.append(op_col)

#    print("base col %d avg col %d datatypes %s %s" % (base_col, avg_col,
#          type(data_arr3d[0][base_col]), type(data_arr3d[0][avg_col])))

    startidx = 0
    i = 0
    while i < rawlen and float(data_arr3d[i][base_col]) < basets:
        i += 1

    if (i >= rawlen):
        return None

    startidx = i
    baseindex = startidx
    print("basets %.9f startidx %d len %d" % (basets, startidx, rawlen))

    for i in range(startidx, rawlen):
        ## timestamp does not belong to this range, skip ranges till
        ## enclosing range for the starting timestamp is found
        while (float(data_arr3d[baseindex][base_col]) >= base_end):
#            for cidx in len(avg_col_list):
#                avg_col_arr2d[cidx].append(0.0)
#            avg_ts.append(base_end)
            base += period
            base_end += period

        if ((float(data_arr3d[baseindex][base_col]) >= base
             and float(data_arr3d[baseindex][base_col]) < base_end) == False):
            print(i, rawlen, baseindex, base_col, base, base_end, data_arr3d[baseindex][base_col])
        assert(float(data_arr3d[baseindex][base_col]) >= base
            and float(data_arr3d[baseindex][base_col]) < base_end)

        ## keep iterating through timestamps till one found that is
        ## greater than the end of the current timestamp range
        if (float(data_arr3d[i][base_col]) < base_end):
            continue

#        avg_ts.append(base)
        avg_ts.append(base_end)
        count = i - baseindex

        for cidx in range(len(avg_col_list)):
            op_col = op_col_arr2d[cidx]
            avg = get_avgs_internal(op_col, baseindex, i, period, count, avgtype)
            avg_col_arr2d[cidx].append(avg)
            ## base need not be appended here, since it would have been
            ## appended in the while loop above itself

        ## update starting index for next averaging range
        ## only if previous range contained any elements
        baseindex = i

        count = 0
        base += period
        base_end += period

    count = (i - baseindex + 1)
    if count != 0:
#        avg_ts.append(base)
        avg_ts.append(base_end)

        for cidx in range(len(avg_col_list)):
            op_col = op_col_arr2d[cidx]
            avg = get_avgs_internal(op_col, baseindex, baseindex+count, period, count, avgtype)
            avg_col_arr2d[cidx].append(avg)

    return [ avg_ts, avg_col_arr2d ]


"""
Relativize columns in place,
append normalized columns to the list of colums
"""
def gen_norm_data2(raw_data, rel_col_idx_list=[], norm_col_idx_list=[1]):
#    print("rel cols %s norm cols %s" % (rel_col_idx_list, norm_col_idx_list))

    rd_arr = raw_data[1]
    assert(len(norm_col_idx_list) <= len(rd_arr[0]))

    rel_col_arr = [ rd_arr[0][c] for c in rel_col_idx_list ]

    normed_col_arr2d = []
    for norm_col_idx in norm_col_idx_list:
        orig_col = [ r[norm_col_idx] for r in rd_arr ]
        orig_col = np.reshape(orig_col, (-1, 1))
        scaler = MinMaxScaler()
        normed_col = scaler.fit_transform(orig_col)
        normed_col_arr2d.append(normed_col)

    ncols = len(rd_arr[0])
    nnormcols = len(norm_col_idx_list)
    new_rd_arr2d = []
    for j in range(len(rd_arr)):
        cnorm = 0
        crel = 0
        new_rd_arr = []
        for c in range(ncols):
            if c in rel_col_idx_list:
                new_rd_arr.append(rd_arr[j][c] - rel_col_arr[crel])
                crel += 1
            else:
                new_rd_arr.append(rd_arr[j][c])
        for cnorm in range(nnormcols):
            new_rd_arr.append(normed_col_arr2d[cnorm][j])

        new_rd_arr2d.append(new_rd_arr)

#    print(len(new_rd_arr2d), len(new_rd_arr2d[0]))
    return [ raw_data[0], new_rd_arr2d ]


def get_timeseries(rawdata, first_ts=0, func=None, **kwargs):
##    print(len(rawdata), rawdata[0], len(rawdata[1]), len(rawdata[1][0]))
##    timeseries = [ row[colid] for row in rawdata[1] ]

    if (func != None):
        timeseries = func(rawdata, first_ts, **kwargs)
    else:
        timeseries = []
        col_arr = kwargs['col_arr']
        for row in rawdata:
            t = [ row[colid] for colid in col_arr ]
            timeseries.append(t)
    return timeseries


def gen_train_data_numpy3(train_data_arr3d, ydim_min=0):

    if (ydim_min > 0):
        min_y_dim = ydim_min
    else:
        min_y_dim = 9999999
        for pps_arr2d in train_data_arr3d:
            if len(pps_arr2d[1]) < min_y_dim:
                min_y_dim = len(pps_arr2d[1])
        print(min_y_dim, min_y_dim-1)

        ## eliminate last bucket at min_y_dim also, as it may
        ## also be only partially filled in some iterations
        min_y_dim -= 1

    ## Generate numpy array from input data
    x_train_arr3d = np.empty((len(train_data_arr3d), 1))
    y_train_arr3d = np.empty((len(train_data_arr3d), min_y_dim))
    count = 0
#    print(train_data_arr3d[0][1].shape)
    for pps_arr2d in train_data_arr3d:
        x_train_arr3d[count] = pps_arr2d[0]
        y_tmp_arr = [ p[0] for p in pps_arr2d[1] ]
#        print(pps_arr2d[0], len(pps_arr2d[1]))
#        print(pps_arr2d[1].shape)
        if (len(y_tmp_arr) < min_y_dim):
            for i in range(len(y_tmp_arr), min_y_dim):
                y_tmp_arr.append(y_tmp_arr[-1]) ## generate fake data
        y_train_arr3d[count] = y_tmp_arr[:min_y_dim]

        count += 1

    print(x_train_arr3d.shape)
    print(y_train_arr3d.shape)

    ## Re-shape arrays to format required for the training model
    y_train2_arr3d = np.reshape(y_train_arr3d,
                                (y_train_arr3d.shape[0], 1, y_train_arr3d.shape[1]))

    x_train2_arr3d = np.reshape(x_train_arr3d, (x_train_arr3d.shape[0], 1))
    print(x_train2_arr3d.shape)
    print(y_train2_arr3d.shape)

    ## Convert label array to a binary based representation
    ## for categorical crossentropy loss function
#    x_train3_arr3d = to_categorical(x_train2_arr3d)
#    print(x_train3_arr3d.shape)

#    return (x_train3_arr3d, y_train2_arr3d, min_y_dim)

    return (x_train2_arr3d, y_train2_arr3d, min_y_dim)


###################################################
## TCPDUMP parsing functions

def gen_train_data_numpy_tcpdump(train_data_arr3d, ydim_max=0):

    if (ydim_max > 0):
        max_y_dim = ydim_max
    else:
        max_y_dim = 0
        for pps_arr2d in train_data_arr3d:
            if (len(pps_arr2d[1][0]) > max_y_dim):
                max_y_dim = len(pps_arr2d[1][0])
        print(max_y_dim, max_y_dim-1)

#        max_y_dim -= 1

    ## Generate numpy array from input data
    x_train_arr3d = np.empty((len(train_data_arr3d), 1))
    y_train_arr3d = np.empty((len(train_data_arr3d), 1, max_y_dim))
    count = 0
    for pps_arr2d in train_data_arr3d:
        if (pps_arr2d[0] == 100):
            x_train_arr3d[count] = 0
        elif (pps_arr2d[0] == 500):
            x_train_arr3d[count] = 1
        elif (pps_arr2d[0] == 1000):
            x_train_arr3d[count] = 2
        elif (pps_arr2d[0] == 2000):
            x_train_arr3d[count] = 3
        else:
            x_train_arr3d[count] = pps_arr2d[0]
        if (len(pps_arr2d[1][0]) < max_y_dim):
            for i in range(len(pps_arr2d[1][0]), max_y_dim):
                pps_arr2d[1][1].append(-1000.0) ## fake data, use values that are not legitimate
#                pps_arr2d[1][1].append(pps_arr2d[1][1][-1]) ## generate fake data
        y_train_arr3d[count] = pps_arr2d[1][1][:max_y_dim]

        count += 1

    print(x_train_arr3d.shape)
    print(y_train_arr3d.shape)

    ## Re-shape arrays to format required for the training model
    y_train2_arr3d = np.reshape(y_train_arr3d,
                                (y_train_arr3d.shape[0], y_train_arr3d.shape[1],
                                 y_train_arr3d.shape[2], 1))

    x_train2_arr3d = np.reshape(x_train_arr3d, (x_train_arr3d.shape[0], 1))
    print(x_train2_arr3d.shape)
    print(y_train2_arr3d.shape)

    ## Convert label array to a binary based representation
    ## for categorical crossentropy loss function
    x_train3_arr3d = to_categorical(x_train2_arr3d)
    print(x_train3_arr3d.shape)

    return (x_train3_arr3d, y_train2_arr3d, max_y_dim)

#    return (x_train2_arr3d, y_train2_arr3d, min_y_dim)



##################################################
def get_xpose(label_data_arr3d, start_idx, end_idx):
    max_len = np.amax([ len(label_data_arr3d[i][1][1]) for i in range(start_idx, end_idx) ])
    min_len = np.amin([ len(label_data_arr3d[i][1][1]) for i in range(start_idx, end_idx) ])

    label_data_arr3d_xpose = []
    for count in range(max_len):
        arr2d_xpose = [ label_data_arr3d[i][1][1][count]
                if (len(label_data_arr3d[i][1][1]) > count) else 0
                for i in range(start_idx, end_idx) ]
        label_data_arr3d_xpose.append(arr2d_xpose)

    print(len(label_data_arr3d_xpose), max_len, min_len)
    return label_data_arr3d_xpose


def print_avgs(data_arr3d, new_video_dir, startvid, endvid, incrvid,
    startit, endit, period, is_server=0, avtype="f"):
    # In[69]:

    numvid = int((endvid - startvid)/incrvid) + 1
    numit = (endit - startit + 1)

    for key in range(startvid, endvid+1, incrvid):
    #    start_idx = key * numit
    #    end_idx = (key + 1) * numit
        label_data_arr3d_xpose = get_xpose(data_arr3d, key*numit, key*numit+30)
        print_arr(label_data_arr3d_xpose, ("%s/v%d-%s%4f.txt%s" % (
            new_video_dir, key, ("c" if is_server == 0 else "s"), period, avtype)))


def print_arr(label_data_arr3d_xpose, filename):
    f = open(filename, "w")
    for arr2d_xpose in label_data_arr3d_xpose:
        count = 0
        for v in arr2d_xpose:
            if count == 0:
                f.write("%5.4f" % v)
            else:
                f.write(" %5.4f" % v)
            count += 1
        f.write("\n")
    f.close()
