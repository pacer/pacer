from multiprocessing import Pool
from functools import partial
import inspect
import os
import pandas as pd
import datetime
import time

import re
import itertools

from parse import *

import datetime

##    https://stackoverflow.com/a/36590187

## https://stackoverflow.com/a/48030307
def sorted_alphanumeric(data):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(data, key=alphanum_key)


def parse_one_file(filename, skiprow_count=0, col_list=None, dtype=float):
#    print(col_list)
    data = np.loadtxt(filename, delimiter='\t', ndmin=2, usecols=col_list,
            skiprows=skiprow_count, dtype=dtype)
    return data


def par_gen_raw_data_arr2d(filename, new_video_dir, col_list, skiprow_count=0, dtype=float):
    filepath = ("%s/%s" % (new_video_dir, filename))
    rawdata = parse_one_file(filepath, skiprow_count, col_list, dtype)
    key = int(filename.split('.')[1][1:])
    return [key, rawdata]


def do_par_gen_raw_data(new_video_dir, is_server=0, sel_vid_list=None, col_list=None,
        suffixstr="", dtype=float):
    files = sorted_alphanumeric(os.listdir(new_video_dir))
    if (is_server == 0):
        if (suffixstr == ""):
            file_list = [ filename for filename in files
                         if (len(filename.split('.')) == 4
                             and filename.split('.')[2] =='client')
                         ]
        else:
            file_list = [ filename for filename in files
                         if (len(filename.split('.')) == 5
                             and filename.split('.')[2] =='client'
                             and filename.split('.')[-1] == suffixstr[1:])
                     ]
    else:
        if (suffixstr == ""):
            file_list = [ filename for filename in files
                         if (len(filename.split('.')) == 3
                             and filename.split('.')[2] !='client')
                         ]
        else:
            file_list = [ filename for filename in files
                         if (len(filename.split('.')) == 4
                             and filename.split('.')[2] !='client'
                             and filename.split('.')[-1] == suffixstr[1:])
                         ]

    if (sel_vid_list != None):
        sel_vid_list_flat = list(itertools.product(sel_vid_list[0], sel_vid_list[1]))
#        print(sel_vid_list, sel_vid_list_flat)
        sel_file_list = []
        for f in file_list:
            fsplit = f.split('.')
            itidx = -1 if suffixstr == "" else -2
            if (int(fsplit[itidx]) in sel_vid_list[1] and int(fsplit[1][1:]) in sel_vid_list[0]):
                sel_file_list.append(f)
        file_list = sorted_alphanumeric(sel_file_list)

#    print(file_list)
#    return

    start = datetime.datetime.now().timestamp()

    with Pool(processes=32) as pool:
        raw_data_arr3d = pool.map(partial(par_gen_raw_data_arr2d,
                                          new_video_dir=new_video_dir,
                                          col_list=col_list, dtype=dtype), file_list)
    print("#vid-iters %d, dim of 1 elem %d" % (len(raw_data_arr3d), len(raw_data_arr3d[0])))
    print("vid-label %d, len series of 1 vid-iter %d, dim of series %d" % (
        raw_data_arr3d[0][0], len(raw_data_arr3d[0][1]), len(raw_data_arr3d[0][1][0])))

    end = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (end - start))

    pool.close()
    pool.join()

    return raw_data_arr3d


def load_file_list(directory, filelist, col_list=None, skiprow_count=0, dtype=float):
    with Pool(processes=32) as pool:
        raw_data_arr3d = pool.map(partial(par_gen_raw_data_arr2d, new_video_dir=directory,
            col_list=col_list, skiprow_count=skiprow_count, dtype=dtype), filelist)
    pool.close()
    pool.join()

    return raw_data_arr3d


def par_gen_timeseries_arr2d(data_arr2d, first_ts=0, **kwargs):
    from parse import get_timeseries
    timeseries = get_timeseries(data_arr2d[1], first_ts=first_ts, **kwargs)
    return [ data_arr2d[0], timeseries ]


def do_par_gen_label_data(raw_data_arr3d, **kwargs):
    start = datetime.datetime.now().timestamp()
    print("Gen label data %d" % (len(raw_data_arr3d)))

    from parse import get_timeseries
    with Pool(processes=16) as pool:
        label_data_arr3d = pool.map(partial(par_gen_timeseries_arr2d,
                                            **kwargs), raw_data_arr3d)

    print("#vid-iters %d, dim of 1 elem %d" % (len(label_data_arr3d), len(label_data_arr3d[0])))
    print("vid-label %d, len series of 1 vid-iter %d, dim of series %d" % (
        label_data_arr3d[0][0], len(label_data_arr3d[0][1]), len(label_data_arr3d[0][1][0])))
#    print("vid-label %d elem[0] %f elem[1] %f" % (label_data_arr3d[0][0],
#        label_data_arr3d[0][1][0][0], label_data_arr3d[0][1][1][0]))

    end = datetime.datetime.now().timestamp()
    print("%s Time taken (s): %f" % (start, (end - start)))

    pool.close()
    pool.join()

    return label_data_arr3d


def parse_one_file_basets(filename, new_video_dir):
    filepath = ("%s/%s" % (new_video_dir, filename))
    f = open(filepath, "r")
    buf = f.readline()
    buf = f.readline()
    buf2 = buf.split('\t')
    buf2 = buf2[0].split(' ')
    basets = buf2[-1]
    basets_f = float(basets)
    f.close()
    key = int(filename.split('.')[1][1:])

#    print("%s %10.9f" % (basets, basets_f))
    return [ key, basets_f ]


def load_file_list_basets(directory, filelist):
    with Pool(processes=32) as pool:
        raw_data_arr3d = pool.map(partial(parse_one_file_basets, new_video_dir=directory), filelist)
    pool.close()
    pool.join()

    return raw_data_arr3d
