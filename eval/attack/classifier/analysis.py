#!/usr/bin/python

from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf

from tensorflow import keras as keras
from tensorflow.keras import layers
from tensorflow.keras import backend as K
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv1D, Flatten, MaxPooling1D, Dropout
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Activation
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import GlobalAveragePooling1D
from tensorflow.keras.layers import Lambda

import dis

def build_model_1d(num_classes, rows, cols, nb_filters, psize, ksize):
    model = Sequential()
    model.add(Conv1D(nb_filters, kernel_size=ksize, activation='relu', input_shape=(rows, cols)))
    model.add(Conv1D(nb_filters, kernel_size=ksize, activation='relu'))
    model.add(Conv1D(nb_filters, kernel_size=ksize, activation='relu'))
    model.add(Dropout(rate=0.5))
    model.add(MaxPooling1D(pool_size=psize))
    model.add(Dropout(rate=0.7))
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(rate=0.5))
    model.add(Dense(num_classes, activation='softmax'))
    model.compile(optimizer='adam', loss='categorical_crossentropy',
        metrics=['accuracy'])
    return model


def build_model_1d_FFT(num_classes, rows, cols, nb_filters, psize, ksize):
    model = Sequential()
    model.add(Conv1D(nb_filters, kernel_size=ksize, activation='relu', input_shape=(rows, cols)))
    model.add(Conv1D(nb_filters, kernel_size=ksize, activation='relu'))
#    model.add(Dropout(rate=0.5))
    model.add(MaxPooling1D(pool_size=psize))
    model.add(Lambda(lambda v: tf.cast(tf.compat.v1.spectral.fft(
        tf.cast(v,dtype=tf.complex64)), tf.float32)))
    model.add(Conv1D(nb_filters, kernel_size=ksize, activation='relu'))
    model.add(Conv1D(nb_filters, kernel_size=ksize, activation='relu'))
    model.add(GlobalAveragePooling1D())
    model.add(Dropout(rate=0.5))
#    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(rate=0.5))
    model.add(Dense(num_classes, activation='softmax'))
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy',
        metrics=['accuracy'])
    print(model.summary())
    return model


def build_model_roei(num_classes, rows, cols, nb_filters, pool_size, kernel_size):
    #K.set_image_dim_ordering('th')

    model = Sequential()
    model.add(Conv2D(nb_filters, kernel_size[0], kernel_size[1],
        input_shape = (1, rows, cols)))
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters, kernel_size[0], kernel_size[1]))
    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters, kernel_size[0], kernel_size[1]))
    model.add(Dropout(rate=0.5))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))
    model.add(Dropout(rate=0.7))
    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dropout(rate=0.5))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))
    model.compile(loss='sparse_categorical_crossentropy',
        optimizer='adam', metrics=['accuracy'])
    return model


def get_model_summary(model):
    stringlist = []
    model.summary(print_fn=lambda x: stringlist.append(x))
    model_summary = "\n".join(stringlist)
    return model_summary

def build_model_newapi(num_classes=10, rows=10, cols=100, nb_filters=32,
    pool_size=(1, 6), kernel_size=(1, 16), dropout_arr=(0.5, 0.7, 0.5)):

    model = Sequential()
    model.add(Conv2D(nb_filters, kernel_size = kernel_size,
        input_shape = (rows, cols, 1), activation='relu', padding='same'))
    model.add(Conv2D(nb_filters, kernel_size = kernel_size, activation='relu', padding='same'))
    model.add(Conv2D(nb_filters, kernel_size = kernel_size, activation='relu'))
    model.add(Dropout(rate=dropout_arr[0]))
    model.add(MaxPooling2D(pool_size=pool_size))
    model.add(Dropout(rate=dropout_arr[1]))
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(rate=dropout_arr[2]))
    model.add(Dense(num_classes, activation='softmax'))
    model.compile(loss='sparse_categorical_crossentropy',
        optimizer='adam', metrics=['accuracy'])
#    print(model.summary())
    model_summary = get_model_summary(model)
    return model, model_summary


def build_model_multidim(num_classes=10, rows=10, cols=100, channels=1, nb_filters=32,
    pool_size=(1, 6), kernel_size=(1, 16), dropout_arr=(0.5, 0.7, 0.5)):

    model = Sequential()
    model.add(Conv2D(nb_filters, kernel_size = kernel_size,
        input_shape = (rows, cols, channels), activation='relu', padding='same'))
    model.add(Conv2D(nb_filters, kernel_size = kernel_size, activation='relu', padding='same'))
    model.add(Conv2D(nb_filters, kernel_size = kernel_size, activation='relu'))
    model.add(Dropout(rate=dropout_arr[0]))
    model.add(MaxPooling2D(pool_size=pool_size))
    model.add(Dropout(rate=dropout_arr[1]))
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(rate=dropout_arr[2]))
    model.add(Dense(num_classes, activation='softmax'))
    model.compile(loss='sparse_categorical_crossentropy',
        optimizer='adam', metrics=['accuracy'])
#    print(model.summary())
    model_summary = get_model_summary(model)
    return model, model_summary

def build_model_newapi_act(num_classes=10, rows=10, cols=100, nb_filters=32,
    pool_size=(1, 6), kernel_size=(1, 16), dropout_arr=(0.5, 0.7, 0.5)):

    model = Sequential()
    model.add(Conv2D(nb_filters, kernel_size=kernel_size,
        input_shape=(rows, cols, 1), padding='same', activation='relu'))
#    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters, kernel_size=kernel_size, padding='same', activation='relu'))
#    model.add(Activation('relu'))
    model.add(Conv2D(nb_filters, kernel_size=kernel_size, activation='relu'))
    model.add(Dropout(rate=dropout_arr[0]))
#    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size))
    model.add(BatchNormalization())
    model.add(Dropout(rate=dropout_arr[1]))
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
#    model.add(Activation('relu'))
    model.add(Dropout(rate=dropout_arr[2]))
    model.add(Dense(num_classes, activation='softmax'))
    model.compile(loss='sparse_categorical_crossentropy',
        optimizer='adam', metrics=['accuracy'])
#    print(model.summary())
    model_summary = get_model_summary(model)
    return model, model_summary


##################################
### Model for TCPDUMP data


def build_model_tcpdump(num_classes, rows, cols, nb_filters, pool_size, kernel_size):
    model = Sequential()
    model.add(Conv2D(nb_filters, kernel_size = kernel_size,
        input_shape = (rows, cols, 1), activation='relu', padding='same'))
    model.add(Conv2D(nb_filters, kernel_size = kernel_size,
        activation='relu', padding='same'))
    model.add(Conv2D(nb_filters, kernel_size = kernel_size,
        activation='relu'))
    model.add(Dropout(rate=0.3))
    model.add(MaxPooling2D(pool_size=pool_size))
    model.add(Dropout(rate=0.2))
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(rate=0.3))
    model.add(Dense(num_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
        optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    return model


