#!/bin/bash

date;

#config="unshaped"
config="shaped"

workdir=/local/sme/workspace/pacer/eval/attack
inputdir="$workdir/dataset/$config"
innerdir="NIC1000-base/pervid_niter"

###########################
## run classifier generator
###########################

python3 $workdir/analyze_classifier.py  \
  --directory $inputdir \
  --innerdir $innerdir  \
  --logtype "v" \
  --tcpdump 0 \
  --startvid 0  \
  --endvid 9  \
  --incrvid 1 \
  --startit 0 \
  --endit 49 \
  --basecol 0 \
  --avgcol 2  \
  --periodlist 0.25  \
  --client_ip "139.19.171.95" \
  --server_ip "139.19.171.101"  \
  --server_port 443 \
  --train_percent 0.7 \
  --val_percent 0.05  \
  --action "build"  \
  --shuffle 0 \


exit 0

###########################################
## prediction using pre-trained classifiers
###########################################
modeldir="pre_trained_model/model1_0500"
modelname="$modeldir/model1_kern16pool6B64E64_d222_period0500_dim2798"

#modeldir="pre_trained_model/model2_0500"
#modelname="$modeldir/model1_kern16pool6B64E64_d222_period0500_dim2798"

#modeldir="pre_trained_model/model3_2500"
#modelname="$modeldir/model1_kern16pool6B64E64_d222_period2500_dim559"

## use this if want to try a newly trained classifier,
## which will be generated in the $inputdir
#modelname="model1_kern16pool6B64E64_d222_period0500_dim2798"

python3 $workdir/analyze_classifier.py  \
  --directory $inputdir \
  --innerdir $innerdir  \
  --logtype "v" \
  --tcpdump 0 \
  --startvid 0  \
  --endvid 9  \
  --incrvid 1 \
  --startit 0 \
  --endit 49 \
  --basecol 0 \
  --avgcol 2  \
  --periodlist 0.25  \
  --client_ip "139.19.171.95" \
  --server_ip "139.19.171.101"  \
  --server_port 443 \
  --train_percent 0.7 \
  --val_percent 0.05  \
  --action "load"  \
  --model "$modelname"  \
  --shuffle 1 \
