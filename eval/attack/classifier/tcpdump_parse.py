#!/usr/bin/python

###########################
## tcpdump_parse.py
##
## created on: Apr 27, 2020
##     author: aasthakm
##
## parse tcpdump output
###########################

import os, sys
import argparse
import re
import numpy

from multiprocessing import Pool
from functools import partial

import parse
from parse import *
from parallel_parse import *

#from multiplot import *

# 0 for tcpdump logs from VM 101 and 1 for logs from max15
#idx_off = 0
TIMESTAMP = 4
SRC_IP = 5
SRC_PORT = 6
DST_IP = 7
DST_PORT = 8
PKT_LEN = 9
SSL_TYPE = 10
TCP_FLAGS_START = 12

class reqrsp:
    abs_ts_idx = 0
    pkt_len_idx = 0
    req_rawdata = None
    rsp_rawdata_arr = []
    clnt_ack_rawdata_arr = []

    def __init__(self, req_rawdata, abs_ts_idx, pkt_len_idx):
        self.abs_ts_idx = abs_ts_idx
        self.pkt_len_idx = pkt_len_idx
        self.req_rawdata = req_rawdata
        self.rsp_rawdata_arr = []
        self.clnt_ack_rawdata_arr = []

    def add_rsp_pkt(self, rsp_rawdata):
        self.rsp_rawdata_arr.append(rsp_rawdata)

    def add_clnt_ack_pkt(self, ack_rawdata):
        self.clnt_ack_rawdata_arr.append(ack_rawdata)

    def has_rsp_pkt(self):
        return (len(self.rsp_rawdata_arr) > 0)

    def rsp_len(self):
        return (len(self.rsp_rawdata_arr))

    def ack_len(self):
        return (len(self.clnt_ack_rawdata_arr))

    def req_abs_ts(self):
        return (float(self.req_rawdata[self.abs_ts_idx]))

    def rsp_abs_ts(self, rsp_arr_idx):
        return (float(self.rsp_rawdata_arr[rsp_arr_idx][self.abs_ts_idx]))

    def ack_abs_ts(self, ack_arr_idx):
        return (float(self.clnt_ack_rawdata_arr[ack_arr_idx][self.abs_ts_idx]))

    def req_pkt_len(self):
        return (int(self.req_rawdata[self.pkt_len_idx]))

    def rsp_pkt_len(self, rsp_arr_idx):
        return (int(self.rsp_rawdata_arr[rsp_arr_idx][self.pkt_len_idx]))

    def ack_pkt_len(self, ack_arr_idx):
        return (int(self.clnt_ack_rawdata_arr[ack_arr_idx][self.pkt_len_idx]))

def tcp_fin(rawdata, TCP_FLAGS_START):
    return int(rawdata[TCP_FLAGS_START+0])

def tcp_syn(rawdata, TCP_FLAGS_START):
    return int(rawdata[TCP_FLAGS_START+1])

def tcp_rst(rawdata, TCP_FLAGS_START):
    return int(rawdata[TCP_FLAGS_START+2])

def tcp_psh(rawdata, TCP_FLAGS_START):
    return int(rawdata[TCP_FLAGS_START+3])

def tcp_ack(rawdata, TCP_FLAGS_START):
    return int(rawdata[TCP_FLAGS_START+4])

def tcp_urg(rawdata, TCP_FLAGS_START):
    return int(rawdata[TCP_FLAGS_START+5])

def tcp_ece(rawdata, TCP_FLAGS_START):
    return int(rawdata[TCP_FLAGS_START+6])

def tcp_cwr(rawdata, TCP_FLAGS_START):
    return int(rawdata[TCP_FLAGS_START+7])


def check_valid_pkt(row, client_ip, server_ip, server_port,
    src_ip_idx, src_port_idx, dst_ip_idx, dst_port_idx):

    if ((row[src_ip_idx] != client_ip and row[dst_ip_idx] != client_ip)
        or (row[src_ip_idx] != server_ip and row[dst_ip_idx] != server_ip)
#        or (int(row[SRC_PORT + idx_off]) != client_port
#            and int(row[DST_PORT + idx_off]) != client_port)
        or (int(row[src_port_idx]) != server_port
            and int(row[dst_port_idx]) != server_port)):
        return False

#    print(row[src_ip_idx], row[dst_ip_idx],
#        row[src_port_idx], row[dst_port_idx])
    return True


def check_payload_pkt(row, pkt_len_idx):
    return (int(row[pkt_len_idx]) != 0)


def check_pkt_type(row, ip, port, ip_col, port_col):
    return (row[ip_col] == ip and int(row[port_col]) == port)


def check_pkt_ssl_ctrl_type(row, ssl_type_idx):
    rv = re.split('([0-9]+)', row[ssl_type_idx])
#    print(rv)
    ssltypes = [ 20, 21, 22, 24, 25 ]
    for t in ssltypes:
        if str(t) in rv:
            return True

    return False

"""
Extract non-SSL payload packets sent from victim server
"""
def process_one_tcpdump_file(raw_data_arr2d, client_ip, server_ip, server_port,
    src_ip_idx, src_port_idx, dst_ip_idx, dst_port_idx, pkt_len_idx, ssl_type_idx):
    vid = raw_data_arr2d[0]
    rawdata = raw_data_arr2d[1]
    filtered_data = []
    for i in range(len(rawdata)):
        if (check_valid_pkt(rawdata[i], client_ip, server_ip, server_port,
            src_ip_idx, src_port_idx, dst_ip_idx, dst_port_idx) == False):
            continue
        if (check_payload_pkt(rawdata[i], pkt_len_idx) == False):
            continue
        if (check_pkt_ssl_ctrl_type(rawdata[i], ssl_type_idx) == True):
            continue
        if (check_pkt_type(rawdata[i], server_ip, server_port, src_ip_idx,
            src_port_idx) == False):
            continue

        filtered_data.append(rawdata[i])

#    print("vid %d i %d len %d" % (vid, i, len(filtered_data)))
    return filtered_data


"""
Extract non-SSL requests, associate with each request the server response pkt seq
and the seq of client acks to the server response pkts in separate arrays
"""
def process_one_tcpdump_file_reqrsp(raw_data_arr2d, client_ip, server_ip, server_port,
        src_ip_idx, src_port_idx, dst_ip_idx, dst_port_idx, pkt_len_idx,
        ssl_type_idx, tcp_flags_start_idx, abs_ts_idx):

    vid = raw_data_arr2d[0]
    rawdata = raw_data_arr2d[1]

    filtered_data = []
    i = 0
    req_cnt = 0
    rrobj = None
    inv_cnt = 0
    while (i < len(rawdata)):
        if (check_valid_pkt(rawdata[i], client_ip, server_ip, server_port,
            src_ip_idx, src_port_idx, dst_ip_idx, dst_port_idx) == False):
            i += 1
            inv_cnt += 1
            continue

        if (tcp_syn(rawdata[i], tcp_flags_start_idx) == 1):
            # tcp handshake (syn-ack): skip next ack too
            if (tcp_ack(rawdata[i], tcp_flags_start_idx)):
                i += 1

            i += 1
            continue

        if (check_pkt_ssl_ctrl_type(rawdata[i], ssl_type_idx) == True):
            j = i+1
            while (j < len(rawdata)):
                ## skip acks
                if (check_payload_pkt(rawdata[j], pkt_len_idx) == False):
                    j += 1
                    continue

                ## any other pkt in the sequence should be ssl, first non-ssl pkt is a request
                if (check_pkt_ssl_ctrl_type(rawdata[j], ssl_type_idx) == False):
                    req_cnt += 1
                    break

                j += 1

            i = j
            continue

        # new client request
        if (check_payload_pkt(rawdata[i], pkt_len_idx) == True
                and check_pkt_type(rawdata[i], server_ip, server_port,
                    dst_ip_idx, dst_port_idx) == True):
            nxt_req = 0
            rrobj = reqrsp(rawdata[i], abs_ts_idx, pkt_len_idx)
            j = i+1
            while (j < len(rawdata)):
                if (check_pkt_type(rawdata[j], server_ip, server_port,
                    src_ip_idx, src_port_idx) == True):
                    rrobj.add_rsp_pkt(rawdata[j])
                    j += 1
                    continue

                if (check_pkt_type(rawdata[j], server_ip, server_port,
                    dst_ip_idx, dst_port_idx) == True):
                    # next client request or conn shutdown seq
                    if (check_payload_pkt(rawdata[j], pkt_len_idx) == True
                            or tcp_fin(rawdata[j], tcp_flags_start_idx) == 1):
                        nxt_req = 1
                        break

                    # client ack to server rsp payload pkts
                    rrobj.add_clnt_ack_pkt(rawdata[j])
                    j += 1
                    continue

                print("i %d j %d req_cnt %d nxt req %d rlen %d alen %d" % (
                    i, j, req_cnt, nxt_req, rrobj.rsp_len(), rrobj.ack_len()))
                assert(0)

            assert(nxt_req != 0 or rrobj.has_rsp_pkt())

            i = j
            filtered_data.append(rrobj)
            rrobj = None
            req_cnt += 1
            continue

        i += 1

    if (rrobj != None and rrobj.has_rsp_pkt()):
        filtered_data.append(rrobj)

    tot_rsp_len = 0
    tot_ack_len = 0
    for r in filtered_data:
        tot_rsp_len += r.rsp_len()
        tot_ack_len += r.ack_len()

#    print("vid %d i %d #reqs %d inv %d rlen %d alen %d" % (vid, i,
#        len(filtered_data), inv_cnt, tot_rsp_len, tot_ack_len))
    return [vid, filtered_data]


def get_tcpdump_rsppkts(raw_data_arr2d):
    vid = raw_data_arr2d[0]
    rawdata = raw_data_arr2d[1]
    rsp_ts_arr = []
    base_req_ts = 0.0
    prev_ts = 0.0
    for d in rawdata:
        if (base_req_ts == 0.0):
            base_req_ts = d.req_abs_ts()

        for i in range(d.rsp_len()):
            rsp_ts = d.rsp_abs_ts(i)
            pktlen = d.rsp_pkt_len(i)
            rel_ts = rsp_ts - base_req_ts
            if (prev_ts == 0.0):
                rsppkt_space = rsp_ts - base_req_ts
            else:
                rsppkt_space = rsp_ts - prev_ts
#            if (len(rsp_ts_arr) < 5):
#                print("rel ts %f diff %f" % (rel_ts, rsppkt_space))
            rsp_ts_arr.append([rel_ts, rsppkt_space, pktlen])
            prev_ts = rsp_ts

#    print("vid %d rsp arr len %d" % (vid, len(rsp_ts_arr)))
#    print("rsp ts[0] %f %f %d" % (rsp_ts_arr[0][0], rsp_ts_arr[0][1], rsp_ts_arr[0][2]))
    return [ vid, rsp_ts_arr ]


def get_tcpdump_reqacks(raw_data_arr2d):
    vid = raw_data_arr2d[0]
    rawdata = raw_data_arr2d[1]
    req_ts_arr = []
    base_req_ts = 0.0
    prev_ts = 0.0
    for d in rawdata:
        if (base_req_ts == 0.0):
            base_req_ts = d.req_abs_ts()

        req_ts = d.req_abs_ts()
        reqlen = d.req_pkt_len()
        rel_ts = req_ts - base_req_ts
        if (prev_ts == 0.0):
            pkt_space = req_ts - base_req_ts
        else:
            pkt_space = req_ts - prev_ts
        req_ts_arr.append([rel_ts, pkt_space, reqlen])
        prev_ts = req_ts

        for i in range(d.ack_len()):
            ack_ts = d.ack_abs_ts(i)
            pktlen = d.ack_pkt_len(i)
            rel_ts = ack_ts - base_req_ts
            pkt_space = ack_ts - prev_ts
            req_ts_arr.append([rel_ts, pkt_space, pktlen])
            prev_ts = ack_ts

#    print("vid %d req arr len %d" % (vid, len(req_ts_arr)))
#    print("req ts[0] %f %f %d" % (req_ts_arr[0][0], req_ts_arr[0][1], req_ts_arr[0][2]))
    return [ vid, req_ts_arr ]


def get_tcpdump_files(directory, sstr="attclient", sel_vid_list=None,
    suffixstr="abs"):
    files = sorted_alphanumeric(os.listdir(directory))
    print("#files %d, suffixstr %s" % (len(files), suffixstr))

    filelist = []
#    for vid in sel_vid_list[0]:
#        for it in sel_vid_list[1]:
#            filestr = "tstamps.%s%d.tshark.%s.%d" % (datatype, vid, sstr, it)
#            if filestr in files:
#                filelist.append(filestr)

    for f in files:
        fsplit = f.split('.')

        if ((suffixstr == "abs" and len(fsplit) < 6) or len(fsplit) < 5):
            continue

        if (len(fsplit) > 6 and fsplit[5] == "raw" and fsplit[6] == "analysed"):
            continue

        if (fsplit[2] != "tshark" or fsplit[3] != sstr):
            continue

        if (suffixstr == "abs" and fsplit[-1] != "abs"):
            continue

        if (int(fsplit[1][1:]) not in sel_vid_list[0]):
            continue

        if (int(fsplit[4]) not in sel_vid_list[1]):
            continue

        filelist.append(f)

    return filelist


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to parse tcpdump files")
    parser.add_argument("-d", "--dir", dest="dir", required=True, action="store",
        help="Directory containing the log files")
    parser.add_argument("-dt", "--datatype", dest="datatype", required=True,
        action="store", help="Substring in file name (\"p\" for pages, \"v\" for videos)")
    parser.add_argument("-cl", "--col_list", dest="col_list", required=True,
        type=int, nargs="+", help="List of columns to extract")
    parser.add_argument("-cip", "--client_ip", dest="client_ip", required=True,
        help="Client IP address")
#    parser.add_argument("-cport", "--client_port", dest="client_port", required=True,
#        type=int, help="Client port address")
    parser.add_argument("-sip", "--server_ip", dest="server_ip", required=True,
        help="Server IP address")
    parser.add_argument("-sport", "--server_port", dest="server_port", required=True,
        type=int, help="Server port address")
    parser.add_argument("-sv", "--startvid", dest="startvid", required=True,
        type=int, help="Start video index")
    parser.add_argument("-ev", "--endvid", dest="endvid", required=True,
        type=int, help="End video index")
    parser.add_argument("-iv", "--incrvid", dest="incrvid", required=True,
        type=int, help="Increment index")
    parser.add_argument("-si", "--startit", dest="startit", required=True,
        type=int, help="Start iteration index")
    parser.add_argument("-ei", "--endit", dest="endit", required=True,
        type=int, help="End iteration index")
    parser.add_argument("-t", "--logtype", dest="logtype", required=True,
        help="Type of log files (e.g. server, client, attclient)")
    parser.add_argument("-i", "--idxoff", dest="idx_off", required=True,
        type=int, help="Index offset for col idxes (server: 1, client: 0)")

    args = parser.parse_args()
    directory = args.dir
    datatype = args.datatype
    col_list = args.col_list
    client_ip = args.client_ip
#    client_port = args.client_port
    server_ip = args.server_ip
    server_port = int(args.server_port)
    startvid = int(args.startvid)
    endvid = int(args.endvid)
    incrvid = int(args.incrvid)
    startit = int(args.startit)
    endit = int(args.endit)
    logtype = args.logtype
    idx_off = int(args.idx_off)
    
    print(col_list)

    tsharkfile_list = []

    sel_vid_list = [ [ i for i in range(startvid, endvid+1, incrvid) ],
            [ i for i in range(startit, endit+1, 1) ] ]

    t1 = datetime.datetime.now().timestamp()
    tsharkfile_list = get_tcpdump_files(directory, sstr=logtype,
            sel_vid_list=sel_vid_list, suffixstr="")

#    files = sorted_alphanumeric(os.listdir(directory))
#
##    for vid in range(startvid, endvid, incrvid):
#    for vid in range(startvid, endvid+1, incrvid):
#        for it in range(startit, endit+1):
#            tsharkfile_str = "tstamps.%s%d.tshark.%s.%d" % (datatype, vid,
#                ("server" if (logtype == 1) else "client"), it)
#            if tsharkfile_str in files:
#                tsharkfile = "%s/%s" % (directory, tsharkfile_str)
#                tsharkfile_list.append(tsharkfile)

    print(len(tsharkfile_list))

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    print("=== %f Get list of tcpdump files === " % t2)
    t1 = t2

    raw_data = load_file_list(directory, tsharkfile_list, dtype=str)
    print("raw len %d raw[0] len %d raw[0][0] %d raw[0][1] len %d" % (
        len(raw_data), len(raw_data[0]), raw_data[0][0], len(raw_data[0][1])))

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    print("=== %f Load tcpdump files === " % t2)
    t1 = t2

#    raw_data = load_file_list(directory, tsharkfile_list, dtype=str,
#            col_list=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])

#    print(tsharkfile_list)
#    process_one_tcpdump_file(tsharkfile, client_ip, server_ip, server_port)

#    ## make sure to run script with python3
#    with Pool(processes=32) as pool:
#        pool.map(partial(process_one_tcpdump_file, client_ip=client_ip,
#            server_ip=server_ip, server_port=server_port,
#            src_ip_idx=6, src_port_idx=7, dst_ip_idx=8, dst_port_idx=9,
#            pkt_len_idx=10, ssl_type_idx=11), raw_data)
##            print(tsharkfile)
##    print(filtered_data)

    with Pool(processes=32) as pool:
        filtered_data_arr2d = pool.map(partial(process_one_tcpdump_file_reqrsp,
            client_ip=client_ip, server_ip=server_ip, server_port=server_port,
            src_ip_idx=6, src_port_idx=7, dst_ip_idx=8, dst_port_idx=9,
            pkt_len_idx=10, ssl_type_idx=11, tcp_flags_start_idx=12, abs_ts_idx=2),
            raw_data)

    pool.close()
    pool.join()

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    print("=== %f Extract req-rsp data === " % t2)
    t1 = t2

    with Pool(processes=32) as pool:
        rsppkts_arr2d = pool.map(partial(get_tcpdump_rsppkts), filtered_data_arr2d)

    pool.close()
    pool.join()

    with Pool(processes=32) as pool:
        reqacks_arr2d = pool.map(partial(get_tcpdump_reqacks), filtered_data_arr2d)

    pool.close()
    pool.join()

#    reqrsp_zip_list = list(zip(rsppkts_arr2d, reqacks_arr2d))
#    combined_arr2d = []
#    for rr in reqrsp_zip_list:
#        rsp = rr[0]
#        req = rr[1]
#        label1 = rsp[0]
#        label2 = req[0]
#        assert(label1 == label2)
#        n_avg_arrs_1 = len(rsp[1])
#        n_avg_arrs_2 = len(req[1])

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    print("=== %f Extract rsp pkt space data === " % t2)
    t1 = t2

#    kwargs={"func": parse.get_avgs, "period": 0.000010, "avgtype": "cnt",
#            "base_col": 0, "avg_col": 1}
    kwargs={"func": parse.get_avgs2, "period": 0.000010, "avgtype": "cnt",
            "base_col": 0, "avg_col_list": (1, 2)}
    label_data_arr3d_1 = do_par_gen_label_data(rsppkts_arr2d, **kwargs)

    print("len avgs %d %d %d %d %d" % (len(label_data_arr3d_1[0][1]),
        len(label_data_arr3d_1[0][1][0]), len(label_data_arr3d_1[0][1][1]),
        len(label_data_arr3d_1[0][1][1][0]), len(label_data_arr3d_1[0][1][1][1])))

    kwargs={"func": parse.get_avgs2, "period": 0.000010, "avgtype": "cnt",
            "base_col": 0, "avg_col_list": (1, 2)}
    label_data_arr3d_2 = do_par_gen_label_data(reqacks_arr2d, **kwargs)

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    print("=== %f Generate label data === " % t2)
    t1 = t2

    gen_train_data_numpy_multidim(label_data_arr3d_1, label_data_arr3d_2, ydim_max=0)
