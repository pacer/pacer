#!/usr/bin/env python
# coding: utf-8

# In[114]:


import os, sys
import argparse
import numpy
from matplotlib import pyplot as plt

import parse
from parse import *
from analysis import *
from parallel_parse import *
from tcpdump_parse import *

from sklearn import metrics
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import GridSearchCV
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier

from tensorflow.keras.models import model_from_json
from tensorflow.keras.models import load_model

import itertools

## for pretty-printing dictionary
import json
import pprint

import random

import time
import datetime

import warnings
import sklearn.exceptions
warnings.filterwarnings("ignore", category=sklearn.exceptions.UndefinedMetricWarning)

MIN_TEST_SAMPLES = 1

np.set_printoptions(suppress=True)

def get_x_label(x):
#    if (x == 0):
#        return 0
#    elif (x == 4):
#        return 1
#    elif (x == 6):
#        return 2
#    elif (x == 7):
#        return 3

    return x

def get_shaped_label_arr(orig):
    label_arr = np.empty(orig.shape)
    for i in range(orig.shape[0]):
        label_arr[i] = get_x_label(orig[i])
    return label_arr

def split_data_xy(label_data_arr3d, startvid, endvid, incrvid, numit,
    train_start, train_end, val_start, val_end, eval_start, eval_end,
    pred_start, pred_end):

    train_data_arr3d = select_iters(label_data_arr3d, startvid, endvid, incrvid,
        numit, train_start, train_end)
    (X, train_Y, dim) = gen_train_data_numpy2(train_data_arr3d, ydim_max=-1)
    print(X.shape)
    print(X[0])
    train_X = get_shaped_label_arr(X)

    val_data_arr3d = select_iters(label_data_arr3d, startvid, endvid, incrvid,
        numit, val_start, val_end)
    (X, val_Y, dim) = gen_train_data_numpy2(val_data_arr3d, ydim_max=dim)
    val_X = get_shaped_label_arr(X)

    eval_data_arr3d = select_iters(label_data_arr3d, startvid, endvid, incrvid,
        numit, eval_start, eval_end)
    (X, eval_Y, _) = gen_train_data_numpy2(eval_data_arr3d, ydim_max=dim)
    eval_X = get_shaped_label_arr(X)

    pred_data_arr3d = select_iters(label_data_arr3d, startvid, endvid, incrvid,
        numit, pred_start, pred_end)
    (X, pred_Y, _) = gen_train_data_numpy2(pred_data_arr3d, ydim_max=dim)
    pred_X = get_shaped_label_arr(X)

    return [train_Y, train_X, val_Y, val_X, eval_Y, eval_X, pred_Y, pred_X]


def model_build_evaluate(param_dict=None, classifier_type=1, train_Y=None, train_X=None,
    val_Y=None, val_X=None, eval_Y=None, eval_X=None, pred_Y=None, pred_X=None,
    n_labels=None, period=None, dim=0):

    n_filters = param_dict['nb_filters']
    pool_size = param_dict['pool_size']
    kernel_size = param_dict['kernel_size']
    batch_size = param_dict['batch_size']
    epochs = param_dict['epochs']
    dropout_arr = param_dict['dropout_arr']
    period_int = int(period * 10000)

    model_filename = "model%d_kern%dpool%dB%dE%d_d%d%d%d_period%04d_dim%d" % (classifier_type,
            kernel_size[1], pool_size[1], batch_size, epochs,
            int(dropout_arr[0] * 10), int(dropout_arr[1] * 10), int(dropout_arr[2] * 10),
            period_int, dim)
    print("model output: %s, directory: %s" % (model_filename, directory))

    t1 = datetime.datetime.now().timestamp()
    print("=== %f Building model %d ===" % (t1, classifier_type))
    if (classifier_type == 1):
        model, model_summary = build_model_newapi(n_labels,
            rows=train_Y.shape[1], cols=train_Y.shape[2],
            nb_filters=n_filters, pool_size=pool_size, kernel_size=kernel_size,
            dropout_arr=dropout_arr)
    elif (classifier_type == 3):
        model, model_summary = build_model_newapi_act(n_labels,
            rows=train_Y.shape[1], cols=train_Y.shape[2],
            nb_filters=n_filters, pool_size=pool_size, kernel_size=kernel_size,
            dropout_arr=dropout_arr)
    elif (classifier_type == 4):
        model, model_summary = build_model_multidim(n_labels,
            rows=train_Y.shape[1], cols=train_Y.shape[2], channels=train_Y.shape[3],
            nb_filters=n_filters, pool_size=pool_size, kernel_size=kernel_size,
            dropout_arr=dropout_arr)

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    print("=== %f Training model ===" % t2)
    t1 = t2
    history = model.fit(train_Y, train_X, epochs=epochs, batch_size=batch_size,
            validation_data=(val_Y, val_X), verbose=0)

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    print("=== %f Evaluating & Predictions with model ===" % t2)
    t1 = t2

#    print(model)

    eval_loss, eval_acc = model.evaluate(eval_Y, eval_X, batch_size=batch_size)
    print("%s eval loss %.4f acc %.4f" % (param_dict, eval_loss, eval_acc))

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    t1 = t2

    pred_out = model.predict(pred_Y)
#    model_check_perf(n_labels, len(pred_X), pred_out)

#    pprint.pprint(history.history)
    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))

    model_json = model.to_json()

    with open("%s/%s.json" % (directory, model_filename), "w") as json_file:
        json_file.write(model_json)

    model.save_weights("%s/%s.h5" % (directory, model_filename))

    with open("%s/%s.hist" % (directory, model_filename), "w") as f:
        f.write("trainacc\ttrainloss\tvalacc\tvalloss\n")
        trainacc = [ v for v in history.history['accuracy'] ]
        trainloss = [ v for v in history.history['loss'] ]
        valacc = [ v for v in history.history['val_accuracy'] ]
        valloss = [ v for v in history.history['val_loss'] ]
        for ta, tl, va, vl in zip(trainacc, trainloss, valacc, valloss):
            f.write("%f\t%f\t%f\t%f\n" % (ta, tl, va, vl))

    with open("%s/%s.summary" % (directory, model_filename), "w") as f:
        f.write(model_summary)

    return [param_dict, history.history, pred_out]


def print_pred_out(pred_out, pred_in):
#    for i in range(len(pred_out)):
    for pout, pin in zip(pred_out, pred_in):
        printstr = "%d: " % pin
        for j in range(len(pout)):
            printstr = ("%s%.6f " % (printstr, pout[j]))
        print("%s" % (printstr))


def model_check_perf(num_classes, num_pred_iters, pred_out, pred_in):
    print_pred_out(pred_out, pred_in)

#    y_orig = [ int(i/num_pred_iters) for i in range(len(pred_out)) ]
    y_orig = pred_in.tolist()
    y_pred = np.argmax(pred_out, axis=1)
    print("ORIG (%d): %s\nPRED (%d): %s" % (len(y_orig), y_orig,
        len(y_pred.tolist()), y_pred.tolist()))

    print("=== Confusion Matrix ===")
    cm = confusion_matrix(y_orig, y_pred)
    print(cm)

    y_target = [ str(i) for i in range(num_classes) ]
    print("=== Classification Report ===")
    print(classification_report(y_orig, y_pred, target_names=y_target))

    tp_arr = np.diag(cm)

    fp_arr = []
    for i in range(num_classes):
        fp_arr.append(sum(cm[:,i]) - cm[i,i])

    fn_arr = []
    for i in range(num_classes):
        fn_arr.append(sum(cm[i,:]) - cm[i,i])

    tn_arr = []
    for i in range(num_classes):
        tmp = np.delete(cm, i, 0) # delete ith row
        tmp = np.delete(tmp, i, 1) # delete ith col
        tn_arr.append(sum(sum(tmp)))

    tot_num_pred = tot_tp = tot_fp = tot_fn = tot_tn = 0
    for i in range(num_classes):
        tot_tp += tp_arr[i]
        tot_tn += tn_arr[i]
        tot_fp += fp_arr[i]
        tot_fn += fn_arr[i]
        tot_num_pred += (tp_arr[i] + fp_arr[i])
    tot_all = tot_tp + tot_fp + tot_fn + tot_tn
    accuracy = (tot_tp + tot_tn)/tot_all
    recall = (tot_tp / (tot_tp + tot_fn))
    precision = (tot_tp / (tot_tp + tot_fp))
    f1score = (2 * precision * recall) / (precision + recall)

    print("TP: %s\nFP: %s\nFN: %s\nTN: %s" % (tp_arr.tolist(), fp_arr, fn_arr, tn_arr))
    print("Agg: #pred\ttot_tp\ttot_fp\ttot_fn\ttot_tn\taccu\treca\tprec\tf1")
    print("\t%d\t%d\t%d\t%d\t%d\t%6.3f\t%6.3f\t%6.3f\t%6.3f" % (
        tot_num_pred, tot_tp, tot_fp, tot_fn, tot_tn,
        accuracy, recall, precision, f1score))


def run_one_analysis(directory, data, startvid, endvid, incrvid, startit, endit,
    train_percent, val_percent, periodlist, param_grid, do_plot=0, shuffle=1):

    numvid = int((endvid - startvid)/incrvid) + 1
    numit = (endit - startit + 1)
    numtrain = int(numit * train_percent)
    numval = int(numtrain * val_percent)
    ## for too small a dataset
    if (numval < MIN_TEST_SAMPLES):
        numval = MIN_TEST_SAMPLES

    train_start = startit
    train_end = train_start + (numtrain - numval) - 1
    val_start = train_end + 1
    val_end = val_start + numval - 1
    eval_start = val_end + 1
    eval_end = endit - 1
    pred_start = eval_start #eval_end + 1
    pred_end = endit
    num_pred = pred_end - pred_start + 1

    print("Total iters %d, train [%d-%d] val [%d-%d] test [%d-%d] predict %d"   \
        " periodlist %s " %
        (numit, train_start, train_end, val_start, val_end,
        eval_start, eval_end, endit, periodlist))

    period = data[0]
    label_data_arr3d = data[1]

    print("======== Period %f ========" % period)

    train_data_arr3d = select_iters(label_data_arr3d, startvid, endvid, incrvid,
            numit, train_start, train_end)
    val_data_arr3d = select_iters(label_data_arr3d, startvid, endvid, incrvid,
            numit, val_start, val_end);
    eval_data_arr3d = select_iters(label_data_arr3d, startvid, endvid, incrvid,
            numit, eval_start, eval_end)
    pred_data_arr3d = select_iters(label_data_arr3d, startvid, endvid, incrvid,
            numit, pred_start, pred_end)

    if (shuffle == 1):
        numpy.random.shuffle(train_data_arr3d)
        numpy.random.shuffle(val_data_arr3d)
        numpy.random.shuffle(eval_data_arr3d)
        numpy.random.shuffle(pred_data_arr3d)

    (X, train_Y, dim) = gen_train_data_numpy2(train_data_arr3d, ydim_max=-1)
    train_X = get_shaped_label_arr(X)
    (X, val_Y, _) = gen_train_data_numpy2(val_data_arr3d, ydim_max=dim)
    val_X = get_shaped_label_arr(X)
    (X, eval_Y, _) = gen_train_data_numpy2(eval_data_arr3d, ydim_max=dim)
    eval_X = get_shaped_label_arr(X)
    (X, pred_Y, _) = gen_train_data_numpy2(pred_data_arr3d, ydim_max=dim)
    pred_X = get_shaped_label_arr(X)

    plt_index = 1
    if (do_plot == 1):
        plt_ncols = 3
        plt_nrows = int((len(param_grid)-1)/plt_ncols) + 1
        fig = plt.figure(figsize=(plt_ncols * 15, plt_nrows * 10), dpi=64)

    plotfile = "p%.5f" % (period)

    print("#param combinations: %d" % len(param_grid))
    numproc = len(param_grid)%8
    mod_hist_arr = []
    with Pool(processes=numproc) as pool:
        mod_hist_arr = pool.map(partial(model_build_evaluate, classifier_type=1,
            train_Y=train_Y, train_X=train_X, val_Y=val_Y, val_X=val_X,
            eval_Y=eval_Y, eval_X=eval_X, pred_Y=pred_Y, pred_X=pred_X,
            n_labels=numvid, period=periodlist[0], dim=dim), param_grid)

    pool.close()
    pool.join()

    for result in mod_hist_arr:
        param_dict = result[0]
        history = result[1]
        pred_out = result[2]

        model_check_perf(num_classes=numvid, num_pred_iters=num_pred,
            pred_out=pred_out, pred_in=pred_X)

        if (do_plot == 1):
            ax = fig.add_subplot(plt_nrows, plt_ncols, plt_index)
            ax.title.set_text(str(param_dict))
            ax.set_xlabel('epoch')
            ax.set_ylim(bottom=0, top=100)
            ax.plot(history['loss'], label='train loss')
            ax.plot(history['val_loss'], label='test loss')
            ax.set_ylabel('loss')
            ax.legend(framealpha=0, loc='upper left')

            ax2 = ax.twinx()
            ax2.set_ylim(bottom=0.0, top=1.0)
            ax2.plot(history['accuracy'], linestyle='--', label='train acc')
            ax2.plot(history['val_accuracy'], linestyle='--', label='test acc')
            ax2.set_ylabel('accuracy')
            ax2.legend(framealpha=0, loc='upper right')

        plt_index += 1

    if (do_plot == 1):
        plt.savefig("%s/%s.pdf" % (directory, plotfile), transparent=True)


def run_one_analysis_multidim(directory,
    data, startvid, endvid, incrvid, startit, endit,
    train_percent, val_percent, periodlist, param_grid, do_plot=0, shuffle=1):

    numvid = int((endvid - startvid)/incrvid) + 1
    numit = (endit - startit + 1)
    numtrain = int(numit * train_percent)
    numval = int(numtrain * val_percent)
    ## for too small a dataset
    if (numval < MIN_TEST_SAMPLES):
        numval = MIN_TEST_SAMPLES

    train_start = startit
    train_end = train_start + (numtrain - numval) - 1
    val_start = train_end + 1
    val_end = val_start + numval - 1
    eval_start = val_end + 1
    eval_end = endit - 1
    pred_start = eval_start #eval_end + 1
    pred_end = endit
    num_pred = pred_end - pred_start + 1

    print("Total iters %d, train [%d-%d] val [%d-%d] test [%d-%d] predict %d"   \
        " periodlist %s " %
        (numit, train_start, train_end, val_start, val_end,
        eval_start, eval_end, endit, periodlist))

    period = data[0]
    label_data_arr3d_1 = data[1]
    label_data_arr3d_2 = data[2]

    print("======== Period %f ========" % period)

    train_data_arr3d_1 = select_iters(label_data_arr3d_1, startvid, endvid, incrvid,
            numit, train_start, train_end)
    val_data_arr3d_1 = select_iters(label_data_arr3d_1, startvid, endvid, incrvid,
            numit, val_start, val_end);
    eval_data_arr3d_1 = select_iters(label_data_arr3d_1, startvid, endvid, incrvid,
            numit, eval_start, eval_end)
    pred_data_arr3d_1 = select_iters(label_data_arr3d_1, startvid, endvid, incrvid,
            numit, pred_start, pred_end)

    train_data_arr3d_2 = select_iters(label_data_arr3d_2,
        startvid, endvid, incrvid, numit, train_start, train_end)
    val_data_arr3d_2 = select_iters(label_data_arr3d_2,
        startvid, endvid, incrvid, numit, val_start, val_end);
    eval_data_arr3d_2 = select_iters(label_data_arr3d_2,
        startvid, endvid, incrvid, numit, eval_start, eval_end)
    pred_data_arr3d_2 = select_iters(label_data_arr3d_2,
        startvid, endvid, incrvid, numit, pred_start, pred_end)

    if (shuffle == 1):
        numpy.random.shuffle(train_data_arr3d_1)
        numpy.random.shuffle(val_data_arr3d_1)
        numpy.random.shuffle(eval_data_arr3d_1)
        numpy.random.shuffle(pred_data_arr3d_1)
        numpy.random.shuffle(train_data_arr3d_2)
        numpy.random.shuffle(val_data_arr3d_2)
        numpy.random.shuffle(eval_data_arr3d_2)
        numpy.random.shuffle(pred_data_arr3d_2)

    (X, train_Y, dim) = gen_train_data_numpy_multidim(
        train_data_arr3d_1, train_data_arr3d_2, ydim_max=-1)
    train_X = get_shaped_label_arr(X)
    (X, val_Y, _) = gen_train_data_numpy_multidim(
        val_data_arr3d_1, val_data_arr3d_2, ydim_max=dim)
    val_X = get_shaped_label_arr(X)
    (X, eval_Y, _) = gen_train_data_numpy_multidim(
        eval_data_arr3d_1, eval_data_arr3d_2, ydim_max=dim)
    eval_X = get_shaped_label_arr(X)
    (X, pred_Y, _) = gen_train_data_numpy_multidim(
        pred_data_arr3d_1, pred_data_arr3d_2, ydim_max=dim)
    pred_X = get_shaped_label_arr(X)

    plt_index = 1
    if (do_plot == 1):
        plt_ncols = 3
        plt_nrows = int((len(param_grid)-1)/plt_ncols) + 1
        fig = plt.figure(figsize=(plt_ncols * 15, plt_nrows * 10), dpi=64)

    plotfile = "p%.5f" % (period)

    print("#param combinations: %d" % len(param_grid))
    numproc = len(param_grid)%8
    mod_hist_arr = []
    with Pool(processes=numproc) as pool:
        mod_hist_arr = pool.map(partial(model_build_evaluate, classifier_type=4,
            train_Y=train_Y, train_X=train_X, val_Y=val_Y, val_X=val_X,
            eval_Y=eval_Y, eval_X=eval_X, pred_Y=pred_Y, pred_X=pred_X,
            n_labels=numvid, period=periodlist[0], dim=dim), param_grid)

    pool.close()
    pool.join()

    for result in mod_hist_arr:
        param_dict = result[0]
        history = result[1]
        pred_out = result[2]

        model_check_perf(num_classes=numvid, num_pred_iters=num_pred,
            pred_out=pred_out, pred_in=pred_X)

        if (do_plot == 1):
            ax = fig.add_subplot(plt_nrows, plt_ncols, plt_index)
            ax.title.set_text(str(param_dict))
            ax.set_xlabel('epoch')
            ax.set_ylim(bottom=0, top=100)
            ax.plot(history['loss'], label='train loss')
            ax.plot(history['val_loss'], label='test loss')
            ax.set_ylabel('loss')
            ax.legend(framealpha=0, loc='upper left')

            ax2 = ax.twinx()
            ax2.set_ylim(bottom=0.0, top=1.0)
            ax2.plot(history['accuracy'], linestyle='--', label='train acc')
            ax2.plot(history['val_accuracy'], linestyle='--', label='test acc')
            ax2.set_ylabel('accuracy')
            ax2.legend(framealpha=0, loc='upper right')

        plt_index += 1

    if (do_plot == 1):
        plt.savefig("%s/%s.pdf" % (directory, plotfile), transparent=True)


def search_opt_model(raw_data_arr3d, startvid, endvid, incrvid, startit, endit,
    train_percent, val_percent, periodlist):

    numvid = int((endvid - startvid)/incrvid) + 1
    numit = (endit - startit + 1)
    numtrain = int(numit * train_percent)
    numval = int(numtrain * val_percent)
    ## for too small a dataset
    if (numval < MIN_TEST_SAMPLES):
        numval = MIN_TEST_SAMPLES

    train_start = startit
    train_end = train_start + (numtrain - numval) - 1
    val_start = train_end + 1
    val_end = val_start + numval - 1
    eval_start = val_end + 1
    eval_end = endit - 1
    pred_start = eval_end + 1
    pred_end = endit
    num_pred = pred_end - pred_start + 1

    print("Total iters %d, train [%d-%d] val [%d-%d] test [%d-%d] predict %d"   \
        " periodlist %s " %
        (numit, train_start, train_end, val_start, val_end,
        eval_start, eval_end, endit, periodlist))

    for period in periodlist:
        print("======== Period %f ========" % period)
        kwargs={"func": parse.get_avgs, "period": period, "avgtype": "ts",
                "base_col": 0, "avg_col": 1}
        label_data_arr3d = do_par_gen_label_data(raw_data_arr3d, **kwargs)

        (X, Y, max_y_dim) = gen_train_data_numpy2(label_data_arr3d, ydim_max=-1)

        (train_Y, train_X, val_Y, val_X, eval_Y, eval_X, pred_Y, pred_X) =  \
            split_data_xy(label_data_arr3d, startvid, endvid, incrvid, numit,
                train_start, train_end, val_start, val_end, eval_start, eval_end,
                pred_start, pred_end)

        model = KerasClassifier(build_fn=build_model_newapi, num_classes=numvid,
                rows=train_Y.shape[1], cols=train_Y.shape[2],
#                nb_filters=16, pool_size=(1,6),
#                kernel_size=(1,12), dropout_arr=(0.2, 0.3, 0.2),
                verbose=0)

        batch_size = [ 32, 64, 128 ]
        epochs = [ 8, 16, 32, 64, 128 ]
        nb_filters = [ 16, 32, 64 ]
        kernel_size = [ (1,12), (1,16) ]
        pool_size = [ (1,6) ]
        dropout_arr = [ (0.5, 0.7, 0.5), (0.2, 0.3, 0.2), (0.1, 0.1, 0.1), (0.2, 0.2, 0.2) ]
        param_grid = dict(batch_size=batch_size, epochs=epochs,
                nb_filters=nb_filters, pool_size=pool_size,
                kernel_size=kernel_size, dropout_arr=dropout_arr
                )
        param_size = (len(batch_size) * len(epochs) * len(nb_filters) *
                len(kernel_size) * len(dropout_arr))
        print("size of param_grid %d" % (param_size))
        grid = GridSearchCV(estimator=model, param_grid=param_grid, cv=3)
        grid_result = grid.fit(train_Y, train_X)

        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with %r" % (mean, stdev, param))


if __name__ == "__main__":
    parser = argparse.ArgumentParser("ML Analysis")
    parser.add_argument("-d", "--directory", dest="directory", action="store",
        required=True, help="Path of log directory")
    parser.add_argument("-sv", "--startvid", dest="startvid", action="store",
        default=0, type=int, help="Start video index")
    parser.add_argument("-ev", "--endvid", dest="endvid", action="store",
        default=9, type=int, help="End video index")
    parser.add_argument("-iv", "--incrvid", dest="incrvid", action="store",
        default=1, type=int, help="Increment step for video index")
    parser.add_argument("-si", "--startit", dest="startit", action="store",
        default=0, type=int, help="First video index")
    parser.add_argument("-ei", "--endit", dest="endit", action="store",
        default=29, type=int, help="First video index")
    parser.add_argument("-trpct", "--train_percent", dest="train_percent", action="store",
        default=0.8, type=float, help="%% of full dataset for train")
    parser.add_argument("-vapct", "--val_percent", dest="val_percent", action="store",
        default=0.05, type=float, help="%% of train dataset for validation")
    parser.add_argument("-t", "--logtype", dest="logtype", action="store",
        required=True, default="v", help="Type of files to analyze (vid: v, file: p)")
    parser.add_argument("-bc", "--basecol", dest="basecol", action="store",
        required=True, type=int, help="Index of base column containing timestamps")
    parser.add_argument("-ac", "--avgcol", dest="avgcol", action="store",
        required=True, type=int, help="Index of column to be averaged and analyzed \
                (e.g. inter-pkt delay, RTT, one-way delay columns)")
    parser.add_argument("-pl", "--periodlist", dest="periodlist", action="store",
        nargs="+", type=float, help="List of batching periods to run analysis for   \
                (e.g. 0.25, 0.0025)")
    parser.add_argument("-shuf", "--shuffle", dest="shuffle", default=1,
        type=int, help="Flag to control shuffling of data input to ML analysis")
    parser.add_argument("-a", "--action", dest="action", default="plot",
        required=True, help="Type of action to perform  \
                ('search' optimal cfg/'build' model/'load' model)")
    parser.add_argument("-m", "--model", dest="model", default=None,
        help="Filename to load model from")
    parser.add_argument("-tcpdump", "--tcpdump", dest="tcpdump", default=0,
        help="Input data tcpdump files (1) or tstamps avg files (0)")
    parser.add_argument("-cip", "--client_ip", dest="client_ip", required=True,
        help="Client IP address")
    parser.add_argument("-sip", "--server_ip", dest="server_ip", required=True,
        help="Server IP address")
    parser.add_argument("-sport", "--server_port", dest="server_port", required=True,
        type=int, help="Server port address")
    parser.add_argument("-id", "--innerdir", dest="innerdir",
        help="tcpdump inner dir (e.g. NIC1000-sme/pervid_niter)")
    
    args = parser.parse_args()
    directory = args.directory
    startvid = args.startvid
    endvid = args.endvid
    incrvid = args.incrvid
    startit = args.startit
    endit = args.endit
    train_percent = args.train_percent
    val_percent = args.val_percent
    datatype = args.logtype
    basecol = args.basecol
    avgcol = args.avgcol
    periodlist = args.periodlist
    shuffle = args.shuffle
    action = args.action
    model_filename = args.model
    is_tcpdump = int(args.tcpdump)
    client_ip = args.client_ip
    server_ip = args.server_ip
    server_port = args.server_port
    innerdir = args.innerdir

    numit = endit - startit + 1
    numtrain = int(numit * train_percent)
    remaining = numit - numtrain

    random.seed(5)

    period = periodlist[0]

    ## load raw data once

    sel_vid_list = [
            [ i for i in range(startvid, endvid+1, incrvid) ],
            [ i for i in range(startit, endit+1) ]
        ]

#    batch_size = [ 32, 64, 128]
#    epochs = [ 8, 16, 32, 64, 128 ]
#    nb_filters = [ 16, 32, 64 ]
#    kernel_size = [ (1,12), (1,16) ]
#    pool_size = [ (1,6) ]
#    dropout_arr = [ (0.5, 0.7, 0.5), (0.2, 0.3, 0.2), (0.1, 0.1, 0.1), (0.2, 0.2, 0.2) ]
    batch_size = [ 64 ]
    epochs = [ 64 ]
    nb_filters = [ 32 ]
    kernel_size = [ (1,16) ]
    pool_size = [ (1,6) ]
    dropout_arr = [ (0.2, 0.2, 0.2) ]
    param_dict = dict(batch_size=batch_size, epochs=epochs, nb_filters=nb_filters,
                        kernel_size=kernel_size, pool_size=pool_size,
                        dropout_arr=dropout_arr)
    keys, values = zip(*param_dict.items())
    print(keys, values)

    param_list = list(itertools.product(*values))
    param_grid = []
    for bundle in param_list:
        d = dict(zip(keys, bundle))
        param_grid.append(d)
    print(len(param_grid))
#    print(param_grid)

    t1 = datetime.datetime.now().timestamp()
    print("=== %f Get raw data %f v [%d-%d] i [%d-%d] ===" % (t1, period,
        startvid, endvid, startit, endit))

    if (is_tcpdump == 0):
        period_int = int(period * 10000)
        innerdir=("%s/data%04d" % (directory, period_int))
        filelist = get_tstamps_files(innerdir, datatype=datatype,
                sstr=("rcvpktnorm%04d" % period_int), sel_vid_list=sel_vid_list)
        raw_data_arr3d = load_file_list(innerdir, filelist)
        print("dir %s #files %d #entries %d" %
            (innerdir, len(filelist), len(raw_data_arr3d)))

        kwargs={"func": parse.get_avgs, "period": period, "avgtype": "sum",
                "base_col": basecol, "avg_col": avgcol}

        t2 = datetime.datetime.now().timestamp()
        print("Time taken (s): %f" % (t2 - t1))
        print("=== %f Generate label data === " % t2)
        t1 = t2

        label_data_arr3d = do_par_gen_label_data(raw_data_arr3d, **kwargs)

    else:
        tsharkfile_list = get_tcpdump_files("%s/%s" % (directory, innerdir),
            sstr=datatype, suffixstr="", sel_vid_list=sel_vid_list)
        raw_data = load_file_list("%s/%s" % (directory, innerdir),
            tsharkfile_list, dtype=str)

        with Pool(processes=32) as pool:
            filtered_data_arr2d = pool.map(partial(process_one_tcpdump_file_reqrsp,
                client_ip=client_ip, server_ip=server_ip, server_port=server_port,
                src_ip_idx=6, src_port_idx=7, dst_ip_idx=8, dst_port_idx=9,
                pkt_len_idx=10, ssl_type_idx=11, tcp_flags_start_idx=12, abs_ts_idx=2),
                raw_data)

        pool.close()
        pool.join()

        with Pool(processes=32) as pool:
            raw_data_arr3d_1 = pool.map(partial(get_tcpdump_rsppkts),
                    filtered_data_arr2d)

        pool.close()
        pool.join()

        with Pool(processes=32) as pool:
            raw_data_arr3d_2 = pool.map(partial(get_tcpdump_reqacks),
                    filtered_data_arr2d)

        pool.close()
        pool.join()

        t2 = datetime.datetime.now().timestamp()
        print("Time taken (s): %f" % (t2 - t1))
        print("=== %f Generate label data === " % t2)
        t1 = t2

        kwargs={"func": parse.get_avgs2, "period": period, "avgtype": "cnt",
            "base_col": 0, "avg_col_list": (1, 2)}
        label_data_arr3d_1 = do_par_gen_label_data(raw_data_arr3d_1, **kwargs)

        kwargs={"func": parse.get_avgs2, "period": period, "avgtype": "cnt",
            "base_col": 0, "avg_col_list": (1, 2)}
        label_data_arr3d_2 = do_par_gen_label_data(raw_data_arr3d_2, **kwargs)

    if (action == "build"):
        t2 = datetime.datetime.now().timestamp()
        print("Time taken (s): %f" % (t2 - t1))
        print("=== %f %s classifier === " % (t2, action))
        t1 = t2

        if (is_tcpdump == 0):
            run_one_analysis(directory,
                [period, label_data_arr3d],
                startvid=startvid, endvid=endvid, incrvid=incrvid,
                startit=startit, endit=endit, train_percent=train_percent,
                val_percent=val_percent, periodlist=periodlist,
                param_grid=param_grid, do_plot=1, shuffle=shuffle)
        else:
            run_one_analysis_multidim(directory,
                [period, label_data_arr3d_1, label_data_arr3d_2],
                startvid=startvid, endvid=endvid, incrvid=incrvid,
                startit=startit, endit=endit, train_percent=train_percent,
                val_percent=val_percent, periodlist=periodlist,
                param_grid=param_grid, do_plot=1, shuffle=shuffle)

    elif (action == "load"):
        numvid = endvid - startvid + 1
        numpred = endit - startit + 1

        fsplit = model_filename.split('_')
        dim = int(fsplit[-1][3:])

        if (is_tcpdump == 0):
            if (shuffle == 1):
                numpy.random.shuffle(label_data_arr3d)

            (X, pred_Y, _) = gen_train_data_numpy2(label_data_arr3d, ydim_max=dim)
        else:
#            if (shuffle == 1):
#                numpy.random.shuffle(label_data_arr3d_1)
#                numpy.random.shuffle(label_data_arr3d_2)

            (X, pred_Y, _) = gen_train_data_numpy_multidim(
                    label_data_arr3d_1, label_data_arr3d_2, ydim_max=dim)

        pred_X = get_shaped_label_arr(X)

        t2 = datetime.datetime.now().timestamp()
        print("Time taken (s): %f" % (t2 - t1))
        print("=== %f %s model %s === " % (t2, action, model_filename))
        t1 = t2

        json_file = open("%s/%s.json" % (directory, model_filename), "r")
        loaded_model_json = json_file.read()
        json_file.close()

        loaded_model = model_from_json(loaded_model_json)
        loaded_model.load_weights("%s/%s.h5" % (directory, model_filename))

        t2 = datetime.datetime.now().timestamp()
        print("Time taken (s): %f" % (t2 - t1))
        print("=== %f Run classifier === " % t2)
        t1 = t2

        pred_out = loaded_model.predict(pred_Y)
        model_check_perf(num_classes=numvid, num_pred_iters=numpred,
                pred_out=pred_out, pred_in=pred_X)


    elif (action == "search"):
        print(action)

        search_opt_model(raw_data_arr3d, startvid, endvid, incrvid, startit, endit,
            train_percent, val_percent, periodlist)

    t2 = datetime.datetime.now().timestamp()
    print("Time taken (s): %f" % (t2 - t1))
    print("=== %f The End === " % t2)
    t1 = t2

