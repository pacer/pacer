<?php

/*
 * libphpioctl.so must be specified in /etc/php/7.0/php.ini
 */
abstract class phpioctl {
	static function dev_open($devname) {
		return dev_open($devname);
	}

	static function dev_close($fd) {
		return dev_close($fd);
	}

	static function dev_ioctl($fd,$cmd,$buf,$len) {
		return dev_ioctl($fd,$cmd,$buf,$len);
	}
}


$logfile = "benchVideo.dbg";
$debug = 1;

$server_ip = "";
$server_port = 0;
$client_ip = "";
$client_port = 0;
$dummy_ip = "139.19.168.173";
$dummy_port = 2231;
$MEMCACHE_SERVER_ARR = array("139.19.171.103", "139.19.171.104");
#$MEMCACHE_SERVER_ARR = array("139.19.171.103");
#$MEMCACHE_SERVER_ARR = array("139.19.171.104");
$MEMCACHE_PORT = 11211;

$sme_ctrl_dev = "/dev/SMECTRL";
$sme_ctrl_fd = 0;

function get_local_ip_port(&$local_ip, &$local_port)
{
  $local_ip = $_SERVER['SERVER_ADDR'];
  $local_port = $_SERVER['SERVER_PORT'];
}

function get_remote_ip_port(&$remote_ip, &$remote_port)
{
  if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $remote_ip = $_SERVER['REMOTE_ADDR'];
      $remote_port = $_SERVER['REMOTE_PORT'];
  } else {
      $remote_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote_port = $_SERVER['REMOTE_PORT'];
  }
}

# packing functions
class int_helper
{
    public static function int8($i)
    {
      return is_int($i) ? pack("c", $i) : unpack("c", $i)[1];
    }

    public static function uInt8($i)
    {
      return is_int($i) ? pack("C", $i) : unpack("C", $i)[1];
    }

    public static function int16($i)
    {
      return is_int($i) ? pack("s", $i) : unpack("s", $i)[1];
    }

    public static function uInt16($i, $endianness = null)
    {
      $f = is_int($i) ? "pack" : "unpack";

      if ($endianness === true) {  // big-endian
          $i = $f("n", $i);
      } else if ($endianness === false) {  // little-endian
          $i = $f("v", $i);
      } else if ($endianness === null) {  // machine byte order
          $i = $f("S", $i);
      }

      return is_array($i) ? $i[1] : $i;
    }

    public static function int32($i)
    {
      return is_int($i) ? pack("l", $i) : unpack("l", $i)[1];
    }

    public static function uInt32($i, $endianness = null)
    {
      $f = is_int($i) ? "pack" : "unpack";

      if ($endianness === true) {  // big-endian
          $i = $f("N", $i);
      } else if ($endianness === false) {  // little-endian
          $i = $f("V", $i);
      } else if ($endianness === null) {  // machine byte order
          $i = $f("L", $i);
      }

      return is_array($i) ? $i[1] : $i;
    }

    public static function int64($i)
    {
      return is_int($i) ? pack("q", $i) : unpack("q", $i)[1];
    }

    public static function uInt64($i, $endianness = null)
    {
      $f = is_int($i) ? "pack" : "unpack";

      if ($endianness === true) {  // big-endian
          $i = $f("J", $i);
      } else if ($endianness === false) {  // little-endian
          $i = $f("P", $i);
      } else if ($endianness === null) {  // machine byte order
          $i = $f("Q", $i);
      }

      return is_array($i) ? $i[1] : $i;
    }
}

# profile related functions

define('PROF_CONN_IP_HDR_LEN', 16);
define('PROF_CONN_PORT_HDR_LEN', 8);
define('PROF_CONN_IP_PORT_HDR_LEN', 24);
define('MSG_HDR_LEN', 16);

define('MSG_T_IPPORT', 0);
define('MSG_T_PROFILE', 1);
define('MSG_T_PMAP', 2);
define('MSG_T_PROF_ID', 3);
define('MSG_T_MARKER', 4);
define('MSG_T_DATA', 5);

define('HASH_LEN', 16);

$FEBE_REQUEST_ID = hex2bin("42450000000000000000000000000000");

function PROF_CONN_PMAP_HDR_LEN($n_prof)
{
  $len = 2 + ($n_prof * 4);
  return $len;
}

function PROF_CONN_VAR_LEN_BUF_LEN($n_req)
{
  $len = ($n_req * 2) + (($n_req * 8) * 3);
  return $len;
}

function PROF_CONN_LEN($n_req)
{
  $len = (2 * 2) + PROF_CONN_VAR_LEN_BUF_LEN($n_req);
  return $len;
}

function get_data_from_file($filename, $size) {
  # $fstr = "bench_data/$size" . "b.txt";
  $fstrf = $filename;
  $buf = file_get_contents($fstr, NULL, NULL, 0, $size);
  echo "$buf";
  return $buf;
}

# get file from remote memcached server
function get_data_from_memcached($filename, $seg_id, $server_idx) {
  global $MEMCACHE_SERVER_ARR, $MEMCACHE_PORT;
  global $debug, $logfile;
  global $cfg, $use_memc;
  $num_pub = 1;
  $num_priv = 0;

  $memcache = new Memcache;
  $ret = $memcache->connect($MEMCACHE_SERVER_ARR[$server_idx], $MEMCACHE_PORT);
  if ($debug == 1) {
    $currtime = exec('cat /proc/uptime | cut -d\' \' -f1');
    $currtime_ns = exec('date +%s%N');
    file_put_contents($logfile, "[$currtime] $currtime_ns conn ret: $ret\n",
      FILE_APPEND|LOCK_EX);
  }

  if ($ret != 1)
    return get_data_from_filepath($filename);

  /*
  if ($cfg) {
    if (isset($use_memc) && $use_memc == 1) {
      do_marker_febe($debug, $sme_ctrl_fd);
    }
  }
   */

  $public_input_hash = md5($seg_id, True);
  $public_ihash_str = bin2hex($public_input_hash);

  $memc_filename = substr($filename, 1);

  $getkey = "$num_pub,$num_priv,$public_ihash_str,$memc_filename";

  $buf = $memcache->get($getkey);
  echo "$buf";

  if ($debug == 1) {
    $currtime = exec('cat /proc/uptime | cut -d\' \' -f1');
    $currtime_ns = exec('date +%s%N');
    file_put_contents($logfile,
      "[$currtime] $currtime_ns $filename, $getkey, ret: $ret, buf len: "
      . strlen($buf) . "\n", FILE_APPEND|LOCK_EX);
  }
  return $buf;
}


# get file from filepath
function get_data_from_filepath($filename) {
  $buf = file_get_contents($filename);
  echo "$buf";
  return $buf;
}

# get file size
function get_file_size($filename) {
  return filesize($filename);
}

# get file fragment of size $size (starting from $offset)
function get_data_from_file_with_range($filename, $offset, $size) {
  # $fstr = "bench_data/$size" . "b.txt";
  $fstrf = $filename;
  $buf = file_get_contents($fstr, NULL, NULL, $offset, $size);
  echo "$buf";
  return $buf;
}

# get fragment range from the HTTP Request Header, and check its validity
function get_and_check_media_range()
{
  if (isset($_SERVER['HTTP_RANGE'])) {
    if (!preg_match('^bytes=\d*-\d*(,\d*-\d*)*$', $_SERVER['HTTP_RANGE'])) {
        header('HTTP/1.1 416 Requested Range Not Satisfiable');
        header('Content-Range: bytes */' . filelength); // Required in 416.
        return false;
    }
    $offsets = array();
    $i = 0;
    $ranges = explode(',', substr($_SERVER['HTTP_RANGE'], 6));
    foreach ($ranges as $range) {
        $parts = explode('-', $range);
        $start = $parts[0]; // If this is empty, this should be 0.
        $end = $parts[1]; // If this is empty or greater than than filelength - 1, this should be filelength - 1.
        if ($start > $end) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            header('Content-Range: bytes */' . filelength); // Required in 416.
            return false;
        }
        $offsets[$i] = $start;
        $i = $i + 1;
    }
    return $offsets;
  }
  return false;
}

# get fragment range from the HTTP Request Header
function get_media_range()
{
  return preg_match('/^bytes=((\d*-\d*,? ?)+)$/', @$_SERVER['HTTP_RANGE'], $matches) ? $matches[1] : array();
}

# check fragment range validity, if exists (defined in URL as ($rs - $re))
function check_url_range()
{
  if (isset($_REQUEST["rs"]) and isset($_REQUEST["re"])) {
    if ($_REQUEST["rs"] > $_REQUEST["re"]) {
        header('HTTP/1.1 416 Requested Range Not Satisfiable');
        header('Content-Range: bytes */' . filelength); // Required in 416.
        return false;
    }
    return true;
  }
  return false;
}

$SME_REQUEST_TIMESTAMP = -1;
$SME_REQUEST_ID = -1;
$public_input_hash = array();
$private_input_hash = array();


function set_request_id($pubinput, $privinput) {
  global $SME_REQUEST_TIMESTAMP;
  global $SME_REQUEST_ID;
  global $public_input_hash;
  global $private_input_hash;
  $num_pub = count($pubinput);
  $num_priv = count($privinput);

  #print_r($pubinput);
  #print_r($privinput);
  for ($i = 0; $i < $num_pub; $i++) {
    #$pubhash[] = "1234567890abcdef";
    $public_input_hash[] = md5($pubinput[$i], True);
    #$pubhash[] = md5($client_public_input, True);
  }

  for ($i = 0; $i < $num_priv; $i++) {
    $private_input_hash[] = md5($privinput[$i], True);
  }

  #print_r($public_input_hash);

  // request ID
  $reqID1 = str_repeat('0', HASH_LEN/2);
  $reqID2 = str_repeat('0', HASH_LEN/2);

  if ($num_pub > 0)
    $reqID1 = substr($public_input_hash[0], 0, HASH_LEN/2);
  if ($num_priv > 0)
    $reqID2 = substr($private_input_hash[$num_priv-1], 0, HASH_LEN/2);

  $SME_REQUEST_TIMESTAMP = intval(microtime(True)*1000000000);

  $SME_REQUEST_ID = md5($SME_REQUEST_TIMESTAMP . $reqID1 . $reqID2, True);
  #to make sure the request timestamp will be used if enforcement is not running

  return $SME_REQUEST_ID;
}

function create_tmp_profile_marker(&$buf, &$buf_len, $src_ip, $src_port,
  $in_ip, $in_port, $out_ip, $out_port, $dst_ip, $dst_port,
  $num_pub, $num_priv, $pubhash, $privhash, $SME_REQUEST_ID)
{
  global $debug;
  global $logfile;
  global $SME_REQUEST_TIMESTAMP;

  $prof_buf = "";
  if ($src_ip != "")
    $prof_buf .= int_helper::uInt32(ip2long($src_ip), true);
  else
    $prof_buf .= int_helper::uInt32(ip2long("0.0.0.0"), true);

  if ($in_ip != "")
    $prof_buf .= int_helper::uInt32(ip2long($in_ip), true);
  else
    $prof_buf .= int_helper::uInt32(ip2long("0.0.0.0"), true);

  if ($out_ip != "")
    $prof_buf .= int_helper::uInt32(ip2long($out_ip), true);
  else
    $prof_buf .= int_helper::uInt32(ip2long("0.0.0.0"), true);

  if ($dst_ip != "")
    $prof_buf .= int_helper::uInt32(ip2long($dst_ip), true);
  else
    $prof_buf .= int_helper::uInt32(ip2long("0.0.0.0"), true);

  $prof_buf .= int_helper::uInt16(intval($src_port), true);
  $prof_buf .= int_helper::uInt16(intval($in_port), true);
  $prof_buf .= int_helper::uInt16(intval($out_port), true);
  $prof_buf .= int_helper::uInt16(intval($dst_port), true);

  $prof_buf .= int_helper::uInt32(intval($num_pub), false);
  $prof_buf .= int_helper::uInt32(intval($num_priv), false);

  $prof_buf .=  $SME_REQUEST_ID;
  #print_r($SME_REQUEST_ID);

  // marker timestamp, may be used by the kernel
  $prof_buf .=  int_helper::uInt64($SME_REQUEST_TIMESTAMP, false);

  // #pub hashes
  for ($i = 0; $i < $num_pub; $i++) {
    #print_r($pubhash[$i]);
    $prof_buf .= $pubhash[$i];
  }

  // #priv hashes
  for ($i = 0; $i < $num_priv; $i++) {
    #print_r($privhash[$i]);
    $prof_buf .= $privhash[$i];
  }

  $plen = PROF_CONN_IP_PORT_HDR_LEN + 2*4 + HASH_LEN + 8
    + ($num_pub + $num_priv) * HASH_LEN;

  $msg_type = MSG_T_MARKER;

  $msg = "";
  $msg .= int_helper::uInt64($msg_type, false);
  $msg .= int_helper::uInt64($plen, false);

  $buf = $msg . $prof_buf;
  $buf_len = MSG_HDR_LEN + $plen;

  if (isset($debug) && $debug) {
    $dstr = "";
    $dstr .= "msg type: $msg_type";
    $dstr .= ", len: $plen";
    $dstr .= ", src ip: $src_ip";
    $dstr .= ", src port: $src_port";
    $dstr .= ", in ip: $in_ip";
    $dstr .= ", in port: $in_port";
    $dstr .= ", out ip: $out_ip";
    $dstr .= ", out port: $out_port";
    $dstr .= ", dst ip: $dst_ip";
    $dstr .= ", dst port: $dst_port";
    $dstr .= ", num pub: $num_pub";
    $dstr .= ", num priv: $num_priv";
    if($num_pub > 0){
      $pubhash1 = bin2hex($pubhash[0]);
      $dstr .= ", pub_hash1: $pubhash1";
    }
    $ts = bin2hex(int_helper::uInt64($SME_REQUEST_TIMESTAMP, false));
    $dstr .= ", TS $ts TIMESTAMP VALUE: $SME_REQUEST_TIMESTAMP";
    $reqid = bin2hex($SME_REQUEST_ID);
    $dstr .= ", REQUEST_ID: $reqid";
    $hstr = bin2hex($prof_buf);
    $hlen = strlen($hstr);

    file_put_contents($logfile,
      "BINARY[$hlen] === $hstr\nSTRING === $dstr\n", LOCK_EX|FILE_APPEND);
  }

  /*
  if (isset($debug) && $debug) {
    file_put_contents("udp.txt",
      "SRC: $src_ip, $src_port, IN: $in_ip, $in_port\n" .
      "OUT: $out_ip, $out_port, DST: $dst_ip, $dst_port\n" .
      "plen: $plen, buflen: $buf_len\n",
      FILE_APPEND|LOCK_EX);
  }
   */
}

function do_marker($debug, $fd, $seg_id) {
  global $client_ip, $client_port;
  global $server_ip, $server_port;
  global $dummy_ip, $dummy_port;

  global $public_input_hash;
  global $private_input_hash;
  
  $num_pub = 1;
  $num_priv = 0;
  $pub_input = array();
  $priv_input = array();

  for ($i = 0; $i < $num_priv; $i++)
    $priv_input[$i] = "1234567890abcdef";

  for ($i = 0; $i < $num_pub; $i++)
    $pub_input[$i] = "$seg_id";

# Decomment to store hash mapping to file
#  for ($i = 0; $i < $num_pub; $i++) {
#    $id = $pub_input[$i];
#    $hashid = md5($id, True);
#    $hexhashid = bin2hex($hashid);
#    file_put_contents("hash.txt", "\nCluster-Segment: $id, Hash: $hexhashid", LOCK_EX|FILE_APPEND);
#  }

  $reqID = set_request_id($pub_input, $priv_input);

  create_tmp_profile_marker($pbuf, $plen, $client_ip, $client_port,
    $server_ip, $server_port, $server_ip, $server_port, $client_ip, $client_port,
    $num_pub, $num_priv, $public_input_hash, $private_input_hash, $reqID);

  $ret = dev_ioctl($fd, 0, $pbuf, $plen);

  /*
  $ret = socket_sendto($sock, $pbuf, $plen, 0, $dummy_ip, $dummy_port);
  if (isset($debug) && $debug == 0) {
    $ret = socket_recvfrom($sock, $ackbuf, 8, 0, $dummy_ip, $dummy_port);
    file_put_contents("msg.txt",
      "sock: $sock, RCVD ACK, ret: $ret\n", FILE_APPEND);
  }
   */
}

function do_marker_febe($debug, $fd, $server_idx)
{
  global $use_memc;
  global $MEMCACHE_SERVER_ARR, $MEMCACHE_PORT;
  global $FEBE_REQUEST_ID;
  global $client_ip, $client_port;
  global $server_ip, $server_port;
  global $dummy_ip, $dummy_port;

  global $public_input_hash;
  global $private_input_hash;

  global $logfile;

  $reqID = $FEBE_REQUEST_ID;
  $num_pub = 1;
  $num_priv = 0;

  for ($i = 0; $i < $num_pub; $i++)
    $public_input_hash[$i] = $FEBE_REQUEST_ID;

  create_tmp_profile_marker($pbuf, $plen, $client_ip, $client_port,
    $server_ip, $server_port, $server_ip, 0,
    $MEMCACHE_SERVER_ARR[$server_idx], $MEMCACHE_PORT,
    $num_pub, $num_priv, $public_input_hash, $private_input_hash, $reqID);

  $ret = 0;
  $ret = dev_ioctl($fd, 0, $pbuf, $plen);

  if ($debug == 1) {
    file_put_contents($logfile,
      "req: " . bin2hex($FEBE_REQUEST_ID) . " ioctl ret: $ret\n",
      FILE_APPEND|LOCK_EX);
  }
}


function wait_for_marker_ack($debug, $sock)
{
  global $dummy_ip, $dummy_port;

  if (isset($debug) && $debug == 0) {
    $ret = socket_recvfrom($sock, $ackbuf, 8, 0, $dummy_ip, $dummy_port);
    #file_put_contents("msg.txt", "RCVD ACK, ret: $ret\n", FILE_APPEND);
  }
}

$size = 0;
$seg_id = 0;
$cfg = 0;

if (php_sapi_name() === "cli") {
  parse_str(implode("&", array_slice($argv, 1)), $_REQUEST);
  fclose(STDOUT);
  $STDOUT = fopen("/dev/null", "wb");
}

# get configuration
if (isset($_REQUEST["c"]))
  $cfg = $_REQUEST["c"];
# get debug mode
if (isset($_REQUEST["d"]))
  $debug = $_REQUEST["d"];
# seed for selecting backend server (same seed for all segs of a single client's videos)
if (isset($_REQUEST["s"]))
  $seed = $_REQUEST["s"];
# get filename
if (isset($_REQUEST["f"])) {
  $filename = $_REQUEST["f"];
//  $size = get_file_size($filename);
}
# get clusterid_segmentid
if (isset($_REQUEST["i"])) {
  $seg_id = $_REQUEST["i"];
}
# dataset in local file system or remote memcached server
if (isset($_REQUEST["m"]))
  $use_memc = $_REQUEST["m"];

srand($seed);
$server_idx = random_int(0, 1);


if (php_sapi_name() === "cli") {
  $server_ip = "139.19.171.101";
  $server_port = 2230;
  $client_ip = "139.19.168.173";
  $client_port = 13424;
} else {
  # set server ip:port, client ip:port
  get_local_ip_port($server_ip, $server_port);
  get_remote_ip_port($client_ip, $client_port);
}

if ($debug == 1) {
  $dbgstr = "";
  $dbgstr .= "server: $server_ip, $server_port\t";
  $dbgstr .= "client: $client_ip, $client_port\n";
  $dbgstr .= "req cfg: $cfg, debug: $debug, ";
  $dbgstr .= "memcache: $use_memc, server idx: $server_idx, ";
  $dbgstr .= "seg id: $seg_id, hash: " . bin2hex(md5($seg_id, True)) . "\n";
  $dbgstr .= "file: $filename, " . substr($filename, 1) . "\n";
  file_put_contents($logfile, $dbgstr, FILE_APPEND|LOCK_EX);
//  do_marker_febe($debug, $sme_ctrl_fd);
}

if ($cfg) {
  // $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
  // $sock = do_marker($debug, $sock, $seg_id);

  $sme_ctrl_fd = dev_open($sme_ctrl_dev);
  do_marker($debug, $sme_ctrl_fd, $seg_id);

  /*
  if (isset($use_memc) && $use_memc == 1) {
    do_marker_febe($debug, $sme_ctrl_fd, $server_idx);
  }
   */
}

if (php_sapi_name() === "cli")
  $starttime = exec('date +%s%N');

if (isset($use_memc) && $use_memc == 1)
  $buf = get_data_from_memcached($filename, $seg_id, $server_idx);
else
  $buf = get_data_from_filepath($filename);

if (php_sapi_name() === "cli")
  $endtime = exec('date +%s%N');

if ($cfg) {
  // wait_for_marker_ack($debug, $sock);
  dev_close($sme_ctrl_fd);
}

unset($buf);

if (php_sapi_name() === "cli") {
  $totaltime = $endtime - $starttime;
  $totaltime_us = ($totaltime/1000.0);
  fprintf(STDERR, "%-50s %-12.3f us servidx %d\n", $filename, $totaltime_us, $server_idx);
}
