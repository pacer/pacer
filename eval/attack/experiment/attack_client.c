// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/syscall.h>
#include <errno.h>
#include <argp.h>

#if _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#endif

#define ASYNC 1

#define PORT	 8080
#define MTU_PAYLOAD 1472
#define DEFAULT_PAYLOAD (7 * sizeof(long int))
#define MTU     1514
#define EXTRA_PACKETS 100

/*
 * Header lengths
 * UDP:  8 bytes
 * IP:  20 bytes
 * ETH: 14 bytes
 */
#define HDR_OVERHEAD  42
/*
 * 8 bytes preamble + 4 bytes CRC trailer +
 * 12 bytes minimum inter-packet gap between ethernet frames
 */
#define ETH_OVERHEAD  24
#define DEFAULT_BYTES_ON_WIRE (DEFAULT_PAYLOAD + HDR_OVERHEAD)

#define KB  (1024)
#define MB  (1024 * KB)
#define GB  (1024 * MB)

#define Kb  (1000)
#define Mb  (1000 * Kb)
#define Gb  (1000 * Mb)

#define ms_per_s  (1000)
#define us_per_s  (1000 * ms_per_s)
#define ns_per_s  (1000 * us_per_s)

#define PRINT_LOG 1

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))


struct sockaddr_in	 servaddr;
struct sockaddr_in	 servaddr1;
int experiment_time;
int sleep_time_us = 13;
int sleep_time_ns = 0;
int payload_size = DEFAULT_PAYLOAD;
int bytes_on_wire = DEFAULT_BYTES_ON_WIRE;
struct timespec first_time_ns;

enum {
  ARG_IP = 1,
  ARG_DELAY,
  ARG_BANDWIDTH,
  MAX_ARGS
};

const char *arg_prog_version = "UDP client 1.0";
const char *arg_prog_bug_address = "xxx";
static char doc[] = "yyy";
static char args_doc[] = "--serv_ip x.x.x.x --serv_port 8080 --delay 5"
                         " --pkt_rate 78433 --payload_size 40"
                         " --outfile /local/sme/exp/attack/udp/tstamps.txt";

static struct argp_option options[] = {
  {"serv_ip", 'i', "serv_ip", 0, "IP address of the UDP server to connect to"},
  {"serv_port", 'p', "serv_port", 0, "Port of the UDP server to connect to"},
  {"delay", 't', "delay", 0, "Inter-request delay"},
  {"pkt_rate", 'r', "pkt_rate", 0, "attacker's packet rate"},
  {"payload_size", 's', "payload_size", 0, "attacker's UDP payload size (do not count headers)"},
  {"snd_wnd", 'w', "snd_wnd", 0, "max #packets that can be in flight (= BDP + n)"},
  {"outfile", 'o', "outfile", 0, "Output file path"},
  { 0 }
};

struct arguments {
  char *serv_ip;
  int serv_port;
  int delay;
  float pkt_rate;
  int payload_size;
  int snd_wnd;
  char *outfile;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  char *err = NULL;
  switch (key) {
    case 'i': arguments->serv_ip = arg;
      break;
    case 'p': arguments->serv_port = (int) strtol(arg, &err, 10);
      if (err == arg) {
        printf("[%s:%d] conversion err %s\n", __func__, __LINE__, err);
        return -EINVAL;
      }
      break;
    case 't':
      arguments->delay = (int) strtol(arg, &err, 10);
      if (err == arg) {
        printf("[%s:%d] conversion err %s\n", __func__, __LINE__, err);
        return -EINVAL;
      }
      break;
    case 'r': arguments->pkt_rate = (float) strtod(arg, &err);
      if (err == arg) {
        printf("[%s:%d] conversion err %s\n", __func__, __LINE__, err);
        return -EINVAL;
      }
      break;
    case 's':
      arguments->payload_size = (int) strtol(arg, &err, 10);
      if (err == arg) {
        printf("[%s:%d] conversion err %s\n", __func__, __LINE__, err);
        return -EINVAL;
      }
      break;
    case 'w':
      arguments->snd_wnd = (int) strtol(arg, &err, 10);
      if (err == arg) {
        printf("[%s:%d] conversion err %s\n", __func__, __LINE__, err);
        return -EINVAL;
      }
      break;
    case 'o': arguments->outfile = arg;
      break;
    case ARGP_KEY_ARG: return 0;
    case ARGP_KEY_END:
      if (state->argc < (2*MAX_ARGS - 1))
        argp_usage(state);
      break;
    default: return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

static struct argp argp = {
  options,
  parse_opt,
  args_doc,
  doc,
  0, 0, 0
};

pid_t gettid(void)
{
  return syscall(SYS_gettid);
}

/*
 * signal handler for kill signals
 */
int flag = 0;

void sig_handler(int signo)
{

  flag = 1;
}

static __inline__ unsigned long long rdtsc(void)
{
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

double time_diff_us(struct timeval x, struct timeval y)
{
  double x_us , y_us , diff;

  x_us = (double)x.tv_sec*1000000 + (double)x.tv_usec;
  y_us = (double)y.tv_sec*1000000 + (double)y.tv_usec;

  diff = (double)y_us - (double)x_us;

  return diff;
}

uint64_t time_diff_ns(struct timespec x, struct timespec y)
{
#if DBG
  uint64_t x_ns = (x.tv_sec * ns_per_s) + x.tv_nsec;
  uint64_t y_ns = (y.tv_sec * ns_per_s) + y.tv_nsec;
  uint64_t diff = y_ns - x_ns;
  return diff;
#else
  return (((y.tv_sec * ns_per_s) + y.tv_nsec) - ((x.tv_sec * ns_per_s) + x.tv_nsec));
#endif
}

void sleep_ms(int milliseconds)
{
#if _POSIX_C_SOURCE >= 199309L
  struct timespec ts;
  ts.tv_sec = milliseconds / 1000;
  ts.tv_nsec = (milliseconds % 1000) * 1000000;
  nanosleep(&ts, NULL);
#else
  usleep(milliseconds * 1000);
#endif
}
void sleep_us(int microseconds)
{
#if _POSIX_C_SOURCE >= 199309L
  struct timespec ts;
  ts.tv_sec = microseconds / 1000000;
  ts.tv_nsec = (microseconds % 1000000) * 1000;
  nanosleep(&ts, NULL);
#else
  usleep(microseconds);
#endif
}
void sleep_ns(int nanoseconds)
{
#if _POSIX_C_SOURCE >= 199309L
  struct timespec ts;
  ts.tv_sec = nanoseconds / 1000000000;
  ts.tv_nsec = (nanoseconds % 1000000000);
  nanosleep(&ts, NULL);
#else
  usleep(nanoseconds/1000.0);
#endif
}

void spin_sleep_us(struct timeval tstart, unsigned long us)
{
  struct timeval tnow;
  gettimeofday(&tnow , NULL);
  while (time_diff_us(tstart, tnow) < us) {
    gettimeofday(&tnow , NULL);
  }
}

void spin_sleep_ns(struct timespec tstart_ns, int ns)
{
  struct timespec tnow_ns;
  clock_gettime(CLOCK_REALTIME, &tnow_ns);
  while (time_diff_ns(tstart_ns, tnow_ns) < sleep_time_ns) {
    clock_gettime(CLOCK_REALTIME, &tnow_ns);
  }
//  printf("start %10ld.%09ld now %10ld.%09ld diff %ld\n", tstart_ns.tv_sec, tstart_ns.tv_nsec,
//      tnow_ns.tv_sec, tnow_ns.tv_nsec, time_diff_ns(tstart_ns, tnow_ns));
}

void copy_ts(struct timespec *dst, struct timespec *src)
{
  dst->tv_sec = src->tv_sec;
  dst->tv_nsec = src->tv_nsec;
}

unsigned long snd_wnd = 0;
unsigned long snd_nxt = 0;
unsigned long snd_una = 0;
unsigned long snd_max = 0;

typedef struct clnt_thread_arg {
  int sockfd;
  float pkt_rate;
  char *outfile;
  FILE *outfile_fp;
} clnt_thread_arg_t;

#if ASYNC
void *
send_msg(void *arg_p)
{
  clnt_thread_arg_t *arg = (clnt_thread_arg_t *) arg_p;
  int sock = arg->sockfd;
  char buffer[payload_size];
  struct timespec tnow_ns;
  long int count = 0;
  snd_nxt = count;

  memset(buffer, 0, payload_size);

  while (!flag) {
    if (snd_nxt > snd_max)
        continue;

    clock_gettime(CLOCK_REALTIME, &tnow_ns);

    ((long int *) buffer)[0] = count;
    ((long int *) buffer)[1] = tnow_ns.tv_sec;
    ((long int *) buffer)[2] = tnow_ns.tv_nsec;

    sendto(sock, buffer, payload_size, MSG_CONFIRM,
        (const struct sockaddr *) &servaddr, sizeof(servaddr));

    if (count == 0) {
      copy_ts(&first_time_ns, &tnow_ns);
    }

    count++;
    snd_nxt++;

//    printf("%10ld.%09ld snd_nxt %ld gap %ld\n",
//        tnow_ns.tv_sec, tnow_ns.tv_nsec, snd_nxt, snd_nxt - snd_una);

    spin_sleep_ns(tnow_ns, sleep_time_ns);
  }

#if PRINT_LOG
  fprintf(arg->outfile_fp, "#UDP messages sent = %ld\n", count);
#endif

  return 0;
}

void *
recv_msg(void *arg_p)
{
  clnt_thread_arg_t *arg = (clnt_thread_arg_t *) arg_p;
  int sock = arg->sockfd;

  int ret = 0;
  unsigned int len;
  char buffer[payload_size];

  //local view
  struct timespec tnow_ns;
#if PRINT_LOG
  struct timespec clnt_snd_time_ns;
  struct timespec serv_rcv_time_ns;
  struct timespec serv_snd_time_ns;
  struct timespec prev_snd_time_ns;
  struct timespec prev_rcv_time_ns;
  struct timespec prev_serv_rcv_time_ns;
  struct timespec first_snd_time_ns;
  struct timespec first_serv_rcv_time_ns;
//  uint64_t inter_snd_time_ns;
//  uint64_t inter_rcv_time_ns;
//  uint64_t inter_serv_rcv_time_ns;
  uint64_t snd_delay_ns;
  uint64_t rcv_delay_ns;
  uint64_t srv_delay_ns;
//  uint64_t rtt_ns;
  uint64_t rel_tnow_ns;
  struct timespec rel_ts;
  uint64_t rel_serv_rcv_time_ns;
  struct timespec rel_serv_rcv_ts;

  int curr_seq_number;
  int prev_seq_number;
#endif

  long count = 0;

  struct timespec ts;
  ts.tv_sec = experiment_time + 5;

  sigset_t sigmask;
  sigemptyset(&sigmask);
  sigaddset(&sigmask, SIGINT);

  clock_gettime(CLOCK_REALTIME, &tnow_ns);

  fd_set socks;
  FD_ZERO(&socks);
  FD_SET(sock, &socks);

  memset(buffer, 0, payload_size);

  while (!flag) {
    if (snd_una >= snd_nxt)
      continue;

    ret = pselect(sock + 1, &socks, NULL, NULL, &ts, &sigmask);
    // timeout
    if (ret == 0)
      continue;

    // interrupt or error
    if (ret < 0) {
      if (ret == -EINTR) // signal
        break;

      printf("%d select ret %d flag %d\n", gettid(), ret, flag);
      continue;
    }

    ret = recvfrom(sock, buffer, payload_size, MSG_WAITALL,
        (struct sockaddr *) &servaddr1, &len);
    clock_gettime(CLOCK_REALTIME, &tnow_ns);

    snd_una = curr_seq_number = ((long int *) buffer)[0];
    snd_max = snd_una + snd_wnd;

#if PRINT_LOG
    clnt_snd_time_ns.tv_sec = ((long int *) buffer)[1];
    clnt_snd_time_ns.tv_nsec = ((long int *) buffer)[2];
    serv_rcv_time_ns.tv_sec = ((long int *) buffer)[3];
    serv_rcv_time_ns.tv_nsec = ((long int *) buffer)[4];
    serv_snd_time_ns.tv_sec = ((long int *) buffer)[5];
    serv_snd_time_ns.tv_nsec = ((long int *) buffer)[6];

    if (count == 0) {
      prev_seq_number = curr_seq_number;
      copy_ts(&prev_snd_time_ns, &clnt_snd_time_ns);
      copy_ts(&prev_rcv_time_ns, &tnow_ns);
      copy_ts(&prev_serv_rcv_time_ns, &serv_rcv_time_ns);
      copy_ts(&first_snd_time_ns, &clnt_snd_time_ns);
      copy_ts(&first_serv_rcv_time_ns, &serv_rcv_time_ns);
      fprintf(arg->outfile_fp,
          "#first clnt sndtime: %10ld.%09ld\tserv rcvtime: %10ld.%09ld\n",
          first_snd_time_ns.tv_sec, first_snd_time_ns.tv_nsec,
          first_serv_rcv_time_ns.tv_sec, first_serv_rcv_time_ns.tv_nsec);
    }

//    inter_snd_time_ns = time_diff_ns(prev_snd_time_ns, clnt_snd_time_ns);
//    inter_rcv_time_ns = time_diff_ns(prev_rcv_time_ns, tnow_ns);
//    inter_serv_rcv_time_ns = time_diff_ns(prev_serv_rcv_time_ns, serv_rcv_time_ns);
    snd_delay_ns = time_diff_ns(clnt_snd_time_ns, serv_rcv_time_ns);
    rcv_delay_ns = time_diff_ns(serv_snd_time_ns, tnow_ns);
    srv_delay_ns = time_diff_ns(serv_rcv_time_ns, serv_snd_time_ns);
//    rtt_ns = time_diff_ns(clnt_snd_time_ns, tnow_ns);
//    rel_tnow_ns = time_diff_ns(first_time_ns, clnt_snd_time_ns);
    rel_tnow_ns = time_diff_ns(first_snd_time_ns, clnt_snd_time_ns);
    rel_ts.tv_sec = rel_tnow_ns / ns_per_s;
    rel_ts.tv_nsec = rel_tnow_ns % ns_per_s;
    rel_serv_rcv_time_ns = time_diff_ns(first_serv_rcv_time_ns, serv_rcv_time_ns);
    rel_serv_rcv_ts.tv_sec = rel_serv_rcv_time_ns / ns_per_s;
    rel_serv_rcv_ts.tv_nsec = rel_serv_rcv_time_ns % ns_per_s;

    fprintf(arg->outfile_fp,
        "%2ld.%09ld\t%2ld.%09ld\t"
        "%3d\t%ld\t%ld\t%ld\t%ld\t%ld\n",
        rel_ts.tv_sec, rel_ts.tv_nsec,
//        clnt_snd_time_ns.tv_sec, clnt_snd_time_ns.tv_nsec,
        rel_serv_rcv_ts.tv_sec, rel_serv_rcv_ts.tv_nsec,
//        serv_rcv_time_ns.tv_sec, serv_rcv_time_ns.tv_nsec,
//        inter_snd_time_ns,
//        inter_rcv_time_ns,
        curr_seq_number - prev_seq_number,
//        rtt_ns,
        snd_delay_ns, srv_delay_ns, rcv_delay_ns,
//        inter_serv_rcv_time_ns,
        snd_nxt, (snd_nxt - snd_una));
#endif

    count++;

#if PRINT_LOG
    prev_seq_number = curr_seq_number;
    copy_ts(&prev_snd_time_ns, &clnt_snd_time_ns);
    copy_ts(&prev_rcv_time_ns, &tnow_ns);
    copy_ts(&prev_serv_rcv_time_ns, &serv_rcv_time_ns);
#endif
  }

  double exp_time_ns = time_diff_ns(first_time_ns, tnow_ns);
  double exp_time_s = exp_time_ns / ns_per_s;
  double pkt_rate = (double) count / exp_time_s;
  double xput = (((double) count * payload_size)/GB) / exp_time_s;
  double xput_gbps = (((double) count * bytes_on_wire * 8)/Gb) / exp_time_s;

  /*
   * set exp_time_s to 0 after doing the above divisions
   * to avoid div-by-zero exception
   */
  if (count == 0)
    exp_time_s = 0;

  fprintf(arg->outfile_fp, "#Client side statistics\n");
  fprintf(arg->outfile_fp, "#Total time for experiment = %f s\n", exp_time_s);
  fprintf(arg->outfile_fp, "#Total packets recieved = %ld\n", count);
  fprintf(arg->outfile_fp, "#Packet rate (theoretical %d) = %9.3f pkts/s\n",
      (int) arg->pkt_rate, pkt_rate);
  fprintf(arg->outfile_fp, "#Inter-pkt interval (theoretical %d) = %d ns\n",
      (int) ((ns_per_s / arg->pkt_rate) + 1), sleep_time_ns);
  fprintf(arg->outfile_fp, "#Total MBs recieved (payload size %d) = %f\n",
      payload_size, ((double) count * payload_size)/MB);
  fprintf(arg->outfile_fp, "#Total xput GB/s (1024)= %f\n", xput);
  fprintf(arg->outfile_fp, "#Total xput Gbps (1000)= %f\n", xput_gbps);

  return 0;
}

void
run_client_async(void *sockfd, char *outfile, float pkt_rate)
{
  pthread_t tids[2];
  pthread_attr_t attr;
  pthread_attr_init(&attr);

  FILE *fp = fopen(outfile, "a+");
  fprintf(fp, "#rel-sndts\trel-srv-rcvts"
      "\tseqdiff\tsnd-delay\tsrv-delay\trcv-delay\tsndnxt\tinflight\n");

  clnt_thread_arg_t arg = {
    .sockfd = *(int *) sockfd,
    .pkt_rate = pkt_rate,
    .outfile = outfile,
    .outfile_fp = fp,
  };

  pthread_create(&tids[1], &attr, recv_msg, (void *) &arg);
  pthread_create(&tids[0], &attr, send_msg, (void *) &arg);

  pthread_join(tids[0], NULL);
  pthread_join(tids[1], NULL);

  fclose(arg.outfile_fp);
}
#else
void run_client_sync(int sockfd)
{
  int n, len;
  char buffer[DEFAULT_PAYLOAD];
  char *hello = "Hello from client";

  while (1) {
    sendto(sockfd, (const char *) buffer, strlen(buffer), MSG_CONFIRM,
        (const struct sockaddr *) &servaddr, sizeof(servaddr));
    printf("Hello message sent.\n");

    n = recvfrom(sockfd, (char *) buffer, DEFAULT_PAYLOAD, MSG_WAITALL,
        (struct sockaddr *) &servaddr, &len);
    buffer[n] = '\0';
    printf("Server : %s\n", buffer);
  }


}
#endif

int
main(int argc, char *argv[])
{
  char *serv_ip;
  int port = -1;
  struct arguments arguments;

  int sockfd;
  char *outfile = NULL;

  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  serv_ip = arguments.serv_ip;
  port = arguments.serv_port;
  experiment_time = arguments.delay;
  outfile = arguments.outfile;

  snd_wnd = arguments.snd_wnd;
  snd_max = snd_wnd;
//  if (arguments.pkt_rate > 0)
//    sleep_time_ns = (int) ((ns_per_s / arguments.pkt_rate) + 1);
//  else
    sleep_time_ns = 1;

  if (!(arguments.payload_size < DEFAULT_PAYLOAD)) {
    payload_size = arguments.payload_size;
    bytes_on_wire = (payload_size + HDR_OVERHEAD);
  }

#if 0
  printf("IP %s port %d inter-request gap %d packet rate %f payload sz %d sleep time %d\n",
      arguments.serv_ip, arguments.serv_port, arguments.delay,
      arguments.pkt_rate, payload_size, sleep_time_ns);
#endif

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  if (signal(SIGINT, &sig_handler) == SIG_ERR)
      perror("\ncan't catch SIGINT\n");

  struct timeval tv;
  tv.tv_sec=0;
  tv.tv_usec=100000;

  if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
    perror("Error");
    close(sockfd);
    exit(EXIT_FAILURE);
  }

  int rcvbuf_size = ((1024 * 1024 * 1024) - 1);
  if (setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &rcvbuf_size, (socklen_t) sizeof(int)) < 0) {
    perror("Error setting rcvbuf size");
    close(sockfd);
    exit(EXIT_FAILURE);
  }

  int sndbuf_size = 0;
  sndbuf_size = ((1024 * 1024 * 1024) - 1);
  if (setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &sndbuf_size, (socklen_t) sizeof(int)) < 0) {
    perror("Error setting sndbuf size");
    close(sockfd);
    exit(EXIT_FAILURE);
  }

  // Filling server information
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  if (port < 0)
    servaddr.sin_port = htons(PORT);
  else
    servaddr.sin_port = htons(port);

  servaddr.sin_addr.s_addr = INADDR_ANY;

  // Bind the socket with the server address
  if (bind(sockfd, (const struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }	

  inet_pton(AF_INET, serv_ip, &(servaddr.sin_addr));

#if ASYNC
  run_client_async((void *) &sockfd, outfile, arguments.pkt_rate);
#else
  run_client_sync(sockfd);
#endif

  close(sockfd);

  return 0;
}
