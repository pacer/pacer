#!/bin/bash

###################################
## attack.sh
##
## author: aasthakm
##
## script to run attack experiment
##################################

. "cmd.sh"

SEC_TO_NS=1000000000
NS_TO_SEC=0.000000001

## ==== static configurations (rarely change) ====
user="sme"
ruser="root"
rootdir="/local/sme"
workdir="$rootdir/workspace/pacer"
attack_srcdir="$workdir/eval/attack/experiment"

fe_gpacedir="$workdir/frontend/gpace/linux-frontend-4.9.5"
be_gpacedir="$workdir/backend/gpace/linux-backend-4.9.5"
cl_gpacedir="$workdir/client/gpace/linux-client-4.9.5"

fe_hypacedir="$workdir/frontend/hypace/xen-frontend-4.10.0"
be_hypacedir="$workdir/backend/hypace/xen-backend-4.10.0"

fe_gpacekmod="$workdir/frontend/gpace/kmod-frontend"
be_gpacekmod="$workdir/backend/gpace/kmod-backend"
cl_gpacekmod="$workdir/client/gpace/kmod-client"

fe_gpacescripts="$workdir/frontend/gpace/scripts"
be_gpacescripts="$workdir/backend/gpace/scripts"
cl_gpacescripts="$workdir/client/gpace/scripts"

fe_profpacedir="$workdir/frontend/profpace"
be_profpacedir="$workdir/backend/profpace"
cl_profpacedir="$workdir/client/profpace"

daemondir="$rootdir/daemon_profiler"
workdir="$rootdir/workspace/attack"
expdir="$rootdir/exp/attack"
mntexpdir="/mnt/attack"
apachedir="/usr/local/apache2"
webserver="apache"

app_client_port=443
protocol="https"
#wrk2flags="--latency --u_latency"

use_ping=0
is_victim=1
is_attack=1
do_tcpdump=0
do_attack_tcpdump=0
cfg_enf=0

function get_gitinfo()
{
  host=$1
  dir=$2
  cmd="cd $dir; git show --name-status > $dir/gitinfo; "
  cmd=$cmd"echo "" >> $dir/gitinfo; "
  cmd=$cmd"git diff >> $dir/gitinfo; "
  do_cmd $ruser $host "$cmd"
}

MTU=1514
## 8-byte frame preamble + 4-byte CRC trailer + 12-byte min inter-frame gap
frame_overhead=24
## 14-byte ETH + 20-byte IP + 8-byte UDP
udp_overhead=42
## default payload size for UDP traffic (size of 5 long ints)
payload_size=56

#payload_size=1472
ping_i=0.00001

if [[ $# -gt 3 ]]
then
  payload_size=$4
fi

if [[ $# -gt 4 ]]
then
  ping_i=$5
fi

postfix_str=""
if [[ $# -gt 5 ]]
then
  postfix_str=$6
fi


numclients=1

host_ip="139.19.218.13"
victim_ip="139.19.218.101"
victim_client="139.19.218.10"
#attacker_server="139.19.171.102"
#attacker_client="139.19.171.93"
attacker_server="139.19.218.12"
attacker_client="139.19.218.102"
attack_serv_port=2230
contention="NIC"

sizes=( 300 600 900 1200 1500 1800 2100 2400 2700 3000 3300)

for e in ${sizes[*]};
do
	declare "page_$e"=$e;
done

function trunc_logs()
{
  ruser=$1
  host=$2
  cmd=""
  if [[ $webserver == "apache" ]]; then
    cmd=$cmd"echo \"\" > /var/log/apache2/responsetime.log; "
    cmd=$cmd"echo \"\" > $apachedir/logs/responsetime.log; "
    cmd=$cmd"echo \"\" > /var/log/apache2/access.log; "
  else
    cmd=$cmd"echo \"\" > $nginxdir/logs/access.log; "
  fi
  cmd=$cmd"echo \"\" > /local/sme/msg_sent_to_kernel.txt; "
  cmd=$cmd"echo \"\" > /local/sme/stats_5_tuple.txt; "
  cmd=$cmd"echo \"\" > /local/sme/stats_marker_sync.txt; "

  do_cmd $ruser $host "$cmd" 0 0
}

function apache_copy_configs()
{
  ruser=$1
  rhost=$2

  cmd="scp $ruser@$rhost:\"$apachedir/conf/httpd.conf\" $outdir/"
  echo "$cmd"
  eval "$cmd"

  cmd="scp $ruser@$rhost:\"$apachedir/conf/extra/httpd-mpm.conf\" $outdir/"
  echo "$cmd"
  eval "$cmd"
}

function apache_copy_logs()
{
  ruser=$1
  rhost=$2

  cmd=""
  cmd=$cmd"scp $ruser@$rhost:/var/log/apache2/access.log $webservlog; "
  echo "$cmd"
  eval "$cmd"
}

function apache_rmpid()
{
  cmd=""
  cmd=$cmd"rm -f $apachedir/logs/httpd.pid; sleep 1; "
  do_cmd $ruser $rhost "$cmd" 0 0
}

function apache_start()
{
  cmd=""
  cmd=$cmd"sleep 1; $apachedir/bin/apachectl -k start; "
  do_cmd $ruser $rhost "$cmd" 0 0
}

function apache_stop()
{
  cmd=""
  cmd=$cmd"$apachedir/bin/apachectl -k stop; sleep 1; "
  do_cmd $ruser $rhost "$cmd" 0 0
}

function nginx_copy_configs()
{
  ruser=$1
  rhost=$2
  cmd="scp $ruser@$rhost:\"$nginxdir/conf/nginx.conf\" $outdir/"
  echo "$cmd"
  eval "$cmd"

  cmd="scp $ruser@$rhost:\"$nginxdir/conf/fastcgi.conf\" $outdir/"
  echo "$cmd"
  eval "$cmd"
}

function nginx_copy_logs()
{
  ruser=$1
  rhost=$2

  cmd=""
  cmd=$cmd"scp $ruser@$rhost:$nginxdir/logs/access.log $webservlog; "
  echo "$cmd"
  eval "$cmd"
}

function nginx_rmpid()
{
  cmd=""
  cmd=$cmd"rm -f $nginxdir/logs/nginx.pid; sleep 1; "
  do_cmd $ruser $rhost "$cmd" 0 0
}

function nginx_start()
{
  cmd=""
  cmd=$cmd"sleep 1; $nginxdir/sbin/nginx; "
  do_cmd $ruser $rhost "$cmd" 0 0
}

function nginx_stop()
{
  cmd=""
  cmd=$cmd"sleep 1; $nginxdir/sbin/nginx -s stop; sleep 1;"
  do_cmd $ruser $rhost "$cmd" 0 1
}

function webserver_copy_configs()
{
  ruser=$1
  rhost=$2

  if [[ $webserver == "apache" ]]; then
    apache_copy_configs $ruser $rhost
  else
    nginx_copy_configs $ruser $rhost
  fi
}

function webserver_copy_logs()
{
  ruser=$1
  rhost=$2

  if [[ $webserver == "apache" ]]; then
    apache_copy_logs $ruser $rhost
  else
    nginx_copy_logs $ruser $rhost
  fi
}

function webserver_rmpid()
{
  if [[ $webserver == "apache" ]]; then
    apache_rmpid
#  else
#    nginx_rmpid
  fi
}

function webserver_start()
{
  if [[ $webserver == "apache" ]]; then
    apache_start
  else
    nginx_start
  fi
}

function webserver_stop()
{
  if [[ $webserver == "apache" ]]; then
    apache_stop
  else
    nginx_stop
  fi
}

function webserver_copy_ssl_mod()
{
  if [[ $webserver == "apache" ]]; then
    if [[ $cfg -eq $cfg_enf ]]; then
      cmd=$cmd"cp $apachedir/modules/mod_ssl_enf.so $apachedir/modules/mod_ssl.so; "
    else
      cmd=$cmd"cp $apachedir/modules/mod_ssl_base.so $apachedir/modules/mod_ssl.so; "
    fi
    do_cmd $ruser $host "$cmd" 0 0
  fi
}

function webserver_restart()
{
  ruser=$1
  rhost=$2
  webserver_stop
  webserver_rmpid
  webserver_start
}

function setup_mysql()
{
  ruser=$1
  host=$2
  cmd=""
  cmd=$cmd"ps aux | grep mysqld | wc -l"
  x=$(do_cmd $ruser $host "$cmd" 0 0)
  read x <<< "$x"
  echo "MYSQL running : $x"
  if [[ $x -lt 3 ]]; then
    cmd="/etc/init.d/mysql start"
    do_cmd $ruser $host "$cmd" 0 1
  fi
}

function do_setup()
{
  outdir=$1

  gitinfo="gitinfo"

  get_gitinfo $victim_ip $workdir
  scp $ruser@$victim_ip:$workdir/gitinfo "$outdir/$gitinfo.VS-gpace"

  get_gitinfo $victim_ip $fe_gpacedir
  scp $ruser@$victim_ip:$fe_gpacedir/gitinfo "$outdir/$gitinfo.VS-linux"

  get_gitinfo $host_ip $workdir
  scp $ruser@$host_ip:$workdir/gitinfo "$outdir/$gitinfo.VS-hypace"

  get_gitinfo $host_ip $fe_hypacedir
  scp $ruser@$host_ip:$fe_hypacedir/gitinfo "$outdir/$gitinfo.VS-xen"

  get_gitinfo localhost $workdir
  scp $ruser@localhost:$workdir/gitinfo "$outdir/$gitinfo.VC-gpace"

  get_gitinfo localhost $fe_gpacedir
  scp $ruser@localhost:$fe_gpacedir/gitinfo "$outdir/$gitinfo.VC-linux"

  if [[ $is_attack -eq 1 ]]; then
    get_gitinfo $attacker_server $workdir
    scp $ruser@$attacker_server:$workdir/gitinfo "$outdir/$gitinfo.AS-gpace"

    get_gitinfo $attacker_server $fe_gpacedir
    scp $ruser@$attacker_server:$fe_gpacedir/gitinfo "$outdir/$gitinfo.AS-linux"

    get_gitinfo $attacker_client $workdir
    scp $ruser@$attacker_client:$workdir/gitinfo "$outdir/$gitinfo.AC-gpace"

    get_gitinfo $attacker_client $fe_gpacedir
    scp $ruser@$attacker_client:$fe_gpacedir/gitinfo "$outdir/$gitinfo.AC-linux"
  fi

  scp $ruser@$host_ip:$workdir/tccfg.sh "$outdir/"

  ## ==== disable C-states on server ====
  run_dom0_setup $ruser "$host_ip"
  ## needed on maxwell13 if booted into dom0
  run_dom0_setup $ruser "$attacker_server"

  do_cmd $ruser $host_ip "xl vcpu-list; "

  do_cmd $ruser $victim_client "pkill -9 tcpdump; pkill -9 tshark; pkill -9 tcpdump_profiler.py"
  if [[ $is_attack -eq 1 ]]; then
    do_cmd $ruser $attacker_client "pkill -9 tcpdump; pkill -9 tshark; pkill -9 tcpdump_profiler.py"
  fi

#  ## ==== apache setup ====
#  trunc_logs $ruser $victim_ip
#  webserver_restart $ruser $victim_ip
#
#  ## ==== mysql setup ====
#  setup_mysql $ruser $victim_ip
#
#  sleep 2

  if [[ $cfg_enf -eq 1 ]]; then
    scp -r $ruser@$victim_ip:$(ssh -f $ruser@$victim_ip "ls -dtr1 $daemondir/$profiles_to_enf_dir | tail -1") $outdir/enforced_profile

    ## setup on server side
    run_appvm_setup1 $ruser $victim_ip
    run_once_setup $ruser $victim_ip $fe_gpacescripts
    run_host_setup $ruser $victim_ip $victim_ip $app_client_port "FE" $dbvm_ip  \
        $serv_epoch $serv_spin_thresh $serv_max_pkts_per_epoch  \
        $serv_max_other_per_epoch
    run_profile_logger $ruser $victim_ip $victim_ip $app_client_port  \
      $profiles_to_enf_dir $fe_profpacedir

    ## setup on client side
    run_once_setup $ruser $victim_client $cl_gpacescripts
    run_host_setup $ruser $victim_client $victim_ip $app_client_port "FE" $dbvm_ip  \
        $client_epoch $client_spin_thresh $client_max_pkts_per_epoch  \
        $client_max_other_per_epoch
  else
    run_appvm_setup1 $ruser $victim_ip
    run_once_setup $ruser $victim_ip $fe_gpacescripts
    run_once_setup $ruser $victim_client $cl_gpacescripts
  fi

}

function do_shutdown()
{
#  webserver_stop
#
#  sleep 2

  if [[ $cfg_enf -eq 1 ]]; then
    run_host_shutdown $ruser $victim_ip
    run_host_shutdown $ruser "$victim_client"
  fi
}

function copy_logs()
{
  citer=$1

  if [[ $cfg_enf -eq 1 ]]; then
    scp $ruser@$victim_ip:/var/log/syslog "$tstampfile.VS-gpace.syslog.$citer"
    scp $ruser@$victim_client:/var/log/syslog "$tstampfile.VC-gpace.syslog.$citer"

    dcmd="xl dmesg -c > /local/sme/xenstat "
    do_cmd $ruser $host_ip "$dcmd" 0 0
    scp $ruser@$host_ip:/local/sme/xenstat "$tstampfile.VS-hypace.xenstats.$citer"
  fi

  if [[ $is_attack -eq 1 ]] && [[ $use_ping -eq 0 ]]; then
#    scp $ruser@$attacker_server:"$mntexpdir/udp/tstamps.txt" "$tstampfile.AS-gpace.$citer"
    scp $ruser@$attacker_client:"$mntexpdir/udp/tstamps.txt" "$tstampfile.AC-gpace.$citer"
  fi
#  cp $workdir/attack_usenix.sh $outdir
}

function change_nic_bw()
{
  bw=$1
#  edev=$(ssh $ruser@$victim_ip "ifconfig | grep -B1 \"$victim_ip\" | head -1 | cut -d' ' -f1")
  cmd="ethtool -s eno1 speed $bw duplex full autoneg on"
  do_cmd $ruser $host_ip "$cmd"
}

function get_edev()
{
  ruser=$1
  host=$2

  edev=$(ssh $ruser@$host "ifconfig | grep -B1 \"$host\" | head -1 | cut -d' ' -f1")
  echo $edev
}

function run_tcpdump()
{
  ruser=$1
  host=$2
  appvm_ip=$3
  client_ip=$4
  edev=$5
  transport=$6
  profdir=$8

  cmd=""
  cmd=$cmd"mkdir -p $rootdir/tdump; "
  cmd=$cmd"python $profdir/tcpdump_profiler.py"
  cmd=$cmd" --server-ip $appvm_ip"
  cmd=$cmd" --client-ip $client_ip"
  cmd=$cmd" --iface $edev"
  cmd=$cmd" --transport $transport"
  cmd=$cmd" --server-port $7"
#  cmd=$cmd" --dumpfile $mntexpdir/tdump/$protocol.$appvm_ip-$client_ip.tdump"
#  cmd=$cmd" --outfile $mntexpdir/tdump/$protocol.$appvm_ip-$client_ip"
  cmd=$cmd" --dumpfile $mntexpdir/tdump/$tstampfstr.$iiter.tdump"
  cmd=$cmd" --outfile $mntexpdir/tdump/$tstampfstr.$iiter"
#  cmd=$cmd" --multi-tier False"
  cmd=$cmd" --action 0; "
  tcpdump_pid=$(ssh -f $ruser@$host "$cmd")
  echo "$tcpdump_pid"
  echo "ssh $ruser@$host \"$cmd\""
}

function stop_tcpdump()
{
  ruser=$1
  host=$2
  appvm_ip=$3
  client_ip=$4
  tcpdump_pid=$5
  transport=$6
  profdir=$8

  echo $tcpdump_pid

  tcpdump_pattern="tcpdump -B"
  kill_iter=0
  grepstr=$(ssh $ruser@$host "ps aux | grep $tcpdump_pid")
  if [[ "$grepstr" =~ "$tcpdump_pattern" ]]; then ntcpdump=1; else ntcpdump=0; fi
  echo "grep $kill_iter $tcpdump_pid: $ntcpdump"
  while [[ $ntcpdump -eq 1 ]]; do
    cmd=""
    cmd=$cmd"python $profdir/tcpdump_profiler.py"
    cmd=$cmd" --server-ip $appvm_ip"
    cmd=$cmd" --client-ip $client_ip"
    cmd=$cmd" --transport $transport"
    cmd=$cmd" --server-port $7"
#    cmd=$cmd" --dumpfile $mntexpdir/tdump/$protocol.$appvm_ip-$client_ip.tdump"
#    cmd=$cmd" --outfile $mntexpdir/tdump/$protocol.$appvm_ip-$client_ip"
    cmd=$cmd" --dumpfile $mntexpdir/tdump/$tstampfstr.$iiter.tdump"
    cmd=$cmd" --outfile $mntexpdir/tdump/$tstampfstr.$iiter"
    cmd=$cmd" --action $tcpdump_pid"
    do_cmd $ruser $host "$cmd" 0 1

    kill_iter=$(( $kill_iter + 1 ))
    grepstr=$(ssh $ruser@$host "ps aux | grep $tcpdump_pid")
    if [[ "$grepstr" =~ "$tcpdump_pattern" ]]; then ntcpdump=1; else ntcpdump=0; fi
    echo "grep $kill_iter $tcpdump_pid: $ntcpdump"
  done
}

function do_cmd_attack ()
{
  #echo $1 $2 $3 $4 $5 $6 $7
  do_bg=0 ## default value
  ruser=$1
  host=$2
  cmd=$3
  do_bg=$4
  out_null=$5
  is_out_file=$6
  out_file_name=$7
  #echo "1. ssh $ruser@$host \"$cmd\" $do_bg $out_null"
  if [[ $do_bg -eq 1 ]]; then
    if [[ $out_null -eq 1 ]]; then
      echo "ssh -f $ruser@$host \"$cmd &\" > /dev/null 2>&1"
      ssh -f $ruser@$host "$cmd &" > /dev/null 2>&1
    else
      if [[ $is_out_file -eq 0 ]]; then
        echo "ssh -f $ruser@$host \"$cmd\""
        ssh -f $ruser@$host "$cmd"
      else
        echo "ssh -f $ruser@$host \"$cmd & echo \$! \" >> $out_file_name 2>>$out_file_name.err"
        ssh -f $ruser@$host "$cmd & echo \$! " >> $out_file_name 2>>$out_file_name.err #2>&1
      fi
    fi
  else
    if [[ $out_null -eq 1 ]]; then
      echo "ssh $ruser@$host \"$cmd\" > /dev/null 2>&1"
      ssh $ruser@$host "$cmd" > /dev/null 2>&1
    else
      if [[ $is_out_file -eq 0 ]]; then
        echo "ssh $ruser@$host \"$cmd\""
        ssh $ruser@$host "$cmd"
      else
        echo "ssh $ruser@$host \"$cmd\" >> $out_file_name 2>>$out_file_name.err"
        ssh $ruser@$host "$cmd" >> $out_file_name 2>>$out_file_name.err #2>&1
      fi
    fi
  fi
#  echo "ssh $ruser@$host \"$cmd\" $do_bg $out_null $is_out_file $out_file_name"
}

function run_profile_logger()
{
  # runtype:
  # 0 - use_log_dir and set profile
  # 1 - run as a daemon (make sure to invoke stop_profile_logger)
  # 2 - run for n requests (set prof_for_n_req flag)
  # 3 - send_ascii_profiles_dir and set profile

  dummy_ip="139.19.168.173"
  dummy_port=2231
  ruser=$1
  host=$2
  scheddir=$7
  profdir=$8

  echo "Run profile logger $cfg runtype $runtype"

  if [[ $cfg_enf -ne 1 ]]; then
    return
  fi
  cmd="cd $profdir; python sme_profiler.py"
  cmd=$cmd" --dummy_ip $dummy_ip --dummy_port $dummy_port"
  cmd=$cmd" --ep_ip1 \"0.0.0.0\" --ep_port1 0 --ep_ip2 \"$3\" --ep_port2 $4"
  cmd=$cmd" --ep_ip3 \"$3\" --ep_port3 0 --ep_ip4 \"139.19.171.106\" --ep_port4 3306"
  cmd=$cmd" --update_prof_freq 1"
  cmd=$cmd" --send_ascii_profiles_dir \"$daemondir/$scheddir\""
  do_cmd_attack $ruser $host "$cmd" 0 0 0

}

function run_appvm_setup1()
{
  ruser=$1
  host=$2
  cmd="echo \"\" > /var/log/apache2/responsetime.log;"
  cmd="echo \"\" > /usr/local/apache2/logs/responsetime.log;"
  cmd=$cmd" echo \"\" > /var/log/apache2/access.log;"
  #cmd=$cmd" service apache2 stop && service apache2 start; "
  #cmd=$cmd" /usr/local/apache2/bin/apachectl -k stop && sleep 1 && /usr/local/apache2/bin/apachectl -k start;"
  cmd=$cmd" echo \"\" > /local/sme/msg_sent_to_kernel.txt;"
  cmd=$cmd" echo \"\" > /local/sme/stats_5_tuple.txt;"
  cmd=$cmd" echo \"\" > /local/sme/stats_marker_sync.txt;"

  do_cmd_attack $ruser $host "$cmd" 0 0 0
}

function start_attack_traffic()
{
  aiter=$1

  if [[ $is_attack -ne 1 ]]; then
    echo "No attack traffic"
    return
  fi

  if [[ $use_ping -eq 0 ]]; then
    cmd=""
    cmd=$cmd"rm -rf $mntexpdir/udp/tstamps.txt; sleep 1; "
    cmd=$cmd"taskset -c 0 $workdir/udp/$aserv_script --serv_ip \"$attacker_server\""
    cmd=$cmd" --serv_port $attack_serv_port --exptime 5 --payload_size $payload_size"
    cmd=$cmd" --outfile $mntexpdir/udp/tstamps.txt"
    cmd=$cmd" & echo \$!"
    cmd=$cmd" > $mntexpdir/udp/tstamps.pid 2>&1"
    do_cmd_attack $ruser $attacker_server "$cmd" 1 0 0
#    do_cmd_attack $ruser $attacker_server "$cmd" 1 0 1 $tstampfile.server.$aiter

    sleep 1

    cmd=""
    cmd=$cmd"rm -rf $mntexpdir/udp/tstamps.txt; sleep 1; "
    cmd=$cmd"$workdir/udp/$aclnt_script --serv_ip \"$attacker_server\""
    cmd=$cmd" --serv_port $attack_serv_port --delay 5" #" --bw $attack_bw_MBs"
    cmd=$cmd" --pkt_rate $attack_pkt_rate --payload_size $payload_size"
    cmd=$cmd" --snd_wnd $attack_snd_wnd"
    cmd=$cmd" --outfile $mntexpdir/udp/tstamps.txt"
    cmd=$cmd" & echo \$!"
    cmd=$cmd" > $mntexpdir/udp/tstamps.pid 2>&1"
    do_cmd_attack $ruser $attacker_client "$cmd" 1 0 0
#    do_cmd_attack $ruser $attacker_client "$cmd" 1 0 1 $tstampfile.client.$aiter

    sleep 1

#    att_servpid=`head -n1 $tstampfile.server.$aiter`
#    att_clientpid=`head -n1 $tstampfile.client.$aiter`
    cmd="head -n1 $mntexpdir/udp/tstamps.pid"
    att_servpid=$(ssh $ruser@$attacker_server "$cmd")
    att_clientpid=$(ssh $ruser@$attacker_client "$cmd")
    echo "ATTACKER SERVER: $att_servpid CLIENT: $att_clientpid $(date)"
  else
    for (( c=1; c<=$numclients; c++ )); do
      date +%s%N
      cmd=""
      cmd=$cmd"ping -w $experiment_time -D -i $ping_i -s $payload_size $attacker_client"
      do_cmd_attack $ruser $attacker_server "$cmd" 1 0 1 $tstampfile.$c.$aiter
    done
  fi
}

function stop_attack_traffic()
{
  if [[ $is_attack -ne 1 ]]; then
    echo "No attack traffic"
    return
  fi

  if [[ $use_ping -eq 1 ]]; then
    return;
  fi

  echo "KILL ATTACKER SERVER: $att_servpid CLIENT: $att_clientpid $(date)"

  clntpattern="$workdir/udp/$aclnt_script"
  servpattern="$workdir/udp/$aserv_script"

  kill_iter=0
  grepstr=$(ssh $ruser@$attacker_client "ps aux | grep $att_clientpid")
  if [[ "$grepstr" =~ "$clntpattern" ]]; then nclient=1; else nclient=0; fi
  echo "grep $kill_iter $att_clientpid: $nclient"
  while [[ $nclient -eq 1 ]]; do
    cmd="kill -2 $att_clientpid && sleep 1;"
    do_cmd_attack $ruser $attacker_client "$cmd" 0 0 0 2>>"$tstampfile.AC-gpace.$aiter.err"

    killed=$(cat "$tstampfile.AC-gpace.$aiter.err" | grep "No such process" | wc -l)
    if [[ $killed -gt 0 ]]; then
      echo "BREAK client kill loop $kill_iter $killed"
      break
    fi

    kill_iter=$(( $kill_iter + 1 ))

    grepstr=$(ssh $ruser@$attacker_client "ps aux | grep $att_clientpid")
    if [[ "$grepstr" =~ "$clntpattern" ]]; then nclient=1; else nclient=0; fi
    echo "grep $kill_iter $att_clientpid: $nclient"
  done

  kill_iter=0
  grepstr=$(ssh $ruser@$attacker_server "ps aux | grep $att_servpid")
  if [[ "$grepstr" =~ "$servpattern" ]]; then nserv=1; else nserv=0; fi
  echo "grep $kill_iter $att_servpid: $nserv"
  while [[ $nserv -eq 1 ]]; do
    cmd="kill -2 $att_servpid && sleep 1;"
    do_cmd_attack $ruser $attacker_server "$cmd" 0 0 0 2>>"$tstampfile.AS-gpace.$aiter.err"

    killed=$(cat "$tstampfile.AS-gpace.$aiter.err" | grep "No such process" | wc -l)
    if [[ $killed -gt 0 ]]; then
      echo "BREAK server kill loop $kill_iter $killed"
      break;
    fi

    kill_iter=$(( $kill_iter + 1 ))

    grepstr=$(ssh $ruser@$attacker_server "ps aux | grep $att_servpid")
    if [[ "$grepstr" =~ "$servpattern" ]]; then nserv=1; else nserv=0; fi
    echo "grep $kill_iter $att_servpid: $nserv"
  done
}

function run_attack_allvideo_1iter()
{
  iter=0

  echostr=""
  echostr=$echostr"[$(date)]"
  echostr=$echostr" cfg: $cfgstr contention: $contention"
  echostr=$echostr" BW: $hostbw mbps attack traffic: $attackwrkload"
  echostr=$echostr" pkt rate: $attack_pkt_rate payload sz: $payload_size"
  echostr=$echostr" bw: $attack_bw_MBs MB/s $attack_bw_mbps mbps"
  echostr=$echostr" sndwnd: $attack_snd_wnd dur: $play_dur #vid: $num_videos iter: $iiter/$niter"
  echo $echostr

  for (( viter=0; viter<$num_videos; viter++ )); do
    suffix="$contention$hostbw.v$viter.$cfgstr.$iter"
    outfile="$outdir/out.$suffix"
    tstampfstr="tstamps.v$viter"
    tstampfile="$outdir/tstamps.v$viter"

  (
    echo $echostr
      ## ==== start tcpdump =====
      if [[ $do_tcpdump -eq 1 ]]; then
        edev=$(get_edev $ruser "$victim_ip")
        remote_tcpdump_pid=$(run_tcpdump $ruser "$victim_ip" "$victim_ip"  \
          "$victim_client" "$edev" "tcp" $app_client_port $attack_srcdir)
        echo "REMOTE TCPDUMP PID: $remote_tcpdump_pid"
        read remote_tcpdump_pid <<< "$remote_tcpdump_pid"

        edev=$(get_edev $ruser "$victim_client")
        local_tcpdump_pid=$(run_tcpdump "root" "$victim_client" "$victim_ip"  \
          "$victim_client" "$edev" "tcp" $app_client_port $attack_srcdir)
        echo "LOCAL TCPDUMP PID: $local_tcpdump_pid"
        read local_tcpdump_pid <<< "$local_tcpdump_pid"

#        sleep 1
      fi

      if [[ $do_attack_tcpdump -eq 1 ]]; then
        edev=$(get_edev $ruser "$attacker_client")
        attackclnt_tcpdump_pid=$(run_tcpdump $ruser "$attacker_client"  \
          "$attacker_server" "$attacker_client" "$edev" "udp" $attack_serv_port \
          $attack_srcdir)
        echo "ATTACK CLNT TCPDUMP PID: $attackclnt_tcpdump_pid"
        read attackclnt_tcpdump_pid <<< "$attackclnt_tcpdump_pid"

#        edev=$(get_edev $ruser "$attacker_server")
#        attackserv_tcpdump_pid=$(run_tcpdump $ruser "$attacker_server" "$attacker_server" \
#          "$attacker_client" "$edev" "udp" $attack_serv_port $attack_srcdir)
#        echo "ATTACK SERV TCPDUMP PID: $attackserv_tcpdump_pid"
#        read attackserv_tcpdump_pid <<< "$attackserv_tcpdump_pid"
      fi

      date;

      # ==== attacker measurements ====
      start_attack_traffic $iiter

      date;

      # ==== client requests to victim ====
      victim_traffic_pervideo_niter $viter $attack_srcdir

      endtime=$(date +%s.%N)

      stop_attack_traffic

      echo "End of run $endtime"

      cmd="tc -s -d qdisc ls dev eno1; tc -s -d class ls dev eno1; tc -s -d filter ls dev eno1"
      do_cmd $ruser $host_ip "$cmd"

      ## ==== copy some logs ====
      copy_logs $iiter

      date;

      ## ==== stop tcpdump ====
      if [[ $do_tcpdump -eq 1 ]]; then
        stop_tcpdump "root" "$victim_ip" "$victim_ip" "$victim_client"  \
          $remote_tcpdump_pid "tcp" $app_client_port $attack_srcdir
#        scp root@$victim_ip:"$mntexpdir/tdump/$tstampfstr.$iiter.tdump"  \
#          "$tstampfile.tdump.VS-gpace.$iiter"
        stop_tcpdump "root" "$victim_client" "$victim_ip" "$victim_client"  \
          $local_tcpdump_pid "tcp" $app_client_port $attack_srcdir
#        cp "$mntexpdir/tdump/$tstampfstr.$iiter.tdump" \
#          "$tstampfile.tdump.VC-gpace.$iiter"
      fi

      if [[ $do_attack_tcpdump -eq 1 ]]; then
        stop_tcpdump $ruser "$attacker_client" "$attacker_server" \
          "$attacker_client" $attackclnt_tcpdump_pid "udp" $attack_serv_port  \
          $attack_srcdir
        scp $ruser@$attacker_client:"$mntexpdir/tdump/$tstampfstr.$iiter.tdump" \
          "$tstampfile.tdump.AC-gpace.$iiter"

#        stop_tcpdump $ruser "$attacker_server" "$attacker_server" \
#          "$attacker_client" $attackserv_tcpdump_pid "udp" $attack_serv_port $attack_srcdir
#        scp $ruser@$attacker_server:"$mntexpdir/tdump/$tstampfstr.$iiter.tdump" \
#          "$tstampfile.tdump.AS-gpace.$iiter"
      fi

      date;
  ) >> $outfile

  done
}

function victim_traffic_pervideo_niter()
{
  if [[ $is_victim -ne 1 ]]; then
    echo "No victim video traffic"
    return
  fi

  echo "$(date +%s.%N) Requesting video files"

  seed=$1
  profdir=$2

  url="victim_server.php?c=$cfg_enf&d=0&f=./videos"

  cmd=""
  cmd=$cmd"sleep 1 &&"
  cmd=$cmd" (python3 $profdir/victim_client.py -I $victim_ip -p $protocol"
  cmd=$cmd" -u \"$url\" -f \"$profdir/$videolist_attack\" -c $n_clusters"
  cmd=$cmd" -ps $play_sleep -nv 1 -dur $play_dur -s $seed -q $hqvideo"
  cmd=$cmd" -bgs $bufgoal_s -rbgs $rebufgoal_s)"
  #cmd=$cmd" >> $outdir/vid.$suffix)"

  do_cmd_attack $ruser $victim_client "$cmd" 0 0 1 "$outdir/vid.$suffix"

#  victim_cpid=`head -n1 $outdir/vid.$suffix`
#  echo "VICTIM CLIENT: $victim_cpid $(date)"
}

function run_attack_pervideo_niter()
{
  iter=0
  suffix="$contention$hostbw.v$viter.$cfgstr.$iter"
  outfile="$outdir/out.$suffix"

  echostr=""
  echostr=$echostr"[$(date)]"
  echostr=$echostr" cfg: $cfgstr contention: $contention"
  echostr=$echostr" BW: $hostbw mbps attack traffic: $attackwrkload"
  echostr=$echostr" pkt rate: $attack_pkt_rate payload sz: $payload_size"
  echostr=$echostr" bw: $attack_bw_MBs MB/s $attack_bw_mbps mbps"
  echostr=$echostr" sndwnd: $attack_snd_wnd dur: $play_dur video: $viter #iter: $niter"
  echo $echostr

  (
    echo $echostr
    for (( iiter=0; iiter<$niter; iiter++ )); do

      ## ==== start tcpdump =====
      if [[ $do_tcpdump -eq 1 ]]; then
        edev=$(get_edev $ruser "$victim_ip")
        remote_tcpdump_pid=$(run_tcpdump $ruser "$victim_ip" "$victim_ip"  \
          "$victim_client" "$edev" "tcp" $app_client_port $attack_srcdir)
        echo "REMOTE TCPDUMP PID: $remote_tcpdump_pid"
        read remote_tcpdump_pid <<< "$remote_tcpdump_pid"

#        edev=$(get_edev $ruser "$victim_client")
#        local_tcpdump_pid=$(run_tcpdump "root" "$victim_client" "$victim_ip"  \
#          "$victim_client" "$edev" "tcp" $app_client_port $attack_srcdir)
#        echo "LOCAL TCPDUMP PID: $local_tcpdump_pid"
#        read local_tcpdump_pid <<< "$local_tcpdump_pid"

#        sleep 1
      fi

      if [[ $do_attack_tcpdump -eq 1 ]]; then
        edev=$(get_edev $ruser "$attacker_client")
        attackclnt_tcpdump_pid=$(run_tcpdump $ruser "$attacker_client"  \
          "$attacker_server" "$attacker_client" "$edev" "udp" $attack_serv_port \
          $attack_srcdir)
        echo "ATTACK CLNT TCPDUMP PID: $attackclnt_tcpdump_pid"
        read attackclnt_tcpdump_pid <<< "$attackclnt_tcpdump_pid"

#        edev=$(get_edev $ruser "$attacker_server")
#        attackserv_tcpdump_pid=$(run_tcpdump $ruser "$attacker_server" "$attacker_server" \
#          "$attacker_client" "$edev" "udp" $attack_serv_port $attack_srcdir)
#        echo "ATTACK SERV TCPDUMP PID: $attackserv_tcpdump_pid"
#        read attackserv_tcpdump_pid <<< "$attackserv_tcpdump_pid"
      fi

      date;

      # ==== attacker measurements ====
      start_attack_traffic $iiter

      date;

      # ==== client requests to victim ====
      victim_traffic_pervideo_niter $viter $attack_srcdir

      endtime=$(date +%s.%N)

      stop_attack_traffic

      echo "End of run $endtime"

      cmd="tc -s -d qdisc ls dev eno1; tc -s -d class ls dev eno1; tc -s -d filter ls dev eno1"
      do_cmd $ruser $host_ip "$cmd"

      ## ==== copy some logs ====
      copy_logs $iiter

      date;

      ## ==== stop tcpdump ====
      if [[ $do_tcpdump -eq 1 ]]; then
        stop_tcpdump "root" "$victim_ip" "$victim_ip" "$victim_client"  \
          $remote_tcpdump_pid "tcp" $app_client_port $attack_srcdir
#        scp root@$victim_ip:"$mntexpdir/tdump/$tstampfstr.$iiter.tdump"  \
#          "$tstampfile.tdump.VS-gpace.$iiter"
#        stop_tcpdump "root" "$victim_client" "$victim_ip" "$victim_client"  \
#          $local_tcpdump_pid "tcp" $app_client_port $attack_srcdir
#        cp "$mntexpdir/tdump/$tstampfstr.$iiter.tdump" \
#          "$tstampfile.tdump.VC-gpace.$iiter"
      fi

      if [[ $do_attack_tcpdump -eq 1 ]]; then
        stop_tcpdump $ruser "$attacker_client" "$attacker_server" \
          "$attacker_client" $attackclnt_tcpdump_pid "udp" $attack_serv_port  \
          $attack_srcdir
        scp $ruser@$attacker_client:"$mntexpdir/tdump/$tstampfstr.$iiter.tdump" \
          "$tstampfile.tdump.AC-gpace.$iiter"

#        stop_tcpdump $ruser "$attacker_server" "$attacker_server" \
#          "$attacker_client" $attackserv_tcpdump_pid "udp" $attack_serv_port $attack_srcdir
#        scp $ruser@$attacker_server:"$mntexpdir/tdump/$tstampfstr.$iiter.tdump" \
#          "$tstampfile.tdump.AS-gpace.$iiter"
      fi

      date;

    done

  ) > $outfile
}

#ethbw=$( ssh -f $ruser@$attacker_client 'ethtool eno1 | grep Speed' | cut -d' ' -f 2)
ethbw=$( ssh -f $ruser@$host_ip 'ethtool eno1 | grep Speed' | cut -d' ' -f 2)
ethbw=${ethbw%M*}
setup_str=""
if [[ $is_victim -eq 1 ]]; then
  if [[ $cfg_enf -eq 1 ]]; then
    cfgstr="sme"
  else
    cfgstr="base"
  fi
  setup_str=$setup_str"${ethbw}_$cfgstr"
else
  cfgstr="novictim"
  setup_str=$setup_str"${ethbw}_$cfgstr"
fi

if [[ $use_ping -eq 0 ]]; then
  attackwrkload="udp"
else
  attackwrkload="ping"
fi

exptime=$(date +"%y%m%d-%H%M")

dirname="${exptime}"
if [[ $postfix_str != "" ]]; then
  postfix_str="_"$postfix_str
fi

if [[ $use_ping -eq 0 ]] ;then
  main_outdir="$expdir/$attackwrkload/${dirname}$postfix_str"
else
  main_outdir="$expdir/$attackwrkload/${dirname}_${ping_i}$postfix_str"
fi

mkdir -p "$main_outdir"


profiles_to_enf_dir="schedules_video_1tier"
#videolist_attack="victim_videolist"
videolist_attack="videolist"

#experiment_time=$(( ${#sizes[*]} + 2 ))

hostbw=1000 ## TODO: convert to cmdline arg
pkts_rate=$(( ($hostbw * 1000 * 1000)/(8 * ($payload_size + $udp_overhead)) ))
if [[ $payload_size -lt $MTU ]]; then
  pkts_rate=$(( ($hostbw * 1000 * 1000)/(8 * $MTU) ))
fi
## set this to 80-90% of the MTU-based pkt rate above
attack_pkt_rate_pct=100
attack_pkt_rate=$(( ($pkts_rate * $attack_pkt_rate_pct)/100 ))
attack_bw_mbps=$(( ($attack_pkt_rate * ($payload_size + 42) * 8)/(1000*1000) ))
## attacker bandwidth in MB/s (not Mbps)
attack_bw_MBs=$(( ($attack_pkt_rate * ($payload_size + 42))/(1024*1024) ))
#attack_bw_MBs=$(( $(( $hostbw * 1000 * 1000 * 95 ))/(8 * 1024 * 1024 * 100) ))
#attack_bw_MBs=$(( $(( $hostbw * 1000 * 1000 * 80))/ (8 * 1024 * 1024 * 100) ))
attack_snd_wnd=4500

aserv_script="attack_server"

aclnt_script="attack_client"
#aclnt_script="attack_client_openloop"
date;

(
  ## ==== change NIC BW on host ====
  change_nic_bw $hostbw
)

######################################################################
## victim traffic: videos, iterate n times over each video before next
######################################################################
num_videos=10
play_sleep=5
n_clusters=0 ## change to 20 for pacer config
play_dur=150
bufgoal_s=30
#bufgoal_s=$play_sleep
#bufgoal_s=5.0
rebufgoal_s=15
#rebufgoal_s=$play_sleep
#rebufgoal_s=5.0
experiment_time=$play_dur
## 2 for indirect measurements based attack
## 0: 240p --> 100mbps, 1: 1080p --> 10gbps, 2: 720p --> 1gbps
## 20M: 20MB-40MB
hqvideo="2"
niter=50

outdir="$main_outdir/$contention$hostbw-$cfgstr/pervid_niter"
mkdir -p $outdir
echo "[$(date)] $outdir/exp.out hostbw: $hostbw attack pktrate: $attack_pkt_rate"  \
  "payload sz: $payload_size bw: $attack_bw_MBs MB/s $attack_bw_mbps mbps"  \
  "sndwnd: $attack_snd_wnd #videos: $num_videos dur: $play_dur #iter: $niter"
#(
#  do_setup "$outdir"
#
#  for (( viter=0; viter<$num_videos; viter++ )); do
#    tstampfstr="tstamps.v$viter"
#    tstampfile="$outdir/tstamps.v$viter"
#    run_attack_pervideo_niter
#  done
#  date;
#
#  do_shutdown
#) > "$outdir/exp.out"

(
  do_setup "$outdir"

  for (( iiter=0; iiter<$niter; iiter++ )); do
    run_attack_allvideo_1iter
  done
  date;

  do_shutdown
) > "$outdir/exp.out"

## ==== reset NIC BW on host ====
date;
change_nic_bw 10000

