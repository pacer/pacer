// Server side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>
#include <sys/syscall.h>
#include <errno.h>
#include <argp.h>

#define PORT	 8080
#define MTU_PAYLOAD 1472
#define DEFAULT_PAYLOAD (7 * sizeof(long int))
#define MTU 1514
/*
 * Header lengths
 * UDP:  8 bytes
 * IP:  20 bytes
 * ETH: 14 bytes
 */
#define HDR_OVERHEAD  42
/*
 * 8 bytes preamble + 4 bytes CRC trailer +
 * 12 bytes minimum inter-packet gap between ethernet frames
 */
#define ETH_OVERHEAD  24
#define DEFAULT_BYTES_ON_WIRE (DEFAULT_PAYLOAD + HDR_OVERHEAD)

#define KB  (1024)
#define MB  (1024 * KB)
#define GB  (1024 * MB)

#define Kb  (1000)
#define Mb  (1000 * Kb)
#define Gb  (1000 * Mb)

#define ms_per_s  (1000)
#define us_per_s  (1000 * ms_per_s)
#define ns_per_s  (1000 * us_per_s)
//#define DEFAULT_PAYLOAD 65500

#define PRINT_LOG 0

int experiment_time;
int payload_size = DEFAULT_PAYLOAD;
int bytes_on_wire = DEFAULT_BYTES_ON_WIRE;

enum {
  ARG_IP = 1,
  ARG_PORT,
  ARG_EXP_TIME,
  MAX_ARGS
};

const char *arg_prog_version = "UDP server 1.0";
const char *arg_prog_bug_address = "xxx";
static char doc[] = "yyy";
static char args_doc[] = "--serv_ip x.x.x.x --serv_port 8080 --exptime 6 --payload_size 40"
                         " --outfile /local/sme/exp/attack/udp/tstamps.txt";

static struct argp_option options[] = {
  {"serv_ip", 'i', "serv_ip", 0, "IP address of the UDP server"},
  {"serv_port", 'p', "serv_port", 0, "Port of the UDP server"},
  {"exptime", 't', "exptime", 0, "Max wait time on select()"},
  {"payload_size", 's', "payload_size", 0, "attacker's UDP payload size (do not count headers)"},
  {"outfile", 'o', "outfile", 0, "Output file path"},
  { 0 },
};

struct arguments {
  char *serv_ip;
  int serv_port;
  int exptime;
  int payload_size;
  char *outfile;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  char *err = NULL;
  switch (key) {
    case 'i': arguments->serv_ip = arg;
      break;
    case 'p': arguments->serv_port = (int) strtol(arg, &err, 10);
      if (err == arg) {
        printf("[%s:%d] conversion err %s\n", __func__, __LINE__, err);
        return -EINVAL;
      }
      break;
    case 't': arguments->exptime = (int) strtol(arg, &err, 10);
      if (err == arg) {
        printf("[%s:%d] conversion err %s\n", __func__, __LINE__, err);
        return -EINVAL;
      }
      break;
    case 's': arguments->payload_size = (int) strtol(arg, &err, 10);
      if (err == arg) {
        printf("[%s:%d] conversion err %s\n", __func__, __LINE__, err);
        return -EINVAL;
      }
      break;
    case 'o': arguments->outfile = arg;
      break;
    case ARGP_KEY_ARG: return 0;
    case ARGP_KEY_END:
      if (state->argc < (2*MAX_ARGS - 1))
        argp_usage(state);
      break;
    default: return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

static struct argp argp = {
  options,
  parse_opt,
  args_doc,
  doc,
  0, 0, 0
};

pid_t gettid(void)
{
  return syscall(SYS_gettid);
}

/*
 * signal handler for kill signals
 */
int flag = 0;
void sig_handler(int signo)
{
  flag = 1;
//  printf("%d received sig %d\n", gettid(), signo);
}

static __inline__ unsigned long long rdtsc(void)
{
      unsigned hi, lo;
      __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
      return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

double time_diff_us(struct timeval x , struct timeval y)
{
    double x_us , y_us , diff;

    x_us = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_us = (double)y.tv_sec*1000000 + (double)y.tv_usec;

    diff = (double)y_us - (double)x_us;

    return diff;
}

uint64_t time_diff_ns(struct timespec x, struct timespec y)
{
  uint64_t x_ns = (x.tv_sec * ns_per_s) + x.tv_nsec;
  uint64_t y_ns = (y.tv_sec * ns_per_s) + y.tv_nsec;
  uint64_t diff = y_ns - x_ns;
  return diff;
}

typedef struct serv_thread_arg {
  int sockfd;
  char *outfile;
  FILE *outfile_fp;
} serv_thread_arg_t;

void *
send_recv_msg(void *arg_p)
{
  int ret = 0;
  unsigned int len;
  char buffer[payload_size];
  struct sockaddr_in cliaddr;

  struct timespec rcv_start_time_ns;
  struct timespec rcv_tnow_ns;
  struct timespec snd_tnow_ns;
#if PRINT_LOG
  struct timespec prev_time_ns;
  struct timespec sender_time_ns;
  uint64_t time_drift_ns;
  uint64_t rcvd_pkts_interval_ns;
  int curr_seq_number;
  int prev_seq_number;
#endif

  long count = 0;
  struct timespec ts;
  ts.tv_sec = experiment_time;

  serv_thread_arg_t *arg = (serv_thread_arg_t *) arg_p;
  int sockfd = arg->sockfd;

  fd_set socks;
  FD_ZERO(&socks);
  FD_SET(sockfd, &socks);

  sigset_t sigmask;
  sigemptyset(&sigmask);
  sigaddset(&sigmask, SIGINT);

  memset(buffer, 0, payload_size);
  memset(&cliaddr, 0, sizeof(cliaddr));
//  printf("%d Created server thread\n", gettid());

  while (!flag) {
//    ret = select(sockfd+1, &socks, NULL, NULL, &tv);
    ret = pselect(sockfd+1, &socks, NULL, NULL, &ts, &sigmask);
    // timeout
    if (ret == 0)
      continue;

    // interrupt or error
    if (ret < 0) {
      if (ret == -EINTR) // signal
        break;

      printf("%d select ret %d flag %d\n", gettid(), ret, flag);
      continue;
    }

    ret = recvfrom(sockfd, buffer, payload_size, MSG_WAITALL,
        (struct sockaddr *) &cliaddr, &len);

    clock_gettime(CLOCK_REALTIME, &rcv_tnow_ns);

    if (count == 0) {
      rcv_start_time_ns.tv_sec = rcv_tnow_ns.tv_sec;
      rcv_start_time_ns.tv_nsec = rcv_tnow_ns.tv_nsec;
    }

#if PRINT_LOG
    curr_seq_number = ((long int *) buffer)[0];
    sender_time_ns.tv_sec = ((long int *) buffer)[1];
    sender_time_ns.tv_nsec = ((long int *) buffer)[2];
    if (count == 0) {
        prev_seq_number = curr_seq_number;
        prev_time_ns.tv_sec = rcv_tnow_ns.tv_sec;
        prev_time_ns.tv_nsec = rcv_tnow_ns.tv_nsec;
    }

    time_drift_ns = time_diff_ns(sender_time_ns, rcv_tnow_ns);
    rcvd_pkts_interval_ns = time_diff_ns(prev_time_ns, rcv_tnow_ns);

    fprintf(arg->outfile_fp, "%08ld\t%10ld.%09ld\t%06ld\t%08d\t%d\t%ld\n",
        count, sender_time_ns.tv_sec, sender_time_ns.tv_nsec, rcvd_pkts_interval_ns,
        curr_seq_number, curr_seq_number - prev_seq_number, time_drift_ns);
#endif

    ((long int *) buffer)[3] = rcv_tnow_ns.tv_sec;
    ((long int *) buffer)[4] = rcv_tnow_ns.tv_nsec;

    clock_gettime(CLOCK_REALTIME, &snd_tnow_ns);
    ((long int *) buffer)[5] = snd_tnow_ns.tv_sec;
    ((long int *) buffer)[6] = snd_tnow_ns.tv_nsec;

    sendto(sockfd, buffer, payload_size, MSG_CONFIRM,
        (const struct sockaddr *) &cliaddr, len);

    count++;
#if PRINT_LOG
    prev_seq_number = curr_seq_number;
    prev_time_ns.tv_sec = rcv_tnow_ns.tv_sec;
    prev_time_ns.tv_nsec = rcv_tnow_ns.tv_nsec;
#endif
  }

  /*
   * These statistics do not take into account the true start time of the thread.
   * Instead they start measuring only from the time of receiving the first packet.
   */
  double exp_time_ns = time_diff_ns(rcv_start_time_ns, rcv_tnow_ns);
  double exp_time_s = exp_time_ns / ns_per_s;
  fprintf(arg->outfile_fp, "#Server side statistics\n");
  fprintf(arg->outfile_fp, "#Total time for experiment = %f s\n", exp_time_s);
  fprintf(arg->outfile_fp, "#Total packets recieved = %ld\n", count);
  fprintf(arg->outfile_fp, "#Packet rate = %9.3f pkts/s\n", count/exp_time_s);
  fprintf(arg->outfile_fp, "#Total MBs recieved (payload size %d) = %f\n",
      payload_size, ((double) count * payload_size)/MB);
  fprintf(arg->outfile_fp, "#Total xput GB/s (1024)= %f\n",
      (((double) count * payload_size)/GB) / exp_time_s);
  fprintf(arg->outfile_fp, "#Total xput Gbps (1000)= %f\n",
      (((double) 8 * count * bytes_on_wire)/Gb) / exp_time_s);

  return NULL;
}

void
run_server_sync(void *sockfd, char *outfile)
{
  pthread_t tid[1];
  pthread_attr_t attr;
  pthread_attr_init(&attr);

  FILE *fp = fopen(outfile, "a+");

  serv_thread_arg_t arg = {
    .sockfd = *(int *) sockfd,
    .outfile = outfile,
    .outfile_fp = fp,
  };

  pthread_create(&tid[0], &attr, send_recv_msg, (void *) &arg);

  pthread_join(tid[0], NULL);

  fclose(fp);
}

// Driver code
int main(int argc, char *argv[])
{
  char *serv_ip;
  int port = -1;
  struct arguments arguments;
  char *outfile = NULL;

  int sockfd;
  struct sockaddr_in servaddr;

  argp_parse(&argp, argc, argv, 0, 0, &arguments);
#if 0
  printf("IP: %s port %u exp time %d\n", arguments.serv_ip, arguments.serv_port,
      arguments.exptime);
#endif

  serv_ip = arguments.serv_ip;
  port = arguments.serv_port;
  experiment_time = arguments.exptime;
  outfile = arguments.outfile;

  if (!(arguments.payload_size < DEFAULT_PAYLOAD)) {
    payload_size = arguments.payload_size;
    bytes_on_wire = (payload_size + HDR_OVERHEAD);
  }

  // Filling server information
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET; // IPv4
  servaddr.sin_addr.s_addr = INADDR_ANY;
  if (port < 0)
    servaddr.sin_port = htons(PORT);
  else
    servaddr.sin_port = htons(port);

  inet_pton(AF_INET, serv_ip, &(servaddr.sin_addr));

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  int rcvbuf_size = ((1024 * 1024 * 1024) - 1);
  if (setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &rcvbuf_size, (socklen_t) sizeof(int)) < 0) {
    perror("Error setting rcvbuf size");
    exit(EXIT_FAILURE);
  }

  int sndbuf_size = 0;
  sndbuf_size = ((1024 * 1024 * 1024) - 1);
  if (setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &sndbuf_size, (socklen_t) sizeof(int)) < 0) {
    perror("Error setting sndbuf size");
    exit(EXIT_FAILURE);
  }

  // Bind the socket with the server address
  if (bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  if (signal(SIGINT, sig_handler) == SIG_ERR)
    perror("Cannot catch SIGINT");

  run_server_sync((void *) &sockfd, outfile);

  close(sockfd);

  return 0;
}

