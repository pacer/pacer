#!/bin/bash

##################################################################
## prerequisite command to be executed before running this script:
##
## `tc qdisc add dev eno1 root handle 0: htb`
## `tc -s -d class ls dev eno1`
## `tc -s -d qdisc ls dev eno1`
## this will reveal id of the configured traffic class, e.g., 8001
## set parentid to 8001, and action is "add" or "del"
##################################################################


## id of traffic class, which can be known after running above commands
parentid=$1
action=$2

##########################################################
## attack config: 1Gbps bottleneck on NIC of shared server
## 2 traffic classes:
##  victim + others: prio 1
##  attack: prio 2
##########################################################
if [[ $action == "add" ]]; then
	tc class $action dev eno1 parent $parentid: classid $parentid:1 htb rate 1gbit burst 1600 cburst 1600
	tc class $action dev eno1 parent $parentid:1 classid $parentid:10 htb rate 1gbit ceil 1gbit burst 100m cburst 100m prio 1
	tc class $action dev eno1 parent $parentid:1 classid $parentid:20 htb rate 65mbit ceil 1gbit burst 1600 cburst 1600 prio 2
	tc qdisc $action dev eno1 parent $parentid:10 handle 10: fq limit 90000 flow_limit 90000 quantum 8442688 nopacing
	tc qdisc $action dev eno1 parent $parentid:20 handle 20: fq limit 90000 flow_limit 90000 quantum 8442688 nopacing
	tc filter $action dev eno1 parent $parentid:0 protocol ip prio 1 u32 match ip src 139.19.171.101/32 match ip sport 443 0xffff flowid $parentid:10 classid $parentid:10
	tc filter $action dev eno1 parent $parentid:0 protocol ip prio 2 u32 match ip src 139.19.171.102/32 match ip sport 2230 0xffff flowid $parentid:20 classid $parentid:20
fi

## deletion must be in reverse order of addition
if [[ $action == "del" ]]; then
	tc filter $action dev eno1 parent $parentid:0 protocol ip prio 2 u32 match ip src 139.19.171.102/32 match ip sport 2230 0xffff flowid $parentid:20 classid $parentid:20
	tc filter $action dev eno1 parent $parentid:0 protocol ip prio 1 u32 match ip src 139.19.171.101/32 match ip sport 443 0xffff flowid $parentid:10 classid $parentid:10
	tc qdisc $action dev eno1 parent $parentid:20 handle 20: fq limit 90000 flow_limit 90000 quantum 8442688 nopacing
	tc qdisc $action dev eno1 parent $parentid:10 handle 10: fq limit 90000 flow_limit 90000 quantum 8442688 nopacing
	tc class $action dev eno1 parent $parentid:1 classid $parentid:20 htb rate 65mbit ceil 1gbit burst 1600 cburst 1600 prio 2
	tc class $action dev eno1 parent $parentid:1 classid $parentid:10 htb rate 1gbit ceil 1gbit burst 100m cburst 100m prio 1
	tc class $action dev eno1 parent $parentid: classid $parentid:1 htb rate 1gbit burst 1600 cburst 1600
#	tc qdisc $action dev eno1 root handle 0: htb
fi

exit

##########################################################
## attack config: 10Gbps on NIC of shared server
## 2 traffic classes:
##  victim + others: prio 1
##  attack: prio 2
##########################################################
if [[ $action == "add10" ]]; then
	tc class $action dev eno1 parent $parentid: classid $parentid:1 htb rate 10gbit burst 5000 cburst 5000
	tc class $action dev eno1 parent $parentid:1 classid $parentid:10 htb rate 10gbit ceil 10gbit burst 1000m cburst 1000m prio 1
	tc class $action dev eno1 parent $parentid:1 classid $parentid:20 htb rate 650mbit ceil 10gbit burst 2600 cburst 5000 prio 2
	tc qdisc $action dev eno1 parent $parentid:10 handle 10: fq limit 900000 flow_limit 900000 quantum 8442688 nopacing
	tc qdisc $action dev eno1 parent $parentid:20 handle 20: fq limit 900000 flow_limit 900000 quantum 8442688 nopacing
	tc filter $action dev eno1 parent $parentid:0 protocol ip prio 1 u32 match ip src 139.19.171.101/32 match ip sport 443 0xffff flowid $parentid:10 classid $parentid:10
	tc filter $action dev eno1 parent $parentid:0 protocol ip prio 2 u32 match ip src 139.19.171.102/32 match ip sport 2230 0xffff flowid $parentid:20 classid $parentid:20
fi

## deletion must be in reverse order of addition
if [[ $action == "del10" ]]; then
	tc filter $action dev eno1 parent $parentid:0 protocol ip prio 2 u32 match ip src 139.19.171.102/32 match ip sport 2230 0xffff flowid $parentid:20 classid $parentid:20
	tc filter $action dev eno1 parent $parentid:0 protocol ip prio 1 u32 match ip src 139.19.171.101/32 match ip sport 443 0xffff flowid $parentid:10 classid $parentid:10
	tc qdisc $action dev eno1 parent $parentid:20 handle 20: fq limit 900000 flow_limit 900000 quantum 8442688 nopacing
	tc qdisc $action dev eno1 parent $parentid:10 handle 10: fq limit 900000 flow_limit 900000 quantum 8442688 nopacing
	tc class $action dev eno1 parent $parentid:1 classid $parentid:20 htb rate 650mbit ceil 10gbit burst 1600 cburst 5000 prio 2
	tc class $action dev eno1 parent $parentid:1 classid $parentid:10 htb rate 10gbit ceil 10gbit burst 1000m cburst 1000m prio 1
	tc class $action dev eno1 parent $parentid: classid $parentid:1 htb rate 10gbit burst 5000 cburst 5000
#	tc qdisc $action dev eno1 root handle 0: htb
fi

exit
