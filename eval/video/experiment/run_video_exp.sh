#!/bin/bash

############
## constants
############
cfg_base=0
cfg_mod_irq=1
cfg_enf=2

rootdir="/local/sme"
workdir="$rootdir/workspace/pacer"
clientdir="$workdir/apps/videoclient"

fe_gpacedir="$workdir/frontend/gpace/linux-frontend-4.9.5"
be_gpacedir="$workdir/backend/gpace/linux-backend-4.9.5"
cl_gpacedir="$workdir/client/gpace/linux-client-4.9.5"

fe_hypacedir="$workdir/frontend/hypace/xen-frontend-4.10.0"
be_hypacedir="$workdir/backend/hypace/xen-backend-4.10.0"

fe_gpacekmod="$workdir/frontend/gpace/kmod-frontend"
be_gpacekmod="$workdir/backend/gpace/kmod-backend"
cl_gpacekmod="$workdir/client/gpace/kmod-client"

fe_gpacescripts="$workdir/frontend/gpace/scripts"
be_gpacescripts="$workdir/backend/gpace/scripts"
cl_gpacescripts="$workdir/client/gpace/scripts"

fe_profpacedir="$workdir/frontend/profpace"
be_profpacedir="$workdir/backend/profpace"

outdir="$rootdir/exp/videos/$(date +%y%m%d-%H%M)"
daemon_dir="$rootdir/daemon_profiler"

ruser="root"

mkdir -p $outdir

############
## variables
############

## these IPs are used to setup kernel,
## and collect kernel logs, gitinfo properly.
appvm_ip="139.19.218.101" ## VM IP
maxwell_ip="139.19.218.13"
## temp change, required on maxwell15 only because
## libvirtd was enabled, thus enabling virbr0 interface
## which caused hostname -I to show two IPs
client_ip=$(hostname -I | cut -d' ' -f1)
#client_ip=$(hostname -I | cut -d' ' -f2)

################################
## not relevant for video service
#dbvm_ip=$client_ip
#db_app_port=3306
dbvm_ip="139.19.218.104"
dbhost_ip="139.19.218.12"

db_app_port=11211 ## memcached
db_arr=(
  "139.19.218.103","139.19.218.12"
  "139.19.218.104","139.19.218.11"
)

## deprecated
dummy_ip="139.19.218.102"
dummy_port=2231

################################

dryrun=0
do_stat=0
do_ktrace=0
do_kmemcheck=0
do_tcpdump_remote=0
do_tcpdump_local=0

. "cmd.sh"

function apache_restart()
{
  ruser=$1
  host=$2

  cmd=""
  hostapdir="/usr/local/apache2"
  cmd=$cmd"rm -f $hostapdir/logs/http.pid; sleep 1; "
  cmd=$cmd"$hostapdir/bin/apachectl -k stop; sleep 2; "
  if [[ $cfg -eq $cfg_base ]]; then
    cmd=$cmd"cp $hostapdir/modules/mod_ssl_base.so $hostapdir/modules/mod_ssl.so; "
  else
    cmd=$cmd"cp $hostapdir/modules/mod_ssl_enf.so $hostapdir/modules/mod_ssl.so; "
  fi
  cmd=$cmd"$hostapdir/bin/apachectl -k start; "
  do_cmd $ruser $host "$cmd"
}

function trunc_logs()
{
  ruser=$1
  host=$2
  cmd="echo \"\" > /var/log/apache2/access.log"
  do_cmd $ruser $host "$cmd"
}

function run_wondershaper()
{
  echo "Restricting client bandwidth to 10000 Kbps"
  sudo wondershaper eno1 10000 10000
}

function clear_wondershaper()
{
  echo "Unrestricting client bandwidth"
  sudo wondershaper clear eno1
}

function start_vmstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="vmstat 1 > /local/sme/exp/vmstat.out"
    do_cmd $ruser $host "$cmd" 1 1
  fi
}

function stop_vmstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="pkill -9 vmstat"
    do_cmd $ruser $host "$cmd"
  fi
}

function start_pidstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="pidstat -C \"apache2|pacer|ackthread\" -u -w -r -h 1 > /local/sme/exp/pidstat.out"
    do_cmd $ruser $host "$cmd" 1 1
  fi
}

function stop_pidstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="pkill -9 pidstat"
    do_cmd $ruser $host "$cmd"
  fi
}

function start_mpstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="mpstat -u -P ALL 1 > /local/sme/exp/mpstat.out"
    do_cmd $ruser $host "$cmd" 1 1
  fi
}

function stop_mpstat()
{
  if [[ $do_stat -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="pkill -9 mpstat"
    do_cmd $ruser $host "$cmd"
  fi
}

function start_kedr()
{
  if [[ $do_kmemcheck -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="kedr start sme_lsm"
    do_cmd $ruser $host "$cmd"
  fi
}

function stop_kedr()
{
  if [[ $do_kmemcheck -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="kedr stop"
    do_cmd $ruser $host "$cmd"
  fi
}

function get_kedr_info()
{
  if [[ $do_kmemcheck -eq 1 ]]; then
    ruser=$1
    host=$2
    cmd="cat /sys/kernel/debug/kedr_leak_check/info"
    cmd=$cmd" > /local/sme/exp/kedr.out;"
    cmd=$cmd" cat /sys/kernel/debug/kedr_leak_check/possible_leaks"
    cmd=$cmd" > /local/sme/exp/kedr_leaks.out;"
    do_cmd $ruser $host "$cmd"
  fi
}

function run_profile_logger()
{
  # runtype:
  # 0 - use_log_dir and set profile
  # 1 - run as a daemon (make sure to invoke stop_profile_logger)
  # 2 - run for n requests (set prof_for_n_req flag)
  ruser=$1
  host=$2
  runtype=$7
  totalreq=$8
  scheddir=$9
  profdir=${10}

  echo "Run profile logger: exp cfg $cfg, runtype $runtype"

  if [[ $cfg -ne $cfg_enf ]]; then
    return
  fi

  cmd="cd $profdir; python sme_profiler.py"
  cmd=$cmd" --dummy_ip $dummy_ip --dummy_port $dummy_port"
  cmd=$cmd" --ep_ip1 \"0.0.0.0\" --ep_port1 0 --ep_ip2 \"$3\" --ep_port2 $4"
  cmd=$cmd" --ep_ip3 \"$3\" --ep_port3 0 --ep_ip4_list $5 --ep_port4 $6"

  if [[ $runtype -eq 0 ]]; then
    cmd=$cmd" --update_prof_freq 0"
    cmd=$cmd" --gen_prof_freq $totalreq"
    cmd=$cmd" --use_logs_dir \"$daemon_dir/28_12_2017-18_25/kernel_logs\""
    cmd=$cmd" --keep_logs 0"
    do_cmd $ruser $host "$cmd" 0
  elif [[ $runtype -eq 1 ]]; then
    cmd=$cmd" --update_prof_freq 0"
    do_cmd $ruser $host "$cmd" 1
  elif [[ $runtype -eq 2 ]]; then
    cmd=$cmd" --update_prof_freq 0"
    cmd=$cmd" --pace_log 0"
    #cmd=$cmd" --prof_for_n_req $totalreq"
    #cmd=$cmd" --gen_prof_freq 1"
    echo "ssh $ruser@$host \"$cmd\""
    ssh $ruser@$host "$cmd" &
    echo "REMOTE LOGGER PID: $!"
  elif [[ $runtype -eq 3 ]]; then
    cmd=$cmd" --update_prof_freq 1"
    cmd=$cmd" --send_ascii_profiles_dir $daemon_dir/$scheddir"
    do_cmd $ruser $host "$cmd" 0
  fi

}

function stop_profile_logger()
{
  ruser=$1
  host=$2
  if [[ $cfg -eq $cfg_enf ]]; then
    cmd="pkill -15 python"
    do_cmd $ruser $host "$cmd"
  fi
}

function run_tcpdump()
{
  ruser=$1
  host=$2
  serv_ip=$3
  clnt_ip=$4
  edev=$5
  multitier=$8
  dbvm_ip=$9
  dbvm_port=${10}
  cmd=""
  cmd=$cmd"mkdir -p $rootdir/tdump; "
  cmd=$cmd"python $profdir/network_profiler.py"
  cmd=$cmd" --server-ip $serv_ip"
  cmd=$cmd" --client-ip $clnt_ip"
  cmd=$cmd" --iface $edev"
  cmd=$cmd" --transport $6"
  cmd=$cmd" --server-port $7"
  cmd=$cmd" --dumpfile $rootdir/tdump/$cfgstr.$serv_ip-$clnt_ip.tdump"
  cmd=$cmd" --outfile $rootdir/tdump/$cfgstr.$serv_ip-$clnt_ip"

  if [[ "x$multitier" != "x" ]] && [[ $multitier -eq 1 ]]; then
    cmd=$cmd" --multi-tier $multitier"
    cmd=$cmd" --tier2-ip $dbvm_ip"
    cmd=$cmd" --tier2-port $dbvm_port"
  fi
  cmd=$cmd" --action 0; "
  tcpdump_pid=$(ssh -f $ruser@$host "$cmd")
  echo "$tcpdump_pid"
  echo "ssh $ruser@$host \"$cmd\""
}

function stop_tcpdump()
{
  ruser=$1
  host=$2
  serv_ip=$3
  clnt_ip=$4
  tcpdump_pid=$5
  echo $tcpdump_pid
  cmd=""
  cmd=$cmd"python $profdir/network_profiler.py"
  cmd=$cmd" --server-ip $serv_ip"
  cmd=$cmd" --client-ip $clnt_ip"
  cmd=$cmd" --transport $6"
  cmd=$cmd" --server-port $7"
  cmd=$cmd" --dumpfile $rootdir/tdump/$cfgstr.$serv_ip-$clnt_ip.tdump"
  cmd=$cmd" --outfile $rootdir/tdump/$cfgstr.$serv_ip-$clnt_ip"
  cmd=$cmd" --action $tcpdump_pid"
  do_cmd $ruser $host "$cmd" 0 1
}

function get_gitinfo()
{
  ruser=$1
  host=$2
  dir=$3
  cmd="cd $dir; git show --name-status > $dir/gitinfo; "
  cmd=$cmd"echo \"\" >> $dir/gitinfo; "
  cmd=$cmd"git diff >> $dir/gitinfo; "
  do_cmd $ruser $host "$cmd"
}

function do_setup()
{
  ## ==== generate git info ====
  get_gitinfo $ruser "$appvm_ip" $workdir
  get_gitinfo $ruser "$appvm_ip" $fe_gpacedir
  get_gitinfo $ruser "$maxwell_ip" $workdir
  get_gitinfo $ruser "$maxwell_ip" $fe_hypacedir

  get_gitinfo $ruser "$client_ip" $workdir
  get_gitinfo $ruser "$client_ip" $cl_gpacedir

  if [[ $do_multitier -eq 1 ]]; then
    for dbvm_host in "${db_arr[@]}"; do
      IFS=","
      set -- $dbvm_host
      dbvm_ip=$1
      dbhost_ip=$2
      if [[ $dbvm_ip != $appvm_ip ]]; then
        get_gitinfo $ruser "$dbvm_ip" $workdir
        get_gitinfo $ruser "$dbvm_ip" $be_gpacedir
      fi
      if [[ $dbhost_ip != $maxwell_ip ]]; then
        get_gitinfo $ruser "$dbhost_ip" $workdir
        get_gitinfo $ruser "$dbhost_ip" $be_hypacedir
      fi
    done
  fi

  ## ==== setup hosts ====
  run_once_setup $ruser "$appvm_ip" $fe_gpacescripts

  ## ==== apache setup ====
  trunc_logs $ruser "$appvm_ip";
  apache_restart $ruser "$appvm_ip";

  ## ==== perf and mem monitoring ====
  start_kedr $ruser "$appvm_ip";
#  start_vmstat $ruser "$appvm_ip";
  start_pidstat $ruser "$appvm_ip";
  start_mpstat $ruser "$appvm_ip";

  start_mpstat $ruser "$client_ip"

  if [[ $do_multitier -eq 1 ]]; then
    for dbvm_host in "${db_arr[@]}"; do
      IFS=","
      set -- $dbvm_host
      dbvm_ip=$1
      dbhost_ip=$2
      if [[ $dbvm_ip != $appvm_ip ]]; then
        start_pidstat $ruser "$dbvm_ip"
        start_mpstat $ruser "$dbvm_ip"
      fi
    done
  fi

  ## === wait for everything to be setup clearly ===
  sleep 1

  ## ==== disable C-states on server ====
  run_dom0_setup $ruser "$maxwell_ip"

  ## ==== insmod ====
  dbvm_ip_list=""
  dbvm_ip_list2=""
  for dbvm_host in "${db_arr[@]}"; do
    IFS=","
    set -- $dbvm_host
    dbvm_ip=$1
    dbhost_ip=$2
    dbvm_ip_list=$dbvm_ip_list"\"$dbvm_ip\" "
    dbvm_ip_list2=$dbvm_ip_list2"$dbvm_ip,"
  done
  dbvm_ip_str=${dbvm_ip_list:0:-1}
  dbvm_ip_str2=${dbvm_ip_list2:0:-1}
  echo "DBVM IP LIST: $dbvm_ip_str"
  echo "DBVM IP LIST2: $dbvm_ip_str2"

  IFS=" "
  run_host_setup $ruser "$appvm_ip" "$appvm_ip" $app_client_port "FE" \
    $dbvm_ip_str2 $db_app_port \
    $serv_epoch $serv_spin_thresh $serv_max_pkts_per_epoch  \
    $serv_max_other_per_epoch $fe_gpacekmod

  IFS=","
  if [[ $do_multitier -eq 1 ]]; then
    for dbvm_host in "${db_arr[@]}"; do
      set -- $dbvm_host
      dbvm_ip=$1
      dbhost_ip=$2
      echo "XXXXXXXXXX HOST SETUP: $dbvm_host || $dbvm_ip || $dbhost_ip XXXXXXXXXX"
        run_once_setup $ruser "$dbvm_ip" $be_gpacescripts
        run_dom0_setup $ruser "$dbhost_ip"
      if [[ $dbvm_ip != $appvm_ip ]]; then
        run_host_setup $ruser "$dbvm_ip" "$dbvm_ip" $db_app_port "BE" \
          $dbvm_ip $db_app_port \
          $serv_epoch $serv_spin_thresh $serv_max_pkts_per_epoch  \
          $serv_max_other_per_epoch $be_gpacekmod

        if [[ $send_ascii_profile_for_enf -eq 1 ]]; then
          run_profile_logger $ruser $dbvm_ip $dbvm_ip $db_app_port  \
            $dbvm_ip $db_app_port 3 $totalreq $be_sched_dir $be_profpacedir
        fi
      fi ## dbvm_ip != appvm_ip
    done
  fi ## do_multitier

  if [[ $cfg_enf_on_client -eq 1 ]]; then
    run_once_setup $ruser "$client_ip" $cl_gpacescripts
    run_host_setup $ruser "$client_ip" "$appvm_ip" $app_client_port "CLIENT"  \
      $client_ip $app_client_port  \
      $client_epoch $client_spin_thresh $client_max_pkts_per_epoch  \
      $client_max_other_per_epoch $cl_gpacekmod
  fi

  if [[ $send_ascii_profile_for_enf -eq 1 ]]; then
    ## ==== run profiler to set custom profile in kernels ====
    run_profile_logger $ruser $appvm_ip $appvm_ip $app_client_port  \
      $dbvm_ip_str $db_app_port 3 $totalreq $fe_sched_dir $fe_profpacedir
  fi

  ## ==== generate profile from kernel logs and set profile in kernels ====
#  run_profile_logger $ruser $appvm_ip $appvm_ip $app_client_port  \
#    $dbvm_ip $db_app_port 0 $totalreq $fe_sched_dir $fe_profpacedir

  if [[ $run_profiler_during_exp -eq 1 ]]; then
    ## ==== without custom prof enabled, run profiler for n reqs ====
    run_profile_logger $ruser $appvm_ip $appvm_ip $app_client_port \
      $dbvm_ip $db_app_port 2 $totalreq $fe_sched_dir $fe_profpacedir
  fi

  ## ==== with custom prof enabled, run profiler in daemon mode ====
#  run_profile_logger $ruser $appvm_ip $appvm_ip $app_client_port  \
#    $dbvm_ip $db_app_port 1 $totalreq $fe_sched_dir $fe_profpacedir

}

function do_shutdown()
{
  ## ==== stop profiler daemon in FE dom0 userspace ====
#  stop_profile_logger $ruser $appvm_ip

  ## ==== rmmod ====
  if [[ $do_multitier -eq 1 ]]; then
    for dbvm_host in "${db_arr[@]}"; do
      IFS=","
      set -- $dbvm_host
      dbvm_ip=$1
      dbhost_ip=$2
      if [[ $dbvm_ip != $appvm_ip ]]; then
        run_host_shutdown $ruser "$dbvm_ip" $be_gpacescripts
      fi
    done
  fi

  run_host_shutdown $ruser "$appvm_ip" $fe_gpacescripts

  if [[ $cfg_enf_on_client -eq 1 ]]; then
    run_host_shutdown $ruser "$client_ip" $cl_gpacescripts
  fi

  ## ==== get kmem leak info ====
  get_kedr_info $ruser "$appvm_ip";
  stop_kedr $ruser "$appvm_ip";

  ## ==== stop stat tools ====
#  stop_vmstat $ruser "$appvm_ip";
  stop_pidstat $ruser "$appvm_ip";
  stop_mpstat $ruser "$appvm_ip";

  stop_mpstat $ruser "$client_ip";

  if [[ $do_multitier -eq 1 ]]; then
    for dbvm_host in "${db_arr[@]}"; do
      IFS=","
      set -- $dbvm_host
      dbvm_ip=$1
      dbhost_ip=$2
      if [[ $dbvm_ip != $appvm_ip ]]; then
        stop_pidstat $ruser "$dbvm_ip"
        stop_mpstat $ruser "$dbvm_ip"
      fi
    done
  fi

}

function copy_logs()
{
  ## ==== copy stats ====
  scp $ruser@$appvm_ip:$workdir/gitinfo $gitinfo.FE-gpace
  scp $ruser@$appvm_ip:$fe_gpacedir/gitinfo $gitinfo.FE-linux
  scp $ruser@$maxwell_ip:$workdir/gitinfo $gitinfo.FE-hypace
  scp $ruser@$maxwell_ip:$fe_hypacedir/gitinfo $gitinfo.FE-xen

  cp $workdir/gitinfo $gitinfo.CL-gpace
  cp $cl_gpacedir/gitinfo $gitinfo.CL-linux

  if [[ $do_multitier -eq 1 ]]; then
    for dbvm_host in "${db_arr[@]}"; do
      IFS=","
      set -- $dbvm_host
      dbvm_ip=$1
      dbhost_ip=$2
      if [[ $dbvm_ip != $appvm_ip ]]; then
        scp $ruser@$dbvm_ip:$workdir/gitinfo $gitinfo.BE-gpace.$dbvm_ip
        scp $ruser@$dbvm_ip:$be_gpacedir/gitinfo $gitinfo.BE-linux.$dbvm_ip
      fi
      if [[ $dbhost_ip != $appvm_ip ]]; then
        scp $ruser@$dbhost_ip:$workdir/gitinfo $gitinfo.BE-hypace.$dbhost_ip
        scp $ruser@$dbhost_ip:$be_hypacedir/gitinfo $gitinfo.BE-xen.$dbhost_ip
      fi
    done
  fi

  if [[ $cfg -ge $cfg_mod_irq ]]; then
    scp $ruser@$appvm_ip:/var/log/kern.log $sysfile.FE-gpace
    scp $ruser@$maxwell_ip:/var/log/syslog $sysfile.FE-hypace
    scp $ruser@$appvm_ip:"$daemon_dir/$fe_sched_dir/FE-Client.4D570000000000000000000000000000.1523553451487.profile" "$outdir/default_profile"

    if [[ $do_multitier -eq 1 ]]; then
      for dbvm_ip in "${dbmv_ip_arr[@]}"; do
        if [[ $dbvm_ip != $appvm_ip ]]; then
          scp $ruser@$dbvm_ip:/var/log/kern.log $sysfile.BE-gpace.$dbvm_ip
        fi
      done
    fi
  fi

  if [[ $cfg_enf_on_client -eq 1 ]]; then
    cp /var/log/kern.log $sysfile.CL-gpace
  fi

  scp $ruser@$appvm_ip:/var/log/apache2/access.log $apachelog

  if [[ $do_stat -eq 1 ]]; then
#    scp $ruser@$appvm_ip:/local/sme/exp/vmstat.out $vmstatfile
    scp $ruser@$appvm_ip:/local/sme/exp/pidstat.out "$pidstatfile.FE-gpace"
    scp $ruser@$appvm_ip:/local/sme/exp/mpstat.out "$mpstatfile.FE-gpace"
    cp /local/sme/exp/mpstat.out "$mpstatfile.CL-gpace"

    if [[ $do_multitier -eq 1 ]]; then
      for dbvm_host in "${db_arr[@]}"; do
        IFS=","
        set -- $dbvm_host
        dbvm_ip=$1
        dbhost_ip=$2
        if [[ $dbvm_ip != $appvm_ip ]]; then
          scp $ruser@$dbvm_ip:/local/sme/exp/pidstat.out "$pidstatfile.BE-gpace.$dbvm_ip"
          scp $ruser@$dbvm_ip:/local/sme/exp/mpstat.out "$mpstatfile.BE-gpace.$dbvm_ip"
        fi ## dbvm_ip != appvm_ip
      done
    fi ## do_multitier
  fi ## do_stat

  if [[ $do_kmemcheck -eq 1 ]]; then
    scp $ruser@$appvm_ip:/local/sme/exp/kedr.out $kedroutfile.FE-gpace
    scp $ruser@$appvm_ip:/local/sme/exp/kedr_leaks.out $kedrleakfile.FE-gpace
  fi

  if [[ $cfg -eq $cfg_enf ]]; then
    cmd="xl dmesg -c > /local/sme/xenstat;"
    #cmd="xl dmesg > /local/sme/xenstat"
    do_cmd $ruser $maxwell_ip "$cmd"
    scp $ruser@$maxwell_ip:/local/sme/xenstat $xenstat.FE-xen

    if [[ $do_multitier -eq 1 ]]; then
      for dbvm_host in "${db_arr[@]}"; do
        IFS=","
        set -- $dbvm_host
        dbvm_ip=$1
        dbhost_ip=$2
        if [[ $dbvm_ip != $appvm_ip ]] && [[ $dbhost_ip != $maxwell_ip ]]; then
          cmd="xl dmesg -c > /local/sme/xenstat;"
          do_cmd $ruser $dbhost_ip "$cmd"
          scp $ruser@$dbhost_ip:/local/sme/xenstat $xenstat.BE-xen.$dbhost_ip
        fi ## dbvm_ip != appvm_ip && dbhost_ip != maxwell_ip
      done
    fi ## do_multitier

  fi ## cfg = cfg_enf
}

function run_exp()
{
  maxcpus=$1
  nreq=$2

  suffix="n$iter""-c$client"".$cfgstr"
  abfile="$outdir/ab.$suffix"
  outfile="$outdir/out.$suffix"
  sysfile="$outdir/syslog.$suffix"
  vmstatfile="$outdir/vmstat.$suffix"
  pidstatfile="$outdir/pidstat.$suffix"
  mpstatfile="$outdir/mpstat.$suffix"
  mpavgfile="$outdir/mpavg.$suffix"
  intrfile="$outdir/intr.$suffix"
  sirqfile="$outdir/sirq.$suffix"
  kedroutfile="$outdir/kedr.$suffix"
  kedrleakfile="$outdir/kleak.$suffix"
  apachelog="$outdir/apache.$suffix"
  xenstat="$outdir/xenstat.$suffix"
  tsharkfile="$outdir/tshark.$suffix"
  gitinfo="$outdir/gitinfo.$suffix"

  echostr="$(date) cfg: $cfgstr nvids: $num_videos dur: $play_dur"
  echostr=$echostr" multitier: $do_multitier"
  if [[ $cfg -eq $cfg_enf ]]; then
    echostr=$echostr" epoch: $serv_epoch spinthresh: $serv_spin_thresh"
    echostr=$echostr" pkts/epoch: $serv_max_pkts_per_epoch"
    echostr=$echostr" other/epoch: $serv_max_other_per_epoch"
  fi
  echostr=$echostr" clients: $client iter: $iter"
  echo $echostr

  totalreq=$(($nreq * $maxcpus))

  (

    if [[ $bwcfg == "10m" ]]; then
      run_wondershaper
    fi

    do_setup

    ## ==== start tcpdump ====
    if [[ $do_tcpdump_remote -eq 1 ]]; then
      edev=$(ssh $ruser@$appvm_ip "ifconfig | grep -B1 \"$appvm_ip\" | head -1 | cut -d' ' -f1")
      fe_tcpdump_pid=$(run_tcpdump $ruser "$appvm_ip" "$appvm_ip" "$client_ip" \
        "$edev" "tcp" $app_client_port 1 $dbvm_ip $db_app_port)
      echo "REMOTE TCPDUMP PID: $fe_tcpdump_pid"
      ## this is needed, because run_tcpdump echoes the PID followed by
      ## the tcpdump command string. we do not want to accidentally pass
      ## the whole string returned from the function as an argument to
      ## stop_tcpdump later.
      read fe_tcpdump_pid <<< "$fe_tcpdump_pid"

      if [[ $do_multitier -eq 1 ]]; then
        for dbvm_host in "${db_arr[@]}"; do
          IFS=","
          set -- $dbvm_host
          dbvm_ip=$1
          dbhost_ip=$2
          if [[ $dbvm_ip != $appvm_ip ]]; then
            edev=$(ssh $ruser@$dbvm_ip "ifconfig | grep -B1 \"$dbvm_ip\" | head -1 | cut -d' ' -f1")
            be_tcpdump_pid=$(run_tcpdump $ruser "$dbvm_ip" "$dbvm_ip" "$appvm_ip" \
              "$edev" "tcp" $db_app_port)
            echo "BACKEND TCPDUMP PID: $be_tcpdump_pid"
            read be_tcpdump_pid <<< "$be_tcpdump_pid"
          fi ## dbvm_ip != appvm_ip
        done ## dbvm_ip loop
      fi ## do_multitier
    fi ## do_tcpdump_remote

    if [[ $do_tcpdump_local -eq 1 ]]; then
      edev=`ifconfig | grep -B1 $client_ip | head -1 | cut -d' ' -f1`
      edev="eno1"
      local_tcpdump_pid=$(run_tcpdump $ruser "$client_ip" "$appvm_ip" "$client_ip"  \
        "$edev" "tcp" $app_client_port)
      echo "LOCAL TCPDUMP PID: $local_tcpdump_pid"
      read local_tcpdump_pid <<< "$local_tcpdump_pid"
    fi
    sleep 5

    date;

    run_vs
#    run_memc_only

    ## ==== wait for clients to finish ====
    FAIL=0
    joblist=`jobs -p`
    while read -r job; do
      echo "Waiting for $job to complete.."
      wait $job || let "FAIL+=1"
    done <<< "$joblist";

    if [[ "$FAIL" == "0" ]]; then
      echo "Success!"
    else
      echo "Fail ($FAIL)"
    fi;
    date;

    ## ==== shutdown ====

    sleep 60

    do_shutdown

    sleep 5

    if [[ $bwcfg == "10m" ]]; then
      clear_wondershaper
    fi

    copy_logs

    ## ==== stop tcpdump ====
    if [[ $do_tcpdump_remote -eq 1 ]]; then
      echo "Stop remote tcpdump"
      stop_tcpdump $ruser "$appvm_ip" "$appvm_ip" "$client_ip" $fe_tcpdump_pid \
        "tcp" $app_client_port

      if [[ $do_multitier -eq 1 ]]; then
        for dbvm_host in "${db_arr[@]}"; do
          IFS=","
          set -- $dbvm_host
          dbvm_ip=$1
          dbhost_ip=$2
          if [[ $dbvm_ip != $appvm_ip ]]; then
            echo "Stop backend tcpdump"
            stop_tcpdump $ruser "$dbvm_ip" "$dbvm_ip" "$appvm_ip" $be_tcpdump_pid \
              "tcp" $db_app_port
          fi ## dbvm_ip != appvm_ip
        done ## dbvm_ip loop
      fi ## do_multitier
    fi ## do_tcpdump_remote

    if [[ $do_tcpdump_local -eq 1 ]]; then
      echo "Stop local tcpdump"
      stop_tcpdump $ruser "$client_ip" "$appvm_ip" "$client_ip" $local_tcpdump_pid  \
        "tcp" $app_client_port
    fi

    sleep 2;

    if [[ $do_tcpdump_remote -eq 1 ]]; then
      echo "Copying remote tshark"
      scp $ruser@$appvm_ip:"$rootdir/tdump/$cfgstr.$appvm_ip-$client_ip.tshark" \
        "$tsharkfile.fe"

      if [[ $do_multitier -eq 1 ]]; then
        for dbvm_host in "${db_arr[@]}"; do
          IFS=","
          set -- $dbvm_host
          dbvm_ip=$1
          dbhost_ip=$2
          if [[ $dbvm_ip != $appvm_ip ]]; then
            echo "Copying backend tshark"
            scp $ruser@$dbvm_ip:"$rootdir/tdump/$cfgstr.$dbvm_ip-$appvm_ip.tshark"  \
              "$tsharkfile.be"
          fi ## dbvm_ip != appvm_ip
        done ## dbvm_ip loop
      fi ## do_multitier
    fi ## do_tcpdump_remote

    if [[ $do_tcpdump_local -eq 1 ]]; then
      echo "Copying local tshark"
      cp "$rootdir/tdump/$cfgstr.$appvm_ip-$client_ip.tshark" "$tsharkfile.client"
    fi

  ) > $outfile

}

function run_vs()
{
  clist=`seq 1 $client`
  while read -r c; do
    echo "Executing client $c..."
    url="benchVideo.php?c=$rcfgid&d=0&m=$do_multitier&s=$c&f=./videos"

    python3 $clientdir/streaming_requests.py -I $appvm_ip -p $proto  \
      -u "$url" -f "$clientdir/videolist" -s $c -c $n_clusters \
      -ps $play_sleep -dur $play_dur -bgs $bufgoal_s -rbgs $rebufgoal_s \
      -nv $num_videos > $outfile.$c 2>&1 &
  done <<< $clist
}

function run_memc_only()
{
  cmd="php -f $workdir/scripts/memcache_client.php d=1 f=./videos"
  cmd=$cmd"/yt-2uxtcy4FpN8-frag/n20-mpd-2uxtcy4FpN8.mpd i=\"8--1\""
  do_cmd $ruser $appvm_ip "$cmd" 0 1
}

cfg=$cfg_base ## cfg to run baseline (no insmod, rmmod, sme profiler)
#cfg=$cfg_enf ## cfg to run insmod, rmmod, sme profiler

cfg_enf_on_client=0
send_ascii_profile_for_enf=0
run_profiler_during_exp=0
play_sleep=5
play_dur=120
bufgoal_s=60
rebufgoal_s=30
num_videos=1
do_multitier=0

if [[ $do_multitier -eq 0 ]]; then
  fe_sched_dir="schedules_video_1tier"
else
  fe_sched_dir="schedules_video_2tier_frontend"
  be_sched_dir="schedules_video_2tier_backend"
fi

#bwcfg="10m" ## 10mbps
bwcfg="10g" ## 10gbps

nreq=1
maxcpus=1
client_epoch=0
client_spin_thresh=0
client_max_pkts_per_epoch=0
client_max_other_per_epoch=0

spin_epoch_set=(  \
    35000,120000,38,0 \
  )

for app_client_port in 443
do
  proto="https"

  for spin_epoch in "${spin_epoch_set[@]}";
  do
    IFS=","
    set -- $spin_epoch
    serv_spin_thresh=$1
    serv_epoch=$2
    serv_max_pkts_per_epoch=$3
    serv_max_other_per_epoch=$4

    for client in 1 2 4 8 16 32 64 128
    do

      for iter in 1 2 3
      do

        if [[ $cfg -eq $cfg_base ]]; then
          rcfgid=0
          cfgstr="base$bwcfg-$proto"
          n_clusters=0
        else
          rcfgid=1
          cfgstr="sme$bwcfg-$proto"
          n_clusters=20
          #n_clusters=0
        fi

        ## ==== run experiment ====
        run_exp $maxcpus $nreq

        sleep 5

      done ## iter
    done ## client
  done ## serv_spin_thresh, serv_epoch, serv_max_pkts, serv_max_other
done ## app_client_port

date
