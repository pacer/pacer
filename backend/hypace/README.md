# Setting up HyPace

## Steps to boot xen and hypace
* compile `hypace/xen-4.10.0` and install from source
* copy `configs/dom0.grub` to `/etc/default/grub` in dom0
* copy `configs/dom0.xen.cfg` to `/etc/default/grub.d/xen.cfg` in dom0
* configure SR-IOV, disable hyperthreading, and set power profile to "performance" in BIOS

## Detailed BIOS config
- CPU power mgmt: Max Performance
- Memory frequency: Max Performance
- Turbo boost: Disabled
- Energy Efficient Turbo: Disabled
- C1E: Disabled
- C states: Disabled
- Write data CRC: Disabled
- Collaborative CPU Perf control: Disabled
- Memory patrol scrub: Standard
- Memory refresh rate: 1x
- Uncore frequency: Maximum
- Energy Efficient policy: Performance
- Number of turbo boost enabled cores for processor 1, processor 2: All
- Adjacent cache line prefetch: Disabled

## After reboot
* disable DVFS: `xenpm set-max-cstate 0`
* use scripts/vfsetup.sh to initialize `N` VFs, where `N` depends on the number of guest VMs to be run on the physical host. requires a csv file of `<mac address,ip addr>` mapping.
* boot a guest using `xl create` command.
