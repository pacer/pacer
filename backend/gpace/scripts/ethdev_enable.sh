#!/bin/bash

for ifstr in "eno1" "enp0s0" "eth0"; do
  ifline=`ifconfig | grep $ifstr`
  if [[ "x$ifline" != "x" ]]; then
    iface=`echo $ifline | cut -d' ' -f1`
    break
  fi
done

edev=$iface
echo "enable debug msges for INTR, RX_STATUS, TX_DONE on $edev..."
#ethtool -s $edev msglvl intr on rx_status on tx_done on probe on

let "ml_probe = 0x0002"
let "ml_ifdown = 0x0010"
let "ml_rx_err = 0x0040"
let "ml_tx_msg_queue = 0x0100"
let "ml_intr = 0x0200"
let "ml_tx_done = 0x0400"
let "ml_rx_status = 0x0800"
let "ml_bnx2x_sp = 0x0100000"
let "ml_bnx2x_ptp = 0x1000000"
let "ml_bnx2x_iov = 0x0800000"
#ml=$(( $ml_intr | $ml_rx_status | $ml_tx_done | $ml_probe | $ml_bnx2x_sp ))
#ml=$(( $ml_intr | $ml_rx_status | $ml_tx_done | $ml_probe | $ml_bnx2x_sp | $ml_bnx2x_ptp ))
#ml=$(( $ml_intr | $ml_bnx2x_sp | $ml_rx_status ))
#ml=$(( $ml_intr | $ml_bnx2x_sp ))
#ml=$(( $ml_rx_err ))
#ml=$(( $ml_intr | $ml_rx_status | $ml_tx_done | $ml_tx_msg_queue | $ml_probe | $ml_bnx2x_ptp | $ml_bnx2x_iov ))
ml=$(( $ml_ifdown ))
#ml=0
echo "msglvl: $ml"
ethtool -s $edev msglvl $ml
