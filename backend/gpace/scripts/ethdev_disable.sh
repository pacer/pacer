#!/bin/bash

for ifstr in "eno1" "enp0s0" "eth0"; do
  ifline=`ifconfig | grep $ifstr`
  if [[ "x$ifline" != "x" ]]; then
    iface=`echo $ifline | cut -d' ' -f1`
    break
  fi
done

edev=$iface
echo "disable debug msges for INTR, RX_STATUS, TX_DONE on $edev..."
#ethtool -s $edev msglvl intr off rx_status off tx_done off probe off

ml=0
echo "msglvl: $ml"
ethtool -s $edev msglvl $ml
