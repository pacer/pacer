## Guest VM instructions
`irqsetup.sh`: configures proper IRQ affinity for NIC Tx and Rx interrupts, depending on number of VCPUs configured for the VM. This script must be executed in a guest VM everytime upon first bootup.

`ethdev_enable.sh`: can be used to enable various debug prints from the kerne network stack. Run this script at the start of any experiment, and be sure to run the counterpart `ethdev_disable.sh` after the experiment to ensure that the system is not overwhelmed with too many prints.

## GPace kernel module
* compile gpace/linux-4.9.5` and install from source.
  ```
  cd pacer/backend/gpace/linux-backend-4.9.5
  cp domu.config.vm1.sme .config
  make menuconfig ## Simply exit
  ./build.sh
  ./install.sh
  ```
* configure the `KERNELDIR` path in the Makefile of `gpace/kmod` before compiling.
* run `insmod` to install kernel module and `rmmod` to uninstall it.
* look at `run_host_setup` in the scripts in `eval/video/experiment` directory for exact parameters to the `insmod` command on various nodes.
