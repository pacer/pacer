/*
 * sme_statistics.h
 *
 *  created on: Sep 27, 2017
 *  author: aasthakm
 */

#ifndef __SME_STATISTICS_H__
#define __SME_STATISTICS_H__

#include <linux/types.h>
#include "sme_config.h"

typedef struct sme_stat
{
  char name[64]; //variable name

  long start;
  long end;
  long step;

  long count;
  uint64_t sum;

  long min;
  long max;

  unsigned int* dist; //the distribution

} sme_stat;

int
init_sme_stats (sme_stat* stat, char* name, long start, long end, long step);

int
init_sme_stats_arr(sme_stat *stat, long start, long end, long step,
    char name[], ...);

// take a value in ns
void inline
add_point (sme_stat* stat, long value);

void inline
add_custom_point (sme_stat* stat, int index, long value);

void
print_distribution (sme_stat* stat);

void
serialize_distribution (sme_stat* stat, char* ser, int len);

void
free_sme_stats (sme_stat* stat);

#if SME_CONFIG_STAT == 1

#define SME_DECL_STAT_EXTERN(name)	extern sme_stat* stats_##name
#define SME_DECL_STAT(name)	sme_stat* stats_##name = NULL
#define SME_INIT_STAT(name, sta, end, st, desc) \
	stats_##name = kzalloc(sizeof(sme_stat), GFP_KERNEL); \
	if(!stats_##name) { \
		return -ENOMEM; \
	} \
	if(init_sme_stats(stats_##name, desc, sta, end, st))  \
		return -ENOMEM

#define SME_DEST_STAT(name, toprint) \
  if(stats_##name != NULL) {  \
    if (toprint) {  \
          print_distribution(stats_##name); \
    } \
    free_sme_stats(stats_##name); \
    kfree(stats_##name);  \
  }

#define SME_PRINT_STAT(name)  \
  if(stats_##name != NULL) {  \
    print_distribution(stats_##name); \
  }

#define SME_INIT_TIMER(name)  \
	struct timespec start_##name; \
	struct timespec end_##name

//#define SME_START_TIMER(name) getnstimeofday(&start_##name)
#define SME_START_TIMER(name) getrawmonotonic(&start_##name)

#define SME_START_TIME(name) timespec_to_ns(&start_##name)
#define SME_END_TIME(name) timespec_to_ns(&end_##name)
#define SME_SPENT_TIME(name)  \
	(SME_END_TIME(name) - SME_START_TIME(name))

#define SME_ADD_TIME_POINT(name)  \
	(add_point(stats_##name, SME_SPENT_TIME(name)))

#if 0
#define SME_END_TIMER(name) \
	getnstimeofday(&end_##name);  \
	SME_ADD_TIME_POINT(name)
#endif

#define SME_END_TIMER(name) \
	getrawmonotonic(&end_##name);  \
	SME_ADD_TIME_POINT(name)

#define SME_ADD_COUNT_POINT(name, val)  \
	(add_point(stats_##name, val))

#define SME_ADD_CUSTOM_POINT(name, index, val)  \
  (add_custom_point(stats_##name, index, val))

#define SME_DECL_STAT_ARR_EXTERN(name, N) extern sme_stat *stats_##name[N]
#define SME_DECL_STAT_ARR(name, N)  \
  sme_stat *stats_##name[N] __cacheline_aligned_in_smp
#define SME_INIT_STAT_ARR(name, sta, end, st, N, desc, arg...)  \
  do {  \
    int stati;  \
    for (stati = 0; stati < N; stati++) { \
      stats_##name[stati] = kzalloc(sizeof(sme_stat), GFP_KERNEL);  \
      if (init_sme_stats_arr(stats_##name[stati], sta, end, st, desc, stati)) \
        return ENOMEM;  \
    } \
  } while (0)

#define SME_DEST_STAT_ARR(name, toprint, N) \
  do {  \
    int stati;  \
    for (stati = 0; stati < N; stati++) { \
      if (stats_##name[stati] != NULL) {  \
        if (toprint) {  \
          print_distribution(stats_##name[stati]);  \
        } \
        free_sme_stats(stats_##name[stati]);  \
        kfree(stats_##name[stati]); \
      } \
    } \
  } while (0)

#define SME_ADD_TIME_POINT_ARR(name, stati) \
  (add_point(stats_##name[stati], SME_SPENT_TIME(name)))

#if 0
#define SME_END_TIMER_ARR(name, stati)  \
  getnstimeofday(&end_##name);  \
  SME_ADD_TIME_POINT_ARR(name, stati)
#endif

#define SME_END_TIMER_ARR(name, stati)  \
  getrawmonotonic(&end_##name); \
  SME_ADD_TIME_POINT_ARR(name, stati)

#define SME_ADD_COUNT_POINT_ARR(name, val, stati) \
  (add_point(stats_##name[stati], val))

#else /* SME_CONFIG_STAT == 0 */
#define SME_DECL_STAT_EXTERN(name)
#define SME_DECL_STAT(name)
#define SME_INIT_STAT(name, sta, end, st, desc)
#define SME_DEST_STAT(name, toprint)
#define SME_PRINT_STAT(name)
#define SME_INIT_TIMER(name)
#define SME_START_TIMER(name)
#define SME_START_TIME(name)
#define SME_END_TIME(name)
#define SME_SPENT_TIME(name)
#define SME_ADD_TIME_POINT(name)
#define SME_END_TIMER(name)
#define SME_ADD_COUNT_POINT(name, val)
#define SME_ADD_CUSTOM_POINT(name, index, val)

#define SME_DECL_STAT_ARR_EXTERN(name, N)
#define SME_DECL_STAT_ARR(name, N)
#define SME_INIT_STAT_ARR(name, sta, end, st, N, desc, arg...)
#define SME_DEST_STAT_ARR(name, toprint, N)
#define SME_ADD_TIME_POINT_ARR(name, stati)
#define SME_END_TIMER_ARR(name, stati)
#define SME_ADD_COUNT_POINT_ARR(name, val, stati)
#endif

#endif /* SME_STATISTICS_H_ */
