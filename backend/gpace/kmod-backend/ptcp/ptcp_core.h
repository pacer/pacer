/*
 * ptcp_core.h
 *
 * created on: Jun 22, 2018
 * author: aasthakm
 *
 * implementation of interception hooks in core
 */

#ifndef __PTCP_CORE_H__
#define __PTCP_CORE_H__

#include <linux/skbuff.h>
#include <linux/tcp.h>

void mod_print_sock_skb(struct sock *sk, struct sk_buff *skb, char *dbg_str);
int mod_rx_pred_flags(struct sock *sk, struct sk_buff *skb, struct tcphdr *th);
int mod_rx_handle_tcp_urg(struct sock *sk, struct sk_buff *skb, struct tcphdr *th);
int mod_rx_handle_dummy(struct sock *sk, struct sk_buff *skb);
int mod_rx_disable_coalesce(struct sock *sk, struct sk_buff *to,
    struct sk_buff *from);
int mod_rx_adjust_skb_size(struct sock *sk, struct sk_buff *skb, int req_len,
    int offset, int chunk, int copied, int flags);
int mod_rx_adjust_copied_seq(struct sock *sk, struct sk_buff *skb, int old_skb_len,
    int new_skb_len);

int mod_tx_adjust_seq(struct sock *sk, struct sk_buff *skb, int copy, int copied);
int mod_tx_adjust_urg(struct sock *sk, struct sk_buff *skb);
int mod_tx_incr_tail(struct sock *sk, struct sk_buff *skb);
int mod_tx_incr_profile(struct sock *sk, struct sk_buff *skb);
int mod_is_paced_flow(struct sock *sk);
void mod_timer_rto_check(struct sock *sk);
int mod_get_retransmit_skb(struct sock *sk, struct sk_buff **skb_p);
int mod_build_rexmit_dummy(struct sock *sk, struct sk_buff **skb_p,
    uint32_t seqno, uint32_t seqack, int sacked_flags);

int mod_rxtx_sync_shared_seq(struct sock *sk, struct sk_buff *skb, int write);
void do_read_sync(struct sock *sk, struct sk_buff *skb);

#endif /* __PTCP_CORE_H__ */
