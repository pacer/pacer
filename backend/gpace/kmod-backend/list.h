/*
 * list.h
 *
 * created on: Nov 8, 2017
 * author: aasthakm
 *
 * Linux kernel list struct
 */

#ifndef __LIST_H__
#define __LIST_H__

typedef struct list_head list_t;

#endif /* __LIST_H__ */
