/*
 * sme_time.h
 *
 * created on: Apr 10, 2017
 * author: aasthakm
 *
 * time utilities API
 */

#ifndef __SME_TIME_H__
#define __SME_TIME_H__

#include <linux/timekeeping.h>
#include <linux/ktime.h>
#include <linux/time.h>
#include <linux/hrtimer.h>

enum {
  SCALE_NS = 0,
  SCALE_US,
  SCALE_MS,
  SCALE_RDTSC
};

static inline uint64_t get_current_time(int scale)
{
//#if !CONFIG_XEN_PACER
  struct timespec ts_ns;
//#endif
  struct timeval tv;
  s64 curr_ts = 0;
  if (scale == SCALE_NS) {
    /*
     * getrawmonotonic is always monotonic, whereas
     * getnstimeofday may go backwards if there is a
     * change in timezone, or clock reset.
     *
     * but getrawmonotonic is simply a TSC since
     * last boot time?
     *
     * XXX: userspace timestamps, for e.g., php's
     * microtime() match with values generated using
     * getnstimeofday rather than getrawmonotonic.
     * However, we do not really care about userspace
     * timestamps, so we continue to use
     * getrawmonotonic uniformly.
     */
//#if !CONFIG_XEN_PACER
    getnstimeofday(&ts_ns);
//    getrawmonotonic(&ts_ns);
    curr_ts = timespec_to_ns(&ts_ns);
//#else
//    /*
//     * we need to use the pure cycle counter in order to
//     * match with xen's time measurement capabilities.
//     */
//    curr_ts = rdtsc();
//#endif
  } else if (scale == SCALE_US) {
    do_gettimeofday(&tv);
    curr_ts = tv.tv_sec * 1000000 + tv.tv_usec;
  } else if (scale == SCALE_MS) {
    do_gettimeofday(&tv);
    curr_ts = tv.tv_sec * 1000 + tv.tv_usec / 1000;
  } else if (scale == SCALE_RDTSC) {
    curr_ts = rdtsc_ordered();
  }
  return (uint64_t) curr_ts;
}


#endif /* __SME_TIME_H__ */
