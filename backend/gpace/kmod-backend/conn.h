/*
 * conn.h
 *
 * created on: Aug 25, 2017
 * author: aasthakm
 * 
 * conn pair identifier datastructure
 */

#ifndef __CONN_H__
#define __CONN_H__

#include <linux/types.h>
#include "../../include/conn_common.h"

enum {
  KEY_PRIMARY = 0,
  KEY_SECONDARY
};

enum {
  COMPARE_CLIENT_SCM = 0, /* installed SCM key has only in and out IP */
  COMPARE_CROSSTIER_SCM, /* installed SCM key has in, out and dst IP */
  COMPARE_INC_PKT, /* conn tuple for incoming request has only src, in */
  MAX_COMPARE_TYPES
};

void init_conn(conn_t *conn_id, uint32_t src_ip, uint32_t in_ip,
    uint32_t out_ip, uint32_t dst_ip, uint16_t src_port, uint16_t in_port,
    uint16_t out_port, uint16_t dst_port);
void print_conn(conn_t *c);

int compare_conn_generic(conn_t *c1, conn_t *c2, int compare_type);
int compare_client_scm_conn(conn_t *c1, conn_t *c2);
int compare_ct_scm_conn(conn_t *c1, conn_t *c2);
int compare_inc_pkt(conn_t *c1, conn_t *c2);

int inherit_key_fields(conn_t *conn_id, conn_t *sconn_id);

#endif /* __CONN_H__ */
