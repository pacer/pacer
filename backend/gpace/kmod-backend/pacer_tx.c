/*
 * sme_dma.c
 *
 * created on: Apr 1, 2017
 * atuhor: aasthakm
 *
 * DMA code factored from the bnx2x driver code
 */

#include <linux/types.h>
#include <linux/compiler.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/dma-mapping.h>
#include <linux/dma-direction.h>

#include <uapi/linux/ip.h>
#include <uapi/linux/in.h>
#include <uapi/linux/if_ether.h>
#include <linux/if_vlan.h>
#include <net/ip.h>

#include <xen/page.h>

#include "sme_config.h"
#include "sme_debug.h"
#include "../linux-backend-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"
#include "../linux-backend-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x_cmn.h"

#include "sme_common.h"
#include "sme_core.h"
#include "sme_helper.h"
#include "sme_statistics.h"
#include "sme_time.h"

#ifndef DBG_BUF_SIZE
#define DBG_BUF_SIZE 256
#endif

#if CONFIG_XEN_PACER
#include "sme_xen.h"
#include "sme_nic.h"
extern char **default_payload;
extern dma_addr_t default_payload_dmaddr;
extern profq_hdr_t *profq_hdr[MAX_HP_ARGS];
extern nic_txintq_t *nic_txintq[MAX_HP_ARGS];
extern global_nicq_hdr_t *global_nicq[MAX_HP_ARGS];
extern nic_freelist_t *global_nic_freelist[MAX_HP_ARGS];
#endif

int
prepare_tofree(struct bnx2x *bp, struct bnx2x_fp_txdata *txdata, u16 idx,
    __le32 *tofree_tx_start_bd_addr_lo_p, __le32 *tofree_tx_start_bd_addr_hi_p,
    __le16 *tofree_tx_start_bd_nbytes_p, u16 *split_bd_len_p, int *tofree_nbd_p,
    __le32 **tofree_tx_data_bd_addr_lo_p, __le32 **tofree_tx_data_bd_addr_hi_p,
    __le16 **tofree_tx_data_bd_nbytes_p, struct sk_buff **skb_p)
{
  struct sw_tx_bd *tofree_tx_buf = &txdata->tx_buf_ring[idx];
  struct eth_tx_start_bd *tofree_tx_start_bd;
  struct eth_tx_bd *tx_data_bd;
  u16 tofree_bd_idx = TX_BD(tofree_tx_buf->first_bd);
  u16 new_cons;
  int tofree_nbd;
  u16 split_bd_len = 0;

  tofree_tx_start_bd = &txdata->tx_desc_ring[tofree_bd_idx].start_bd;
  *tofree_tx_start_bd_addr_lo_p = tofree_tx_start_bd->addr_lo;
  *tofree_tx_start_bd_addr_hi_p = tofree_tx_start_bd->addr_hi;
  *tofree_tx_start_bd_nbytes_p = tofree_tx_start_bd->nbytes;

  tofree_nbd = le16_to_cpu(tofree_tx_start_bd->nbd) - 1;
  new_cons = tofree_nbd + tofree_tx_buf->first_bd;

  tofree_bd_idx = TX_BD(NEXT_TX_IDX(tofree_bd_idx));
  --tofree_nbd;
  tofree_bd_idx = TX_BD(NEXT_TX_IDX(tofree_bd_idx));

  if (tofree_tx_buf->flags & BNX2X_HAS_SECOND_PBD) {
    --tofree_nbd;
    tofree_bd_idx = TX_BD(NEXT_TX_IDX(tofree_bd_idx));
  }

  if (tofree_tx_buf->flags & BNX2X_TSO_SPLIT_BD) {
    tx_data_bd = &txdata->tx_desc_ring[tofree_bd_idx].reg_bd;
    split_bd_len = BD_UNMAP_LEN(tx_data_bd);
    --tofree_nbd;
    tofree_bd_idx = TX_BD(NEXT_TX_IDX(tofree_bd_idx));
  }
  *split_bd_len_p = split_bd_len;

  *tofree_nbd_p = tofree_nbd;

  *skb_p = tofree_tx_buf->skb;

  return 0;
}

/* "free" dummy skb in the packet ring at pos idx
 * return idx of last bd freed
 */
u16 sme_bnx2x_free_tx_pkt(struct bnx2x *bp, struct bnx2x_fp_txdata *txdata,
    u16 idx, unsigned int *pkts_compl, unsigned int *bytes_compl,
    __le32 tofree_tx_start_bd_addr_lo, __le32 tofree_tx_start_bd_addr_hi,
    __le16 tofree_tx_start_bd_nbytes, u16 split_bd_len, int tofree_nbd,
    __le32 *tofree_tx_data_bd_addr_lo, __le32 *tofree_tx_data_bd_addr_hi,
    __le16 *tofree_tx_data_bd_nbytes, struct sk_buff *skb)
{
  struct sw_tx_bd *tx_buf = &txdata->tx_buf_ring[idx];
  struct eth_tx_start_bd *tx_start_bd;
  u16 bd_idx = TX_BD(tx_buf->first_bd), new_cons;
  int nbd;

#if 0
  /* prefetch skb end pointer to speedup dev_kfree_skb() */
  prefetch(&skb->end);
#endif

  tx_start_bd = &txdata->tx_desc_ring[bd_idx].start_bd;

  nbd = le16_to_cpu(tx_start_bd->nbd) - 1;
  new_cons = nbd + tx_buf->first_bd;

  iprint(LVL_DBG, "fp[%d]: pkt_idx %d orig bd_cons %d bd %d => %d "
      "bytes compl %d\n",
     txdata->txq_index, idx, txdata->tx_bd_cons, tx_buf->first_bd
     , new_cons, (skb ? skb->len : -1));

	if (!bytes_compl ||
			((unsigned long) bytes_compl & 0xffff87ff00000000) == 0xffff87ff00000000 ||
      (unsigned long) bytes_compl == 0xffff87fffffff098) {
      printk(KERN_ERR "(%d:%.16s,core:%d) %s:%d bc %p pc %p"
      "HW BUG!!! TXQ[%d]: hw_cons %u pkt_cons %u bd_cons %u\n"
      "bd_prod %u pkt_prod %u doorbell %u maddr %llu skb %p txintq %p %llu\n"
      , task_pid_nr(current), current->comm, smp_processor_id()
      , __func__, __LINE__
      , bytes_compl, pkts_compl
      , txdata->txq_index, le16_to_cpu(*txdata->tx_cons_sb)
      , txdata->tx_pkt_cons, txdata->tx_bd_cons
      , txdata->tx_bd_prod, txdata->tx_pkt_prod
      , txdata->tx_db.data.prod
      , nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]->skb_maddr
      , skb, nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]
      , (virt_to_machine(nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)])).maddr
      );

      return new_cons;
  }

	if (!pkts_compl ||
			((unsigned long) pkts_compl & 0xffff87ff00000000) == 0xffff87ff00000000) {
      printk(KERN_ERR "(%d:%.16s,core:%d) %s:%d bc %p pc %p"
      "HW BUG!!! TXQ[%d]: hw_cons %u pkt_cons %u bd_cons %u\n"
      "bd_prod %u pkt_prod %u doorbell %u maddr %llu skb %p txintq %p %llu\n"
      , task_pid_nr(current), current->comm, smp_processor_id()
      , __func__, __LINE__
      , bytes_compl, pkts_compl
      , txdata->txq_index, le16_to_cpu(*txdata->tx_cons_sb)
      , txdata->tx_pkt_cons, txdata->tx_bd_cons
      , txdata->tx_bd_prod, txdata->tx_pkt_prod, txdata->tx_db.data.prod
      , nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]->skb_maddr
      , skb, nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]
      , (virt_to_machine(nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)])).maddr
      );

      return new_cons;
  }

	if (!skb ||
			((unsigned long) skb & 0xffff87ff00000000) == 0xffff87ff00000000) {
      printk(KERN_ERR "(%d:%.16s,core:%d) %s:%d bc %p pc %p"
      "HW BUG!!! TXQ[%d]: hw_cons %u pkt_cons %u bd_cons %u\n"
      "bd_prod %u pkt_prod %u doorbell %u maddr %llu skb %p txintq %p %llu\n"
      , task_pid_nr(current), current->comm, smp_processor_id()
      , __func__, __LINE__
      , bytes_compl, pkts_compl
      , txdata->txq_index, le16_to_cpu(*txdata->tx_cons_sb)
      , txdata->tx_pkt_cons, txdata->tx_bd_cons
      , txdata->tx_bd_prod, txdata->tx_pkt_prod, txdata->tx_db.data.prod
      , nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]->skb_maddr
      , skb, nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]
      , (virt_to_machine(nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)])).maddr
      );

      return new_cons;
  }

  /* release skb */
  WARN_ON(!skb);
  if (likely(skb)) {
    (*pkts_compl)++;
    (*bytes_compl) += skb->len;
//    kfree_skb_partial(skb, true);
  }

#if 0
  tx_buf->first_bd = 0;
  tx_buf->skb = NULL;
#endif

  return new_cons;
}

/* free skb in the packet ring at pos idx
 * return idx of last bd freed
 */
static u16 bnx2x_free_tx_pkt(struct bnx2x *bp, struct bnx2x_fp_txdata *txdata,
           u16 idx, unsigned int *pkts_compl,
           unsigned int *bytes_compl)
{
  struct sw_tx_bd *tx_buf = &txdata->tx_buf_ring[idx];
  struct eth_tx_start_bd *tx_start_bd;
  struct eth_tx_bd *tx_data_bd;
#if CONFIG_XEN_PACER_SLOTSHARE || !CONFIG_XEN_PACER_DB
  struct sk_buff *skb = tx_buf->skb;
  uint64_t skb_maddr = (virt_to_machine(skb)).maddr;
#else
  uint64_t skb_maddr = nic_txintq[txdata->txq_index]->queue_p[idx]->skb_maddr;
  struct sk_buff *skb = __va((machine_to_phys(XMADDR(skb_maddr))).paddr);
#endif
  u16 bd_idx = TX_BD(tx_buf->first_bd), new_cons;
  int nbd;
  u16 split_bd_len = 0;

  /* prefetch skb end pointer to speedup dev_kfree_skb() */
  prefetch(&skb->end);

  tx_start_bd = &txdata->tx_desc_ring[bd_idx].start_bd;

  nbd = le16_to_cpu(tx_start_bd->nbd) - 1;
#ifdef BNX2X_STOP_ON_ERROR
  if ((nbd - 1) > (MAX_SKB_FRAGS + 2)) {
    BNX2X_ERR("BAD nbd!\n");
    bnx2x_panic();
  }
#endif
  new_cons = nbd + tx_buf->first_bd;

#if !(CONFIG_XEN_PACER_SLOTSHARE || !CONFIG_XEN_PACER_DB)
  if (skb_maddr == 0 ||
      ((unsigned long) skb & 0xffff87ff00000000) == 0xffff87ff00000000) {
    printk(KERN_ERR "(%d:%.16s,core:%d) %s:%d"
      "BUG!!! TXQ[%d]: pkt_cons %u (%u) bd_cons %u "
      "bd_prod %u pkt_prod %u skb %llu skb %p\n"
      , task_pid_nr(current), current->comm, smp_processor_id()
      , __func__, __LINE__
      , txdata->txq_index, idx
      , txdata->tx_pkt_cons, txdata->tx_bd_cons
      , txdata->tx_bd_prod, txdata->tx_pkt_prod
      , nic_txintq[txdata->txq_index]->queue_p[idx]->skb_maddr, skb
      );
  }
#endif

  DP(NETIF_MSG_TX_DONE, "fp[%d]: pkt_idx %d orig bd_cons %d bd %d => %d "
      "bytes compl %d\n",
     txdata->txq_index, idx, txdata->tx_bd_cons, tx_buf->first_bd
     , new_cons, (skb ? skb->len : -1));

  /* Get the next bd */
  bd_idx = TX_BD(NEXT_TX_IDX(bd_idx));

  /* Skip a parse bd... */
  --nbd;
  bd_idx = TX_BD(NEXT_TX_IDX(bd_idx));

  if (tx_buf->flags & BNX2X_HAS_SECOND_PBD) {
    /* Skip second parse bd... */
    --nbd;
    bd_idx = TX_BD(NEXT_TX_IDX(bd_idx));
  }

  /* TSO headers+data bds share a common mapping. See bnx2x_tx_split() */
  if (tx_buf->flags & BNX2X_TSO_SPLIT_BD) {
    tx_data_bd = &txdata->tx_desc_ring[bd_idx].reg_bd;
    split_bd_len = BD_UNMAP_LEN(tx_data_bd);
    --nbd;
    bd_idx = TX_BD(NEXT_TX_IDX(bd_idx));
  }

  /* unmap first bd */
  dma_unmap_single(&bp->pdev->dev, BD_UNMAP_ADDR(tx_start_bd),
       BD_UNMAP_LEN(tx_start_bd) + split_bd_len,
       DMA_TO_DEVICE);

  /* now free frags */
  while (nbd > 0) {

    tx_data_bd = &txdata->tx_desc_ring[bd_idx].reg_bd;
    dma_unmap_page(&bp->pdev->dev, BD_UNMAP_ADDR(tx_data_bd),
             BD_UNMAP_LEN(tx_data_bd), DMA_TO_DEVICE);
    if (--nbd)
      bd_idx = TX_BD(NEXT_TX_IDX(bd_idx));
  }

	if (!bytes_compl ||
			((unsigned long) bytes_compl & 0xffff87ff00000000) == 0xffff87ff00000000) {
      printk(KERN_ERR "(%d:%.16s,core:%d) %s:%d bc %p pc %p"
      "HW BUG!!! TXQ[%d]: hw_cons %u pkt_cons %u bd_cons %u\n"
      "bd_prod %u pkt_prod %u doorbell %u maddr %llu skb %p txintq %p %llu\n"
      , task_pid_nr(current), current->comm, smp_processor_id()
      , __func__, __LINE__
      , bytes_compl, pkts_compl
      , txdata->txq_index, le16_to_cpu(*txdata->tx_cons_sb)
      , txdata->tx_pkt_cons, txdata->tx_bd_cons
      , txdata->tx_bd_prod, txdata->tx_pkt_prod
      , txdata->tx_db.data.prod
      , nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]->skb_maddr
      , skb, nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]
      , (virt_to_machine(nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)])).maddr
      );
  }

	if (!pkts_compl ||
			((unsigned long) pkts_compl & 0xffff87ff00000000) == 0xffff87ff00000000) {
      printk(KERN_ERR "(%d:%.16s,core:%d) %s:%d bc %p pc %p"
      "HW BUG!!! TXQ[%d]: hw_cons %u pkt_cons %u bd_cons %u\n"
      "bd_prod %u pkt_prod %u doorbell %u maddr %llu skb %p txintq %p %llu\n"
      , task_pid_nr(current), current->comm, smp_processor_id()
      , __func__, __LINE__
      , bytes_compl, pkts_compl
      , txdata->txq_index, le16_to_cpu(*txdata->tx_cons_sb)
      , txdata->tx_pkt_cons, txdata->tx_bd_cons
      , txdata->tx_bd_prod, txdata->tx_pkt_prod, txdata->tx_db.data.prod
      , nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]->skb_maddr
      , skb, nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)]
      , (virt_to_machine(nic_txintq[txdata->txq_index]->queue_p[TX_BD(txdata->tx_pkt_cons)])).maddr
      );
  }
  /* release skb */
//  WARN_ON(!skb);
  if (likely(skb)) {
    (*pkts_compl)++;
    (*bytes_compl) += skb->len;
    dev_kfree_skb_any(skb);
  }

  tx_buf->first_bd = 0;
  tx_buf->skb = NULL;

  return new_cons;
}

void
sme_bnx2x_free_tx_skbs_queue(struct bnx2x_fastpath *fp)
{
  u8 cos;
  u16 pkt_cons;
  u16 bd_cons;
  struct bnx2x *bp = fp->bp;
  struct bnx2x_fp_txdata *txdata = NULL;
  unsigned pkts_compl = 0, bytes_compl = 0;
  u16 sw_prod, sw_cons;

#if CONFIG_XEN_PACER
  int tofree = 0;
  u16 tofree_idx = 0;
  struct bnx2x_fp_txdata *tofree_txdata = NULL;
  __le32 tofree_tx_start_bd_addr_lo, tofree_tx_start_bd_addr_hi;
  __le16 tofree_tx_start_bd_nbytes;
  u16 split_bd_len;
  int tofree_nbd;
  __le32 *tofree_tx_data_bd_addr_lo = NULL;
  __le32 *tofree_tx_data_bd_addr_hi = NULL;
  __le16 *tofree_tx_data_bd_nbytes = NULL;
  struct sk_buff *tofree_skb = NULL;
  int ret = 0;
  struct sw_tx_bd *tx_buf = NULL;
#endif

  for_each_cos_in_tx_queue(fp, cos) {
    txdata = fp->txdata_ptr[cos];
    sw_prod = txdata->tx_pkt_prod;
    sw_cons = txdata->tx_pkt_cons;

    if (sw_prod < sw_cons)
      break;

    while (sw_cons != sw_prod) {
      pkt_cons = TX_BD(sw_cons);
#if CONFIG_XEN_PACER
      tx_buf = &txdata->tx_buf_ring[pkt_cons];
      if (tx_buf->skb && tx_buf->skb->head == (unsigned char *) default_payload) {
        iprintk(LVL_DBG, "DEFAULT PAYLOAD");
        tofree = 1;
        tofree_txdata = txdata;
        tofree_idx = pkt_cons;
        ret = prepare_tofree(bp, tofree_txdata, tofree_idx,
            &tofree_tx_start_bd_addr_lo, &tofree_tx_start_bd_addr_hi,
            &tofree_tx_start_bd_nbytes, &split_bd_len, &tofree_nbd,
            &tofree_tx_data_bd_addr_lo, &tofree_tx_data_bd_addr_hi,
            &tofree_tx_data_bd_nbytes, &tofree_skb);
        ret = sme_bnx2x_free_tx_pkt(bp, tofree_txdata, tofree_idx,
            &pkts_compl, &bytes_compl,
            tofree_tx_start_bd_addr_lo, tofree_tx_start_bd_addr_hi,
            tofree_tx_start_bd_nbytes, split_bd_len, tofree_nbd,
            tofree_tx_data_bd_addr_lo, tofree_tx_data_bd_addr_hi,
            tofree_tx_data_bd_nbytes, tofree_skb);
      } else {
        bd_cons = bnx2x_free_tx_pkt(bp, txdata, pkt_cons,
                  &pkts_compl, &bytes_compl);
      }
#else
      bd_cons = bnx2x_free_tx_pkt(bp, txdata, pkt_cons,
					    &pkts_compl, &bytes_compl);
#endif
      sw_cons++;
    }

    netdev_tx_reset_queue(netdev_get_tx_queue(bp->dev, txdata->txq_index));
  }
}

int sme_bnx2x_tx_int(struct bnx2x *bp, struct bnx2x_fp_txdata *txdata)
{
	struct netdev_queue *txq;
	u16 hw_cons, sw_cons, bd_cons = txdata->tx_bd_cons;
	unsigned int pkts_compl = 0, bytes_compl = 0;

  u16 orig_bd_cons, orig_pkt_cons;

#ifdef BNX2X_STOP_ON_ERROR
	if (unlikely(bp->panic))
		return -1;
#endif

  orig_bd_cons = txdata->tx_bd_cons;
  orig_pkt_cons = txdata->tx_pkt_cons;

	txq = netdev_get_tx_queue(bp->dev, txdata->txq_index);
	hw_cons = le16_to_cpu(*txdata->tx_cons_sb);
	sw_cons = txdata->tx_pkt_cons;

	while (sw_cons != hw_cons) {
		u16 pkt_cons;

		pkt_cons = TX_BD(sw_cons);

    if (txdata->txq_index >= SME_PACER_CORE
        && txdata->txq_index < (SME_PACER_CORE + N_HYPACE)) {
      iprint(LVL_INFO,
         "TXQ[%d]: hw_cons %u sw_cons %u (%u) pkt_cons %u bd_cons %u\n"
         "bd_prod %u pkt_prod %u doorbell %u"
         , txdata->txq_index, hw_cons, sw_cons, pkt_cons
         , txdata->tx_pkt_cons, txdata->tx_bd_cons
         , txdata->tx_bd_prod, txdata->tx_pkt_prod
         , txdata->tx_db.data.prod);
    }

		bd_cons = bnx2x_free_tx_pkt(bp, txdata, pkt_cons,
					    &pkts_compl, &bytes_compl);

		sw_cons++;
	}

//#if !CONFIG_SELECT_TXQ
#if 0
	netdev_tx_completed_queue(txq, pkts_compl, bytes_compl);
#endif
//#endif

	txdata->tx_pkt_cons = sw_cons;
	txdata->tx_bd_cons = bd_cons;

	/* Need to make the tx_bd_cons update visible to start_xmit()
	 * before checking for netif_tx_queue_stopped().  Without the
	 * memory barrier, there is a small possibility that
	 * start_xmit() will miss it and cause the queue to be stopped
	 * forever.
	 * On the other hand we need an rmb() here to ensure the proper
	 * ordering of bit testing in the following
	 * netif_tx_queue_stopped(txq) call.
	 */
	smp_mb();

//#if !CONFIG_SELECT_TXQ
	if (unlikely(netif_tx_queue_stopped(txq))) {
    iprint(LVL_DBG, "TXQ[%d] doorbell %u hw cons %u TS %llu\n"
        "bd prod %u (%u) pkt prod %u (%u) "
        "bd cons %u (%u) => %u pkt cons %u (%u) => %u"
        , txdata->txq_index, txdata->tx_db.data.prod, hw_cons
        , get_current_time(SCALE_RDTSC)
        , txdata->tx_bd_prod, (unsigned int) TX_BD(txdata->tx_bd_prod)
        , txdata->tx_pkt_prod, (unsigned int) TX_BD(txdata->tx_pkt_prod)
        , orig_bd_cons, (unsigned int) TX_BD(orig_bd_cons), txdata->tx_bd_cons
        , orig_pkt_cons, (unsigned int) TX_BD(orig_pkt_cons), txdata->tx_pkt_cons
        );

		/* Taking tx_lock() is needed to prevent re-enabling the queue
		 * while it's empty. This could have happen if rx_action() gets
		 * suspended in bnx2x_tx_int() after the condition before
		 * netif_tx_wake_queue(), while tx_action (bnx2x_start_xmit()):
		 *
		 * stops the queue->sees fresh tx_bd_cons->releases the queue->
		 * sends some packets consuming the whole queue again->
		 * stops the queue
		 */

		__netif_tx_lock(txq, smp_processor_id());

		if ((netif_tx_queue_stopped(txq)) &&
		    (bp->state == BNX2X_STATE_OPEN) &&
		    (bnx2x_tx_avail(bp, txdata) >= MAX_DESC_PER_TX_PKT))
			netif_tx_wake_queue(txq);

		__netif_tx_unlock(txq);
	}
//#endif

	return 0;
}

#if 0
/* we split the first BD into headers and data BDs
 * to ease the pain of our fellow microcode engineers
 * we use one mapping for both BDs
 */
static u16 bnx2x_tx_split(struct bnx2x *bp,
        struct bnx2x_fp_txdata *txdata,
        struct sw_tx_bd *tx_buf,
        struct eth_tx_start_bd **tx_bd, u16 hlen,
        u16 bd_prod)
{
  struct eth_tx_start_bd *h_tx_bd = *tx_bd;
  struct eth_tx_bd *d_tx_bd;
  dma_addr_t mapping;
  int old_len = le16_to_cpu(h_tx_bd->nbytes);

  /* first fix first BD */
  h_tx_bd->nbytes = cpu_to_le16(hlen);

  DP(NETIF_MSG_TX_QUEUED, "TSO split header size is %d (%x:%x)\n",
     h_tx_bd->nbytes, h_tx_bd->addr_hi, h_tx_bd->addr_lo);

  /* now get a new data BD
   * (after the pbd) and fill it */
  bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
  d_tx_bd = &txdata->tx_desc_ring[bd_prod].reg_bd;

  mapping = HILO_U64(le32_to_cpu(h_tx_bd->addr_hi),
         le32_to_cpu(h_tx_bd->addr_lo)) + hlen;

  d_tx_bd->addr_hi = cpu_to_le32(U64_HI(mapping));
  d_tx_bd->addr_lo = cpu_to_le32(U64_LO(mapping));
  d_tx_bd->nbytes = cpu_to_le16(old_len - hlen);

  /* this marks the BD as one that has no individual mapping */
  tx_buf->flags |= BNX2X_TSO_SPLIT_BD;

  DP(NETIF_MSG_TX_QUEUED,
     "TSO split data size is %d (%x:%x)\n",
     d_tx_bd->nbytes, d_tx_bd->addr_hi, d_tx_bd->addr_lo);

  /* update tx_bd */
  *tx_bd = (struct eth_tx_start_bd *)d_tx_bd;

  return bd_prod;
}
#endif

#define bswab32(b32) ((__force __le32) swab32((__force __u32) (b32)))
#define bswab16(b16) ((__force __le16) swab16((__force __u16) (b16)))
#if 0
static __le16 bnx2x_csum_fix(unsigned char *t_header, u16 csum, s8 fix)
{
  __sum16 tsum = (__force __sum16) csum;

  if (fix > 0)
    tsum = ~csum_fold(csum_sub((__force __wsum) csum,
          csum_partial(t_header - fix, fix, 0)));

  else if (fix < 0)
    tsum = ~csum_fold(csum_add((__force __wsum) csum,
          csum_partial(t_header, -fix, 0)));

  return bswab16(tsum);
}
#endif

/* VXLAN: 4 = 1 (for linear data BD) + 3 (2 for PBD and last BD) */
#define BNX2X_NUM_VXLAN_TSO_WIN_SUB_BDS         4

/* Regular: 3 = 1 (for linear data BD) + 2 (for PBD and last BD) */
#define BNX2X_NUM_TSO_WIN_SUB_BDS               3

static int bnx2x_pkt_req_lin(struct bnx2x *bp, struct sk_buff *skb,
           u32 xmit_type)
{
  int first_bd_sz = 0, num_tso_win_sub = BNX2X_NUM_TSO_WIN_SUB_BDS;
  int to_copy = 0, hlen = 0;

  if (xmit_type & XMIT_GSO_ENC)
    num_tso_win_sub = BNX2X_NUM_VXLAN_TSO_WIN_SUB_BDS;

  if (skb_shinfo(skb)->nr_frags >= (MAX_FETCH_BD - num_tso_win_sub)) {
    if (xmit_type & XMIT_GSO) {
      unsigned short lso_mss = skb_shinfo(skb)->gso_size;
      int wnd_size = MAX_FETCH_BD - num_tso_win_sub;
      /* Number of windows to check */
      int num_wnds = skb_shinfo(skb)->nr_frags - wnd_size;
      int wnd_idx = 0;
      int frag_idx = 0;
      u32 wnd_sum = 0;

      /* Headers length */
      if (xmit_type & XMIT_GSO_ENC)
        hlen = (int)(skb_inner_transport_header(skb) -
               skb->data) +
               inner_tcp_hdrlen(skb);
      else
        hlen = (int)(skb_transport_header(skb) -
               skb->data) + tcp_hdrlen(skb);

      /* Amount of data (w/o headers) on linear part of SKB*/
      first_bd_sz = skb_headlen(skb) - hlen;

      wnd_sum  = first_bd_sz;

      /* Calculate the first sum - it's special */
      for (frag_idx = 0; frag_idx < wnd_size - 1; frag_idx++)
        wnd_sum +=
          skb_frag_size(&skb_shinfo(skb)->frags[frag_idx]);

      /* If there was data on linear skb data - check it */
      if (first_bd_sz > 0) {
        if (unlikely(wnd_sum < lso_mss)) {
          to_copy = 1;
          goto exit_lbl;
        }

        wnd_sum -= first_bd_sz;
      }

      /* Others are easier: run through the frag list and
         check all windows */
      for (wnd_idx = 0; wnd_idx <= num_wnds; wnd_idx++) {
        wnd_sum +=
        skb_frag_size(&skb_shinfo(skb)->frags[wnd_idx + wnd_size - 1]);

        if (unlikely(wnd_sum < lso_mss)) {
          to_copy = 1;
          break;
        }
        wnd_sum -=
          skb_frag_size(&skb_shinfo(skb)->frags[wnd_idx]);
      }
    } else {
      /* in non-LSO too fragmented packet should always
         be linearized */
      to_copy = 1;
    }
  }

exit_lbl:
  if (unlikely(to_copy))
    iprint(LVL_EXP,
       "Linearization IS REQUIRED for %s packet. num_frags %d  hlen %d  first_bd_sz %d\n",
       (xmit_type & XMIT_GSO) ? "LSO" : "non-LSO",
       skb_shinfo(skb)->nr_frags, hlen, first_bd_sz);

  return to_copy;
}

#if 0
/**
 * bnx2x_set_pbd_gso - update PBD in GSO case.
 *
 * @skb:  packet skb
 * @pbd:  parse BD
 * @xmit_type:  xmit flags
 */
static void bnx2x_set_pbd_gso(struct sk_buff *skb,
            struct eth_tx_parse_bd_e1x *pbd,
            u32 xmit_type)
{
  pbd->lso_mss = cpu_to_le16(skb_shinfo(skb)->gso_size);
  pbd->tcp_send_seq = bswab32(tcp_hdr(skb)->seq);
  pbd->tcp_flags = pbd_tcp_flags(tcp_hdr(skb));

  if (xmit_type & XMIT_GSO_V4) {
    pbd->ip_id = bswab16(ip_hdr(skb)->id);
    pbd->tcp_pseudo_csum =
      bswab16(~csum_tcpudp_magic(ip_hdr(skb)->saddr,
               ip_hdr(skb)->daddr,
               0, IPPROTO_TCP, 0));
  } else {
    pbd->tcp_pseudo_csum =
      bswab16(~csum_ipv6_magic(&ipv6_hdr(skb)->saddr,
             &ipv6_hdr(skb)->daddr,
             0, IPPROTO_TCP, 0));
  }

  pbd->global_data |=
    cpu_to_le16(ETH_TX_PARSE_BD_E1X_PSEUDO_CS_WITHOUT_LEN);
}

/**
 * bnx2x_set_pbd_csum_enc - update PBD with checksum and return header length
 *
 * @bp:     driver handle
 * @skb:    packet skb
 * @parsing_data: data to be updated
 * @xmit_type:    xmit flags
 *
 * 57712/578xx related, when skb has encapsulation
 */
static u8 bnx2x_set_pbd_csum_enc(struct bnx2x *bp, struct sk_buff *skb,
         u32 *parsing_data, u32 xmit_type)
{
  *parsing_data |=
    ((((u8 *)skb_inner_transport_header(skb) - skb->data) >> 1) <<
    ETH_TX_PARSE_BD_E2_L4_HDR_START_OFFSET_W_SHIFT) &
    ETH_TX_PARSE_BD_E2_L4_HDR_START_OFFSET_W;

  if (xmit_type & XMIT_CSUM_TCP) {
    *parsing_data |= ((inner_tcp_hdrlen(skb) / 4) <<
      ETH_TX_PARSE_BD_E2_TCP_HDR_LENGTH_DW_SHIFT) &
      ETH_TX_PARSE_BD_E2_TCP_HDR_LENGTH_DW;

    return skb_inner_transport_header(skb) +
      inner_tcp_hdrlen(skb) - skb->data;
  }

  /* We support checksum offload for TCP and UDP only.
   * No need to pass the UDP header length - it's a constant.
   */
  return skb_inner_transport_header(skb) +
    sizeof(struct udphdr) - skb->data;
}
#endif

/**
 * bnx2x_set_pbd_csum_e2 - update PBD with checksum and return header length
 *
 * @bp:     driver handle
 * @skb:    packet skb
 * @parsing_data: data to be updated
 * @xmit_type:    xmit flags
 *
 * 57712/578xx related
 */
static u8 bnx2x_set_pbd_csum_e2(struct bnx2x *bp, struct sk_buff *skb,
        u32 *parsing_data, u32 xmit_type)
{
  *parsing_data |=
    ((((u8 *)skb_transport_header(skb) - skb->data) >> 1) <<
    ETH_TX_PARSE_BD_E2_L4_HDR_START_OFFSET_W_SHIFT) &
    ETH_TX_PARSE_BD_E2_L4_HDR_START_OFFSET_W;

  if (xmit_type & XMIT_CSUM_TCP) {
    *parsing_data |= ((tcp_hdrlen(skb) / 4) <<
      ETH_TX_PARSE_BD_E2_TCP_HDR_LENGTH_DW_SHIFT) &
      ETH_TX_PARSE_BD_E2_TCP_HDR_LENGTH_DW;

    return skb_transport_header(skb) + tcp_hdrlen(skb) - skb->data;
  }
  /* We support checksum offload for TCP and UDP only.
   * No need to pass the UDP header length - it's a constant.
   */
  return skb_transport_header(skb) + sizeof(struct udphdr) - skb->data;
}

/* set FW indication according to inner or outer protocols if tunneled */
static void bnx2x_set_sbd_csum(struct bnx2x *bp, struct sk_buff *skb,
             struct eth_tx_start_bd *tx_start_bd,
             u32 xmit_type)
{
  if (!CONFIG_SW_CSUM)
    tx_start_bd->bd_flags.as_bitfield |= ETH_TX_BD_FLAGS_L4_CSUM;

  if (xmit_type & (XMIT_CSUM_ENC_V6 | XMIT_CSUM_V6))
    tx_start_bd->bd_flags.as_bitfield |= ETH_TX_BD_FLAGS_IPV6;

  if (!(xmit_type & XMIT_CSUM_TCP))
    tx_start_bd->bd_flags.as_bitfield |= ETH_TX_BD_FLAGS_IS_UDP;
}

#if 0
/**
 * bnx2x_set_pbd_csum - update PBD with checksum and return header length
 *
 * @bp:   driver handle
 * @skb:  packet skb
 * @pbd:  parse BD to be updated
 * @xmit_type:  xmit flags
 */
static u8 bnx2x_set_pbd_csum(struct bnx2x *bp, struct sk_buff *skb,
           struct eth_tx_parse_bd_e1x *pbd,
           u32 xmit_type)
{
  u8 hlen = (skb_network_header(skb) - skb->data) >> 1;

  /* for now NS flag is not used in Linux */
  pbd->global_data =
    cpu_to_le16(hlen |
          ((skb->protocol == cpu_to_be16(ETH_P_8021Q)) <<
           ETH_TX_PARSE_BD_E1X_LLC_SNAP_EN_SHIFT));

  pbd->ip_hlen_w = (skb_transport_header(skb) -
      skb_network_header(skb)) >> 1;

  hlen += pbd->ip_hlen_w;

  /* We support checksum offload for TCP and UDP only */
  if (xmit_type & XMIT_CSUM_TCP)
    hlen += tcp_hdrlen(skb) / 2;
  else
    hlen += sizeof(struct udphdr) / 2;

  pbd->total_hlen_w = cpu_to_le16(hlen);
  hlen = hlen*2;

  if (xmit_type & XMIT_CSUM_TCP) {
    pbd->tcp_pseudo_csum = bswab16(tcp_hdr(skb)->check);

  } else {
    s8 fix = SKB_CS_OFF(skb); /* signed! */

    DP(NETIF_MSG_TX_QUEUED,
       "hlen %d  fix %d  csum before fix %x\n",
       le16_to_cpu(pbd->total_hlen_w), fix, SKB_CS(skb));

    /* HW bug: fixup the CSUM */
    pbd->tcp_pseudo_csum =
      bnx2x_csum_fix(skb_transport_header(skb),
               SKB_CS(skb), fix);

    DP(NETIF_MSG_TX_QUEUED, "csum after fix %x\n",
       pbd->tcp_pseudo_csum);
  }

  return hlen;
}

static void bnx2x_update_pbds_gso_enc(struct sk_buff *skb,
              struct eth_tx_parse_bd_e2 *pbd_e2,
              struct eth_tx_parse_2nd_bd *pbd2,
              u16 *global_data,
              u32 xmit_type)
{
  u16 hlen_w = 0;
  u8 outerip_off, outerip_len = 0;

  /* from outer IP to transport */
  hlen_w = (skb_inner_transport_header(skb) -
      skb_network_header(skb)) >> 1;

  /* transport len */
  hlen_w += inner_tcp_hdrlen(skb) >> 1;

  pbd2->fw_ip_hdr_to_payload_w = hlen_w;

  /* outer IP header info */
  if (xmit_type & XMIT_CSUM_V4) {
    struct iphdr *iph = ip_hdr(skb);
    u32 csum = (__force u32)(~iph->check) -
         (__force u32)iph->tot_len -
         (__force u32)iph->frag_off;

    outerip_len = iph->ihl << 1;

    pbd2->fw_ip_csum_wo_len_flags_frag =
      bswab16(csum_fold((__force __wsum)csum));
  } else {
    pbd2->fw_ip_hdr_to_payload_w =
      hlen_w - ((sizeof(struct ipv6hdr)) >> 1);
    pbd_e2->data.tunnel_data.flags |=
      ETH_TUNNEL_DATA_IPV6_OUTER;
  }

  pbd2->tcp_send_seq = bswab32(inner_tcp_hdr(skb)->seq);

  pbd2->tcp_flags = pbd_tcp_flags(inner_tcp_hdr(skb));

  /* inner IP header info */
  if (xmit_type & XMIT_CSUM_ENC_V4) {
    pbd2->hw_ip_id = bswab16(inner_ip_hdr(skb)->id);

    pbd_e2->data.tunnel_data.pseudo_csum =
      bswab16(~csum_tcpudp_magic(
          inner_ip_hdr(skb)->saddr,
          inner_ip_hdr(skb)->daddr,
          0, IPPROTO_TCP, 0));
  } else {
    pbd_e2->data.tunnel_data.pseudo_csum =
      bswab16(~csum_ipv6_magic(
          &inner_ipv6_hdr(skb)->saddr,
          &inner_ipv6_hdr(skb)->daddr,
          0, IPPROTO_TCP, 0));
  }

  outerip_off = (skb_network_header(skb) - skb->data) >> 1;

  *global_data |=
    outerip_off |
    (outerip_len <<
      ETH_TX_PARSE_2ND_BD_IP_HDR_LEN_OUTER_W_SHIFT) |
    ((skb->protocol == cpu_to_be16(ETH_P_8021Q)) <<
      ETH_TX_PARSE_2ND_BD_LLC_SNAP_EN_SHIFT);

  if (ip_hdr(skb)->protocol == IPPROTO_UDP) {
    SET_FLAG(*global_data, ETH_TX_PARSE_2ND_BD_TUNNEL_UDP_EXIST, 1);
    pbd2->tunnel_udp_hdr_start_w = skb_transport_offset(skb) >> 1;
  }
}
#endif

static inline void bnx2x_set_ipv6_ext_e2(struct sk_buff *skb, u32 *parsing_data,
           u32 xmit_type)
{
  struct ipv6hdr *ipv6;

  if (!(xmit_type & (XMIT_GSO_ENC_V6 | XMIT_GSO_V6)))
    return;

  if (xmit_type & XMIT_GSO_ENC_V6)
    ipv6 = inner_ipv6_hdr(skb);
  else /* XMIT_GSO_V6 */
    ipv6 = ipv6_hdr(skb);

  if (ipv6->nexthdr == NEXTHDR_IPV6)
    *parsing_data |= ETH_TX_PARSE_BD_E2_IPV6_WITH_EXT_HDR;
}

static u32 bnx2x_xmit_type(struct bnx2x *bp, struct sk_buff *skb)
{
  u32 rc;
  __u8 prot = 0;
  __be16 protocol;

  if (skb->ip_summed != CHECKSUM_PARTIAL)
    return XMIT_PLAIN;

  protocol = vlan_get_protocol(skb);
  if (protocol == htons(ETH_P_IPV6)) {
    rc = XMIT_CSUM_V6;
    prot = ipv6_hdr(skb)->nexthdr;
  } else {
    rc = XMIT_CSUM_V4;
    prot = ip_hdr(skb)->protocol;
  }

  if (!CHIP_IS_E1x(bp) && skb->encapsulation) {
    if (inner_ip_hdr(skb)->version == 6) {
      rc |= XMIT_CSUM_ENC_V6;
      if (inner_ipv6_hdr(skb)->nexthdr == IPPROTO_TCP)
        rc |= XMIT_CSUM_TCP;
    } else {
      rc |= XMIT_CSUM_ENC_V4;
      if (inner_ip_hdr(skb)->protocol == IPPROTO_TCP)
        rc |= XMIT_CSUM_TCP;
    }
  }
  if (prot == IPPROTO_TCP)
    rc |= XMIT_CSUM_TCP;

  if (skb_is_gso(skb)) {
    if (skb_is_gso_v6(skb)) {
      rc |= (XMIT_GSO_V6 | XMIT_CSUM_TCP);
      if (rc & XMIT_CSUM_ENC)
        rc |= XMIT_GSO_ENC_V6;
    } else {
      rc |= (XMIT_GSO_V4 | XMIT_CSUM_TCP);
      if (rc & XMIT_CSUM_ENC)
        rc |= XMIT_GSO_ENC_V4;
    }
  }

  return rc;
}

int
sme_bnx2x_send_dma(struct sk_buff *skb, struct net_device *dev,
    profile_conn_map_t *pcm)
{
  struct bnx2x *bp = netdev_priv(dev);

  struct netdev_queue *txq;
  struct bnx2x_fp_txdata *txdata;
#if SME_DEBUG_LVL <= LVL_INFO
  struct tcphdr *tcph = NULL;
  int ret = 0;
#endif

  struct sw_tx_bd *tx_buf;
  struct eth_tx_start_bd *tx_start_bd, *first_bd;
  struct eth_tx_bd *tx_data_bd;
//  struct eth_tx_bd *total_pkt_bd = NULL;
//  struct eth_tx_parse_bd_e1x *pbd_e1x = NULL;
  struct eth_tx_parse_bd_e2 *pbd_e2 = NULL;
//  struct eth_tx_parse_2nd_bd *pbd2 = NULL;
  u32 pbd_e2_parsing_data = 0;
  u16 pkt_prod, bd_prod;
  int nbd, txq_index;
  dma_addr_t mapping;
  u32 xmit_type = bnx2x_xmit_type(bp, skb);
//  int i;
  u8 hlen = 0;
  __le16 pkt_size = 0;
  struct ethhdr *eth;
  u8 mac_type = UNICAST_ADDRESS;

  SME_INIT_TIMER(dma_lat);

#ifdef BNX2X_STOP_ON_ERROR
  if (unlikely(bp->panic))
    return NETDEV_TX_BUSY;
#endif

  SME_START_TIMER(dma_lat);

  txq_index = skb_get_queue_mapping(skb);
  txq = netdev_get_tx_queue(dev, txq_index);

//  BUG_ON(txq_index >= MAX_ETH_TXQ_IDX(bp) + (CNIC_LOADED(bp) ? 1 : 0));

  txdata = &bp->bnx2x_txq[txq_index];

  /* enable this debug print to view the transmission queue being used
  DP(NETIF_MSG_TX_QUEUED, "indices: txq %d, fp %d, txdata %d\n",
     txq_index, fp_index, txdata_index); */

  /* enable this debug print to view the transmission details
  DP(NETIF_MSG_TX_QUEUED,
     "transmitting packet cid %d fp index %d txdata_index %d tx_data ptr %p fp pointer %p\n",
     txdata->cid, fp_index, txdata_index, txdata, fp); */

#if !CONFIG_SELECT_TXQ
  if (unlikely(bnx2x_tx_avail(bp, txdata) <
      skb_shinfo(skb)->nr_frags +
      BDS_PER_TX_PKT +
      NEXT_CNT_PER_TX_PKT(MAX_BDS_PER_TX_PKT))) {
    /* Handle special storage cases separately */
    if (txdata->tx_ring_size == 0) {
      struct bnx2x_eth_q_stats *q_stats =
        bnx2x_fp_qstats(bp, txdata->parent_fp);
      q_stats->driver_filtered_tx_pkt++;
      dev_kfree_skb(skb);

      SME_END_TIMER_ARR(dma_lat, smp_processor_id());

      return NETDEV_TX_OK;
    }
    bnx2x_fp_qstats(bp, txdata->parent_fp)->driver_xoff++;
    netif_tx_stop_queue(txq);
    iprint(LVL_DBG, "TXQ[%d] ring size %d avail %d nr_frags %d\n"
        "bd_prod %d bd_cons %d pkt_prod %d pkt_cons %d hw_cons %d doorbell %d"
        , txq->txq_index, txq->tx_ring_size
        , bnx2x_tx_avail(bp, txdata), skb_shinfo(skb)->nr_frags
        , txdata->tx_bd_prod, txdata->tx_bd_cons
        , txdata->tx_pkt_prod, txdata->tx_pkt_cons
        , le16_to_cpu(*txdata->tx_cons_sb)
        , txdata->tx_db.data.prod);
    BNX2X_ERR("BUG! Tx ring full when queue awake!\n");


    SME_END_TIMER_ARR(dma_lat, smp_processor_id());

    return NETDEV_TX_BUSY;
  }
#endif

  iprint(LVL_DBG,
     "queue[%d]: SKB: %p summed %x protocol %x protocol(%x,%x) gso type %x xmit_type %x len %d",
     txq_index, skb, skb->ip_summed, skb->protocol, ipv6_hdr(skb)->nexthdr,
     ip_hdr(skb)->protocol, skb_shinfo(skb)->gso_type, xmit_type,
     skb->len);

  eth = (struct ethhdr *)skb->data;

#if 0
  /* set flag according to packet type (UNICAST_ADDRESS is default)*/
  if (unlikely(is_multicast_ether_addr(eth->h_dest))) {
    if (is_broadcast_ether_addr(eth->h_dest))
      mac_type = BROADCAST_ADDRESS;
    else
      mac_type = MULTICAST_ADDRESS;
  }
#endif

#if (MAX_SKB_FRAGS >= MAX_FETCH_BD - BDS_PER_TX_PKT)
  /* First, check if we need to linearize the skb (due to FW
     restrictions). No need to check fragmentation if page size > 8K
     (there will be no violation to FW restrictions) */
  if (bnx2x_pkt_req_lin(bp, skb, xmit_type)) {
    /* Statistics of linearization */
    bp->lin_cnt++;
    if (skb_linearize(skb) != 0) {
      iprintk(LVL_ERR, "SKB linearization failed - silently dropping this SKB\n");
      dev_kfree_skb_any(skb);
      return NETDEV_TX_OK;
    }
  }
#endif

  /* Map skb linear data for DMA */
  mapping = dma_map_single(&bp->pdev->dev, skb->data,
         skb_headlen(skb), DMA_TO_DEVICE);
  if (unlikely(dma_mapping_error(&bp->pdev->dev, mapping))) {
    iprintk(LVL_ERR, "SKB mapping failed - silently dropping this SKB\n");
    dev_kfree_skb_any(skb);

    SME_END_TIMER_ARR(dma_lat, smp_processor_id());

    return NETDEV_TX_OK;
  }

#if SME_DEBUG_LVL <= LVL_INFO
  ret = parse_skb_headers(skb, NULL, NULL, NULL, NULL, NULL, NULL,
      NULL, NULL, NULL, NULL, NULL, NULL, NULL, (void **) &tcph, NULL);
#endif
  /*
  Please read carefully. First we use one BD which we mark as start,
  then we have a parsing info BD (used for TSO or xsum),
  and only then we have the rest of the TSO BDs.
  (don't forget to mark the last one as last,
  and to unmap only AFTER you write to the BD ...)
  And above all, all pdb sizes are in words - NOT DWORDS!
  */

  /* get current pkt produced now - advance it just before sending packet
   * since mapping of pages may fail and cause packet to be dropped
   */
  pkt_prod = txdata->tx_pkt_prod;
  bd_prod = TX_BD(txdata->tx_bd_prod);

  /* get a tx_buf and first BD
   * tx_start_bd may be changed during SPLIT,
   * but first_bd will always stay first
   */
  tx_buf = &txdata->tx_buf_ring[TX_BD(pkt_prod)];
  tx_start_bd = &txdata->tx_desc_ring[bd_prod].start_bd;
  first_bd = tx_start_bd;

  tx_start_bd->bd_flags.as_bitfield = ETH_TX_BD_FLAGS_START_BD;

#if 0
  if (unlikely(skb_shinfo(skb)->tx_flags & SKBTX_HW_TSTAMP)) {
    if (!(bp->flags & TX_TIMESTAMPING_EN)) {
      BNX2X_ERR("Tx timestamping was not enabled, this packet will not be timestamped\n");
    } else if (bp->ptp_tx_skb) {
      BNX2X_ERR("The device supports only a single outstanding packet to timestamp, this packet will not be timestamped\n");
    } else {
      skb_shinfo(skb)->tx_flags |= SKBTX_IN_PROGRESS;
      /* schedule check for Tx timestamp */
      bp->ptp_tx_skb = skb_get(skb);
      bp->ptp_tx_start = jiffies;
      schedule_work(&bp->ptp_task);
    }
  }
#endif

  /* header nbd: indirectly zero other flags! */
  tx_start_bd->general_data = 1 << ETH_TX_START_BD_HDR_NBDS_SHIFT;

  /* remember the first BD of the packet */
  tx_buf->first_bd = txdata->tx_bd_prod;
  tx_buf->skb = skb;
  tx_buf->flags = 0;

  iprint(LVL_DBG,
     "sending pkt %u @%p next_idx %u bd %u @%p start nbd %u skb %p",
     pkt_prod, tx_buf, txdata->tx_pkt_prod, bd_prod, tx_start_bd,
     le16_to_cpu(tx_start_bd->nbd) - 1, tx_buf->skb);

//  if (skb_vlan_tag_present(skb)) {
//    tx_start_bd->vlan_or_ethertype =
//        cpu_to_le16(skb_vlan_tag_get(skb));
//    tx_start_bd->bd_flags.as_bitfield |=
//        (X_ETH_OUTBAND_VLAN << ETH_TX_BD_FLAGS_VLAN_MODE_SHIFT);
//  } else {
    /* when transmitting in a vf, start bd must hold the ethertype
     * for fw to enforce it
     */
//#ifndef BNX2X_STOP_ON_ERROR
//    if (IS_VF(bp))
//#endif
      tx_start_bd->vlan_or_ethertype =
        cpu_to_le16(ntohs(eth->h_proto));
//#ifndef BNX2X_STOP_ON_ERROR
//    else
//      /* used by FW for packet accounting */
//      tx_start_bd->vlan_or_ethertype = cpu_to_le16(pkt_prod);
//#endif
//  }

  nbd = 2; /* start_bd + pbd + frags (updated when pages are mapped) */

  /* turn on parsing and get a BD */
  bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));

//  if (xmit_type & XMIT_CSUM)
    bnx2x_set_sbd_csum(bp, skb, tx_start_bd, xmit_type);

//  if (!CHIP_IS_E1x(bp)) {
    pbd_e2 = &txdata->tx_desc_ring[bd_prod].parse_bd_e2;
    memset(pbd_e2, 0, sizeof(struct eth_tx_parse_bd_e2));

#if 0
    if (xmit_type & XMIT_CSUM_ENC) {
      u16 global_data = 0;

      /* Set PBD in enc checksum offload case */
      hlen = bnx2x_set_pbd_csum_enc(bp, skb,
                  &pbd_e2_parsing_data,
                  xmit_type);

      /* turn on 2nd parsing and get a BD */
      bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));

      pbd2 = &txdata->tx_desc_ring[bd_prod].parse_2nd_bd;

      memset(pbd2, 0, sizeof(*pbd2));

      pbd_e2->data.tunnel_data.ip_hdr_start_inner_w =
        (skb_inner_network_header(skb) -
         skb->data) >> 1;

      if (xmit_type & XMIT_GSO_ENC)
        bnx2x_update_pbds_gso_enc(skb, pbd_e2, pbd2,
                &global_data,
                xmit_type);

      pbd2->global_data = cpu_to_le16(global_data);

      /* add addition parse BD indication to start BD */
      SET_FLAG(tx_start_bd->general_data,
         ETH_TX_START_BD_PARSE_NBDS, 1);
      /* set encapsulation flag in start BD */
      SET_FLAG(tx_start_bd->general_data,
         ETH_TX_START_BD_TUNNEL_EXIST, 1);

      tx_buf->flags |= BNX2X_HAS_SECOND_PBD;

      nbd++;
    } else if (xmit_type & XMIT_CSUM) {
#endif
      /* Set PBD in checksum offload case w/o encapsulation */
      hlen = bnx2x_set_pbd_csum_e2(bp, skb,
                 &pbd_e2_parsing_data,
                 xmit_type);
//    }

//    bnx2x_set_ipv6_ext_e2(skb, &pbd_e2_parsing_data, xmit_type);
    /* Add the macs to the parsing BD if this is a vf or if
     * Tx Switching is enabled.
     */
//    if (IS_VF(bp)) {
      /* override GRE parameters in BD */
      bnx2x_set_fw_mac_addr(&pbd_e2->data.mac_addr.src_hi,
                &pbd_e2->data.mac_addr.src_mid,
                &pbd_e2->data.mac_addr.src_lo,
                eth->h_source);

      bnx2x_set_fw_mac_addr(&pbd_e2->data.mac_addr.dst_hi,
                &pbd_e2->data.mac_addr.dst_mid,
                &pbd_e2->data.mac_addr.dst_lo,
                eth->h_dest);
#if 0
    } else {
      if (bp->flags & TX_SWITCHING)
        bnx2x_set_fw_mac_addr(
            &pbd_e2->data.mac_addr.dst_hi,
            &pbd_e2->data.mac_addr.dst_mid,
            &pbd_e2->data.mac_addr.dst_lo,
            eth->h_dest);
#ifdef BNX2X_STOP_ON_ERROR
      /* Enforce security is always set in Stop on Error -
       * source mac should be present in the parsing BD
       */
      bnx2x_set_fw_mac_addr(&pbd_e2->data.mac_addr.src_hi,
                &pbd_e2->data.mac_addr.src_mid,
                &pbd_e2->data.mac_addr.src_lo,
                eth->h_source);
#endif
    }
#endif
    SET_FLAG(pbd_e2_parsing_data,
       ETH_TX_PARSE_BD_E2_ETH_ADDR_TYPE, mac_type);
#if 0
  } else {
    u16 global_data = 0;
    pbd_e1x = &txdata->tx_desc_ring[bd_prod].parse_bd_e1x;
    memset(pbd_e1x, 0, sizeof(struct eth_tx_parse_bd_e1x));
    /* Set PBD in checksum offload case */
    if (xmit_type & XMIT_CSUM)
      hlen = bnx2x_set_pbd_csum(bp, skb, pbd_e1x, xmit_type);

    SET_FLAG(global_data,
       ETH_TX_PARSE_BD_E1X_ETH_ADDR_TYPE, mac_type);
    pbd_e1x->global_data |= cpu_to_le16(global_data);
  }
#endif

  /* Setup the data pointer of the first BD of the packet */
  tx_start_bd->addr_hi = cpu_to_le32(U64_HI(mapping));
  tx_start_bd->addr_lo = cpu_to_le32(U64_LO(mapping));
  tx_start_bd->nbytes = cpu_to_le16(skb_headlen(skb));
  pkt_size = tx_start_bd->nbytes;

#if 0
  iprint(LVL_DBG,
     "first bd @%p addr (%x:%x) nbytes %d flags %x vlan %x",
     tx_start_bd, tx_start_bd->addr_hi, tx_start_bd->addr_lo,
     le16_to_cpu(tx_start_bd->nbytes),
     tx_start_bd->bd_flags.as_bitfield,
     le16_to_cpu(tx_start_bd->vlan_or_ethertype));

  if (xmit_type & XMIT_GSO) {

    iprint(LVL_DBG,
       "TSO packet len %d hlen %d total len %d tso size %d",
       skb->len, hlen, skb_headlen(skb),
       skb_shinfo(skb)->gso_size);

    tx_start_bd->bd_flags.as_bitfield |= ETH_TX_BD_FLAGS_SW_LSO;

    if (unlikely(skb_headlen(skb) > hlen)) {
      nbd++;
      bd_prod = bnx2x_tx_split(bp, txdata, tx_buf,
             &tx_start_bd, hlen,
             bd_prod);
    }
    if (!CHIP_IS_E1x(bp))
      pbd_e2_parsing_data |=
        (skb_shinfo(skb)->gso_size <<
         ETH_TX_PARSE_BD_E2_LSO_MSS_SHIFT) &
         ETH_TX_PARSE_BD_E2_LSO_MSS;
    else
      bnx2x_set_pbd_gso(skb, pbd_e1x, xmit_type);
  }
#endif

  /* Set the PBD's parsing_data field if not zero
   * (for the chips newer than 57711).
   */
  if (pbd_e2_parsing_data)
    pbd_e2->parsing_data = cpu_to_le32(pbd_e2_parsing_data);

  tx_data_bd = (struct eth_tx_bd *)tx_start_bd;

#if 0
  /* Handle fragmented skb */
  for (i = 0; i < skb_shinfo(skb)->nr_frags; i++) {
    skb_frag_t *frag = &skb_shinfo(skb)->frags[i];

    mapping = skb_frag_dma_map(&bp->pdev->dev, frag, 0,
             skb_frag_size(frag), DMA_TO_DEVICE);
    if (unlikely(dma_mapping_error(&bp->pdev->dev, mapping))) {
      unsigned int pkts_compl = 0, bytes_compl = 0;

      iprintk(LVL_DBG,
         "Unable to map page - dropping packet...\n");

      /* we need unmap all buffers already mapped
       * for this SKB;
       * first_bd->nbd need to be properly updated
       * before call to bnx2x_free_tx_pkt
       */
      first_bd->nbd = cpu_to_le16(nbd);
      bnx2x_free_tx_pkt(bp, txdata,
            TX_BD(txdata->tx_pkt_prod),
            &pkts_compl, &bytes_compl);

      SME_END_TIMER(dma_lat);

      return NETDEV_TX_OK;
    }

    bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
    tx_data_bd = &txdata->tx_desc_ring[bd_prod].reg_bd;
    if (total_pkt_bd == NULL)
      total_pkt_bd = &txdata->tx_desc_ring[bd_prod].reg_bd;

    tx_data_bd->addr_hi = cpu_to_le32(U64_HI(mapping));
    tx_data_bd->addr_lo = cpu_to_le32(U64_LO(mapping));
    tx_data_bd->nbytes = cpu_to_le16(skb_frag_size(frag));
    le16_add_cpu(&pkt_size, skb_frag_size(frag));
    nbd++;

    iprint(LVL_DBG,
       "frag %d  bd @%p addr (%x:%x) nbytes %d",
       i, tx_data_bd, tx_data_bd->addr_hi, tx_data_bd->addr_lo,
       le16_to_cpu(tx_data_bd->nbytes));
  }

  iprint(LVL_DBG, "last bd @%p\n", tx_data_bd);
#endif

  /* update with actual num BDs */
  first_bd->nbd = cpu_to_le16(nbd);

  bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));

  /* now send a tx doorbell, counting the next BD
   * if the packet contains or ends with it
   */
  if (TX_BD_POFF(bd_prod) < nbd)
    nbd++;

#if 0
  /* total_pkt_bytes should be set on the first data BD if
   * it's not an LSO packet and there is more than one
   * data BD. In this case pkt_size is limited by an MTU value.
   * However we prefer to set it for an LSO packet (while we don't
   * have to) in order to save some CPU cycles in a none-LSO
   * case, when we much more care about them.
   */
  if (total_pkt_bd != NULL)
    total_pkt_bd->total_pkt_bytes = pkt_size;

  if (pbd_e1x)
    iprint(LVL_DBG,
       "PBD (E1X) @%p ip_data %x ip_hlen %u ip_id %u lso_mss %u tcp_flags %x xsum %x seq %u hlen %u",
       pbd_e1x, pbd_e1x->global_data, pbd_e1x->ip_hlen_w,
       pbd_e1x->ip_id, pbd_e1x->lso_mss, pbd_e1x->tcp_flags,
       pbd_e1x->tcp_pseudo_csum, pbd_e1x->tcp_send_seq,
        le16_to_cpu(pbd_e1x->total_hlen_w));
  if (pbd_e2)
    iprint(LVL_DBG,
       "PBD (E2) @%p dst %x %x %x src %x %x %x parsing_data %x",
       pbd_e2,
       pbd_e2->data.mac_addr.dst_hi,
       pbd_e2->data.mac_addr.dst_mid,
       pbd_e2->data.mac_addr.dst_lo,
       pbd_e2->data.mac_addr.src_hi,
       pbd_e2->data.mac_addr.src_mid,
       pbd_e2->data.mac_addr.src_lo,
       pbd_e2->parsing_data);
#endif
#if SME_DEBUG_LVL <= LVL_INFO
#if CONFIG_XEN_PACER
  iprint(LVL_DBG, "TXQ[%d] doorbell %u nbd %d TS %llu\n"
      "bd prod %u (%u) => %u pkt prod %u (%u) => %u "
      "bd cons %u (%u) pkt cons %u (%u) mapping %0llx\n"
      "G%lld [%pI4 %u, %pI4 %u] (%u:%u) xmit_type %d skb %p data %p"
      , txq_index, txdata->tx_db.data.prod
      , nbd, get_current_time(SCALE_NS)
      , txdata->tx_bd_prod, (unsigned int) TX_BD(txdata->tx_bd_prod), bd_prod
      , txdata->tx_pkt_prod, (unsigned int) TX_BD(txdata->tx_pkt_prod)
      , (unsigned int) TX_BD(txdata->tx_pkt_prod+1)
      , txdata->tx_bd_cons, (unsigned int) TX_BD(txdata->tx_bd_cons)
      , txdata->tx_pkt_cons, (unsigned int) TX_BD(txdata->tx_pkt_cons)
      , mapping
      , (pcm ? pcm->profq_idx : -1)
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , (tcph ? htonl(tcph->seq) : -1), (tcph ? htonl(tcph->ack_seq) : -1)
      , xmit_type, skb, skb->data
      );
#else
  iprint(LVL_DBG, "TXQ[%d] doorbell %u nbd %d TS %llu\n"
      "bd prod %u (%u) => %u pkt prod %u (%u) "
      "bd cons %u (%u) pkt cons %u (%u)"
      , txq_index, txdata->tx_db.data.prod, nbd, get_current_time(SCALE_NS)
      , txdata->tx_bd_prod, (unsigned int) TX_BD(txdata->tx_bd_prod), bd_prod
      , txdata->tx_pkt_prod, (unsigned int) TX_BD(txdata->tx_pkt_prod)
      , txdata->tx_bd_cons, (unsigned int) TX_BD(txdata->tx_bd_cons)
      , txdata->tx_pkt_cons, (unsigned int) TX_BD(txdata->tx_pkt_cons)
      );
#endif
#endif

//#if !CONFIG_SELECT_TXQ
#if 0
  netdev_tx_sent_queue(txq, skb->len);
#endif
//#endif

  skb_tx_timestamp(skb);

  txdata->tx_pkt_prod++;

  // XXX: this must be done irrespective of dummy queue for xen pacer
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  txdata->tx_bd_prod += nbd;
#endif

  /*
   * Make sure that the BD data is updated before updating the producer
   * since FW might read the BD right after the producer is updated.
   * This is only applicable for weak-ordered memory model archs such
   * as IA-64. The following barrier is also mandatory since FW will
   * assumes packets must have BDs.
   */
  wmb();

  // XXX: this must be done irrespective of dummy queue for xen pacer
#if !CONFIG_XEN_PACER || !CONFIG_XEN_PACER_DB
  txdata->tx_db.data.prod += nbd;
  barrier();

  DOORBELL(bp, txdata->cid, txdata->tx_db.raw);

  mmiowb();

  txdata->tx_bd_prod += nbd;
#endif

//#if !CONFIG_SELECT_TXQ
  if (unlikely(bnx2x_tx_avail(bp, txdata) < MAX_DESC_PER_TX_PKT)) {
    iprint(LVL_DBG, "TXQ[%d] doorbell %u nbd %d avail %d TS %llu\n"
        "bd prod %u (%u) => %u pkt prod %u (%u) => %u "
        "bd cons %u (%u) pkt cons %u (%u) hw cons %u"
        , txq_index, txdata->tx_db.data.prod, nbd, bnx2x_tx_avail(bp, txdata)
        , get_current_time(SCALE_NS)
        , txdata->tx_bd_prod, (unsigned int) TX_BD(txdata->tx_bd_prod), bd_prod
        , txdata->tx_pkt_prod, (unsigned int) TX_BD(txdata->tx_pkt_prod)
        , (unsigned int) TX_BD(txdata->tx_pkt_prod+1)
        , txdata->tx_bd_cons, (unsigned int) TX_BD(txdata->tx_bd_cons)
        , txdata->tx_pkt_cons, (unsigned int) TX_BD(txdata->tx_pkt_cons)
        , le16_to_cpu(*txdata->tx_cons_sb)
        );

    netif_tx_stop_queue(txq);

    /* paired memory barrier is in bnx2x_tx_int(), we have to keep
     * ordering of set_bit() in netif_tx_stop_queue() and read of
     * fp->bd_tx_cons */
    smp_mb();

    bnx2x_fp_qstats(bp, txdata->parent_fp)->driver_xoff++;
    if (bnx2x_tx_avail(bp, txdata) >= MAX_DESC_PER_TX_PKT)
      netif_tx_wake_queue(txq);
  }
//#endif
  txdata->tx_pkt++;


  SME_END_TIMER_ARR(dma_lat, smp_processor_id());

  return NETDEV_TX_OK;
}

#if CONFIG_XEN_PACER
#if !CONFIG_XEN_PACER_SLOTSHARE
int
pacer_merged_nicq_bnx2x_tx_int(struct bnx2x *bp, struct bnx2x_fp_txdata *txdata)
{
  struct netdev_queue *txq;
  u16 hw_cons, sw_cons, bd_cons = txdata->tx_bd_cons;
  unsigned int pkts_compl = 0, bytes_compl = 0;
  struct sw_tx_bd *tx_buf = NULL;
  u16 orig_bd_cons, orig_pkt_cons;
  struct sk_buff *skb = NULL;
  int txq_index = txdata->txq_index;
  int real_per_irq = 0, dummy_per_irq = 0;

#ifdef BNX2X_STOP_ON_ERROR
	if (unlikely(bp->panic))
		return -1;
#endif

  orig_bd_cons = txdata->tx_bd_cons;
  orig_pkt_cons = txdata->tx_pkt_cons;

  txq = netdev_get_tx_queue(bp->dev, txdata->txq_index);
  hw_cons = le16_to_cpu(*txdata->tx_cons_sb);
  sw_cons = txdata->tx_pkt_cons;

  while (sw_cons != hw_cons) {
    u16 pkt_cons;

    pkt_cons = TX_BD(sw_cons);

    tx_buf = &txdata->tx_buf_ring[pkt_cons];
    if (nic_txintq[txq_index]->queue_p[pkt_cons]->skb_maddr == 0) {
      iprint(LVL_EXP,
        "HW BUG!!! TXQ[%d]: hw_cons %u sw_cons %u (%u) pkt_cons %u bd_cons %u\n"
        "bd_prod %u pkt_prod %u doorbell %u skb %llu"
        , txdata->txq_index, hw_cons, sw_cons, pkt_cons
        , txdata->tx_pkt_cons, txdata->tx_bd_cons
        , txdata->tx_bd_prod, txdata->tx_pkt_prod
        , txdata->tx_db.data.prod
        , nic_txintq[txq_index]->queue_p[pkt_cons]->skb_maddr
        );
    } else {
      iprint(LVL_INFO,
       "TXQ[%d]: m 0x%0llx\n"
       "hw_cons %u sw_cons %u (%u) pkt_cons %u bd_cons %u "
       "bd_prod %u pkt_prod %u doorbell %u"
       , txdata->txq_index, nic_txintq[txq_index]->queue_p[pkt_cons]->skb_maddr
       , hw_cons, sw_cons, pkt_cons
       , txdata->tx_pkt_cons, txdata->tx_bd_cons
       , txdata->tx_bd_prod, txdata->tx_pkt_prod
       , txdata->tx_db.data.prod);
    }

    sw_cons++;

    skb = __va((machine_to_phys(
            XMADDR(nic_txintq[txq_index]->queue_p[pkt_cons]->skb_maddr))).paddr);

    if (nic_txintq[txq_index]->queue_p[pkt_cons]->is_real == 0) {
      bd_cons = sme_bnx2x_free_tx_pkt(bp, txdata, pkt_cons,
          &pkts_compl, &bytes_compl, 0, 0, 0, 0, 0, 0, 0, 0, skb);
      dummy_per_irq++;
      continue;
    }

    bd_cons = bnx2x_free_tx_pkt(bp, txdata, pkt_cons, &pkts_compl, &bytes_compl);
    real_per_irq++;
  }

  txdata->tx_pkt_cons = sw_cons;
  txdata->tx_bd_cons = bd_cons;

  /* Need to make the tx_bd_cons update visible to start_xmit()
   * before checking for netif_tx_queue_stopped().  Without the
   * memory barrier, there is a small possibility that
   * start_xmit() will miss it and cause the queue to be stopped
   * forever.
   * On the other hand we need an rmb() here to ensure the proper
   * ordering of bit testing in the following
   * netif_tx_queue_stopped(txq) call.
   */
  smp_mb();

  SME_ADD_COUNT_POINT_ARR(dummy_per_irq, dummy_per_irq, smp_processor_id());
  SME_ADD_COUNT_POINT_ARR(real_per_irq, real_per_irq, smp_processor_id());

  // TODO: check if netif_tx_queue_stopped check required

  return 0;
}


int
pacer_bnx2x_prep_dma(struct sk_buff *skb, struct net_device *dev,
    profile_conn_map_t *pcm)
{
  struct bnx2x *bp = netdev_priv(dev);

  struct netdev_queue *txq;
  struct ethhdr *eth;
  int txq_index;
  dma_addr_t mapping;
  profq_elem_t *pelem = NULL;
  nic_txdata_t *nic_data_elem = NULL;
#if CONFIG_NIC_QUEUE == CONFIG_NQ_DYNAMIC
  nic_fl_elem_t ins_elem;
  int ins_ret1 = -1, ins_ret2 = -1, init_ret = -1;
#endif

  u32 pbd_e2_parsing_data = 0;
  u32 xmit_type = bnx2x_xmit_type(bp, skb);
  u8 mac_type = UNICAST_ADDRESS;

  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  int tcplen = 0;

  SME_INIT_TIMER(nicq_ins);

  if (CONFIG_SW_CSUM) {
    iph = ip_hdr(skb);
    tcph = tcp_hdr(skb);
    tcplen = ntohs(iph->tot_len) - (iph->ihl * 4);
    tcph->check = 0;
    tcph->check = tcp_v4_check(tcplen, iph->saddr, iph->daddr,
        csum_partial((char *) tcph, tcplen, 0));
  }

  txq_index = skb_get_queue_mapping(skb);
  txq = netdev_get_tx_queue(dev, txq_index);
  eth = (struct ethhdr *)skb->data;

  // TODO: check if bnx2x_tx_avail check required

  /* Map skb linear data for DMA */
  mapping = dma_map_single(&bp->pdev->dev, skb->data,
         skb_headlen(skb), DMA_TO_DEVICE);
  if (unlikely(dma_mapping_error(&bp->pdev->dev, mapping))) {
    iprintk(LVL_ERR, "SKB mapping failed - silently dropping this SKB\n");
    dev_kfree_skb_any(skb);

    return NETDEV_TX_OK;
  }

  // populate into nic_txdata
  pelem = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];

  if (!pelem) {
    printk(KERN_ERR "PORT %u HyP %d G%lld pelem %p\n"
        , pcm->sec_conn_id.dst_port, pcm->hypace_idx, pcm->profq_idx, pelem
        );
    return NETDEV_TX_BUSY;
  }
  if (!pelem->nic_data_p) {
    struct tcphdr *tcph = NULL;
    parse_skb_headers(skb, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, (void **) &tcph, NULL);
    if (pcm->sec_conn_id.src_port != 11211 && pcm->sec_conn_id.src_port != 0) {
      printk(KERN_ERR "%s:%d PORT [%u, %u] HyP %d pcm state %d sk state %d "
        "skb (%u:%u) [%d %d %d %d %d %d %d %d] len %d\n"
        "G%lld Q prod %d cons %d state %d paused %d W %d N %d P %d T %d R %d D %d"
        , __func__, __LINE__
        , pcm->sec_conn_id.src_port
        , pcm->sec_conn_id.dst_port, pcm->hypace_idx, pcm->state
        , skb->sk ? skb->sk->sk_state : -1
        , htonl(tcph->seq), htonl(tcph->ack_seq)
        , tcph->fin, tcph->syn, tcph->rst, tcph->psh
        , tcph->ack, tcph->urg, tcph->ece, tcph->cwr, ntohs(tcph->urg_ptr)
        , pcm->profq_idx, pelem->nic_prod_idx, pelem->nic_cons_idx
        , pelem->state, atomic_read(&pelem->is_paused)
        , pelem->cwnd_watermark, pelem->next_timer_idx, pelem->num_timers
        , pelem->tail, atomic_read(&pelem->real_counter), pelem->dummy_counter
        );
    }
    return NETDEV_TX_BUSY;
  }

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  nic_data_elem = pelem->nic_data_p[pelem->nic_prod_idx];
  //nic_data_elem = &pelem->nic_data[pelem->nic_prod_idx];
#else

  init_ret = init_nicq_hdr_missing(&pcm->pcm_nicq, global_nicq[pcm->hypace_idx],
      global_nic_freelist[pcm->hypace_idx]);
  if (init_ret < 0) {
#if 0
    {
      profq_elem_t *prof_p = NULL;
      struct sock *sk = NULL;
      struct tcp_sock *tp = NULL;
      struct inet_connection_sock *inet_sk = NULL;
      largef_elem_t e;
      int dbg_idx = 0;
      uint16_t d_port = ntohs(tcph->dest);
      prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
      sk = pcm->tcp_sock;
      tp = sk ? tcp_sk(sk) : NULL;
      inet_sk = sk ? inet_csk(sk) : NULL;
      memset((void *) &e, 0, sizeof(largef_elem_t));
      e.ts = get_current_time(SCALE_NS);
      e.snd_nxt = tp ? tp->snd_nxt : 0;
      e.snd_una = tp ? tp->snd_una : 0;
      e.rcv_nxt = tp ? tp->rcv_nxt : 0;
      e.packets_out = tp ? tp->packets_out : 0;
      e.lost_out = tp ? tp->lost_out : 0;
      e.sacked_out = tp ? tp->sacked_out : 0;
      e.retrans_out = tp ? tp->retrans_out : 0;
      e.cwnd = tp ? tp->snd_cwnd : 0;
      e.snd_wnd = tp ? tp->snd_wnd : 0;
      e.rcv_wnd = tp ? tp->rcv_wnd : 0;
      e.icsk_rto = htonl(tcph->seq);
  //    e.icsk_rto = inet_sk ? inet_sk->icsk_rto : 0;
      e.cwm = prof_p->cwnd_watermark;
      e.tail = prof_p->tail;
      e.real = atomic_read(&prof_p->real_counter);
      e.dummy = prof_p->dummy_counter;
      e.num_timers = prof_p->num_timers;
      e.nxt = prof_p->next_timer_idx;
      e.pidx = prof_p->profq_idx;
      e.ca_state = inet_sk ? inet_sk->icsk_ca_state : 0;
      e.caller = 8;
      e.coreid = smp_processor_id();
      e.owner = sk ? sk->sk_lock.owned : 0;
      e.undo_marker = tp->undo_marker;
      e.undo_retrans = tp->undo_retrans;
      e.high_seq = tp->high_seq;
      dbg_idx = d_port % STATQ_MODULO;
      if (generic_q_empty(largef_q[dbg_idx]))
        largef_q[dbg_idx]->port = d_port;
      put_generic_q(largef_q[dbg_idx], (void *) &e);
    }
#endif
    return NETDEV_TX_BUSY;
  }

  ins_ret1 = get_one_nic_freelist(global_nic_freelist[pcm->hypace_idx], &ins_elem);
  if (ins_ret1 < 0) {
#if 0
    {
      profq_elem_t *prof_p = NULL;
      struct sock *sk = NULL;
      struct tcp_sock *tp = NULL;
      struct inet_connection_sock *inet_sk = NULL;
      largef_elem_t e;
      int dbg_idx = 0;
      uint16_t d_port = ntohs(tcph->dest);
      prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
      sk = pcm->tcp_sock;
      tp = sk ? tcp_sk(sk) : NULL;
      inet_sk = sk ? inet_csk(sk) : NULL;
      memset((void *) &e, 0, sizeof(largef_elem_t));
      e.ts = get_current_time(SCALE_NS);
      e.snd_nxt = tp ? tp->snd_nxt : 0;
      e.snd_una = tp ? tp->snd_una : 0;
      e.rcv_nxt = tp ? tp->rcv_nxt : 0;
      e.packets_out = tp ? tp->packets_out : 0;
      e.lost_out = tp ? tp->lost_out : 0;
      e.sacked_out = tp ? tp->sacked_out : 0;
      e.retrans_out = tp ? tp->retrans_out : 0;
      e.cwnd = tp ? tp->snd_cwnd : 0;
      e.snd_wnd = tp ? tp->snd_wnd : 0;
      e.rcv_wnd = tp ? tp->rcv_wnd : 0;
      e.icsk_rto = htonl(tcph->seq);
  //    e.icsk_rto = inet_sk ? inet_sk->icsk_rto : 0;
      e.cwm = prof_p->cwnd_watermark;
      e.tail = prof_p->tail;
      e.real = atomic_read(&prof_p->real_counter);
      e.dummy = prof_p->dummy_counter;
      e.num_timers = prof_p->num_timers;
      e.nxt = prof_p->next_timer_idx;
      e.pidx = prof_p->profq_idx;
      e.ca_state = inet_sk ? inet_sk->icsk_ca_state : 0;
      e.caller = 9;
      e.coreid = smp_processor_id();
      e.owner = sk ? sk->sk_lock.owned : 0;
      e.undo_marker = tp->undo_marker;
      e.undo_retrans = tp->undo_retrans;
      e.high_seq = tp->high_seq;
      dbg_idx = d_port % STATQ_MODULO;
      if (generic_q_empty(largef_q[dbg_idx]))
        largef_q[dbg_idx]->port = d_port;
      put_generic_q(largef_q[dbg_idx], (void *) &e);
    }
#endif
    return NETDEV_TX_BUSY;
  }
  nic_data_elem = global_nicq[pcm->hypace_idx]->queue_p[ins_elem.offset];
#endif

  if (!nic_data_elem) {
    iprint(LVL_EXP, "PORT %u HyP %d G%lld Q prod %d cons %d\n"
        "state %d paused %d W %d N %d P %d T %d R %d D %d"
        , pcm->sec_conn_id.dst_port, pcm->hypace_idx, pcm->profq_idx
        , pelem->nic_prod_idx, pelem->nic_cons_idx
        , pelem->state, atomic_read(&pelem->is_paused)
        , pelem->cwnd_watermark, pelem->next_timer_idx, pelem->num_timers
        , pelem->tail, atomic_read(&pelem->real_counter), pelem->dummy_counter
        );
    return NETDEV_TX_BUSY;
  }

  nic_data_elem->tx_tsc = get_current_time(SCALE_RDTSC);

  nic_data_elem->skb_maddr = (virt_to_machine(skb)).maddr;

  bnx2x_set_pbd_csum_e2(bp, skb, &pbd_e2_parsing_data, xmit_type);
  SET_FLAG(pbd_e2_parsing_data, ETH_TX_PARSE_BD_E2_ETH_ADDR_TYPE, mac_type);
  nic_data_elem->pbd2_parsing_data = cpu_to_le32(pbd_e2_parsing_data);

  nic_data_elem->txbd_addr_hi = cpu_to_le32(U64_HI(mapping));
  nic_data_elem->txbd_addr_lo = cpu_to_le32(U64_LO(mapping));
  nic_data_elem->txbd_nbytes = cpu_to_le16(skb_headlen(skb));
  nic_data_elem->txbd_vlan_or_ethertype = cpu_to_le16(ntohs(eth->h_proto));
  nic_data_elem->txbd_flags = ETH_TX_BD_FLAGS_START_BD;
  if (!CONFIG_SW_CSUM)
    nic_data_elem->txbd_flags |= ETH_TX_BD_FLAGS_L4_CSUM;
  /* header nbd: indirectly zero other flags! */
  nic_data_elem->txbd_general_data = 1 << ETH_TX_START_BD_HDR_NBDS_SHIFT;
  nic_data_elem->txbuf_flags = 0;
  ether_addr_copy(nic_data_elem->s_mac, eth->h_source);
  ether_addr_copy(nic_data_elem->d_mac, eth->h_dest);

  skb_tx_timestamp(skb);

  // barrier to ensure the nic_data_elem is populated before hypace picks it up
  smp_mb();

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  SME_START_TIMER(nicq_ins);

  pelem->nic_prod_idx = (pelem->nic_prod_idx+1) % NIC_DATA_QSIZE;

  SME_END_TIMER_ARR(nicq_ins, smp_processor_id());
#else

  SME_START_TIMER(nicq_ins);

    ins_ret2 = nicq_insert(&pcm->pcm_nicq, global_nicq[pcm->hypace_idx],
        ins_elem.offset, pcm);

  SME_END_TIMER_ARR(nicq_ins, smp_processor_id());

    if (ins_ret2 < 0) {
#if 0
    {
      profq_elem_t *prof_p = NULL;
      struct sock *sk = NULL;
      struct tcp_sock *tp = NULL;
      struct inet_connection_sock *inet_sk = NULL;
      largef_elem_t e;
      int dbg_idx = 0;
      uint16_t d_port = ntohs(tcph->dest);
      prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
      sk = pcm->tcp_sock;
      tp = sk ? tcp_sk(sk) : NULL;
      inet_sk = sk ? inet_csk(sk) : NULL;
      memset((void *) &e, 0, sizeof(largef_elem_t));
      e.ts = get_current_time(SCALE_NS);
      e.snd_nxt = tp ? tp->snd_nxt : 0;
      e.snd_una = tp ? tp->snd_una : 0;
      e.rcv_nxt = tp ? tp->rcv_nxt : 0;
      e.packets_out = tp ? tp->packets_out : 0;
      e.lost_out = tp ? tp->lost_out : 0;
      e.sacked_out = tp ? tp->sacked_out : 0;
      e.retrans_out = tp ? tp->retrans_out : 0;
      e.cwnd = tp ? tp->snd_cwnd : 0;
      e.snd_wnd = tp ? tp->snd_wnd : 0;
      e.rcv_wnd = tp ? tp->rcv_wnd : 0;
      e.icsk_rto = htonl(tcph->seq);
  //    e.icsk_rto = inet_sk ? inet_sk->icsk_rto : 0;
      e.cwm = prof_p->cwnd_watermark;
      e.tail = prof_p->tail;
      e.real = atomic_read(&prof_p->real_counter);
      e.dummy = prof_p->dummy_counter;
      e.num_timers = prof_p->num_timers;
      e.nxt = prof_p->next_timer_idx;
      e.pidx = prof_p->profq_idx;
      e.ca_state = inet_sk ? inet_sk->icsk_ca_state : 0;
      e.caller = 10;
      e.coreid = smp_processor_id();
      e.owner = sk ? sk->sk_lock.owned : 0;
      e.undo_marker = tp->undo_marker;
      e.undo_retrans = tp->undo_retrans;
      e.high_seq = tp->high_seq;
      dbg_idx = d_port % STATQ_MODULO;
      if (generic_q_empty(largef_q[dbg_idx]))
        largef_q[dbg_idx]->port = d_port;
      put_generic_q(largef_q[dbg_idx], (void *) &e);
    }
#endif
      put_one_nic_freelist(global_nic_freelist[pcm->hypace_idx], &ins_elem);
      return NETDEV_TX_BUSY;
    }
#endif /* CONFIG_NIC_QUEUE */

#if SME_DEBUG_LVL <= LVL_INFO
//  parse_skb_headers(skb, NULL, NULL, NULL, NULL, NULL, NULL,
//      NULL, NULL, NULL, NULL, NULL, NULL, NULL, (void **) &tcph, NULL);
    iprint(LVL_INFO, "[%pI4 %u, %pI4 %u]\n"
      "G%lld state %d paused %d CWM %d NXT %d P %d T %d R %d D %d "
      "seq (%u:%u) [%d %d %d %d %d %d %d %d] xmit_type %d"
      "\nskb %p m %0llx data %p dma %0llx skb len %d tail %d end %d"
#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
      "\nQ P %d C %d"
#else
      "\nINS %d r1 %d r2 %d FL P %d C %d"
#endif
      " bd prod %u cons %u pkt prod %u cons %u"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->profq_idx, pelem->state, atomic_read(&pelem->is_paused)
      , pelem->cwnd_watermark, pelem->next_timer_idx
      , pelem->num_timers, pelem->tail, atomic_read(&pelem->real_counter)
      , pelem->dummy_counter, htonl(tcph->seq), htonl(tcph->ack_seq)
      , tcph->fin, tcph->syn, tcph->rst, tcph->psh
      , tcph->ack, tcph->urg, tcph->ece, tcph->cwr
      , xmit_type
      , skb, nic_data_elem->skb_maddr, skb->head, mapping
      , skb->len, skb->tail, skb->end
#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
      , pelem->nic_prod_idx, pelem->nic_cons_idx
#else
      , ins_elem.offset, ins_ret1, ins_ret2
      , global_nic_freelist[pcm->hypace_idx]->prod_idx
      , global_nic_freelist[pcm->hypace_idx]->cons_idx
#endif
      , bp->bnx2x_txq[txq_index].tx_bd_prod, bp->bnx2x_txq[txq_index].tx_bd_cons
      , bp->bnx2x_txq[txq_index].tx_pkt_prod, bp->bnx2x_txq[txq_index].tx_pkt_cons
      );
#endif
  // TODO: check if bnx2x_tx_avail check required

#if 0
  {
    profq_elem_t *prof_p = NULL;
    struct sock *sk = NULL;
    struct tcp_sock *tp = NULL;
    struct inet_connection_sock *inet_sk = NULL;
    largef_elem_t e;
    int dbg_idx = 0;
    uint16_t d_port = ntohs(tcph->dest);
    prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
    sk = pcm->tcp_sock;
    tp = sk ? tcp_sk(sk) : NULL;
    inet_sk = sk ? inet_csk(sk) : NULL;
    memset((void *) &e, 0, sizeof(largef_elem_t));
    e.ts = get_current_time(SCALE_NS);
    e.snd_nxt = tp ? tp->snd_nxt : 0;
    e.snd_una = tp ? tp->snd_una : 0;
    e.rcv_nxt = tp ? tp->rcv_nxt : 0;
    e.packets_out = tp ? tp->packets_out : 0;
    e.lost_out = tp ? tp->lost_out : 0;
    e.sacked_out = tp ? tp->sacked_out : 0;
    e.retrans_out = tp ? tp->retrans_out : 0;
    e.cwnd = tp ? tp->snd_cwnd : 0;
    e.snd_wnd = tp ? tp->snd_wnd : 0;
    e.rcv_wnd = tp ? tp->rcv_wnd : 0;
    e.icsk_rto = htonl(tcph->seq); // re-using rto field to track seq# sent here
//    e.icsk_rto = inet_sk ? inet_sk->icsk_rto : 0;
    e.cwm = prof_p->cwnd_watermark;
    e.tail = prof_p->tail;
    e.real = atomic_read(&prof_p->real_counter);
    e.dummy = prof_p->dummy_counter;
    e.num_timers = prof_p->num_timers;
    e.nxt = prof_p->next_timer_idx;
    e.pidx = prof_p->profq_idx;
    e.ca_state = inet_sk ? inet_sk->icsk_ca_state : 0;
    e.caller = 7;
    e.coreid = smp_processor_id();
    e.owner = sk ? sk->sk_lock.owned : 0;
    e.undo_marker = tp ? tp->undo_marker : 0;
    e.undo_retrans = tp ? tp->undo_retrans : 0;
    e.high_seq = tp ? tp->high_seq : 0;
    dbg_idx = d_port % STATQ_MODULO;
    if (generic_q_empty(largef_q[dbg_idx]))
      largef_q[dbg_idx]->port = d_port;
    put_generic_q(largef_q[dbg_idx], (void *) &e);
  }
#endif
  return NETDEV_TX_OK;
}
#endif /* !CONFIG_XEN_PACER_SLOTSHARE */

void
prepare_dummy_nicq(global_nicq_hdr_t *nicq, struct sk_buff **dummy_skb,
    dma_addr_t *dummy_dmaddr)
{
  int i;
  nic_txdata_t *nic_data_elem = NULL;

  u32 pbd_e2_parsing_data = 0;
  u32 xmit_type = 0;
  u8 mac_type = UNICAST_ADDRESS;

  if (!CONFIG_SW_CSUM)
    xmit_type = XMIT_CSUM_V4 | XMIT_CSUM_TCP;

  for (i = 0; i < nicq->qsize; i++) {
    nic_data_elem = nicq->queue_p[i];
    nic_data_elem->skb_maddr = (virt_to_machine(dummy_skb[i])).maddr;
    bnx2x_set_pbd_csum_e2(NULL, dummy_skb[i], &pbd_e2_parsing_data, xmit_type);
    SET_FLAG(pbd_e2_parsing_data, ETH_TX_PARSE_BD_E2_ETH_ADDR_TYPE, mac_type);
    nic_data_elem->pbd2_parsing_data = cpu_to_le32(pbd_e2_parsing_data);

    nic_data_elem->txbd_addr_hi = cpu_to_le32(U64_HI(dummy_dmaddr[i]));
    nic_data_elem->txbd_addr_lo = cpu_to_le32(U64_LO(dummy_dmaddr[i]));
    nic_data_elem->txbd_nbytes = cpu_to_le16(skb_headlen(dummy_skb[i]));
    nic_data_elem->txbd_vlan_or_ethertype = cpu_to_le16(ntohs(htons(ETH_P_IP)));
    nic_data_elem->txbd_flags = ETH_TX_BD_FLAGS_START_BD;
    if (!CONFIG_SW_CSUM)
      nic_data_elem->txbd_flags |= ETH_TX_BD_FLAGS_L4_CSUM;
    nic_data_elem->txbd_general_data = 1 << ETH_TX_START_BD_HDR_NBDS_SHIFT;
    nic_data_elem->txbuf_flags = 0;
    // useless for dummies
    nic_data_elem->tx_tsc = 0;
  }

  iprint(LVL_EXP, "xmit_type %d txbd_flags 0x%0x vlan_or_ethertype %d"
      , xmit_type, nicq->queue_p[0]->txbd_flags, nicq->queue_p[0]->txbd_vlan_or_ethertype
      );
}

#endif /* CONFIG_XEN_PACER */
