/*
 * profile.h
 *
 * created on: Nov 18, 2018
 * author: aasthakm
 *
 * datastructure for a profile in profile database
 */

#ifndef __PROFILE_H__
#define __PROFILE_H__

#include "../../include/profile_conn_format.h"
#include "conn.h"
#include "list.h"

typedef struct profile {
  /*
   * unique ID agreed between the app, kernel modules
   */
  ihash_t id_ihash;
  /*
   * conn for which profile installed
   */
  conn_t conn_id;
  /*
   * pointer to profile internal datastructure
   */
  void *priv;
  list_t profile_listp;
} profile_t;

#endif /* __PROFILE_H__ */
