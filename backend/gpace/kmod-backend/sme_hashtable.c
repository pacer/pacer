/*
 * sme_hashtable.c
 *
 * created on: Nov 8, 2017
 * author: aasthakm
 *
 * hashtable for pcm lookup data structure
 */

#include "../../include/conn_common.h"
#include "sme_config.h"
#include "sme_debug.h"
#include "sme_hashtable.h"
#include "sme_statistics.h"
#include <linux/slab.h>
#include <linux/rculist.h>

//SME_DECL_STAT_EXTERN(htbl_idx);

int
init_htable(sme_htable_t *htbl, int size, hash_fn_t *hfn, int lock)
{
  int i;
  if (!htbl || !size || !hfn)
    return -EINVAL;

  htbl->pcm_list = kzalloc(size * sizeof(list_t), GFP_KERNEL);
  if (!htbl->pcm_list) {
    return -ENOMEM;
  }

  if (lock) {
    htbl->lock = kzalloc(size * sizeof(spinlock_t), GFP_KERNEL);
    if (!htbl->lock) {
      kfree(htbl->pcm_list);
      return -ENOMEM;
    }

    for (i = 0; i < size; i++)
      spin_lock_init(&htbl->lock[i]);
  }

  for (i = 0; i < size; i++) {
    INIT_LIST_HEAD(&htbl->pcm_list[i]);
  }

  htbl->size = size;
  htbl->ht_func = hfn;
  iprint(LVL_INFO, "Init PCM HT of size: %d", htbl->size);
  return 0;
}

void
cleanup_htable(sme_htable_t *htbl)
{
  int i;
  if (!htbl->ht_func && !htbl->pcm_list && !htbl->size)
    return;

  // we don't need to lock in cleanup function at the moment,
  // since it is only invoked at rmmod.
  if (htbl->pcm_list) {
    for (i = 0; i < htbl->size; i++) {

      if (htbl->lock)
        spin_lock_bh(&htbl->lock[i]);

      htbl->ht_func->free(&htbl->pcm_list[i]);

      if (htbl->lock)
        spin_unlock_bh(&htbl->lock[i]);
    }

    kfree(htbl->pcm_list);
  }

  if (htbl->lock)
    kfree(htbl->lock);

  memset(htbl, 0, sizeof(sme_htable_t));

  iprintk(LVL_INFO, "Cleanup PCM HT");
}

int
htable_key_idx(sme_htable_t *htbl, void *key, int keylen)
{
  uint32_t idx = htbl->ht_func->hash(key, keylen, 0);
  int idx2 = idx & (htbl->size-1);
  ipport_t *ipp = (ipport_t *)key;

  iprint(LVL_DBG, "key [%pI4 %u, %pI4 %u], idx %u, idx2 %d"
      , (void *) &ipp->src_ip, ipp->src_port
      , (void *) &ipp->dst_ip, ipp->dst_port, idx, idx2);
  return idx2;
}

void
htable_insert(sme_htable_t *htbl, void *key, int keylen, list_t *elem_listp)
{
  int idx;
//  int ret;
//  void *entry = NULL;

  idx = htable_key_idx(htbl, key, keylen);
  if (idx < 0)
    return;

//  SME_ADD_COUNT_POINT(htbl_idx, idx);
//  ret = htbl->ht_func->lookup(&htbl->pcm_list[idx], key, keylen, &entry);
//  iprint(LVL_DBG, "insert KV at idx %d, exists match %d", idx, ret);
  if (htbl->lock)
    spin_lock_bh(&htbl->lock[idx]);

  list_add_tail(elem_listp, &htbl->pcm_list[idx]);

  if (htbl->lock)
    spin_unlock_bh(&htbl->lock[idx]);
}

void
htable_insert_unique(sme_htable_t *htbl, void *key, int keylen, list_t *elem_listp)
{
  int idx;
  int ret;
  void *entry = NULL;
  ipport_t *ipkey = (ipport_t *) key;

  idx = htable_key_idx(htbl, key, keylen);
  if (idx < 0)
    return;

  if (htbl->lock)
    spin_lock_bh(&htbl->lock[idx]);

  ret = htbl->ht_func->lookup(&htbl->pcm_list[idx], key, keylen, &entry);
  if (ret < 0) {
    list_add_tail(elem_listp, &htbl->pcm_list[idx]);
    iprint(LVL_INFO, "insert KV at idx %d, exists [%pI4 %u, %pI4 %u], match %d"
        , idx, (void *) &ipkey->src_ip, ipkey->src_port
        , (void *) &ipkey->dst_ip, ipkey->dst_port, ret);
  }

  if (htbl->lock)
    spin_unlock_bh(&htbl->lock[idx]);
}

void *
htable_lookup(sme_htable_t *htbl, void *key, int keylen)
{
  void *entry = NULL;
  list_t *pcm_list = NULL;
  int ret;
  int idx;

  idx = htable_key_idx(htbl, key, keylen);

  pcm_list = &htbl->pcm_list[idx];
  ret = htbl->ht_func->lookup(pcm_list, key, keylen, &entry);
  iprint(LVL_DBG, "lookup KV idx %d, match ret %d", idx, ret);

  if (!ret)
    return entry;

  return NULL;

}
