/*
 * sme_xen.h
 *
 * created on: Mar 23, 2018
 * author: aasthakm
 * 
 * xen related
 */

#ifndef __SME_XEN_H__
#define __SME_XEN_H__

#include "sme_config.h"
#include "profile.h"
#include "sme_spinlock.h"
#include "mem_desc.h"
#include "sme_nic.h"

//#define PROF_REF_MASK 0x4000000000000000
//#define PROF_REF_MASK (_AC(1,UL) << 30)
//#define PROF_REF_UNMASK ~(_AC(1,UL) << 30)

#define PROF_REF_MASK (1 << 30)
#define PROF_REF_UNMASK ~(1 << 30)

enum {
  PACER_OP_SET_ADDRS = 0,
  PACER_OP_UNSET_ADDRS,
  PACER_OP_WAKEUP,
  PACER_OP_PMAP_IHASH,
  PACER_OP_PROF_UPDATE,
  PACER_OP_CWND_UPDATE,
  PACER_OP_PROF_FREE,
  PACER_OP_INIT_SPIN_BENCH,
  PACER_OP_CLEANUP_SPIN_BENCH,
  PACER_OP_INIT_DB_BENCH,
  PACER_OP_CLEANUP_DB_BENCH,
  MAX_PACER_OPS
};

typedef struct hypace_arg {
#define MAX_TXBUF_FRAGS 4
  uint16_t txbuf_n_md;
  mem_desc_t txbuf_md_arr[MAX_TXBUF_FRAGS];
  uint64_t txdata_maddr;
  uint64_t doorbell_maddr;
  uint64_t profq_hdr_maddr;
  uint64_t profq_fl_maddr;
  uint64_t nicq_maddr;
  uint64_t dummy_nicq_maddr;
  uint64_t nic_fl_maddr;
  uint64_t nic_txint_maddr;
  uint16_t pacer_core;
} hypace_arg_t;

typedef struct sme_pacer_arg {
  uint32_t pmap_htbl_size;
  uint32_t pac_timerq_size;
  uint32_t nic_data_qsize;
  uint32_t cwnd_update_latency;
  uint32_t rexmit_update_latency;
  uint32_t pacer_spin_threshold;
  uint32_t global_nic_data_qsize;
  uint32_t epoch_size;
  uint32_t ptimer_heap_size;
  uint32_t max_dequeue_time;
  uint32_t max_prof_free_time;
  uint32_t max_pkts_per_epoch;
  uint32_t max_other_per_epoch;
  uint16_t config_tcp_mtu;
  uint16_t config_sw_csum;
  uint16_t max_hp_per_irq;
  uint16_t pacer_config;
  uint16_t n_hp_args;
  hypace_arg_t hparr[MAX_HP_ARGS];
} sme_pacer_arg_t;

enum {
  P_UPDATE_PROF_ID = 0,
  P_EXTEND_PROF_RETRANS,
  P_UPDATE_CWND_ACK,
  P_UPDATE_CWND_LOSS,
  MAX_P_CMDS
};

typedef struct prof_update_arg {
  int32_t profq_idx;
  int32_t hypace_idx;
  int cmd_type;
} prof_update_arg_t;

typedef struct profq_elem {
  /*
   * unique ID agreed between the app, kernel modules
   */
  ihash_t id_ihash;
  /*
   * conn for which profile installed
   */
  conn_t conn_id;
  /*
   * used in profile queue shared with the hypervisor
   */
  uint64_t req_ts;
  /*
   * used in profile queue shared with the hypervisor
   * related to cwnd updates.
   */
  uint64_t ack_ts;
  /*
   * timestamp for shifting retransmission slot upon loss event
   */
  uint64_t loss_ts;
  /*
   * physical addr of the profile DB element corr. to this profile
   */
  uint64_t prof_maddr;
  /*
   * xen updates accumulated cwn latency shift. shift the profile
   * timestamps to be played out by adding this value to the initial
   * response latency of each burst in the profile that has not been
   * played out yet. use this in the guest while matching profile prefix.
   */
  uint64_t cwnd_latency_shift;
  /*
   * array index
   */
  uint32_t profq_idx;
  /*
   * timerq index in hypace
   */
  uint32_t tobj_idx;
  /*
   * for multi-core hypace, (logical) hypace id
   */
  uint32_t hypace_idx;
  /*
   * relative offset to next element in array
   * -1 indicates NULL pointer
   */
  atomic_t next;
  /*
   * index into the timers upto which the profile is open at the moment.
   * (based on cwnd - packets_in_flight)
   * only relevant in the profiles shared with hypervisor.
   */
  int32_t cwnd_watermark;
#if CONFIG_DUMMY == DUMMY_OOB
  uint32_t *shared_seq_p;
  uint32_t *shared_dummy_out_p;
  uint32_t *shared_dummy_lsndtime_p;
  uint64_t shared_seq_maddr;
  uint64_t shared_dummy_out_maddr;
  uint64_t shared_dummy_lsndtime_maddr;
#endif
  /*
   * MAC addrs of the flow
   */
  char *src_mac_p;
  uint64_t src_mac_maddr;
  char *dst_mac_p;
  uint64_t dst_mac_maddr;
  /*
   * # of real packets transmitted by the hypervisor in this profile.
   */
  atomic_t real_counter;
  /*
   * flag for guest to know when to hypercall if change in cwnd state
   */
  atomic_t is_paused;
  /*
   * # of dummies transmitted on this profile. used to
   * ensure we do not eat into cwnd due to transmitting dummies.
   */
  uint32_t dummy_counter;
  /*
   * base value of tcp_sk->segs_out when the profile is instantiated.
   * this is used, together with real_counter, to keep track of the NIC
   * level view of packets in flight, which in turn is used to adjust
   * cwnd_watermark appropriately. this is set once at a new profile
   * installation, and does not change in the lifetime of the profile.
   */
  uint32_t initial_segs_out;
  /*
   * max seq# of outgoing pkt seen by hypace on any slot in this profile.
   * when tcp receives ack# acking this seq#, this profile is free to be reused.
   */
  uint32_t max_seqno;
  /*
   * # data and ack pkts committed to by tcp in response to this profile's
   * request. used to ensure during loss, hypace watermark does not fall
   * below all the pkts already transmitted by tcp and hypace itself.
   */
  uint32_t tail;
  /*
   * set by guest
   */
  uint32_t *last_real_ack_p;
  uint64_t last_real_ack_maddr;
  /*
   * value of tcphdr->window set in last skb,
   * to be used read-only by hypervisor for dummies
   */
  uint16_t last_skb_snd_window;
  /*
   * id for debugging only
   * state to determine if profile elements are in
   * profq_hdr or in profq_freelist (mutually exclusive).
   */
  uint16_t id:14,
           state:2;
  /*
   * total number of instantiated timers in this profile.
   * updated by guest on marker from app, or on retransmissions.
   */
  uint32_t num_timers;
  /*
   * next outstanding timer index in the profile in the hypervisor.
   * includes both real and dummies transmitted so far.
   * this field is read-only in the guest.
   */
  uint32_t next_timer_idx;
#if !CONFIG_XEN_PACER_SLOTSHARE
#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
#define MAX_NDATA_FRAGS 2
  uint16_t n_md;
  mem_desc_t md_arr[MAX_NDATA_FRAGS];
  uint16_t nic_prod_idx;
  uint16_t nic_cons_idx;
  nic_txdata_t *nic_data;
  nic_txdata_t **nic_data_p;
#else
  /*
   * pointers to indexes of per-flow NIC queue of the pcm to which this
   * profile belongs. the per-flow NIC queue is carved from a global nic_freelist.
   * XXX: this must replace the above statically sized nic_data array.
   */
  nicq_hdr_t *pcm_nicq_p;
  uint64_t pcm_nicq_maddr;
#endif /* CONFIG_NIC_QUEUE */
#endif /* CONFIG_XEN_PACER_SLOTSHARE */
  /*
   * cached pointer to profile entry in the profile database in guest
   */
  struct profile *pmap_prof;
} profq_elem_t;

typedef struct profq_hdr {
  uint64_t qsize;
  union {
    struct {
      atomic_t head;
      atomic_t tail;
      tkt_lock_t tkt_lock;
    } sorted2;
  } u;
#define MAX_QUEUE_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_QUEUE_FRAGS];
  profq_elem_t *queue;
  profq_elem_t **queue_p;
} profq_hdr_t;

// single producer (xen) multi-consumer (guest) fifo
typedef struct profq_freelist2 {
  // maddr of the idx_list
  uint64_t idx_list_maddr;
  int64_t idx_list_size;
  uint32_t prod_idx;
  uint32_t cons_idx;
#define MAX_FL_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_FL_FRAGS];
  int32_t *idx_list;
  spinlock_t lock;
} profq_freelist2_t;

#if CONFIG_XEN_PACER
void prepare_hypercall_args(sme_pacer_arg_t *harg, struct pci_dev *vf_pdev,
  profq_hdr_t **profq_hdr, profq_freelist2_t **profq_fl2, global_nicq_hdr_t **nicq,
  global_nicq_hdr_t **dummy_nicq, nic_freelist_t **nfl, nic_txintq_t **nic_txintq,
  int n_hypace);
#else
static inline void
prepare_hypercall_args(sme_pacer_arg_t *harg, struct pci_dev *vf_pdev,
  profq_hdr_t **profq_hdr, profq_freelist2_t **profq_fl2, global_nicq_hdr_t **nicq,
  global_nicq_hdr_t **dummy_nicq, nic_freelist_t **nfl, nic_txintq_t **nic_txintq,
  int n_hypace) {
  return;
}
#endif

/*
 * =============
 * profile queue
 * =============
 */

int init_profq_hdr(profq_hdr_t *profq_hdr, nic_txdata_t **nic_data_arr, int qsize);
void cleanup_profq_hdr(profq_hdr_t *profq_hdr);

int profile_get_prev_ts(profq_elem_t *p, uint64_t *ts);
int profile_get_next_ts(profq_elem_t *p, uint64_t *ts);
int check_profile_prefix(profq_elem_t *dprofq_elem, profile_t *dprof, profile_t *cprof);

/*
 * ==============
 * profq freelist
 * ==============
 */

int init_profq_freelist2(profq_freelist2_t *pfl, int qsize);
void cleanup_profq_freelist2(profq_freelist2_t *pfl);
int get_one_profq_freelist2(profq_freelist2_t *pfl, int *idx);

#endif /* __SME_XEN_H__ */
