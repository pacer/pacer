/*
 * profile_log.c
 *
 * created on: Jun 22, 2017
 * author: aasthakm
 *
 * logs network packets for profiling
 */

#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <uapi/linux/in.h>

#include "sme_config.h"
#include "sme_debug.h"
#include "sme_common.h"
#include "sme_statistics.h"

#include "../../include/log_format.h"
#include "profile_log.h"
#include "profile_map.h"
#include "sme_skb.h"

#define MAX_BUF_SIZE 1024

log_t profile_log;

//SME_DECL_STAT_EXTERN(alloc_log_elem);
//SME_DECL_STAT_EXTERN(add_log_elem);

#if CONFIG_PROF_LOG
/*
 * ============
 * log elem API
 * ============
 */

void
init_log_elem(log_elem_t *le, partial_sk_buff_t *pskb, void *optional_data,
    int optional_data_len)
{
  if (!le)
    return;

  INIT_LIST_HEAD(&le->log_listp);

  if (pskb)
    copy_pskb(&le->pskb, pskb, 1);
  if (optional_data && optional_data_len) {
    le->optional_data = optional_data;
    le->optional_data_len = optional_data_len;
  }
}

void
alloc_init_log_elem(log_elem_t **lelem, partial_sk_buff_t *pskb,
    void *optional_data, int optional_data_len)
{
  log_elem_t *le = NULL;
//  SME_INIT_TIMER(alloc_log_elem);

  if (!lelem || !pskb)
    return;

//  SME_START_TIMER(alloc_log_elem);

  le = (log_elem_t *) kzalloc(sizeof(log_elem_t), GFP_KERNEL);
  init_log_elem(le, pskb, optional_data, optional_data_len);

  *lelem = le;

//  SME_END_TIMER(alloc_log_elem);
}

void
cleanup_log_elem(log_elem_t *le)
{
  if (!le)
    return;

  if (le->optional_data && le->optional_data_len)
    kfree(le->optional_data);
  le->optional_data = NULL;
  le->optional_data_len = 0;
}

void
add_log_elem(log_t *p_log, log_elem_t *le)
{
//  SME_INIT_TIMER(add_log_elem);

  if (!p_log || !le)
    return;

//  SME_START_TIMER(add_log_elem);

  list_add_tail(&le->log_listp, &p_log->log_list);

//  SME_END_TIMER(add_log_elem);
}

int
get_sizeof_log_elem(log_elem_t *le)
{
  int ol_size = 0;

  if (!le)
    return -EINVAL;

  ol_size += sizeof(log_record_t);
  // optional data overlaps log_record_t from num_public_inputs
  if (le->optional_data && le->optional_data_len)
    ol_size += (le->optional_data_len - 2 * sizeof(uint32_t));

  return ol_size;
}

/*
 * ===========
 * generic API
 * ===========
 */

int
init_log(log_t *p_log, log_func_t *fn)
{
  if (!p_log)
    return -EINVAL;

  memset(p_log, 0, sizeof(log_t));
  INIT_LIST_HEAD(&p_log->log_list);
  p_log->fn = fn;

  return 0;
}

void
cleanup_log(log_t *p_log)
{
  int cnt = 0;
  log_elem_t *le = NULL;
  list_t *log_head, *log_head2;

  if (!p_log)
    return;

  list_for_each_safe(log_head, log_head2, &p_log->log_list) {
    le = list_entry(log_head, log_elem_t, log_listp);
    list_del_init(&le->log_listp);
    cleanup_log_elem(le);
    kfree(le);
    le = NULL;
    cnt++;
  }

  INIT_LIST_HEAD(&p_log->log_list);

  p_log->fn = NULL;
  iprint(LVL_DBG, "cleaned %d log elements", cnt);
}

void
generate_log(log_t *p_log, conn_t *c, int scm_type, char *wbuf)
{
  int elen = 0, wlen = 0;
  log_elem_t *e = NULL;

  if (!p_log || !c || !wbuf)
    return;

  list_for_each_entry(e, &p_log->log_list, log_listp) {
    elen = get_sizeof_log_elem(e);
    if (elen < 0)
      return;

    p_log->fn->create_log_record(wbuf + wlen, e, c, scm_type);
    wlen += elen;
  }
}

void
write_log(log_t *p_log, conn_t *c, int scm_type)
{
  int elen = 0;
  log_elem_t *e = NULL;
  int dir = 0;

  if (!p_log || !c)
    return;

  list_for_each_entry(e, &p_log->log_list, log_listp) {
    elen = get_sizeof_log_elem(e);
    if (elen < 0)
      return;

    if (scm_type == SME_FE_BE
        && e->pskb.saddr == c->src_ip && e->pskb.daddr == c->in_ip
        && e->pskb.source == c->src_port && e->pskb.dest == c->in_port) {
      dir = 1;
    } else if (e->pskb.saddr == c->out_ip && e->pskb.daddr == c->dst_ip
        && e->pskb.source == c->out_port && e->pskb.dest == c->dst_port) {
      dir = 0;
    } else {
      dir = 1;
    }

    iprint(LVL_INFO, "%llu, %u, %u, %u, %u, %u%u%u%u%u%u%u%u"
        , e->pskb.in_timestamp, e->pskb.data_len, e->pskb.seqno, e->pskb.seqack
        , dir, e->pskb.fin, e->pskb.syn, e->pskb.rst, e->pskb.psh
        , e->pskb.ack, e->pskb.urg, e->pskb.ece, e->pskb.cwr);
  }
}

int
count_log_entries_size(log_t *p_log, int *e_count, int *e_size)
{
  int cnt = 0;
  int l_size = 0;
  int ol_size = 0;
  log_elem_t *le = NULL;

  if (!p_log || !e_count || !e_size)
    return -EINVAL;

  list_for_each_entry(le, &p_log->log_list, log_listp) {
    ol_size = get_sizeof_log_elem(le);
    if (ol_size < 0)
      return -EINVAL;

    l_size += ol_size;
    cnt++;
  }

  *e_count = cnt;
  *e_size = l_size;

  return 0;
}

/*
 * ================
 * ASCII log format
 * ================
 */
static void
log_optional_data(void *wbuf, void *optional_data, int optional_data_len)
{
  int off = 0;
  char *ptr = NULL;
  uint16_t num_pub, num_priv;

  if (!optional_data || !optional_data_len || !wbuf)
    return;

  ptr = optional_data + off;
  num_pub = ((uint16_t *) ptr)[0];
  num_priv = ((uint16_t *) ptr)[1];
  sprintf(wbuf, "\t%u\t%u", num_pub, num_priv);
}

static void
create_log_record_ascii(void *wbuf, log_elem_t *e, conn_t *c, int scm_type)
{
  int dir = -1;
  int wlen = 0;

  if (!wbuf || !e || !c)
    return;

#if 0
  sprintf(wbuf, "0\t%u\t%u\t%llu\t%pI4\t%u\t%pI4\t%u\t%u\t%u\t%u"
      "\t[%u %u %u %u %u %u %u %u]\n"
    , e->pskb.seqno, e->pskb.seqack, e->pskb.in_timestamp
    , (void *) &e->pskb.saddr, e->pskb.source
    , (void *) &e->pskb.daddr, e->pskb.dest
    , e->pskb.data_len, 0, 0
    , e->pskb.fin, e->pskb.syn, e->pskb.rst, e->pskb.psh
    , e->pskb.ack, e->pskb.urg, e->pskb.ece, e->pskb.urg
    );
#endif
#if 0
  sprintf(wbuf, "0\t%u\t%u\t%llu\t%pI4\t%u\t%pI4\t%u\t%u\t%u\t%u\n"
    , e->pskb.seqno, e->pskb.seqack, e->pskb.in_timestamp
    , (void *) &e->pskb.saddr, e->pskb.source
    , (void *) &e->pskb.daddr, e->pskb.dest
    , e->pskb.data_len, 0, 0
    );
#endif

  if (c->out_ip == e->pskb.saddr && c->dst_ip == e->pskb.daddr
      && c->out_port == e->pskb.source && c->dst_port == e->pskb.dest)
    dir = 0;
  else
    dir = 1;
#if CONFIG_PROF_MINI_LOG
  sprintf(wbuf, "%llu\t%u\t%u\t%u\t%u\t%u"
      , e->pskb.in_timestamp, e->pskb.data_len, e->pskb.seqno, e->pskb.seqack
      , dir, e->count);
#else
  sprintf(wbuf, "%llu\t%u\t%u\t%u\t%u"
      , e->pskb.in_timestamp, e->pskb.data_len, e->pskb.seqno, e->pskb.seqack
      , dir);
#endif
  wlen = strlen(wbuf);
  if (e->optional_data) {
    log_optional_data(wbuf + wlen, e->optional_data, e->optional_data_len);
    wlen = strlen(wbuf);
  }
  sprintf(wbuf + wlen, "\n");
}

/*
 * =================
 * binary log format
 * =================
 */
static void
create_log_record_binary(void *buf, log_elem_t *e, conn_t *c, int scm_type)
{
  log_record_t *rec = NULL;
  int size_rec = 0;
  char *ptr = NULL;

  if (!buf || !e || !c)
    return;

  rec = (log_record_t *) buf;

#if CONFIG_PROF_MINI_LOG
  rec->count = e->count;
  //iprint(LVL_DBG, "CurrentConnection Number of packets =  %u", rec->count);
#endif
  rec->timestamp = e->pskb.in_timestamp;
  rec->payload_len = e->pskb.data_len;
  rec->seqno = e->pskb.seqno;
  rec->seqack = e->pskb.seqack;
  /*
   * special case: using the TS of the client req. as
   * a reference for the cross-tier and backend pcm's
   */
  if (scm_type == SME_FE_BE
      && e->pskb.saddr == c->src_ip && e->pskb.daddr == c->in_ip
      && e->pskb.source == c->src_port && e->pskb.dest == c->in_port) {
    rec->dir = 1;
  } else if (e->pskb.saddr == c->out_ip && e->pskb.daddr == c->dst_ip
      && e->pskb.source == c->out_port && e->pskb.dest == c->dst_port) {
    rec->dir = 0;
  } else {
    rec->dir = 1;
  }

#if CONFIG_PROF_MINI_LOG
  iprint(LVL_DBG, "%llu, %u, %u, %u, %u, %u"
      , ((uint64_t *) buf)[0], ((uint32_t *) buf)[2], ((uint32_t *) buf)[3]
      , ((uint32_t *) buf)[4], ((uint8_t *) buf)[20], ((uint32_t *) buf)[6]);
#else
  iprint(LVL_DBG, "%llu, %u, %u, %u, %u"
      , ((uint64_t *) buf)[0], ((uint32_t *) buf)[2], ((uint32_t *) buf)[3]
      , ((uint32_t *) buf)[4], ((uint8_t *) buf)[20]);
#endif
  if (!e->optional_data) {
    rec->num_public_inputs = 0;
    rec->num_private_inputs = 0;
    size_rec = sizeof(log_record_t);
    return;
  }

  ptr = (char *) &rec->num_public_inputs;
  memcpy(ptr, e->optional_data, e->optional_data_len);

  /*
   * the single byte of direction seems to be getting padded
   * by one byte before placing num_pub and num_priv values.
   *
   * NOTE: the direction is defined to be of type uin32_t
   */
#if CONFIG_PROF_MINI_LOG
  iprint(LVL_DBG, "sizeof lrt: %lu, count: %u,  rec TS: %llu, plen: %u, seqno: %u, seqack: %u, dir: %u"
      ", #pub: %u, #priv: %u"
      , sizeof(log_record_t),  ((uint32_t *) buf)[6]
      , ((uint64_t *) buf)[0], ((uint32_t *) buf)[2], ((uint32_t *) buf)[3]
      , ((uint32_t *) buf)[4], ((uint32_t *) buf)[5]
      , ((uint32_t *) buf)[7], ((uint32_t *) buf)[8]);

#else
  iprint(LVL_DBG, "sizeof lrt: %lu, rec TS: %llu, plen: %u, seqno: %u, seqack: %u, dir: %u"
      ", #pub: %u, #priv: %u"
      , sizeof(log_record_t)
      , ((uint64_t *) buf)[0], ((uint32_t *) buf)[2], ((uint32_t *) buf)[3]
      , ((uint32_t *) buf)[4], ((uint32_t *) buf)[5]
      , ((uint32_t *) buf)[6], ((uint32_t *) buf)[7]);
#endif
}

log_func_t log_ascii_fn = {
  .create_log_record = create_log_record_ascii
};

log_func_t log_binary_fn = {
  .create_log_record = create_log_record_binary
};

#else /* CONFIG_PROF_LOG = 0 */
void
alloc_init_log_elem(log_elem_t **le, partial_sk_buff_t *pskb,
    void *optional_data, int optional_data_len)
{
  return;
}

void
add_log_elem(log_t *p_log, log_elem_t *le)
{
  return;
}

int
get_sizeof_log_elem(log_elem_t *le)
{
  return 0;
}

int
init_log(log_t *p_log, log_func_t *fn)
{
  return 0;
}

void
cleanup_log(log_t *p_log)
{
  return;
}

void
generate_log(log_t *p_log, conn_t *c, int scm_type, char *wbuf)
{
  return;
}

void
write_log(log_t *p_log, conn_t *c, int scm_type)
{
  return;
}

int
count_log_entries_size(log_t *p_log, int *e_count, int *e_size)
{
  return 0;
}

log_func_t log_ascii_fn = {
  .create_log_record = 0
};
log_func_t log_binary_fn = {
  .create_log_record = 0
};

#endif /* CONFIG_PROF_LOG */
