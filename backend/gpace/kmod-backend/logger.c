/*
 * logger.c
 *
 * created on: Dec 05, 2017
 * author: aasthakm
 *
 * implementation of the logger thread
 */

#include "logger.h"
#include "sme_common.h"
#include "sme_time.h"
#include "sme_helper.h"
#include "sme_statistics.h"
#include "sme_stats_decl.h"

#include <linux/kthread.h>

#if CONFIG_XEN_PACER
extern pcm_q_t pcm_rxq[RX_NCPUS];
extern pcm_q_t pcm_txq[RX_NCPUS];
#endif

struct task_struct *logger = NULL;

/*
 * ===========================================
 * functions common to logger and pacer config
 * ===========================================
 */
int
is_pcm_ready_for_logging(profile_conn_map_t *pcm)
{
  uint64_t now;
  int64_t delta_ts = 0;
  int pcm_state = 0;
  int pcm_state_mask = TCPF_TIME_WAIT | TCPF_CLOSE;

  iprint(LVL_DBG, "pcm: %p tmo delta %lld\n[%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
      ", state %d, logged %d"
      , pcm, (int64_t) (pcm->tw_timeout - get_current_time(SCALE_NS))
      , (void *) &pcm->sec_conn_id.src_ip, pcm->sec_conn_id.src_port
      , (void *) &pcm->sec_conn_id.in_ip, pcm->sec_conn_id.in_port
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->state, pcm->written_proflog
      );
  if (pcm->written_proflog)
    return -EEXIST;

  pcm_state = (1 << pcm->state);
  /*
   * second condition is a hack to cleanup fake pcm's
   * generated because of cleaning up previous pcm too early.
   */
  if (((pcm_state & pcm_state_mask) == 0) && pcm->state != 0)
    return -EINVAL;

  if (pcm->state == TCP_TIME_WAIT && pcm->tw_timeout > 0) {
    now = get_current_time(SCALE_NS);
    delta_ts = (int64_t) (pcm->tw_timeout - now);
    if (delta_ts > PCM_EXPIRE_THRESHOLD)
      return -EBUSY;
  }

  return 0;
}

int
calc_log_entry_size(list_t *log_pcm_ptr_list, int *log_len, int *n_pcm,
    uint32_t **pcm_off_p, int **pcm_log_info_p, int log_type)
{
  int ret = 0;
  int total_n_pcm = 0;
  int total_log_size = 0;
  int log_hdr_size = 0;
  int log_off = 0;
  int conn_it = 0;
  uint32_t *pcm_off = NULL;
  int *pcm_log_info = NULL;
  scm_pcm_ptr_t *pcm_ptr = NULL;
  profile_conn_map_t *pcm = NULL;

  if (!log_pcm_ptr_list || list_empty(log_pcm_ptr_list))
    return -EINVAL;

  if (!log_len || !n_pcm)
    return -ENOENT;

  list_for_each_entry(pcm_ptr, log_pcm_ptr_list, listp) {
    total_n_pcm++;
  }

  total_log_size += sizeof(log_header_t) // # conn
    + total_n_pcm*sizeof(uint32_t) // offsets for log buffer of each pcm
    ;

  log_hdr_size = total_log_size;

  total_log_size +=
    total_n_pcm*sizeof(log_conn_header_t) // conn header for each pcm
    ;

  pcm_off = (uint32_t *) kzalloc(sizeof(uint32_t)*total_n_pcm, GFP_KERNEL);
  pcm_log_info = (int *) kzalloc(2*sizeof(int)*total_n_pcm, GFP_KERNEL);

  conn_it = 0;
  log_off = 0;
  list_for_each_entry(pcm_ptr, log_pcm_ptr_list, listp) {
    pcm = (profile_conn_map_t *) pcm_ptr->ptr;
    pcm_off[conn_it/2] = log_hdr_size // at least size of log header
      // size of each preceding log conn header
      + (conn_it/2*sizeof(log_conn_header_t))
      // size of all preceding variable length log buffers
      + log_off;

    if (log_type == PROF_LOG) {
      ret = count_log_entries_size(&pcm->prof_log, &pcm_log_info[conn_it],
          &pcm_log_info[conn_it+1]);
    }
    if (ret < 0)
      break;
   
    iprint(LVL_DBG, "pcm [%pI4 %u, %pI4 %u], #log entries: %d, pcm log offset: %d, size: %d"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm_log_info[conn_it], pcm_off[conn_it/2], pcm_log_info[conn_it+1]);

    log_off += pcm_log_info[conn_it+1];
    total_log_size += pcm_log_info[conn_it+1]; // size of log on each pcm
    conn_it += 2;
  }

  *log_len = total_log_size;
  *n_pcm = total_n_pcm;
  *pcm_off_p = pcm_off;
  *pcm_log_info_p = pcm_log_info;

  return 0;
}

int
prepare_one_log(list_t *log_pcm_ptr_list, char **log, int total_log_size,
    int total_n_pcm, uint32_t *pcm_off, int *pcm_log_info)
{
  int log_off = 0;
  int conn_it = 0;
  profile_conn_map_t *pcm = NULL;
  scm_pcm_ptr_t *pcm_ptr = NULL;
  char *pcm_log_buf = NULL;
  char *log_buf = NULL;
  log_header_t *lh = NULL;
  log_conn_header_t *lch = NULL;

  if (!log_pcm_ptr_list || list_empty(log_pcm_ptr_list))
    return -EINVAL;

  /*
   * generate actual log buffer
   */

  log_buf = (char *) kzalloc(total_log_size, GFP_KERNEL);
  if (!log_buf) {
    return -ENOMEM;
  }

  /*
   * log_header_t
   */
  log_off = 0;
  lh = (log_header_t *) log_buf;
  lh->total_log_size = total_log_size;
  lh->n_conn = (uint16_t) total_n_pcm;
  log_off += sizeof(log_header_t);
  for (conn_it = 0; conn_it < total_n_pcm; conn_it++) {
    ((uint32_t *) ((char *) log_buf + log_off))[conn_it] = pcm_off[conn_it];
  }
  log_off += (total_n_pcm*sizeof(uint32_t));

  pcm_log_buf = (char *) log_buf + log_off;

  conn_it = 0;

  /*
   * for each pcm add log_conn_header_t and all log_record_t entries
   */
  list_for_each_entry(pcm_ptr, log_pcm_ptr_list, listp) {
    pcm = (profile_conn_map_t *) pcm_ptr->ptr;
    lch = (log_conn_header_t *) pcm_log_buf;
    memcpy(&lch->conn_id, &pcm->sec_conn_id, sizeof(conn_t));
    lch->n_recs = pcm_log_info[conn_it];
    generate_log(&pcm->prof_log, &pcm->sec_conn_id, pcm->s->sme_type,
        (char *) pcm_log_buf + sizeof(log_conn_header_t));

    //pcm->written_proflog = 1;
    log_off += (sizeof(log_conn_header_t) + pcm_log_info[conn_it+1]);
    pcm_log_buf = (char *) log_buf + log_off;
    conn_it += 2;

    iprint(LVL_DBG, "pcm: %pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u"
        , (void *) &pcm->sec_conn_id.src_ip, pcm->sec_conn_id.src_port
        , (void *) &pcm->sec_conn_id.in_ip, pcm->sec_conn_id.in_port
        , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
        , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port);
  }

  *log = (char *) log_buf;

  return 0;
}

int
do_log_and_free(sme_log_q_t *proflog_q, pcm_q_t *pcm_q)
{
  int ret = 0;
  int htbl_idx = 0;
  list_t *htbl_list = NULL;
  profile_conn_map_t *pcm = NULL;
  SME_INIT_TIMER(pcm_sl);

#if CONFIG_PROF_LOG
  scm_pcm_ptr_t *pcm_ptr = NULL;
  int log_len = 0, n_pcm = 0;
  uint32_t *pcm_off = NULL;
  int *pcm_log_info = NULL;
  char *log_buf = NULL;
  list_t log_pcm_ptr_list;

  INIT_LIST_HEAD(&log_pcm_ptr_list);

  pcm_ptr = (scm_pcm_ptr_t *) kzalloc(sizeof(scm_pcm_ptr_t), GFP_KERNEL);
  INIT_LIST_HEAD(&pcm_ptr->listp);
  list_add_tail(&pcm_ptr->listp, &log_pcm_ptr_list);
#endif

  while (!ret) { // !ret is required for prof_log case..
    ret = dequeue_pcm_q(pcm_q, (void **) &pcm);
    if (ret < 0)
      break;

#if CONFIG_PROF_LOG
    pcm_ptr->ptr = pcm;

    calc_log_entry_size(&log_pcm_ptr_list, &log_len, &n_pcm,
        &pcm_off, &pcm_log_info, PROF_LOG);
    ret = prepare_one_log(&log_pcm_ptr_list, &log_buf, log_len, n_pcm,
        pcm_off, pcm_log_info);

    if (ret == 0)
      enqueue_sme_log_q(proflog_q, log_buf, log_len);

    if (pcm_off)
      kfree(pcm_off);
    if (pcm_log_info)
      kfree(pcm_log_info);
    if (log_buf)
      kfree(log_buf);
    pcm_off = NULL;
    pcm_log_info = NULL;
    log_buf = NULL;
#endif

    // TODO: locking before free pcm
    htbl_list = pcm->pcm_list_head;
    // this is correct
    htbl_idx = (htbl_list - &(pcm->s->pcm_htbl.pcm_list[0]));
    iprint(LVL_DBG, "pcm [%pI4 %u, %pI4 %u] idx %d\n"
        "list head %p, list[0] %p"
        , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
        , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
        , htbl_idx, htbl_list, &(pcm->s->pcm_htbl.pcm_list[0]));

    SME_START_TIMER(pcm_sl);

    spin_lock_bh(&(pcm->s->pcm_htbl.lock[htbl_idx]));

    SME_END_TIMER_ARR(pcm_sl, smp_processor_id());

    list_del_init(&pcm->pcm_listp);

    spin_unlock_bh(&(pcm->s->pcm_htbl.lock[htbl_idx]));

    free_profile_conn_map(&pcm, 0);
  }

#if CONFIG_PROF_LOG
  cleanup_scm_pcm_ptr_list(&log_pcm_ptr_list);
  INIT_LIST_HEAD(&log_pcm_ptr_list);
#endif

  return 0;
}

static int
logger_fn(void *data)
{
  int i;
  uint64_t log_tmo = 250; // us
  iprintk(LVL_EXP, "Initializing logger thread");
  while (!kthread_should_stop()) {

    i = 0;

#if CONFIG_XEN_PACER
    for (i = 0; i < RX_NCPUS; i++) {
      do_log_and_free(proflog_q, &pcm_rxq[i]);
    }

    for (i = 0; i < RX_NCPUS; i++) {
      do_log_and_free(proflog_q, &pcm_txq[i]);
    }
#endif

    usleep_range(log_tmo, log_tmo);
  }

  iprintk(LVL_EXP, "Exiting logger thread");
  return 0;
}

int
setup_logger_thread(void *data)
{
  logger = kthread_create(logger_fn, data, "sme_logger");
  wake_up_process(logger);
  return 0;
}

void
remove_logger_thread(void *data)
{
  if (logger) {
    send_sig(SIGINT, logger, 1);
    kthread_stop(logger);
    logger = NULL;
  }
}

