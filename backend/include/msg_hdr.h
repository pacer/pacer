/*
 * msg_hdr.h
 *
 * created on: Mar 22, 2017
 * author: aasthakm
 *
 * msg header format for communication between
 * dummy client and server
 */
#ifndef __MSG_HDR_H__
#define __MSG_HDR_H__

enum {
  MSG_T_IPPORT = 0,
  MSG_T_PROFILE,
  MSG_T_PMAP,
  MSG_T_PROF_ID,
  MSG_T_MARKER,
  MSG_T_DATA,
  MSG_T_PROF_REQ,
  MSG_T_PROF_RSP,
  MSG_T_PMAP_IHASH,
  MSG_T_PMAP_IHASH_HASHTABLE,
  MSG_T_SSL_HANDSHAKE,
  //Always add new formats after this comment
  MAX_NUM_MSG_TYPES
};

typedef struct {
  uint64_t cmd;
  uint64_t length;
} msg_hdr_t;

#define MSG_HDR_LEN sizeof(msg_hdr_t)

#define init_msg(buf, MSG, len_p) \
{ \
  msg_hdr_t *h = (msg_hdr_t *) buf; \
  memset(h, 0, sizeof(msg_hdr_t));  \
  h->cmd = MSG; \
  *len_p = &h->length;  \
}

#define get_msg_args_len(buf, len)  \
{ \
  msg_hdr_t *h = (msg_hdr_t *) buf; \
  len = h->length;  \
}

#define get_msg_cmd(buf, msg_type)  \
{ \
  msg_hdr_t *h = (msg_hdr_t *) buf; \
  msg_type = h->cmd;  \
}

#endif /* __MSG_HDR_H__ */
