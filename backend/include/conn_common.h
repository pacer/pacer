/*
 * conn_common.h
 *
 * created on: Sep 17, 2017
 * author: aasthakm
 * 
 * conn pair identifier datastructure
 * common between kernel and userspace
 */

#ifndef __CONN_COMMON_H__
#define __CONN_COMMON_H__

#include <linux/types.h>

typedef struct ipport {
  uint32_t src_ip;
  uint32_t dst_ip;
  uint16_t src_port;
  uint16_t dst_port;
} ipport_t;

typedef struct conn {
  uint32_t src_ip;
  /*
   * in_ip should be equal to out_ip,
   * unless there are multiple NICs
   * configured with diff. IP addresses
   */
  uint32_t in_ip;
  uint32_t out_ip;
  uint32_t dst_ip;
  uint16_t src_port;
  uint16_t in_port;
  uint16_t out_port;
  uint16_t dst_port;
} conn_t;

#endif /* __CONN_COMMON_H__ */
