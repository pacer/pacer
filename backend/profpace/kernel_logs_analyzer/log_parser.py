import log_format
from log_format_sizes import log_header_size as lhs
from log_format_sizes import log_conn_header_size as lchs
from log_format_sizes import conn_size as cs
from log_format_sizes import log_record_size as lrs
from log_format_sizes import opt_log_record_size as olrs
import socket, struct
from log_observation import log_observation
import itertools
import sys
import collections
import numpy as np

sys.path.insert(0, "../")
from config_sme_profiler import SPECIAL_REQ_ACK_BURST, SKIP_ACKS, LOG_IN_MINI_FORMAT, MULTI_TIER_PROFILING, LOG_PARSER_DBG_LVL, LVL_DBG, LVL_WARN, LVL_ERR, LVL_EXP

compare = lambda x, y: collections.Counter(x) == collections.Counter(y)

class BinaryBuffer:
    def __init__(self, buff, buff_len):
        self.buffer = buff
        self.buffer_index = 0
        self.buffer_len = buff_len

# direction of the logged packet
# 0 - source is s_addr, s_port
# 1 - source is d_addr, d_port
DIR_INCOMING = 1
DIR_OUTGOING = 0

#use the FE marker as a reference for all backend queries
USE_MARKER_REF_TIMESTAMPS = 1

# nanoseconds to ms
MULT_FACTOR = 0.000001

#CPU SPEED in GHZ
CPU_FREQ_FACTOR = 3.2
def bytes2ip(addr):
    return socket.inet_ntoa(addr)


def port2int(port):
    return struct.unpack("<H", port)[0]


def read_next(buf, bc):
    if buf.buffer_index + bc >= buf.buffer_len:
        return ""
    result = buf.buffer[buf.buffer_index : buf.buffer_index+bc]
    buf.buffer_index += bc
    return result


def uint8_t(b):
    if len(b) < 1:
        return -1
    return struct.unpack("<B", b)[0]


def uint16_t(b):
    if len(b) < 2:
        return -1
    return struct.unpack("<H", b)[0]


def uint32_t(b):
    if len(b) < 4:
        return -1
    return struct.unpack("<I", b)[0]


def uint64_t(b):
    if len(b) < 8:
        return -1
    return struct.unpack("<Q", b)[0]


def uint128_t(b):
    if len(b) < 16:
        return -1
    return struct.unpack("<16s", b)[0]


def get_int(b, size):
    if size is 1:
        return uint8_t(b)
    elif size is 2:
        return uint16_t(b)
    elif size is 4:
        return uint32_t(b)
    elif size is 8:
        return uint64_t(b)
    elif size is 16:
        return uint128_t(b)
    else:
        raise IOError("Wrong conversion of bytes!")


def read_next_int(buf, size):
    return get_int(read_next(buf, size), size)


"""
Starting point of the pipeline: takes as an input a binary buffer
reads the binary buffer, parses it and constructs a set of log records for
each connection. Each log record correxponds to a single packet.
Next, BE connections are linked to their corresponding frontend connections
in the next step of the pipeline
"""
def parse_logs(log_buff, log_buff_len): 
    connections = []
    log_buffer = BinaryBuffer(log_buff, log_buff_len)

    log_bytes_consumed = 0

    while log_bytes_consumed < log_buff_len:
        log_size = read_next_int(log_buffer, lhs.total_log_size_size)
        n_conn = read_next_int(log_buffer, lhs.n_conn_size)
        read_next(log_buffer, lhs.skip_size)  # 6 bytes

        offsets = [read_next_int(log_buffer, lhs.conn_offsets_size)
                    for _ in range(n_conn)]

        if LOG_PARSER_DBG_LVL <= LVL_DBG:
            print "log size %d, #conn %d, skip size %d" \
                % (log_size, n_conn, lhs.skip_size)
            print "offsets: " + str(offsets)

        last_query_bad_record = False

        log_header = log_format.log_header(log_size, n_conn)

        for cnn_idx in range(n_conn):
            curr_conn = log_format.log_conn(log_header)
            conn_5_tuple = log_format.conn(
                src_ip=bytes2ip(read_next(log_buffer, cs.src_ip_size)),
                in_ip=bytes2ip(read_next(log_buffer, cs.in_ip_size)),
                out_ip=bytes2ip(read_next(log_buffer, cs.out_ip_size)),
                dst_ip=bytes2ip(read_next(log_buffer, cs.dst_ip_size)),
                src_port=port2int(read_next(log_buffer, cs.src_port_size)),
                in_port=port2int(read_next(log_buffer, cs.in_port_size)),
                out_port=port2int(read_next(log_buffer, cs.out_port_size)),
                dst_port=port2int(read_next(log_buffer, cs.dst_port_size)))

            conn_n_recs = read_next_int(log_buffer, lchs.n_recs_size)
            
            curr_conn.log_conn_header = log_format.log_conn_header(conn_5_tuple, conn_n_recs)
            lch = curr_conn.log_conn_header
            
            if LOG_PARSER_DBG_LVL <= LVL_DBG:
                print "#recs: " + str(lch.n_recs)
                print lch.conn

            if (lch.conn.src_ip != lch.conn.dst_ip
                   and lch.conn.src_port != lch.conn.dst_port):
                curr_conn.is_crosstier = True
                if LOG_PARSER_DBG_LVL <= LVL_ERR:
                    if not MULTI_TIER_PROFILING and cnn_idx == 0:
                        print "[ERROR] First log entry is a crosstier connection!"
                        print "[%s %d, %s %d, %s %d, %s %d]"    \
                            % (lch.conn.src_ip, lch.conn.src_port,
                                lch.conn.in_ip, lch.conn.in_port,
                                lch.conn.out_ip, lch.conn.out_port,
                                lch.conn.dst_ip, lch.conn.dst_port)
                        print "[/ERROR]"
                if LOG_PARSER_DBG_LVL <= LVL_DBG:
                    print "I am crosstier"

            for _ in range(curr_conn.log_conn_header.n_recs):
                timestamp = read_next_int(log_buffer, lrs.timestamp_size) * MULT_FACTOR
                payload_len = read_next_int(log_buffer, lrs.payload_len_size)
                seqno = read_next_int(log_buffer, lrs.seqno_size)
                seqack = read_next_int(log_buffer, lrs.seqack_size)
                direction = read_next_int(log_buffer, lrs.direction_size)
                if LOG_IN_MINI_FORMAT == 1:
                    count = read_next_int(log_buffer, lrs.count_size)
                    #print "Log mini format, count = " + str(count)
                    if count == 0:
                        count = 1 
                else:
                    count = 1
                num_public_inputs = read_next_int(log_buffer, lrs.num_public_inputs_size)
                num_private_inputs = read_next_int(log_buffer, lrs.num_private_inputs_size)
                curr_record = log_format.log_record(timestamp, payload_len, seqno, seqack,
                        direction, num_public_inputs, num_private_inputs, count)

                if (curr_record.num_public_inputs > 0
                        or curr_record.num_private_inputs > 0):

                    req_id = read_next_int(log_buffer, olrs.req_id_size),
                    marker_timestamp = read_next_int(log_buffer,
                        olrs.marker_timestamp_size) * MULT_FACTOR
                    pub_hashes = [read_next_int(log_buffer, olrs.pub_hash_size)
                        for _ in range(curr_record.num_public_inputs)]
                    priv_hashes = [read_next_int(log_buffer, olrs.priv_hash_size)
                            for _ in range(curr_record.num_private_inputs)]
                    curr_record.opt_log_record = log_format.opt_log_record(
                        req_id, marker_timestamp, pub_hashes, priv_hashes)
                    
                    #print curr_record.opt_log_record.pub_hashes

                    # For easier and more efficient matching
                    curr_conn.conn_req_ids.append(curr_record.opt_log_record.req_id)
                    curr_conn.marker_records.append(curr_record)
                    #read_next(log_buffer, olrs.skip_size)  # 8 bytes

                    if LOG_PARSER_DBG_LVL <= LVL_DBG:
                        print curr_record.opt_log_record.__dict__
                        print curr_conn.conn_req_ids
                if LOG_IN_MINI_FORMAT == 1:
                    read_next_int(log_buffer,lrs.skip_size)

                # Have to do it after log entry has been fully consumed
                if (last_query_bad_record and curr_record.payload_len == 0
                        and not curr_record.opt_log_record):
                    if LOG_PARSER_DBG_LVL <= LVL_WARN:
                        print "[WARNING] ACK PACKET SKIPPED"
                    continue

                if last_query_bad_record and LOG_PARSER_DBG_LVL <= LVL_ERR:
                    print "[ERROR] A TCP packet of payload 5 that is not the last"  \
                        + "query was found, Payload: " + str(curr_record.payload_len)

                #HACK1
                if curr_record.payload_len == 5 and not curr_record.opt_log_record:
                    last_query_bad_record = True
                    if LOG_PARSER_DBG_LVL <= LVL_DBG:
                        print "Last Query payload 5 PACKET SKIPPED:"
                        print curr_record.__dict__

                    continue

                #HACK2
                if SKIP_ACKS and curr_record.payload_len == 0:
                    if LOG_PARSER_DBG_LVL <= LVL_DBG:
                        print "ACK  PACKET SKIPPED:"
                        print curr_record.__dict__

                    continue

                curr_conn.log_records.append(curr_record)

                if LOG_PARSER_DBG_LVL <= LVL_DBG:
                    print curr_record.__dict__
                    # print "DUMP:"
                    # print curr_conn.log_records
            # exit()

            #sort the logs by timestamp
            curr_conn.log_records.sort(key=lambda r: r.timestamp)
            
            last_query_bad_record = False
            connections.append(curr_conn)
            if LOG_PARSER_DBG_LVL <= LVL_WARN:
                print "CONNECTION PARSED: "
                print curr_conn
        # print_all()
        #if LOG_PARSER_DBG_LVL:
        #    print "ALL PARSED CONNECTIONS"
        #    print connections
        log_bytes_consumed += log_size
    #if MULTI_TIER_PROFILING:
    return connections
    #return analyse_standalone_connections(connections)


# socket.inet_ntoa(struct.pack('!L', ipint))
# struct.unpack("!L", socket.inet_aton(ipstr))[0]

def id_in_list_of_lists_finder_and_skip_index(id, list_of_lists, skip):
    """
    In the list of lists, find the index of the connection having request 
    id correlated with the request id (id) of the skip index.
    """
    if LOG_PARSER_DBG_LVL <= LVL_DBG:
        print "link key: " + str(id.encode('hex_codec'))
        lookup_list = []
        for l in list_of_lists:
            inner_list = []
            for il in l:
                inner_list.append(str(il.encode('hex_codec')))
            lookup_list.append(inner_list)

        print "lookup list: " + str(lookup_list)

        print "skip list: " + str(skip)
    for c_id in range(len(list_of_lists)):
        if c_id != skip and len(list_of_lists[c_id]) > 0 and list_of_lists[c_id][0] == id:
            #print list_of_lists[c_id][0].encode('hex_codec')
            return c_id
    return -1

def link_connections(connections):
    """
    This function links front-end and back-end connections
    based on the request ID that is part of the marker injected
    into the kernel logs (request id is parsed into the field
    conn_req_ids of each connection)
    """

    if LOG_PARSER_DBG_LVL <= LVL_DBG:
        print "==== Linking ===="
    connection_ids = [c.conn_req_ids for c in connections]
    linked_backend_connections = []
    #print connection_ids
    for c in range(len(connections)):
        # go through all front end connections and
        # search for the corresponding backend connections
        if not connections[c].is_crosstier:
            for id in connections[c].conn_req_ids:
                be_conn_idx = id_in_list_of_lists_finder_and_skip_index(id,
                    connection_ids, c)
                if(be_conn_idx == -1):
                    if LOG_PARSER_DBG_LVL <= LVL_ERR:
                        print "[WARNING]: No corresponding backend log "    \
                            + "for the request id:" + str(id.encode('hex_codec'))
                    continue

                conn = connections[be_conn_idx]
                if conn.is_crosstier:
                    connections[c].crootier_connections.append(conn)
                    linked_backend_connections.append(conn)
                    if LOG_PARSER_DBG_LVL <= LVL_DBG:
                        print "CROSSTIER_CONNECTION_FOUND"
                        print id.encode('hex_codec')
                        #print connections[connection_ids.index([id])]
                else:
                    if LOG_PARSER_DBG_LVL <= LVL_ERR:
                        print "[Warning]: Prevented mapping a frontend connection as a backend connection "    \
                            + "for the request id:" + str(id.encode('hex_codec'))


    
    #if LOG_PARSER_DBG_LVL <= LVL_DBG:
    #    print "#Crosstier Connections: "    \
    #        + str(len(connections[0].crootier_connections))

    frontend_connections = [c for c in connections if not c.is_crosstier]
    stray_connections = [item for item in connections if item not in linked_backend_connections and
                                                        item not in frontend_connections]
    if LOG_PARSER_DBG_LVL <= LVL_WARN:
        print "Strayed backend connections after linking:"
        print stray_connections
        for sc in stray_connections:
            print sc
    ready_frontend_connections = []
    for fc in frontend_connections:
        if is_connection_ready_for_analysis(fc):
            ready_frontend_connections.append(fc)
        else:
            stray_connections.append(fc)
        
    if MULTI_TIER_PROFILING:
        return (ready_frontend_connections, stray_connections)

    #technically will never be called, kept for backward compaitability 
    #where every step's output was feeded to the next step as an input
    return analyse_combined_connections(frontend_connections)

def is_connection_ready_for_analysis(connection):
    #print "IS Connection Ready?"
    #print connection
    #print connection.__dict__
    if LOG_PARSER_DBG_LVL <= LVL_WARN:
        print "IS Connection Ready?" 
        print connection.request_count == len(connection.crootier_connections)
    return connection.request_count == len(connection.crootier_connections)


def analyse_acks_in_standalone_connections(connections):
    return connections
    
def analyse_standalone_connections(connections):
    conn_cnt = 0

    if LOG_PARSER_DBG_LVL <= LVL_DBG:
        print "========== Analyzing logs =========="
    for c in connections:
        if LOG_PARSER_DBG_LVL <= LVL_DBG:
            print "NEW CONNECTION " + str(conn_cnt) \
                + ", #recs: " + str(len(c.log_records))
            myconn = c.log_conn_header.conn
            print "c: " \
                + str(myconn.src_ip) + " " + str(myconn.src_port) + " " \
                + str(myconn.in_ip) + " " + str(myconn.in_port) + " "   \
                + str(myconn.out_ip) + " " + str(myconn.out_port) + " " \
                + str(myconn.dst_ip) + " " + str(myconn.dst_port)

        conn_cnt += 1
        curr_connection_markers = c.marker_records

        list_of_seen_source_to_dest_sequences_acks = []
        list_of_seen_dest_to_source_sequences_acks = []
        req_IDs_idx = -1
        i = 0
        while i < len(c.log_records):
            # this loop reads a full request and response stream of packets
            req_IDs_idx = req_IDs_idx + 1
            curr_log_obs = log_observation()

            if len(c.conn_req_ids) == 0:
                if LOG_PARSER_DBG_LVL <= LVL_ERR:
                    print "[ERROR] Request IDs have not been correctly parsed!"
                return connections

            last_real_response_index = i
            last_real_request_index = i
            
            # != is an xor for python booleans
            # (this is to consider the outgoing packets as requests for backends)
            # We would like to capture the first request packet:
            # If we are capturing it for a FE connection, then we must see a marker
            # If it is a crosstier connection, then markers are not needed.
            if (bool(c.log_records[i].direction) is
                    (bool(DIR_INCOMING) != bool(c.is_crosstier))
                    and (c.log_records[i].opt_log_record or c.is_crosstier)):
                curr_log_obs.request_ID = c.conn_req_ids[req_IDs_idx % len(c.conn_req_ids)]
                if LOG_PARSER_DBG_LVL <= LVL_DBG:
                    # print c.__dict__
                    print c.log_records[i].__dict__
                    print "Request ID = " + str(curr_log_obs.request_ID)
                    print "First req ts = " + str(c.log_records[i].timestamp)
                curr_log_obs.first_req_timestamp = c.log_records[i].timestamp
                curr_log_obs.req_size = c.log_records[i].payload_len
                curr_log_obs.req_packet_count = c.log_records[i].count
                curr_log_obs.conn = c.log_conn_header
                #list_of_seen_source_to_dest_sequences_acks.append((c.log_records[i].seqno, c.log_records[i].seqack))

                # we got information of first incoming packet, move to next!
                i = i + 1
                while (i < len(c.log_records)
                    and bool(c.log_records[i].direction) is (
                            bool(DIR_INCOMING) != bool(c.is_crosstier))):
                    # continue if seq_ack seen before or control message
                    #list_of_seen_source_to_dest_sequences_acks.append((c.log_records[i].seqno, c.log_records[i].seqack))
                    curr_log_obs.req_size += c.log_records[i].payload_len
                    curr_log_obs.req_packet_count += c.log_records[i].count
                    i = i + 1
                curr_log_obs.last_req_timestamp = c.log_records[i - 1].timestamp
                last_real_request_index = i-1
            else:
                i = i + 1
                req_IDs_idx = req_IDs_idx - 1
                continue

            if i >= len(c.log_records):
                if LOG_PARSER_DBG_LVL <= LVL_WARN:
                    print "Ignoring current observation due to missing logs"
                    print "Ignored: "+ str(curr_log_obs)
                break

            if (bool(c.log_records[i].direction) is
                    (bool(DIR_OUTGOING) != bool(c.is_crosstier))):
                if SPECIAL_REQ_ACK_BURST:
                    while i < len(c.log_records) and c.log_records[i].payload_len == 0 and \
                            (c.log_records[i].seqack == c.log_records[last_real_request_index].seqno + c.log_records[last_real_request_index].payload_len or c.log_records[last_real_request_index].seqack == 0) :
                    #HACK3
                    #This is an ack packet, we profile for it separately 
                        if LOG_PARSER_DBG_LVL <= LVL_DBG:
                            print "FOUND SPECIAL ACK"
                        if i - last_real_request_index == 1:
                            ack_observation = log_observation()
                            curr_log_obs.ack_observation = ack_observation
                            ack_observation.first_rsp_timestamp = c.log_records[i].timestamp
                        ack_observation.last_rsp_timestamp = c.log_records[i].timestamp
                        ack_observation.rsp_size = c.log_records[i].payload_len
                        ack_observation.rsp_packet_count += 1
                        ack_observation.first_rsp_packet_latency = (
                            ack_observation.first_rsp_timestamp - curr_log_obs.last_req_timestamp) / CPU_FREQ_FACTOR

                        ack_observation.last_rsp_packet_latency = (
                            ack_observation.last_rsp_timestamp - curr_log_obs.first_rsp_timestamp) / CPU_FREQ_FACTOR
                        ack_observation.rsp_pacing = (ack_observation.last_rsp_timestamp
                             - ack_observation.first_rsp_timestamp) / max(ack_observation.rsp_packet_count-1, 1)
                        i = i+1
                if i >= len(c.log_records):
                    return connections

                curr_log_obs.first_rsp_timestamp = c.log_records[i].timestamp
                curr_log_obs.rsp_size = c.log_records[i].payload_len
                curr_log_obs.rsp_packet_count = c.log_records[i].count
                #list_of_seen_dest_to_source_sequences_acks.append((c.log_records[i].seqno, c.log_records[i].seqack))

                last_real_response_index = i
                # we got information of first outgoing packet, move to next!
                i = i + 1
                #If we get an ack during the response, we skip it (it is not a new request!)
                while (i < len(c.log_records)
                    and (bool(c.log_records[i].direction) is (
                            bool(DIR_OUTGOING) != bool(c.is_crosstier)) or 
                            ( bool(c.log_records[i].direction) is (
                            bool(DIR_INCOMING) != bool(c.is_crosstier)) and \
                            c.log_records[i].payload_len == 0 and not c.log_records[i].opt_log_record ) )):
                    #This is an incoming ACK, skip it
                    if ( bool(c.log_records[i].direction) is (
                        bool(DIR_INCOMING) != bool(c.is_crosstier)) and c.log_records[i].payload_len == 0 ):
                        i = i + 1
                        continue;

                    curr_log_obs.rsp_size += c.log_records[i].payload_len

                    if LOG_IN_MINI_FORMAT == 1:
                        curr_log_obs.rsp_packet_count = max(curr_log_obs.rsp_packet_count, c.log_records[i].count)
                    else:
                        if c.log_records[i].payload_len != 0:
                            curr_log_obs.rsp_packet_count += 1

                    #last_rsp_inter_packet_time = (c.log_records[i].timestamp - c.log_records[i - 1].timestamp)/ CPU_FREQ_FACTOR
                    #if len(curr_log_obs.rsp_inter_packet_time) == 0:
                    #    curr_log_obs.rsp_inter_packet_time.append(last_rsp_inter_packet_time)
                    #if len(curr_log_obs.rsp_inter_packet_time) > 0 and last_rsp_inter_packet_time < 5 * np.mean(curr_log_obs.rsp_inter_packet_time):
                    #    curr_log_obs.rsp_inter_packet_time.append(last_rsp_inter_packet_time)
                    if c.log_records[i].payload_len != 0 and (not LOG_IN_MINI_FORMAT or ( c.log_records[i].count > c.log_records[last_real_response_index].count)):
                        last_real_response_index = i
                    i = i + 1
                    #list_of_seen_dest_to_source_sequences_acks.append((c.log_records[i].seqno, c.log_records[i].seqack))
                curr_log_obs.last_rsp_timestamp = c.log_records[last_real_response_index].timestamp
                if LOG_PARSER_DBG_LVL <= LVL_DBG:
                    print "Current log observations interpacket spacing:"
                    for obs in curr_log_obs.rsp_inter_packet_time:
                        print obs
            curr_log_obs.first_rsp_packet_latency = (
                curr_log_obs.first_rsp_timestamp - curr_log_obs.last_req_timestamp)/ CPU_FREQ_FACTOR

            curr_log_obs.last_rsp_packet_latency = (
                curr_log_obs.last_rsp_timestamp - curr_log_obs.last_req_timestamp)/ CPU_FREQ_FACTOR

            if req_IDs_idx > 0:
                curr_log_obs.req_inter_arrival_time = max(0,
                    (curr_log_obs.first_req_timestamp - c.log_observations[
                        len(c.log_observations) - 1].last_req_timestamp)/ CPU_FREQ_FACTOR)
            else:
                curr_log_obs.req_inter_arrival_time = 0
            if LOG_PARSER_DBG_LVL < LVL_DBG:
                print "(curr_log_obs.req_packet_count, curr_log_obs.req_size):"
                print (curr_log_obs.req_packet_count, curr_log_obs.req_size)
                print "(curr_log_obs.rsp_packet_count, curr_log_obs.rsp_size):"
                print (curr_log_obs.rsp_packet_count, curr_log_obs.rsp_size)

            curr_log_obs.req_pacing = ((curr_log_obs.last_req_timestamp
                - curr_log_obs.first_req_timestamp)/ CPU_FREQ_FACTOR) / max(curr_log_obs.req_packet_count-1, 1)
            curr_log_obs.rsp_pacing = ((curr_log_obs.last_rsp_timestamp
                - curr_log_obs.first_rsp_timestamp)/ CPU_FREQ_FACTOR )/ max(curr_log_obs.rsp_packet_count-1, 1) 

            c.log_observations.append(curr_log_obs)

            if LOG_PARSER_DBG_LVL <= LVL_DBG:
                print "Current connection details: (log_observations count, connection is crosstier)"
                print (len(c.log_observations), c.is_crosstier)
                print c

            c.request_count = c.request_count + 1
            #print "Adding Private Hashes"
            #print len(curr_connection_markers)
            #print req_IDs_idx
            curr_log_obs.is_crosstier = c.is_crosstier
            if not c.is_crosstier:
                curr_log_obs.public_hashes = \
                    curr_connection_markers[req_IDs_idx % len(c.conn_req_ids)].opt_log_record.pub_hashes
                curr_log_obs.private_hashes = \
                    curr_connection_markers[req_IDs_idx % len(c.conn_req_ids)].opt_log_record.priv_hashes
                curr_log_obs.marker_delay = \
                    (curr_connection_markers[req_IDs_idx % len(c.conn_req_ids)].opt_log_record.marker_timestamp - \
                    curr_log_obs.first_req_timestamp) / CPU_FREQ_FACTOR
           #curr_log_obs.public_hashes = \
           #    curr_connection_markers[req_IDs_idx].opt_log_record.pub_hashes
           #curr_log_obs.private_hashes = \
           #    curr_connection_markers[req_IDs_idx].opt_log_record.priv_hashes
        #print list_of_seen_source_to_dest_sequences_acks
        #print list_of_seen_dest_to_source_sequences_acks


    #if MULTI_TIER_PROFILING:
    
    return connections
    
    #else:
        #since we are profiling for just a singl tier, there is no need to link connections with the backend.
        #return link_connections(connections)
    #    return [c.log_observations for c in connections] 



def analyse_combined_connections(connections):
    """
    Assuming that client retransmissions are eliminated by kernel
    MULT_FACTOR is the factor to multiply nanoseconds
    with i.e to get latencies in ms, use MULT_FACTOR of 0.000001
    This function returns analysed logs for frontend and crosstier
    requests in the form of an array of log_observatios
    Previously, analysed logs were provided in the form of an array of
    of arrays with log_observations serializeation
    (each observation in a separate array) returns
    ([FE-Client analysed logs], [FE-BE analysed logs],
        [crosstier dependent queries count]]
    """
    frontend_connections = [c for c in connections if not c.is_crosstier]
    for c in frontend_connections:
        # True for mediawiki onlys, till now
        if LOG_PARSER_DBG_LVL <= LVL_DBG:
            print "=== CROSSTIER COMPUTATION ==="
            print c.request_count
            print len(c.crootier_connections)
            print [len(c.crootier_connections[i].log_observations) for i in
                    range(len(c.crootier_connections))]

            #print c.crootier_connections[-1]

        if c.request_count != len(c.crootier_connections):
            if LOG_PARSER_DBG_LVL <= LVL_ERR:
                print "[ERROR] Number of client requests is not "   \
                    + "equal to number of backend connections!!" \
                    + str(c.request_count) + ", "   \
                    + str(len(c.crootier_connections)) + ", "   \
                    + str(c.log_conn_header)
            #print c.log_observations
        #assert(c.request_count == len(c.crootier_connections))
        fe_conn_idx = 0
        for ct_conn in c.crootier_connections:
            for lo in ct_conn.log_observations:
                c.log_observations[fe_conn_idx].backend_observations.append(lo)

                if LOG_PARSER_DBG_LVL <= LVL_DBG:
                    print "FrontEnd Connection index:" + str(fe_conn_idx)
                if USE_MARKER_REF_TIMESTAMPS:
                    lo.crosstier_req_inter_arrival_time = (lo.first_req_timestamp
                        - c.marker_records[fe_conn_idx].timestamp)
                    lo.crosstier_rsp_inter_arrival_time = (lo.first_rsp_timestamp
                        - c.marker_records[fe_conn_idx].timestamp)
                else:
                    lo.crosstier_req_inter_arrival_time = (lo.first_req_timestamp
                        - c.log_observations[fe_conn_idx].last_req_timestamp)
                    lo.crosstier_rsp_inter_arrival_time = (lo.first_rsp_timestamp
                        - c.log_observations[fe_conn_idx].last_req_timestamp)

                if LOG_PARSER_DBG_LVL <= LVL_DBG:
                    print "==== CROSS_TIER_LATENCIES ===="
                    print lo.__dict__
                    print lo.first_req_timestamp
                    print lo.first_rsp_timestamp
                    print c.log_observations[fe_conn_idx].last_req_timestamp
                    print c.marker_records[fe_conn_idx].timestamp
                    print lo.crosstier_req_inter_arrival_time
                    print lo.crosstier_rsp_inter_arrival_time

            fe_conn_idx = fe_conn_idx + 1
    #return ([lo.serialise() for lo in
    #         list(itertools.chain.from_iterable([fc.log_observations for fc in frontend_connections]))],
    #        [lo.serialise() for lo in list(itertools.chain.from_iterable(
    #            [ct.log_observations for fc in frontend_connections for ct in fc.crootier_connections ]))],
    #            #[ct.log_observations for ct in fc.crootier_connections for fc in frontend_connections ]))],
    #        list(itertools.chain.from_iterable(
    #            [[ct.request_count for ct in fc.crootier_connections] for fc in frontend_connections])),
    #        [c.log_observations for c in frontend_connections]) #, frontend_connections)
    return [c.log_observations for c in frontend_connections] #, frontend_connections)


def print_all():
    for c in connections:
        print c.__dict__
