#!/usr/bin/python
#### information about a single client request and response ####
import numpy

# REQUEST_ID=0
# FRAME_ID=1
# FIRST_IN_TIMETAMP=2
# LAST_IN_TIMETAMP=3
# IN_SIZE=4
# IN_PACKET_COUNT=5
# FIRST_OUT_TIMESTAMP=6
# LAST_OUT_TIMESTAMP=7
# OUT_SIZE=8
# OUT_PACKET_COUNT=9
# SRC_IP=10
# SRC_PORT=11
# DST_IP=12
# DST_PORT=13
# FIRST_RESPONSE_PACKET_LATENCY=14
# LAST_RESPONSE_PACKET_LATENCY=15
# INTER_ARRIVAL_TIME=16
# REQ_PACING=17
# RSP_PACING=18
# SUBSEQUENT_REQUESTS_INTER_ARR_TIME=19
# CROSSTIER_REQ_INTER_ARRIVAL_TIME=20
# CROSSTIER_RSP_INTER_ARRIVAL_TIME=21

class log_observation:
    def __init__(self):
        self.request_ID = 0
        self.first_req_timestamp = 0
        self.last_req_timestamp = 0
        self.req_size = 0
        self.req_packet_count = 0
        self.first_rsp_timestamp = 0
        self.last_rsp_timestamp = 0
        self.rsp_size = 0
        self.rsp_packet_count = 0
        self.conn = None
        self.first_rsp_packet_latency = 0
        self.last_rsp_packet_latency = 0
        self.req_inter_arrival_time = 0
        self.rsp_inter_packet_time = []
        self.req_pacing = 0
        self.rsp_pacing = 0
        self.subsequent_req_inter_arrival_time = 0
        self.crosstier_req_inter_arrival_time = 0
        self.crosstier_rsp_inter_arrival_time = 0
        self.public_hashes = []
        self.private_hashes = []
        self.is_crosstier = 0
        self.marker_delay = 0
        self.backend_observations = []
        self.ack_observation = None
        self.bursts = []


    def __str__(self):
        reqid_hex = "".join(x.encode('hex') for x in self.request_ID)
        p_str = ""
        if not self.is_crosstier:
            p_str += "conn: " + str(self.conn) + "\n"
            p_str += "reqID: " + reqid_hex + "\n"
            p_str += "ID: " + reqid_hex + "\n"
        p_str += "\treq (first TS, last TS, marker delay, size, pkt count, iat, pacing): "
        p_str += str("%f" % self.first_req_timestamp) + ", "
        p_str += str("%f" % self.last_req_timestamp) + ", "
        p_str += str("%f" % self.marker_delay) + ", "
        p_str += str(int(self.req_size)) + ", " + str(int(self.req_packet_count)) + ", "
        p_str += str(self.req_inter_arrival_time) + ", "
        p_str += str(self.req_pacing) + "\n"

        p_str += "\trsp (first TS, last TS, size, pkt count, iat[3], pacing, "
        p_str += "lat[2]): "
        p_str += str("%f" % self.first_rsp_timestamp) + ", "
        p_str += str("%f" % self.last_rsp_timestamp) + ", "
        p_str += str(int(self.rsp_size)) + ", " + str(int(self.rsp_packet_count)) + ", "
        p_str += "[" + str(self.subsequent_req_inter_arrival_time) + ", "
        p_str += str(self.crosstier_req_inter_arrival_time) + ", "
        p_str += str(self.crosstier_rsp_inter_arrival_time) + "], "
        p_str += str(self.rsp_pacing) + ", "
        p_str += "[" + str(self.first_rsp_packet_latency) + ", "
        p_str += str(self.last_rsp_packet_latency) + "]\n"

        if not self.is_crosstier:
            p_str += "\t" + str(self.public_hashes) +"\n"
            p_str += "\t" + str(self.private_hashes) +"\n"
        
        
        p_str += "\tNumber of backend queries: " + str(len(self.backend_observations)) +"\n"
        if not self.is_crosstier:
            i = 1
            p_str += "\n[==== BACKEND =====\n"
            for s in self.backend_observations:
                p_str+=  "\t\tBackend Query " + str(i) + ":\n"
                p_str += "\t\t" + str(s)
                i+=1
            p_str += " \n ==== /BACKEND ====]\n"
        

        return p_str

    def mini_str(self):
        reqid_hex = "".join(x.encode('hex') for x in self.request_ID)
        p_str = ""

        if not self.is_crosstier:
            p_str += "reqID: " + reqid_hex + ", "
            p_str += "PuH: " + str([str(self.public_hashes[i]).encode('hex') for i in
                range(len(self.public_hashes))])   +","
            p_str += "PrH: " + str([str(self.private_hashes[i]).encode('hex') for i in
                range(len(self.private_hashes))]) +","
        p_str += "req_iat: "+ str("%f" % self.req_inter_arrival_time) + ", "
        p_str += "req_size: "+ str(int(self.req_size)) + ", "
        p_str += "req_pkt_cnt: "+ str(int(self.req_packet_count)) + ", "

        p_str += "rsp_latency: "+ str("%f" % self.first_rsp_packet_latency) + ", "
        p_str += "rsp_size: "+ str(int(self.rsp_size)) + ", "
        p_str += "rsp_pkt_cnt: "+ str(int(self.rsp_packet_count)) + ", "
        if not self.is_crosstier:
            p_str += "bknd_q_count: " + str(len(self.backend_observations)) +"\n"
        i = 0
        for s in self.backend_observations:
            p_str+= "\tBackendQ "+str(i)+": "+s.mini_str()+"\n"
            i+=1
        
        return p_str


    def create_log_observation(self, request_ID, first_req_timestamp,
        last_req_timetamp, req_size, req_packet_count, first_resp_timestamp,
        last_resp_timestamp, rsp_size, rsp_packet_count, conn,
        first_response_packet_latency, last_response_packet_latency,
        req_inter_arrival_time, req_pacing, rsp_pacing,
        subsequent_req_inter_arrival_time, crosstier_req_inter_arrival_time,
        crosstier_rsp_inter_arrival_time, marker_delay = 0):
        self.request_ID = request_ID
        self.first_req_timestamp = first_req_timestamp
        self.last_req_timestamp = last_req_timetamp
        self.req_size = req_size
        self.req_packet_count = req_packet_count
        self.first_rsp_timestamp = first_resp_timestamp
        self.last_rsp_timestamp = last_resp_timestamp
        self.rsp_size = rsp_size
        self.rsp_packet_count = rsp_packet_count
        self.conn = conn
        self.first_rsp_packet_latency = first_response_packet_latency
        self.last_rsp_packet_latency = last_response_packet_latency
        self.req_inter_arrival_time = req_inter_arrival_time
        self.req_pacing = req_pacing
        self.rsp_pacing = rsp_pacing
        self.subsequent_req_inter_arrival_time = subsequent_req_inter_arrival_time
        self.crosstier_req_inter_arrival_time = crosstier_req_inter_arrival_time
        self.crosstier_rsp_inter_arrival_time = crosstier_rsp_inter_arrival_time
        self.marker_delay = marker_delay

    def serialise(self):
        return numpy.array(["0", "0", self.first_req_timestamp,
            self.last_req_timestamp, self.req_size, self.req_packet_count,
            self.first_rsp_timestamp, self.last_rsp_timestamp, self.rsp_size,
            self.rsp_packet_count, "0", "0", "0", "0",
            self.first_rsp_packet_latency, self.last_rsp_packet_latency,
            self.req_inter_arrival_time, self.req_pacing, self.rsp_pacing,
            self.subsequent_req_inter_arrival_time,
            self.crosstier_req_inter_arrival_time,
            self.crosstier_rsp_inter_arrival_time, self.marker_delay])
