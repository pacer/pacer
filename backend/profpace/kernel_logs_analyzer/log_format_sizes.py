COMPILER_ALLIGNMENT = 16

from config_sme_profiler import LOG_IN_MINI_FORMAT
class log_record_size:

    # timestamp of sending/receiving the packet

    # uint64_t
    timestamp_size = 8

    # true length of the packet payload
    # (i.e. without size padding)

    # uint32_t
    payload_len_size = 4

    # seq and ack numbers (valid only for TCP packets)

    # uint32_t
    seqno_size = 4
    # uint32_t
    seqack_size = 4

    # direction of the logged packet
    # 0 - source is s_addr, s_port
    # 1 - source is d_addr, d_port

    # uint32_t
    direction_size = 4
    
    #uint32_t
    #this entry represents "count" entries
    count_size = 4

    ## of public input hashes following the log record header

    # uint16_t
    num_public_inputs_size = 4

    ## of private input hashes following the log record header

    # uint16_t
    num_private_inputs_size = 4

    if LOG_IN_MINI_FORMAT:
        total_record_size_size = timestamp_size + payload_len_size + seqno_size + \
            seqack_size + direction_size + num_public_inputs_size + num_private_inputs_size + \
            count_size
    else:
        total_record_size_size = timestamp_size + payload_len_size + seqno_size + \
            seqack_size + direction_size + num_public_inputs_size + num_private_inputs_size
    
    skip_size = 4 #((COMPILER_ALLIGNMENT
#            - ((total_record_size_size) % COMPILER_ALLIGNMENT))
#        % COMPILER_ALLIGNMENT)


# ==========================
# the following fields may
# occur only in some records
# ==========================

class opt_log_record_size:

    # hash(TS, public inputs#, private inputs#)
    # used to link traffic across multiple connection interfaces,
    # and separate packets of one request from another

    # ihash_t 128 bits
    req_id_size = 16

    # timestamp of when the profile marker packet (containing the
    # public and private inputs hashes) reached the kernel
    # may use this to profile the latencies of the application marker

    # uint64_t
    marker_timestamp_size = 8

    # ==========================
    # list of public and private
    # input hashes follow here
    # ==========================

    pub_hash_size = 16

    priv_hash_size = 16

    skip_size = 0
    # ((COMPILER_ALLIGNMENT
    #    - ((req_id_size+marker_timestamp_size+pub_hash_size+priv_hash_size)
    #        % COMPILER_ALLIGNMENT))
    #    % COMPILER_ALLIGNMENT)



class log_conn_header_size:

    # pair of connection 5-tuple

    # conn_t
    conn_size = 24

    # number of records following the header

    # uint32_t
    n_recs_size = 4



class log_header_size:

    # total size of log from the start of this header
    # till the end of the buffers of all n_conn listed here

    # uint64_t
    total_log_size_size = 8

    # number of pcm's whose logs follow this header

    # uint16_t
    n_conn_size = 2

    skip_size = ((COMPILER_ALLIGNMENT
            - ((total_log_size_size+n_conn_size) % COMPILER_ALLIGNMENT))
        % COMPILER_ALLIGNMENT)


    # array of offsets of conn logs

    # uint32_t each
    conn_offsets_size = 4


class conn_size:

    # uint32_t
    src_ip_size = 4

    # in_ip should be equal to out_ip,
    # unless there are multiple NICs
    # configured with diff. IP addresses

    # uint32_t
    in_ip_size = 4
    # uint32_t
    out_ip_size = 4
    # uint32_t
    dst_ip_size = 4
    # uint16_t
    src_port_size = 2
    # uint16_t
    in_port_size = 2
    # uint16_t
    out_port_size = 2
    # uint16_t
    dst_port_size = 2
