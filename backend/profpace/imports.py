#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File  : ../profiling/imports.py
# Author: Mohamed Alzayat <alzayat@mpi-sws.org>
# Date  : 30.03.2018
from ctypes import *

plib = CDLL("./proflib/libproflib.so")
pyioctllib = CDLL("./proflib/pythonioctl.so")


#print_char = plib.print_char
#print_char.argtypes = [ POINTER(c_char_p)]

"""
returns:
(void *)  ptr to consumer structure
"""
alloc_log_consumer = plib.alloc_log_consumer
alloc_log_consumer.restype = c_void_p

"""
arguments format to the C function:
===================================
&(void *)   ptr to ptr to consumer structure
"""
free_log_consumer = plib.free_log_consumer
free_log_consumer.argtypes = [
    c_void_p
    ]

"""
arguments format to the C function:
===================================
(char *)    ptr to dev string
(void *)    ptr to consumer structure
"""
prepare_log_consumer = plib.prepare_log_consumer
prepare_log_consumer.argtypes = [
    c_char_p,
    c_void_p
    ]

"""
arguments format to the C function:
===================================
(void *)    ptr to consumer structure
&(char *)   ptr to buffer holding the next valid log buffer
&(int)      ptr to integer variable holding the length of the log buffer
"""
get_next_log = plib.get_next_log
get_next_log.argtypes = [
    c_void_p,
    c_void_p,
    c_void_p
    ]

"""
arguments format to the C function:
===================================
(char *)    ptr to log buffer
(uint64_t)  len of log buffer
"""
print_consumed_log = plib.print_consumed_log
print_consumed_log.argtypes = [
    c_char_p,
    c_int
    ]



#class ihash_t(Structure):
#        _fields_ = [("byte", c_char_p)]


"""
arguments format to the C function:
===================================
&(char *)   ptr for buffer
&(uint64_t)      ptr for buffer length
(char *)    ip1
(uint16_t)  port1
(char *)    ip2
(uint16_t)  port2
(char *)    ip3
(uint16_t)  port3
(char *)    ip4
(uint16_t)  port4
(uint16_t)  # profiles
(ihash *)    array of profile ids
(uint16_t **)   array of # out msges, one value in each profile
(uint16_t **)   array of array of extra slots, one array in each profile
(uint64_t **)   array of array of # frames, one array in each profile
(uint64_t **)   array of array of frame spacing, one array in each profile
(uint64_t **)   array of array of first frame latency, one array in each profile
"""
create_profile_map_ihash_hashtable = plib.create_profile_map_ihash_hashtable
create_profile_map_ihash_hashtable.argtypes = [
    c_void_p, c_void_p,
    c_char_p, c_ushort, c_char_p, c_ushort,
    c_char_p, c_ushort, c_char_p, c_ushort,
    c_ushort,    
    POINTER(c_char_p),
    POINTER(c_ushort),
    POINTER(POINTER(c_ushort)),
    POINTER(POINTER(c_ulong)),
    POINTER(POINTER(c_ulong)),
    POINTER(POINTER(c_ulong))
    ]


"""
arguments format to the C function:
===================================
&(char *)   ptr for buffer
&(uint64_t)      ptr for buffer length
(char *)    ip1
(uint16_t)  port1
(char *)    ip2
(uint16_t)  port2
(char *)    ip3
(uint16_t)  port3
(char *)    ip4
(uint16_t)  port4
(uint16_t)  # profiles
(ihash *)    array of profile ids
(uint16_t **)   array of # out msges, one value in each profile
(uint16_t **)   array of array of extra slots, one array in each profile
(uint64_t **)   array of array of # frames, one array in each profile
(uint64_t **)   array of array of frame spacing, one array in each profile
(uint64_t **)   array of array of first frame latency, one array in each profile
"""
create_profile_map_ihash = plib.create_profile_map_ihash
create_profile_map_ihash.argtypes = [
    c_void_p, c_void_p,
    c_char_p, c_ushort, c_char_p, c_ushort,
    c_char_p, c_ushort, c_char_p, c_ushort,
    c_ushort,    
    POINTER(c_char_p),
    POINTER(c_ushort),
    POINTER(POINTER(c_ushort)),
    POINTER(POINTER(c_ulong)),
    POINTER(POINTER(c_ulong)),
    POINTER(POINTER(c_ulong))
    ]


"""
arguments format to the C function:
===================================
&(char *)   ptr for buffer
&(int)      ptr for buffer length
(char *)    ip1
(uint16_t)  port1
(char *)    ip2
(uint16_t)  port2
(char *)    ip3
(uint16_t)  port3
(char *)    ip4
(uint16_t)  port4
(uint16_t)  # profiles
(uint16_t *)    array of profile ids
(uint16_t **)   array of # out msges, one value in each profile
(uint16_t **)   array of array of extra slots, one array in each profile
(uint64_t **)   array of array of # frames, one array in each profile
(uint64_t **)   array of array of frame spacing, one array in each profile
(uint64_t **)   array of array of first frame latency, one array in each profile
"""
create_profile_map = plib.create_profile_map
create_profile_map.argtypes = [
    c_void_p, c_void_p,
    c_char_p, c_ushort, c_char_p, c_ushort,
    c_char_p, c_ushort, c_char_p, c_ushort,
    c_ushort,
    POINTER(c_ushort),
    POINTER(c_ushort),
    POINTER(POINTER(c_ushort)),
    POINTER(POINTER(c_ulong)),
    POINTER(POINTER(c_ulong)),
    POINTER(POINTER(c_ulong))
    ]

"""
arguments format to the C function:
===================================
(char *)   ptr for buffer
(int)      ptr for buffer length
"""
print_profile_map_buf_ihash = plib.print_profile_map_buf_ihash
print_profile_map_buf_ihash.argtypes = [
    c_void_p,
    c_int
    ]

"""
arguments format to the C function:
===================================
(char *)   ptr for buffer
(uint64_t)      ptr for buffer length
"""
print_profile_map_buf_ihash_hashtable = plib.print_profile_map_buf_ihash_hashtable
print_profile_map_buf_ihash_hashtable.argtypes = [
    c_void_p,
    c_uint64
    ]

"""
arguments format to the C function:
===================================
(char *)   ptr for buffer
(int)      ptr for buffer length
"""
print_profile_map_buf = plib.print_profile_map_buf
print_profile_map_buf.argtypes = [
    c_void_p,
    c_int
    ]

"""
arguments format for C function:
================================
&(char *)   ptr to buffer
(int)       len of buffer
"""
free_buf = plib.free_buf
free_buf.argtypes = [
    c_void_p,
    c_int
    ]

"""
arguments format for C function:
================================
&(char *)   ptr to buffer
(uint64_t)       len of buffer
"""
free_buffer_hashtable = plib.free_buffer_hashtable
free_buffer_hashtable.argtypes = [
    c_void_p,
    c_uint64
    ]

"""
arguments format for C function:
================================
(char *)    ip addr
(int)       port
(int)       protocol (0 - UDP | 1 - TCP)
"""
create_client_socket = plib.create_client_socket
create_client_socket.argtypes = [
    c_char_p,
    c_int,
    c_int
    ]
create_client_socket.restype = c_int

"""
arguments format for C function:
================================
(int)       socket
"""
close_client_socket = plib.close_client_socket
close_client_socket.argtypes = [
    c_int
    ]

"""
arguments format for C function:
================================
(int)       socket
(char *)    IP addr of dummy server
(int)       port of dummy server
(char *)    buffer to send
(int)       length of the buffer
"""
sendto_socket = plib.sendto_socket
sendto_socket.argtypes = [
    c_int,
    c_char_p,
    c_int,
    POINTER(c_char),
    c_int
    ]

"""
arguments format for C function:
================================
(int)       socket
"""
recvfrom_socket = plib.recvfrom_socket
recvfrom_socket.argtypes = [
    c_int,
    c_char_p,
    c_int,
    POINTER(c_char),
    c_int,
    POINTER(c_int),
    POINTER(c_ushort)
    ]

"""
arguments format for C function:
================================
(char *)       devname
"""
dev_open = pyioctllib.dev_open
dev_open.argtypes = [
    c_char_p
    ]

"""
arguments format for C function:
================================
(int)       fd
"""
dev_close = pyioctllib.dev_close
dev_close.argtypes = [
    c_int
    ]

"""
arguments format for C function:
================================
(int)       fd
(int)       cmd
(char *)    buf
(int)       len
"""
dev_ioctl = pyioctllib.dev_ioctl 
dev_ioctl.argtypes = [
    c_int,
    c_int,
    c_char_p,
    c_int
    ]

