#!/usr/bin/python
#### information about a generated profile ####
import numpy

class profile:
    def __init__(self):
        self.profile_id = 0
        self.num_out_reqs = 1
        self.num_extra_slots = 0
        self.num_out_frames = []
        self.frame_spacing = []
        self.latency = []
        self.public_hashes = []
        self.private_hashes = []


    def __str__(self):
        reqid_hex = "".join(x.encode('hex') for x in self.request_ID)
        p_str = ""
        p_str += str(self.profile_id) +"\n"
        p_str += str(len(self.public_hashes)) +"\n"
        p_str += str(len(self.private_hashes)) +"\n"
        p_str += str(self.num_requests) +"\n"
        p_str += str(self.num_extra_slots) +"\n"
        p_str += str(self.num_out_frames) +"\n"
        p_str += str(self.frame_spacing) +"\n"
        p_str += str(self.latency) +"\n"
        

        return p_str

    def set_profile(self, profile_id = 0, num_out_reqs = 1, num_extra_slots = 0,
            num_out_frames = [], frame_spacing = [], frame_latency = [],
            public_hashes = [], private_hashes = []):
        self.profile_id = profile_id
        self.num_out_reqs = num_out_reqs
        self.num_extra_slots = num_extra_slots
        self.num_out_frames = num_out_frames
        self.frame_spacing = frame_spacing
        self.frame_latency = frame_latency
        self.public_hashes = public_hashes
        self.private_hashes = private_hashes


