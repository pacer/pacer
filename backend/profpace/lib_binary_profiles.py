#!/usr/bin/env python

from ctypes import *
import struct
import pprint
import math
import mysql.connector
from mysql.connector import errorcode
import itertools
import binascii
import socket
import time
from imports import (
    create_client_socket,
    close_client_socket,
    alloc_log_consumer,
    prepare_log_consumer,
    free_log_consumer,
    get_next_log,
    free_buf,
    sendto_socket,
    recvfrom_socket,
    print_consumed_log,
    create_profile_map,
    print_profile_map_buf,
    create_profile_map_ihash,
    create_profile_map_ihash_hashtable,
    print_profile_map_buf_ihash
    )

from config_sme_profiler import (
    USE_MMAP_PROFILE_DATABASE_64bit,
    SME_PROFILER_DBG_LVL,
    LVL_DBG,
    LVL_WARN,
    LVL_ERR,
    LVL_EXP
    )

MULT_FACTOR = 0.000001

# send the profile to the backend kernel through the backend profiler slave 
def send_to_backend_slave(backend_ip, backend_port,
        pmap_buf, pmap_buf_len):

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        s.connect((backend_ip, backend_port))
        s.send(str(pmap_buf_len.value)) 
        time.sleep(2)
        s.send(pmap_buf[0:pmap_buf_len.value])
        return pmap_buf_len.value 

def create_profile_map_int(pmap_arr_ptr, pmap_arr_len_ptr,
        ip1, port1, ip2, port2, ip3, port3, ip4, port4, n_prof=0,
        profile_id_arr=[0], num_out_reqs_arr=[0], num_extra_slots_arr=[0],
        num_out_frames_arr=[0], spacing_arr=[0], latency_arr=[0]):

    if SME_PROFILER_DBG_LVL <= LVL_DBG:
        print "CTYPES FORMAT"
        print "#profiles:\t" + str(n_prof)
        for i in range(0, n_prof):
            print "profile id:\t" + str(profile_id_arr[i]).encode('hex_codec')
            n_req = num_out_reqs_arr[i]
            print "#requests:\t" + str(n_req)
            extra_str = ""

            for j in range(0, n_req):
                extra_str += str(num_extra_slots_arr[i][j]) + " "
            print "#extra slots:\t" + extra_str
            frames_str = ""
            for j in range(0, n_req):
                frames_str += str(num_out_frames_arr[i][j]) + " "
            print "#frames:\t" + frames_str
            spc_str = ""
            for j in range(0, n_req):
                spc_str += str(spacing_arr[i][j]) + " "
            print "spacing:\t" + spc_str
            lat_str = ""
            for j in range(0, n_req):
                lat_str += str(latency_arr[i][j]) + " "
            print "latency:\t" + lat_str

    if USE_MMAP_PROFILE_DATABASE_64bit:
        create_profile_map_ihash_hashtable(pmap_arr_ptr, pmap_arr_len_ptr,
            ip1, port1, ip2, port2, ip3, port3, ip4, port4,
            n_prof, profile_id_arr, num_out_reqs_arr, num_extra_slots_arr,
            num_out_frames_arr, spacing_arr, latency_arr)
    else:
        create_profile_map_ihash(pmap_arr_ptr, pmap_arr_len_ptr,
            ip1, port1, ip2, port2, ip3, port3, ip4, port4,
            n_prof, profile_id_arr, num_out_reqs_arr, num_extra_slots_arr,
            num_out_frames_arr, spacing_arr, latency_arr)


    # if SME_PROFILER_DBG_LVL <= LVL_DBG:
    #     print "==== debug output pmap ===="
    #     pmap_len = cast(pmap_arr_len_ptr, POINTER(c_int)).contents
    #     pmap = cast(pmap_arr_ptr, POINTER(POINTER(c_char))).contents
    #     print_profile_map_buf_ihash(pmap[4:(pmap_len.value)], pmap_len.value-4)


def prepare_profile_map_binary(pmap_buf_ptr, pmap_buf_len_ptr,
        ip1, port1, ip2, port2, ip3, port3, ip4, port4,
        num_out_reqs, num_extra_slots, num_out_frames,
        frame_spacing, frame_latency, profile_ids = [1]):
    n_prof = len(profile_ids)

    if SME_PROFILER_DBG_LVL <= LVL_DBG:
        #print "row splits: " + str(row_splits)
        for i in range(n_prof):
            profile_id = profile_ids[i]
            print "profile_id:\t" + str(profile_id).encode('hex_codec') \
                + ",\nnum_out_reqs:\t" + str(num_out_reqs[i])  \
                + ",\nnum_extra_slot:\t" + str(num_extra_slots[i])    \
                + ",\nnum_out_frames:\t" + str(num_out_frames[i])  \
                + ",\nspacing:\t" + str(frame_spacing[i])  \
                + ",\nlatency:\t" + str(frame_latency[i])

    ## preparing UDP message for kernel
    profile_id_arr = (c_char_p * n_prof)()
    num_out_reqs_arr = (c_ushort * n_prof)()
    num_extra_slots_arr = (POINTER(c_ushort) * n_prof)()
    num_out_frames_arr = (POINTER(c_ulong) * n_prof)()
    spacing_arr = (POINTER(c_ulong) * n_prof)()
    latency_arr = (POINTER(c_ulong) * n_prof)()

    for i in range(0, n_prof):
        profile_id_arr[i] = c_char_p(str(profile_ids[i]))
        num_out_reqs_arr[i] =  int(num_out_reqs[i])
        n_req = num_out_reqs_arr[i]
        num_extra_slots_arr[i] = (c_ushort * n_req)(num_extra_slots[i])
        num_out_frames_arr[i] = (c_ulong * n_req)()
        spacing_arr[i] = (c_ulong * n_req)()
        latency_arr[i] = (c_ulong * n_req)()

        j = 0
        for n in num_out_frames[i]:
            num_out_frames_arr[i][j] = long(n)
            j += 1

        j = 0
        for n in frame_spacing[i]:
            spacing_arr[i][j] = long(math.floor(n/MULT_FACTOR))
            j += 1

        j = 0
        for n in frame_latency[i]:
            latency_arr[i][j] = long(math.floor(n/MULT_FACTOR))
            j += 1

    create_profile_map_int(pmap_buf_ptr, pmap_buf_len_ptr,
    ip1, port1, ip2, port2, ip3, port3, ip4, port4,
    n_prof, profile_id_arr,
    num_out_reqs_arr, num_extra_slots_arr, num_out_frames_arr,
    spacing_arr, latency_arr)
