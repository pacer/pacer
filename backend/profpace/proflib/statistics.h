/*
 * statistics.h
 *
 *	created on: Oct 23, 2017
 *	author: aasthakm
 *
 */

#ifndef __STATISTICS_H__
#define __STATISTICS_H__

#include <errno.h>
#include <sys/types.h>

#include "dummy_time.h"

#define CONFIG_STAT 1

typedef struct usr_stat {
	char name[64];
	double start;
	double end;
	double step;
	double sum;
	double sqr_sum;
	double min;
	double max;
	uint32_t count;
	uint32_t *dist;
} usr_stat_t;

#if CONFIG_STAT == 1
int
init_usr_stats(usr_stat_t *stat, char *name, double start, double end, double step);

void
add_point(usr_stat_t *stat, double value);

void
print_distribution(usr_stat_t *stat, FILE *fp);

void
free_usr_stats(usr_stat_t *stat);

#define DECL_STAT_EXTERN(name)	extern usr_stat_t *stat_##name
#define DECL_STAT(name) usr_stat_t *stat_##name
#define INIT_STAT(name, desc, sta, end, stp)	\
	stat_##name = malloc(sizeof(usr_stat_t));	\
	if (!stat_##name) {	\
		return -ENOMEM;	\
	}	\
	if (init_usr_stats(stat_##name, desc, sta, end, stp))	\
		return -ENOMEM

#define DEST_STAT(name)	\
	if (stat_##name != NULL) {	\
		print_distribution(stat_##name);	\
		free_usr_stats(stat_##name);	\
		free(stat_##name);	\
	}

#define PRINT_STAT(name, f)	\
	if (stat_##name != NULL) {	\
		print_distribution(stat_##name, f);	\
	}

#define ADD_TIME_POINT(name)	\
	add_point(stat_##name, SPENT_TIME(name))

#define ADD_COUNT_POINT(name, val)	\
	add_point(stat_##name, val)

#else
#define DECL_STAT_EXTERN(name)
#define DECL_STAT(name)
#define INIT_STAT(name, desc, sta, end, stp)
#define DEST_STAT(name)
#define PRINT_STAT(name, f)
#define ADD_TIME_POINT(name)
#define ADD_COUNT_POINT(name, val)
#endif
#endif /* __STATISTICS_H__ */
