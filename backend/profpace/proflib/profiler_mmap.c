#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>

#include <arpa/inet.h>

#include "config.h"
#include "../include/log_format.h"
#include "../include/config_common.h"
#include "profiler_mmap.h"

#define PAGE_SIZE 4096

void
alloc_log_consumer(sme_log_q_info_t **slq_infop)
{
  if (!slq_infop)
    return;

  sme_log_q_info_t *slq_info = NULL;
  slq_info = (sme_log_q_info_t *) malloc(sizeof(sme_log_q_info_t));
  if (!slq_info)
    return;

  memset(slq_info, 0, sizeof(sme_log_q_info_t));
  *slq_infop = slq_info;
#if DBG
  printf("alloc'ed %p\n", slq_info);
#endif
}

void
free_log_consumer(sme_log_q_info_t **slq_infop)
{
  if (!slq_infop || !(*slq_infop))
    return;

#if DBG
  printf("freeing %p\n", *slq_infop);
#endif
  sme_log_q_info_t *slq_info = *slq_infop;
  cleanup_log_consumer(slq_info);
  free(slq_info);
  *slq_infop = slq_info = NULL;
}

int
prepare_log_consumer(char *dev, sme_log_q_info_t *slq_info)
{
  int ret = 0;
	char *address = NULL;

  if (!dev || !slq_info)
    return -EINVAL;

	int fd = open(dev, O_RDWR);
#if DBG
	printf("opened dev, fd: %d\n", fd);
#endif
	if (fd < 0) {
		//perror("failed to open dev");
		ret = -1;
		goto close_dev;
	}

	int total_ring_size = PAGE_SIZE;
	address = mmap(NULL, total_ring_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
	if (address == MAP_FAILED) {
		//perror("mmap");
		ret = -1;
		goto close_dev;
	}

  slq_info->fd = fd;
  slq_info->hdr_addr = address;
  slq_info->max_qbuf = &((uint64_t *) slq_info->hdr_addr)[0];
  slq_info->gen_id = &((uint64_t *) slq_info->hdr_addr)[1];
  slq_info->prod_idx = &((uint64_t *) slq_info->hdr_addr)[2];
  slq_info->head_idx = &((uint64_t *) slq_info->hdr_addr)[3];
  slq_info->cons_idx = *(slq_info->head_idx);
  if (*(slq_info->head_idx) > *(slq_info->prod_idx))
    slq_info->cons_gen_id = *(slq_info->gen_id) - 1;
  else
    slq_info->cons_gen_id = *(slq_info->gen_id);
#if DBG
	if (address) {
		printf("addr: %p, %lu\n", address, *(uint64_t *) address);
		printf("max qbuf: %lu, prod [%lu:%lu], head %lu, cons [%lu:%lu]\n"
        , *(slq_info->max_qbuf), *(slq_info->gen_id)
        , *(slq_info->prod_idx), *(slq_info->head_idx)
        , slq_info->cons_gen_id, slq_info->cons_idx);
	}
#endif

  // for the ring buffer
	total_ring_size = (((MAX_QBUF_SIZE * *(slq_info->max_qbuf)) - 1)/PAGE_SIZE + 1)
		* PAGE_SIZE;
  address = mmap(NULL, total_ring_size, PROT_READ, MAP_PRIVATE, fd, PAGE_SIZE);
  if (address == MAP_FAILED) {
    //perror("ring buffer mmap");
    ret = -1;
    goto close_dev;
  }

  slq_info->total_ring_size = total_ring_size;
  slq_info->ring_addr = address;
#if DBG
  printf("ring addr: %p, size: %d\n", address, total_ring_size);
#endif

  return 0;

close_dev:
  if (slq_info->hdr_addr && munmap(slq_info->hdr_addr, PAGE_SIZE)) {
		//perror("munmap");
		ret = -1;
	}

  if (slq_info->ring_addr && munmap(slq_info->ring_addr,
        slq_info->total_ring_size)) {
    //perror("ring buffer munmap");
    ret = -1;
  }

	if (fd > 0) {
		close(fd);
		printf("close ret: %d\n", ret);
	}

  return ret;
}

int
cleanup_log_consumer(sme_log_q_info_t *slq_info)
{
  if (!slq_info || slq_info->fd < 0)
    return -EINVAL;

  if (slq_info->hdr_addr)
    munmap(slq_info->hdr_addr, PAGE_SIZE);

  if (slq_info->ring_addr)
    munmap(slq_info->ring_addr, slq_info->total_ring_size);

  close(slq_info->fd);
  return 0;
}

static inline void
prt_hash(char *hbuf, int hlen, char *out)
{
  if (!hbuf || !hlen || !out)
    return;

  int i;
  for (i = 0; i < hlen; i++) {
    sprintf(out + (i*2), "%02x", *(unsigned char *) (hbuf + i));
  }
}

void
print_optional_log_record(char *buf, log_record_t *rec)
{
  if (!buf || !rec)
    return;

  int j;
  ihash_t *hash_ptr = NULL;
  char hbuf[64];
  int hbuf_len = 64;
  memset(hbuf, 0, hbuf_len);
  int off = 0;
  uint64_t marker_ts = 0;

  char *req_id = buf;
  prt_hash(req_id, sizeof(ihash_t), hbuf);
  off += sizeof(ihash_t);
  marker_ts = ((uint64_t *) ((char *) buf + off))[0];
  printf("\t\t\t%s\t%lu\n", hbuf, marker_ts);

  // skip over reqID and timestamp fields
  off = sizeof(ihash_t) + sizeof(uint64_t);
  hash_ptr = (ihash_t *) ((char *) buf + off);
  for (j = 0; j < rec->num_public_inputs; j++) {
    memset(hbuf, 0, hbuf_len);
    prt_hash(hash_ptr[j].byte, sizeof(ihash_t), hbuf);
    printf("\t\t\t%s\n", hbuf);
  }

  off += (rec->num_public_inputs * sizeof(ihash_t));
  hash_ptr = (ihash_t *) ((char *) buf + off);
  for (j = 0; j < rec->num_private_inputs; j++) {
    memset(hbuf, 0, hbuf_len);
    prt_hash(hash_ptr[j].byte, sizeof(ihash_t), hbuf);
    printf("\t\t\t%s\n", hbuf);
  }

}

void
print_conn_log(char *conn_buf, int conn_buf_size, int idx)
{
  if (!conn_buf || conn_buf_size < sizeof(log_conn_header_t))
    return;

  int i;
  char *next_rec = NULL;
  char *conn_buf_end = conn_buf + conn_buf_size;
  log_record_t *rec = NULL;
  log_conn_header_t *lch = (log_conn_header_t *) conn_buf;

  struct sockaddr_in saddr, iaddr, oaddr, daddr;
  char sip[64], iip[64], oip[64], dip[64];

  memset(&saddr, 0, sizeof(struct sockaddr_in));
  memset(&iaddr, 0, sizeof(struct sockaddr_in));
  memset(&oaddr, 0, sizeof(struct sockaddr_in));
  memset(&daddr, 0, sizeof(struct sockaddr_in));
  saddr.sin_addr.s_addr = lch->conn_id.src_ip;
  iaddr.sin_addr.s_addr = lch->conn_id.in_ip;
  oaddr.sin_addr.s_addr = lch->conn_id.out_ip;
  daddr.sin_addr.s_addr = lch->conn_id.dst_ip;

  memset(sip, 0, 64);
  memset(iip, 0, 64);
  memset(oip, 0, 64);
  memset(dip, 0, 64);
  sprintf(sip, "%s", inet_ntoa(saddr.sin_addr));
  sprintf(iip, "%s", inet_ntoa(iaddr.sin_addr));
  sprintf(oip, "%s", inet_ntoa(oaddr.sin_addr));
  sprintf(dip, "%s", inet_ntoa(daddr.sin_addr));
  printf("\tc%d: (%s %u, %s %u, %s %u, %s %u) #records: %d\n"
      , idx, sip, lch->conn_id.src_port
      , iip, lch->conn_id.in_port
      , oip, lch->conn_id.out_port
      , dip, lch->conn_id.dst_port
      , lch->n_recs);

  next_rec = conn_buf + sizeof(log_conn_header_t);
  while (next_rec < conn_buf_end) {
#if DBG
    printf("next_rec: %p end: %p", next_rec, conn_buf_end);
#endif
    if (next_rec > conn_buf_end)
      return;

    rec = (log_record_t *) next_rec;
#if CONFIG_PROF_MINI_LOG
    printf("\t%lu\t%u\t%u\t%u\t%u\t%u\t%u\t%u\n"
      , rec->timestamp, rec->payload_len, rec->seqno, rec->seqack
      , rec->dir, rec->count, rec->num_public_inputs, rec->num_private_inputs);
#else
    printf("\t%lu\t%u\t%u\t%u\t%u\t%u\t%u\n"
      , rec->timestamp, rec->payload_len, rec->seqno, rec->seqack
      , rec->dir, rec->num_public_inputs, rec->num_private_inputs);
#endif
    next_rec = (char *) next_rec + sizeof(log_record_t);
    if (rec->num_public_inputs == 0 && rec->num_private_inputs == 0) {
      continue;
    }

#if DBG
    printf("\toptional data marker\n");
#endif
#if CONFIG_PROF_MINI_LOG
    print_optional_log_record(next_rec-4, rec);
#else
    print_optional_log_record(next_rec, rec);
#endif

    next_rec = (char *) next_rec + sizeof(ihash_t) + sizeof(uint64_t)
      + (rec->num_public_inputs*sizeof(ihash_t))
      + (rec->num_private_inputs*sizeof(ihash_t));
  }
}

void
print_consumed_log(char *log_buf, uint64_t log_size)
{
  int i;
  char *start_buf = NULL;
  char *ptr = NULL;
  uint32_t *conn_off_ptr = NULL;
  uint32_t conn_log_off = 0;
  int conn_log_size = 0;

  if (!log_buf || !log_size)
    return;

  if (log_size < sizeof(log_header_t))
    return;

  start_buf = log_buf;
  ptr = log_buf;
  log_header_t *lh = (log_header_t *) ptr;
  printf("log size: %lu, #conn: %d, size log hdr %lu\n"
      , lh->total_log_size, lh->n_conn, sizeof(log_header_t));

  ptr += sizeof(log_header_t);
  conn_off_ptr = (uint32_t *) ptr;

  for (i = 0; i < lh->n_conn; i++) {
    conn_log_off = conn_off_ptr[i];
    if (i < (lh->n_conn - 1)) {
      conn_log_size = conn_off_ptr[i+1] - conn_log_off;
    } else {
      conn_log_size = log_size - conn_log_off;
    }
    ptr = start_buf + conn_log_off;
    printf("conn: %d off: %d, log size: %d, buf: %p\n"
        , i, conn_off_ptr[i], conn_log_size, ptr);
    print_conn_log(ptr, conn_log_size, i);
  }
}

static int
check_cons_valid(sme_log_q_info_t *slq_info)
{
  int i;
  int cons_valid = 0;
  int qmask = *(slq_info->max_qbuf) - 1;
  int gen_diff = *(slq_info->gen_id) - slq_info->cons_gen_id;
  uint64_t cons_idx = slq_info->cons_idx;
  uint64_t prod_idx = *(slq_info->prod_idx);
  uint64_t log_size = 0;
  char *log_buf = NULL;

#if DBG
  printf("prod [%lu:%lu] head %lu, cons [%lu:%lu] gen_diff: %d\n"
      , *(slq_info->gen_id), *(slq_info->prod_idx), *(slq_info->head_idx)
      , slq_info->cons_gen_id, slq_info->cons_idx, gen_diff);
#endif
  if (gen_diff == 0) {
    if (slq_info->cons_idx == *(slq_info->prod_idx))
      return 0;
    else
      return 1;
  }

  if (gen_diff == 1) {
    if (slq_info->cons_idx >= *(slq_info->head_idx))
      return 1;
  }

  slq_info->cons_idx = *(slq_info->head_idx);
  /*
   * fast forward to the gen just preceding the current
   * generation of the producer. we have missed several
   * rounds of logs in the ring, but we cannot get them.
   */
  if (gen_diff >= 2)
    slq_info->cons_gen_id = *(slq_info->gen_id) - 1;

  return -1;

}

void
do_consume(sme_log_q_info_t *slq_info)
{
  uint64_t prod_idx, cons_idx, new_cons_idx;
  uint64_t qmask, new_cons_idx_modulo;
  int cons_valid = 0;
  int do_retry = 0;

  uint64_t log_size = 0;
  char *log_buf = NULL;
  char *my_log_buf = NULL;
  int woff = 0, wlen = 0, pending = 0;
  uint64_t new_cons_gen = 0;

  int gen_diff = *(slq_info->gen_id) - slq_info->cons_gen_id;
  qmask = *(slq_info->max_qbuf) - 1;
  cons_idx = slq_info->cons_idx;

retry:
  while (check_cons_valid(slq_info) != 1) {
    sleep(1);
  }

  while (check_cons_valid(slq_info) == 1) {
    new_cons_idx = slq_info->cons_idx;
    log_buf = slq_info->ring_addr + (new_cons_idx * MAX_QBUF_SIZE);
    log_size = ((uint64_t *) log_buf)[0];
//#if DBG
    printf("%s:%d prod [%lu:%lu] head %lu cons [%lu:%lu] log size %lu buf %p\n"
        , __func__, __LINE__, *(slq_info->gen_id), *(slq_info->prod_idx)
        , *(slq_info->head_idx), slq_info->cons_gen_id, slq_info->cons_idx
        , log_size, log_buf);
//#endif
    if (log_size == 0)
      goto exit;

    my_log_buf = (char *) malloc(log_size);
    pending = log_size;
    woff = 0;
    while (pending > 0) {
      log_buf = slq_info->ring_addr + (new_cons_idx * MAX_QBUF_SIZE);
      if (pending > MAX_QBUF_SIZE)
        wlen = MAX_QBUF_SIZE;
      else
        wlen = pending;
      memcpy(my_log_buf + woff, log_buf, wlen);
      woff += wlen;
      pending -= wlen;
      new_cons_idx = (new_cons_idx+1) & qmask;
      if (new_cons_idx == 0)
        new_cons_gen++;
    }

    if (check_cons_valid(slq_info) != 1) {
      do_retry = 1;
      free(my_log_buf);
      goto abort;
    }

    slq_info->cons_idx = new_cons_idx;
    slq_info->cons_gen_id += new_cons_gen;
    new_cons_gen = 0;
#if DBG
    // print or use the malloc buffer
    print_consumed_log(my_log_buf, log_size);
    free(my_log_buf);
    printf("%s:%d prod [%lu:%lu] head %lu cons [%lu:%lu] log size %lu buf %p\n"
        , __func__, __LINE__, *(slq_info->gen_id), *(slq_info->prod_idx)
        , *(slq_info->head_idx), slq_info->cons_gen_id, slq_info->cons_idx
        , log_size, log_buf);
#endif
  }

abort:
  if (do_retry == 1) {
    do_retry = 0;
    goto retry;
  }

exit:
  return;
}

int
get_next_log(sme_log_q_info_t *slq_info, char **log, int *log_len)
{
  uint64_t prod_idx, new_cons_idx, new_cons_gen = 0;
  uint64_t qmask;

  char *log_buf = NULL;
  uint64_t log_buf_len = 0;
  char *my_log_buf = NULL;
  int woff = 0, wlen = 0, pending = 0;

  if (!slq_info || !log || !log_len)
    return -EINVAL;

retry:
  if (check_cons_valid(slq_info) != 1)
    return -ENOENT;

  qmask = *(slq_info->max_qbuf) - 1;
  new_cons_idx = slq_info->cons_idx;
  log_buf = slq_info->ring_addr + (new_cons_idx * MAX_QBUF_SIZE);
  log_buf_len = ((uint64_t *) log_buf)[0];
#if DBG
  printf("%s:%d prod [%lu:%lu] head %lu cons [%lu:%lu] log size %lu buf %p\n"
      , __func__, __LINE__, *(slq_info->gen_id), *(slq_info->prod_idx)
      , *(slq_info->head_idx), slq_info->cons_gen_id, slq_info->cons_idx
      , log_buf_len, log_buf);
#endif
  if (log_buf_len <= 0)
    return -EINVAL;

  my_log_buf = (char *) malloc(log_buf_len);
  pending = log_buf_len;
  woff = 0;
  while (pending > 0) {
    log_buf = slq_info->ring_addr + (new_cons_idx * MAX_QBUF_SIZE);
    wlen = (pending > MAX_QBUF_SIZE) ? MAX_QBUF_SIZE : pending;
    memcpy(my_log_buf + woff, log_buf, wlen);
    woff += wlen;
    pending -= wlen;
    new_cons_idx = (new_cons_idx+1) & qmask;
    if (new_cons_idx == 0)
      new_cons_gen++;
  }

  /*
   * consumer index changed while we were copying data
   * retry with new indices.
   */
  if (check_cons_valid(slq_info) != 1) {
    free(my_log_buf);
    goto retry;
  }

  slq_info->cons_idx = new_cons_idx;
  slq_info->cons_gen_id += new_cons_gen;
  new_cons_gen = 0;
#if DBG
  print_consumed_log(my_log_buf, log_buf_len);
#endif
  *log = my_log_buf;
  *log_len = log_buf_len;

  return 0;
}
