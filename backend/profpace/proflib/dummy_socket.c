/*
 * dummy_socket.c
 *
 * created on: Mar 28, 2017
 * author: aasthakm
 *
 * dummy socket library
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sys/socket.h>
#include <arpa/inet.h>
//#include <openssl/ssl.h>
//#include <openssl/err.h>

#include <msg_hdr.h>
#include <profile_conn_format.h>
#include "profile_conn_msg.h"
#include "dummy_time.h"
#include "dummy_socket.h"
#include "config.h"

int
create_client_socket(char *ip, int port, int proto)
{
  int sock;
  struct sockaddr_in addr;

  if (proto == PROTO_UDP)
    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  else
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (sock < 0) {
    perror("Unable to create client socket");
    exit(EXIT_FAILURE);
  }

  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  inet_aton(ip, &addr.sin_addr);

  if (proto != 0) {
    if (connect(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
      perror("Unable to connect client to server");
      exit(EXIT_FAILURE);
    }
  }
//  printf("Opened %s client conn ip: %s, %u, port: %d\n",
//      (proto == 0 ? "UDP" : "TCP"), ip, addr.sin_addr.s_addr,
//      ntohs(addr.sin_port));
  printf("Opened %s client conn ip: %s, port: %d\n",
      (proto == 0 ? "UDP" : "TCP"), ip, ntohs(addr.sin_port));

  return sock;
}

int
close_client_socket(int socket)
{
  if (socket < 0)
    return -EINVAL;

  return close(socket);
}

int
create_server_tcp_socket(char *ip, int port)
{
  int s;
  struct sockaddr_in addr;

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s < 0) {
    perror("Unable to create socket");
    exit(EXIT_FAILURE);
  }

  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  //addr.sin_addr.s_addr = htonl(INADDR_ANY);
  inet_aton(ip, &addr.sin_addr);

  if (bind(s, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
    perror("Unable to bind");
    exit(EXIT_FAILURE);
  }

  if (listen(s, 1) < 0) {
    perror("Unable to listen");
    exit(EXIT_FAILURE);
  }

//  printf("Opened TCP server conn ip: %s, %u, port: %d, %d\n", ip,
//      addr.sin_addr.s_addr, addr.sin_port, ntohs(addr.sin_port));
  printf("Opened TCP server conn ip: %s, port: %d\n", ip, ntohs(addr.sin_port));
  return s;
}

int
create_server_udp_socket(char *ip, int port)
{
  int s;
  struct sockaddr_in addr;

  s = socket(AF_INET, SOCK_DGRAM, 0);
  if (s < 0) {
    perror("Unable to create socket");
    exit(EXIT_FAILURE);
  }

  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  //addr.sin_addr.s_addr = htonl(INADDR_ANY);
  inet_aton(ip, &addr.sin_addr);

  if (bind(s, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
    perror("Unable to bind");
    exit(EXIT_FAILURE);
  }

  printf("Opened UDP server conn ip: %u, %s, port: %d, %d\n",
      addr.sin_addr.s_addr, ip, addr.sin_port, ntohs(addr.sin_port));
  return s;
}

int write_to_socket(int sock, char *write_buf, int write_len)
{
  int ret;
  int w_off = 0, w_len = 0;

  w_off = 0;
  w_len = write_len;
  while (w_off < write_len) {
    ret = write(sock, write_buf+w_off, w_len);
    if (ret < 0) {
      perror("error in tcp write");
      break;
    }

    w_off += ret;
    w_len -= ret;
  }

#if DBG
  printf("TCP write %d\n", w_off);
#endif
  if (ret < 0)
    return ret;

  return w_off;
}

int
read_from_socket(int sock, char *read_buf, int read_len)
{
  int ret;
  int r_off = 0, r_len = 0;

  // =====
  // simulate receiving client's http req
  // kernel should be looking at its profile now
  r_off = 0;
  r_len = read_len;
  while (r_off < read_len) {
    ret = read(sock, read_buf+r_off, r_len);
    if (ret <= 0) {
      if (ret < 0)
        perror("error reading integer");
      break;
    }

    r_off += ret;
    r_len -= ret;
  }

#if DBG
  printf("TCP read %d\n", r_off);
#endif
  if (ret <= 0)
    return ret;

  return r_off;
}

int
sendto_socket(int sock, char *client_ip, int client_port,
    char *buf, int buf_len)
{
  int ret = 0;
  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(struct sockaddr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(client_port);
  inet_aton(client_ip, &addr.sin_addr);
  ret = sendto(sock, buf, buf_len, 0,
      (struct sockaddr *) &addr, sizeof(addr));
#if DBG
  printf("UDP sendto\t%s %d, write len %d ret %d\n", client_ip, client_port,
      buf_len, ret);
#endif
  return ret;
}

int
recvfrom_socket(int sock, char *server_ip, int server_port,
    char *read_buf, int read_len, uint32_t *other_ip, uint16_t *other_port)
{
  int ret;
  int r_off = 0, r_len = 0;

  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(addr));

  socklen_t fromlen = sizeof(addr);

  addr.sin_family = AF_INET;
  addr.sin_port = htons(server_port);
  inet_aton(server_ip, &addr.sin_addr);

  r_off = 0;
  r_len = read_len;
  while (r_off < read_len) {
    ret = recvfrom(sock, read_buf+r_off, r_len, 0,
        (struct sockaddr *) &addr, &fromlen);
    if (ret < 0) {
      perror("recvfrom error");
      break;
    }

    r_off += ret;
    r_len -= ret;
  }

#if DBG
  printf("TS: %lf, UDP recvfrom\t%s %d, len: %d\n"
      , get_time(), inet_ntoa(addr.sin_addr), ntohs(addr.sin_port), r_off);
#endif

  if (other_ip)
    *other_ip = addr.sin_addr.s_addr;
  if (other_port)
    *other_port = ntohs(addr.sin_port);
  if (ret < 0)
    return ret;

  return r_off;
}

