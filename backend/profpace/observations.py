#!/usr/bin/python

#### information about a single client request and response ####


class observation:
    url = ""
    reqid = 0
    rspid = 0
    status = 0
    rlen = 0
    timestamp = 0.0
    rtime = 0.0
    text = ""

    def __init__(self,  url, reqid, rspid, status, rlen, timestamp, rtime, text=""):
        self.url = url
        self.reqid = reqid
        self.rspid = rspid
        self.status = status
        self.rlen = rlen
        self.timestamp = timestamp
        self.rtime = rtime
        self.text = text

    def set_time(self, timestamp, rtime):
        self.timestamp = timestamp
        self.rtime = rtime

    def set_len(self, size):
        self.rlen = size
