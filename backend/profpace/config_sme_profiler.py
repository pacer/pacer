#!/usr/bin/env python

LVL_DBG = 0
LVL_WARN = 1
LVL_ERR = 2
LVL_EXP = 3


# dbg level for sme_profiler.py and lib_binary_profiles.py
SME_PROFILER_DBG_LVL = LVL_EXP

# dbg level for kernel_logs_analyzer/log_parser.py
LOG_PARSER_DBG_LVL = LVL_EXP

# dbg level for prepare_profile.py
PREP_PROFILE_DBG_LVL = LVL_EXP

# print stats on how long parsing and profile preparation takes
SME_PROFILER_STATS = 0

# should profile messages sent to kernel be stored?
# should be generally 1 (at least for debugging or getting to
# know what profiles were used for the experiment)
SME_PROFILER_STORE_KERNEL_MESSAGES = 1
PREPARE_PROFILE_STORE_GENERATED_PROFILES_ASCII = 1

#Change directories format to keep logs with timestamps
FINAL_EXPERIMENTS = 1

#Should be set to 0 if a single tier workload will be run
MULTI_TIER_PROFILING = 0

#Collect Data for binning or clustering?
COLLECT_LOG_RECORDS = 1

#Set to one if ACKS should be skipped (not considered in profiling )
SKIP_ACKS = 0

#Have a special burst for the ACK of the request
SPECIAL_REQ_ACK_BURST = 1

#set to 1 if profile database should be shared using an mmap
USE_MMAP_PROFILE_DATABASE_64bit = 0

# set to 1 if profile database should be shared using ioctl
# mutually exclusive with USE_MMAP_PROFILE_DATABASE_64bit
USE_IOCTL_PROFILE_DATABASE_64bit = 1

#The log is generated with the kernel option CONFIG_PROF_MINI_LOG set to 1
LOG_IN_MINI_FORMAT = 1
