/*
 * statq.h
 *
 * created on: Jan 20, 2018
 * author: aasthakm
 *
 * queue to hold timeseries data to be printed at rmmod
 */

#ifndef __STATQ_H__
#define __STATQ_H__

#include <linux/types.h>
#include <linux/atomic.h>
#include <linux/string.h>
#include <linux/vmalloc.h>

#include "sme_config.h"
#include "generic_q_inst.h"

typedef struct generic_q {
  uint64_t prod_idx;
  uint64_t cons_idx;
  int qidx;
  int size;
  int elem_size;
  uint16_t port;
  uint16_t has_negative;
  void *elem_arr;
  void (*printfn)(struct generic_q *queue);
  spinlock_t lock;
} generic_q_t;

#define init_generic_q(Q, i, s, t, f)                 \
{                                                     \
  (Q).elem_arr = vzalloc(s * sizeof(t));              \
  (Q).qidx = i;                                       \
  (Q).size = s;                                       \
  (Q).elem_size = sizeof(t);                          \
  (Q).prod_idx = 0;                                   \
  (Q).cons_idx = 0;                                   \
  (Q).printfn = f;                                    \
  spin_lock_init(&(Q).lock);                          \
}

#define cleanup_generic_q(Q)  \
{                             \
  if ((Q).elem_arr) {         \
    vfree((Q).elem_arr);      \
    (Q).elem_arr = NULL;      \
    (Q).elem_size = 0;        \
    (Q).size = 0;             \
    (Q).prod_idx = 0;         \
    (Q).cons_idx = 0;         \
  }                           \
}

#define print_generic_q(Q)  \
{                           \
  if ((Q).printfn) {        \
    (Q).printfn(&(Q));      \
  }                         \
}

static inline int generic_q_empty(generic_q_t *queue)
{
  return (queue->cons_idx == queue->prod_idx);
}

static inline int generic_q_full(generic_q_t *queue)
{
  return ((queue->prod_idx % queue->size == queue->cons_idx % queue->size)
      && (queue->prod_idx / queue->size != queue->cons_idx / queue->size));
}

static inline void put_generic_q(generic_q_t *queue, void *elem)
{
  void *e = NULL;
  int ret = 0;
  uint64_t curr_prod_idx = 0;
  unsigned long flag = 0;
  spin_lock_irqsave(&queue->lock, flag);
  if (!generic_q_full(queue)) {
    do {
      curr_prod_idx = queue->prod_idx;
      ret = cmpxchg(&queue->prod_idx, curr_prod_idx, curr_prod_idx+1);
    } while (ret != curr_prod_idx);

    curr_prod_idx = curr_prod_idx % queue->size;
    e = (void *) ((unsigned long) queue->elem_arr
        + (curr_prod_idx * queue->elem_size));
    memcpy(e, elem, queue->elem_size);
  }
  spin_unlock_irqrestore(&queue->lock, flag);
}

#if 0
#define put_generic_q(Q, E, t)                                                  \
{                                                                               \
  t *e = NULL;                                                                  \
  int ret = 0;                                                                  \
  int curr_prod_idx = 0;                                                        \
  if (!generic_q_full(&(Q))) {                                                  \
    do {                                                                        \
      curr_prod_idx = (Q).prod_idx;                                             \
      ret = cmpxchg(&((Q).prod_idx), curr_prod_idx, curr_prod_idx+1);           \
    } while (ret != curr_prod_idx);                                             \
                                                                                \
    curr_prod_idx = curr_prod_idx % (Q).size;                                   \
    e = (t *) ((unsigned long) (Q).elem_arr + (curr_prod_idx * (Q).elem_size)); \
    memcpy((void *) e, (void *) &(E), (Q).elem_size);                           \
  }                                                                             \
}
#endif

// element in dbg_statq
typedef struct statq_elem {
  uint64_t start_ts;
  int pid;
  uint32_t shared_seq;
  uint32_t write_seq;
  uint32_t snd_nxt;
  uint32_t snd_una;
  int packets_out;
  int new_packets_out;
  int old_packets_out;
  uint32_t rcv_nxt;
  uint32_t copied_seq;
  uint32_t snd_cwnd;
//  uint32_t snd_wnd;
//  uint32_t rcv_wnd;
//  uint32_t seqno;
//  uint32_t seqack;
  uint16_t skb_len;
  uint16_t padding;
  uint16_t urg_ptr;
  uint8_t gso:1,
          owner:1,
          coreid:6;
//  uint32_t sndbuf;
//  uint32_t rcvbuf;
  uint8_t caller;
} statq_elem_t;

// specific instance of generic_q
extern generic_q_t dbg_statq[MAX_STATQ_ARRAYS];

// print function for dbg_statq
void do_print_statq(generic_q_t *queue);

extern generic_q_t *largef_q[MAX_STATQ_ARRAYS];
extern void do_print_largef_q(generic_q_t *queue);

#endif /* __STATQ_H__  */
