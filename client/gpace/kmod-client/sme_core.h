/*
 * sme_core.h
 *
 * created on: Jan 30, 2017
 * author: aasthakm
 *
 * interception interfaces added
 * into the core network driver
 */

#ifndef __SME_CORE_H__
#define __SME_CORE_H__

#include <linux/net.h>
#include <linux/netdevice.h>
#include <linux/tcp.h>
#include <net/tcp.h>
#include "sme_skb.h"
#include "../../include/conn_common.h"
#include "sme_debug.h"
#include "sme_statistics.h"

#include "generic_q.h"
#include "sme_time.h"
#include "sme_stats_decl.h"

#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"
#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x_cmn.h"
#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/sme/xen_sme_hooks.h"


/*
 * ================
 * helper functions
 * ================
 */

int enqueue_skb_for_dma(struct sk_buff *skb, int skb_type, int qtype,
    partial_sk_buff_t *pskb, conn_t *conn);

/*
 * ==========================
 * module interface functions
 * ==========================
 */

#if 0
enum {
  C_TCP_ACK = 0,
  C_TCP_ENTER_LOSS,
  C_TCP_UNDO_CWND_REDUCTION,
  C_TCP_END_CWND_REDUCTION,
  C_TCP_CWND_RESTART,
  C_TCP_CWND_APP_LIMITED,
};
#endif

#if SME_CONFIG != SME_DBG_LOG && CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB && CONFIG_PACER_TCP
void mod_update_cwnd(struct sk_buff *skb, struct sock *sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int num_marked_lost,
    char *extra_dbg_string);
#elif CONFIG_PACER_TCP
static inline void mod_update_cwnd(struct sk_buff *skb, struct sock *sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int num_marked_lost,
    char *extra_dbg_string)
{
  struct inet_sock *inet = inet_sk(sk);
  struct tcp_sock *tcp = tcp_sk(sk);
  struct inet_connection_sock *inet_c_sock = inet_csk(sk);
  int b_inflight = (b_pkts + b_retrans - (b_lost + b_sack));
  int a_inflight = (a_pkts + a_retrans - (a_lost + a_sack));

  uint32_t s_addr, d_addr;
  uint16_t s_port, d_port;
  char dbg_buff[256];

  if (!sk)
    return;

  d_addr = inet->inet_daddr;
  s_addr = inet->inet_saddr;
  d_port = ntohs(inet->inet_dport);
  s_port = ntohs(inet->inet_sport);

  if (ntohs(inet->inet_sport) != 443 && ntohs(inet->inet_sport) != 80)
    return;

  memset(dbg_buff, 0, 256);
  if (!extra_dbg_string)
    extra_dbg_string = dbg_buff;

#if 1
  {
    largef_elem_t e;
    int dbg_idx = 0;
    memset((void *) &e, 0, sizeof(largef_elem_t));
    e.ts = get_current_time(SCALE_NS);
    e.snd_nxt = tcp->snd_nxt;
    e.snd_una = tcp->snd_una;
    e.rcv_nxt = tcp->rcv_nxt;
    e.packets_out = tcp->packets_out;
    e.lost_out = tcp->lost_out;
    e.sacked_out = tcp->sacked_out;
    e.retrans_out = tcp->retrans_out;
    e.cwnd = tcp->snd_cwnd;
    e.snd_wnd = tcp->snd_wnd;
    e.rcv_wnd = tcp->rcv_wnd;
    e.icsk_rto = inet_csk(sk)->icsk_rto;
#if 0
    e.cwm = prof_p->cwnd_watermark;
    e.tail = prof_p->tail;
    e.real = atomic_read(&prof_p->real_counter);
    e.dummy = prof_p->dummy_counter;
    e.num_timers = prof_p->num_timers;
    e.nxt = prof_p->next_timer_idx;
    e.pidx = prof_p->profq_idx;
#endif
    e.ca_state = inet_csk(sk)->icsk_ca_state;
    e.caller = caller;
    e.coreid = smp_processor_id();
    e.owner = sk->sk_lock.owned;
    dbg_idx = d_port % STATQ_MODULO;
    if (generic_q_empty(largef_q[dbg_idx]))
      largef_q[dbg_idx]->port = d_port;
    put_generic_q(largef_q[dbg_idx], (void *) &e);
  }
#endif

  if (/* inet_csk(sk)->icsk_ca_state > TCP_CA_Open || */
      (int) tcp->packets_out < 0 || (int) tcp->lost_out < 0
      || (int) tcp->retrans_out < 0 || (int) tcp->sacked_out < 0) {
  iprint(LVL_INFO,
    "%pS %pS %pS %pS\n"
    "PORT %u pkts %d => %d lost %d => %d retrans %d => %d sack_out %d => %d "
    "cwnd %d => %d inflight %d => %d delivered %d acks %d flag 0x%0x\n"
    "snd_nxt %u snd_una %u write_seq %u rcv_nxt %u timeout %lu currtime %lu RTO %u\n"
    "[TCP_SOCK] snd_ssthresh %u prior_ssthresh %u prior_cwnd %u snd_wnd %u "
    "cwnd_limited %d app_limited %u sstart %d\n"
    "sk_state %d CA %d pending %u high_seq %u undo una %u "
    "snd_cwnd_used %u packets_out %u max_packets_out %u snd_cwnd_stamp %u\n"
    "%s %u undo retrans %u retrans stamp %u saw tstamp %u rcv_tsecr %u"
    , __builtin_return_address(2), __builtin_return_address(3)
    , __builtin_return_address(4), __builtin_return_address(5)
    , d_port, b_pkts, a_pkts, b_lost, a_lost, b_retrans, a_retrans, b_sack, a_sack
    , b_cwnd, a_cwnd, b_inflight, a_inflight, delivered, una_diff, ack_flag
    , tcp->snd_nxt, tcp->snd_una, tcp->write_seq, tcp->rcv_nxt
    , inet_csk(sk)->icsk_timeout, jiffies, inet_csk(sk)->icsk_rto
    , tcp->snd_ssthresh
    , tcp->prior_ssthresh
    , tcp->prior_cwnd
    , tcp->snd_wnd
    , tcp->is_cwnd_limited
    , tcp->app_limited
    , tcp_in_slow_start(tcp)
    , sk->sk_state
    , inet_c_sock->icsk_ca_state
    , inet_c_sock->icsk_pending
    , tcp->high_seq, tcp->undo_marker
    , tcp->snd_cwnd_used
    , tcp->packets_out
    , tcp->max_packets_out
    , tcp->snd_cwnd_stamp
    , extra_dbg_string
    , tcp->undo_marker
    , tcp->undo_retrans
    , tcp->retrans_stamp
    , tcp->rx_opt.saw_tstamp
    , tcp->rx_opt.rcv_tsecr
    );
  }

  if (caller == C_TCP_ENTER_LOSS) {
    SME_ADD_COUNT_POINT_ARR(loss_cwnd_update, 1, smp_processor_id());
  }
  return;
}
#else
static inline void mod_update_cwnd(struct sk_buff *skb, struct sock *sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int num_marked_lost,
    char *extra_dbg_string) { return; }
#endif 
int mod_intercept_select_queue(struct net_device *dev, struct sk_buff *skb,
    char *extra_dbg_string);
void mod_intercept_tx_sent_queue(struct netdev_queue *txq, unsigned int bytes);
void mod_intercept_tx_completed_queue(struct netdev_queue *txq,
    unsigned int pkts, unsigned int bytes);
int mod_intercept_sk_buff(struct sk_buff *skb, char *extra_dbg_string);
void mod_intercept_xmit_stop_queue(struct net_device *dev, struct netdev_queue *txq,
    struct bnx2x_fp_txdata *txdata);
int mod_intercept_tx_int(struct net_device *dev, struct bnx2x_fp_txdata *txdata);
void mod_intercept_free_tx_skbs_queue(struct bnx2x_fastpath *fp);
void mod_parse_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod,
    uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons,
    char *data, int data_len, char *extra_dbg_string);
void mod_print_sk_buff(struct sk_buff *skb, struct sock *sk, char *extra_dbg_string);
void mod_intercept_rx_path(void *dev, int fp_idx, uint16_t rx_bd_prod,
    uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons, int irq,
    char *extra_dbg_string);
void mod_print_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod,
    uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons,
    char *extra_dbg_string);

#endif /* __SME_CORE_H__ */
