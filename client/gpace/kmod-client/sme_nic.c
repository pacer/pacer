#include "sme_config.h"
#include "sme_debug.h"
#include "sme_nic.h"
#include "sme_time.h"
#include "profile_map.h"

#include <xen/page.h>

void
init_nic_txdata(nic_txdata_t *nic_tx_elem, uint64_t tsc, uint64_t maddr)
{
  if (!nic_tx_elem)
    return;

  nic_tx_elem->tx_tsc = tsc;
  nic_tx_elem->skb_maddr = maddr;
}

int
init_global_nicq_hdr(global_nicq_hdr_t *global_queue, int qsize)
{
  int i, j;
  int n_elem_per_page = 0;
  int nicq_size = 0;
  int n_pages = 0;
  int page_it = 0;
  int page_off = 0;
  int ptr_it = 0;
  void *page_start = NULL;

  if (!global_queue || !qsize)
    return -ENOMEM;

  n_elem_per_page = PAGE_SIZE/sizeof(nic_txdata_t);
  n_pages = (qsize - 1)/n_elem_per_page + 1;
  nicq_size = n_pages * PAGE_SIZE;
  iprint(LVL_EXP, "sz nic_txdata %lu #elems/page %d #pages %d tot size %d"
      , sizeof(nic_txdata_t), n_elem_per_page, n_pages, nicq_size
      );

//  global_queue->queue = (nic_txdata_t *) kzalloc(nicq_size, GFP_KERNEL);
  global_queue->queue = (nic_txdata_t *) kcalloc(n_pages, PAGE_SIZE, GFP_KERNEL);
  if (!global_queue->queue)
    return -ENOMEM;

  global_queue->qsize = qsize;

  global_queue->n_md = generate_mdesc_palign(global_queue->queue,
      sizeof(nic_txdata_t), qsize, global_queue->md_arr, MAX_NICQ_FRAGS);

  if (global_queue->n_md < 0) {
    kfree(global_queue->queue);
    return global_queue->n_md;
  }

  global_queue->queue_p =
    (nic_txdata_t **) kzalloc(sizeof(nic_txdata_t *) * qsize, GFP_KERNEL);
  if (!global_queue->queue_p) {
    kfree(global_queue->queue);
    return -ENOMEM;
  }

  iprint(LVL_EXP, "global queue %p .queue %p .queue_p %p"
      , global_queue, global_queue->queue, global_queue->queue_p);

  ptr_it = 0;
  for (i = 0; i < global_queue->n_md; i++) {
    page_start =
      (void *) __va((machine_to_phys(XMADDR(global_queue->md_arr[i].maddr))).paddr);
    page_it = 0;
    for (j = 0; j < global_queue->md_arr[i].n_elems; j++) {
      page_off = get_velem_size_off(j, sizeof(nic_txdata_t));
      global_queue->queue_p[ptr_it] = (nic_txdata_t *) (page_start + page_off);
      ptr_it++;
    }
  }

  return nicq_size;
}

void
cleanup_global_nicq_hdr(global_nicq_hdr_t *global_queue)
{
  if (!global_queue)
    return;

  if (global_queue->queue_p)
    kfree(global_queue->queue_p);

  if (global_queue->queue)
    kfree(global_queue->queue);
}

// =========
#define NIC_REF_MASK    (1 << 30)
#define NIC_REF_UNMASK  ~(1 << 30)

static int
is_marked_reference(int idx)
{
  return (int) (idx & NIC_REF_MASK);
}

static int
get_unmarked_reference(int idx)
{
  return (idx & NIC_REF_UNMASK);
}

static int
get_marked_reference(int idx)
{
  return (idx | NIC_REF_MASK);
}


int
nicq_search(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_nicq,
    uint64_t tsc_key, void *pcm_p)
{
  int idx = 0, idx_next = 0, idx_next_um, idx_last_unmarked, idx_last_unmarked_next;
  int idx_tail = 0;
  int ret = 0;
  nic_txdata_t *t_elem;

  profile_conn_map_t *pcm = (profile_conn_map_t *) pcm_p;

  do {
    idx = atomic_read(&nicq_hdr->head);
    // head has already been removed from the pcm
    if (idx < 0 || idx >= GLOBAL_NIC_DATA_QSIZE) {
      iprint(LVL_EXP, "%pS PORT %u state %d G%lld\n"
          "head %d tail %d idx %d idx_nxt %d idx_tail %d"
          , __builtin_return_address(0), pcm->sec_conn_id.dst_port
          , pcm->state, pcm->profq_idx
          , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail)
          , idx, idx_next, idx_tail
          );
      return -1;
    }

    idx_next = atomic_read(&global_nicq->queue_p[idx]->next);
    // concurrently, head->next was changed before freeing pcm
    if (idx_next < 0 || idx_next >= GLOBAL_NIC_DATA_QSIZE) {
      iprint(LVL_EXP, "%pS PORT %u state %d G%lld\n"
          "head %d tail %d idx %d idx_nxt %d %d idx_tail %d"
          , __builtin_return_address(0), pcm->sec_conn_id.dst_port
          , pcm->state, pcm->profq_idx
          , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail)
          , idx, idx_next, get_unmarked_reference(idx_next), idx_tail
          );

      // XXX: the if condition was added because of an observed bug (unsovled)
      if (get_unmarked_reference(idx_next) < 0
          || get_unmarked_reference(idx_next) >= GLOBAL_NIC_DATA_QSIZE)
        return -1;
    }

    idx_tail = atomic_read(&nicq_hdr->tail);
    // tail has already been removed from the pcm
    if (idx_tail < 0 || idx_tail >= GLOBAL_NIC_DATA_QSIZE) {
      iprint(LVL_EXP, "%pS PORT %u state %d G%lld\n"
          "head %d tail %d idx %d idx_nxt %d idx_tail %d"
          , __builtin_return_address(0), pcm->sec_conn_id.dst_port
          , pcm->state, pcm->profq_idx
          , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail)
          , idx, idx_next, idx_tail
          );
      return -1;
    }

    idx_last_unmarked = idx;
    idx_last_unmarked_next = idx_next;

    // XXX: added masking for unmarked reference because of an observed bug (unsolved)
    t_elem = global_nicq->queue_p[get_unmarked_reference(idx_next)];

    while (((idx_next_um = get_unmarked_reference(idx_next)) !=
        atomic_read(&nicq_hdr->tail))
        && (is_marked_reference(atomic_read(&global_nicq->queue_p[idx_next_um]->next))
          || t_elem->tx_tsc < tsc_key)) {

      if (!is_marked_reference(idx_next)) {
        idx_last_unmarked = idx;
        idx_last_unmarked_next = idx_next;
      }

      idx = idx_next_um;
      idx_next = get_unmarked_reference(atomic_read(&global_nicq->queue_p[idx]->next));
      t_elem = global_nicq->queue_p[get_unmarked_reference(idx_next)];

    }

    if (!is_marked_reference(idx_next))
      return idx;

    ret = atomic_cmpxchg(&global_nicq->queue_p[idx_last_unmarked]->next,
        idx_last_unmarked_next, idx_next_um);
    if (ret == idx_last_unmarked_next)
      return idx_last_unmarked;

  } while (1);

}

int
nicq_insert(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_nicq, int ins_idx,
    void *pcm_p)
{
  int idx, idx_next;
  int head_idx = 0;
  int tail_idx = 0;
  int ret = 0;
  nic_txdata_t *r_elem = NULL;

  nic_txdata_t *ins_elem = global_nicq->queue_p[ins_idx];

  head_idx = atomic_read(&nicq_hdr->head);
  tail_idx = atomic_read(&nicq_hdr->tail);
  if (head_idx < 0 || tail_idx < 0)
    return -ENOENT;

  do {
    idx = nicq_search(nicq_hdr, global_nicq, ins_elem->tx_tsc, pcm_p);
    if (idx < 0 || idx >= GLOBAL_NIC_DATA_QSIZE) {
      iprint(LVL_EXP, "head %d tail %d idx %d"
          , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail), idx
          );
      return -1;
    }

    idx_next = get_unmarked_reference(atomic_read(&global_nicq->queue_p[idx]->next));
    if (idx_next < 0 || idx_next >= GLOBAL_NIC_DATA_QSIZE) {
      iprint(LVL_EXP, "head %d tail %d idx %d idx_nxt %d idx.nxt %d"
          , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail)
          , idx, idx_next, atomic_read(&global_nicq->queue_p[idx]->next)
          );
      return -1;
    }

    // get key of right element
    r_elem = global_nicq->queue_p[idx_next];

    // new key already in queue
    if (idx_next != atomic_read(&nicq_hdr->tail) &&
        r_elem->tx_tsc == ins_elem->tx_tsc) {
      iprint(LVL_INFO, "idx %d idx_nxt %d ins_idx %d ins tsc %llu r tsc %llu"
          , idx, idx_next, ins_idx, ins_elem->tx_tsc, r_elem->tx_tsc);
//      return -1;
      BUG();
    }

    // check if the key of the current successor node >= new key
    // (concurrent insertions)
    if (ins_elem->tx_tsc < r_elem->tx_tsc
        || idx_next == atomic_read(&nicq_hdr->tail)) {
      atomic_set(&global_nicq->queue_p[ins_idx]->next, idx_next);
      // swing idx.next to point to new node
      ret = atomic_cmpxchg(&global_nicq->queue_p[idx]->next, idx_next, ins_idx);
      if (ret == idx_next) // success
        return 1;
    }

    // else retry
  } while (1);
}

int
nicq_remove(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_nicq,
    uint64_t tsc_key, void *pcm_p)
{
  int idx, idx_next, idx_nextnext;
  int ret = 0;
  nic_txdata_t *r_elem;
  profile_conn_map_t *pcm = (profile_conn_map_t *) pcm_p;

  if (!nicq_hdr)
    return -1;

  do {
    // find index of predecessor of node with smallest key >= argument key,
    // or predecessor of tail if no such node exists
    idx = nicq_search(nicq_hdr, global_nicq, tsc_key, pcm_p);
    if (idx < 0)
      return idx;

    idx_next = get_unmarked_reference(atomic_read(&global_nicq->queue_p[idx]->next));

    // element not found, nothing to remove
    if (idx_next == atomic_read(&nicq_hdr->tail))
      return -1;

    // try to automatically mark the node at idx_next. it is not necessarily the
    // node with the smallest key >= argument key under concurrent insertions.
    idx_nextnext = get_unmarked_reference(
        atomic_read(&global_nicq->queue_p[idx_next]->next));
    ret = atomic_cmpxchg(&global_nicq->queue_p[idx_next]->next, idx_nextnext,
        get_marked_reference(idx_nextnext));
    if (ret == idx_nextnext) {
      if (idx_next == atomic_read(&nicq_hdr->head)) {
        iprint(LVL_EXP, "PORT %u state %d G%lld\n"
            "head %d tail %d idx %d idx_nxt %d idx_nxtnxt %d idx.nxt %d idx_nxt.nxt %d"
            , pcm->sec_conn_id.dst_port, pcm->state, pcm->profq_idx
            , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail)
            , idx, idx_next, idx_nextnext
            , atomic_read(&global_nicq->queue_p[idx]->next)
            , atomic_read(&global_nicq->queue_p[idx_next]->next)
            );
      }
      break; // success
    }

    // another thread marked the node or changed its successor, retry
  } while (1);

  // node at idx_next is now marked, at this point we are committed to removing
  // the node automatically.
  // try changing queue[idx].next to point to idx_nextnext
  ret = atomic_cmpxchg(&global_nicq->queue_p[idx]->next, idx_next, idx_nextnext);
  if (ret == idx_next) {
    return idx_next; // success
  }

  // a concurrent thread has changed or marked the pointer pointing to idx_next.
  // could be caused by one or more insertions before the marked node, or because
  // the previous node was marked. do the search again to clean up marked node(s).
  r_elem = global_nicq->queue_p[idx];
  idx = nicq_search(nicq_hdr, global_nicq, r_elem->tx_tsc, pcm_p);

  return idx_next;
}

int
init_nicq_hdr(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_queue,
    nic_freelist_t *nfl)
{
  nic_fl_elem_t nfl_head, nfl_tail;
  int ret = 0;

  if (!nicq_hdr || !global_queue || !nfl)
    return -EINVAL;

  nicq_hdr->qsize = 0;
  ret = get_one_nic_freelist(nfl, &nfl_head);
  if (ret < 0) {
    atomic_set(&nicq_hdr->head, -1);
    atomic_set(&nicq_hdr->tail, -1);
    return ret;
  }

  ret = get_one_nic_freelist(nfl, &nfl_tail);
  if (ret < 0) {
    put_one_nic_freelist(nfl, &nfl_head);
    atomic_set(&nicq_hdr->head, -1);
    atomic_set(&nicq_hdr->tail, -1);
    return ret;
  }

  if (nfl_head.offset < 0 || nfl_head.offset >= GLOBAL_NIC_DATA_QSIZE
      || nfl_tail.offset < 0 || nfl_tail.offset >= GLOBAL_NIC_DATA_QSIZE
      || !global_queue->queue_p[nfl_head.offset]
      || !global_queue->queue_p[nfl_tail.offset]) {
    iprint(LVL_EXP, "Err head %d %p tail %d %p"
        , nfl_head.offset, global_queue->queue_p[nfl_head.offset]
        , nfl_tail.offset, global_queue->queue_p[nfl_tail.offset]);
    atomic_set(&nicq_hdr->head, -1);
    atomic_set(&nicq_hdr->tail, -1);
    return -EINVAL;
  }

  init_nic_txdata(global_queue->queue_p[nfl_head.offset], 0, 0);
  init_nic_txdata(global_queue->queue_p[nfl_tail.offset], (uint64_t)-1, 0);

  atomic_set(&nicq_hdr->head, nfl_head.offset);
  atomic_set(&nicq_hdr->tail, nfl_tail.offset);
  atomic_set(&global_queue->queue_p[nfl_head.offset]->next, nfl_tail.offset);
  iprint(LVL_DBG, "head %d tail %d head.nxt %d"
      , atomic_read(&nicq_hdr->head)
      , atomic_read(&nicq_hdr->tail)
      , atomic_read(&global_queue->queue_p[nfl_head.offset]->next)
      );

  return 0;
}

int
init_nicq_hdr_missing(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_queue,
    nic_freelist_t *nfl)
{
  nic_fl_elem_t nfl_head = { 0 }, nfl_tail = { 0 };
  int pcm_nicq_head = 0, pcm_nicq_tail = 0;
  int head_ret = -1, tail_ret = -1;

  if (!nicq_hdr || !global_queue || !nfl)
    return -EINVAL;

  pcm_nicq_head = atomic_read(&nicq_hdr->head);
  pcm_nicq_tail = atomic_read(&nicq_hdr->tail);
  // pcm nicq head and tail already populated
  if ((pcm_nicq_head >= 0 && pcm_nicq_head < global_queue->qsize)
      && (pcm_nicq_tail >= 0 && pcm_nicq_head < global_queue->qsize))
    return 0;

  // missing head
  if (pcm_nicq_head < 0)
    head_ret = get_one_nic_freelist(nfl, &nfl_head);

  // missing tail
  if (pcm_nicq_tail < 0)
    tail_ret = get_one_nic_freelist(nfl, &nfl_tail);

  // missing head and allocating head failed
  if (pcm_nicq_head < 0 && head_ret < 0) {
    // if we allocated tail, free it before returning failure
    if (!tail_ret)
      put_one_nic_freelist(nfl, &nfl_tail);

    return -ENOMEM;
  }

  // missing tail and allocating tail failed
  if (pcm_nicq_tail < 0 && tail_ret < 0) {
    // if we allocated head, free it before returning failure
    if (!head_ret)
      put_one_nic_freelist(nfl, &nfl_head);

    return -ENOMEM;
  }

  // allocating head succeeded
  if (!head_ret) {
    atomic_set(&nicq_hdr->head, nfl_head.offset);
    init_nic_txdata(global_queue->queue_p[nfl_head.offset], 0, 0);
  }

  // allocating tail succeeded
  if (!tail_ret) {
    atomic_set(&nicq_hdr->tail, nfl_tail.offset);
    init_nic_txdata(global_queue->queue_p[nfl_tail.offset], (uint64_t)-1, 0);
  }

  atomic_set(&global_queue->queue_p[nfl_head.offset]->next, nfl_tail.offset);
  iprint(LVL_DBG, "head %d tail %d head.nxt %d"
      , atomic_read(&nicq_hdr->head)
      , atomic_read(&nicq_hdr->tail)
      , atomic_read(&global_queue->queue_p[nfl_head.offset]->next)
      );
  return 0;
}

int
cleanup_nicq_hdr(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_queue,
    nic_freelist_t *nfl, void *pcm_p)
{
  int rm_idx;
  int head_idx, tail_idx;
  int ret = 0;
  nic_fl_elem_t rm_elem;
  nic_fl_elem_t nfl_head, nfl_tail;

  if (!nicq_hdr)
    return -EINVAL;

  do {
    rm_idx = nicq_remove(nicq_hdr, global_queue, 0, pcm_p);
    iprint(LVL_INFO, "RM %d", rm_idx);
    // we emptied the nicq
    if (rm_idx < 0)
      break;

    rm_elem.offset = rm_idx;
//    rm_elem.length = 1;

    put_one_nic_freelist(nfl, &rm_elem);
  } while (1);

  do {
    head_idx = atomic_read(&nicq_hdr->head);
    ret = atomic_cmpxchg(&nicq_hdr->head, head_idx, -1);
  } while (ret != head_idx);
  nfl_head.offset = head_idx;

  do {
    tail_idx = atomic_read(&nicq_hdr->tail);
    ret = atomic_cmpxchg(&nicq_hdr->tail, tail_idx, -1);
  } while (ret != tail_idx);
  nfl_tail.offset = tail_idx;

//  nfl_head.length = nfl_tail.length = 1;

  if (nfl_head.offset >= 0)
    atomic_set(&global_queue->queue_p[nfl_head.offset]->next, -1);

  if (nfl_head.offset >= 0)
    put_one_nic_freelist(nfl, &nfl_head);
  if (nfl_tail.offset >= 0)
    put_one_nic_freelist(nfl, &nfl_tail);

  if (nfl_head.offset < 0 || nfl_head.offset >= GLOBAL_NIC_DATA_QSIZE
      || nfl_tail.offset < 0 || nfl_tail.offset >= GLOBAL_NIC_DATA_QSIZE
      || !global_queue->queue_p[nfl_head.offset]
      || !global_queue->queue_p[nfl_tail.offset]) {
    iprint(LVL_EXP, "Err head %d %p tail %d %p"
        , nfl_head.offset, global_queue->queue_p[nfl_head.offset]
        , nfl_tail.offset, global_queue->queue_p[nfl_tail.offset]);

    return -100;
  }

  return 0;
}

// =======

int
init_nic_freelist(nic_freelist_t *nfl, int total_size, int pcm_slab_size)
{
  int i;
  int n_elems = 0;

  if (!nfl || !total_size || !pcm_slab_size)
    return -EINVAL;

  n_elems = total_size / pcm_slab_size;
  nfl->nfl_arr = kcalloc(n_elems, sizeof(nic_fl_elem_t), GFP_KERNEL);
  if (!nfl->nfl_arr)
    return -ENOMEM;

  nfl->fl_size = n_elems;
  for (i = 0; i < n_elems; i++) {
    nfl->nfl_arr[i].offset = i * pcm_slab_size;
  }

  nfl->cons_idx = 0;
  nfl->prod_idx = nfl->fl_size;

  nfl->n_md = generate_mdesc(nfl->nfl_arr, sizeof(nic_fl_elem_t), nfl->fl_size,
      nfl->md_arr, MAX_NICFL_FRAGS);

  spin_lock_init(&nfl->lock);

  return 0;
}

void
cleanup_nic_freelist(nic_freelist_t *nfl)
{
  if (!nfl)
    return;

  if (nfl->nfl_arr)
    kfree(nfl->nfl_arr);
}

int
get_one_nic_freelist(nic_freelist_t *nfl, nic_fl_elem_t *nelem)
{
  int cons = 0;

  if (!nfl || !nelem)
    return -EINVAL;

  spin_lock_bh(&nfl->lock);

  if (nfl->cons_idx == nfl->prod_idx) {
    spin_unlock_bh(&nfl->lock);
    return -ENOMEM;
  }

  cons = nfl->cons_idx % nfl->fl_size;
  nelem->offset = nfl->nfl_arr[cons].offset;
  nfl->cons_idx = nfl->cons_idx + 1;

  spin_unlock_bh(&nfl->lock);

  return 0;
}

int
put_one_nic_freelist(nic_freelist_t *nfl, nic_fl_elem_t *nelem)
{
  int prod_idx = 0;

  if (!nfl || !nelem)
    return -EINVAL;

  prod_idx = nfl->prod_idx % nfl->fl_size;
  nfl->nfl_arr[prod_idx].offset = nelem->offset;

  nfl->prod_idx = nfl->prod_idx + 1;

  return 0;
}

/*
 * ==========
 * nic txintq
 * ==========
 */

int
init_nic_txintq(nic_txintq_t *ntq, int qsize)
{
  int i, j;
  int n_elem_per_page = 0;
  int ntq_size = 0;
  int n_pages = 0;
  int page_it = 0;
  int page_off = 0;
  int ptr_it = 0;
  void *page_start = NULL;

  if (!ntq)
    return -EINVAL;

  n_elem_per_page = PAGE_SIZE/sizeof(nic_txint_elem_t);
  n_pages = (qsize-1)/n_elem_per_page + 1;
  ntq_size = n_pages * PAGE_SIZE;

  ntq->queue = (nic_txint_elem_t *) kzalloc(ntq_size, GFP_KERNEL);
  if (!ntq->queue)
    return -ENOMEM;

  ntq->n_md = generate_mdesc_palign(ntq->queue, sizeof(nic_txint_elem_t), qsize,
      ntq->md_arr, MAX_TXINTQ_FRAGS);
  if (ntq->n_md < 0) {
    kfree(ntq->queue);
//    kfree(ntq->queue_p);
    return ntq->n_md;
  }

  ntq->queue_p =
    (nic_txint_elem_t **) kzalloc(sizeof(nic_txint_elem_t *) * qsize, GFP_KERNEL);
  if (!ntq->queue_p) {
    kfree(ntq->queue);
    return -ENOMEM;
  }

  ptr_it = 0;
  for (i = 0; i < ntq->n_md; i++) {
    page_start =
      (void *) __va((machine_to_phys(XMADDR(ntq->md_arr[i].maddr))).paddr);
    page_it = 0;
    for (j = 0; j < ntq->md_arr[i].n_elems; j++) {
      page_off = get_velem_size_off(j, sizeof(nic_txint_elem_t));
      ntq->queue_p[ptr_it] = (nic_txint_elem_t *) (page_start + page_off);
      ptr_it++;
    }
  }

  ntq->qsize = qsize;

  return ntq_size;
}

void
cleanup_nic_txintq(nic_txintq_t *ntq)
{
  if (!ntq)
    return;

  if (ntq->queue_p)
    kfree(ntq->queue_p);

  if (ntq->queue)
    kfree(ntq->queue);
}
