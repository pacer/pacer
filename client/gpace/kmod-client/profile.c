/*
 * profile.c
 *
 * created on: Feb 21, 2017
 * author: aasthakm
 * 
 * in kernel profile data structure
 */

#include "profile_map.h"
#include "sme_debug.h"
#include "sme_time.h"
#include <linux/slab.h>

typedef struct profile_int {
  /*
   * #outgoing requests per incoming request
   */
  uint16_t num_out_reqs;
  /*
   * over provisioning for enforcement
   * for each outgoing request on the out channel
   */
  uint16_t *num_extra_slots;
  /*
   * size(msg) = n*MTU
   * array of #frames
   * for each outgoing request on the out channel
   */
  uint64_t *num_out_frames;
  /*
   * inter-frame time
   * for each outgoing request on the out channel
   */
  uint64_t *spacing;
  /*
   * latency for first packet of the response
   * for each outgoing response on the out channel
   */
  uint64_t *latency;
} profile_int_t;

static void
__init_profile_int(profile_int_t *p, uint16_t num_out_reqs,
    uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *spacing, uint64_t *latency)
{
  if (!p)
    return;

  p->num_out_reqs = num_out_reqs;
  p->num_extra_slots = num_extra_slots;
  p->num_out_frames = num_out_frames;
  p->spacing = spacing;
  p->latency = latency;
}

int
init_profile(profile_t *p, uint16_t id, ihash_t id_ihash, uint16_t num_out_reqs,
    uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *spacing, uint64_t *latency)
{
  profile_int_t *p_int;

  if (!p) {
    printk(KERN_ERR "Fatal error!!!\n");
    return -EINVAL;
  }

  memset(p, 0, sizeof(profile_t));
  p->id_ihash = id_ihash;
  
  p_int = (profile_int_t *) kzalloc(sizeof(profile_int_t),
      GFP_KERNEL);

  if (!p_int) {
    printk(KERN_ERR "memory allocation error\n");
    return -ENOMEM;
  }

  __init_profile_int(p_int, num_out_reqs, num_extra_slots, num_out_frames,
      spacing, latency);
  p->priv = (void *) p_int;
  INIT_LIST_HEAD(&p->profile_listp);

  return 0;
}

#if 0
int
init_profile_hashes(profile_t *p, uint32_t num_pub, uint32_t num_priv,
    ihash_t *pubhash, ihash_t *privhash, uint16_t num_out_reqs,
    uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *spacing, uint64_t *latency)
{
  profile_int_t *p_int;

  if (!p) {
    printk(KERN_ERR "Fatal error!!!\n");
    return -EINVAL;
  }

  p->id = 0;
  p->num_pub = num_pub;
  p->num_priv = num_priv;
  p->pubhash = pubhash;
  p->privhash = privhash;

  p_int = (profile_int_t *) kzalloc(sizeof(profile_int_t),
      GFP_KERNEL);

  if (!p_int) {
    printk(KERN_ERR "memory allocation error\n");
    return -ENOMEM;
  }

  __init_profile_int(p_int, num_out_reqs, num_extra_slots, num_out_frames,
      spacing, latency);
  p->priv = (void *) p_int;

  INIT_LIST_HEAD(&p->profile_listp);
  return 0;
}
#endif

void
init_default_profile(profile_t *prof)
{
  uint16_t num_out_reqs = 1;
  uint16_t *num_extra_slots =
    (uint16_t *) kzalloc(sizeof(uint16_t) * num_out_reqs, GFP_KERNEL);
  uint64_t *num_out_frames =
    (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  uint64_t *spacing =
    (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  uint64_t *latency =
    (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);

  num_extra_slots[0] = 0;
  num_out_frames[0] = 1;
  spacing[0] = 0;
  latency[0] = 15;
  memset(prof, 0, sizeof(profile_t));
  init_profile(prof, DEFAULT_PROFILE_ID, DEFAULT_PROFILE_IHASH,
      num_out_reqs, num_extra_slots, num_out_frames, spacing, latency);

#if SME_DEBUG_LVL <= LVL_INFO
  print_profile(prof);
#endif
}

int
get_req_frame_idx_from_timer_idx(profile_t *p, uint16_t timer_idx,
    int *req_idx, int *frame_idx)
{
  int req_it = 0, frame_it = 0;
  int prev_timer_count = 0, timer_count = 0;
  profile_int_t *p_int = NULL;

  if (!p) {
    itrace;
    return -1;
  }

  p_int = p->priv;
  if (!p_int) {
    itrace;
    return -1;
  }

  prev_timer_count = 0;
  frame_it = -1;
  for (req_it = 0; req_it < p_int->num_out_reqs; req_it++) {
    timer_count += p_int->num_out_frames[req_it];
    if (timer_idx < timer_count) {
      frame_it = timer_idx - prev_timer_count;
      break;
    }

    prev_timer_count = timer_count;
  }

  if (frame_it < 0) {
    iprint(LVL_DBG, "No more timers, request %d max %d", timer_idx, timer_count);
    return -1;
  }

  *req_idx = req_it;
  *frame_idx = frame_it;

  return 0;
}

uint64_t
get_profile_timer_at_idx(profile_t *p, uint64_t ref_ts, uint16_t timer_idx)
{
  int ret = 0;
  int req_it = 0, frame_it = 0, timer_it = 0;
  uint64_t next_ts = 0;
  profile_int_t *p_int = NULL;

  p_int = (profile_int_t *) p->priv;
  ret = get_req_frame_idx_from_timer_idx(p, timer_idx, &req_it, &frame_it);
  if (ret < 0) {
    return ret;
  }

  // TODO: share cwnd shift from hypervisor
  next_ts = ref_ts + p_int->latency[req_it];
  iprint(LVL_DBG, "ref ts %lld timer idx %d req %d frame %d\n"
      "lat %lld spc %lld next ts %lld"
      , ref_ts, timer_idx, req_it, frame_it
      , p_int->latency[req_it], p_int->spacing[req_it], next_ts);
  if (frame_it == 0)
    return next_ts;

  for (timer_it = 0; timer_it < frame_it; timer_it++) {
    next_ts += p_int->spacing[req_it];
  }

  return next_ts;
}

void
init_one_profile(profile_t *prof, ihash_t pid)
{
  int i;
  uint16_t num_out_reqs;
  uint16_t *num_extra_slots;
  uint64_t *num_out_frames;
  uint64_t *latency, *spacing;
  uint64_t l_arr[100];

  memset((void *) &l_arr, 0, sizeof(uint64_t)*100);
  if (memcmp(pid.byte, DEFAULT_PROFILE_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 3;
    l_arr[0] = 5000000;
    l_arr[1] = 100000000;
    l_arr[2] = 200000000;
  } else if (memcmp(pid.byte, DEFAULT_HTTP_PROF_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 1;
    l_arr[0] = 5000000;
//    l_arr[1] = 10000000;
  } else if (memcmp(pid.byte, DEFAULT_HTTPS_PROF_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 4;
    l_arr[0] = 200000;
    l_arr[1] = 1000000;
    l_arr[2] = 6000000;
    l_arr[3] = 10000000;
  } else if (memcmp(pid.byte, DEFAULT_MW_PROF_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 4;
    l_arr[0] = 200000;
    l_arr[1] = 1000000;
    l_arr[2] = 6000000;
    l_arr[3] = 10000000;
  } else if (memcmp(pid.byte, DEFAULT_VIDEO_PROF_IHASH.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 50;
    l_arr[0] = 200000;
    l_arr[1] = 1000000;
    l_arr[2] = 6000000;
    l_arr[3] = 10000000;
    for (i = 4; i < 50; i++) {
      l_arr[i] = l_arr[i-1] + 5000000;
    }
  } else if (memcmp(pid.byte, pid1.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 3;
    l_arr[0] = 5000000;
    l_arr[1] = 100000000;
    l_arr[2] = 200000000;
  } else if (memcmp(pid.byte, pid2.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 5;
    l_arr[0] = 5000000;
    l_arr[1] = 100000000;
    l_arr[2] = 100050000;
    l_arr[3] = 100100000;
    l_arr[4] = 100200000;
  } else if (memcmp(pid.byte, pid3.byte, sizeof(ihash_t)) == 0) {
    num_out_reqs = 15;
    l_arr[0] = 5000000;
    l_arr[1] = 100000000;
    for (i = 2; i < 15; i++)
      l_arr[i] = l_arr[i-1] + 50000;
  } else {
    num_out_reqs = 4;
    l_arr[0] = 100000000;
    l_arr[1] = 200000000;
    l_arr[2] = 300000000;
    l_arr[3] = 400000000;
  }

  num_extra_slots = kzalloc(sizeof(uint16_t) * num_out_reqs, GFP_KERNEL);
  num_out_frames = kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  latency = kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  spacing = kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  for (i = 0; i < num_out_reqs; i++) {
    num_extra_slots[i] = 0;
    num_out_frames[i] = 1;
    spacing[i] = 0;
    latency[i] = l_arr[i];
  }

  memset(prof, 0, sizeof(profile_t));
  init_profile(prof, 0, pid, num_out_reqs, num_extra_slots, num_out_frames,
      spacing, latency);

#if SME_DEBUG_LVL <= LVL_INFO
  print_profile(prof);
#endif
}

void
init_ihashes(void)
{
  uint32_t def_prof_id = 100;
  char *profid = NULL;
  memset(DEFAULT_PROFILE_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_PROFILE_IHASH.byte, &def_prof_id, sizeof(uint32_t));

  profid = "http";
  memset(DEFAULT_HTTP_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_HTTP_PROF_IHASH.byte, profid, strlen(profid));

  profid = "sslfin";
  memset(DEFAULT_SSLFIN_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_SSLFIN_PROF_IHASH.byte, profid, strlen(profid));

  profid = "https";
  memset(DEFAULT_HTTPS_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_HTTPS_PROF_IHASH.byte, profid, strlen(profid));

  profid = "MW";
  memset(DEFAULT_MW_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_MW_PROF_IHASH.byte, profid, strlen(profid));

  profid = "VIDEO";
  memset(DEFAULT_VIDEO_PROF_IHASH.byte, 0, sizeof(ihash_t));
  memcpy(DEFAULT_VIDEO_PROF_IHASH.byte, profid, strlen(profid));

  profid = "0x1234567890";
  memset(pid1.byte, 0, sizeof(ihash_t));
  memcpy(pid1.byte, profid, strlen(profid));

  def_prof_id = 2048;
  memset(pid2.byte, 0, sizeof(ihash_t));
  memcpy(pid2.byte, (void *) &def_prof_id, sizeof(uint32_t));

  def_prof_id = 16384;
  memset(pid3.byte, 0, sizeof(ihash_t));
  memcpy(pid3.byte, (void *) &def_prof_id, sizeof(uint32_t));
}

void
init_profiles(sme_htable_t *pmap_htbl)
{
  profile_t *prof = NULL;
  prof = kzalloc(sizeof(profile_t), GFP_KERNEL);
  init_one_profile(prof, DEFAULT_PROFILE_IHASH);
  htable_insert(pmap_htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = kzalloc(sizeof(profile_t), GFP_KERNEL);
  init_one_profile(prof, DEFAULT_HTTP_PROF_IHASH);
  htable_insert(pmap_htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = kzalloc(sizeof(profile_t), GFP_KERNEL);
  init_one_profile(prof, DEFAULT_HTTPS_PROF_IHASH);
  htable_insert(pmap_htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = kzalloc(sizeof(profile_t), GFP_KERNEL);
  init_one_profile(prof, DEFAULT_MW_PROF_IHASH);
  htable_insert(pmap_htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = kzalloc(sizeof(profile_t), GFP_KERNEL);
  init_one_profile(prof, DEFAULT_VIDEO_PROF_IHASH);
  htable_insert(pmap_htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = kzalloc(sizeof(profile_t), GFP_KERNEL);
  init_one_profile(prof, pid1);
  htable_insert(pmap_htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = kzalloc(sizeof(profile_t), GFP_KERNEL);
  init_one_profile(prof, pid2);
  htable_insert(pmap_htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);

  prof = kzalloc(sizeof(profile_t), GFP_KERNEL);
  init_one_profile(prof, pid3);
  htable_insert(pmap_htbl, (void *) &prof->id_ihash, sizeof(ihash_t),
      &prof->profile_listp);
}

void
__free_profile_int(profile_int_t *p)
{
  if (!p)
    return;

  if (p->num_extra_slots)
    kfree(p->num_extra_slots);

  if (p->num_out_frames)
    kfree(p->num_out_frames);

  if (p->spacing)
    kfree(p->spacing);

  if (p->latency)
    kfree(p->latency);
}

void
reset_profile(profile_t *p)
{
  profile_int_t *p_int;
  if (!p) {
    printk(KERN_ERR "Fatal error!!!\n");
    return;
  }

  p_int = (profile_int_t *) p->priv;
  __free_profile_int(p_int);
  kfree(p_int);

  memset(p, 0, sizeof(profile_t));
}

int16_t
get_num_out_reqs(profile_t *p)
{
  profile_int_t *p_int;
  if (!p || !p->priv)
    return -1;

  p_int = (profile_int_t *) p->priv;
  return p_int->num_out_reqs;
}

int64_t
get_num_out_frames_for_req(profile_t *p, int req_idx)
{
  profile_int_t *p_int;
  if (!p || !p->priv)
    return -1;

  p_int = (profile_int_t *) p->priv;
  return p_int->num_out_frames[req_idx];
}

int16_t
get_num_extra_slots_for_req(profile_t *p, int req_idx)
{
  profile_int_t *p_int;
  if (!p || !p->priv)
    return -1;

  p_int = (profile_int_t *) p->priv;
  return p_int->num_extra_slots[req_idx];
}

int64_t
get_latency_for_req(profile_t *p, int req_idx)
{
  profile_int_t *p_int;
  if (!p || !p->priv)
    return -1;

  p_int = (profile_int_t *) p->priv;
  return p_int->latency[req_idx];
}

int64_t
get_spacing_for_req(profile_t *p, int req_idx)
{
  profile_int_t *p_int;
  if (!p || !p->priv)
    return -1;

  p_int = (profile_int_t *) p->priv;
  return p_int->spacing[req_idx];
}

int64_t
get_total_frames(profile_t *p)
{
  int req_it;
  int64_t total_frames = 0;
  profile_int_t *p_int;
  if (!p)
    return -1;

  p_int = (profile_int_t *) p->priv;
  for (req_it = 0; req_it < p_int->num_out_reqs; req_it++) {
    total_frames += p_int->num_out_frames[req_it];
  }

  return total_frames;
}

int64_t
get_last_latency(profile_t *p)
{
  int last_req;
  int64_t last_req_frames;
  uint64_t last_req_spacing;
  int64_t last_ts = 0;
  profile_int_t *p_int;
  if (!p)
    return -1;

  p_int = (profile_int_t *) p->priv;
  if (!p_int)
    return -1;

  last_req = p_int->num_out_reqs - 1;
  last_req_frames = p_int->num_out_frames[last_req];
  last_req_spacing = p_int->spacing[last_req];

  last_ts += p_int->latency[last_req];
  last_ts += last_req_spacing * (last_req_frames - 1);
  return last_ts;
}

int64_t
get_last_resp_timestamp(profile_t *p, uint64_t ref_ts)
{
  int last_req;
  int64_t last_req_frames;
  uint64_t last_req_spacing;
  int64_t last_ts = 0;
  profile_int_t *p_int;
  if (!p)
    return -1;

  p_int = (profile_int_t *) p->priv;
  if (!p_int)
    return -1;

  last_req = p_int->num_out_reqs - 1;
  last_req_frames = p_int->num_out_frames[last_req];
  last_req_spacing = p_int->spacing[last_req];

  last_ts = ref_ts;
  last_ts += p_int->latency[last_req];
  last_ts += last_req_spacing * (last_req_frames - 1);
  return last_ts;
}

void
print_profile(profile_t *p)
{
  profile_int_t *p_int;
  char dbg_buf[MAX_DBG_BUF_LEN];
  int i, dbg_buf_off = 0;

  //SME_IHASH
  char hbuf[64];
  int hbuf_len = 64;
  if (!p)
    return;

  p_int = (profile_int_t *) p->priv;

  //SME_IHASH 
  memset(hbuf, 0, hbuf_len);
  prt_hash(p->id_ihash.byte, sizeof(ihash_t), hbuf);

  memset(dbg_buf, 0, MAX_DBG_BUF_LEN);
  sprintf(dbg_buf + dbg_buf_off, "prof ID (Hex): %s\n", hbuf);
  dbg_buf_off = strlen(dbg_buf);
  
  sprintf(dbg_buf + dbg_buf_off, "# reqs: %d\n", p_int->num_out_reqs);
  dbg_buf_off = strlen(dbg_buf);
  sprintf(dbg_buf + dbg_buf_off, "# extra slots --- ");
  dbg_buf_off = strlen(dbg_buf);
  for (i = 0; i < p_int->num_out_reqs; i++) {
    sprintf(dbg_buf + dbg_buf_off, "%u ", p_int->num_extra_slots[i]);
    dbg_buf_off = strlen(dbg_buf);
  }
  sprintf(dbg_buf + dbg_buf_off, "\n");
  dbg_buf_off = strlen(dbg_buf);
  sprintf(dbg_buf + dbg_buf_off, "# out frames --- ");
  dbg_buf_off = strlen(dbg_buf);
  for(i = 0; i < p_int->num_out_reqs; i++) {
    sprintf(dbg_buf + dbg_buf_off, "%llu ", p_int->num_out_frames[i]);
    dbg_buf_off = strlen(dbg_buf);
  }
  sprintf(dbg_buf + dbg_buf_off, "\n");
  dbg_buf_off = strlen(dbg_buf);
  sprintf(dbg_buf + dbg_buf_off, "spacing --- ");
  dbg_buf_off = strlen(dbg_buf);
  for(i = 0; i < p_int->num_out_reqs; i++) {
    sprintf(dbg_buf + dbg_buf_off, "%llu ", p_int->spacing[i]);
    dbg_buf_off = strlen(dbg_buf);
  }
  sprintf(dbg_buf + dbg_buf_off, "\n");
  dbg_buf_off = strlen(dbg_buf);
  sprintf(dbg_buf + dbg_buf_off, "latency --- ");
  dbg_buf_off = strlen(dbg_buf);
  for(i = 0; i < p_int->num_out_reqs; i++) {
    sprintf(dbg_buf + dbg_buf_off, "%llu ", p_int->latency[i]);
    dbg_buf_off = strlen(dbg_buf);
  }

  iprint(LVL_INFO, "PROFILE %s", dbg_buf);
}

