/*
 * sme_skb_single.h
 *
 * created on: Apr 21, 2017
 * author: aasthakm
 *
 * single global ring buffer shared across all cpus
 */

#ifndef __SME_SKB_SINGLE_H__
#define __SME_SKB_SINGLE_H__

#include "sme_q_common.h"

extern sme_q_t ts_irq_q[RX_NCPUS];
extern sme_log_q_t *proflog_q;
extern sme_log_q_t *pacelog_q[NUM_PACERS];

#endif /* __SME_SKB_PER_CONN_H__ */

