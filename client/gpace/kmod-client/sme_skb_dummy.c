/*
 * sme_skb_dummy.c
 *
 * created on: Apr 25, 2017
 * author: aasthakm
 *
 * API to create dummy skb
 */

#include "sme_skb.h"
#include "sme_skb_dummy.h"
#include "sme_common.h"
#include "sme_time.h"
#include "sme_debug.h"

#include <linux/etherdevice.h> /* for eth_type_trans(), MAX_ADDR_LEN */

#include <net/tcp.h> /* for tcp_v4_check(), tcphdr */
#include <net/udp.h> /* for udp_v4_check(), udphdr */

void
populate_eth_hdr(struct ethhdr *eth, char *s_mac, char *d_mac)
{
  if (!eth)
    return;

  if (s_mac)
    ether_addr_copy((u8 *) eth->h_source, (u8 *) s_mac);
  if (d_mac)
    ether_addr_copy((u8 *) eth->h_dest, (u8 *) d_mac);

  eth->h_proto = htons(ETH_P_IP);
}

void
populate_ip_hdr(struct iphdr *iph, uint32_t skb_len, uint32_t ip_ihl,
    uint32_t s_addr, uint32_t d_addr, uint8_t protocol)
{
  if (!iph)
    return;

  iph->version = 4;
  iph->ihl = ip_ihl;
  iph->tot_len = htons(skb_len);
  iph->frag_off = htons(IP_DF);
  iph->protocol = protocol; // or UDP?
  iph->saddr = s_addr;
  iph->daddr = d_addr;
  // TODO
  iph->tos = 0;
  iph->ttl = 64;
  iph->id = 0;
  iph->check = 0;
  iph->check = ip_fast_csum((u8 *) iph, iph->ihl);
  //ip_send_check(iph);

}

void
populate_tcp_hdr(struct tcphdr *tcph, uint32_t skb_len, uint32_t ip_ihl,
    uint32_t s_addr, uint32_t d_addr, uint16_t s_port, uint16_t d_port,
    uint32_t seqno, uint32_t seqack,
    uint8_t fin, uint8_t syn, uint8_t rst, uint8_t psh,
    uint8_t ack, uint8_t urg, uint8_t ece, uint8_t cwr)
{
  int transport_hdr_len = 32;
  int tcplen = 0;

  if (!tcph)
    return;

  tcph->source = htons(s_port);
  tcph->dest = htons(d_port);
  tcph->doff = transport_hdr_len/4;
  tcph->res1 = 0;
  tcph->cwr = cwr;
  tcph->ece = ece;
  tcph->urg = urg;
  tcph->ack = 1;
  tcph->psh = psh;
  tcph->rst = rst;
  tcph->syn = syn;
  tcph->fin = fin;
  // TODO
  tcph->seq = seqno;
  tcph->ack_seq = seqack;
  tcph->check = 0;
  tcplen = skb_len - (ip_ihl << 2);
  tcph->check = tcp_v4_check(tcplen, s_addr, d_addr,
      csum_partial((char *) tcph, tcplen, 0));
}

void
populate_udp_hdr(struct udphdr *udph, int data_len,
    uint32_t s_addr, uint32_t d_addr, uint16_t s_port, uint16_t d_port)
{
  int udplen = 0;

  if (!udph)
    return;

  udph->source = htons(s_port);
  udph->dest = htons(d_port);
  udplen = data_len + sizeof(struct udphdr);
  udph->len = htons(udplen);
  udplen = udph->len - sizeof(struct udphdr);
  udph->check = 0;
#if 0
  // XXX: disable UDP checksum if app not waiting for udp ack
  udph->check = udp_v4_check(udplen, s_addr, d_addr,
      csum_partial((char *) udph, udplen, 0));
#endif
}

#if 0
static void
prepare_payload(char *buf, int *data_len, partial_sk_buff_t *pskb)
{
  if (!buf || !data_len)
    return;

  memset(buf, 0, SME_TCP_MTU);

  if (!pskb) {
    //sprintf(buf, "Dummy packet data");
    //*data_len = strlen(buf);
    ((uint64_t *) buf)[0] = get_current_time(SCALE_NS);
    *data_len = sizeof(uint64_t);
    return;
  }

  ((uint64_t *) buf)[0] = pskb->in_timestamp;
  *data_len = sizeof(uint64_t);
}
#endif

void
build_dummy_skb_with_data(struct sk_buff **dummy_skb_p, char *payload,
    int plen_in_hdr, uint32_t s_addr, uint32_t d_addr, uint16_t s_port,
    uint16_t d_port, uint32_t seqno, uint32_t seqack, uint8_t protocol,
    char *s_mac, char *d_mac, struct net_device *dev, int coreid)
{
  struct sk_buff *dummy_skb = NULL;
  char *data = NULL;
  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  struct udphdr *udph = NULL;
  int transport_hdr_len = 0;
  int max_hdr_len = 0;
  int ip_tot_len = 0;
  int payload_len = 0;
  int real_payload_len = 0;
#if 0
  int ip_ihl = 5;
  int data_len = 0;
#endif

  if (!dummy_skb_p || !payload)
    return;

  // set second arg to non-zero value to avoid warning in ksize()
  // then reset head_frag and pfmemalloc flags below.
  dummy_skb = build_skb(payload, PAGE_SIZE);
  if (!dummy_skb)
    return;

  dummy_skb->head_frag = 0;
  dummy_skb->pfmemalloc = 0;

  if (protocol == IPPROTO_TCP) {
    if (SME_TCP_MTU == 1460)
      transport_hdr_len = 20;
    else
      transport_hdr_len = 32;
    payload_len = SME_TCP_MTU;
  } else {
    transport_hdr_len = 8;
    payload_len = SME_TCP_MTU + 24;
  }
  real_payload_len = plen_in_hdr;

  max_hdr_len = sizeof(struct ethhdr) + sizeof(struct iphdr) + transport_hdr_len;

  skb_reserve(dummy_skb, max_hdr_len);
  eth = (struct ethhdr *) skb_put(dummy_skb, sizeof(struct ethhdr));
  iph = (struct iphdr *) skb_put(dummy_skb, sizeof(struct iphdr));
  if (protocol == IPPROTO_TCP) {
    tcph = (struct tcphdr *) skb_put(dummy_skb, transport_hdr_len);
  } else {
    udph = (struct udphdr *) skb_put(dummy_skb, transport_hdr_len);
  }
  data = skb_put(dummy_skb, payload_len);

  ip_tot_len = sizeof(struct iphdr) + transport_hdr_len + real_payload_len;

#if 0
  // add eth hdr
  populate_eth_hdr(eth, s_mac, d_mac);

  // add ip hdr
  populate_ip_hdr(iph, ip_tot_len, ip_ihl, s_addr, d_addr, protocol);

  // add transport hdr
  if (protocol == IPPROTO_TCP) {
    populate_tcp_hdr(tcph, ip_tot_len, ip_ihl, s_addr, d_addr, s_port, d_port,
        0, 0, 0, 0, 0, 0, 1, 0, 0, 0);
  } else {
    populate_udp_hdr(udph, real_payload_len, s_addr, d_addr, s_port, d_port);
  }

  prepare_payload(data, &data_len, NULL);
#endif

  dummy_skb->dev = dev;
  dummy_skb->protocol = htons(ETH_P_IP);
  dummy_skb->ip_summed = CHECKSUM_PARTIAL;
//  dummy_skb->csum_start = dummy_skb->transport_header;
//  dummy_skb->csum_offset = 16;
  skb_set_mac_header(dummy_skb, 0);
  skb_set_network_header(dummy_skb, sizeof(struct ethhdr));
  skb_set_transport_header(dummy_skb, sizeof(struct ethhdr)+sizeof(struct iphdr));

  dummy_skb->len = ETH_FRAME_LEN;
  skb_set_queue_mapping(dummy_skb, coreid);

  *dummy_skb_p = dummy_skb;
}

void
build_dummy_tcp_skb(struct sk_buff **dummy_skb_p, struct sock *sk,
    int alloc_type, uint32_t seqno, uint32_t seqack)
{
  struct sk_buff *dummy_skb = NULL;
  struct tcp_skb_cb *tcb = NULL;
  int wmem_scheduled = 0;
  struct inet_sock *inet = inet_sk(sk);

  if (!dummy_skb_p)
    return;

  if (alloc_type == TCP_DUMMY_ALLOC) {
    dummy_skb = alloc_skb_fclone(SME_TCP_MTU + sk->sk_prot->max_header, GFP_KERNEL);
//    dummy_skb = alloc_skb(SME_TCP_MTU + sk->sk_prot->max_header, GFP_KERNEL);
//  } else {
//    dummy_skb = sk_stream_alloc_skb(sk, tcp_sk(sk)->mss_cache,
//        sk->sk_allocation, 0);
  }

  if (!dummy_skb) {
    printk(KERN_ERR "%s:%d [%pI4 %u, %pI4 %u] failed to alloc dummy skb\n"
        "sndbuf %u rcvbuf %u fwd_alloc %u wmem_queued %u rmem_alloc %u wmem_alloc %u"
        , __func__, __LINE__, (void *) &inet->inet_saddr, ntohs(inet->inet_sport)
        , (void *) &inet->inet_daddr, ntohs(inet->inet_dport)
        , sk->sk_sndbuf, sk->sk_rcvbuf, sk->sk_forward_alloc
        , sk->sk_wmem_queued, atomic_read(&sk->sk_rmem_alloc)
        , atomic_read(&sk->sk_wmem_alloc)
        );
    sk->sk_prot->enter_memory_pressure(sk);
    sk_stream_moderate_sndbuf(sk);
    return;
  }

  wmem_scheduled = sk_wmem_schedule(sk, dummy_skb->truesize);
  if (!wmem_scheduled) {
    __kfree_skb(dummy_skb);
    return;
  }

  skb_reserve(dummy_skb, sk->sk_prot->max_header);
  dummy_skb->reserved_tailroom = dummy_skb->end - dummy_skb->tail - SME_TCP_MTU;

  dummy_skb->head_frag = 0;
  dummy_skb->csum = 0;
  tcb = TCP_SKB_CB(dummy_skb);
  tcb->seq = seqno;
  tcb->ack_seq = seqack;
  tcb->end_seq = tcb->seq + SME_TCP_MTU;
  tcb->tcp_flags = TCPHDR_ACK;
  tcb->sacked = 0;
  tcb->tx_padding = SME_TCP_MTU;
  tcb->tcp_gso_segs = 1;
  /*
   * Ideally, we should not have to set skb->len here. later parts of TCP TX path
   * should set skb->len. However, if this field is 0, then skb->len infact
   * becomes negative in tcp_retransmit_skb path, and causes trouble.
   */
  dummy_skb->len = 0;
//  dummy_skb->len = SME_TCP_MTU;
  dummy_skb->data_len = 0;

  skb_mstamp_get(&dummy_skb->skb_mstamp);

  *dummy_skb_p = dummy_skb;
}

void
free_dummy_skb(struct sk_buff **dummy_skb_p)
{
  struct sk_buff *dummy_skb;
  if (!dummy_skb_p || !(*dummy_skb_p))
    return;

  dummy_skb = *dummy_skb_p;
  kfree_skb(dummy_skb);
  dummy_skb = NULL;
  *dummy_skb_p = NULL;
}

