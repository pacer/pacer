#include "generic_q.h"
#include "sme_config.h"
#include "sme_debug.h"
#include "sme_time.h"

#include <linux/slab.h>
#include <linux/time.h>
#include <linux/delay.h>

void
do_print_statq(generic_q_t *queue)
{
  uint64_t i;
  statq_elem_t *e = NULL;
  int64_t num_elems = queue->prod_idx - queue->cons_idx;

  if (num_elems == 0)
    return;

  if (queue->has_negative == 0)
    return;

  iprint2(LVL_EXP, "=== [%d] port %d #elems %lld prod %llu cons %llu ==="
      , queue->qidx, queue->port, num_elems
      , queue->prod_idx, queue->cons_idx);
  for (i = queue->cons_idx; i  < queue->prod_idx; i++) {
    e = (statq_elem_t *) ((unsigned long) queue->elem_arr + (i * queue->elem_size));
    iprint2(LVL_EXP, "%llu (%d) %d:%d:%d %d %u %u %u [%u] [%d, %d, %d] %d %d"
        , e->start_ts, e->pid, e->caller, e->owner, e->coreid, e->gso
        , e->snd_una, e->snd_nxt, e->write_seq, e->shared_seq, e->packets_out
        , e->old_packets_out, e->new_packets_out, e->skb_len, e->padding);
    if (i % 100 == 0)
      msleep(1);
  }
}

void
do_print_largef_q(generic_q_t *queue)
{
  uint64_t i;
  largef_elem_t *e = NULL;
  int64_t num_elems = queue->prod_idx - queue->cons_idx;

  if (num_elems == 0)
    return;

  iprint3(LVL_EXP, "=== [%d] port %d #elems %lld prod %llu cons %llu ==="
      , queue->qidx, queue->port, num_elems, queue->prod_idx, queue->cons_idx
      );
  for (i = queue->cons_idx; i < queue->prod_idx; i++) {
    e = (largef_elem_t *) ((unsigned long) queue->elem_arr + (i * queue->elem_size));
    iprint3(LVL_EXP, "%llu [%d %d %d %d] T %u %u %u %u %u %u %u W %u %u %u "
        "G %u H %u %u %u %u %u %u RTO %u U %u %d %u"
        , e->ts, e->coreid, e->owner, e->ca_state, e->caller
        , e->packets_out, e->lost_out, e->sacked_out, e->retrans_out
        , e->snd_una, e->snd_nxt, e->rcv_nxt, e->cwnd, e->snd_wnd, e->rcv_wnd
        , e->pidx, e->tail, e->real, e->dummy, e->nxt, e->cwm, e->num_timers
        , e->icsk_rto, e->undo_marker, e->undo_retrans, e->high_seq
        );
    if (i % 100 == 0)
      msleep(1);
  }
}
