#include "../sme_config.h"
#include "../sme_debug.h"
#include "../sme_common.h"
#include "../sme_skb_dummy.h"
#include "../profile_map.h"
#include "../sme_xen.h"
#include "../sme_helper.h"
#include "../sme_time.h"
#include "../generic_q.h"
#include "ptcp_core.h"

#include <linux/tcp.h>
#include <linux/ip.h>
#include <net/tcp.h> // for TCP_SKB_CB(skb)

#include <asm/xen/hypercall.h>

extern list_t scm_list[NUM_PACERS];
#if CONFIG_XEN_PACER
extern profq_hdr_t *profq_hdr[MAX_HP_ARGS];
#endif

void
mod_print_sock_skb(struct sock *sk, struct sk_buff *skb, char *dbg_str)
{
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
#if defined(CONFIG_XEN_SME) && CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  sme_channel_map_t *scm = NULL;
  profile_conn_map_t *pcm = NULL;
  profq_elem_t *pelem = NULL;
  int ret = 0;
#endif

  char skb_str[256];

  if (!sk)
    return;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return;

#if defined(CONFIG_XEN_SME) && CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  ret = get_profile_conn_info_from_skb_header(s_addr, d_addr, s_port, d_port,
      &scm_list[0], &scm, &pcm, 0, 0, 0, NULL);

  if (ret < 0 || !scm || !pcm)
    return;

  pelem = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
#endif

  memset(skb_str, 0, 256);
  if (skb) {
    sprintf(skb_str, "skb %p (%u-%u:%u) len %d sacked 0x%x pcount %d "
        "tail %d end %d data_len %d padding %d"
        , skb, TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->end_seq
        , TCP_SKB_CB(skb)->ack_seq, skb->len
        , TCP_SKB_CB(skb)->sacked, tcp_skb_pcount(skb)
        , skb->tail, skb->end, skb->data_len, TCP_SKB_CB(skb)->tx_padding
        );
  }

  iprint(LVL_INFO, "%s\n"
    "[%pI4 %u, %pI4 %u] "
    "retrans %d sack_out %d lost %d pkts %d "
    "snd_cwnd %d inflight %d CA %d pending %d\n"
    "rcv_nxt %u copied_seq %u snd_nxt %u snd_una %u write_seq %u "
#ifdef CONFIG_XEN_SME
    "shared_seq %u"
#endif
    "\n"
    "snd_head %p wq head %p "
    "wmem_alloc %d sndbuf %d fwd_alloc %d rmem_alloc %d rcvbuf %d\n"
    "snd_ssthresh %d rcv_ssthresh %d rcv queue len %d ucopy.len %d "
    "segs_out %d lock owner %d\n"
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
    "[PROF] G%llu state %d paused %d CWM %d NXT %d P %d T %d R %d D %d"
#endif
//    "wmem_queued %d wmem_alloc %d fwd_alloc %d sndbuf %d snd_wl1 %u sack_ok %d high_seq %u\n"
    "\n%s"
    , dbg_str
    , (void *) &inet->inet_saddr, ntohs(inet->inet_sport)
    , (void *) &inet->inet_daddr, ntohs(inet->inet_dport)
    , tp->retrans_out, tp->sacked_out, tp->lost_out, tp->packets_out
    , tp->snd_cwnd, tcp_packets_in_flight(tp)
    , inet_csk(sk)->icsk_ca_state, inet_csk(sk)->icsk_pending
    , tp->rcv_nxt, tp->copied_seq, tp->snd_nxt, tp->snd_una, tp->write_seq
#ifdef CONFIG_XEN_SME
    , tp->shared_seq
#endif
    , tcp_send_head(sk), tcp_write_queue_head(sk)
    , atomic_read(&sk->sk_wmem_alloc), sk->sk_sndbuf, sk->sk_forward_alloc
    , atomic_read(&sk->sk_rmem_alloc), sk->sk_rcvbuf
    , tp->snd_ssthresh, tp->rcv_ssthresh, skb_queue_len(&sk->sk_receive_queue)
    , tp->ucopy.len, tp->segs_out, sk->sk_lock.owned
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
    , pcm->profq_idx, pelem->state, atomic_read(&pelem->is_paused)
    , pelem->cwnd_watermark, pelem->next_timer_idx, pelem->num_timers
    , pelem->tail, atomic_read(&pelem->real_counter), pelem->dummy_counter
#endif
//    , sk->sk_wmem_queued, atomic_read(&sk->sk_wmem_alloc), sk->sk_forward_alloc
//    , sk->sk_sndbuf, tp->snd_wl1, tcp_is_sack(tp), tp->high_seq
    , skb_str
    );

#if 0
  {
    profq_elem_t *prof_p = NULL;
    largef_elem_t e;
    int dbg_idx = 0;
    prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
    memset((void *) &e, 0, sizeof(largef_elem_t));
    e.ts = get_current_time(SCALE_NS);
    e.snd_nxt = tp->snd_nxt;
    e.snd_una = tp->snd_una;
    e.rcv_nxt = tp->rcv_nxt;
    e.packets_out = tp->packets_out;
    e.lost_out = tp->lost_out;
    e.sacked_out = tp->sacked_out;
    e.retrans_out = tp->retrans_out;
    e.cwnd = tp->snd_cwnd;
    e.snd_wnd = tp->snd_wnd;
    e.rcv_wnd = tp->rcv_wnd;
    e.icsk_rto = inet_csk(sk)->icsk_rto;
    e.cwm = prof_p->cwnd_watermark;
    e.tail = prof_p->tail;
    e.real = atomic_read(&prof_p->real_counter);
    e.dummy = prof_p->dummy_counter;
    e.num_timers = prof_p->num_timers;
    e.nxt = prof_p->next_timer_idx;
    e.pidx = prof_p->profq_idx;
    e.ca_state = inet_csk(sk)->icsk_ca_state;
    e.caller = 7;
    e.coreid = smp_processor_id();
    e.owner = sk->sk_lock.owned;
    dbg_idx = d_port % STATQ_MODULO;
    if (generic_q_empty(largef_q[dbg_idx]))
      largef_q[dbg_idx]->port = d_port;
    put_generic_q(largef_q[dbg_idx], (void *) &e);
  }
#endif

  return;
}

int
mod_rx_pred_flags(struct sock *sk, struct sk_buff *skb, struct tcphdr *th)
{
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint16_t s_port = 0, d_port = 0;
  int ret = -EINVAL;

  if (!sk || !skb || !th)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

#if SME_CONFIG == SME_EXP
  ret =
    (((tcp_flag_word(th) & ~(TCP_RESERVED_BITS|TCP_FLAG_PSH|TCP_FLAG_URG))
      == tp->pred_flags) &&
      TCP_SKB_CB(skb)->seq == tp->rcv_nxt &&
      !after(TCP_SKB_CB(skb)->ack_seq, tp->snd_nxt));
  iprint(LVL_DBG, "th flag %0x masked %0x pred_flags %0x ret %d\n"
      "src %pI4 %u dst %pI4 %u (%u:%u) rcv_nxt %u copied_seq %u\n"
      "snd_una %u snd_nxt %u write_seq %u"
      , tcp_flag_word(th)
      , (tcp_flag_word(th) & ~(TCP_RESERVED_BITS|TCP_FLAG_PSH|TCP_FLAG_URG))
      , tp->pred_flags, ret
      , (void *) &inet->inet_saddr, s_port
      , (void *) &inet->inet_daddr, d_port
      , TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->ack_seq
      , tp->rcv_nxt, tp->copied_seq
      , tp->snd_una, tp->snd_nxt, tp->write_seq
      );
#endif
  return ret;
}

// returns 0 for pkts of paced flow (which have padding), and -EINVAL o.w.
int
mod_rx_handle_tcp_urg(struct sock *sk, struct sk_buff *skb, struct tcphdr *th)
{
#if SME_CONFIG == SME_DBG_LOG
  return -EINVAL;
#else
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint16_t s_port = 0, d_port = 0;

  if (!sk || !skb || !th)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  if (th->urg && th->urg_ptr) {
    iprint(LVL_DBG, "%pS skb len %d\n"
        "src %pI4 %u, dst %pI4 %u, [%d %d %d %d %d %d %d %d] "
        "(%u:%u) urg ptr %d sk state %d CA %d\n"
        "TCB seq (%u-%u) snd_una %d snd_nxt %d write_seq %d "
        "rcv_nxt %u copied_seq %u rcv_wnd %u rcv_wup %u\n"
        "urg_data %d current %p ucopy: task %p len %d sock_owned_user %d"
        , __builtin_return_address(2)
        , (skb ? skb->len : -1)
        , (void *) &inet->inet_saddr, s_port
        , (void *) &inet->inet_daddr, d_port
        , th->fin, th->syn, th->rst, th->psh
        , th->ack, th->urg, th->ece, th->cwr
        , ntohl(th->seq), ntohl(th->ack_seq), ntohs(th->urg_ptr)
        , (sk ? sk->sk_state : -1), inet_csk(sk)->icsk_ca_state
        , TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->end_seq
        , (tp ? tp->snd_una : -1), (tp ? tp->snd_nxt : -1)
        , (tp ? tp->write_seq : -1)
        , (tp ? tp->rcv_nxt : -1), (tp ? tp->copied_seq : -1)
        , (tp ? tp->rcv_wnd : -1), (tp ? tp->rcv_wup : -1)
        , tp->urg_data, current, tp->ucopy.task, tp->ucopy.len
        , sock_owned_by_user(sk)
        );
  }
  return 0;
#endif
}

int
mod_rx_handle_dummy(struct sock *sk, struct sk_buff *skb)
{
#if SME_CONFIG == SME_DBG_LOG
  return -EINVAL;
#else
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint16_t s_port = 0, d_port = 0;
  struct tcphdr *th = NULL;

  if (!sk || !skb)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  th = tcp_hdr(skb);
  // control pkts, or real pkts with padding
  if (!th->urg || th->urg_ptr != 0)
    return 1;

  tp->copied_seq += SME_TCP_MTU;
  TCP_SKB_CB(skb)->rx_copy_off = 0;
  tp->rcvq_space.seq += SME_TCP_MTU;


  iprint(LVL_DBG, "%pS %pS skb len %d\n"
      "src %pI4 %u, dst %pI4 %u, [%d %d %d %d %d %d %d %d] "
      "(%u:%u) urg ptr %d sk state %d\n"
      "snd_nxt %u rcv_nxt %u snd_una %u write_seq %u copied_seq %u"
      , __builtin_return_address(2), __builtin_return_address(3)
      , (skb ? skb->len : -1)
      , (void *) &inet->inet_saddr, s_port
      , (void *) &inet->inet_daddr, d_port
      , th->fin, th->syn, th->rst, th->psh
      , th->ack, th->urg, th->ece, th->cwr
      , ntohl(th->seq), ntohl(th->ack_seq), ntohs(th->urg_ptr)
      , (sk ? sk->sk_state : -1)
      , tp->snd_nxt, tp->rcv_nxt, tp->snd_una, tp->write_seq, tp->copied_seq
      );
  return 0;
#endif
}

int
mod_rx_disable_coalesce(struct sock *sk, struct sk_buff *to, struct sk_buff *from)
{
#if SME_CONFIG == SME_DBG_LOG
  return -EINVAL;
#else
  struct inet_sock *inet = NULL;
  uint16_t s_port = 0, d_port = 0;
  struct tcphdr *to_th = NULL;
  struct tcphdr *from_th = NULL;

  if (!sk)
    return -EINVAL;

  inet = inet_sk(sk);
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  if (to && from) {
    to_th = tcp_hdr(to);
    from_th = tcp_hdr(from);
    iprint(LVL_DBG, "%pS to %p nxt %p\n"
        "src %pI4 %u, dst %pI4 %u, TO[%d:%d] (%u:%u) NXT[%d:%d] (%u:%u)\n"
        "rcv_nxt %u copied_seq %u write head %p"
        , __builtin_return_address(2), to, from
        , (void *) &inet->inet_saddr, s_port
        , (void *) &inet->inet_daddr, d_port
        , to->len, ntohs(to_th->urg_ptr)
        , TCP_SKB_CB(to)->seq, TCP_SKB_CB(to)->end_seq
        , from->len, ntohs(from_th->urg_ptr)
        , TCP_SKB_CB(from)->seq, TCP_SKB_CB(from)->end_seq
        , tcp_sk(sk)->rcv_nxt, tcp_sk(sk)->copied_seq, tcp_write_queue_head(sk)
        );

    // if both skb's are full MTUs, we can allow coalescing them
    if (ntohs(to_th->urg_ptr) == SME_TCP_MTU
        && ntohs(from_th->urg_ptr) == SME_TCP_MTU)
      return 1;

  } else {
    iprint(LVL_DBG, "%pS to %p from %p\n"
        "src %pI4 %u, dst %pI4 %u, TO %p FROM %p"
        , __builtin_return_address(2), to, from
        , (void *) &inet->inet_saddr, s_port
        , (void *) &inet->inet_daddr, d_port
        , to, from
        );
  }
  return 0;
#endif
}

// return old value of skb->len
int
mod_rx_adjust_skb_size(struct sock *sk, struct sk_buff *skb, int req_len,
    int offset, int chunk, int copied, int flags)
{
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  struct tcphdr *th = NULL;
  struct iphdr *ih = NULL;
  int old_len = 0;

  if (!sk || !skb)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  th = tcp_hdr(skb);
  ih = ip_hdr(skb);
  iprint(LVL_DBG, "%pS len %d off %d chunk %d copied %d flags %x\n"
      "src %pI4 %u dst %pI4 %u [%d %d %d %d %d %d %d %d] ID %d urg ptr %d size %d %d\n"
      "seq %u end_seq %u copied %u rcv_nxt %u sk_rcvlowat %d "
      "ucopy len %d task %p\n"
      , __builtin_return_address(3)
      , req_len, offset, chunk, copied, flags
      , (void *) &inet->inet_saddr, ntohs(inet->inet_sport)
      , (void *) &inet->inet_daddr, ntohs(inet->inet_dport)
      , th->fin, th->syn, th->rst, th->psh, th->ack, th->urg, th->ece, th->cwr
      , ntohs(ih->id), ntohs(th->urg_ptr), skb->len, ntohs(ih->tot_len)
      , TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->end_seq
      , tp->copied_seq, tp->rcv_nxt, sk->sk_rcvlowat, tp->ucopy.len
      , tp->ucopy.task
      );

  // TODO: return adjust skb->len - headerlen --> no, just old skb->len
#if SME_CONFIG == SME_EXP
  old_len = skb->len;
  if (skb->len > SME_TCP_MTU) { // coalesced skbuff
    th->urg_ptr = 0;
  } else {
    skb->len = ntohs(th->urg_ptr);
  }
#endif
  return old_len;
}

int
mod_rx_adjust_copied_seq(struct sock *sk, struct sk_buff *skb, int old_skb_len,
    int new_skb_len)
{
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  struct tcphdr *th = NULL;
  struct iphdr *ih = NULL;
  int seq_delta = 0;
  int mtu_delta = 0;

  if (!sk || !skb)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  th = tcp_hdr(skb);
  ih = ip_hdr(skb);
  iprint(LVL_DBG, "%pS skb len %d => %d uclen %d rx_off %u\n"
      "src %pI4 %u, dst %pI4 %u\n"
      "snd_nxt %u rcv_nxt %u snd_una %u write_seq %u copied_seq %u\n"
      "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %d inflight %d reordering %d\n"
      "wmem_queued %d wmem_alloc %d fwd_alloc %d rmem_alloc %d sndbuf %u rcvbuf %u"
      , __builtin_return_address(3)
      , old_skb_len, new_skb_len, tp->ucopy.len, TCP_SKB_CB(skb)->rx_copy_off
      , (void *) &inet->inet_saddr, ntohs(inet->inet_sport)
      , (void *) &inet->inet_daddr, ntohs(inet->inet_dport)
      , tp->snd_nxt, tp->rcv_nxt, tp->snd_una, tp->write_seq, tp->copied_seq
      , tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup, tp->snd_cwnd
      , tcp_packets_in_flight(tp), tp->reordering
      , sk->sk_wmem_queued, atomic_read(&sk->sk_wmem_alloc), sk->sk_forward_alloc
      , atomic_read(&sk->sk_rmem_alloc), sk->sk_sndbuf, sk->sk_rcvbuf
      );

  seq_delta = 0;
  mtu_delta = 0;
#if SME_CONFIG == SME_EXP
  // coalesced buff, definitely no padding here
  if (skb->len > SME_TCP_MTU)
    return 0;

  mtu_delta = SME_TCP_MTU - skb->len;
  if (TCP_SKB_CB(skb)->rx_copy_off == skb->len) {
    tp->copied_seq += mtu_delta;
    tp->rcvq_space.seq += mtu_delta;
  }
#if 0
  seq_delta = tp->copied_seq - TCP_SKB_CB(skb)->seq;
  mtu_delta = TCP_SKB_CB(skb)->seq + SME_TCP_MTU - tp->copied_seq;
  if (seq_delta == ntohs(th->urg_ptr))
    tp->copied_seq += mtu_delta;
#endif
#endif

  return 0;
}

int
mod_tx_adjust_seq(struct sock *sk, struct sk_buff *skb, int copy, int copied)
{
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  uint32_t old_write_seq = 0, old_end_seq = 0;

  if (!sk || !skb)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port)) {
    return -EINVAL;
  }

  old_write_seq = tp->write_seq;
  old_end_seq = TCP_SKB_CB(skb)->end_seq;
#if SME_CONFIG == SME_DBG_LOG
  tp->write_seq += copy;
  TCP_SKB_CB(skb)->end_seq += copy;
#else
  // update end_seq and write_seq by MTU
  // set padding length so that we can set urg pointer later on.
  tp->write_seq += SME_TCP_MTU;
  TCP_SKB_CB(skb)->end_seq += SME_TCP_MTU;
  TCP_SKB_CB(skb)->tx_padding = (SME_TCP_MTU - copy);
#endif
  iprint(LVL_DBG, "%pS copy %d copied %d skb %p len %d\n"
      "src %pI4 %u, dst %pI4 %u (%u:%u) write_seq %u => %u\n"
//      " end_seq %u => %u"
      "snd_nxt %u rcv_nxt %u snd_una %u"
#ifdef CONFIG_XEN_SME
      " shared_seq %u gen %llu xmit %llu"
#endif
      " pcount %d\n"
      "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %u inflight %u\n"
      "availroom %d tail %d end %d tailroom %d restail %d EOR %d "
      "wmem_queued %d wmem_alloc %d fwd_alloc %d sndbuf %d flags %d \n"
      , __builtin_return_address(2)
      , copy, copied, skb, skb->len
      , (void *) &inet->inet_saddr, ntohs(inet->inet_sport)
      , (void *) &inet->inet_daddr, ntohs(inet->inet_dport)
      , TCP_SKB_CB(skb)->seq, tp->rcv_nxt
      , old_write_seq, tp->write_seq
//      , old_end_seq, TCP_SKB_CB(skb)->end_seq
      , tp->snd_nxt, tp->rcv_nxt, tp->snd_una
#ifdef CONFIG_XEN_SME
      , tp->shared_seq, tp->gen_out, tp->xmit_out
#endif
      , tcp_skb_pcount(skb)
      , tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup, tp->snd_cwnd
      , tcp_packets_in_flight(tp)
      , skb_availroom(skb), skb->tail, skb->end, skb_tailroom(skb)
      , skb->reserved_tailroom, TCP_SKB_CB(skb)->eor
      , sk->sk_wmem_queued, atomic_read(&sk->sk_wmem_alloc), sk->sk_forward_alloc
      , sk->sk_sndbuf, TCP_SKB_CB(skb)->tcp_flags
      );

#if 0
  {
    sme_channel_map_t *scm = NULL;
    profile_conn_map_t *pcm = NULL;
    profq_elem_t *prof_p = NULL;
    int ret = 0;
    largef_elem_t e;
    int dbg_idx = 0;
    ret = get_profile_conn_info_from_skb_header(s_addr, d_addr, s_port, d_port,
        &scm_list[0], &scm, &pcm, 0, 0, 0, NULL);

    if (ret < 0 || !scm || !pcm)
      return 0;

    prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
    memset((void *) &e, 0, sizeof(largef_elem_t));
    e.ts = get_current_time(SCALE_NS);
    e.snd_nxt = tp->snd_nxt;
    e.snd_una = tp->snd_una;
    e.rcv_nxt = tp->rcv_nxt;
    e.packets_out = tp->packets_out;
    e.lost_out = tp->lost_out;
    e.sacked_out = tp->sacked_out;
    e.retrans_out = tp->retrans_out;
    e.cwnd = tp->snd_cwnd;
    e.snd_wnd = tp->snd_wnd;
    e.rcv_wnd = tp->rcv_wnd;
    e.icsk_rto = inet_csk(sk)->icsk_rto;
    e.cwm = prof_p->cwnd_watermark;
    e.tail = prof_p->tail;
    e.real = atomic_read(&prof_p->real_counter);
    e.dummy = prof_p->dummy_counter;
    e.num_timers = prof_p->num_timers;
    e.nxt = prof_p->next_timer_idx;
    e.pidx = prof_p->profq_idx;
    e.ca_state = inet_csk(sk)->icsk_ca_state;
    e.caller = 6;
    e.coreid = smp_processor_id();
    e.owner = sk->sk_lock.owned;
    dbg_idx = d_port % STATQ_MODULO;
    if (generic_q_empty(largef_q[dbg_idx]))
      largef_q[dbg_idx]->port = d_port;
    put_generic_q(largef_q[dbg_idx], (void *) &e);
  }
#endif
  return 0;
}

#ifdef CONFIG_XEN_SME
void
do_read_sync(struct sock *sk, struct sk_buff *skb)
{
  struct tcp_sock *tp = tcp_sk(sk);
  int curr_dummy_out = 0;
  int curr_packets_out = 0;
  int old_write_seq = 0;
  int new_shared_seq = 0;
  int ret = 0;
  int old_pkts, new_pkts, old_max_pkts, new_max_pkts;

  // sync snd_nxt with write_seq
  old_write_seq = tp->write_seq;
  new_shared_seq = tp->shared_seq;
  tp->write_seq = new_shared_seq;

  tp->snd_nxt = tp->write_seq;

  // sync dummy_out with packets_out
  do {
    curr_dummy_out = tp->shared_dummy_out;
    ret = cmpxchg(&tp->shared_dummy_out, curr_dummy_out, 0);
  } while (ret != curr_dummy_out);

  old_pkts = tp->packets_out;
  old_max_pkts = tp->max_packets_out;

  do {
    curr_packets_out = tp->packets_out;
    ret = cmpxchg(&tp->packets_out, curr_packets_out,
        curr_packets_out + curr_dummy_out);
  } while (ret != curr_packets_out);
//  tp->packets_out += curr_dummy_out;
  tp->segs_out += curr_dummy_out;
  tp->data_segs_out += curr_dummy_out;
  tp->max_packets_out = max_t(u32, tp->packets_out, tp->max_packets_out);

  new_pkts = tp->packets_out;
  new_max_pkts = tp->max_packets_out;

  // TODO: fix lsndtime sync
  // sync dummy_lsndtime with lsndtime
//  tp->lsndtime = max_t(uint64_t, tp->lsndtime, tp->shared_dummy_lsndtime);

#if SME_DEBUG_LVL <= LVL_INFO
  if (skb) {
    struct inet_sock *inet = inet_sk(sk);
    iprint(LVL_DBG, "%pS READ len %d datalen %d\n"
      "truesize %d state %d CA %d retrans %d sack_out %d lost %d pkts %d => %d"
      " dummy %d => %d\nPORT %u (%u:%u) pcount %d "
      "snd_nxt %u rcv_nxt %u snd_una %u write_seq %u => %u shared_seq %u => %u\n"
      "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %u inflight %d "
      " max_pkts_out %d => %d\n"
      , __builtin_return_address(3)
      , skb->len, skb->data_len, skb->truesize
      , sk->sk_state, inet_csk(sk)->icsk_ca_state
      , tp->retrans_out, tp->sacked_out, tp->lost_out
      , old_pkts, new_pkts, curr_dummy_out, tp->shared_dummy_out
      , ntohs(inet->inet_dport)
      , TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->ack_seq, tcp_skb_pcount(skb)
      , tp->snd_nxt, tp->rcv_nxt, tp->snd_una
      , old_write_seq, tp->write_seq, new_shared_seq, tp->shared_seq
      , tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup
      , tp->snd_cwnd, tcp_packets_in_flight(tp), old_max_pkts, new_max_pkts
      );
  } else {
    struct inet_sock *inet = inet_sk(sk);
    iprint(LVL_INFO, "%pS READ len %d datalen %d\n"
      "truesize %d state %d CA %d retrans %d sack_out %d lost %d pkts %d => %d "
      "dummy %d => %d\nPORT %u "
      "snd_nxt %u rcv_nxt %u snd_una %u write_seq %u => %u shared_seq %u => %u"
      " gen %llu xmit %llu\n"
      "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %u inflight %d "
      "max_pkts_out %d => %d\n"
      , __builtin_return_address(3)
      , -1, -1, -1
      , sk->sk_state, inet_csk(sk)->icsk_ca_state
      , tp->retrans_out, tp->sacked_out, tp->lost_out
      , old_pkts, new_pkts
      , curr_dummy_out, tp->shared_dummy_out
      , ntohs(inet->inet_dport)
      , tp->snd_nxt, tp->rcv_nxt, tp->snd_una
      , old_write_seq, tp->write_seq, new_shared_seq, tp->shared_seq
      , tp->gen_out, tp->xmit_out
      , tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup
      , tp->snd_cwnd, tcp_packets_in_flight(tp), old_max_pkts, new_max_pkts
      );
  }
#endif
}

void
do_write_sync(struct sock *sk, struct sk_buff *skb, int write)
{
  struct inet_sock *inet = inet_sk(sk);
  struct tcp_sock *tp = tcp_sk(sk);
  int curr_dummy_out = 0;
  int curr_packets_out = 0;
  int curr_seq = 0;
  int old_write_seq = 0;
  int old_shared_seq = 0;
  int new_shared_seq = 0;
  int ret = 0;
  int old_pkts, new_pkts, old_max_pkts, new_max_pkts;

  // sync snd_nxt with write_seq
  old_write_seq = tp->write_seq;
  old_shared_seq = tp->shared_seq;

  // CAS on shared_seq and return old value of shared_seq
  do {
    curr_seq = tp->shared_seq;
    ret = cmpxchg(&tp->shared_seq, curr_seq, curr_seq + write);
  } while (ret != curr_seq);

  new_shared_seq = curr_seq;
  tp->write_seq = new_shared_seq;

  if (skb) {
    TCP_SKB_CB(skb)->seq = TCP_SKB_CB(skb)->end_seq = new_shared_seq;
    TCP_SKB_CB(skb)->tx_padding = 0;
  }

  do {
    curr_dummy_out = tp->shared_dummy_out;
    ret = cmpxchg(&tp->shared_dummy_out, curr_dummy_out, 0);
  } while (ret != curr_dummy_out);

  old_pkts = tp->packets_out;
  old_max_pkts = tp->max_packets_out;

  do {
    curr_packets_out = tp->packets_out;
    ret = cmpxchg(&tp->packets_out, curr_packets_out,
        curr_packets_out + curr_dummy_out);
  } while (ret != curr_packets_out);
  tp->segs_out += curr_dummy_out;
  tp->data_segs_out += curr_dummy_out;
  tp->max_packets_out = max_t(u32, tp->packets_out, tp->max_packets_out);

  new_pkts = tp->packets_out;
  new_max_pkts = tp->max_packets_out;

  // TODO: fix lsndtime sync
//  tp->lsndtime = max_t(uint64_t, tp->lsndtime, tp->shared_dummy_lsndtime);
  if (skb) {
    iprint(LVL_DBG, "%pS write %d len %d datalen %d\n"
      "truesize %d state %d CA %d retrans %d sack_out %d lost %d pkts %d => %d"
      " dummy %d snd_head %p\nPORT %u (%u:%u) pcount %d skb %p rcv_nxt %u\n"
      "snd_nxt %u snd_una %u write_seq %u => %u shared_seq %u => %u"
      " gen %llu xmit %llu\n"
      "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %u inflight"
      " %d max_pkts_out %d => %d\n"
      , __builtin_return_address(3)
      , write, skb->len, skb->data_len, skb->truesize
      , sk->sk_state, inet_csk(sk)->icsk_ca_state
      , tp->retrans_out, tp->sacked_out, tp->lost_out
      , old_pkts, new_pkts, tp->shared_dummy_out, tcp_send_head(sk)
      , ntohs(inet->inet_dport)
      , tp->write_seq, tp->rcv_nxt, tcp_skb_pcount(skb), skb, tp->rcv_nxt
      , tp->snd_nxt, tp->snd_una
      , old_write_seq, tp->write_seq, old_shared_seq, tp->shared_seq
      , tp->gen_out, tp->xmit_out, tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup
      , tp->snd_cwnd, tcp_packets_in_flight(tp), old_max_pkts, new_max_pkts
      );
  }
}

void
do_read_sync2(struct sock *sk, struct sk_buff *skb)
{
  struct inet_sock *inet = inet_sk(sk);
  struct tcp_sock *tp = tcp_sk(sk);
  int curr_dummy_out = 0;
  int curr_packets_out = 0;
  int old_write_seq = 0;
  int new_shared_seq = 0;
  int ret = 0;
  int old_pkts, new_pkts, old_max_pkts, new_max_pkts;

  // sync snd_nxt with write_seq
  old_write_seq = tp->snd_nxt;
  new_shared_seq = tp->shared_seq;
  tp->snd_nxt = new_shared_seq;

  // sync dummy_out with packets_out
  do {
    curr_dummy_out = tp->shared_dummy_out;
    ret = cmpxchg(&tp->shared_dummy_out, curr_dummy_out, 0);
  } while (ret != curr_dummy_out);

  old_pkts = tp->packets_out;
  old_max_pkts = tp->max_packets_out;

  do {
    curr_packets_out = tp->packets_out;
    ret = cmpxchg(&tp->packets_out, curr_packets_out,
        curr_packets_out + curr_dummy_out);
  } while (ret != curr_packets_out);
  tp->segs_out += curr_dummy_out;
  tp->data_segs_out += curr_dummy_out;
  tp->max_packets_out = max_t(u32, tp->packets_out, tp->max_packets_out);

  new_pkts = tp->packets_out;
  new_max_pkts = tp->max_packets_out;

  // TODO: fix lsndtime sync
  // sync dummy_lsndtime with lsndtime
//  tp->lsndtime = max_t(uint64_t, tp->lsndtime, tp->shared_dummy_lsndtime);

  if (skb) {
    iprint(LVL_DBG, "%pS READ len %d datalen %d\n"
      "truesize %d state %d CA %d retrans %d sack_out %d lost %d pkts %d => %d"
      " dummy %d => %d\nPORT %u (%u:%u) pcount %d "
      "snd_nxt %u => %u rcv_nxt %u snd_una %u write_seq %u shared_seq %u => %u\n"
      "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %u inflight %d "
      " max_pkts_out %d => %d\n"
      , __builtin_return_address(3)
      , skb->len, skb->data_len, skb->truesize
      , sk->sk_state, inet_csk(sk)->icsk_ca_state
      , tp->retrans_out, tp->sacked_out, tp->lost_out
      , old_pkts, new_pkts, curr_dummy_out, tp->shared_dummy_out
      , ntohs(inet->inet_dport)
      , TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->ack_seq, tcp_skb_pcount(skb)
      , old_write_seq, tp->snd_nxt, tp->rcv_nxt, tp->snd_una
      , tp->write_seq, new_shared_seq, tp->shared_seq
      , tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup
      , tp->snd_cwnd, tcp_packets_in_flight(tp), old_max_pkts, new_max_pkts
      );
  } else {
    iprint(LVL_INFO, "%pS READ len %d datalen %d\n"
      "truesize %d state %d CA %d retrans %d sack_out %d lost %d pkts %d => %d "
      "dummy %d => %d\nPORT %u "
      "snd_nxt %u rcv_nxt %u snd_una %u write_seq %u => %u shared_seq %u => %u"
      " gen %llu xmit %llu\n"
      "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %u inflight %d "
      "max_pkts_out %d => %d\n"
      "wnd_end %u before(wnd_end, seq_nxt) %d"
      , __builtin_return_address(3)
      , -1, -1, -1
      , sk->sk_state, inet_csk(sk)->icsk_ca_state
      , tp->retrans_out, tp->sacked_out, tp->lost_out
      , old_pkts, new_pkts
      , curr_dummy_out, tp->shared_dummy_out
      , ntohs(inet->inet_dport)
      , tp->snd_nxt, tp->rcv_nxt, tp->snd_una
      , old_write_seq, tp->write_seq, new_shared_seq, tp->shared_seq
      , tp->gen_out, tp->xmit_out
      , tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup
      , tp->snd_cwnd, tcp_packets_in_flight(tp), old_max_pkts, new_max_pkts
      , tcp_wnd_end(tp), before(tcp_wnd_end(tp), tp->snd_nxt)
      );
  }
}

void
do_write_sync2(struct sock *sk, struct sk_buff *skb, int write)
{
  struct inet_sock *inet = inet_sk(sk);
  struct tcp_sock *tp = tcp_sk(sk);
  int curr_dummy_out = 0;
  int curr_packets_out = 0;
  int curr_seq = 0;
  int old_write_seq = 0;
  int old_shared_seq = 0;
  int new_shared_seq = 0;
  int ret = 0;
  int old_pkts, new_pkts, old_max_pkts, new_max_pkts;

  // sync snd_nxt with write_seq
  old_write_seq = tp->write_seq;
  old_shared_seq = tp->shared_seq;

  // CAS on shared_seq and return old value of shared_seq
  do {
    curr_seq = tp->shared_seq;
    ret = cmpxchg(&tp->shared_seq, curr_seq, curr_seq + write);
  } while (ret != curr_seq);

  new_shared_seq = curr_seq;
  tp->snd_nxt = new_shared_seq;

  if (skb) {
    TCP_SKB_CB(skb)->seq = new_shared_seq;
    TCP_SKB_CB(skb)->end_seq = new_shared_seq + write;
  }

  do {
    curr_dummy_out = tp->shared_dummy_out;
    ret = cmpxchg(&tp->shared_dummy_out, curr_dummy_out, 0);
  } while (ret != curr_dummy_out);

  old_pkts = tp->packets_out;
  old_max_pkts = tp->max_packets_out;

  do {
    curr_packets_out = tp->packets_out;
    ret = cmpxchg(&tp->packets_out, curr_packets_out,
        curr_packets_out + curr_dummy_out);
  } while (ret != curr_packets_out);
  tp->segs_out += curr_dummy_out;
  tp->data_segs_out += curr_dummy_out;
  tp->max_packets_out = max_t(u32, tp->packets_out, tp->max_packets_out);

  new_pkts = tp->packets_out;
  new_max_pkts = tp->max_packets_out;

  // TODO: fix lsndtime sync
//  tp->lsndtime = max_t(uint64_t, tp->lsndtime, tp->shared_dummy_lsndtime);
  if (skb) {
    iprint(LVL_INFO, "%pS write %d len %d datalen %d\n"
      "truesize %d state %d CA %d retrans %d sack_out %d lost %d pkts %d => %d"
      " dummy %d snd_head %p\nPORT %u (%u:%u) pcount %d skb %p rcv_nxt %u\n"
      "snd_nxt %u snd_una %u write_seq %u => %u shared_seq %u => %u"
      " gen %llu xmit %llu\n"
      "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %u inflight"
      " %d max_pkts_out %d => %d\n"
      , __builtin_return_address(3)
      , write, skb->len, skb->data_len, skb->truesize
      , sk->sk_state, inet_csk(sk)->icsk_ca_state
      , tp->retrans_out, tp->sacked_out, tp->lost_out
      , old_pkts, new_pkts, tp->shared_dummy_out, tcp_send_head(sk)
      , ntohs(inet->inet_dport)
      , tp->write_seq, tp->rcv_nxt, tcp_skb_pcount(skb), skb, tp->rcv_nxt
      , tp->snd_nxt, tp->snd_una
      , old_write_seq, tp->write_seq, old_shared_seq, tp->shared_seq
      , tp->gen_out, tp->xmit_out
      , tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup
      , tp->snd_cwnd, tcp_packets_in_flight(tp), old_max_pkts, new_max_pkts
      );
  }
}

/*
 * adjust tcp_sock seq# vars (write_seq, snd_nxt) here.
 * return 0 on success.
 */
int
mod_rxtx_sync_shared_seq(struct sock *sk, struct sk_buff *skb, int write)
{
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint16_t s_port = 0, d_port = 0;

  if (!sk)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  // no-op for unpaced flows
  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  iprint(LVL_DBG, "%pS %pS write %d\n"
      "src %pI4 %u, dst %pI4 %u (%u:%u) shared_seq %u pkts_out %d dummy_out %d\n"
      "delack timeout %lu ack pending %0x ato %d snd_una %u sock_owned %d "
      "sk_state %d snd_nxt %u\n"
      "snd_head %p tail %p head %p prequeue len %d"
      , __builtin_return_address(2), __builtin_return_address(3), write
      , (void *) &inet->inet_saddr, ntohs(inet->inet_sport)
      , (void *) &inet->inet_daddr, ntohs(inet->inet_dport)
      , tp->write_seq, tp->rcv_nxt, tp->shared_seq, tp->packets_out
      , tp->shared_dummy_out
      , inet_csk(sk)->icsk_ack.timeout, inet_csk(sk)->icsk_ack.pending
      , inet_csk(sk)->icsk_ack.ato, tp->snd_una, sock_owned_by_user(sk)
      , sk->sk_state, tp->snd_nxt
      , tcp_send_head(sk), tcp_write_queue_tail(sk), tcp_write_queue_head(sk)
      , skb_queue_len(&tp->ucopy.prequeue)
      );

#if !defined(CONFIG_XEN_PACER) || !defined(CONFIG_XEN_PACER_DB)
  return -EINVAL;
#endif

  if (write) {
#if SME_CONFIG == SME_DBG_LOG
    TCP_SKB_CB(skb)->seq = tp->write_seq;
    TCP_SKB_CB(skb)->end_seq = tp->write_seq;
    tp->write_seq = tp->write_seq;
    return 0;
#else
#if CONFIG_SYNC_SNDNXT
    do_write_sync2(sk, skb, write);
#else
    do_write_sync(sk, skb, write);
#endif
    return 0;
#endif
  }

#if SME_CONFIG == SME_EXP
#if CONFIG_SYNC_SNDNXT
  do_read_sync2(sk, skb);
#else
  // case: write = 0, here we also want to sync snd_nxt to write_seq
  do_read_sync(sk, skb);
#endif
#endif

  return 0;
}
#endif /* CONFIG_XEN_SME */

int
mod_tx_adjust_urg(struct sock *sk, struct sk_buff *skb)
{
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  struct tcphdr *th = NULL;
#if SME_CONFIG == SME_EXP
  int old_len = 0, old_data_len = 0, delta = 0, old_tailroom = 0;
  int old_availroom = 0;
#endif

  if (!sk || !skb)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  th = (struct tcphdr *) skb->data;
#if SME_CONFIG == SME_DBG_LOG
  iprint(LVL_INFO, "%pS %pS %pS\n"
      "PORT %u [%d %d %d %d %d %d %d %d] (%u:%u) len %d\n"
      "snd_nxt %u snd_una %u write_seq %u\n"
      "availroom %d tail %d end %d tailroom %d restail %d data %p\n"
      "cwnd %u pkts %u lost %u retrans %u sacked %u CA %d sstart %d "
      "timeout %lu currtime %lu RTO %u"
      , __builtin_return_address(3), __builtin_return_address(4)
      , __builtin_return_address(5)
      , ntohs(inet->inet_dport)
      , th->fin, th->syn, th->rst, th->psh, th->ack, th->urg, th->ece, th->cwr
      , ntohl(th->seq), ntohl(th->ack_seq), skb->len
      , tp->snd_nxt, tp->snd_una, tp->write_seq
      , skb_availroom(skb), skb->tail, skb->end, skb_tailroom(skb)
      , skb->reserved_tailroom, skb->data
      , tp->snd_cwnd, tp->packets_out, tp->lost_out, tp->retrans_out
      , tp->sacked_out, inet_csk(sk)->icsk_ca_state, tcp_in_slow_start(tp)
      , inet_csk(sk)->icsk_timeout, jiffies, inet_csk(sk)->icsk_rto
      );

  return -EINVAL;
#else
  // do not pad control packets
  if ((TCP_SKB_CB(skb)->end_seq == TCP_SKB_CB(skb)->seq)
      || th->syn || th->fin || th->rst)
    return -EINVAL;

  old_data_len = SME_TCP_MTU - TCP_SKB_CB(skb)->tx_padding;
  old_len = skb->len;
  delta = TCP_SKB_CB(skb)->tx_padding;
  old_availroom = skb_availroom(skb);
  old_tailroom = skb_tailroom(skb);
  if ((skb->tail + delta > skb->end) || skb_is_nonlinear(skb)
      || old_tailroom < delta) {
    iprint(LVL_EXP, "%pS BUG!!! tail %d end %d data_len %d\n"
        "[%pI4 %u, %pI4 %u] (%u:%u) urg ptr %d delta %d len %d skb %p"
        , __builtin_return_address(5), skb->tail, skb->end, skb->data_len
        , (void *) &inet->inet_saddr, ntohs(inet->inet_sport)
        , (void *) &inet->inet_daddr, ntohs(inet->inet_dport)
        , ntohl(th->seq), ntohl(th->ack_seq), ntohs(th->urg_ptr), delta
        , skb->len, skb
        );
  } else {
    th->urg = 1;
    th->urg_ptr = htons(old_data_len);
    skb_put(skb, delta);
  }
  iprint(LVL_DBG, "%pS\n%pS %pS sacked %d\nskb %p state %d CA %d "
    "retrans %d sack_out %d fackets %d lost %d pkts %d\n"
    "PORT %u [%d %d %d %d %d %d %d %d] (%u:%u) urg ptr %d len %d delta %d\n"
//    "availroom %d => %d tail %d end %d tailroom %d => %d restail %d data %p "
//    "len %d %d => %d\n"
    "snd_nxt %u snd_una %u write_seq %u rcv_nxt %u sstart %d"
#if defined(CONFIG_XEN_SME)
    " gen %llu xmit %llu"
#endif
    "\n"
    "snd_wnd %u rcv_wnd %u rcv_wup %u snd_cwnd %u inflight %d\n"
    "wmem_queued %d wmem_alloc %d fwd_alloc %d sndbuf %d"
    , __builtin_return_address(5), __builtin_return_address(6)
    , __builtin_return_address(7)
    , TCP_SKB_CB(skb)->sacked, skb
    , sk->sk_state, inet_csk(sk)->icsk_ca_state
    , tp->retrans_out, tp->sacked_out, tp->fackets_out
    , tp->lost_out, tp->packets_out
    , ntohs(inet->inet_dport)
    , th->fin, th->syn, th->rst, th->psh, th->ack, th->urg, th->ece, th->cwr
    , ntohl(th->seq), ntohl(th->ack_seq), ntohs(th->urg_ptr), skb->len, delta
//    , old_availroom, skb_availroom(skb), skb->tail, skb->end
//    , old_tailroom, skb_tailroom(skb)
//    , skb->reserved_tailroom, skb->data, old_data_len, old_len, skb->len
    , tp->snd_nxt, tp->snd_una, tp->write_seq, tp->rcv_nxt
    , tcp_in_slow_start(tp)
#if defined(CONFIG_XEN_SME)
    , tp->gen_out, tp->xmit_out
#endif
    , tp->snd_wnd, tp->rcv_wnd, tp->rcv_wup
    , tp->snd_cwnd, tcp_packets_in_flight(tp)
    , sk->sk_wmem_queued, atomic_read(&sk->sk_wmem_alloc), sk->sk_forward_alloc
    , sk->sk_sndbuf
    );

  return 0;
#endif /* SME_EXP */
}

int
mod_is_paced_flow(struct sock *sk)
{
#if SME_CONFIG == SME_DBG_LOG
  return -EINVAL;
#else
  struct inet_sock *inet = NULL;
  uint16_t s_port = 0, d_port = 0;

  if (!sk)
    return -EINVAL;

  inet = inet_sk(sk);
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  return 0;
#endif
}

#ifdef CONFIG_XEN_SME
int
mod_tx_incr_tail(struct sock *sk, struct sk_buff *skb)
{
#if SME_CONFIG == SME_DBG_LOG
  return -EINVAL;
#else
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  struct tcphdr *th = NULL;
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  sme_channel_map_t *scm = NULL;
  profile_conn_map_t *pcm = NULL;
  int ret = 0;
  int old_tail = 0, new_tail = 0;
  profq_elem_t *pelem = NULL;
#endif

  if (!sk || !skb)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  th = (struct tcphdr *) skb->data;
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  ret = get_profile_conn_info_from_skb_header(s_addr, d_addr, s_port, d_port,
      &scm_list[0], &scm, &pcm, 0, 0, 0, NULL);

  if (ret < 0 || !scm || !pcm)
    return -ENOENT;

  pelem = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
  old_tail = pelem->tail;
  pelem->tail += tcp_skb_pcount(skb);
  new_tail = pelem->tail;
#endif

  iprint(LVL_DBG, "TAIL retrans %d sack_out %d lost %d pkts %d\n"
    "[%pI4 %u, %pI4 %u] [%u %u %u %u %u %u %u %u] (%u:%u) urg %u\n"
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
    "G%lld state %d paused %d CWM %d NXT %d P %d T %d => %d R %d D %d"
#endif
    , tp->retrans_out, tp->sacked_out, tp->lost_out, tp->packets_out
    , (void *) &s_addr, s_port, (void *) &d_addr, d_port
    , th->fin, th->syn, th->rst, th->psh, th->ack, th->urg, th->ece, th->cwr
    , ntohl(th->seq), ntohl(th->ack_seq), ntohs(th->urg_ptr)
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
    , pcm->profq_idx, pelem->state, atomic_read(&pelem->is_paused)
    , pelem->cwnd_watermark, pelem->next_timer_idx, pelem->num_timers
    , old_tail, new_tail, atomic_read(&pelem->real_counter)
    , pelem->dummy_counter
#endif
    );
  return 0;
#endif
}

int
mod_tx_incr_profile(struct sock *sk, struct sk_buff *skb)
{
#if SME_CONFIG == SME_DBG_LOG
  return -EINVAL;
#else
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  sme_channel_map_t *scm = NULL;
  profile_conn_map_t *pcm = NULL;
  profq_elem_t *pelem = NULL;
  prof_update_arg_t parg;
  int old_prof = 0;
#endif
  int ret = 0;

  if (!sk || !skb)
    return -EINVAL;

  tp = tcp_sk(sk);
  inet = inet_sk(sk);

  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  ret = get_profile_conn_info_from_skb_header(s_addr, d_addr, s_port, d_port,
      &scm_list[0], &scm, &pcm, 0, 0, 0, NULL);

  if (ret < 0 || !scm || !pcm)
    return -ENOENT;

  /*
   * we already added a slot for the retransmission pkt
   * in tcp_enter_loss, when N == CWM == P.
   */
  if (pcm->num_loss_extn > 0)
    return -EBUSY;

  pcm->num_loss_extn--;

  pelem = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
  old_prof = pelem->num_timers;

  parg.profq_idx = pcm->profq_idx;
  parg.hypace_idx = pcm->hypace_idx;
  parg.cmd_type = P_EXTEND_PROF_RETRANS;
  ret = HYPERVISOR_sme_pacer(PACER_OP_PROF_UPDATE, &parg);

#if 0
  {
    largef_elem_t e;
    int dbg_idx = 0;
    memset((void *) &e, 0, sizeof(largef_elem_t));
    e.ts = get_current_time(SCALE_NS);
    e.snd_nxt = tp->snd_nxt;
    e.snd_una = tp->snd_una;
    e.rcv_nxt = tp->rcv_nxt;
    e.packets_out = tp->packets_out;
    e.lost_out = tp->lost_out;
    e.sacked_out = tp->sacked_out;
    e.retrans_out = tp->retrans_out;
    e.cwnd = tp->snd_cwnd;
    e.snd_wnd = tp->snd_wnd;
    e.rcv_wnd = tp->rcv_wnd;
    e.icsk_rto = inet_csk(sk)->icsk_rto;
    e.cwm = pelem->cwnd_watermark;
    e.tail = pelem->tail;
    e.real = atomic_read(&pelem->real_counter);
    e.dummy = pelem->dummy_counter;
    e.num_timers = pelem->num_timers;
    e.nxt = pelem->next_timer_idx;
    e.pidx = pelem->profq_idx;
    e.ca_state = inet_csk(sk)->icsk_ca_state;
    e.caller = 11;
    e.coreid = smp_processor_id();
    e.owner = sk->sk_lock.owned;
    e.undo_marker = tp->undo_marker;
    e.undo_retrans = tp->undo_retrans;
    e.high_seq = tp->high_seq;
    dbg_idx = d_port % STATQ_MODULO;
    if (generic_q_empty(largef_q[dbg_idx]))
      largef_q[dbg_idx]->port = d_port;
    put_generic_q(largef_q[dbg_idx], (void *) &e);
  }
#endif

#endif

//  BUG_ON(TCP_SKB_CB(skb)->tx_padding == 0 && skb->len == 0);

  if (TCP_SKB_CB(skb)->tx_padding == 0 && skb->len == 0) {
    iprint(LVL_EXP, "RETRANS %pS ret %d\n[%pI4 %u, %pI4 %u] "
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
      "G%lld state %d paused %d CWM %d NXT %d P %d => %d T %d R %d D %d\n"
#endif
      "sk state %d len %d padding %d flags %0x seq (%u:%u) sacked 0x%0x"
      , __builtin_return_address(4), ret
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
      , pcm->profq_idx, pelem->state, atomic_read(&pelem->is_paused)
      , pelem->cwnd_watermark, pelem->next_timer_idx
      , old_prof, pelem->num_timers, pelem->tail
      , atomic_read(&pelem->real_counter), pelem->dummy_counter
#endif
      , sk->sk_state, skb->len, TCP_SKB_CB(skb)->tx_padding
      , TCP_SKB_CB(skb)->tcp_flags, TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->ack_seq
      , TCP_SKB_CB(skb)->sacked
      );
  }
  return 0;
#endif
}

void
mod_timer_rto_check(struct sock *sk)
{
#if SME_CONFIG == SME_DBG_LOG
  return;
#else
  struct tcp_sock *tp = NULL;
  struct inet_sock *inet = NULL;
  struct inet_connection_sock *icsk = NULL;
  uint16_t s_port = 0, d_port = 0;
  uint32_t s_addr = 0, d_addr = 0;
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  sme_channel_map_t *scm = NULL;
  profile_conn_map_t *pcm = NULL;
  profq_elem_t *pelem = NULL;
  int ret = 0;
  int old_pending;
  unsigned long old_to;
  u64 now64 = 0;
  u32 now32 = 0;
#endif

  if (!sk)
    return;

  inet = inet_sk(sk);
  tp = tcp_sk(sk);
  icsk = inet_csk(sk);
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);
  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;

  if (!check_relevant_ports(s_port, d_port))
    return;

  /*
   * socket initialization, ignore RTO handling for now.
   */
  if (sk->sk_state == TCP_SYN_RECV) {
    sk_reset_timer(sk, &sk->sk_rto_timer, RTO_CHECK_TIMER);
    return;
  }

#if !CONFIG_XEN_PACER || !CONFIG_XEN_PACER_DB
  // no point in playing this function if no XEN_PACER code enabled
  return;
#else
  ret = get_profile_conn_info_from_skb_header(s_addr, d_addr, s_port, d_port,
      &scm_list[0], &scm, &pcm, 0, 0, 0, NULL);
  if (ret < 0 || !scm || !pcm) {
    iprint(LVL_DBG, "[%pI4 %u, %pI4 %u] tcp state %d ret %d\n"
      "lsndtime %u rcvtstamp %u pending %d timeout %lu"
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port, sk->sk_state, ret
      , tp->lsndtime, tp->rcv_tstamp
      , icsk->icsk_pending, icsk->icsk_timeout);
    bh_lock_sock(sk);
    sk_reset_timer(sk, &sk->sk_rto_timer, RTO_CHECK_TIMER);
    bh_unlock_sock(sk);
    sock_put(sk);
    return;
  }

  pelem = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];

  bh_lock_sock(sk);
  /*
   * set RTO if there are outstanding (dummy) packets, and tcp has not set an RTO
   */
  old_pending = icsk->icsk_pending;
  old_to = icsk->icsk_timeout;
  now64 = jiffies;
  now32 = (__u32) now64;
  if (!(tp->snd_una >= tp->snd_nxt /* no outstanding unacked pkts */
      || icsk->icsk_pending == ICSK_TIME_RETRANS /* retransmit timer event */)) {
    if (!sock_owned_by_user(sk)) {
      /*
       * XXX: ideally, TCP's RTO timers need to be aligned with the hypace schedule.
       */
      inet_csk_reset_xmit_timer(sk, ICSK_TIME_RETRANS, icsk->icsk_rto, TCP_RTO_MAX);
#if SME_DEBUG_LVL <= LVL_INFO
      iprint(LVL_INFO, "lsndtime %u rcvtstamp %u\n"
        "pending %d => %d timeout %lu => %lu tcpstate %d RTO %u init RTO %u HZ %d\n"
        "[%pI4 %u, %pI4 %u] "
        "G%lld state %d paused %d CWM %d NXT %d P %d T %d R %d D %d "
        "pcm segs_out %d\n"
        "delivered %d segs_out %d data_segs_out %d segs_in %d data_segs_in %d\n"
        "snd_cwnd %d snd_una %u snd_nxt %u write_seq %u inflight %d\n"
        "jif %llu (u32) %u getns %llu rdtsc %llu"
        , tp->lsndtime, tp->rcv_tstamp
        , old_pending, icsk->icsk_pending, old_to, icsk->icsk_timeout
        , sk->sk_state, icsk->icsk_rto, TCP_TIMEOUT_INIT, HZ
        , (void *) &s_addr, s_port, (void *) &d_addr, d_port
        , pcm->profq_idx, pelem->state, atomic_read(&pelem->is_paused)
        , pelem->cwnd_watermark, pelem->next_timer_idx, pelem->num_timers
        , pelem->tail, atomic_read(&pelem->real_counter), pelem->dummy_counter
        , pcm->tcp_segs_out
        , tp->delivered, tp->segs_out, tp->data_segs_out, tp->segs_in
        , tp->data_segs_in, tp->snd_cwnd, tp->snd_una, tp->snd_nxt, tp->write_seq
        , tcp_packets_in_flight(tp)
        , now64, now32, get_current_time(SCALE_NS), get_current_time(SCALE_RDTSC)
        );
#endif
    } else {
#if SME_DEBUG_LVL <= LVL_INFO
      iprint(LVL_INFO, "BUSY lsndtime %u rcvtstamp %u\n"
        "pending %d => %d timeout %lu => %lu tcpstate %d RTO %u init RTO %u HZ %d\n"
        "[%pI4 %u, %pI4 %u] "
        "G%lld state %d paused %d CWM %d NXT %d P %d T %d R %d D %d "
        "pcm segs_out %d\n"
        "delivered %d segs_out %d data_segs_out %d segs_in %d data_segs_in %d\n"
        "snd_cwnd %d snd_una %u snd_nxt %u write_seq %u inflight %d\n"
        "jif %llu (u32) %u getns %llu rdtsc %llu"
        , tp->lsndtime, tp->rcv_tstamp
        , old_pending, icsk->icsk_pending, old_to, icsk->icsk_timeout
        , sk->sk_state, icsk->icsk_rto, TCP_TIMEOUT_INIT, HZ
        , (void *) &s_addr, s_port, (void *) &d_addr, d_port
        , pcm->profq_idx, pelem->state, atomic_read(&pelem->is_paused)
        , pelem->cwnd_watermark, pelem->next_timer_idx, pelem->num_timers
        , pelem->tail, atomic_read(&pelem->real_counter), pelem->dummy_counter
        , pcm->tcp_segs_out
        , tp->delivered, tp->segs_out, tp->data_segs_out, tp->segs_in
        , tp->data_segs_in, tp->snd_cwnd, tp->snd_una, tp->snd_nxt, tp->write_seq
        , tcp_packets_in_flight(tp)
        , now64, now32, get_current_time(SCALE_NS), get_current_time(SCALE_RDTSC)
        );
#endif
    }
  }
  sk_reset_timer(sk, &sk->sk_rto_timer, RTO_CHECK_TIMER);

  bh_unlock_sock(sk);
  sock_put(sk);
#endif /* CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB */
#endif /* SME_CONFIG */
}

int
mod_get_retransmit_skb(struct sock *sk, struct sk_buff **skb_p)
{
#if SME_CONFIG == SME_DBG_LOG
  return -EINVAL;
#else
  struct inet_sock *inet = NULL;
  struct tcp_sock *tp = NULL;
  uint16_t s_port = 0, d_port = 0;
  uint32_t s_addr = 0, d_addr = 0;
  struct sk_buff *head_skb = NULL;
  struct sk_buff *dummy_skb = NULL;
  struct sk_buff *send_skb = NULL;
  uint32_t curr_seq = 0, end_seq = 0;
  int num_dummies = 0;
  int old_fwd_alloc = 0;

  if (!sk || !skb_p)
    return -EINVAL;

  inet = inet_sk(sk);
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);
  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  tp = tcp_sk(sk);
  head_skb = tcp_write_queue_head(sk);
  send_skb = tcp_send_head(sk);

  curr_seq = tp->snd_una;
  if (head_skb)
    end_seq = TCP_SKB_CB(head_skb)->seq;
  else
    end_seq = tp->snd_nxt;

  num_dummies = (int) (end_seq - curr_seq) / (int) SME_TCP_MTU;
  old_fwd_alloc = sk->sk_forward_alloc;

#if SME_DEBUG_LVL <= LVL_INFO
  if ((end_seq - curr_seq) < 0) {
    iprint(LVL_INFO, "%pS [%pI4 %u, %pI4 %u]\n"
      "wq head %p seq (%u:%u) sacked 0x%0x len %d "
      "snd_cwnd %d pkts %d lost %d retrans %d sack_out %d\n"
      "snd_head %p seq (%u:%u) sacked 0x%0x len %d sk state %d CA %d pending %d\n"
      "write_seq %u shared_seq %u snd_nxt %u snd_una %u rcv_nxt %u\n"
      , __builtin_return_address(4)
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port
      , head_skb
      , (head_skb ? TCP_SKB_CB(head_skb)->seq : -1)
      , (head_skb ? TCP_SKB_CB(head_skb)->ack_seq : -1)
      , (head_skb ? TCP_SKB_CB(head_skb)->sacked : 0)
      , (head_skb ? head_skb->len : -1)
      , tp->snd_cwnd, tp->packets_out, tp->lost_out, tp->retrans_out, tp->sacked_out
      , send_skb
      , (send_skb ? TCP_SKB_CB(send_skb)->seq : -1)
      , (send_skb ? TCP_SKB_CB(send_skb)->ack_seq : -1)
      , (send_skb ? TCP_SKB_CB(send_skb)->sacked : 0)
      , (send_skb ? send_skb->len : -1)
      , sk->sk_state, inet_csk(sk)->icsk_ca_state, inet_csk(sk)->icsk_pending
      , tp->write_seq, tp->shared_seq, tp->snd_nxt, tp->snd_una, tp->rcv_nxt
      );
    iprint(LVL_INFO, "%pS %pS\n"
      "[%pI4 %u, %pI4 %u] seq DELTA %d (%d) write_seq %u shared_seq %u\n"
      "dummy_start %u dummy_end %u fwd_alloc %u wmem_alloc %u"
      " wmem_queued %u rmem_alloc %u\n"
      , __builtin_return_address(3), __builtin_return_address(4)
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port
      , (end_seq - curr_seq), num_dummies, tp->write_seq
      , tp->shared_seq, curr_seq, end_seq
      , sk->sk_forward_alloc, atomic_read(&sk->sk_wmem_alloc)
      , sk->sk_wmem_queued, atomic_read(&sk->sk_rmem_alloc)
      );
  }
#endif

#if !CONFIG_XEN_PACER || !CONFIG_XEN_PACER_DB
  dummy_skb = NULL;
  return -EINVAL;
#else
  // skb at the head of the queue has same seq# as snd_una
  if (head_skb && curr_seq == end_seq) {
    *skb_p = head_skb;
    return 0;
  }

  if (num_dummies <= 0 || ((end_seq - curr_seq) <= 0))
    return -EINVAL;

  build_dummy_tcp_skb(&dummy_skb, sk, TCP_DUMMY_ALLOC, curr_seq, tp->rcv_nxt);
  if (!dummy_skb) {
    iprint(LVL_ERR, "[%pI4 %u, %pI4 %u] dummy seq %u:%u\n"
        "mem pressure %d %d sock pressure %lu fwd_alloc %u wmem_alloc %u "
        "wmem_queued %d"
        , (void *) &s_addr, s_port, (void *) &d_addr, d_port
        , curr_seq, tp->rcv_nxt
        , tcp_under_memory_pressure(sk), sk->sk_memcg->tcpmem_pressure
        , sk->sk_memcg->socket_pressure
        , sk->sk_forward_alloc, atomic_read(&sk->sk_wmem_alloc)
        , sk->sk_wmem_queued
        );

    return -EINVAL;
  }

  // we are only creating a dummy here when they have to be actually retransmitted.
  // before retransmitting, tcp_enter_loss would have marked the dummy LOST if it existed.
  TCP_SKB_CB(dummy_skb)->sacked |= TCPCB_LOST;
  __skb_header_release(dummy_skb);
  __tcp_add_write_queue_head(sk, dummy_skb);
  sk->sk_wmem_queued += dummy_skb->truesize;
  sk_mem_charge(sk, dummy_skb->truesize);
  *skb_p = dummy_skb;

  iprint(LVL_INFO, "%pS PORT %u skb %p\ndummy seq (%u:%u) truesize %d "
      "snd_nxt %u snd_una %u write_seq %u shared_seq %u\n"
      "wmem_queued %d wmem_alloc %d fwd_alloc %d => %d wq head %p snd_head %p"
      , __builtin_return_address(3), d_port, dummy_skb
      , curr_seq, tp->rcv_nxt, dummy_skb->truesize
      , tp->snd_nxt, tp->snd_una, tp->write_seq, tp->shared_seq
      , sk->sk_wmem_queued, atomic_read(&sk->sk_wmem_alloc)
      , old_fwd_alloc, sk->sk_forward_alloc
      , tcp_write_queue_head(sk), tcp_send_head(sk)
      );

  return 0;
#endif /* CONFIG_XEN_PACER && CONFIG_XEN_PACER_DBG */
#endif /* SME_CONFIG */
}

int
mod_build_rexmit_dummy(struct sock *sk, struct sk_buff **skb_p,
    uint32_t seqno, uint32_t seqack, int sacked_flags)
{
#if SME_CONFIG == SME_DBG_LOG
  return -EINVAL;
#else
  struct inet_sock *inet = NULL;
  struct tcp_sock *tp = NULL;
  uint16_t s_port = 0, d_port = 0;
  uint32_t s_addr = 0, d_addr = 0;
  struct sk_buff *dummy_skb = NULL;
  int old_fwd_alloc = 0;

  if (!sk || !skb_p)
    return -EINVAL;

  inet = inet_sk(sk);
  s_port = ntohs(inet->inet_sport);
  d_port = ntohs(inet->inet_dport);
  s_addr = inet->inet_saddr;
  d_addr = inet->inet_daddr;

  if (!check_relevant_ports(s_port, d_port))
    return -EINVAL;

  tp = tcp_sk(sk);

#if !CONFIG_XEN_PACER || !CONFIG_XEN_PACER_DB
  iprint(LVL_EXP, "%pS %pS\n"
      "[%pI4 %u, %pI4 %u] snd_una %u snd_nxt %u write_seq %u shared_seq %u"
      " rcv_nxt %u copied_seq %u\n"
      "wmem_queued %d wmem_alloc %d fwd_alloc %d sndbuf %d snd_ssthresh %d\n"
      "rmem_alloc %d rcv queue len %d rcvbuf %d rcv_ssthresh %d"
      , __builtin_return_address(3), __builtin_return_address(4)
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port
      , tp->snd_una, tp->snd_nxt, tp->write_seq, tp->shared_seq
      , tp->rcv_nxt, tp->copied_seq
      , sk->sk_wmem_queued, atomic_read(&sk->sk_wmem_alloc), sk->sk_forward_alloc
      , sk->sk_sndbuf, tp->snd_ssthresh
      , atomic_read(&sk->sk_rmem_alloc), skb_queue_len(&sk->sk_receive_queue)
      , sk->sk_rcvbuf, tp->rcv_ssthresh
      );
  dummy_skb = NULL;
  old_fwd_alloc = 0;
  return -EINVAL;
#else

  old_fwd_alloc = sk->sk_forward_alloc;

  build_dummy_tcp_skb(&dummy_skb, sk, TCP_DUMMY_ALLOC, seqno, seqack);
  if (!dummy_skb)
    return -ENOMEM;

  TCP_SKB_CB(dummy_skb)->sacked = sacked_flags;
  __skb_header_release(dummy_skb);
  sk->sk_wmem_queued += dummy_skb->truesize;
  sk_mem_charge(sk, dummy_skb->truesize);
  *skb_p = dummy_skb;

#if SME_DEBUG_LVL <= LVL_INFO
  if (tp->snd_nxt == tp->snd_una && tp->packets_out != 0) {
  iprint(LVL_EXP, "%pS %pS\n"
      "PORT %u dummy seq (%u:%u) skb %p truesize %d lock owned %d"
      " ipsum %d csum start %d off %d\n"
      "snd_una %u snd_nxt %u write_seq %u shared_seq %u sacked 0x%0x"
      " sk state %d CA %d\n"
      "wmem_queued %d wmem_alloc %d fwd_alloc %d => %d loss_prior_cwnd %u"
      " retrans hint %p %u\n"
      " pkts %d lost %d retrans %d sack_out %d cwnd %d"
      , __builtin_return_address(2), __builtin_return_address(4)
      , d_port, seqno, seqack
      , dummy_skb, dummy_skb->truesize, sk->sk_lock.owned
      , dummy_skb->ip_summed, dummy_skb->csum_start, dummy_skb->csum_offset
      , tp->snd_una, tp->snd_nxt, tp->write_seq, tp->shared_seq
      , TCP_SKB_CB(dummy_skb)->sacked
      , sk->sk_state, inet_csk(sk)->icsk_ca_state
      , sk->sk_wmem_queued, atomic_read(&sk->sk_wmem_alloc)
      , old_fwd_alloc, sk->sk_forward_alloc, tp->loss_prior_cwnd
      , tp->retransmit_skb_hint
      , (tp->retransmit_skb_hint ? TCP_SKB_CB(tp->retransmit_skb_hint)->seq : -1)
      , tp->packets_out, tp->lost_out, tp->retrans_out, tp->sacked_out
      , tp->snd_cwnd
      );
  }
#endif
  return 0;
#endif /* CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB */
#endif /* SME_CONFIG */
}

#endif /* CONFIG_XEN_SME */

