  /*
 * sme_xen.c
 *
 * created on: Mar 23, 2018
 * author: aasthakm
 *
 * xen facing code
 */

#include "sme_xen.h"
#include "sme_debug.h"
#include "conn.h"
#include "profile_map.h"
#include "sme_helper.h"

#include "sme_time.h"

#include <xen/page.h>
#include <net/tcp.h>

#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"

extern list_t scm_list[NUM_PACERS];
extern uint32_t my_ip_int;
extern uint16_t my_port_int;
extern uint8_t dummy_server_ip[4];
extern uint16_t dummy_server_port;

typedef struct profq_key {
  conn_t conn_id;
  uint64_t req_ts;
  int64_t first_latency;
} profq_key_t;

void
fn_itrace(void)
{
  iprint(LVL_DBG, "(%d:%.16s,core:%d) %s:%d returnaddr %pS\n"
      , task_pid_nr(current), current->comm, smp_processor_id()
      , __func__, __LINE__
      , __builtin_return_address(0)
      );
}

void
init_profq_key(profq_key_t *k, conn_t *c, uint64_t req_ts, int64_t first_latency)
{
  iprint(LVL_DBG, "k %p & c %p\n", k, c);

  if (!k || !c)
    return;

  memcpy((void *) &k->conn_id, (void *) c, sizeof(conn_t));
  k->req_ts = req_ts;
  k->first_latency = first_latency;

  iprint(LVL_DBG, "k->conn_id %p\n", &k->conn_id);

  //iprint(LVL_EXP, "k [%pI4 %u, %pI4 %u] (%lld %lld)\n"
  //    "[%pI4 %u, %pI4 %u] (%lld %lld)"
  //    , (void *) &k->conn_id.out_ip, k->conn_id.out_port
  //    , (void *) &k->conn_id.dst_ip, k->conn_id.dst_port
  //    , req_ts, first_latency);
}

// return 1 if k1 < k2, 0 otherwise, negative if invalid keys
int
compare_key_lt(profq_key_t *k1, profq_key_t *k2)
{
  if (!k1 || !k2)
    return -EINVAL;


  if ((k1->req_ts + k1->first_latency) < (k2->req_ts + k2->first_latency))
    return 1;

  if ((k1->req_ts + k1->first_latency) == (k2->req_ts + k2->first_latency)) {
    if (memcmp((void *) &k1->conn_id, (void *) &k2->conn_id, sizeof(conn_t)) == 0) {
      iprint(LVL_EXP, "BUG!!! k1 [%pI4 %u, %pI4 %u] (%lld %lld)\n"
      "[%pI4 %u, %pI4 %u] (%lld %lld)"
      , (void *) &k1->conn_id.out_ip, k1->conn_id.out_port
      , (void *) &k1->conn_id.dst_ip, k1->conn_id.dst_port
      , k1->req_ts, k1->first_latency
      , (void *) &k2->conn_id.out_ip, k2->conn_id.out_port
      , (void *) &k2->conn_id.dst_ip, k2->conn_id.dst_port
      , k2->req_ts, k2->first_latency
      );
      BUG();
//      return 0;
    }
    if (memcmp((void *) &k1->conn_id, (void *) &k2->conn_id, sizeof(conn_t)) < 0)
      return 1;
  }

  return 0;
}

// return 1 if k1 == k2, 0 otherwise, negative if invalid keys
int
compare_key_eq(profq_key_t *k1, profq_key_t *k2)
{
  if (!k1 || !k2)
    return -EINVAL;

  iprint(LVL_DBG, "k1 [%pI4 %u, %pI4 %u] (%lld %lld)\n"
      "caller %pS [%pI4 %u, %pI4 %u] (%lld %lld)"
      , (void *) &k1->conn_id.out_ip, k1->conn_id.out_port
      , (void *) &k1->conn_id.dst_ip, k1->conn_id.dst_port
      , k1->req_ts, k1->first_latency
      , __builtin_return_address(0)
      , (void *) &k2->conn_id.out_ip, k2->conn_id.out_port
      , (void *) &k2->conn_id.dst_ip, k2->conn_id.dst_port
      , k2->req_ts, k2->first_latency
      );
  if ((memcmp((void *) &k1->conn_id, (void *) &k2->conn_id, sizeof(conn_t)) == 0)
      && (k1->req_ts == k2->req_ts) && (k1->first_latency == k2->first_latency))
    return 1;

  return 0;
}

#if CONFIG_XEN_PACER
void
prepare_hypercall_args(sme_pacer_arg_t *harg, struct pci_dev *vf_pdev,
  profq_hdr_t **profq_hdr, profq_freelist2_t **profq_fl2, global_nicq_hdr_t **nicq,
  global_nicq_hdr_t **dummy_nicq, nic_freelist_t **nfl, nic_txintq_t **nic_txintq,
  int n_hypace)
{
  struct device *vf_dev = NULL;
  struct net_device *vf_ndev = NULL;
  struct bnx2x *vf_bp = NULL;
  struct bnx2x_fp_txdata *txq = NULL;
  int db_off = 0;
  uint64_t db_maddr = 0;
  uint64_t tx_bd_prod_maddr, tx_pkt_prod_maddr;
  uint64_t swi_tx_bd_prod_maddr, swi_tx_pkt_prod_maddr;
  int i;

  vf_dev = &vf_pdev->dev;
  vf_ndev = (struct net_device *) vf_dev->driver_data;
  vf_bp = netdev_priv(vf_ndev);

  harg->n_hp_args = n_hypace;
  for (i = 0; i < n_hypace; i++) {
    txq = &vf_bp->bnx2x_txq[SME_PACER_CORE + i];

    harg->hparr[i].txbuf_n_md = generate_mdesc(txq->tx_buf_ring,
        sizeof(struct sw_tx_bd), NUM_TX_BD, harg->hparr[i].txbuf_md_arr,
        MAX_TXBUF_FRAGS);

    harg->hparr[i].txdata_maddr = (virt_to_machine(txq)).maddr;

    db_off = vf_bp->doorbells - vf_bp->regview;
    // all doorbells mapped in resource[0] BAR of the PCI device
    db_maddr = vf_pdev->resource[0].start + db_off + (vf_bp->db_size * txq->cid);
    harg->hparr[i].doorbell_maddr = db_maddr;

    harg->hparr[i].pacer_core = XEN_PACER_CORE + i;
    harg->hparr[i].profq_hdr_maddr = (virt_to_machine(profq_hdr[i])).maddr;
    harg->hparr[i].profq_fl_maddr = (virt_to_machine(profq_fl2[i])).maddr;
    harg->hparr[i].nicq_maddr = (virt_to_machine(nicq[i])).maddr;
    harg->hparr[i].dummy_nicq_maddr = (virt_to_machine(dummy_nicq[i])).maddr;
    harg->hparr[i].nic_fl_maddr = (virt_to_machine(nfl[i])).maddr;
    harg->hparr[i].nic_txint_maddr = (virt_to_machine(nic_txintq[i])).maddr;
  }

  harg->pmap_htbl_size = PMAP_HTBL_SIZE;
  harg->pac_timerq_size = PAC_TIMERQ_SIZE;
  harg->nic_data_qsize = NIC_DATA_QSIZE;
  harg->cwnd_update_latency = CWND_UPDATE_LATENCY;
  harg->rexmit_update_latency = REXMIT_UPDATE_LATENCY;
  harg->pacer_spin_threshold = PACER_SPIN_THRESHOLD;
  harg->global_nic_data_qsize = GLOBAL_NIC_DATA_QSIZE;
  harg->pacer_config = HYPACE_CFG;
  harg->epoch_size = EPOCH_SIZE;
  harg->ptimer_heap_size = PTIMER_HEAP_SIZE;
  harg->max_dequeue_time = MAX_DEQUEUE_TIME;
  harg->max_prof_free_time = MAX_PROF_FREE_TIME;
  harg->config_tcp_mtu = SME_TCP_MTU;
  harg->config_sw_csum = CONFIG_SW_CSUM;
  harg->max_hp_per_irq = MAX_HP_PER_IRQ;
  harg->max_pkts_per_epoch = MAX_PKTS_PER_EPOCH;
  harg->max_other_per_epoch = MAX_OTHER_PER_EPOCH;

  tx_pkt_prod_maddr = (virt_to_machine(&txq->tx_pkt_prod)).maddr;
  tx_bd_prod_maddr = (virt_to_machine(&txq->tx_bd_prod)).maddr;
  swi_tx_pkt_prod_maddr = (virt_to_machine(&txq->swi_tx_pkt_prod)).maddr;
  swi_tx_bd_prod_maddr = (virt_to_machine(&txq->swi_tx_bd_prod)).maddr;

  iprint(LVL_DBG, "db maddr %0llx val %d\n"
      "pkt %d %d bd %d %d db %d "
      "txdesc mapping %0llx ring vaddr %p\n"
      "pkt prod [%0llx %d] bd prod [%0llx %d] "
      "swi pkt prod [%0llx %d] swi bd prod [%0llx %d]\n"
//      "profq maddr %0llx queue maddr %0llx %p "
      "sz %llu offsetof txdb prod %lu zerofill %lu header %lu\n"
      "nicq [%0llx %d %d] dummy nicq [%0llx %d %d] nicfl [%0llx %d %d]"
      , db_maddr, readl(vf_bp->doorbells + (vf_bp->db_size * txq->cid))
      , txq->tx_pkt_prod, txq->tx_pkt_cons, txq->tx_bd_prod, txq->tx_bd_cons
      , txq->tx_db.data.prod, txq->tx_desc_mapping, txq->tx_desc_ring
      , tx_pkt_prod_maddr, readw(&txq->tx_pkt_prod)
      , tx_bd_prod_maddr, readw(&txq->tx_bd_prod)
      , swi_tx_pkt_prod_maddr, txq->swi_tx_pkt_prod
      , swi_tx_bd_prod_maddr, txq->swi_tx_bd_prod
//      , harg->profq_hdr_maddr
//      , harg->profq_queue_frag_arr[0].profq_queue_frag_maddr, profq_hdr->queue
      , profq_hdr[0]->qsize
      , offsetof(struct doorbell_set_prod, prod)
      , offsetof(struct doorbell_set_prod, zero_fill1)
      , offsetof(struct doorbell_set_prod, header)
      , harg->hparr[0].nicq_maddr, nicq[0]->qsize, nicq[0]->n_md
      , harg->hparr[0].dummy_nicq_maddr, dummy_nicq[0]->qsize, dummy_nicq[0]->n_md
      , harg->hparr[0].nic_fl_maddr, nfl[0]->fl_size, nfl[0]->n_md
      );

}

#endif

/*
 * =================
 * profile queue API
 * =================
 */

// caller must set next
void
init_profq_elem(profq_elem_t *pelem, profile_conn_map_t *pcm, ihash_t *pid,
    uint64_t req_ts, uint32_t seqno, int cwm, profile_t *p, int state)
{
  int e_it = 0;

  if (!pelem || !pcm)
    return;

  if (pid)
    memcpy((void *) &pelem->id_ihash, pid, sizeof(ihash_t));
  else
    memcpy((void *) &pelem->id_ihash, (void *) &DEFAULT_PROFILE_IHASH, sizeof(ihash_t));

  memcpy((void *) &pelem->conn_id, (void *) &pcm->sec_conn_id, sizeof(conn_t));
  pelem->req_ts = req_ts;
  pelem->ack_ts = req_ts;
  pelem->loss_ts = 0;
  pelem->cwnd_latency_shift = 0;
  pelem->tobj_idx = 0;
  pelem->hypace_idx = pcm->hypace_idx;
  pelem->num_timers = 0;
  pelem->next_timer_idx = 0;
  pelem->cwnd_watermark = cwm;
  atomic_set(&pelem->real_counter, 0);
  // TODO: TCP congestion window may have been paused in previous response
  atomic_set(&pelem->is_paused, 0);
  pelem->dummy_counter = 0;
  pelem->last_real_ack_p = &pcm->last_real_ack;
  pelem->last_real_ack_maddr = (virt_to_machine(&pcm->last_real_ack)).maddr;

  pelem->max_seqno = seqno;
  pelem->tail = 0;
  pelem->last_skb_snd_window = pcm->tcp_rcv_wnd >> pcm->tcp_rcv_wscale;
#if CONFIG_DUMMY == DUMMY_OOB
  // shared pointers in pelem should be set only in the hypervisor
  pelem->shared_seq_p = NULL;
  pelem->shared_dummy_out_p = NULL;
  pelem->shared_dummy_lsndtime_p = NULL;
  pelem->shared_seq_maddr = 0;
  pelem->shared_dummy_out_maddr = 0;
  pelem->shared_dummy_lsndtime_maddr = 0;

  if (pcm->shared_seq_p) {
    pelem->shared_seq_maddr = (virt_to_machine(pcm->shared_seq_p)).maddr;
  }
  if (pcm->shared_dummy_out_p) {
    pelem->shared_dummy_out_maddr =
      (virt_to_machine(pcm->shared_dummy_out_p)).maddr;
  }
  if (pcm->shared_dummy_lsndtime_p) {
    pelem->shared_dummy_lsndtime_maddr =
      (virt_to_machine(pcm->shared_dummy_lsndtime_p)).maddr;
  }
  iprint(LVL_DBG, "seq %0llx %p %u\ndummy_out %0llx %p %u "
      "dummy_lsndtime %0llx %p %u"
      , pelem->shared_seq_maddr, pelem->shared_seq_p
      , (pelem->shared_seq_p ? *pelem->shared_seq_p : 0)
      , pelem->shared_dummy_out_maddr, pelem->shared_dummy_out_p
      , (pelem->shared_dummy_out_p ? *pelem->shared_dummy_out_p : 0)
      , pelem->shared_dummy_lsndtime_maddr, pelem->shared_dummy_lsndtime_p
      , (pelem->shared_dummy_lsndtime_p ? *pelem->shared_dummy_lsndtime_p : 0)
      );
#endif

#if !CONFIG_XEN_PACER_SLOTSHARE
#if CONFIG_NIC_QUEUE == CONFIG_NQ_DYNAMIC
  pelem->pcm_nicq_p = &pcm->pcm_nicq;
  pelem->pcm_nicq_maddr = (virt_to_machine(pelem->pcm_nicq_p)).maddr;
#else
  if (pelem->nic_data && pelem->nic_data_p) {
    // zero out individual pointers
    for (e_it = 0; e_it < NIC_DATA_QSIZE; e_it++) {
      memset(pelem->nic_data_p[e_it], 0, sizeof(nic_txdata_t));
    }
  }
//  if (pelem->nic_data)
//    memset(pelem->nic_data, 0, sizeof(nic_txdata_t) * NIC_DATA_QSIZE);
  pelem->nic_prod_idx = pelem->nic_cons_idx = 0;
#endif /* CONFIG_NIC_QUEUE */
#endif /* CONFIG_XEN_PACER_SLOTSHARE */

  pelem->src_mac_p = pelem->dst_mac_p = NULL;
  pelem->src_mac_maddr = (virt_to_machine(pcm->src_mac)).maddr;
  pelem->dst_mac_p = pcm->dst_mac;
  pelem->dst_mac_maddr = (virt_to_machine(pcm->dst_mac)).maddr;
  pelem->state = state;

  if (p) {
//    pelem->priv = p->priv;
    pelem->pmap_prof = p;
    pelem->prof_maddr = (virt_to_machine(p->priv)).maddr;
  }
}

int
profile_get_prev_ts(profq_elem_t *p, uint64_t *ts)
{
  int ret = 0;
  if (!p || !ts)
    return -PROF_NOENT;

  if (p->next_timer_idx == 0)
    return -PROF_UINIT;

  *ts = get_profile_timer_at_idx(p->pmap_prof, p->req_ts, p->next_timer_idx-1);
  return ret;
}

int
profile_get_next_ts(profq_elem_t *p, uint64_t *ts)
{
  int ret = 0;
  if (!p || !ts)
    return -PROF_NOENT;

  if (p->next_timer_idx == 0)
    return -PROF_UINIT;

  *ts = get_profile_timer_at_idx(p->pmap_prof, p->req_ts, p->next_timer_idx);
  return ret;
}

int
check_profile_prefix(profq_elem_t *dprofq_elem, profile_t *dprof, profile_t *cprof)
{
  int dreq_idx = 0, creq_idx = 0;
  int dframe_idx = 0, cframe_idx = 0;
  int req_it = 0;
  int dret = 0, cret = 0;
  uint64_t dreplace_ts = 0, creplace_ts = 0;
  uint64_t min_ts = 0;
  uint64_t prev_ts = 0;
  int next_timer_idx = 0;
  int ret = 0;

  next_timer_idx = dprofq_elem->next_timer_idx;

  ret = profile_get_prev_ts(dprofq_elem, &prev_ts);
  if (ret >= 0 && get_current_time(SCALE_RDTSC) < prev_ts)
    next_timer_idx--;

  dret = get_req_frame_idx_from_timer_idx(dprof, next_timer_idx,
      &dreq_idx, &dframe_idx);
  cret = get_req_frame_idx_from_timer_idx(cprof, next_timer_idx,
      &creq_idx, &cframe_idx);

  iprint(LVL_DBG, "T%d pivot idx %d default (%d %d %d) custom (%d %d %d)"
      , dprofq_elem->next_timer_idx, next_timer_idx
      , dreq_idx, dframe_idx, dret, creq_idx, cframe_idx, cret);
  if (dret < 0)
    return -PROF_INVAL;

  if (dreq_idx != creq_idx || dframe_idx != cframe_idx)
    return -PROF_INVAL;

  if (dframe_idx == 0) {
    dreq_idx--;
    creq_idx--;
  }

  for (req_it = 0; req_it < dreq_idx; req_it++) {
    if (get_latency_for_req(dprof, req_it) != get_latency_for_req(cprof, req_it)
        || get_spacing_for_req(dprof, req_it) != get_spacing_for_req(cprof, req_it))
      return -PROF_INVAL;
  }

  dreplace_ts = dprofq_elem->req_ts + get_latency_for_req(dprof, dreq_idx+1);
  creplace_ts = dprofq_elem->req_ts + get_latency_for_req(cprof, creq_idx+1);
  min_ts = min_t(uint64_t, dreplace_ts, creplace_ts);
  if (get_current_time(SCALE_RDTSC) >= min_ts)
    return -PROF_EXPRD;

  return 0;
}

/*
 * init profile queue shared with xen
 */
int
init_profq_hdr(profq_hdr_t *profq_hdr, nic_txdata_t **nic_data_arr, int qsize)
{
  int i;
  int head_idx = 0;
  ihash_t ihash_head;
  ihash_t ihash_tail;
  int j = 0;
  int n_elem_per_page = 0;
  int profq_size = 0;
  int n_pages = 0;
  int page_off = 0;
  int ptr_it = 0;
  void *page_start = NULL;
  int n_it = 0;
  int e_it = 0;

  profile_conn_map_t tmp_pcm;

  if (!profq_hdr)
    return -EINVAL;

  /*
   * allocate elements so that they do not straddle frame boundaries
   * in physical RAM
   */
  n_elem_per_page = PAGE_SIZE/sizeof(profq_elem_t);
  n_pages = (qsize-1)/n_elem_per_page + 1;
  profq_size = n_pages * PAGE_SIZE;

  /*
   * there is no guarantee that we got page aligned chunks, but so far
   * we seem to be getting them.
   */
  profq_hdr->queue = (profq_elem_t *) kzalloc(profq_size, GFP_KERNEL);

  if (!profq_hdr->queue) {
    return -ENOMEM;
  }

  profq_hdr->qsize = qsize;

  profq_hdr->n_md = generate_mdesc_palign(profq_hdr->queue,
      sizeof(profq_elem_t), qsize, profq_hdr->md_arr, MAX_QUEUE_FRAGS);
  if (profq_hdr->n_md < 0) {
    kfree(profq_hdr->queue);
    return profq_hdr->n_md;
  }

  /*
   * gpace's pointers to the page-aligned profq elements in the shared queue
   */
  profq_hdr->queue_p =
    (profq_elem_t **) kzalloc(sizeof(profq_elem_t *) * qsize, GFP_KERNEL);

  if (!profq_hdr->queue_p) {
    kfree(profq_hdr->queue);
    return -ENOMEM;
  }

  iprint(LVL_EXP, "sz profq_elem %lu #elems/page %d #pages %d tot size %d"
      , sizeof(profq_elem_t), n_elem_per_page, n_pages, profq_size
      );

  ptr_it = 0;
  for (i = 0; i < profq_hdr->n_md; i++) {
    page_start =
      (void *) __va((machine_to_phys(XMADDR(profq_hdr->md_arr[i].maddr))).paddr);
    for (j = 0; j < profq_hdr->md_arr[i].n_elems; j++) {
      page_off = get_velem_size_off(j, sizeof(profq_elem_t));
      profq_hdr->queue_p[ptr_it] = (profq_elem_t *) (page_start + page_off);
      ptr_it++;
    }
  }

  j = 0;
  for (i = 0; i < qsize; i++) {
    profq_hdr->queue_p[i]->profq_idx = i;
    profq_hdr->queue_p[i]->state = PSTATE_FREELIST;

    if (i == 0 || i == qsize - 1)
      continue;

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
    profq_hdr->queue_p[i]->nic_data = nic_data_arr[j];
    profq_hdr->queue_p[i]->n_md = generate_mdesc_palign(nic_data_arr[j],
        sizeof(nic_txdata_t), NIC_DATA_QSIZE,
        profq_hdr->queue_p[i]->md_arr, MAX_NDATA_FRAGS);
    profq_hdr->queue_p[i]->nic_data_p =
      (nic_txdata_t **) kzalloc(sizeof(nic_txdata_t *) * NIC_DATA_QSIZE, GFP_KERNEL);
    ptr_it = 0;
    for (n_it = 0; n_it < profq_hdr->queue_p[i]->n_md; n_it++) {
      page_start = (void *) __va((machine_to_phys(
              XMADDR(profq_hdr->queue_p[i]->md_arr[n_it].maddr))).paddr);
      for (e_it = 0; e_it < profq_hdr->queue_p[i]->md_arr[n_it].n_elems; e_it++) {
        page_off = get_velem_size_off(e_it, sizeof(nic_txdata_t));
        profq_hdr->queue_p[i]->nic_data_p[ptr_it] =
          (nic_txdata_t *) (page_start + page_off);
        ptr_it++;
      }
    }
#endif

#if SME_DEBUG_LVL <= LVL_DBG
    if (i % n_elem_per_page == 0 || i % n_elem_per_page == 1
        || i % n_elem_per_page == 2) {
      iprint(LVL_DBG, "[%d] mod %d v %p m %0llx pg %llu\n"
          "Q v %p m %0llx pg %llu"
          , i, (i%n_elem_per_page), profq_hdr->queue_p[i]
          , (virt_to_machine(profq_hdr->queue_p[i]).maddr)
          , (virt_to_machine(profq_hdr->queue_p[i]).maddr)/PAGE_SIZE
          , &profq_hdr->queue[i]
          , (virt_to_machine(&profq_hdr->queue[i]).maddr)
          , (virt_to_machine(&profq_hdr->queue[i]).maddr)/PAGE_SIZE
          );
    }
#endif

    j += 1;
//    atomic_set(&(profq_hdr->queue[i].next), -1);
  }

  memset((void *) &tmp_pcm, 0, sizeof(profile_conn_map_t));
  memset((void *) &ihash_head, 0, sizeof(ihash_t));
  memset((void *) &ihash_tail, (uint16_t) -1, sizeof(ihash_t));

  init_conn(&tmp_pcm.sec_conn_id, (uint32_t) -1, (uint32_t) -1, (uint32_t) -1,
      (uint32_t) -1, (uint16_t) -1, (uint16_t) -1, (uint16_t) -1, (uint16_t) -1);

  init_profq_elem(profq_hdr->queue_p[0], &tmp_pcm, &ihash_head, 0, 0, 0, NULL, 0);
  init_profq_elem(profq_hdr->queue_p[qsize-1], &tmp_pcm, &ihash_tail,
      0, 0, 0, NULL, 0);

  head_idx = 0;
  atomic_set(&profq_hdr->u.sorted2.head, head_idx);
  atomic_set(&profq_hdr->u.sorted2.tail, qsize - 1);
  atomic_set(&(profq_hdr->queue_p[head_idx]->next), qsize - 1);

  iprint(LVL_DBG, "tail pmap_prof %p"
      , profq_hdr->queue_p[atomic_read(&profq_hdr->u.sorted2.tail)]->pmap_prof);
  init_tkt_lock(&profq_hdr->u.sorted2.tkt_lock);

  return profq_size;
}

void
cleanup_profq_hdr(profq_hdr_t *profq_hdr)
{
  if (!profq_hdr)
    return;

  if (profq_hdr->queue_p)
    kfree(profq_hdr->queue_p);

  if (profq_hdr->queue)
    kfree(profq_hdr->queue);
}

int
is_marked_reference(int idx)
{
  return (int) (idx & PROF_REF_MASK);
}

int
get_unmarked_reference(int idx)
{
  return (idx & PROF_REF_UNMASK);
}

int
get_marked_reference(int idx)
{
  return (idx | PROF_REF_MASK);
}

int
search(profq_hdr_t *profq_hdr, profq_key_t *search_key)
{
  int idx, idx_next, idx_next_um, idx_last_unmarked, idx_last_unmarked_next;
  int idx_tail;
  int ret = 0;
  profq_elem_t *t_pelem = NULL;
  profq_key_t t_key;

  do {

    // queue head node
    idx = atomic_read(&profq_hdr->u.sorted2.head);
    // current first node (possibly tail)
    idx_next = atomic_read(&profq_hdr->queue_p[idx]->next);
    idx_tail = atomic_read(&profq_hdr->u.sorted2.tail);
    // save last unmarked node we encountered
    idx_last_unmarked = idx;
    // and its current successor
    idx_last_unmarked_next = idx_next;

    // no need for getting unmarked reference, because head should never be marked!
    t_pelem = profq_hdr->queue_p[idx_next];

    if (get_unmarked_reference(idx_next) != idx_tail) {
      init_profq_key(&t_key, &t_pelem->conn_id, t_pelem->req_ts,
          get_latency_for_req(t_pelem->pmap_prof, 0));
    } else {
      init_profq_key(&t_key, &t_pelem->conn_id, t_pelem->req_ts, -1);
    }

    while (
      ((idx_next_um = get_unmarked_reference(idx_next)) !=
        atomic_read(&profq_hdr->u.sorted2.tail))
      && (is_marked_reference(atomic_read(&profq_hdr->queue_p[idx_next_um]->next))
          || compare_key_lt(&t_key, search_key))) {

      // idx's successor is not the tail node AND
      // either the successor is marked or its key is smaller than the search key

      if (!is_marked_reference(idx_next)) {
        // idx is not marked, save it and its successor
        idx_last_unmarked = idx;
        idx_last_unmarked_next = idx_next;
      }

      idx = idx_next_um;  // we could be skipping one or more concurrently
                          // inserted nodes between idx and idx_next
                          // that's OK, because they must have keys < search_key
      idx_next = atomic_read(&profq_hdr->queue_p[idx]->next);

      t_pelem = profq_hdr->queue_p[get_unmarked_reference(idx_next)];
      if (get_unmarked_reference(idx_next) != idx_tail) {
        init_profq_key(&t_key, &t_pelem->conn_id,
          t_pelem->req_ts, get_latency_for_req(t_pelem->pmap_prof, 0));
      } else {
        init_profq_key(&t_key, &t_pelem->conn_id, t_pelem->req_ts, -1);
      }
    }

    // either idx's successor is the tail node
    // or idx's successor is currently unmarked and has a key >= the search key
    // idx may be marked
    // idx_last_unmarked, idx_last_unmarked_next refer to
    // last unmarked node we encountered and its successor, resp.

    // idx is not marked, we're done
    if (!is_marked_reference(idx_next)) {
      return idx;
    }

    // try to delete marked nodes between idx_last_unmarked and idx_next
    ret = atomic_cmpxchg(&profq_hdr->queue_p[idx_last_unmarked]->next,
        idx_last_unmarked_next, idx_next_um);

    if (ret == idx_last_unmarked_next) {
      //success, we removed marked nodes
      return idx_last_unmarked;
    }

    // unable to remove marked nodes due to concurrent modifications, retry
  } while(1);
}

int
insert(profq_hdr_t *profq_hdr, int insert_idx)
{
  int idx, idx_next;
  profq_key_t key, right_key;
  profq_elem_t *insert_pelem = NULL;
  profq_elem_t *right_pelem = NULL;
  int head_idx = 0;
  int ret = 0;
  int loop_count = 0;

  insert_pelem = profq_hdr->queue_p[insert_idx];

  memcpy((void *) &key.conn_id, (void *) &insert_pelem->conn_id, sizeof(conn_t));
  key.req_ts = insert_pelem->req_ts;
  key.first_latency = get_latency_for_req(insert_pelem->pmap_prof, 0);

  head_idx = atomic_read(&profq_hdr->u.sorted2.head);

  do {
    idx = search(profq_hdr, &key);
    idx_next = get_unmarked_reference(atomic_read(&profq_hdr->queue_p[idx]->next));

    if (idx_next != atomic_read(&profq_hdr->u.sorted2.tail)) {
      // set element key values
      // TODO: check if right_index = idx_next !!!
      right_pelem = profq_hdr->queue_p[idx_next];
      init_profq_key(&right_key, &right_pelem->conn_id, right_pelem->req_ts,
          get_latency_for_req(right_pelem->pmap_prof, 0));
    }

    if (idx_next != atomic_read(&profq_hdr->u.sorted2.tail) &&
        compare_key_eq(&right_key, &key)) {
      // new_key is already in queue
      iprint(LVL_EXP, "BUG!!! key already in queue idx %d idx_nxt %d idx.nxt %d "
          "insert idx %d\n"
          "ikey [%pI4 %u, %pI4 %u] (%lld %lld)\nrkey [%pI4 %u, %pI4 %u] (%lld %lld)"
          , idx, idx_next, atomic_read(&profq_hdr->queue_p[idx]->next), insert_idx
          , (void *) &key.conn_id.out_ip, key.conn_id.out_port
          , (void *) &key.conn_id.dst_ip, key.conn_id.dst_port
          , key.req_ts, key.first_latency
          , (void *) &right_key.conn_id.out_ip, right_key.conn_id.out_port
          , (void *) &right_key.conn_id.dst_ip, right_key.conn_id.dst_port
          , right_key.req_ts, right_key.first_latency
      );
      BUG();
//      return -1;
    }

    // check if the key of the current successor node is >= new_key
    // (concurrent insertions)
    if (compare_key_lt(&key, &right_key)
        || idx_next == atomic_read(&profq_hdr->u.sorted2.tail)) {
      atomic_set(&profq_hdr->queue_p[insert_idx]->next, idx_next);
      // swing idx.next to point to new node
      ret = atomic_cmpxchg(&profq_hdr->queue_p[idx]->next, idx_next, insert_idx);
      iprint(LVL_DBG, "l %d r %d => l %d i %d r %d => ret %d"
          , idx, idx_next, idx, insert_idx
          , atomic_read(&profq_hdr->queue_p[insert_idx]->next), ret);

      if (ret == idx_next) { // success
        if (idx == head_idx)
          return 1;
        return 2;
      }
    }

    // else retry
    loop_count++;

  } while(1);
}

int
insert_locked(profq_hdr_t *profq_hdr, profq_freelist2_t *pfl,
    profq_elem_t *insert_pelem, profile_t *p)
{
  int insert_idx = 0;
  int head_idx = 0;
  int tail_idx = 0;
  int left_idx = 0;
  int right_idx = 0;
  int t_idx = 0;
  int t_idx_next = 0;
  profq_key_t key;
  profq_key_t t_key;
  profq_elem_t *t_pelem = NULL;

  if (!profq_hdr || !pfl || !insert_pelem || !p)
    return 0;

  acquire_tkt_lock_bh(&(profq_hdr->u.sorted2.tkt_lock));

  head_idx = atomic_read(&profq_hdr->u.sorted2.head);
  tail_idx = atomic_read(&profq_hdr->u.sorted2.tail);

//  insert_idx = insert_pelem - &(profq_hdr->queue[0]);
  insert_idx = insert_pelem->profq_idx;
  init_profq_key(&key, &insert_pelem->conn_id, insert_pelem->req_ts,
      get_latency_for_req(insert_pelem->pmap_prof, 0));

  left_idx = head_idx;
  t_idx = head_idx;
  t_idx_next = atomic_read(&profq_hdr->queue_p[head_idx]->next);

  iprint(LVL_DBG, "head %d tail %d left %d insert %d t %d t_next %d"
      , head_idx, tail_idx, left_idx, insert_idx, t_idx, t_idx_next);
  do {

    left_idx = t_idx;
    t_idx = t_idx_next;

    if (t_idx == tail_idx)
      break;

    t_pelem = profq_hdr->queue_p[t_idx];
    init_profq_key(&t_key, &t_pelem->conn_id, t_pelem->req_ts,
        get_latency_for_req(t_pelem->pmap_prof, 0));

    t_idx_next = atomic_read(&(profq_hdr->queue_p[t_idx]->next));
  } while (compare_key_lt(&t_key, &key));

  right_idx = t_idx;

//  BUG_ON(!(right_idx > 0));
//  BUG_ON(!(right_idx <= tail_idx));
//  BUG_ON(!(insert_idx > 0));
//  BUG_ON(!(insert_idx < tail_idx));

  atomic_set(&insert_pelem->next, right_idx);
  atomic_set(&(profq_hdr->queue_p[left_idx]->next), insert_idx);

  iprint(LVL_DBG, "inserted %d prev %d elem.nxt %d head %d "
      "[%pI4 %u, %pI4 %u] freelist2 prod %d cons %d"
      , insert_idx, left_idx, right_idx, head_idx
      , (void *) &insert_pelem->conn_id.in_ip, insert_pelem->conn_id.in_port
      , (void *) &insert_pelem->conn_id.src_ip, insert_pelem->conn_id.src_port
      , pfl->prod_idx, pfl->cons_idx
      );
  release_tkt_lock_bh(&(profq_hdr->u.sorted2.tkt_lock));

  if (left_idx == head_idx)
    return 1;

  return 2;
}

int
profq_insert(profq_hdr_t *profq_hdr, profq_freelist2_t *pfl,
    profile_conn_map_t *pcm, ihash_t *pid, uint64_t req_ts, uint32_t seqno,
    int cwm, int *ins_idx)
{
  profq_elem_t *insert_pelem = NULL;
  int insert_idx;
  int ret = 0;
  profile_t *p = NULL;

  sme_channel_map_t *scm = pcm->s;

  // get the profile the new element should point to
  p = htable_lookup(&scm->pmap_htbl, (void *) pid, sizeof(ihash_t));
  if (!p) {
    return -EINVAL;
  }

  ret = get_one_profq_freelist2(pfl, &insert_idx);
  if (ret < 0)
    return ret;

  insert_pelem = profq_hdr->queue_p[insert_idx];
  init_profq_elem(insert_pelem, pcm, pid, req_ts, seqno, cwm, p, PSTATE_PROFQ);

#if CONFIG_SYNC == SYNC_LOCKED
  ret = insert_locked(profq_hdr, pfl, insert_pelem, p);
#else
  ret = insert(profq_hdr, insert_idx);
#endif

  if (ret > 0) {
    *ins_idx = insert_idx;
  }

  return ret;
}

/*
 * ===========
 * freelist v2
 * ===========
 */

int
init_profq_freelist2(profq_freelist2_t *pfl, int qsize)
{
  int i;

  if (!pfl)
    return -EINVAL;

  pfl->idx_list = kzalloc(sizeof(int32_t) * qsize, GFP_KERNEL);
  if (!pfl->idx_list)
    return -ENOMEM;

  pfl->idx_list_maddr = (virt_to_machine(pfl->idx_list)).maddr;
  pfl->idx_list_size = qsize;

  // initially set the indexes in the freelist as 1,2,3,...,(qsize-2)
  // we use index 0 and (qsize - 1) as sentinel head and tail for profq
  for (i = 0; i < qsize; i++)
    pfl->idx_list[i] = i+1;

  pfl->cons_idx = 0;
  pfl->prod_idx = pfl->idx_list_size;

  pfl->n_md = generate_mdesc(pfl->idx_list, sizeof(int32_t), pfl->idx_list_size,
      pfl->md_arr, MAX_FL_FRAGS);
  spin_lock_init(&pfl->lock);

  return 0;
}

void
cleanup_profq_freelist2(profq_freelist2_t *pfl)
{
  if (!pfl)
    return;

  if (pfl->idx_list)
    kfree(pfl->idx_list);
}

int
get_one_profq_freelist2(profq_freelist2_t *pfl, int *idx)
{
  if (!pfl || !idx)
    return -EINVAL;

  spin_lock_bh(&pfl->lock);

  if (pfl->cons_idx == pfl->prod_idx) {
    spin_unlock_bh(&pfl->lock);
    return -ENOMEM;
  }

  *idx = pfl->idx_list[pfl->cons_idx % pfl->idx_list_size];
  pfl->cons_idx = pfl->cons_idx + 1;

  spin_unlock_bh(&pfl->lock);

  return 0;
}
