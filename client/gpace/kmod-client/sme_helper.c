/*
 * pacer_int.c
 *
 * created on: Mar 26, 2017
 * author: aasthakm
 *
 * functions for pacer thread
 */

#include "sme_config.h"
#include "profile_map.h"
#include "sme_common.h"
#include "sme_debug.h"
#include "sme_time.h"
#include "sme_skb.h"

#include "../../include/log_format.h"

#include <linux/skbuff.h>
#include <uapi/linux/tcp.h>
#include <uapi/linux/ip.h>
#include <uapi/linux/in.h>
#include <linux/udp.h>
#include <net/ip.h>

#include "sme_helper.h"
#include "sme_statistics.h"

#ifndef MAX_BUF_SIZE
#define MAX_BUF_SIZE 2048
#endif

extern uint32_t my_ip_int;
extern uint16_t my_port_int;
extern uint32_t bknd_ip_int;
extern uint16_t bknd_port_int;
extern char *host_type_int;
extern uint64_t tot_pmap_htbl_size, tot_pcm_htbl_size;

//SME_DECL_STAT_ARR_EXTERN(pcm_lookup, NUM_PACERS);

SME_DECL_STAT_ARR_EXTERN(rx_ooo_cnt, NUM_PACERS);

/*
 * =============================
 * enforcement related functions
 * =============================
 */
int
get_profile_conn_info_from_skb_header(uint32_t src_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t dst_port, list_t *scm_list,
    sme_channel_map_t **scm_p, profile_conn_map_t **pcm_p, int8_t idx,
    int update, int send, sme_sk_buff_t *sskb)
{
  sme_channel_map_t *scm = NULL;
  sme_channel_map_t *parent_scm = NULL;
  sme_channel_map_t *dep_scm = NULL;
  profile_conn_map_t *pcm = NULL;
  int is_parent_scm = 0, found_scm = 0;
  conn_t conn_id;

  list_for_each_entry(parent_scm, scm_list, scm_listp) {
    if (parent_scm->conn_id.out_ip != src_ip)
      continue;

    // pkt belongs to a conn on the client facing service
    // represented by parent_scm
    if (parent_scm->conn_id.out_port == src_port) {
      scm = parent_scm;
      is_parent_scm = 1;
      found_scm = 1;
      break;
    }

    found_scm = 0;
    list_for_each_entry(dep_scm, &parent_scm->dep_scm_list, dep_scm_listp) {
      // pkt belongs to a conn to the backend service represented by dep_scm
      if (dep_scm->conn_id.dst_ip == dst_ip
          && dep_scm->conn_id.dst_port == dst_port) {
        scm = dep_scm;
        found_scm = 1;
        break;
      }
    }

    if (found_scm)
      break;
  }

  if (!scm) {
    return -EINVAL;
  }

  *scm_p = scm;

  init_conn(&conn_id, src_ip, 0, 0, dst_ip, src_port, 0, 0, dst_port);
  pcm = lookup_profile_conn_map_by_sec_conn(scm, &conn_id, update, send, sskb);

  if (!is_parent_scm) {
    iprint(LVL_INFO, "%pS %pS\n[%pI4 %u, %pI4 %u], dep_scm %d %d, SCM: %p, PCM: %p state %d stage %d"
      , __builtin_return_address(0), __builtin_return_address(3)
      , (void *) &src_ip, src_port, (void *) &dst_ip, dst_port
      , is_parent_scm, found_scm, scm, pcm
      , (pcm ? pcm->state : -1), (pcm ? pcm->stage : -1));
  }

  if (!pcm) {
    return -EINVAL;
  }

  *pcm_p = pcm;

  return 0;
}

void
add_initial_sme_channel_map(list_t *scm_list)
{
  sme_channel_map_t *scm1 = NULL, *scm2 = NULL;
  conn_t conn_id;
  int ret = 0;

  /*
   * intialize scm's for each app interface on this host (currently hard-coded).
   * TODO: API to add scm's for new app interfaces.
   */
  scm1 = (sme_channel_map_t *) kzalloc(sizeof(sme_channel_map_t), GFP_KERNEL);
  if (!scm1) {
    ret = -ENOMEM;
    goto error;
  }

  init_conn(&conn_id, 0, my_ip_int, my_ip_int, 0,
      0, my_port_int, my_port_int, 0);
  ret = init_sme_channel_map(scm1, &conn_id);
  iprint(LVL_EXP, "scm hdr size %lu", sizeof(sme_channel_map_t));
  tot_pmap_htbl_size = (sizeof(sme_htable_t) + (sizeof(list_t) * PMAP_HTBL_SIZE));
  tot_pcm_htbl_size = (sizeof(sme_htable_t) + (sizeof(list_t) * PCM_HTBL_SIZE));
  iprint(LVL_EXP, "pmap htbl Q hdr size %lu elem size %lu #elem %d Q mem %llu"
      , sizeof(sme_htable_t), sizeof(list_t), PMAP_HTBL_SIZE, tot_pmap_htbl_size);
  iprint(LVL_EXP, "pcm htbl Q hdr size %lu elem size %lu #elem %d Q mem %llu"
      , sizeof(sme_htable_t), sizeof(list_t), PCM_HTBL_SIZE, tot_pcm_htbl_size);

  if (strncmp(host_type_int, "FE", strlen("FE")) == 0) {
    scm1->sme_type = SME_FE_CLIENT;
  } else {
    scm1->sme_type = SME_BE_CLIENT;
  }

  set_sme_channel_in_global_list(scm_list, scm1);

  iprint(LVL_EXP, "SCM1 %p [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
      , scm1, (void *) &scm1->conn_id.src_ip, scm1->conn_id.src_port
      , (void *) &scm1->conn_id.in_ip, scm1->conn_id.in_port
      , (void *) &scm1->conn_id.out_ip, scm1->conn_id.out_port
      , (void *) &scm1->conn_id.dst_ip, scm1->conn_id.dst_port
      );
  if (strncmp(host_type_int, "FE", strlen("FE")) != 0)
    return;

  scm2 = (sme_channel_map_t *) kzalloc(sizeof(sme_channel_map_t), GFP_KERNEL);
  if (!scm2) {
    ret = -ENOMEM;
    goto error;
  }

  init_conn(&conn_id, 0, my_ip_int, my_ip_int, bknd_ip_int,
      0, my_port_int, 0, bknd_port_int);
  ret = init_sme_channel_map(scm2, &conn_id);

  scm2->sme_type = SME_FE_BE;
  scm2->parent_scm = scm1;

  list_add_tail(&scm2->dep_scm_listp, &scm1->dep_scm_list);
  //print_sme_channel_map(scm1);
  //print_sme_channel_map(scm2);

  iprint(LVL_EXP, "SCM2 %p [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
      , scm2, (void *) &scm2->conn_id.src_ip, scm2->conn_id.src_port
      , (void *) &scm2->conn_id.in_ip, scm2->conn_id.in_port
      , (void *) &scm2->conn_id.out_ip, scm2->conn_id.out_port
      , (void *) &scm2->conn_id.dst_ip, scm2->conn_id.dst_port);
  return;

error:
  if (scm1)
    free_sme_channel_map(&scm1);
  if (scm2)
    free_sme_channel_map(&scm2);
  iprint(LVL_EXP, "Error in intializing SME's, ret: %d", ret);
}

void
init_new_profile_conn_info(uint32_t s_addr, uint32_t d_addr,
    uint16_t s_port, uint16_t d_port, struct net_device *dev,
    unsigned char *s_mac, unsigned char *d_mac, int8_t cpuid,
    sme_channel_map_t *scm, profile_conn_map_t **pcm_p, int8_t idx)
{
  profile_conn_map_t *pcm = NULL;
  conn_t conn_id;
  int ret = 0;

  if (!pcm_p)
    return;

  init_conn(&conn_id, 0, 0, s_addr, d_addr, 0, 0, s_port, d_port);
  pcm = (profile_conn_map_t *) kzalloc(sizeof(profile_conn_map_t), GFP_KERNEL);
  if (!pcm)
    return;

  ret = init_profile_conn_map(pcm, &conn_id, NULL, dev, s_mac, d_mac, cpuid);
  //ret = set_profile_conn_map_on_channel(scm, pcm);
  ret = set_profile_conn_map_on_channel_nocheck(scm, pcm);

#if SME_DEBUG_LVL <= LVL_INFO
  { // to disable compiler warning of mixed declarations
  unsigned char print_s_mac[ETH_PRINT_HDR_LEN];
  unsigned char print_d_mac[ETH_PRINT_HDR_LEN];
  memset(print_s_mac, 0, ETH_PRINT_HDR_LEN);
  memset(print_d_mac, 0, ETH_PRINT_HDR_LEN);
  if (s_mac)
    print_mac_addr(s_mac, print_s_mac, ETH_PRINT_HDR_LEN);
  if (d_mac)
    print_mac_addr(d_mac, print_d_mac, ETH_PRINT_HDR_LEN);
  iprint(LVL_INFO, "INIT NEW PCM %p scm type: %d, ret: %d\n"
      "for [%pI4:%u, %pI4:%u, %pI4:%u, %pI4:%u]"
      ", SMAC: %s, DMAC: %s"
      , pcm, scm->sme_type, ret
      , (void *) &conn_id.src_ip, conn_id.src_port
      , (void *) &conn_id.in_ip, conn_id.in_port
      , (void *) &conn_id.out_ip, conn_id.out_port
      , (void *) &conn_id.dst_ip, conn_id.dst_port
      , print_s_mac, print_d_mac);
  }
#endif

  if (ret < 0) {
    free_profile_conn_map(&pcm, 1);
    pcm = NULL;
    return;
  }

  *pcm_p = pcm;
}

void
update_pcm_state_recv(profile_conn_map_t *pcm, sme_sk_buff_t *sskb)
{
  int old_state = pcm->state;
  int new_state = 0;
  /*
   * technically this should be SYN_RECV,
   * but since SYNACK is sent from pacer thread,
   * we abuse the state machine and use its states a bit differently
   */
  if (pcm->state == 0) {
    if (sskb->pskb.syn && !sskb->pskb.ack) {
      pcm->state = TCP_LISTEN;
      goto exit;
    }

#if IGNORE_SYN_OUT
    /*
     * this is required only when we do not init a new
     * conn when the host sends out the first SYN pkt.
     */
    if (sskb->pskb.syn && sskb->pskb.ack) {
      pcm->state = TCP_SYN_RECV;
      goto exit;
    }
#endif

    goto exit;
  }

  if (pcm->state == TCP_SYN_RECV) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      pcm->state = TCP_ESTABLISHED;
      goto exit;
    }

    goto exit;
  }

  /*
   * technically this should be ESTABLISHED, but ACK is sent from pacer thread,
   * therefore, we abuse the state machine, set a diff. state here,
   * and let pacer thread change from this state to ESTABLISHED
   * after sending the ACK
   */
  if (pcm->state == TCP_SYN_SENT) {
    if (sskb->pskb.syn) {
      pcm->state = TCP_SYN_RECV;
      goto exit;
    }

    goto exit;
  }

  /*
   * shutdown initiated by the other end of this pcm
   */
  if (pcm->state == TCP_ESTABLISHED) {
    if (sskb->pskb.fin) {
      /*
       * this is an extremely special case, for which we build a workaround
       * at the moment. hopefully it does not occur too often:
       * C, S, PSHACK s,a,l=31 [EST => EST]
       * C, S, FINACK s+l,a [EST => MYSTATE3]
       * S, C, FINACK a,s [MYSTATE3 => MYSTATE]
       * S, C, ACK a+1,s+l+1 [MYSTATE => TW]
       * C, S, ACK s+l+1,a+1 [TW => *]
       *
       * last two pkts can also be reordered. it leads to the same state seq:
       * C, S, ACK s+l+1,a+1 [MYSTATE => TW]
       * S, C, ACK a+1,s+l+1 [TW => *] // this can potentially create a new pcm
       *
       * a third possibility is the reordering of the 3rd and 4th pkts above:
       * S, C, ACK a+1,s+l+1 [MYSTATE3 => MYSTATE3]
       * S, C, FINACK a,s [MYSTATE3 => MYSTATE]
       * C, S, ACK s+l+1,a+1 [MYSTATE => CLOSE]
       */
      if (sskb->pskb.seqno ==
            (pcm->rec_req_pkt.seqno + pcm->rec_req_pkt.data_len)
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqack) {
        iprint(LVL_DBG, "pcm [%pI4 %u, %pI4 %u]\n"
          "last sent (%u:%u), last recv (%u:%u)\n"
          "flags[FIN,SYN,RST,PSH,ACK,URG,ECE,CWR]: %d %d %d %d %d %d %d %d"
          ", seq (%u:%u)"
          , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
          , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
          , pcm->last_send_pkt.seqno, pcm->last_send_pkt.seqack
          , pcm->rec_req_pkt.seqno, pcm->rec_req_pkt.seqack
          , sskb->pskb.fin, sskb->pskb.syn, sskb->pskb.rst, sskb->pskb.psh
          , sskb->pskb.ack, sskb->pskb.urg, sskb->pskb.ece, sskb->pskb.cwr
          , sskb->pskb.seqno, sskb->pskb.seqack);

        pcm->state = TCP_MYSTATE3;
        goto exit;
      }

      pcm->state = TCP_CLOSE_WAIT;
      goto exit;
    }

    if (sskb->pskb.rst) {
      pcm->state = TCP_CLOSE;
      goto exit;
    }
    goto exit;
  }

  /*
   * shutdown initiated from this pcm
   */
  if (pcm->state == TCP_FIN_WAIT1) {
    iprint(LVL_DBG, "pcm [%pI4 %u, %pI4 %u]\n"
        "last sent (%u:%u), last recv (%u:%u)\n"
        "flags[FIN,SYN,RST,PSH,ACK,URG,ECE,CWR]: %d %d %d %d %d %d %d %d"
        ", seq (%u:%u)"
        , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
        , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
        , pcm->last_send_pkt.seqno, pcm->last_send_pkt.seqack
        , pcm->rec_req_pkt.seqno, pcm->rec_req_pkt.seqack
        , sskb->pskb.fin, sskb->pskb.syn, sskb->pskb.rst, sskb->pskb.psh
        , sskb->pskb.ack, sskb->pskb.urg, sskb->pskb.ece, sskb->pskb.cwr
        , sskb->pskb.seqno, sskb->pskb.seqack);

    if (sskb->pskb.fin) {
      if (sskb->pskb.seqno == pcm->last_send_pkt.seqack
          && sskb->pskb.seqack ==
            (pcm->last_send_pkt.seqno + pcm->last_send_pkt.data_len + 1)) {
        pcm->state = TCP_FIN_WAIT2;
        goto exit;
      }

      /*
       * simultaneous FIN was sent from this pcm first
       */
      if (sskb->pskb.seqno == pcm->last_send_pkt.seqack
          && sskb->pskb.seqack == pcm->last_send_pkt.seqno) {
        pcm->state = TCP_CLOSING;
        goto exit;
      }

      /*
       * observed seq in https ab:
       * S, C, FINACK s,a [EST => FW1]
       * C, S, ACK, a,s [FW1 => FW1]
       * C, S, PSHACK, a,s,l=31 [FW1 => FW1]
       * C, S, FINACK, a+l,s+1 [FW1 => FW2]
       * S, C, ACK, s+1,a+l+1 [FW2 => TW]
       */
      if (sskb->pskb.seqno ==
            (pcm->rec_req_pkt.seqno + pcm->rec_req_pkt.data_len)
          && sskb->pskb.seqack == (pcm->rec_req_pkt.seqack+1)) {
        pcm->state = TCP_FIN_WAIT2;
        goto exit;
      }

      /*
       * observed seq in https ab:
       * C, S, PSHACK s,a,l=31 [EST => EST]
       * S, C, FINACK a,s [EST => FW1]
       * C, S, FINACK s+l,a [FW1 => CLOSING]
       * S, C, ACK a+1,s+l+1 [CLOSING => MYSTATE]
       * C, S, ACK s+l+1,a+1 [MYSTATE => CLOSE]
       */
      if (sskb->pskb.seqno ==
            (pcm->rec_req_pkt.seqno + pcm->rec_req_pkt.data_len)
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqack) {
        pcm->state = TCP_CLOSING;
        goto exit;
      }

    } else if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      /*
       * observed seq in https ab:
       * S, C, FINPSHACK s,a,l=31 [EST => FW1]
       * C, S, ACK, a,s+l+1 [FW1 => FW2]
       * C, S, PSHACK, a,s+l+1,l=31 [FW2 => FW2]
       * C, S, FINACK, a+l,s+l+1 [FW2 => FW2]
       *
       * (a) S, C, ACK, s+l+1,a+l+1 [FW2 => TW]
       *
       * (b) C, S, ACK a+l+1,s+l+1 [FW2 => FW2]
       *     S, C, ACK s+l+1,a+l+1 [FW2 => CLOSE]
       */
      if (sskb->pskb.seqno == pcm->last_send_pkt.seqack
          && sskb->pskb.seqack ==
            (pcm->last_send_pkt.seqno + pcm->last_send_pkt.data_len + 1)) {
        pcm->state = TCP_FIN_WAIT2;
        goto exit;
      }

      /*
       * observed seq in https ab:
       * S, C, FINACK s,a [EST => FW1]
       * C, S, ACK, a,s+1 [FW1 => FW2]
       * C, S, PSHACK, a,s+1,l=31 [FW2 => FW2]
       * C, S, FINACK, a+l,s+1 [FW2 => FW2]
       * S, C, ACK, s+1,a+l+1 [FW2 => TW]
       */
      if (sskb->pskb.seqno == pcm->last_send_pkt.seqack
          && sskb->pskb.seqack == (pcm->last_send_pkt.seqno + 1)) {
        pcm->state = TCP_FIN_WAIT2;
        goto exit;
      }

      /*
       * simultaneous ACK, PSHACK, FINACK from client along with server's FINACK
       * S, C, FINACK s,a [EST => FW1]
       * C, S, ACK a,s [FW1 => CLOSING]
       * C, S, ACK a,s,l=31 [CLOSING => CLOSING]
       * C, S, FINACK a+l,s [CLOSING => CLOSING]
       * S, C, ACK s+1,a+l+1 [CLOSING => TW]
       */
      if (sskb->pskb.seqno == pcm->last_send_pkt.seqack
          && sskb->pskb.seqack == pcm->last_send_pkt.seqno) {
        pcm->state = TCP_CLOSING;
        goto exit;
      }
    } else if (sskb->pskb.rst) {
      pcm->state = TCP_CLOSE; //
      goto exit;
    }

    goto exit;
  }

  if (pcm->state == TCP_FIN_WAIT2) {
    if (sskb->pskb.fin) {
      if (sskb->pskb.seqno == pcm->last_send_pkt.seqno
          && sskb->pskb.seqack == pcm->last_send_pkt.seqack) {
        pcm->state = TCP_TIME_WAIT; //
        pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
        goto exit;
      }
    } else if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      /*
       * case observed during enforcement:
       * S, C, FINACK s,a [EST => FW1]
       * C, S, FINACK a,s+1 [FW1 => FW2]
       * C, S, ACK a+1,s+1 [FW2 => MYSTATE2]
       * S, C, ACK s+1,a+1 [MYSTATE2 => CLOSE]
       */
      if (sskb->pskb.seqno == pcm->last_send_pkt.seqack + 1
          && sskb->pskb.seqack == pcm->last_send_pkt.seqno + 1) {
        pcm->state = TCP_MYSTATE2; //**
        goto exit;
      }
    } else if (sskb->pskb.rst) {
      pcm->state = TCP_CLOSE; //
      goto exit;
    }

    goto exit;
  }

  if (pcm->state == TCP_CLOSE_WAIT) {
    if (sskb->pskb.rst) {
      pcm->state = TCP_CLOSE; //
      goto exit;
    }
    goto exit;
  }

  if (pcm->state == TCP_LAST_ACK) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == pcm->last_send_pkt.seqack
          && sskb->pskb.seqack == (pcm->last_send_pkt.seqno + 1)) {
        pcm->state = TCP_CLOSE;
        goto exit; //
      }
    } else if (sskb->pskb.rst) {
      pcm->state = TCP_CLOSE;
      goto exit; //
    }

    goto exit;
  }

  if (pcm->state == TCP_CLOSING) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == (pcm->last_send_pkt.seqack + 1)
          && sskb->pskb.seqack == (pcm->last_send_pkt.seqno + 1)) {
        pcm->state = TCP_MYSTATE; //**
        goto exit;
      }

      /*
       * part of simultaneous shutdown case
       */
      if (sskb->pskb.seqno ==
            (pcm->rec_req_pkt.seqno + pcm->rec_req_pkt.data_len)
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqack) {
        pcm->state = TCP_MYSTATE2; //**
        goto exit;
      }

      /*
       * observed seq in https ab:
       * S, C, FINACK s,a [EST => FW1]
       * C, S, PSHACK a,s,l=31 [FW1 => CLOSING]
       * C, S, FINACK a+l,s [CLOSING => CLOSING]
       * C, S, ACK a+l+1,s+1 [CLOSING => MYSTATE]
       * S, C, ACK s+1,a+l+1 [MYSTATE => CLOSE]
       */
      if (sskb->pskb.seqno == (pcm->rec_req_pkt.seqno + 1)
          && sskb->pskb.seqack == (pcm->rec_req_pkt.seqack + 1)) {
        pcm->state = TCP_MYSTATE; //**
        goto exit;
      }

    } else if (sskb->pskb.rst) {
      pcm->state = TCP_TIME_WAIT;
      pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
      goto exit; //
    }

    goto exit;
  }

  if (pcm->state == TCP_MYSTATE2) {
    /*
     * observed seq in https ab:
     * S, C, FINACK s,a [EST => FW1]
     * C, S, ACK a,s [FW1 => CLOSING]
     * C, S, PSHACK, a,s,l=31 [CLOSING => MYSTATE2]
     * C, S, FINACK, a+l,s [MYSTATE2 => MYSTATE2]
     * C, S, ACK a+l+1,s+1 [MYSTATE2 => MYSTATE]
     * S, C, ACK s+1,a+l+1 [MYSTATE => CLOSE]
     */
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == (pcm->rec_req_pkt.seqno + 1)
          && sskb->pskb.seqack == (pcm->rec_req_pkt.seqack + 1)) {
        pcm->state = TCP_MYSTATE;
        goto exit; //
      }
    } else if (sskb->pskb.rst) {
      pcm->state = TCP_CLOSE; //
      goto exit;
    }

    goto exit;
  }

  if (pcm->state == TCP_MYSTATE) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == (pcm->last_send_pkt.seqack + 1)
          && sskb->pskb.seqack == (pcm->last_send_pkt.seqno + 1)) {
        pcm->state = TCP_TIME_WAIT;
        pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
        goto exit; //
      }

      if (sskb->pskb.seqno == pcm->last_send_pkt.seqack
          && sskb->pskb.seqack == pcm->last_send_pkt.seqno) {
        pcm->state = TCP_CLOSE; //
        goto exit;
      }

    } else if (sskb->pskb.rst) {
      pcm->state = TCP_CLOSE;
      goto exit; //
    }

    goto exit;
  }

  if (pcm->state == TCP_MYSTATE3) {
    if (sskb->pskb.rst) {
      pcm->state = TCP_TIME_WAIT; //
      pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
      goto exit;
    }
  }

exit:
  new_state = pcm->state;
  iprint(LVL_DBG, "pcm [%pI4 %u, %pI4 %u] RX %d => %d\n"
      "last sent (%u:%u) last recv (%u:%u) seq (%u:%u) [%d %d %d %d %d %d %d %d]"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , old_state, new_state
      , pcm->last_send_pkt.seqno, pcm->last_send_pkt.seqack
      , pcm->rec_req_pkt.seqno, pcm->rec_req_pkt.seqack
      , sskb->pskb.seqno, sskb->pskb.seqack
      , sskb->pskb.fin, sskb->pskb.syn, sskb->pskb.rst, sskb->pskb.psh
      , sskb->pskb.ack, sskb->pskb.urg, sskb->pskb.ece, sskb->pskb.cwr
      );
  return;
}

void
update_pcm_state_send(profile_conn_map_t *pcm, sme_sk_buff_t *sskb)
{
  int old_state = pcm->state;
  int new_state = 0;

  if (pcm->state == TCP_LISTEN) {
      if (sskb->pskb.syn) {
        pcm->state = TCP_SYN_RECV;
        goto exit;
      }

      goto exit;
  }

  if (pcm->state == 0) {
    if (sskb->pskb.syn) {
      pcm->state = TCP_SYN_SENT;
      goto exit;
    }

    goto exit;
  }

  if (pcm->state == TCP_SYN_RECV) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      pcm->state = TCP_ESTABLISHED;
      goto exit;
    }

    goto exit;
  }

  /*
   * shutdown initiated from this pcm
   */
  if (pcm->state == TCP_ESTABLISHED) {
    if (sskb->pskb.fin) {
      pcm->state = TCP_FIN_WAIT1;
      goto exit;
    }

    goto exit;
  }

  if (pcm->state == TCP_MYSTATE3) {
    if (sskb->pskb.fin) {
      /*
       * things fell into order somehow
       * C, S, PSHACK s,a,l=31 [EST => EST]
       * C, S, FINACK s+l,a [EST => MYSTATE3]
       * S, C, FINACK a,s+l+1 [MYSTATE3 => LAST_ACK]
       * C, S, ACK s+l+1,a+1 [LAST_ACK => CLOSE]
       */
      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqno + 1) {
        pcm->state = TCP_LAST_ACK;
        goto exit;
      }

      /*
       * due to a possibility of sent pkts arriving ooo at pacer,
       * setting to TCP_CLOSING can fail at times.
       */
      pcm->state = TCP_MYSTATE; //**
      goto exit;
    }

    goto exit;
  }

  /*
   * synthetic case observed during ab enforcement as:
   * S, C, FINACK s,a [EST => FW1]
   * C, S, PSHACK a,s+1,l [FW1 => FW1]
   * C, S, ACK a+l,s+1 [FW1 => FW1]
   * C, S, FINACK a+l,s+1 [FW1 => FW1]
   * S, C, ACK s+1,a+l+1 [FW1 => TW]
   * ideally we want the second last step to lead to FW2,
   * so that the FW2 case below takes care of it. but we
   * do not have the length info of the PSHACK pkt anymore
   * to be able to do.
   */
  if (pcm->state == TCP_FIN_WAIT1) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqack + 1) {
        pcm->state = TCP_TIME_WAIT;
        pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
        goto exit;
      }
    }

    goto exit;
  }

  if (pcm->state == TCP_FIN_WAIT2) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && sskb->pskb.seqack == (pcm->rec_req_pkt.seqno + 1)) {
        pcm->state = TCP_TIME_WAIT; //
        pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
        goto exit;
      }

      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqno) {
        pcm->state = TCP_CLOSE; //
        goto exit;
      }

      goto exit;
    }
  }

  // htonl is not required on outgoing pkts, but I added it for the sake of
  // uniformity of implementation of send and recv functions.
  if (pcm->state == TCP_CLOSE_WAIT) {
    if (sskb->pskb.fin) {
      if ((sskb->pskb.seqno ==
          (pcm->last_send_pkt.seqno + pcm->last_send_pkt.data_len))
            && sskb->pskb.seqack == (pcm->rec_req_pkt.seqno + 1)) {
        pcm->state = TCP_LAST_ACK;
        goto exit;
      }

      if ((sskb->pskb.seqno ==
          (pcm->last_send_pkt.seqno + pcm->last_send_pkt.data_len))
            && sskb->pskb.seqack == pcm->rec_req_pkt.seqno) {
        pcm->state = TCP_CLOSING;
        goto exit;
      }

#if 0
      /*
       * extremely complicated seq observed in https ab:
       * C, S, PSHACK s,a,l=31 [EST => EST]
       * C, S, FINACK s+l,a [EST => CW]
       * S, C, FINACK a,s [CW => CLOSING]
       * S, C, ACK a+1,s+l+1 [CLOSING => MYSTATE]
       * C, S, ACK s+l+1,a+1 [MYSTATE => CLOSE]
       *
       * last two pkts can also be reordered. it leads to the same state seq:
       * C, S, ACK s+l+1,a+1 [CLOSING => MYSTATE]
       * S, C, ACK a+1,s+l+1 [MYSTATE => CLOSE]
       */
      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && (sskb->pskb.seqack + pcm->rec_req_pkt.data_len) ==
              pcm->rec_req_pkt.seqno) {
        iprint(LVL_INFO, "pcm [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]\n"
          "last sent (%u:%u), last recv (%u:%u)\n"
          "flags[FIN,SYN,RST,PSH,ACK,URG,ECE,CWR]: %d %d %d %d %d %d %d %d"
          ", seq (%u:%u)"
          , (void *) &pcm->sec_conn_id.src_ip, pcm->sec_conn_id.src_port
          , (void *) &pcm->sec_conn_id.in_ip, pcm->sec_conn_id.in_port
          , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
          , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
          , pcm->last_send_pkt.seqno, pcm->last_send_pkt.seqack
          , pcm->rec_req_pkt.seqno, pcm->rec_req_pkt.seqack
          , sskb->pskb.fin, sskb->pskb.syn, sskb->pskb.rst, sskb->pskb.psh
          , sskb->pskb.ack, sskb->pskb.urg, sskb->pskb.ece, sskb->pskb.cwr
          , sskb->pskb.seqno, sskb->pskb.seqack);

        pcm->state = TCP_CLOSING; //**
        return;
      }
#endif
    }

    goto exit;
  }

  if (pcm->state == TCP_CLOSING) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == (pcm->rec_req_pkt.seqack + 1)
          && sskb->pskb.seqack == (pcm->rec_req_pkt.seqno + 1)) {
        pcm->state = TCP_MYSTATE; //**
        goto exit;
      }

      // part of simultaneous shutdown case
      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqno + 1) {
        pcm->state = TCP_TIME_WAIT; //
        pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
        goto exit;
      }
    }

    goto exit;
  }

  if (pcm->state == TCP_MYSTATE) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == (pcm->rec_req_pkt.seqack + 1)
          && sskb->pskb.seqack == (pcm->rec_req_pkt.seqno + 1)) {
        pcm->state = TCP_TIME_WAIT;
        pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
        goto exit; //
      }

      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqno) {
        pcm->state = TCP_CLOSE; //
        goto exit;
      }
    }

    goto exit;
  }

  if (pcm->state == TCP_MYSTATE2) {
    if (sskb->pskb.ack && !sskb->pskb.fin && !sskb->pskb.syn
        && !sskb->pskb.rst) {
      if (sskb->pskb.seqno == (pcm->rec_req_pkt.seqack + 1)
          && sskb->pskb.seqack == (pcm->rec_req_pkt.seqno + 1)) {
        pcm->state = TCP_TIME_WAIT;
        pcm->tw_timeout = get_current_time(SCALE_NS) + TCP_TMO;
        goto exit; //
      }

      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && sskb->pskb.seqack == (pcm->rec_req_pkt.seqno + 1)) {
        pcm->state = TCP_CLOSE; //
        goto exit;
      }

      /*
       * make sure this does not break something else completely.
       */
      if (sskb->pskb.seqno == pcm->rec_req_pkt.seqack
          && sskb->pskb.seqack == pcm->rec_req_pkt.seqno) {
        pcm->state = TCP_CLOSE; //
        goto exit;
      }
    }

    goto exit;
  }

exit:
  new_state = pcm->state;
  iprint(LVL_DBG, "pcm [%pI4 %u, %pI4 %u] TX %d => %d\n"
      "last sent (%u:%u) last recv (%u:%u) seq (%u:%u) [%d %d %d %d %d %d %d %d]"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , old_state, new_state
      , pcm->last_send_pkt.seqno, pcm->last_send_pkt.seqack
      , pcm->rec_req_pkt.seqno, pcm->rec_req_pkt.seqack
      , sskb->pskb.seqno, sskb->pskb.seqack
      , sskb->pskb.fin, sskb->pskb.syn, sskb->pskb.rst, sskb->pskb.psh
      , sskb->pskb.ack, sskb->pskb.urg, sskb->pskb.ece, sskb->pskb.cwr
      );
  return;
}

int
add_timestamp_to_profile_conn(sme_sk_buff_t *sskb, profile_conn_map_t *pcm, int8_t idx)
{
  int old_state = 0;
  int new_state = 0;
  char dbg_buff[2048];

  if (!sskb || sskb->type != SKB_PARTIAL_TS)
    return -EINVAL;

  if (!pcm)
    return -EINVAL;

  old_state = pcm->state;
  //We are updating the state while getting the pcm from the hashtable
//  update_pcm_state_recv(pcm, sskb);
  new_state = pcm->state;

  memset(dbg_buff, 0, 2048);
  sprintf(dbg_buff, "RX STATE => %d -- RECV CPU %d\n"
      "src %pI4 %u dst %pI4 %u seq (%u:%u) PAYLOAD %d\n"
      "ip ID %d [%d %d %d %d %d %d %d %d]"
//      "last sent cpu %d ip ID %d len %d seq (%u:%u) "
//      "last recv cpu %u ip ID %d len %d seq(%u:%u)"
      , new_state, sskb->pskb.cpuid
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , sskb->pskb.seqno, sskb->pskb.seqack, sskb->pskb.data_len
      , sskb->pskb.ip_id
      , sskb->pskb.fin, sskb->pskb.syn, sskb->pskb.rst, sskb->pskb.psh
      , sskb->pskb.ack, sskb->pskb.urg, sskb->pskb.ece, sskb->pskb.cwr
//      , pcm->last_send_pkt.cpuid, pcm->last_send_pkt.ip_id
//      , pcm->last_send_pkt.data_len
//      , pcm->last_send_pkt.seqno, pcm->last_send_pkt.seqack
//      , pcm->rec_req_pkt.cpuid, pcm->rec_req_pkt.ip_id, pcm->rec_req_pkt.data_len
//      , pcm->rec_req_pkt.seqno, pcm->rec_req_pkt.seqack
      );

  /*
   * source = sender (remote), and dest = receiver (local)
   */

  if (sskb->pskb.rst
      || (pcm->rec_req_pkt.seqno <= sskb->pskb.seqno
      || (/* seq# wrap around */ pcm->rec_req_pkt.seqno
          + pcm->rec_req_pkt.data_len) <= sskb->pskb.seqno)) {
#if 0
  if (sskb->pskb.rst || sskb->pskb.seqno <= pcm->last_send_pkt.seqack)
#endif
    /*
     * irq queue indices are also copied, but we don't really care about them.
     */
    copy_pskb(&(pcm->rec_req_pkt), &(sskb->pskb), 1);
    iprint(LVL_DBG, "%s", dbg_buff);
  } else {
    iprint(LVL_ERR, "RX OOO%s", dbg_buff);
    SME_ADD_COUNT_POINT_ARR(rx_ooo_cnt, 1, idx);
  }
  idx = idx;

  iprint(LVL_DBG, "added TS, len %d to conn %pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u"
      ", PCM state: %d"
      , sskb->pskb.data_len
      , (void *) &pcm->sec_conn_id.src_ip, pcm->sec_conn_id.src_port
      , (void *) &pcm->sec_conn_id.in_ip, pcm->sec_conn_id.in_port
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port, pcm->state);
  return 0;
}

