/*
 * sme_hashtable.h
 *
 * created on: Nov 8, 2017
 * author: aasthakm
 *
 * hashtable for pcm lookup data structure
 */

#ifndef __SME_HASHTABLE_H__
#define __SME_HASHTABLE_H__

#include "sme_config.h"
#include "list.h"
#include <linux/types.h>
#include <linux/mutex.h>

typedef struct hash_fn {
  uint32_t (*hash) (void *key, int klen, int modulo);
  int (*lookup) (list_t *pcm_list, void *key, int klen, void **entry);
  int (*free) (list_t *pcm_list);
} hash_fn_t;

extern hash_fn_t pmap_hash_fn;
extern hash_fn_t pcm_hash_fn;
extern hash_fn_t flow_hash_fn;
extern hash_fn_t flow_hash_rcu_fn;

typedef struct sme_htable {
  int size;
  list_t *pcm_list;
  spinlock_t *lock;
  hash_fn_t *ht_func;
} sme_htable_t;

int init_htable(sme_htable_t *htbl, int size, hash_fn_t *hfn, int lock);
void cleanup_htable(sme_htable_t *htbl);
int htable_key_idx(sme_htable_t *htbl, void *key, int keylen);
void htable_insert(sme_htable_t *htbl, void *key, int klen, list_t *elem_listp);
void htable_insert_unique(sme_htable_t *htbl, void *key, int klen,
    list_t *elem_listp);
void *htable_lookup(sme_htable_t *htbl, void *key, int klen);

int init_htable_rcu(sme_htable_t *htbl, int size, hash_fn_t *hfn);
void htable_insert_unique_rcu(sme_htable_t *htbl, void *key, int klen,
    list_t *elem_listp);

#endif /* __SME_HASHTABLE_H__ */
