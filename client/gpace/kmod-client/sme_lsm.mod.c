#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x87b79d0, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xc48e49c0, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x1b6314fd, __VMLINUX_SYMBOL_STR(in_aton) },
	{ 0x78c8b53d, __VMLINUX_SYMBOL_STR(lnk_tx_adjust_seq) },
	{ 0x4954b6d7, __VMLINUX_SYMBOL_STR(lnk_rx_handle_tcp_urg) },
	{ 0x7a2af7b4, __VMLINUX_SYMBOL_STR(cpu_number) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xb7562a80, __VMLINUX_SYMBOL_STR(param_ops_charp) },
	{ 0xc0b8aac4, __VMLINUX_SYMBOL_STR(misc_register) },
	{ 0xd30690e2, __VMLINUX_SYMBOL_STR(lnk_is_paced_flow) },
	{ 0xec1aa680, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x20c55ae0, __VMLINUX_SYMBOL_STR(sscanf) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0x964e7d01, __VMLINUX_SYMBOL_STR(lnk_rx_adjust_copied_seq) },
	{ 0xe3ce1813, __VMLINUX_SYMBOL_STR(vm_insert_page) },
	{ 0x7deee48, __VMLINUX_SYMBOL_STR(lnk_rx_adjust_skb_size) },
	{ 0xdb7305a1, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xbdfb6dbb, __VMLINUX_SYMBOL_STR(__fentry__) },
	{ 0x7b9893b2, __VMLINUX_SYMBOL_STR(lnk_print_sock_skb) },
	{ 0x7d5c8af3, __VMLINUX_SYMBOL_STR(vmalloc_to_page) },
	{ 0x1b4ce0ee, __VMLINUX_SYMBOL_STR(param_ops_ushort) },
	{ 0x596421d6, __VMLINUX_SYMBOL_STR(skb_put) },
	{ 0x4feac791, __VMLINUX_SYMBOL_STR(__skb_tx_hash) },
	{ 0x856d7f70, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0x20b92337, __VMLINUX_SYMBOL_STR(lnk_rx_pred_flags) },
	{ 0xcd838184, __VMLINUX_SYMBOL_STR(lnk_rx_disable_coalesce) },
	{ 0xd3764de6, __VMLINUX_SYMBOL_STR(skb_copy_bits) },
	{ 0x7c6736b9, __VMLINUX_SYMBOL_STR(lnk_rx_handle_dummy) },
	{ 0x30fbdb88, __VMLINUX_SYMBOL_STR(misc_deregister) },
	{ 0xc8ceed5c, __VMLINUX_SYMBOL_STR(lnk_tx_adjust_urg) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "352A9D5DDA5971A2F9FA345");
