#include "sme_config.h"
#include "sme_debug.h"

#include "mem_desc.h"

#include <xen/page.h>

int
generate_mdesc(void *vaddr_arr, int sizeof_vobj, int max_vobjs,
    mem_desc_t *md_arr, int max_md)
{
  int velem_it = 0;
  int vfrag_it = 0;
  int start_velem_it = 0, n_velems = 0;
  uint64_t start_vfrag_maddr = 0;
  uint64_t next_vfrag_maddr = 0;

  for (velem_it = 0; velem_it < max_vobjs; ) {
    if (!start_vfrag_maddr) {
      start_vfrag_maddr =
        (virt_to_machine(vaddr_arr + (velem_it*sizeof_vobj))).maddr;
      start_velem_it = velem_it;
    }

    velem_it++;
    n_velems = (velem_it - start_velem_it);
    next_vfrag_maddr =
      (virt_to_machine(vaddr_arr + (velem_it*sizeof_vobj))).maddr;
    if (next_vfrag_maddr != start_vfrag_maddr + (n_velems*sizeof_vobj)) {
      iprint(LVL_DBG, "[%d] sz %d st %d end %d n %d st maddr %llu n maddr %llu"
          , vfrag_it, sizeof_vobj, start_velem_it, velem_it-1, n_velems
          , start_vfrag_maddr, next_vfrag_maddr
          );
      md_arr[vfrag_it].maddr = start_vfrag_maddr;
      md_arr[vfrag_it].n_elems = n_velems;
      start_vfrag_maddr = 0;
      start_velem_it = 0;
      n_velems = 0;
      vfrag_it++;
    }

    if (vfrag_it == max_md && velem_it < max_vobjs)
      return -ENOMEM;
  }

  iprint(LVL_DBG, "[%d] sz %d st %d end %d n %d st maddr %llu n maddr %llu"
      , vfrag_it, sizeof_vobj, start_velem_it, velem_it-1, n_velems
      , start_vfrag_maddr, next_vfrag_maddr
      );
  /*
   * all elements allocated contiguously, and accounted for in the loop above.
   * it's just that before the loop termination condition was checked, we checked
   * if the next element's address is contiguous with the last element's address.
   * however, anyways there is no valid address after the last element, so there
   * is no second frag to allocate.
   */
  if (!start_vfrag_maddr && !n_velems)
    return vfrag_it;

  md_arr[vfrag_it].maddr = start_vfrag_maddr;
  md_arr[vfrag_it].n_elems = n_velems;
  vfrag_it++;

  return vfrag_it;
}

int
get_velem_size_off(int elem_it, int sizeof_vobj)
{
  int n_elem_per_page = PAGE_SIZE/sizeof_vobj;
  int n_page = elem_it / n_elem_per_page;
  int elem_idx_in_page = elem_it % n_elem_per_page;
  int offset = n_page * PAGE_SIZE + elem_idx_in_page * sizeof_vobj;
  return offset;
}

int
generate_mdesc_palign(void *vaddr_arr, int sizeof_vobj, int max_vobjs,
    mem_desc_t *md_arr, int max_md)
{
  int velem_it = 0;
  int vfrag_it = 0;
  int start_velem_it = 0, n_velems = 0;
  uint64_t start_vfrag_maddr = 0;
  uint64_t next_vfrag_maddr = 0;
  void *start_vfrag_vaddr = NULL;
  void *next_vfrag_vaddr = NULL;

  for (velem_it = 0; velem_it < max_vobjs; ) {
    if (!start_vfrag_maddr) {
      start_vfrag_vaddr = vaddr_arr + get_velem_size_off(velem_it, sizeof_vobj);
      start_vfrag_maddr = (virt_to_machine(start_vfrag_vaddr)).maddr;
      start_velem_it = velem_it;
    }

    velem_it++;
    n_velems = (velem_it - start_velem_it);

    next_vfrag_vaddr = vaddr_arr + get_velem_size_off(velem_it, sizeof_vobj);
    next_vfrag_maddr = (virt_to_machine(next_vfrag_vaddr)).maddr;

    if (next_vfrag_maddr != start_vfrag_maddr + get_velem_size_off(n_velems, sizeof_vobj)) {
      iprint(LVL_DBG, "[%d] sz %d st %d end %d n %d st maddr 0x%llx n maddr 0x%llx"
          , vfrag_it, sizeof_vobj, start_velem_it, velem_it-1, n_velems
          , start_vfrag_maddr, next_vfrag_maddr
          );
      md_arr[vfrag_it].maddr = start_vfrag_maddr;
      md_arr[vfrag_it].n_elems = n_velems;
      start_vfrag_maddr = 0;
      start_velem_it = 0;
      n_velems = 0;
      vfrag_it++;
    }

    if (vfrag_it == max_md && velem_it < max_vobjs)
      return -ENOMEM;
  }

  md_arr[vfrag_it].maddr = start_vfrag_maddr;
  md_arr[vfrag_it].n_elems = n_velems;
  iprint(LVL_DBG, "[%d] sz %d st %d end %d n %d st maddr 0x%llx n maddr 0x%llx"
      , vfrag_it, sizeof_vobj, start_velem_it, velem_it-1, n_velems
      , start_vfrag_maddr, next_vfrag_maddr
      );
  vfrag_it++;

  return vfrag_it;
}

int
round_up_pow2(int x)
{
  int y = x--;
  y |= (y >> 1);
  y |= (y >> 2);
  y |= (y >> 4);
  y |= (y >> 8);
  y |= (y >> 16);
  y++;
  return y;
}
