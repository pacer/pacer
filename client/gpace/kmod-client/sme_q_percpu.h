/*
 * sme_skb_percpu.h
 *
 * created on: Apr 20, 2017
 * author: aasthakm
 *
 * per-cpu ring buffer
 */

#ifndef __SME_SKB_PERCPU_H__
#define __SME_SKB_PERCPU_H__

#include "sme_config.h"
#include "sme_skb.h"
#include "sme_q_common.h"

enum {
  SME_Q_DMA = 0,
  SME_Q_CTRL_TS,
  MAX_NUM_SME_Q
};

typedef struct sme_qdesc {
  int pcpu_qtype;
  int qtype;
} sme_qdesc_t;

DECLARE_PER_CPU(sme_q_t[MAX_NUM_SME_Q], sme_pcpu_q);
DECLARE_PER_CPU(sme_q_t[MAX_NUM_SME_Q], pacer_pcpu_q);

/*
 * =====
 * sme_q
 * =====
 */

static inline void init_cpu_sme_q(void *type)
{
  sme_q_t *q = NULL;
  sme_qdesc_t *sq = (sme_qdesc_t *) type;
  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: q = get_cpu_ptr(&sme_pcpu_q[sq->qtype]);
      break;
    case PCPU_QT_PACER: q = get_cpu_ptr(&pacer_pcpu_q[sq->qtype]);
      break;
  }

  init_sme_q(q);

  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: put_cpu_ptr(&sme_pcpu_q[sq->qtype]);
      break;
    case PCPU_QT_PACER: put_cpu_ptr(&pacer_pcpu_q[sq->qtype]);
      break;
  }

  iprint(LVL_DBG, "Init cpu Q(%d,%d)", sq->pcpu_qtype, sq->qtype);
}

static inline void cleanup_cpu_sme_q(void *type)
{
  sme_q_t *q = NULL;
  sme_qdesc_t *sq = (sme_qdesc_t *) type;
  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: q = get_cpu_ptr(&sme_pcpu_q[sq->qtype]);
      break;
    case PCPU_QT_PACER: q = get_cpu_ptr(&pacer_pcpu_q[sq->qtype]);
      break;
  }

  cleanup_sme_q(q);

  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: put_cpu_ptr(&sme_pcpu_q[sq->qtype]);
      break;
    case PCPU_QT_PACER: put_cpu_ptr(&pacer_pcpu_q[sq->qtype]);
      break;
  }

  iprint(LVL_DBG, "Cleanup cpu Q(%d,%d)", sq->pcpu_qtype, sq->qtype);
}

static inline int enqueue_cpu_sme_q(sme_sk_buff_t *sskb, sme_qdesc_t *sq)
{
  sme_q_t *q = NULL;
  int ret = 0;
  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: q = get_cpu_ptr(&sme_pcpu_q[sq->qtype]);
      break;
    case PCPU_QT_PACER: q = get_cpu_ptr(&pacer_pcpu_q[sq->qtype]);
      break;
  }

  ret = enqueue_sme_q(q, sskb);

  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: put_cpu_ptr(&sme_pcpu_q[sq->qtype]);
      break;
    case PCPU_QT_PACER: put_cpu_ptr(&pacer_pcpu_q[sq->qtype]);
      break;
  }

  return ret;
}

static inline int dequeue_cpu_sme_q(sme_sk_buff_t *sskb, sme_qdesc_t *sq)
{
  int ret = 0;
  sme_q_t *q = NULL;

  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: q = get_cpu_ptr(&sme_pcpu_q[sq->qtype]);
      break;
    case PCPU_QT_PACER: q = get_cpu_ptr(&pacer_pcpu_q[sq->qtype]);
      break;
  }

  ret = dequeue_sme_q(q, sskb);

  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: put_cpu_ptr(&sme_pcpu_q[sq->qtype]);
      break;
    case PCPU_QT_PACER: put_cpu_ptr(&pacer_pcpu_q[sq->qtype]);
      break;
  }

  return ret;
}

static inline sme_q_t *per_cpu_sme_q(int cpu, sme_qdesc_t *sq)
{
  sme_q_t *q = NULL;
  switch (sq->pcpu_qtype) {
    case PCPU_QT_IRQ: q = per_cpu_ptr(&sme_pcpu_q[sq->qtype], cpu);
      break;
    case PCPU_QT_PACER: q = per_cpu_ptr(&pacer_pcpu_q[sq->qtype], cpu);
      break;
  }

  return q;
}

#endif /* __SME_SKB_PERCPU_H__ */
