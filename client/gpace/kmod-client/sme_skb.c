/*
 * sme_skb.c
 *
 * created on: Apr 2, 2017
 * author: aasthakm
 *
 * functions to manage a single ring buffer entry
 */

#include "sme_skb.h"
#include "sme_q_common.h"
#include "sme_common.h"
#include "sme_debug.h"
#include "sme_statistics.h"

#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"
#include <linux/smp.h>
#include <linux/etherdevice.h>

//SME_DECL_STAT_EXTERN(pad_cnt_inline);
//SME_DECL_STAT_EXTERN(pad_cnt_alloc);
//SME_DECL_STAT_EXTERN(pad_size);

void
init_partial_skb(partial_sk_buff_t *pskb, uint32_t s_addr, uint32_t d_addr,
    uint16_t s_port, uint16_t d_port, uint32_t seqno, uint32_t seqack,
    unsigned char *s_mac, int s_mac_len, unsigned char *d_mac, int d_mac_len,
    uint8_t fin, uint8_t syn, uint8_t rst, uint8_t psh,
    uint8_t ack, uint8_t urg, uint8_t ece, uint8_t cwr, uint16_t ip_id,
    uint8_t protocol, uint64_t timestamp, struct net_device *dev)
{
  if (!pskb)
    return;

  memset(pskb, 0, sizeof(partial_sk_buff_t));
  pskb->saddr = s_addr;
  pskb->daddr = d_addr;
  pskb->source = s_port;
  pskb->dest = d_port;
  pskb->seqno = seqno;
  pskb->seqack = seqack;
  pskb->protocol = protocol;
  pskb->fin = fin;
  pskb->syn = syn;
  pskb->rst = rst;
  pskb->psh = psh;
  pskb->ack = ack;
  pskb->urg = urg;
  pskb->ece = ece;
  pskb->cwr = cwr;
  pskb->ip_id = ip_id;

  pskb->in_timestamp = timestamp;
  pskb->cpuid = -1;

  eth_zero_addr((u8 *) pskb->s_mac);
  eth_zero_addr((u8 *) pskb->d_mac);

  if (s_mac && s_mac_len)
    ether_addr_copy(pskb->s_mac, s_mac);
  if (d_mac && d_mac_len)
    ether_addr_copy(pskb->d_mac, d_mac);

  return;
}

void
copy_pskb(partial_sk_buff_t *d_pskb, partial_sk_buff_t *s_pskb, int shallow)
{
  if (!d_pskb || !s_pskb)
    return;

  d_pskb->saddr = s_pskb->saddr;
  d_pskb->daddr = s_pskb->daddr;
  d_pskb->source = s_pskb->source;
  d_pskb->dest = s_pskb->dest;
  d_pskb->seqno = s_pskb->seqno;
  d_pskb->seqack = s_pskb->seqack;
  d_pskb->protocol = s_pskb->protocol;
  d_pskb->fin = s_pskb->fin;
  d_pskb->syn = s_pskb->syn;
  d_pskb->rst = s_pskb->rst;
  d_pskb->psh = s_pskb->psh;
  d_pskb->ack = s_pskb->ack;
  d_pskb->urg = s_pskb->urg;
  d_pskb->ece = s_pskb->ece;
  d_pskb->cwr = s_pskb->cwr;
  d_pskb->ip_id = s_pskb->ip_id;
  d_pskb->in_timestamp = s_pskb->in_timestamp;
  d_pskb->cpuid = s_pskb->cpuid;

  if (s_pskb->s_mac && d_pskb->s_mac)
    ether_addr_copy(d_pskb->s_mac, s_pskb->s_mac);
  if (s_pskb->d_mac && d_pskb->d_mac)
    ether_addr_copy(d_pskb->d_mac, s_pskb->d_mac);

  d_pskb->data_len = s_pskb->data_len;
#if 0
  if (shallow) {
    d_pskb->data = s_pskb->data;
    d_pskb->data_len = s_pskb->data_len;
  } else {
    if (s_pskb->data && s_pskb->data_len) {
      d_pskb->data = (char *) kzalloc(s_pskb->data_len, GFP_KERNEL);
      if (d_pskb->data) {
        memcpy(d_pskb->data, s_pskb->data, s_pskb->data_len);
        d_pskb->data_len = s_pskb->data_len;
      }
    }
  }
#endif
}

void
set_pskb_idx_ts(partial_sk_buff_t *pskb, struct net_device *dev, uint8_t fp_idx,
    uint16_t bd_prod, uint16_t bd_cons, uint16_t comp_prod, uint16_t comp_cons,
    uint64_t timestamp)
{
  if (!pskb)
    return;

  pskb->in_timestamp = timestamp;
}

void
set_pskb_cpuid(partial_sk_buff_t *pskb, int8_t cpuid)
{
  if (!pskb)
    return;

  pskb->cpuid = cpuid;
}

void
set_sskb_idx_ts(sme_sk_buff_t *sskb, struct net_device *dev, uint8_t fp_idx,
    uint16_t bd_prod, uint16_t bd_cons, uint16_t comp_prod, uint16_t comp_cons,
    uint64_t timestamp)
{
  if (!sskb)
    return;

  sskb->type = SKB_PARTIAL_TS;
  sskb->fp_index = fp_idx;
  sskb->rx_bd_prod = bd_prod;
  sskb->rx_bd_cons = bd_cons;
  sskb->rx_comp_prod = comp_prod;
  sskb->rx_comp_cons = comp_cons;
  sskb->dev = dev;
  set_pskb_idx_ts(&sskb->pskb, dev, fp_idx, bd_prod, bd_cons,
      comp_prod, comp_cons, timestamp);
}

void
set_sskb_flow_info(sme_sk_buff_t *sskb, uint32_t s_addr, uint32_t d_addr,
    uint16_t s_port, uint16_t d_port, uint32_t seqno,  uint32_t seqack,
    uint8_t fin, uint8_t syn, uint8_t rst, uint8_t psh,
    uint8_t ack, uint8_t urg, uint8_t ece, uint8_t cwr, uint16_t ip_id,
    unsigned char *s_mac, int s_mac_len, unsigned char *d_mac, int d_mac_len,
    uint8_t protocol, struct net_device *dev)
{
  if (!sskb)
    return;

  sskb->pskb.saddr = s_addr;
  sskb->pskb.daddr = d_addr;
  sskb->pskb.source = s_port;
  sskb->pskb.dest = d_port;
  sskb->pskb.seqno = seqno;
  sskb->pskb.seqack = seqack;
  sskb->pskb.protocol = protocol;
  sskb->pskb.fin = fin;
  sskb->pskb.syn = syn;
  sskb->pskb.rst = rst;
  sskb->pskb.psh = psh;
  sskb->pskb.ack = ack;
  sskb->pskb.urg = urg;
  sskb->pskb.ece = ece;
  sskb->pskb.cwr = cwr;
  sskb->pskb.ip_id = ip_id;
  sskb->dev = dev;

  if (s_mac && s_mac_len)
    ether_addr_copy((u8 *) sskb->pskb.s_mac, (u8 *) s_mac);
  if (d_mac && d_mac_len)
    ether_addr_copy((u8 *) sskb->pskb.d_mac, (u8 *) d_mac);
}

void
init_sme_skb(sme_sk_buff_t *sskb, uint8_t type, struct sk_buff *skb,
    struct net_device *dev, partial_sk_buff_t *pskb)
{
  if (!sskb)
    return;

  memset(sskb, 0, sizeof(sme_sk_buff_t));
  sskb->type = type;
  sskb->skb = skb;
  sskb->dev = dev;
  copy_pskb(&sskb->pskb, pskb, 1);

  INIT_LIST_HEAD(&sskb->fifo_listp);

  return;
}

void
clone_sme_skb(sme_sk_buff_t *dst_sskb, sme_sk_buff_t *src_sskb, int shallow)
{
  partial_sk_buff_t *s_pskb = NULL, *d_pskb = NULL;
  if (!dst_sskb || !src_sskb)
    return;

  dst_sskb->type = src_sskb->type;
  dst_sskb->fp_index = src_sskb->fp_index;
  dst_sskb->rx_bd_prod = src_sskb->rx_bd_prod;
  dst_sskb->rx_bd_cons = src_sskb->rx_bd_cons;
  dst_sskb->rx_comp_prod = src_sskb->rx_comp_prod;
  dst_sskb->rx_comp_cons = src_sskb->rx_comp_cons;
  dst_sskb->dev = src_sskb->dev;
  dst_sskb->skb = src_sskb->skb;

  s_pskb = &src_sskb->pskb;
  d_pskb = &dst_sskb->pskb;
  copy_pskb(d_pskb, s_pskb, shallow);

  INIT_LIST_HEAD(&dst_sskb->fifo_listp);
  return;
}

void
cleanup_sme_skb(sme_sk_buff_t *sskb)
{
  if (!sskb)
    return;

  if (sskb->type != SKB_FULL)
    goto cleanup;

  // TODO: figure out where to free skb's

cleanup:
#if 0
  if (sskb->pskb.data && sskb->pskb.data_len) {
    kfree(sskb->pskb.data);
  }
  sskb->pskb.data = NULL;
#endif
  sskb->pskb.data_len = 0;

  memset(sskb, 0, sizeof(sme_sk_buff_t));
  INIT_LIST_HEAD(&sskb->fifo_listp);

  return;
}

// caller holds appropriate locks
// ==== print functions ====
void
print_sme_skb(sme_sk_buff_t *sskb, int idx)
{
  if (!sskb)
    return;

  switch (sskb->type) {
    case SKB_UNINT:
      iprint(LVL_DBG, "idx %d uninit", idx);
      break;
    case SKB_FULL:
    case SKB_PARTIAL_PROF:
      iprint(LVL_DBG, "idx %d sskb type %d, skb %p", idx, sskb->type, sskb->skb);
      break;
    case SKB_PARTIAL_TS:
      iprint(LVL_DBG, "idx %d sskb type %d, src %pI4 %u, dst %pI4 %u, TS %llu"
        , idx, sskb->type, (void *) &sskb->pskb.saddr, sskb->pskb.source
        , (void *) &sskb->pskb.daddr, sskb->pskb.dest, sskb->pskb.in_timestamp);
      break;
  }
}
