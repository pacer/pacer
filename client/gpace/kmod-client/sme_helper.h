#ifndef __SME_HELPER_H__
#define __SME_HELPER_H__

#include <net/tcp.h> // for TCP_TIMEWAIT_LEN

#include "list.h"
#include "profile_map.h"
#include "sme_skb.h"

#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"

/*
 * new state in the TCP FSM.
 * added for our pcm logging and cleanup.
 */
#define TCP_MYSTATE 13
#define TCP_MYSTATE2 14
#define TCP_MYSTATE3 15
#define TCPF_MYSTATE (1 << 13)
#define TCPF_MYSTATE2 (1 << 14)
#define TCPF_MYSTATE3 (1 << 15)

//#define TCP_TMO (TCP_TIMEWAIT_LEN*1)
#define TCP_TMO (_AC(60*1000*1000*1000, UL))
#define PCM_EXPIRE_THRESHOLD 20000

/*
 * ================
 * helper functions
 * ================
 */
static inline int
flow_fpidx_to_tsq_idx(int fpidx)
{
  return fpidx;
}

int get_profile_conn_info_from_skb_header(uint32_t src_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t dst_port, list_t *scm_list,
    sme_channel_map_t **scm_p, profile_conn_map_t **pcm_p, int8_t idx,
    int update, int is_send_side, sme_sk_buff_t *sskb);
void init_new_profile_conn_info(uint32_t src_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t dst_port, struct net_device *dev,
    unsigned char *s_mac, unsigned char *d_mac, int8_t cpuid,
    sme_channel_map_t *scm, profile_conn_map_t **pcm_p, int8_t idx);
void add_initial_sme_channel_map(list_t *scm_list);

void update_pcm_state_recv(profile_conn_map_t *pcm, sme_sk_buff_t *sskb);
void update_pcm_state_send(profile_conn_map_t *pcm, sme_sk_buff_t *sskb);

int add_timestamp_to_profile_conn(sme_sk_buff_t *sskb, profile_conn_map_t *pcm,
    int8_t idx);

int sme_bnx2x_send_dma(struct sk_buff *skb, struct net_device *dev,
    profile_conn_map_t *pcm);
int pacer_bnx2x_prep_dma(struct sk_buff *skb, struct net_device *dev,
    profile_conn_map_t *pcm);
int pacer_merged_nicq_bnx2x_tx_int(struct bnx2x *bp, struct bnx2x_fp_txdata *txdata);
#if CONFIG_XEN_PACER
void prepare_dummy_nicq(global_nicq_hdr_t *nicq, struct sk_buff **dummy_skb,
    dma_addr_t *dummy_dmaddr);
#endif
int sme_bnx2x_tx_int(struct bnx2x *bp, struct bnx2x_fp_txdata *txdata);
void sme_bnx2x_free_tx_skbs_queue(struct bnx2x_fastpath *fp);

#endif /* __SME_HELPER_H__ */
