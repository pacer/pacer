/*
 * mem_desc.h
 *
 * created on: Nov 18, 2018
 * author: aasthakm
 *
 * utility datastructure to store information about
 * contiguous chunks of allocated memory
 */

#ifndef __MEM_DESC_H__
#define __MEM_DESC_H__

#include <linux/types.h>

typedef struct mem_desc {
  uint64_t maddr;
  uint16_t n_elems;
} mem_desc_t;

int get_velem_size_off(int elem_it, int sizeof_vobj);
int generate_mdesc(void *vaddr_arr, int sizeof_vobj, int max_vobjs,
    mem_desc_t *md_arr, int max_md);
int generate_mdesc_palign(void *vaddr_arr, int sizeof_vobj, int max_vobjs,
    mem_desc_t *md_arr, int max_md);

int round_up_pow2(int x);

#endif /* __MEM_DESC_H__ */
