/*
 * sme_skb.h
 *
 * created on: Apr 2, 2017
 * author: aasthakm
 *
 * structure used in the ring buffers shared between
 * the ethernet device driver and the SME pacer thread
 */

#ifndef __SME_SKB_H__
#define __SME_SKB_H__

#include <linux/list.h>
#include <linux/net.h>
#include <linux/socket.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>

#include "sme_config.h"
#include "../../include/config_common.h"

#define ETH_HEADER_LEN ETH_ALEN
#define ETH_PRINT_HDR_LEN (3*ETH_HEADER_LEN)

enum {
  SKB_UNINT = 0,
  SKB_FULL,
  SKB_PARTIAL_TS,
  SKB_PARTIAL_PROF,
  SKB_PROF_REQ,
  SKB_PROF_RSP,
};

typedef struct partial_sk_buff {
  uint64_t in_timestamp;
  uint32_t seqno;
  uint32_t seqack;
  uint32_t saddr;
  uint32_t daddr;
  uint16_t source;
  uint16_t dest;
  uint16_t ip_id;
  uint8_t protocol;
  uint8_t fin:1,
          syn:1,
          rst:1,
          psh:1,
          ack:1,
          urg:1,
          ece:1,
          cwr:1;
  int data_len;
//#if CONFIG_DUMMY != DUMMY_CLONE_SKB
  unsigned char s_mac[ETH_HEADER_LEN];
  unsigned char d_mac[ETH_HEADER_LEN];
//#endif
  /*
   * to check flow mapping to cpus, and
   * if pkts reach OOO at the pacer.
   */
  int8_t cpuid;
} partial_sk_buff_t;

typedef struct sme_sk_buff {
  uint8_t type;
  uint8_t fp_index;
  uint16_t rx_bd_prod;
  uint16_t rx_bd_cons;
  uint16_t rx_comp_prod;
  uint16_t rx_comp_cons;
  struct sk_buff *skb;
  struct net_device *dev;
  struct partial_sk_buff pskb;
  struct list_head fifo_listp;
} sme_sk_buff_t;

// === sme skb functions ===
void init_partial_skb(partial_sk_buff_t *pskb, uint32_t s_addr, uint32_t d_addr,
    uint16_t s_port, uint16_t d_port, uint32_t seqno, uint32_t seqack,
    unsigned char *s_mac, int s_mac_len, unsigned char *d_mac, int d_mac_len,
    uint8_t fin, uint8_t syn, uint8_t rst, uint8_t psh,
    uint8_t ack, uint8_t urg, uint8_t ece, uint8_t cwr, uint16_t ip_id,
    uint8_t protocol, uint64_t timestamp, struct net_device *dev);
void copy_pskb(partial_sk_buff_t *d_pskb, partial_sk_buff_t *s_pskb, int shallow);
void set_pskb_idx_ts(partial_sk_buff_t *pskb, struct net_device *dev,
    uint8_t fp_idx, uint16_t bd_prod, uint16_t bd_cons,
    uint16_t comp_prod, uint16_t comp_cons, uint64_t timestamp);
void set_pskb_cpuid(partial_sk_buff_t *pskb, int8_t cpuid);

void set_sskb_idx_ts(sme_sk_buff_t *pskb, struct net_device *dev,
    uint8_t fp_idx, uint16_t bd_prod, uint16_t bd_cons,
    uint16_t comp_prod, uint16_t comp_cons, uint64_t timestamp);
void set_sskb_flow_info(sme_sk_buff_t *sskb, uint32_t s_addr, uint32_t d_addr,
    uint16_t s_port, uint16_t d_port, uint32_t seqno, uint32_t seqack,
    uint8_t fin, uint8_t syn, uint8_t rst, uint8_t psh,
    uint8_t ack, uint8_t urg, uint8_t ece, uint8_t cwr, uint16_t ip_id,
    unsigned char *s_mac, int s_mac_len, unsigned char *d_mac, int d_mac_len,
    uint8_t protocol, struct net_device *dev);

void init_sme_skb(sme_sk_buff_t *sskb, uint8_t type, struct sk_buff *skb,
    struct net_device *dev, partial_sk_buff_t *pskb);
void clone_sme_skb(sme_sk_buff_t *dst_sskb, sme_sk_buff_t *src_sskb, int shallow);
void cleanup_sme_skb(sme_sk_buff_t *sskb);
void print_sme_skb(sme_sk_buff_t *sskb, int idx);

#endif /* __SME_SKB_H__ */
