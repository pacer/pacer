/*
 * sme_config.h
 *
 * created on: Apr 20, 2017
 * author: aasthakm
 * 
 * config parameters for the kernel module
 */

#ifndef __SME_CONFIG_H__
#define __SME_CONFIG_H__

#include <linux/types.h>
// NUM_TX_BD
#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"

extern int CONFIG_SW_CSUM;
extern int EPOCH_SIZE;
extern int PACER_SPIN_THRESHOLD;
extern int MAX_PKTS_PER_EPOCH;
extern int MAX_OTHER_PER_EPOCH;

/*
 * ==============
 * module configs
 * ==============
 */

/*
 * only log in interception functions, used for ping microbenchmark.
 */
#define SME_DBG_LOG 0
/*
 * timestamp, profile and data sent to pacer thread
 */
#define SME_EXP 1

#define SME_CONFIG SME_EXP

/*
 * =============================================
 * config for guest-hv synchronization mechanism
 * =============================================
 */
#define SYNC_LOCKED   0
#define SYNC_LOCKFREE 1

#define CONFIG_SYNC SYNC_LOCKFREE

/*
 * ====================================
 * config for profile freeing mechanism
 * ====================================
 */
#define PROF_FREE_XEN 0
#define PROF_FREE_TCP 1

#define CONFIG_PROF_FREE  PROF_FREE_TCP

/*
 * =============================
 * config for dummy pkt creation
 * =============================
 */

/*
 * (xenpacer) dummy to nowhere
 */
#define DUMMY_NOACK     0
/*
 * (xenpacer) dummy using OOB flag and urg pointer
 */
#define DUMMY_OOB       1

#define CONFIG_DUMMY DUMMY_OOB

/*
 * =========================================
 * config for per-flow queue for nic buffers
 * =========================================
 */
#define CONFIG_NQ_STATIC  0
#define CONFIG_NQ_DYNAMIC 1

#define CONFIG_NIC_QUEUE  CONFIG_NQ_DYNAMIC

/*
 * profile logging support in kernel
 * 1 - enables profile logging
 */
#define CONFIG_PROF_LOG 0
/*
 * minimal profile logging support in kernel
 * 1 - enables minimal profile logging
 * requires CONFIG_PROF_LOG to be set to 1
 */
#define CONFIG_PROF_MINI_LOG 0

/*
 * config to determine whether to ignore first
 * outgoing syn pkt from the host
 * 0 - do not ignore
 * 1 - ignore
 */
#define IGNORE_SYN_OUT 1

/*
 * config to modify TX queue selection logic,
 * to eliminate locking in pacer thread
 * 1 - enables modification
 */
#define CONFIG_SELECT_TXQ 1

/*
 * kernel statistics
 */
#define SME_CONFIG_STAT 0
#define SME_CONFIG_STAT_DETAIL 0

/*
 * config to sync seq# between gpace and hypace via write_seq or snd_nxt
 * 0 -- write_seq
 * 1 -- snd_nxt (default)
 * changing to 0 requires some changes in the core kernel and recompilation
 * (essentially must revert commit fafbedbc9)
 */
#define CONFIG_SYNC_SNDNXT  1

/*
 * valid combinations of CONFIG_XEN_PACER (XP), CONFIG_XEN_PACER_DB (XPDB)
 * and CONFIG_XEN_PACER_DUMMYQ (XPDQ)
 * XP | XPDB         | XPDQ
 * -------------------------------
 *  0 | don't care   | don't care
 *  1 | 0            | don't care
 *  1 | 1            | 0 => swi based code
 *  1 | 1            | 1 => dummy TXQ based code
 */

/*
 * enable code related to xen based pacer implementation
 * 1 - enables code
 */
#define CONFIG_XEN_PACER 0

/*
 * enable code to ring the doorbell from xen pacer
 * 1 - enables code
 */
#define CONFIG_XEN_PACER_DB 0

/*
 * config for slot sharing by hypace
 * 0 - implies no slot sharing (new code)
 * 1 - implies slot sharing (old code)
 */
#define CONFIG_XEN_PACER_SLOTSHARE  0

/*
 * debug prints level
 */
#define SME_DEBUG_LVL LVL_EXP

/*
 * ==========================
 * numerical constant configs
 * ==========================
 */

#define SME_TCP_MTU 1460

/*
 * TXQ ID on which all paced flows' packets directed
 */
#define SME_PACER_CORE 0

/*
 * # of CPUs used for RX IRQS
 * equal to # RX queues in the NIC
 */
#define RX_NCPUS 8
#define NUM_PACERS 1
#define RDTSC_MUL_FACTOR  1000
#define RDTSC_DIV_FACTOR  3200 //MHz

#define PMAP_HTBL_SIZE 1024

#define PCM_HTBL_SIZE 1024

#define MAX_DBG_BUF_LEN 2048

/*
 * sskb size is 144 bytes
 * alloc pcpu Q of size ~2MB in all
 */
#define TS_QSIZE 4096

#define PCM_QSIZE 4096

/*
 * size of the fifo queue shared between guest and hypervisor
 * 2 extra for sentinel head and tail nodes
 */
#define PROFILE_QSIZE (4096+2)
/*
 * freelist holds indices of the array underlying the lockfree
 * profile queue shared between the guest and hypervisor.
 * the profile queue contains two sentinel elements fixed at
 * indices 0 and arraylen-1. the freelist needs to hold two
 * elements less than the queue, and the indices range from
 * 1 to arraylen-2.
 */
#define PROF_FREELIST_SIZE  ((PROFILE_QSIZE) - 2)

/*
 * size of the fifo queue of NIC idxes in each shared profile slot
 */
#define NIC_DATA_QSIZE  512

#define GLOBAL_NIC_DATA_QSIZE (8 * NUM_TX_BD)

#define MAX_STATQ_ARRAYS  257
#define STATQ_MODULO      257

#define NUM_REQTS_ELEMS (1*1000*1000)

/*
 * timeout value for rto check timer, which periodically checks
 * if there are outstanding dummy packet transmissions that tcp
 * is not aware of.
 */
#define RTO_CHECK_TIMER 25

/*
 * ==================
 * configs for HyPace
 * ==================
 */

/*
 * # hypace cores
 */
#define N_HYPACE    1

/*
 * theoretical max # hypace cores we can support
 * limits the sizes of various statically defined arrays
 */
#define MAX_HP_ARGS 8

/*
 * size of the timerq of timer objects
 */
#define PAC_TIMERQ_SIZE 65536

/*
 * upper bound on incoming ack processing before resuming
 * paused transmission schedule
 */
#define CWND_UPDATE_LATENCY 20000000 // 20ms

/*
 * upper bound on processing a loss retransmission event, before
 * hypace is notified of it approrpriately. used in two cases:
 * (1) to extend a schedule by adding new tx slot at the tail (only
 * if the profile had already been exhausted).
 * (2) to open cwm by 1, in case of a loss, in order to allow for
 * retransmission of first unacked packet.
 */
#define REXMIT_UPDATE_LATENCY 20000000 // 20ms

/*
 * physical core ID for hypace in xen
 */
#define XEN_PACER_CORE  4

#define PTIMER_HEAP_SIZE  256

/*
 * max time for dequeueing profiles in an interrupt handler
 */
//#define MAX_DEQUEUE_TIME  2000 // 2us
#define MAX_DEQUEUE_TIME  0 // 2us
/*
 * max time for freeing profiles in an interrupt handler
 */
//#define MAX_PROF_FREE_TIME 2000 // 2us
#define MAX_PROF_FREE_TIME 0 // 2us

/*
 * one timer for every profile event (deprecated)
 */
#define HP_ORIG         0
/*
 * periodic timer, doorbell at the beginning of the epoch.
 *
 * epoch start time corresponds to the time at which
 * doorbell write must occur. PACER_SPIN_THRESHOLD is used
 * for waking timer earlier than true doorbell write time,
 * and spinning until the true time occurs.
 */
#define HP_BATCH        1
/*
 * periodic timer, doorbell at the end of the epoch.
 *
 * set timer for next epoch using xen's set_timer API
 * may cause more softirq interrupts. timer is set without
 * early wakeup, and PACER_SPIN_THRESHOLD is used for
 * spinning at the end of the epoch before doing doorbell write.
 */
#define HP_BATCH2       2
/*
 * periodic timer, doorbell at the end of the epoch,
 * similar to HP_BATCH2.
 *
 * set timer for next epoch using xen's set_timer API
 * only if current epoch's work finished a threshold
 * amount of time earlier than start of the next epoch.
 */
#define HP_BATCH3       3

#define HYPACE_CFG      HP_BATCH

/*
 * max # hypace loops with one timer softirq. after this
 * count, we should allow for releasing the cpu by using
 * the hardware timer for interrupting.
 *
 * relevant only with HP_BATCH3 config.
 */
#define MAX_HP_PER_IRQ  1

#endif /* __SME_CONFIG_H__ */
