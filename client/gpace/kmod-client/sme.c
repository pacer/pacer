/*
 * sme.c
 *
 * created on: Jan 23, 2017
 * author: aasthakm
 * 
 * overrides the system calls
 */

#include "sme_core.h"
#include "sme_helper.h"

#include "ptcp/ptcp_core.h"

#include "sme_common.h"
#include "sme_debug.h"
#include "sme_statistics.h"
#include "logger.h"
#include "sme_q_common.h"
#include "sme_skb_dummy.h"

#include "sme_xen.h"
#include "sme_nic.h"
#include "generic_q.h"

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/lsm_hooks.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/capability.h>
#include <linux/printk.h>

#include <linux/inet.h>
#include <uapi/linux/stat.h>
#include <uapi/linux/in.h>

#include <linux/miscdevice.h>

#include <linux/smp.h>
#include <linux/atomic.h>

#include <linux/stddef.h>
#include <asm/xen/hypercall.h>
#include <xen/page.h>
#include <asm/msr.h>

#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"
#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x_cmn.h"

uint32_t my_ip_int = 0;
uint16_t my_port_int = 0;
uint32_t bknd_ip_int = 0;
uint16_t bknd_port_int = 0;
uint8_t mod_config_int = 0;
char *host_type_int = NULL;
uint8_t dummy_server_ip[4] = { 139, 19, 168, 173 };
uint16_t dummy_client_port = 2230;
uint16_t dummy_server_port = 2231;

static char *my_ip = "139.19.171.101";
module_param(my_ip, charp, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(my_ip, "IP of the app/DB VM on the host ('139.19.171.101'/'139.19.171.103')");

static unsigned int my_port = 443;
module_param(my_port, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(my_port, "Port of the app/DB VM on the host ('443'/'3306'/'2230')");

static char *bknd_ip = "139.19.171.103";
module_param(bknd_ip, charp, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(bknd_ip, "IP of the DB VM on the host ('139.19.171.101'/'139.19.171.103')");

static unsigned int bknd_port = 3306;
module_param(bknd_port, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(bknd_port, "Port of the app/DB VM on the host ('443'/'3306'/'2230')");

static char *host_type = "FE";
module_param(host_type, charp, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(host_type, "Host type ('FE'/'BE')");

static unsigned short mod_config = 0; // 0 (default) -- enforcement; 1 -- profiling
module_param(mod_config, ushort, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(mod_config, "Experiment config ('0' -- enforcement/'1' -- profiling)");

static unsigned int epoch = 20000; // in ns
module_param(epoch, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(epoch, "Epoch len (ns)");

static unsigned int spin_thresh = 0; // in ns
module_param(spin_thresh, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(spin_thresh, "HyPace early wakeup or spin threshold (ns)");

static unsigned int max_pkts_per_epoch = 75;
module_param(max_pkts_per_epoch, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(max_pkts_per_epoch, "Max packet preparations per epoch");

static unsigned int max_other_per_epoch = 2;
module_param(max_other_per_epoch, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(max_other_per_epoch,
    "Max other ops (dequeue, free profiles) per epoch");

extern struct miscdevice sme_ctrl_dev;
int sme_ctrl_dev_registered = 0;

extern struct miscdevice pdbg_log_dev;
int pdbg_log_dev_registered = 0;

uint64_t tot_largef_q_size = 0, tot_ts_irq_q_size = 0;
uint64_t tot_pcm_rxq_size = 0, tot_pcm_txq_size = 0;
uint64_t tot_proflog_q_size = 0, tot_pmap_htbl_size = 0, tot_pcm_htbl_size = 0;
uint64_t tot_global_nicq_size = 0, tot_global_nicfl_size = 0;
uint64_t tot_defpayload_arr_size = 0, tot_defpayload_dmaddr_arr_size = 0;
uint64_t tot_defskb_arr_size = 0;
uint64_t tot_global_dummy_nicq_size = 0, tot_global_txintq_size = 0;
uint64_t tot_nic_data_arr_size = 0;
uint64_t tot_profq_hdr_size = 0, tot_profq_fl2_size = 0;
uint64_t tot_gpace_mem = 0;
int CONFIG_SW_CSUM = 0;
profile_t default_profile;
sme_log_q_t *proflog_q = NULL;
sme_log_q_t *pacelog_q[NUM_PACERS];
nic_freelist_t *global_nic_freelist[MAX_HP_ARGS];
#if CONFIG_XEN_PACER
pcm_q_t pcm_rxq[RX_NCPUS];
pcm_q_t pcm_txq[RX_NCPUS];
profq_hdr_t *profq_hdr[MAX_HP_ARGS];
profq_freelist2_t *profq_fl2[MAX_HP_ARGS];
nic_txintq_t *nic_txintq[MAX_HP_ARGS];
global_nicq_hdr_t *global_nicq[MAX_HP_ARGS];
global_nicq_hdr_t *global_dummy_nicq[MAX_HP_ARGS];
nic_txdata_t ** nic_data_arr[MAX_HP_ARGS];
#endif
int EPOCH_SIZE = 0;
int PACER_SPIN_THRESHOLD = 0;
int MAX_PKTS_PER_EPOCH = 0;
int MAX_OTHER_PER_EPOCH = 0;
sme_q_t ts_irq_q[RX_NCPUS] ____cacheline_aligned_in_smp;
list_t scm_list[NUM_PACERS];
ihash_t DEFAULT_PROFILE_IHASH;
ihash_t DEFAULT_SSLFIN_PROF_IHASH;
ihash_t DEFAULT_HTTP_PROF_IHASH;
ihash_t DEFAULT_HTTPS_PROF_IHASH;
ihash_t DEFAULT_MW_PROF_IHASH;
ihash_t DEFAULT_VIDEO_PROF_IHASH;
ihash_t pid1, pid2, pid3;
generic_q_t *largef_q[MAX_STATQ_ARRAYS];

#ifdef CONFIG_PACER_BNX2X
extern void
(*lnk_update_cwnd) (struct sk_buff *skb, struct sock* sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int dummy_delivered,
    char *extra_dbg_string);
extern int
(*lnk_intercept_select_queue) (struct net_device *dev, struct sk_buff *skb,
    char *extra_dbg_string);
extern void
(*lnk_intercept_tx_sent_queue) (struct netdev_queue *txq, unsigned int bytes);
extern void
(*lnk_intercept_tx_completed_queue) (struct netdev_queue *txq,
    unsigned int pkts, unsigned int bytes);
extern int
(*lnk_intercept_sk_buff) (struct sk_buff *skb, char *extra_dbg_string);
extern void
(*lnk_intercept_xmit_stop_queue) (struct net_device *dev, struct netdev_queue *txq,
    struct bnx2x_fp_txdata *txdata);
extern int
(*lnk_intercept_tx_int) (struct net_device *dev, struct bnx2x_fp_txdata *txdata);
extern void
(*lnk_intercept_free_tx_skbs_queue) (struct bnx2x_fastpath *fp);
extern void
(*lnk_parse_rx_data) (void *dev, int fp_idx, uint16_t rx_bd_prod,
    uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons,
    char *data, int data_len, char *extra_dbg_string);
extern void
(*lnk_print_sk_buff) (struct sk_buff *skb, struct sock *sk, char *extra_dbg_string);
extern void
(*lnk_intercept_rx_path) (void *dev, int fp_idx, uint16_t rx_bd_prod,
    uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons, int irq,
    char *extra_dbg_string);
extern void
(*lnk_print_rx_data) (void *dev, int fp_idx, uint16_t rx_bd_prod,
    uint16_t rx_bd_cons, uint16_t rx_comp_prod, uint16_t rx_comp_cons,
    char *extra_dbg_string);

int
sme_override_functions(int set)
{
  if (set) {
    lnk_intercept_select_queue =
      (int (*) (struct net_device *, struct sk_buff *,
                char *)) mod_intercept_select_queue;

#if 1
    lnk_intercept_tx_sent_queue =
      (void (*) (struct netdev_queue *, unsigned int)) mod_intercept_tx_sent_queue;
    lnk_intercept_tx_completed_queue =
      (void (*) (struct netdev_queue *, unsigned int,
                 unsigned int)) mod_intercept_tx_completed_queue;
#else
    lnk_intercept_tx_sent_queue =
      (void (*) (struct netdev_queue *, unsigned int)) 0;
    lnk_intercept_tx_completed_queue =
      (void (*) (struct netdev_queue *, unsigned int, unsigned int)) 0;
#endif

    lnk_intercept_sk_buff =
      (int (*) (struct sk_buff *, char *)) mod_intercept_sk_buff;

#if 1
    lnk_intercept_xmit_stop_queue =
      (void (*) (struct net_device *, struct netdev_queue *,
                 struct bnx2x_fp_txdata *)) mod_intercept_xmit_stop_queue;
    lnk_intercept_tx_int =
      (int (*) (struct net_device *, struct bnx2x_fp_txdata *)) mod_intercept_tx_int;
#else
    lnk_intercept_xmit_stop_queue =
      (void (*) (struct net_device *, struct netdev_queue *,
                 struct bnx2x_fp_txdata *)) 0;
    lnk_intercept_tx_int =
      (int (*) (struct net_device *, struct netdev_queue *,
                 struct bnx2x_fp_txdata *)) 0;
#endif
    lnk_update_cwnd =
      (void (*) (struct sk_buff *, struct sock *, int,
                 int, int, int, int, int, int, int, int, int, int,
                 int, int, int, int, char *)) mod_update_cwnd;
    lnk_intercept_free_tx_skbs_queue =
      (void (*) (struct bnx2x_fastpath *)) mod_intercept_free_tx_skbs_queue;
    lnk_parse_rx_data =
      (void (*) (void *, int, uint16_t, uint16_t, uint16_t, uint16_t,
                 char *, int, char *)) mod_parse_rx_data;
    lnk_print_sk_buff =
      (void (*) (struct sk_buff *, struct sock *, char *)) mod_print_sk_buff;
    lnk_print_rx_data =
      (void (*) (void *, int, uint16_t, uint16_t, uint16_t, uint16_t,
                 char *)) mod_print_rx_data;
    lnk_intercept_rx_path =
      (void (*) (void *, int, uint16_t, uint16_t, uint16_t, uint16_t, int,
                 char *)) mod_intercept_rx_path;
  } else {
    lnk_update_cwnd =
      (void (*) (struct sk_buff *, struct sock *, int,
                 int, int, int, int, int, int, int, int, int, int,
                 int, int, int, int, char *)) 0;
    lnk_intercept_select_queue =
      (int (*) (struct net_device *, struct sk_buff *, char *)) 0;
    lnk_intercept_tx_sent_queue =
      (void (*) (struct netdev_queue *, unsigned int)) 0;
    lnk_intercept_tx_completed_queue =
      (void (*) (struct netdev_queue *, unsigned int, unsigned int)) 0;
    lnk_intercept_sk_buff =
      (int (*) (struct sk_buff *, char *)) 0;
    lnk_intercept_xmit_stop_queue =
      (void (*) (struct net_device *, struct netdev_queue *,
                 struct bnx2x_fp_txdata *)) 0;
    lnk_intercept_tx_int =
      (int (*) (struct net_device *, struct bnx2x_fp_txdata *)) 0;
    lnk_intercept_free_tx_skbs_queue = (void (*) (struct bnx2x_fastpath *)) 0;

    lnk_intercept_rx_path =
      (void (*) (void *, int, uint16_t, uint16_t, uint16_t, uint16_t, int,
                 char *)) 0;
    lnk_parse_rx_data =
      (void (*) (void *, int, uint16_t, uint16_t, uint16_t, uint16_t, char *,
                 int, char *)) 0;
    lnk_print_sk_buff =
      (void (*) (struct sk_buff *, struct sock *, char *)) 0;
    lnk_print_rx_data =
      (void (*) (void *, int, uint16_t, uint16_t, uint16_t, uint16_t, char *)) 0;
  }

  return 0;
}
#endif /* CONFIG_PACER_BNX2X */

#ifdef CONFIG_PACER_TCP
extern void
(*lnk_print_sock_skb) (struct sock *sk, struct sk_buff *skb, char *dbg_str);
extern int
(*lnk_rx_pred_flags) (struct sock *sk, struct sk_buff *skb, struct tcphdr *th);
extern int
(*lnk_rx_handle_tcp_urg) (struct sock *sk, struct sk_buff *skb, struct tcphdr *th);
extern int
(*lnk_rx_handle_dummy) (struct sock *sk, struct sk_buff *skb);
extern int
(*lnk_rx_disable_coalesce) (struct sock *sk, struct sk_buff *to,
    struct sk_buff *from);
extern int
(*lnk_rx_adjust_skb_size) (struct sock *sk, struct sk_buff *skb, int req_len,
    int offset, int chunk, int copied, int flags);
extern int
(*lnk_rx_adjust_copied_seq) (struct sock *sk, struct sk_buff *skb, int old_skb_len,
    int new_skb_len);

extern int
(*lnk_tx_adjust_seq) (struct sock *sk, struct sk_buff *skb, int copy, int copied);
#ifdef CONFIG_XEN_SME
extern int
(*lnk_rxtx_sync_shared_seq) (struct sock *sk, struct sk_buff *skb, int write);
#endif
extern int
(*lnk_tx_adjust_urg) (struct sock *sk, struct sk_buff *skb);
extern int
(*lnk_is_paced_flow) (struct sock *sk);
#ifdef CONFIG_XEN_SME
extern int
(*lnk_tx_incr_tail) (struct sock *sk, struct sk_buff *skb);
extern int
(*lnk_tx_incr_profile) (struct sock *sk, struct sk_buff *skb);
extern void
(*lnk_timer_rto_check) (struct sock *sk);
extern int
(*lnk_get_retransmit_skb) (struct sock *sk, struct sk_buff **skb_p);
extern int
(*lnk_build_rexmit_dummy) (struct sock *sk, struct sk_buff **skb_p,
    uint32_t seqno, uint32_t seqack, int sacked_flags);
#endif

int
ptcp_override_functions(int set)
{
  if (set) {
    lnk_print_sock_skb =
      (void (*) (struct sock *, struct sk_buff *, char *)) mod_print_sock_skb;
    lnk_rx_pred_flags =
      (int (*) (struct sock *, struct sk_buff *, struct tcphdr *)) mod_rx_pred_flags;
    lnk_rx_handle_tcp_urg =
      (int (*) (struct sock *, struct sk_buff *,
                struct tcphdr *)) mod_rx_handle_tcp_urg;
    lnk_rx_handle_dummy =
      (int (*) (struct sock *, struct sk_buff *)) mod_rx_handle_dummy;
    lnk_rx_disable_coalesce =
      (int (*) (struct sock *, struct sk_buff *,
                struct sk_buff *)) mod_rx_disable_coalesce;
    lnk_rx_adjust_skb_size =
      (int (*) (struct sock *, struct sk_buff *, int, int,
                int, int, int)) mod_rx_adjust_skb_size;
    lnk_rx_adjust_copied_seq =
      (int (*) (struct sock *, struct sk_buff *, int, int)) mod_rx_adjust_copied_seq;

    lnk_tx_adjust_seq =
      (int (*) (struct sock *, struct sk_buff *, int, int)) mod_tx_adjust_seq;
#ifdef CONFIG_XEN_SME
    lnk_rxtx_sync_shared_seq =
      (int (*) (struct sock *, struct sk_buff *, int)) mod_rxtx_sync_shared_seq;
#endif
    lnk_tx_adjust_urg =
      (int (*) (struct sock *, struct sk_buff *)) mod_tx_adjust_urg;
    lnk_is_paced_flow =
      (int (*) (struct sock *)) mod_is_paced_flow;
#ifdef CONFIG_XEN_SME
    lnk_tx_incr_tail =
      (int (*) (struct sock *, struct sk_buff *)) mod_tx_incr_tail;
    lnk_tx_incr_profile =
      (int (*) (struct sock *, struct sk_buff *)) mod_tx_incr_profile;
    lnk_timer_rto_check =
      (void (*) (struct sock *)) mod_timer_rto_check;
    lnk_get_retransmit_skb =
      (int (*) (struct sock *, struct sk_buff **)) mod_get_retransmit_skb;
    lnk_build_rexmit_dummy =
      (int (*) (struct sock *, struct sk_buff **,
                uint32_t, uint32_t, int)) mod_build_rexmit_dummy;
#endif
  } else {
    lnk_print_sock_skb =
      (void (*) (struct sock *, struct sk_buff *, char *)) 0;
    lnk_rx_pred_flags =
      (int (*) (struct sock *, struct sk_buff *, struct tcphdr *)) 0;
    lnk_rx_handle_tcp_urg =
      (int (*) (struct sock *, struct sk_buff *, struct tcphdr *)) 0;
    lnk_rx_handle_dummy =
      (int (*) (struct sock *, struct sk_buff *)) 0;
    lnk_rx_disable_coalesce =
      (int (*) (struct sock *, struct sk_buff *, struct sk_buff *)) 0;
    lnk_rx_adjust_skb_size =
      (int (*) (struct sock *, struct sk_buff *, int, int, int, int, int)) 0;
    lnk_rx_adjust_copied_seq =
      (int (*) (struct sock *, struct sk_buff *, int, int)) 0;

    lnk_tx_adjust_seq =
      (int (*) (struct sock *, struct sk_buff *, int, int)) 0;
#ifdef CONFIG_XEN_SME
    lnk_rxtx_sync_shared_seq =
      (int (*) (struct sock *, struct sk_buff *, int)) 0;
#endif
    lnk_tx_adjust_urg =
      (int (*) (struct sock *, struct sk_buff *)) 0;
    lnk_is_paced_flow =
      (int (*) (struct sock *)) 0;
#ifdef CONFIG_XEN_SME
    lnk_tx_incr_tail =
      (int (*) (struct sock *, struct sk_buff *)) 0;
    lnk_tx_incr_profile =
      (int (*) (struct sock *, struct sk_buff *)) 0;
    lnk_timer_rto_check =
      (void (*) (struct sock *)) 0;
    lnk_get_retransmit_skb =
      (int (*) (struct sock *, struct sk_buff **)) 0;
    lnk_build_rexmit_dummy =
      (int (*) (struct sock *, struct sk_buff **, uint32_t, uint32_t, int)) 0;
#endif
  }

  return 0;
}
#endif /* CONFIG_PACER_TCP */


char *emvf_cfg_space = NULL;
struct net_device *vf_ndev = NULL;
struct pci_dev *vf_pdev = NULL;
struct bnx2x *vf_bp = NULL;
char **default_payload[MAX_HP_ARGS];
struct sk_buff **default_skb[MAX_HP_ARGS];
dma_addr_t *default_payload_dmaddr[MAX_HP_ARGS];

#ifdef CONFIG_PACER_BNX2X
static void
mod_init_all_datastructures(void)
{
  int i;
  int old_mtu, new_mtu;
  int64_t one_nicq_size = 0, one_dummy_nicq_size = 0;
  int64_t one_txintq_size = 0, one_profq_size = 0;

#if CONFIG_XEN_PACER
  sme_pacer_arg_t harg;
  struct device *vf_dev = NULL;
  uint64_t hw_timer_reg = 0;
  int ret = 0;
  struct bnx2x_fp_txdata *txdata = NULL;
  int hpi;
#endif

  iprint(LVL_EXP, "my IP %pI4 port %u bknd IP %pI4 bknd port %u"
      " epoch %d spin thresh %d max pkts per epoch %d"
      , (void *) &my_ip_int, my_port_int, (void *) &bknd_ip_int, bknd_port_int
      , epoch, spin_thresh, max_pkts_per_epoch);
  iprint(LVL_DBG, "sz sskb %lu pskb %lu pcm %lu sme_q %lu pcm_q %lu "
      "htable %lu ipport %lu"
      , sizeof(sme_sk_buff_t), sizeof(partial_sk_buff_t)
      , sizeof(profile_conn_map_t), sizeof(sme_q_t), sizeof(pcm_q_t)
      , sizeof(sme_htable_t), sizeof(ipport_t));

  iprint(LVL_EXP, "sz profq_elem %lu nic_txdata %lu spinlock %lu profq_fl2 %lu"
      , sizeof(profq_elem_t), sizeof(nic_txdata_t), sizeof(spinlock_t)
      , sizeof(profq_freelist2_t)
      );
  iprint(LVL_EXP, "sz sw_tx_bd %lu bds_per_tx_pkt %d max bds %d nxt cnt %d "
      "max_desc_per_tx_pkt %d"
      , sizeof(struct sw_tx_bd), (int) BDS_PER_TX_PKT, (int) MAX_BDS_PER_TX_PKT
      , (int) NEXT_CNT_PER_TX_PKT(MAX_BDS_PER_TX_PKT)
      , (int) MAX_DESC_PER_TX_PKT
      );
  iprint3(LVL_EXP, "EAGER FPU %d XSAVE %d XSAVEOPT %d FXSR %d\n"
      "CR0 0x%0lx [MP 0x%x EM 0x%x TS 0x%x]"
      " CR4 0x%0lx [OSFXSR 0x%x OSXMMEXCPT 0x%x OSXSAVE 0x%0x]"
      , boot_cpu_has(X86_FEATURE_EAGER_FPU)
      , boot_cpu_has(X86_FEATURE_XSAVE)
      , boot_cpu_has(X86_FEATURE_XSAVEOPT)
      , boot_cpu_has(X86_FEATURE_FXSR)
      , read_cr0()
      , (int) (read_cr0() & X86_CR0_MP)
      , (int) (read_cr0() & X86_CR0_EM)
      , (int) (read_cr0() & X86_CR0_TS)
      , __read_cr4()
      , (int) (__read_cr4() & X86_CR4_OSFXSR)
      , (int) (__read_cr4() & X86_CR4_OSXMMEXCPT)
      , (int) (__read_cr4() & X86_CR4_OSXSAVE)
      );

  EPOCH_SIZE = epoch;
  PACER_SPIN_THRESHOLD = spin_thresh;
  MAX_PKTS_PER_EPOCH = max_pkts_per_epoch;
  MAX_OTHER_PER_EPOCH = max_other_per_epoch;

  old_mtu = PTCP_MTU;

  PTCP_MTU = SME_TCP_MTU;

  new_mtu = PTCP_MTU;

  iprint(LVL_EXP, "MTU value %d => %d", old_mtu, new_mtu);
  /*
   * default profile for pacing
   */
  init_ihashes();
  init_default_profile(&default_profile);

  for (i = 0; i < MAX_STATQ_ARRAYS; i++) {
    largef_q[i] = vzalloc(sizeof(generic_q_t));
    init_generic_q(*largef_q[i], i, NUM_REQTS_ELEMS, largef_elem_t,
        do_print_largef_q);
  }
  tot_largef_q_size = ((sizeof(generic_q_t)
        + (sizeof(largef_elem_t) * NUM_REQTS_ELEMS)) * MAX_STATQ_ARRAYS);
  iprint(LVL_EXP, "largef Q hdr size %lu elem size %lu #elem %d #arrs %d Q mem %llu"
      , sizeof(generic_q_t), sizeof(largef_elem_t), NUM_REQTS_ELEMS, MAX_STATQ_ARRAYS
      , tot_largef_q_size);

  /*
   * init pair of ts queues for as many cores
   * as the number of RX IRQs
   */
  for (i = 0; i < RX_NCPUS; i++) {
    init_sme_q(&ts_irq_q[i], TS_QSIZE);
  }
  tot_ts_irq_q_size = (sizeof(sme_q_t) + (sizeof(sme_sk_buff_t) * TS_QSIZE)) * RX_NCPUS;
  iprint(LVL_EXP, "irq Q hdr size %lu elem size %lu #elem %d RX_NCPUS %d Q mem %llu"
      , sizeof(sme_q_t), sizeof(sme_sk_buff_t), TS_QSIZE, RX_NCPUS
      , tot_ts_irq_q_size);

#if CONFIG_XEN_PACER
  for (i = 0; i < RX_NCPUS; i++) {
    init_pcm_q(&pcm_rxq[i], PCM_QSIZE);
    init_pcm_q(&pcm_txq[i], PCM_QSIZE);
  }
  tot_pcm_rxq_size = (sizeof(pcm_q_t) + (sizeof(pcm_q_elem_t) * PCM_QSIZE)) * RX_NCPUS;
  tot_pcm_txq_size = (sizeof(pcm_q_t) + (sizeof(pcm_q_elem_t) * PCM_QSIZE)) * RX_NCPUS;
  iprint(LVL_EXP, "pcm rx Q hdr size %lu elem size %lu #elem %d RX_NCPUS %d Q mem %llu"
      , sizeof(pcm_q_t), sizeof(pcm_q_elem_t), PCM_QSIZE, RX_NCPUS
      , tot_pcm_rxq_size);
  iprint(LVL_EXP, "pcm tx Q hdr size %lu elem size %lu #elem %d RX_NCPUS %d Q mem %llu"
      , sizeof(pcm_q_t), sizeof(pcm_q_elem_t), PCM_QSIZE, RX_NCPUS
      , tot_pcm_txq_size);

#if SME_CONFIG != SME_DBG_LOG
  /*
   * init queue for profile logs, shared between
   * logger thread and userspace profiler
   */
  proflog_q = alloc_init_sme_log_q(MAX_NUM_QBUFS);
  tot_proflog_q_size = sizeof(proflog_q);
  iprint(LVL_EXP, "proflog Q size %llu", tot_proflog_q_size);
  setup_logger_thread(NULL);

  misc_register(&sme_ctrl_dev);
  sme_ctrl_dev_registered = 1;

#endif

  for (i = 0; i < NUM_PACERS; i++) {
    init_sme_channel_map_list(&scm_list[i]);
    add_initial_sme_channel_map(&scm_list[i]);
  }

#if CONFIG_XEN_PACER_DB
  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    global_nicq[hpi] = (global_nicq_hdr_t *) kzalloc(sizeof(global_nicq_hdr_t),
        GFP_KERNEL);
    one_nicq_size += init_global_nicq_hdr(global_nicq[hpi], GLOBAL_NIC_DATA_QSIZE);
    iprint(LVL_EXP, "Global nicQ %p, #md: %d\n"
        "[(0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d)]"
        , global_nicq[hpi], global_nicq[hpi]->n_md
        , global_nicq[hpi]->md_arr[0].maddr, global_nicq[hpi]->md_arr[0].n_elems
        , global_nicq[hpi]->md_arr[1].maddr, global_nicq[hpi]->md_arr[1].n_elems
        , global_nicq[hpi]->md_arr[2].maddr, global_nicq[hpi]->md_arr[2].n_elems
        , global_nicq[hpi]->md_arr[3].maddr, global_nicq[hpi]->md_arr[3].n_elems
        );

    global_nic_freelist[hpi] = (nic_freelist_t *) kzalloc(sizeof(nic_freelist_t),
        GFP_KERNEL);
    init_nic_freelist(global_nic_freelist[hpi], GLOBAL_NIC_DATA_QSIZE, 1);
    iprint(LVL_EXP, "Global nic freelist %p, #md: %d, ret %d\n"
        "[(0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d)]"
        , global_nic_freelist[hpi], global_nic_freelist[hpi]->n_md, ret
        , global_nic_freelist[hpi]->md_arr[0].maddr
        , global_nic_freelist[hpi]->md_arr[0].n_elems
        , global_nic_freelist[hpi]->md_arr[1].maddr
        , global_nic_freelist[hpi]->md_arr[1].n_elems
        , global_nic_freelist[hpi]->md_arr[2].maddr
        , global_nic_freelist[hpi]->md_arr[2].n_elems
        , global_nic_freelist[hpi]->md_arr[3].maddr
        , global_nic_freelist[hpi]->md_arr[3].n_elems
        );
  }

  tot_global_nicq_size = ((sizeof(global_nicq_hdr_t)
      + (sizeof(nic_txdata_t *) * GLOBAL_NIC_DATA_QSIZE)) * N_HYPACE) + one_nicq_size;
  tot_global_nicfl_size = (sizeof(nic_freelist_t)
      + (sizeof(nic_fl_elem_t) * GLOBAL_NIC_DATA_QSIZE)) * N_HYPACE;
  iprint(LVL_EXP, "global nic Q hdr size %lu elem size %lu ptr size %lu "
      "#elems %lu N_HYPACE %d Q mem %llu"
      , sizeof(global_nicq_hdr_t), sizeof(nic_txdata_t), sizeof(nic_txdata_t *)
      , GLOBAL_NIC_DATA_QSIZE, N_HYPACE, tot_global_nicq_size);
  iprint(LVL_EXP, "global nic FL hdr size %lu elem size %lu "
      "#elems %lu N_HYPACE %d FL mem %llu"
      , sizeof(nic_freelist_t), sizeof(nic_fl_elem_t)
      , GLOBAL_NIC_DATA_QSIZE, N_HYPACE, tot_global_nicfl_size);


  rdmsrl(MSR_IA32_TSC_DEADLINE, hw_timer_reg);
  iprint(LVL_INFO, "HW TSC REG val %llu rdtsc %llu", hw_timer_reg, rdtsc());

  vf_pdev = pci_get_device(PCI_VENDOR_ID_BROADCOM, CHIP_NUM_57800_VF, NULL);
  if (!vf_pdev)
    return;

  vf_dev = &vf_pdev->dev;
  vf_ndev = (struct net_device *) vf_dev->driver_data;

  /*
   * determine at runtime, whether we must do TCP/IP checksumming in s/w.
   */
  if ((vf_ndev->features & (NETIF_F_IP_CSUM | NETIF_F_IPV6_CSUM)) == 0)
    CONFIG_SW_CSUM = 1;

  vf_bp = netdev_priv(vf_ndev);
  iprint(LVL_EXP, "BP flags 0x%0x #queues %d #eth queues %d CONFIG_SW_CSUM %d\n"
      "DEV #real queues %d flags 0x%0x features 0x%0llx HW_CSUM %d IP_CSUM %d"
      , vf_bp->flags, vf_bp->num_queues, vf_bp->num_ethernet_queues
      , CONFIG_SW_CSUM
      , vf_ndev->real_num_tx_queues, vf_ndev->flags, vf_ndev->features
      , (int) (vf_ndev->features & NETIF_F_HW_CSUM)
      , (int) (vf_ndev->features & NETIF_F_IP_CSUM)
      );

  for_each_eth_queue(vf_bp, i) {
    txdata = vf_bp->fp[i].txdata_ptr[0];
    iprint(LVL_EXP, "TXQ[%d] bd_prod %u bd_cons %u pkt_prod %u pkt_cons %u hw_cons %u"
        , i, txdata->tx_bd_prod, txdata->tx_bd_cons
        , txdata->tx_pkt_prod, txdata->tx_pkt_cons, le16_to_cpu(*txdata->tx_cons_sb)
        );
  }
//  iprint(LVL_INFO, "VF pdev %p ndev %p bp %p", vf_pdev, vf_ndev, vf_bp);
//  print_bnx2x(vf_bp);

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    /*
     * alloc default payload, and generate a coherent dma mapping for it.
     * although we need only MTU-sized buffer, we just allocate a full page.
     */
    default_payload[hpi] = (char **) kzalloc(sizeof(char *) * NUM_TX_BD,
        GFP_KERNEL);
    default_payload_dmaddr[hpi] =
      (dma_addr_t *) kzalloc(sizeof(dma_addr_t) * NUM_TX_BD, GFP_KERNEL);
    for (i = 0; i < NUM_TX_BD; i++) {
      default_payload[hpi][i] = dma_alloc_coherent(&vf_bp->pdev->dev, PAGE_SIZE,
        &default_payload_dmaddr[hpi][i], GFP_KERNEL);
    }

    default_skb[hpi] =
      (struct sk_buff **) kzalloc(sizeof(struct sk_buff *) * NUM_TX_BD, GFP_KERNEL);
    for (i = 0; i < NUM_TX_BD; i++) {
      build_dummy_skb_with_data(&default_skb[hpi][i], default_payload[hpi][i],
        0, my_ip_int, *(uint32_t *) dummy_server_ip,
        my_port_int, dummy_server_port,
        0, 0, IPPROTO_TCP, NULL, NULL, vf_ndev, SME_PACER_CORE+hpi);
    }

    for (i = 0; i < NUM_TX_BD; i++) {
      iprint2(LVL_DBG, "[%d] pl v %p m 0x%0llx dma 0x%0llx %d %d skb %p %d %d"
          , i, default_payload[hpi][i]
          , (virt_to_machine(default_payload[hpi][i])).maddr
          , default_payload_dmaddr[hpi][i]
          , PageSlab(virt_to_head_page(default_payload[hpi][i]))
          , PageCompound(virt_to_head_page(default_payload[hpi][i]))
          , default_skb[hpi][i], PageSlab(virt_to_head_page(default_skb[hpi][i]))
          , PageCompound(virt_to_head_page(default_skb[hpi][i])));
      if (i % 100 == 0)
        msleep(1);
    }
  }

  tot_defpayload_arr_size = (PAGE_SIZE + sizeof(char *)) * NUM_TX_BD * N_HYPACE;
  tot_defpayload_dmaddr_arr_size = (sizeof(dma_addr_t) * NUM_TX_BD) * N_HYPACE;
  tot_defskb_arr_size = ((sizeof(struct sk_buff *) + sizeof(struct sk_buff))
      * NUM_TX_BD) * N_HYPACE;
  iprint(LVL_EXP, "default payload elem size %lu ptr size %lu "
      "#elems %lu N_HYPACE %d Q mem %llu"
      , PAGE_SIZE, sizeof(char *), NUM_TX_BD, N_HYPACE, tot_defpayload_arr_size);
  iprint(LVL_EXP, "default payload dmaddr elem size %lu #elems %lu N_HYPACE %d Q mem %llu"
      , sizeof(dma_addr_t), NUM_TX_BD, N_HYPACE, tot_defpayload_dmaddr_arr_size);
  iprint(LVL_EXP, "default skb elem size %lu ptr size %lu #elems %lu N_HYPACE %d Q mem %llu"
      , sizeof(struct sk_buff), sizeof(struct sk_buff *), NUM_TX_BD, N_HYPACE
      , tot_defskb_arr_size);

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    global_dummy_nicq[hpi] = (global_nicq_hdr_t *) kzalloc(sizeof(global_nicq_hdr_t),
        GFP_KERNEL);
    one_dummy_nicq_size += init_global_nicq_hdr(global_dummy_nicq[hpi], NUM_TX_BD);
    prepare_dummy_nicq(global_dummy_nicq[hpi], default_skb[hpi],
        default_payload_dmaddr[hpi]);
    iprint(LVL_EXP, "Global DUMMY nicQ %p, #md: %d\n"
        "[(0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d)]"
        , global_dummy_nicq[hpi], global_dummy_nicq[hpi]->n_md
        , global_dummy_nicq[hpi]->md_arr[0].maddr
        , global_dummy_nicq[hpi]->md_arr[0].n_elems
        , global_dummy_nicq[hpi]->md_arr[1].maddr
        , global_dummy_nicq[hpi]->md_arr[1].n_elems
        , global_dummy_nicq[hpi]->md_arr[2].maddr
        , global_dummy_nicq[hpi]->md_arr[2].n_elems
        , global_dummy_nicq[hpi]->md_arr[3].maddr
        , global_dummy_nicq[hpi]->md_arr[3].n_elems
        );

    /*
     * producer-consumer array of maddrs of skbs passed to hypace for NIC IO.
     * hypace places skb maddrs in this array for guest to free skbs as it
     * receives TX completion notification from the NIC.
     */
    nic_txintq[hpi] = (nic_txintq_t *) kzalloc(sizeof(nic_txintq_t), GFP_KERNEL);
    one_txintq_size += init_nic_txintq(nic_txintq[hpi], NUM_TX_BD);
    iprint(LVL_EXP, "nic txint %p, #md: %d\n"
        "[(0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d)]"
        , nic_txintq[hpi], nic_txintq[hpi]->n_md
        , nic_txintq[hpi]->md_arr[0].maddr, nic_txintq[hpi]->md_arr[0].n_elems
        , nic_txintq[hpi]->md_arr[1].maddr, nic_txintq[hpi]->md_arr[1].n_elems
        , nic_txintq[hpi]->md_arr[2].maddr, nic_txintq[hpi]->md_arr[2].n_elems
        , nic_txintq[hpi]->md_arr[3].maddr, nic_txintq[hpi]->md_arr[3].n_elems
        );
  }

  tot_global_dummy_nicq_size = ((sizeof(global_nicq_hdr_t)
      + (sizeof(nic_txdata_t *) * NUM_TX_BD)) * N_HYPACE) + one_dummy_nicq_size;
  tot_global_txintq_size = ((sizeof(nic_txintq_t)
      + (sizeof(nic_txint_elem_t *) * NUM_TX_BD)) * N_HYPACE) + one_txintq_size;
  iprint(LVL_EXP, "global dummy nic Q hdr size %lu elem size %lu ptr size %lu "
      "#elems %lu N_HYPACE %d Q mem %llu"
      , sizeof(global_nicq_hdr_t), sizeof(nic_txdata_t), sizeof(nic_txdata_t *)
      , NUM_TX_BD, N_HYPACE, tot_global_dummy_nicq_size);
  iprint(LVL_EXP, "global txint Q hdr size %lu elem size %lu ptr size %lu "
      "#elems %lu N_HYPACE %d Q mem %llu"
      , sizeof(nic_txintq_t), sizeof(nic_txint_elem_t), sizeof(nic_txint_elem_t *)
      , NUM_TX_BD, N_HYPACE, tot_global_nicq_size);

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    int n_elem_per_page = PAGE_SIZE/sizeof(nic_txdata_t);
    int n_pages = (NIC_DATA_QSIZE-1)/n_elem_per_page + 1;
//    int nic_data_arr_size = n_pages * PAGE_SIZE;
    nic_data_arr[hpi] = (nic_txdata_t **) kcalloc(PROFILE_QSIZE - 2,
        sizeof(nic_txdata_t *), GFP_KERNEL);
    for (i = 0; i < (PROFILE_QSIZE - 2); i++) {
      nic_data_arr[hpi][i] = (nic_txdata_t *) kcalloc(n_pages,
          PAGE_SIZE, GFP_KERNEL);
    }
  }
  tot_nic_data_arr_size = ((sizeof(nic_txdata_t *) + sizeof(nic_data_arr[0][0]))
    * (PROFILE_QSIZE - 2)) * N_HYPACE;
  iprint(LVL_EXP, "nic data arr elem size %lu ptr size %lu "
      "#elems %lu N_HYPACE %d Q mem %llu"
      , sizeof(nic_txdata_t), sizeof(nic_txdata_t *), NUM_TX_BD, N_HYPACE
      , tot_nic_data_arr_size);
#endif

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    /*
     * allocate primary profile queue shared with the hypervisor
     */
    profq_hdr[hpi] = (profq_hdr_t *) kzalloc(sizeof(profq_hdr_t), GFP_KERNEL);
    if (!profq_hdr[hpi])
      return;

    one_profq_size += init_profq_hdr(profq_hdr[hpi], nic_data_arr[hpi], PROFILE_QSIZE);
    if (ret < 0) {
      kfree(profq_hdr[hpi]);
      return;
    }
    iprint(LVL_EXP, "profq %p, size %lld #md: %d, ret %d\n"
        "[(0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d)]"
        , profq_hdr[hpi], profq_hdr[hpi]->qsize, profq_hdr[hpi]->n_md, ret
        , profq_hdr[hpi]->md_arr[0].maddr, profq_hdr[hpi]->md_arr[0].n_elems
        , profq_hdr[hpi]->md_arr[1].maddr, profq_hdr[hpi]->md_arr[1].n_elems
        , profq_hdr[hpi]->md_arr[2].maddr, profq_hdr[hpi]->md_arr[2].n_elems
        , profq_hdr[hpi]->md_arr[3].maddr, profq_hdr[hpi]->md_arr[3].n_elems
        );

    /*
     * producer-consumer freelist of array indexes corr. to free profile queue slots
     */
    profq_fl2[hpi] = (profq_freelist2_t *) kzalloc(sizeof(profq_freelist2_t), GFP_KERNEL);
    init_profq_freelist2(profq_fl2[hpi], PROF_FREELIST_SIZE);
    iprint(LVL_EXP, "profq freelist %p, size %lld #md: %d, ret %d prod %d cons %d\n"
        "[(0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d) (0x%0llx, %d)]"
        , profq_fl2[hpi], profq_fl2[hpi]->idx_list_size, profq_fl2[hpi]->n_md, ret
        , profq_fl2[hpi]->prod_idx, profq_fl2[hpi]->cons_idx
        , profq_fl2[hpi]->md_arr[0].maddr, profq_fl2[hpi]->md_arr[0].n_elems
        , profq_fl2[hpi]->md_arr[1].maddr, profq_fl2[hpi]->md_arr[1].n_elems
        , profq_fl2[hpi]->md_arr[2].maddr, profq_fl2[hpi]->md_arr[2].n_elems
        , profq_fl2[hpi]->md_arr[3].maddr, profq_fl2[hpi]->md_arr[3].n_elems
        );
  }
  tot_profq_hdr_size = ((sizeof(profq_hdr_t)
    + (sizeof(profq_elem_t *) * PROFILE_QSIZE)) * N_HYPACE) + one_profq_size;
  tot_profq_fl2_size = (sizeof(profq_freelist2_t)
      + (sizeof(int32_t) * PROF_FREELIST_SIZE)) * N_HYPACE;
  iprint(LVL_EXP, "profq hdr size %lu elem size %lu ptr size %lu "
      "#elems %d N_HYPACE %d Q mem %llu"
      , sizeof(profq_hdr_t), sizeof(profq_elem_t), sizeof(profq_elem_t *)
      , PROFILE_QSIZE, N_HYPACE, tot_profq_hdr_size);
  iprint(LVL_EXP, "profq fl hdr size %lu elem size %lu #elems %d N_HYPACE %d Q mem %llu"
      , sizeof(profq_freelist2_t), sizeof(int32_t), PROF_FREELIST_SIZE, N_HYPACE
      , tot_profq_fl2_size);

  prepare_hypercall_args(&harg, vf_pdev, profq_hdr, profq_fl2,
      global_nicq, global_dummy_nicq, global_nic_freelist, nic_txintq, N_HYPACE);
  /*
   * hypercall
   */
  ret = HYPERVISOR_sme_pacer(PACER_OP_SET_ADDRS, (void *) &harg);
  iprint(LVL_EXP, "hypercall arg sz %lu, ret %d", sizeof(sme_pacer_arg_t), ret);
#endif /* CONFIG_XEN_PACER_DB */
#endif /* CONFIG_XEN_PACER */

  iprint(LVL_EXP, "PCM TXQ/RXQ: %d", (RX_NCPUS * PCM_QSIZE * 2));
  iprint(LVL_EXP, "N_HYPACE: %d GLOBAL NIC FREELIST: %ld", N_HYPACE,
      ((GLOBAL_NIC_DATA_QSIZE * sizeof(nic_fl_elem_t)) + sizeof(nic_freelist_t)));
  iprint(LVL_EXP, "N_HYPACE: %d DEFAULT PAYLOAD BUF: %ld", N_HYPACE,
      (sizeof(char *) * NUM_TX_BD));
  iprint(LVL_EXP, "N_HYPACE: %d DEFAULT PAYLOAD DMADDR: %ld", N_HYPACE,
      (sizeof(dma_addr_t) * NUM_TX_BD));
  iprint(LVL_EXP, "N_HYPACE: %d DUMMY SKB: %ld", N_HYPACE,
      (sizeof(struct sk_buff *) * NUM_TX_BD));
  iprint(LVL_EXP, "N_HYPACE: %d GLOBAL DUMMY NICQ: %ld + Q", N_HYPACE,
      (sizeof(global_nicq_hdr_t) + (sizeof(nic_txdata_t *) * global_dummy_nicq[0]->qsize)));
  iprint(LVL_EXP, "N_HYPACE: %d NIC TXINTQ: %ld", N_HYPACE,
      (sizeof(nic_txintq_t) + (sizeof(nic_txint_elem_t *) * nic_txintq[0]->qsize)));
  iprint(LVL_EXP, "N_HYPACE: %d PROFQ HDR: %lld", N_HYPACE,
      (sizeof(profq_hdr_t) + (sizeof(profq_elem_t *) * profq_hdr[0]->qsize)));

  tot_gpace_mem = (tot_largef_q_size + tot_ts_irq_q_size + tot_pcm_rxq_size + tot_pcm_txq_size
        + tot_proflog_q_size + tot_pmap_htbl_size + tot_pcm_htbl_size
        + tot_global_nicq_size + tot_global_nicfl_size + tot_defpayload_arr_size
        + tot_defpayload_dmaddr_arr_size + tot_defskb_arr_size
        + tot_global_dummy_nicq_size + tot_global_txintq_size + tot_nic_data_arr_size
        + tot_profq_hdr_size + tot_profq_fl2_size);
  iprint(LVL_EXP, "=== Tot GPace mem: %llu largef mem: %llu diff: %llu ==="
      , tot_gpace_mem, tot_largef_q_size, (tot_gpace_mem - tot_largef_q_size));
  return;
}

static void
mod_cleanup_all_datastructures(void)
{
#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  int i;
  uint16_t old_pkt_prod, old_bd_prod;
  uint16_t old_bd_cons;

  int ret = 0;
  struct bnx2x_fp_txdata *txdata = NULL;
  struct bnx2x *bp = NULL;
  int cos;
#endif

#if SME_CONFIG != SME_DBG_LOG
  if (sme_ctrl_dev_registered)
    misc_deregister(&sme_ctrl_dev);

  remove_logger_thread(NULL);
  cleanup_sme_log_q(&proflog_q);
#endif /* SME_CONFIG != SME_DBG_LOG */

#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB
  /*
   * notify hypervisor to unmap addresses and free its memory
   */
  ret = HYPERVISOR_sme_pacer(PACER_OP_UNSET_ADDRS, NULL);
  iprint(LVL_INFO, "UNSET profq(%d:%d) ret %d"
      , atomic_read(&profq_hdr[0]->u.sorted2.head)
      , atomic_read(&profq_hdr[0]->u.sorted2.tail), ret);
  msleep(100);

  bp = vf_bp;
  for_each_tx_queue(bp, i) {
    struct bnx2x_fastpath *fp = &bp->fp[i];
    for_each_cos_in_tx_queue(fp, cos)
      ret = bnx2x_clean_tx_queue(bp, fp->txdata_ptr[cos]);
  }

  // reset producer indices to point at the last doorbell value,
  // to allow the NIC to function normally after removing module.
  for_each_eth_queue(vf_bp, i) {
    if (i < SME_PACER_CORE || i >= (SME_PACER_CORE + N_HYPACE)) {
      txdata = vf_bp->fp[i].txdata_ptr[0];
      iprint(LVL_EXP, "TXQ[%d] bd_prod %u bd_cons %u pkt_prod %u pkt_cons %u hw_cons %u"
          , i, txdata->tx_bd_prod, txdata->tx_bd_cons
          , txdata->tx_pkt_prod, txdata->tx_pkt_cons
          , le16_to_cpu(*txdata->tx_cons_sb)
          );
      continue;
    }
    txdata = vf_bp->fp[i].txdata_ptr[0];
    old_pkt_prod = txdata->tx_pkt_prod;
    old_bd_prod = txdata->tx_bd_prod;
    old_bd_cons = txdata->tx_bd_cons;
    txdata->tx_pkt_cons = le16_to_cpu(*txdata->tx_cons_sb);
    txdata->tx_pkt_prod = txdata->tx_pkt_cons;
    txdata->tx_bd_prod = max_t(u16, txdata->tx_bd_prod, txdata->tx_db.data.prod);
    txdata->tx_bd_cons = txdata->tx_bd_prod - 1;

    iprint(LVL_EXP, "TXQ[%d] bd_prod %u => %u bd_cons %u => %u\n"
        "pkt_prod %u => %u pkt_cons %u"
        , i, old_bd_prod, txdata->tx_bd_prod, old_bd_cons, txdata->tx_bd_cons
        , old_pkt_prod, txdata->tx_pkt_cons, le16_to_cpu(*txdata->tx_cons_sb)
        );
  }

  barrier();
  smp_mb();

  rtnl_lock();

  bnx2x_reload_if_running(vf_ndev);

  rtnl_unlock();

#endif
}

static void
mod_cleanup_all_datastructures2(void)
{
  int i;
  int hpi;
#if SME_CONFIG != SME_DBG_LOG
#if 0
  if (sme_ctrl_dev_registered)
    misc_deregister(&sme_ctrl_dev);

  remove_logger_thread(NULL);
  cleanup_sme_log_q(&proflog_q);
#endif /* SME_CONFIG != SME_DBG_LOG */
#endif

#if CONFIG_XEN_PACER
  for (i = 0; i < RX_NCPUS; i++) {
    cleanup_pcm_q(&pcm_txq[i]);
    cleanup_pcm_q(&pcm_rxq[i]);
  }

  for (i = 0; i < NUM_PACERS; i++) {
    free_all_sme_channel_map(&scm_list[i]);
  }

#if CONFIG_XEN_PACER_DB
  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (profq_fl2[hpi]) {
      cleanup_profq_freelist2(profq_fl2[hpi]);
      kfree(profq_fl2[hpi]);
      profq_fl2[hpi] = NULL;
    }
  }

  /*
   * cleanup and free primary profile queue
   */
  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (profq_hdr[hpi]) {
      cleanup_profq_hdr(profq_hdr[hpi]);
      kfree(profq_hdr[hpi]);
      profq_hdr[hpi] = NULL;
    }
  }

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (nic_data_arr[hpi]) {
      for (i = 0; i < (PROFILE_QSIZE - 2); i++) {
        kfree(nic_data_arr[hpi][i]);
      }
      kfree(nic_data_arr[hpi]);
    }
  }
#endif

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (nic_txintq[hpi]) {
      cleanup_nic_txintq(nic_txintq[hpi]);
      kfree(nic_txintq[hpi]);
    }
  }

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (default_payload[hpi]) {
      for (i = 0; i < NUM_TX_BD; i++) {
        dma_free_coherent(&vf_bp->pdev->dev, PAGE_SIZE, default_payload[hpi][i],
          default_payload_dmaddr[hpi][i]);
      }

      kfree(default_payload[hpi]);
      kfree(default_payload_dmaddr[hpi]);
    }
  }

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (default_skb[hpi]) {
      for (i = 0; i < NUM_TX_BD; i++)
        kfree_skb_partial(default_skb[hpi][i], true);
      kfree(default_skb[hpi]);
    }
  }

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (global_nic_freelist[hpi]) {
      cleanup_nic_freelist(global_nic_freelist[hpi]);
      kfree(global_nic_freelist[hpi]);
    }
  }

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (global_dummy_nicq[hpi]) {
      cleanup_global_nicq_hdr(global_dummy_nicq[hpi]);
      kfree(global_dummy_nicq[hpi]);
    }
  }

  for (hpi = 0; hpi < N_HYPACE; hpi++) {
    if (global_nicq[hpi]) {
      cleanup_global_nicq_hdr(global_nicq[hpi]);
      kfree(global_nicq[hpi]);
    }
  }
#endif /* CONFIG_XEN_PACER_DB */
#endif /* CONFIG_XEN_PACER */

  for (i = 0; i < RX_NCPUS; i++) {
    cleanup_sme_q(&ts_irq_q[i]);
  }

  reset_profile(&default_profile);

  for (i = 0; i < MAX_STATQ_ARRAYS; i++) {
//    print_generic_q(*largef_q[i]);
    cleanup_generic_q(*largef_q[i]);
    vfree(largef_q[i]);
  }
  itrace;
}
#endif /* CONFIG_PACER_BNX2X */

SME_DECL_STAT_ARR(init_pcm_cnt, NUM_PACERS);
SME_DECL_STAT_ARR(gc_pcm_cnt, NUM_PACERS);

SME_DECL_STAT_ARR(rx_ooo_cnt, NUM_PACERS);
SME_DECL_STAT_ARR(dma_lat, RX_NCPUS);

SME_DECL_STAT_ARR(prev_pkt_cnt, RX_NCPUS);
SME_DECL_STAT_ARR(prev_unused_cnt, RX_NCPUS);

SME_DECL_STAT_ARR(defprof_install, RX_NCPUS);
SME_DECL_STAT_ARR(ack_cwnd_update, RX_NCPUS);
SME_DECL_STAT_ARR(loss_cwnd_update, RX_NCPUS);
SME_DECL_STAT_ARR(rtx_prof_extend, RX_NCPUS);

SME_DECL_STAT_ARR(phash_dist, MAX_HP_ARGS);

SME_DECL_STAT_ARR(pcm_sl, RX_NCPUS);

SME_DECL_STAT_ARR(nicq_ins, RX_NCPUS);
SME_DECL_STAT_ARR(dummy_per_irq, RX_NCPUS);
SME_DECL_STAT_ARR(real_per_irq, RX_NCPUS);

int
sme_init_all_stats(void)
{
#if SME_CONFIG_STAT
#if 0
  int i;
  char statstr[64];
#endif
#endif
  long start, end, step, dist_len;

  start = 0;
  end = 100000;
  dist_len = 20;
  step = (end - start) / dist_len;

  SME_INIT_STAT_ARR(init_pcm_cnt, start, 10000, 10, NUM_PACERS, "init pcm cnt %d");
  SME_INIT_STAT_ARR(gc_pcm_cnt, start, 10000, 10, NUM_PACERS, "gc pcm cnt %d");

  SME_INIT_STAT_ARR(rx_ooo_cnt, start, 1000000, 1000, NUM_PACERS,
      "#ooo RX pkts %d");

  SME_INIT_STAT_ARR(dma_lat, start, 1000000, 1000, RX_NCPUS, "NIC write lat %d");

  SME_INIT_STAT_ARR(prev_pkt_cnt, 0, 500, 1, RX_NCPUS,
      "prev prof slots used %d");
  SME_INIT_STAT_ARR(prev_unused_cnt, 0, 500, 1, RX_NCPUS,
      "prev prof slots unused %d");

  dist_len = 10;
  end = 50000;
  step = (end - start) / dist_len;
  SME_INIT_STAT_ARR(defprof_install, start, end, step, RX_NCPUS,
      "def prof install %d");
  SME_INIT_STAT_ARR(ack_cwnd_update, start, end, step, RX_NCPUS,
      "ack cwnd update %d");
  SME_INIT_STAT_ARR(loss_cwnd_update, start, end, step, RX_NCPUS,
      "loss cwnd update %d");
  SME_INIT_STAT_ARR(rtx_prof_extend, start, end, step, RX_NCPUS,
      "rtx prof extend %d");

  SME_INIT_STAT_ARR(phash_dist, 0, MAX_HP_ARGS, 1, RX_NCPUS,
      "hypace %d");

  SME_INIT_STAT_ARR(pcm_sl, start, end, step, RX_NCPUS, "pcm spinlock %d");
  SME_INIT_STAT_ARR(nicq_ins, start, end, step, RX_NCPUS, "nicq insert %d");
  SME_INIT_STAT_ARR(dummy_per_irq, 0, 100, 1, RX_NCPUS, "#dummies/Tx irq %d");
  SME_INIT_STAT_ARR(real_per_irq, 0, 100, 1, RX_NCPUS, "#reals/Tx irq %d");

  return 0;
}

void
sme_cleanup_all_stats(void)
{
  SME_DEST_STAT_ARR(init_pcm_cnt, 0, NUM_PACERS);
  SME_DEST_STAT_ARR(gc_pcm_cnt, 0, NUM_PACERS);

  SME_DEST_STAT_ARR(rx_ooo_cnt, 0, NUM_PACERS);

  SME_DEST_STAT_ARR(dma_lat, 0, RX_NCPUS);

  SME_DEST_STAT_ARR(prev_pkt_cnt, 1, RX_NCPUS);
  SME_DEST_STAT_ARR(prev_unused_cnt, 1, RX_NCPUS);

  SME_DEST_STAT_ARR(defprof_install, 1, RX_NCPUS);
  SME_DEST_STAT_ARR(ack_cwnd_update, 1, RX_NCPUS);
  SME_DEST_STAT_ARR(loss_cwnd_update, 1, RX_NCPUS);
  SME_DEST_STAT_ARR(rtx_prof_extend, 1, RX_NCPUS);

  SME_DEST_STAT_ARR(phash_dist, 1, RX_NCPUS);

  SME_DEST_STAT_ARR(pcm_sl, 1, RX_NCPUS);
  SME_DEST_STAT_ARR(nicq_ins, 1, RX_NCPUS);
  SME_DEST_STAT_ARR(dummy_per_irq, 1, RX_NCPUS);
  SME_DEST_STAT_ARR(real_per_irq, 1, RX_NCPUS);

}

static int __init
init_sme_module(void)
{
  printk(KERN_EMERG "SME kernel module init\n");

  my_ip_int = in_aton(my_ip);
  my_port_int = (uint16_t) my_port;
  host_type_int = host_type;
  mod_config_int = mod_config;
  if (strncmp(host_type_int, "FE", strlen("FE")) == 0) {
    bknd_ip_int = in_aton(bknd_ip);
    bknd_port_int = (uint16_t) bknd_port;
  }
  iprint(LVL_DBG, "Host type: %s, my IP: %s, %d, port: %u"
      , host_type_int, my_ip, my_ip_int, my_port_int);

#ifdef CONFIG_PACER_BNX2X
  sme_init_all_stats();

  mod_init_all_datastructures();

  sme_override_functions(1);
#endif

  misc_register(&pdbg_log_dev);
  pdbg_log_dev_registered = 1;

#ifdef CONFIG_PACER_TCP
  ptcp_override_functions(1);
#endif

  return 0;
}

static void __exit
exit_sme_module(void)
{
#ifdef CONFIG_PACER_TCP
  ptcp_override_functions(0);
#endif

#ifdef CONFIG_PACER_BNX2X
  mod_cleanup_all_datastructures();

  sme_override_functions(0);

  mod_cleanup_all_datastructures2();

  barrier();
  smp_mb();

  sme_cleanup_all_stats();
#endif

  if (pdbg_log_dev_registered) {
    misc_deregister(&pdbg_log_dev);
  }

  printk(KERN_EMERG "SME kernel module exit\n");
}

module_init(init_sme_module);
module_exit(exit_sme_module);

MODULE_AUTHOR("Aastha Mehta");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("SME kernel module");
