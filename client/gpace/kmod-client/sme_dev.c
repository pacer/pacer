#include <linux/mm.h>
#include <linux/mm_types.h>

#include <linux/fs.h>
#include <linux/miscdevice.h>

#include "sme_debug.h"
#include "sme_time.h"

#include "sme_skb.h"
#include "sme_q_single.h"

#include "profile_map.h"
#include "sme_helper.h"

#include "sme_xen.h"

#include "../../include/msg_hdr.h"

#include <asm/ioctl.h>
#include <asm/xen/hypercall.h>

#ifdef CONFIG_XEN_PACER
extern profq_hdr_t *profq_hdr[MAX_HP_ARGS];
extern char *default_payload;
extern struct sk_buff *default_skb;
extern struct bnx2x *vf_bp;
#endif

extern list_t scm_list[NUM_PACERS];

#define SME_IOCTL _IO('S', 0)


enum {
  SME_CTRL_DEV = 0,
};

int
sme_parse_profile_conn_buf(char __user *uarg, size_t len)
{
  char *buf = NULL;
  int ret = 0;
  uint64_t msg_type;
#if CONFIG_XEN_PACER
  profile_conn_map_t *pcm = NULL;
  sme_channel_map_t *scm = NULL;
  conn_t conn_id;
#if CONFIG_XEN_PACER_DB
  profile_t *p = NULL;
  profq_elem_t *def_p = NULL;
#endif
#if HYPACE_CFG < HP_BATCH
  int num_default_frames, num_pid_frames;
  uint64_t default_first_latency, pid_first_latency;
  uint64_t now;
  uint64_t hw_timer_reg = 0;
  prof_update_arg_t parg;
  char pid_hash[64];
#endif
#endif /* CONFIG_XEN_PACER */

#if CONFIG_PROF_LOG
  profile_conn_map_t *dep_pcm = NULL;
  scm_pcm_ptr_t *pcm_ptr = NULL;
  char *mdata = NULL;
  log_elem_t *le = NULL;
#endif

  buf = kzalloc(len, GFP_KERNEL);
  if (!buf) {
    iprint(LVL_INFO, "alloc %lu bytes failed", len);
    return -ENOMEM;
  }
  ret = copy_from_user(buf, uarg, len);
  if (ret) {
    iprint(LVL_INFO, "copy err %d", ret);
    if (buf) {
      kfree(buf);
    }
    return ret;
  }

  get_msg_cmd(buf, msg_type);

  /*
   * ================
   * profile database
   * ================
   */

  /*
   * first install profile DB in hypervisor. if it succeeds,
   * then install it in guest, otherwise return error to user.
   */
#if CONFIG_XEN_PACER
  if (msg_type == MSG_T_PMAP_IHASH) {
#if CONFIG_XEN_PACER_DB
    ret = HYPERVISOR_sme_pacer(PACER_OP_PMAP_IHASH, buf);
    iprint(LVL_DBG, "pmap hypercall ret %d", ret);
#endif
    if (ret < 0)
      goto exit;
  }

  print_profile_conn_buf(buf, len);
  ret = parse_profile_conn_buf(&scm_list[0], buf, len);
  iprint(LVL_DBG, "parse profile buf msg_type %llu ret %d", msg_type, ret);

  if (msg_type == MSG_T_PMAP_IHASH || msg_type == MSG_T_SSL_HANDSHAKE)
    goto exit;

  /*
   * ======
   * marker
   * ======
   */

  /*
   * TODO:
   * lookup profile DB and get actual profile id
   * check if profile expired or not. if not, proceed below
   * instantiate appropriate number of default pkts in TX ring as per profile
   * (check how many slots already created by swirq for default profile)
   * replace enqueueing of new profiles below with lookup for matching profiles
   * check for earliest timer event, and if before next xen timer, hypercall
   */
  ret = parse_conn_from_profile_buf(buf, len, &conn_id);

  if (ret < 0)
    return ret;

  if (conn_id.in_port == conn_id.out_port
      && conn_id.src_ip == conn_id.dst_ip
      && conn_id.src_port == conn_id.dst_port) {
    ret = get_profile_conn_info_from_skb_header(conn_id.out_ip, conn_id.dst_ip,
        conn_id.out_port, conn_id.dst_port, &scm_list[0], &scm, &pcm, 0, 0, 0, NULL);
  } else {
    ret = get_profile_conn_info_from_skb_header(conn_id.in_ip, conn_id.src_ip,
        conn_id.in_port, conn_id.src_port, &scm_list[0], &scm, &pcm, 0, 0, 0, NULL);
  }

  if (ret < 0 && (!scm || !pcm))
    return -EINVAL;

#if CONFIG_XEN_PACER_DB
  def_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
  p = pcm->p;
  if (def_p->state != PSTATE_PROFQ)
    goto noprofq;

#if HYPACE_CFG >= HP_BATCH
  memcpy(def_p->id_ihash.byte, p->id_ihash.byte, sizeof(ihash_t));
#else
  parg.profq_idx = pcm->profq_idx;
  parg.cmd_type = P_UPDATE_PROF_ID;

  num_default_frames = get_total_frames(def_p->pmap_prof);
  num_pid_frames = p ? get_total_frames(p) : 2;

  default_first_latency = get_profile_timer_at_idx(def_p->pmap_prof,
      def_p->req_ts + def_p->cwnd_latency_shift,
      def_p->next_timer_idx);
  pid_first_latency = get_profile_timer_at_idx(p,
      def_p->req_ts + def_p->cwnd_latency_shift,
      def_p->next_timer_idx);
  memset(pid_hash, 0, 64);
  prt_hash((char *) &p->id_ihash, sizeof(ihash_t), pid_hash);

  now = get_current_time(SCALE_RDTSC);
  rdmsrl(MSR_IA32_TSC_DEADLINE, hw_timer_reg);

  ret = check_profile_prefix(def_p, def_p->pmap_prof, p);
  if (ret < 0) {
    iprint(LVL_INFO, "[%pI4 %u, %pI4 %u]\nG%lld custom %s fail"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->profq_idx, pid_hash
      );
    goto noprofq;
  }

  if (now < pid_first_latency && now < default_first_latency) {
    memcpy(def_p->id_ihash.byte, p->id_ihash.byte, sizeof(ihash_t));
//    def_p->priv = p->priv;
    def_p->pmap_prof = p;
    if (pid_first_latency <= default_first_latency
        || pid_first_latency < hw_timer_reg) {
      ret = HYPERVISOR_sme_pacer(PACER_OP_PROF_UPDATE, &parg);
#if SME_DEBUG_LVL <= LVL_INFO
#if 0
      iprint(LVL_EXP, "REPLACE (%d:%d) #slots %d ret %d %s"
          , atomic_read(&profq_hdr[pcm->hypace_idx]->queue_p[0]->next)
          , (int) pcm->profq_idx, num_pid_frames, ret, pid_hash
          );
#endif
      iprint(LVL_INFO, "head %d idx %d pcm [%pI4 %u, %pI4 %u]\n"
          "timer idx %d #frames %d => %d ref %lld cwnd shift %lld "
          "now %lld HW TSC REG %llu\n"
          "idx latency %lld => %lld %s PREFIX CHK ret %d"
          , atomic_read(&profq_hdr[pcm->hypace_idx]->queue_p[0]->next)
          , (unsigned int) pcm->profq_idx
          , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
          , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
          , def_p->next_timer_idx, num_default_frames, num_pid_frames
          , def_p->req_ts, def_p->cwnd_latency_shift, now, hw_timer_reg
          , default_first_latency, pid_first_latency, pid_hash, ret);
#endif
    }
  }

#endif /* HYPACE_CFG */

noprofq:

#endif /* CONFIG_XEN_PACER_DB */

#if CONFIG_PROF_LOG
  pcm->rcvd_sess_marker = 1;
  if (pcm->s->sme_type != SME_FE_BE) {
    copy_pskb(&(pcm->last_req_pkt), &(pcm->rec_req_pkt), 1);
  } else {
    /*
     * copy last_req_pkt from parent pcm. to be used only for
     * FE-BE pcm, which have exactly one parent pcm.
     */
    list_for_each_entry(pcm_ptr, &pcm->pcm_ptr_list, listp) {
      dep_pcm = (profile_conn_map_t *) pcm_ptr->ptr;
      iprint(LVL_DBG, "DEP: %pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u"
          ", PCM state: %d"
          , (void *) &dep_pcm->sec_conn_id.src_ip, dep_pcm->sec_conn_id.src_port
          , (void *) &dep_pcm->sec_conn_id.in_ip, dep_pcm->sec_conn_id.in_port
          , (void *) &dep_pcm->sec_conn_id.out_ip, dep_pcm->sec_conn_id.out_port
          , (void *) &dep_pcm->sec_conn_id.dst_ip, dep_pcm->sec_conn_id.dst_port
          , dep_pcm->state);
      copy_pskb(&(pcm->last_req_pkt), &(dep_pcm->last_req_pkt), 1);
    }
  }

  if (pcm->marker_data && pcm->marker_data_len) {
    mdata = (char *) kzalloc(pcm->marker_data_len, GFP_KERNEL);
    memcpy(mdata, pcm->marker_data, pcm->marker_data_len);
    alloc_init_log_elem(&le, &(pcm->last_req_pkt), mdata, pcm->marker_data_len);
    add_log_elem(&pcm->prof_log, le);
  } else {
    BUG_ON(1);
    //alloc_init_log_elem(&le, &(pcm->last_req_pkt), NULL, 0);
  }
#endif /* CONFIG_PROF_LOG */

exit:

#endif /* CONFIG_XEN_PACER */

  if (buf) {
    kfree(buf);
  }

  return 0;
}

/*
 * =============================
 * misc device for app I/O calls
 * =============================
 */

int
sme_ctrl_open(struct inode *inode, struct file *file)
{
  return 0;
}

int
sme_ctrl_release(struct inode *inode, struct file *file)
{
  return 0;
}

ssize_t
sme_ctrl_read(struct file *file, char __user *buf, size_t len, loff_t *pos)
{
  return 0;
}

int
sme_mmap_common(struct file *filp, struct vm_area_struct *vma, int devtype)
{
  int err = 0;
  int size = 0;
  int num_pages = 0;
  int i;
  struct page *pg = NULL;
  char *ptr = NULL;
  unsigned long uaddr = 0;
  sme_log_q_t *log_q = NULL;

  if (devtype == SME_CTRL_DEV)
    log_q = proflog_q;

  // bail out early if profile log is not configured
  if (!log_q)
    return -ENOMEM;

  // cannot share the page with anyone
  if (vma->vm_flags & (VM_MAYSHARE | VM_SHARED))
    return -EINVAL;

  // mapping cannot be kept across forks, cannot be expanded,
  // and is not a "normal" page
  vma->vm_flags |= VM_DONTCOPY | VM_DONTEXPAND | VM_READ | VM_WRITE;
  //vma->vm_flags |= VM_DONTCOPY | VM_DONTEXPAND | VM_READ;

  // do not want the first write access to trigger a "minor" page fault
  // to mark the page dirty. this is transient, private memory, we do not
  // care if it was touched or not. PAGE_SHARED means RW access, but not
  // execute, and avoids copy-on-write behaviour.
  vma->vm_page_prot = PAGE_SHARED;

  uaddr = vma->vm_start;
  size = vma->vm_end - vma->vm_start;

  iprint(LVL_DBG, "mmap start: %lu, end: %lu, off: %lu, size: %d"
      , vma->vm_start, vma->vm_end, vma->vm_pgoff, size);

  ptr = (char *) log_q;
  if (vma->vm_pgoff == 0) {
    pg = vmalloc_to_page((void *) ptr);
    err = vm_insert_page(vma, uaddr, pg);
    if (size <= PAGE_SIZE)
      return err;

    uaddr += PAGE_SIZE;
  }

  ptr = (char *) log_q + PAGE_SIZE;
  num_pages = ((log_q->max_qbuf*MAX_QBUF_SIZE)-1) / PAGE_SIZE + 1;
  for (i = 0; i < num_pages; i++) {
    //ptr = (char *) &qbuf_list[i*(PAGE_SIZE/MAX_QBUF_SIZE)];
    pg = vmalloc_to_page((void *) ptr);
    err = vm_insert_page(vma, uaddr, pg);
    iprint(LVL_DBG, "%d. uaddr: %lu, ptr: %p, pg: %p", i, uaddr, ptr, pg);
    uaddr += PAGE_SIZE;
    ptr += PAGE_SIZE;
  }

  return err;
}

int
sme_ctrl_mmap(struct file *filp, struct vm_area_struct *vma)
{
  return sme_mmap_common(filp, vma, SME_CTRL_DEV);
}

long
sme_ctrl_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
  int ret = 0;
  void __user *uarg = (void __user *) arg;
  msg_hdr_t hdr;

  switch (cmd) {
    case SME_IOCTL:
      ret = copy_from_user(&hdr, uarg, sizeof(msg_hdr_t));
      if (ret) {
        iprint(LVL_INFO, "SME IOCTL copy err %d", ret);
        return ret;
      }
      ret = sme_parse_profile_conn_buf(uarg, sizeof(msg_hdr_t)+hdr.length);
        iprint(LVL_DBG, "SME IOCTL success %d", ret);
      break;

    default:
      iprint(LVL_ERR, "Invalid ioctl %d", cmd);
      ret = -EINVAL;
      break;
  }

  return ret;
}

struct file_operations sme_ctrl_ops = {
  .owner = THIS_MODULE,
  .open = sme_ctrl_open,
  .release = sme_ctrl_release,
  .read = sme_ctrl_read,
  .mmap = sme_ctrl_mmap,
  .unlocked_ioctl = sme_ctrl_ioctl,
};

struct miscdevice sme_ctrl_dev = {
  .name = "SMECTRL",
  .minor = MISC_DYNAMIC_MINOR,
  .fops = &sme_ctrl_ops,
  .mode = 0666,
};
