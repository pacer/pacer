/*
 * conn.c
 *
 * created on: Aug 25, 2017
 * author: aasthakm
 * 
 * API on conn pair identifier datastructure
 */

#include <uapi/asm-generic/errno-base.h>
#include "sme_debug.h"
#include "conn.h"

extern uint32_t my_ip_int;
extern uint16_t my_port_int;
extern uint32_t bknd_ip_int;
extern uint16_t bknd_port_int;

void
init_conn(conn_t *conn_id, uint32_t src_ip, uint32_t in_ip,
    uint32_t out_ip, uint32_t dst_ip, uint16_t src_port, uint16_t in_port,
    uint16_t out_port, uint16_t dst_port)
{
  if (!conn_id)
    return;

  conn_id->src_ip = src_ip;
  conn_id->in_ip = in_ip;
  conn_id->out_ip = out_ip;
  conn_id->dst_ip = dst_ip;
  conn_id->src_port = src_port;
  conn_id->in_port = in_port;
  conn_id->out_port = out_port;
  conn_id->dst_port = dst_port;
}

void
print_conn(conn_t *c)
{
  if (!c)
    return;

  iprint(LVL_DBG, "CONN(%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u)",
      (void *) &c->src_ip, c->src_port, (void *) &c->in_ip, c->in_port,
      (void *) &c->out_ip, c->out_port, (void *) &c->dst_ip, c->dst_port);
}

/*
 * ====================
 * comparator functions
 * ====================
 */

int
compare_conn_generic(conn_t *c1, conn_t *c2, int compare_type)
{
  int ret;
  switch (compare_type) {
    case COMPARE_CLIENT_SCM:
      ret = compare_client_scm_conn(c1, c2);
      break;
    case COMPARE_CROSSTIER_SCM:
      ret = compare_ct_scm_conn(c1, c2);
      break;
    case COMPARE_INC_PKT:
      ret = compare_inc_pkt(c1, c2);
      break;
    default:
      ret = -EINVAL;
  }
  //if (ret != 1) {
    iprint(LVL_DBG, "cmp type: %d, ret: %d\n"
        "key [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u] "
        "lookup [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
        , compare_type, ret
        , (void *) &c1->src_ip, c1->src_port, (void *) &c1->in_ip, c1->in_port
        , (void *) &c1->out_ip, c1->out_port, (void *) &c1->dst_ip, c1->dst_port
        , (void *) &c2->src_ip, c2->src_port, (void *) &c2->in_ip, c2->in_port
        , (void *) &c2->out_ip, c2->out_port, (void *) &c2->dst_ip, c2->dst_port
        );
    print_conn(c1);
    print_conn(c2);
  //}
  return ret;
}

/*
 * c2 is the search key
 */
int
compare_client_scm_conn(conn_t *c1, conn_t *c2)
{
  return (c1->in_ip == c2->in_ip && c1->in_port == c2->in_port
      && c1->out_ip == c2->out_ip && c1->out_port == c2->out_port
      && c2->in_ip == c2->out_ip && c2->in_port == c2->out_port);
}

int
compare_ct_scm_conn(conn_t *c1, conn_t *c2)
{
  return (c1->in_ip == c2->in_ip && c1->in_port == c2->in_port
      && c1->out_ip == c2->out_ip
      && c1->dst_ip == c2->dst_ip && c1->dst_port == c2->dst_port);
}

int
compare_inc_pkt(conn_t *c1, conn_t *c2)
{
  return (c1->src_ip == c2->src_ip && c1->src_port == c2->src_port
      && c1->in_ip == c2->in_ip && c1->in_port == c2->in_port);
}

int
inherit_key_fields(conn_t *conn_id, conn_t *sconn_id)
{
  if (!conn_id || !sconn_id)
    return -EINVAL;

  if (sconn_id->src_ip == 0 && conn_id->src_ip != 0)
    sconn_id->src_ip = conn_id->src_ip;
  if (sconn_id->in_ip == 0 && conn_id->in_ip != 0)
    sconn_id->in_ip = conn_id->in_ip;
  if (sconn_id->out_ip == 0 && conn_id->out_ip != 0)
    sconn_id->out_ip = conn_id->out_ip;
  if (sconn_id->dst_ip == 0 && conn_id->dst_ip != 0)
    sconn_id->dst_ip = conn_id->dst_ip;
  if (sconn_id->src_port == 0 && conn_id->src_port != 0)
    sconn_id->src_port = conn_id->src_port;
  if (sconn_id->in_port == 0 && conn_id->in_port != 0)
    sconn_id->in_port = conn_id->in_port;
  if (sconn_id->out_port == 0 && conn_id->out_port != 0)
    sconn_id->out_port = conn_id->out_port;
  if (sconn_id->dst_port == 0 && conn_id->dst_port != 0)
    sconn_id->dst_port = conn_id->dst_port;
  return 0;
}

