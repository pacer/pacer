/*
 * sme_core.c
 *
 * created on: Jan 30, 2017
 * author: aasthakm
 *
 * implementation of the core network driver
 * interception interfaces
 */

#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <linux/tcp.h>

#include <linux/smp.h>

#include <asm/xen/hypercall.h>
#include <xen/page.h>

#include "sme_config.h"
#include "sme_debug.h"

#include "sme_core.h"
#include "sme_common.h"
#include "sme_statistics.h"

#include "sme_skb.h"
#include "sme_skb_dummy.h"
#include "sme_q_common.h"
#include "sme_q_single.h"

#include "sme_time.h"
#include "profile_map.h"
#include "sme_helper.h"

#include "profile_log.h"
#include "../../include/conn_common.h"

#include "sme_stats_decl.h"

#include "sme_xen.h"
#include "generic_q.h"
#include "ptcp/ptcp_core.h"

#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"
#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x_cmn.h"
#include "../linux-client-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x_hsi.h"

#define DBG_BUF_SIZE 1024

extern uint32_t my_ip_int;
extern uint16_t my_port_int;
extern uint32_t bknd_ip_int;
extern uint16_t bknd_port_int;
extern uint8_t mod_config_int;
extern char *host_type_int;
extern uint16_t dummy_server_port;
extern list_t scm_list[NUM_PACERS];
#if CONFIG_XEN_PACER
extern profq_hdr_t *profq_hdr[MAX_HP_ARGS];
extern profq_freelist2_t *profq_fl2[MAX_HP_ARGS];
extern char *default_payload;
extern struct sk_buff *default_skb;
extern ihash_t DEFAULT_PROFILE_IHASH;
extern ihash_t DEFAULT_HTTP_PROF_IHASH;
extern ihash_t DEFAULT_SSLFIN_PROF_IHASH;
extern ihash_t DEFAULT_HTTPS_PROF_IHASH;
extern ihash_t DEFAULT_MW_PROF_IHASH;
extern ihash_t DEFAULT_VIDEO_PROF_IHASH;
extern ihash_t pid1, pid2, pid3;
extern pcm_q_t pcm_rxq[RX_NCPUS];
extern pcm_q_t pcm_txq[RX_NCPUS];

extern struct bnx2x *vf_bp;

extern int profq_insert(profq_hdr_t *profq_hdr, profq_freelist2_t *pfl,
    profile_conn_map_t *pcm, ihash_t *pid, uint64_t req_ts, uint32_t seqno,
    int cwm, int *ins_idx);

#endif

#if SME_CONFIG != SME_DBG_LOG && CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB && CONFIG_PACER_TCP
static inline bool ptcp_may_raise_cwnd(const struct sock *sk, const int flag)
{
#define FLAG_DATA_ACKED       0x04
#define FLAG_SYN_ACKED        0x10
#define FLAG_DATA_SACKED      0x20
#define FLAG_ACKED            (FLAG_DATA_ACKED|FLAG_SYN_ACKED)
#define FLAG_FORWARD_PROGRESS (FLAG_ACKED|FLAG_DATA_SACKED)
  if (tcp_sk(sk)->reordering > sock_net(sk)->ipv4.sysctl_tcp_reordering)
    return flag & FLAG_FORWARD_PROGRESS;

  return flag & FLAG_DATA_ACKED;
}

void
mod_update_cwnd(struct sk_buff *skb, struct sock *sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int num_marked_lost,
    char *extra_dbg_string)
{
  struct inet_sock *inet = inet_sk(sk);
  struct tcp_sock *tp = tcp_sk(sk);

  uint32_t s_addr, d_addr;
  uint16_t s_port, d_port;

  int ret = 0;
  uint32_t old_cwnd = 0;
  uint32_t new_cwnd = 0;
  int delta = 0;
  int delta1 = 0;
  int old_next = 0;

  prof_update_arg_t parg;
  sme_channel_map_t *scm = NULL;
  profile_conn_map_t *pcm = NULL;
  profq_elem_t *prof_p = NULL;
  struct tcphdr *th = NULL;
  int64_t pidx = 0;
  int old_prof = 0;
  int b_inflight = (b_pkts + b_retrans - (b_lost + b_sack));
  int a_inflight = (a_pkts + a_retrans - (a_lost + a_sack));
  int do_hypercall = 0;
  char dbg_str[256];

  uint32_t fake_cwm = 0;
  int p_tcp_allowance = 0;
  int p_fakecwm_allowance = 0;
  uint64_t loss_cwnd_ts = 0;
  uint64_t ack_cwnd_ts = 0;
  uint64_t end_cwnd_ts = 0;
  uint64_t prof_extend_ts = 0;
  uint64_t end_prof_ts = 0;
  uint64_t ack_delta = 0;
  int rtx_end_prof_flag = 0;

  if (!sk) {
    iprint(LVL_INFO, "Missing sk %p skb %p", sk, skb);
    return;
  }

  d_addr = inet->inet_daddr;
  s_addr = inet->inet_saddr;
  d_port = ntohs(inet->inet_dport);
  s_port = ntohs(inet->inet_sport);

  if (!check_relevant_ports(s_port, d_port))
    return;

  ret = get_profile_conn_info_from_skb_header(s_addr, d_addr, s_port, d_port,
      &scm_list[0], &scm, &pcm, 0, 0, 0, NULL);

  if (ret < 0 || !scm || !pcm)
    return;

  if (!profq_hdr[pcm->hypace_idx] || !profq_hdr[pcm->hypace_idx]->queue_p)
    return;

  memset(dbg_str, 0, 256);
  if (!extra_dbg_string)
    extra_dbg_string = dbg_str;

  pidx = pcm->profq_idx;

  delta = 0;
  prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pidx];
  old_next = prof_p->next_timer_idx;
  old_cwnd = prof_p->cwnd_watermark;

  // timestamp of the ack that we may be processing potentially
  ack_cwnd_ts = prof_p->ack_ts;

  delta1 = (a_cwnd - a_inflight) - (b_cwnd - b_inflight);
  if (skb) {
    th = tcp_hdr(skb);
    iprint(LVL_DBG, "src %pI4 %u, dst %pI4 %u urg_ptr %d\n"
      "skb (%u:%u) tp (%u:%u) snd_nxt %u shared_seq %u snd_wnd %u\n"
      "snd_cwnd %d => %d inflight %d => %d "
      "pkts %d => %d lost %d => %d retrans %d => %d sack %d => %d\n"
      "delta1 %d acks %d "
      "sstart %d max_pkts_out %d snd_cwnd %d real %d "
      "lsndtime %u rcvtstamp %u RTO %lu"
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port, ntohs(th->urg_ptr)
      , TCP_SKB_CB(skb)->seq, TCP_SKB_CB(skb)->ack_seq
      , tp->write_seq, tp->rcv_nxt, tp->snd_nxt, tp->shared_seq, tp->snd_wnd
      , b_cwnd, a_cwnd, b_inflight, a_inflight
      , b_pkts, a_pkts, b_lost, a_lost, b_retrans, a_retrans, b_sack, a_sack
      , delta1, delivered, tcp_in_slow_start(tp), tp->max_packets_out
      , tp->snd_cwnd, atomic_read(&prof_p->real_counter)
      , tp->lsndtime, tp->rcv_tstamp, inet_csk(sk)->icsk_timeout
      );
  }

  p_tcp_allowance = ((tp->snd_una - pcm->rem_initial_seq) / SME_TCP_MTU) + a_cwnd;
  fake_cwm = p_tcp_allowance - pcm->rem_allowance;

  switch (caller) {
    case C_TCP_ACK:
      {
        new_cwnd = prof_p->cwnd_watermark;

        if (una_diff > 0) {
          new_cwnd += una_diff;
        }

        if (tcp_in_cwnd_reduction(sk)) {
          delta = (a_cwnd - b_cwnd);
//          if (new_cwnd + delta < 0 ||
//              new_cwnd + delta < prof_p->next_timer_idx)
          if (delta < 0) {
            delta = (a_cwnd - a_inflight);
            new_cwnd =
              max_t(uint32_t, prof_p->tail, atomic_read(&prof_p->real_counter))
              + prof_p->dummy_counter + delta;
          } else {
            new_cwnd += delta;
          }
        } else if (ptcp_may_raise_cwnd(sk, ack_flag)) {
          delta = (a_cwnd - b_cwnd);
          new_cwnd += delta;
        }

        prof_p->cwnd_watermark = new_cwnd;
      }
      break;

    case C_TCP_ENTER_LOSS:
      {
        /*
         * delta1 == 0 implies already in loss state. just because we enter here
         * again, we need not increment CWM, esp. if a new dummy got transmitted
         * in the remaining allowance available.
         *
         * TODO: hypace increments nxt when a timer is set. the timer may not
         * have expired yet, and a pkt may not hav been sent out. as a result,
         * it is possible that new_cwnd may go to the left of nxt, but this
         * should happen by at most 1 (?). in this case, remove the scheduled
         * timer in hypace and go into cwnd paused state.
         */
//        if (delta1 != 0)
        if (a_cwnd != b_cwnd || a_inflight != b_inflight)
        {
          prof_p->loss_ts = loss_cwnd_ts = get_current_time(SCALE_RDTSC);
          new_cwnd =
            max_t(uint32_t, prof_p->tail, atomic_read(&prof_p->real_counter))
            + prof_p->dummy_counter + 1;
        } else {
          // default case
          new_cwnd = prof_p->cwnd_watermark;
        }
        prof_p->cwnd_watermark = new_cwnd;
      }
      break;

    case C_TCP_UNDO_CWND_REDUCTION:
      {
        /*
         * sometimes undo_cwnd_reduction can cause cwnd to lower.
         * e.g. just upon entering loss, cwnd: 4 => 1, ssthresh: foo => 2,
         * slow start is 0 and CA state is now Loss.
         *
         * slowly tcp starts receiving acks, so cwnd: 1 => 2 => 3 => 5.
         * after this, there is an ack that raises snd_una above high_seq,
         * and tcp calls undo_cwnd_reduction. now in undo_cwnd_reduction,
         * cwnd = ssthresh * 2, or in our case cwnd = loss_prior_cwnd.
         * with both methods, cwnd: 5 => 4.
         *
         * if this is legal in tcp, then we have to reduce our watermark by 1.
         * TODO: check if by the time we come here to reduce watermark by 1,
         * hypace has not played all the slots allowed upto previous watermark
         * already.
         */
#if 0
        if (a_cwnd < b_cwnd)
          delta = a_cwnd - b_cwnd;
        new_cwnd = prof_p->cwnd_watermark + delta;
#endif
        new_cwnd = fake_cwm;
        prof_p->cwnd_watermark = new_cwnd;
      }
      break;

    default:
      {
        delta += (a_cwnd - b_cwnd);
        new_cwnd = prof_p->cwnd_watermark + delta;
        prof_p->cwnd_watermark = new_cwnd;
      }
      break;
  }

#if 0
  {
    largef_elem_t e;
    int dbg_idx = 0;
    memset((void *) &e, 0, sizeof(largef_elem_t));
    e.ts = get_current_time(SCALE_NS);
    e.snd_nxt = tp->snd_nxt;
    e.snd_una = tp->snd_una;
    e.rcv_nxt = tp->rcv_nxt;
    e.packets_out = tp->packets_out;
    e.lost_out = tp->lost_out;
    e.sacked_out = tp->sacked_out;
    e.retrans_out = tp->retrans_out;
    e.cwnd = tp->snd_cwnd;
    e.snd_wnd = tp->snd_wnd;
    e.rcv_wnd = tp->rcv_wnd;
    e.icsk_rto = inet_csk(sk)->icsk_rto;
    e.cwm = prof_p->cwnd_watermark;
    e.tail = prof_p->tail;
    e.real = atomic_read(&prof_p->real_counter);
    e.dummy = prof_p->dummy_counter;
    e.num_timers = prof_p->num_timers;
    e.nxt = prof_p->next_timer_idx;
    e.pidx = prof_p->profq_idx;
    e.ca_state = inet_csk(sk)->icsk_ca_state;
    e.caller = caller;
    e.coreid = smp_processor_id();
    e.owner = sk->sk_lock.owned;
    e.undo_marker = tp->undo_marker;
    e.undo_retrans = tp->undo_retrans;
    e.high_seq = tp->high_seq;
    dbg_idx = d_port % STATQ_MODULO;
    if (generic_q_empty(largef_q[dbg_idx]))
      largef_q[dbg_idx]->port = d_port;
    put_generic_q(largef_q[dbg_idx], (void *) &e);
  }
#endif

  // print stuff to plot
  p_fakecwm_allowance = new_cwnd + pcm->rem_allowance;

#if 0
  if (prof_p->cwnd_watermark < 0 || (int) tp->lost_out < 0
      || (int) tp->retrans_out < 0 || (int) tp->packets_out < 0
      /* || caller == C_TCP_ENTER_LOSS */
      /* || (tp->snd_nxt == tp->snd_una && tp->packets_out != 0) */) {
    struct bnx2x *bp = vf_bp;
    struct bnx2x_fp_txdata *txdata = bp->fp[pcm->hypace_idx].txdata_ptr[0];
    iprint(LVL_EXP,
      "%pS caller %d %s ca %d\n"
//      "[PLOT] pport %u pg %lld p_tcp_allowance %d p_fakecwm_allowance %d "
//      "pcwnd %d => %d pcwm %d => %d pfakecwm %d premallowance %d pca %d\n"
//      "rem_initial_seq %u rem_allowance %d, fake_cwm %d\n"
      "pkts %d => %d lost %d => %d retrans %d => %d sack_out %d => %d delivered %d CA %d high_seq %u\n"
      "cwnd %d => %d loss prior cwnd %u inflight %d => %d "
      "acks %d flag 0x%0x retrans ts %u saw ts %u rx tsecr %u\n"
      "snd_nxt %u snd_una %u write_seq %u timeout %lu currtime %lu RTO %u undo_retrans %d\n"
      "[PCM] state %d stage %d init_cwnd %d segs %d "
      "data_segs %d #mark lost %d delta1 %d delta %d sstart %d\n"
      "[PROF] G%lld state %d paused %d CWM %d => %d NXT %d => %d "
      "P %d T %d R %d D %d max_seqno %u\n"
      "BD %u (%u) %u (%u) PKT %u (%u) %u (%u)\n"
      "[TCP_SOCK] snd_ssthresh %u prior_ssthresh %u prior_cwnd %u "
      "snd_wnd %u cwnd_limited %d app_limited %d\n"
      "sk_state %d pending %d "
      "undo una %u icsk_rexmits %d rmem alloc %d rcvbuf %d fwd_alloc %d"
      , __builtin_return_address(2), caller, extra_dbg_string
      , inet_csk(sk)->icsk_ca_state
//      , d_port, pidx, p_tcp_allowance, p_fakecwm_allowance, b_cwnd, a_cwnd
//      , old_cwnd, new_cwnd, fake_cwm, pcm->rem_allowance
//      , inet_csk(sk)->icsk_ca_state
//      , pcm->rem_initial_seq, pcm->rem_allowance, fake_cwm
      , b_pkts, a_pkts, b_lost, a_lost, b_retrans, a_retrans, b_sack, a_sack
      , delivered, inet_csk(sk)->icsk_ca_state, tp->high_seq
      , b_cwnd, a_cwnd, tp->loss_prior_cwnd, b_inflight, a_inflight, una_diff
      , ack_flag, tp->retrans_stamp, tp->rx_opt.saw_tstamp, tp->rx_opt.rcv_tsecr
      , tp->snd_nxt, tp->snd_una, tp->write_seq, inet_csk(sk)->icsk_timeout
      , jiffies, inet_csk(sk)->icsk_rto, tp->undo_retrans
      , pcm->state, pcm->stage, pcm->tcp_cwnd
      , tp->segs_out, tp->data_segs_out, num_marked_lost, delta1, delta
      , tcp_in_slow_start(tp)
      , pidx, prof_p->state, atomic_read(&prof_p->is_paused), old_cwnd, new_cwnd
      , old_next, prof_p->next_timer_idx, prof_p->num_timers, prof_p->tail
      , atomic_read(&prof_p->real_counter), prof_p->dummy_counter, prof_p->max_seqno
      , txdata->tx_bd_prod, (unsigned int) TX_BD(txdata->tx_bd_prod)
      , txdata->tx_bd_cons, (unsigned int) TX_BD(txdata->tx_bd_cons)
      , txdata->tx_pkt_prod, (unsigned int) TX_BD(txdata->tx_pkt_prod)
      , txdata->tx_pkt_cons, (unsigned int) TX_BD(txdata->tx_pkt_cons)
      , tp->snd_ssthresh, tp->prior_ssthresh, tp->prior_cwnd
      , tp->snd_wnd, tp->is_cwnd_limited, tp->app_limited
      , sk->sk_state, inet_csk(sk)->icsk_pending
      , tp->undo_marker, inet_csk(sk)->icsk_retransmits
      , atomic_read(&sk->sk_rmem_alloc), sk->sk_rcvbuf, sk->sk_forward_alloc
      );
  }
#endif

  // last incoming ACK does not need to update cwnd
  // moreover the profq element may have been freed anyways.
  if (prof_p->state == PSTATE_FREELIST)
    goto skip_prof;
#if CONFIG_PROF_FREE == PROF_FREE_XEN
  if (prof_p->state == PSTATE_XEN_COMPL)
    goto skip_prof;
#endif

  if ((atomic_read(&prof_p->is_paused) == 1 || old_cwnd <= prof_p->next_timer_idx)
      && prof_p->cwnd_watermark > old_cwnd && num_marked_lost >= 0
      && prof_p->state == PSTATE_XEN_COMPL) {
    /*
     * we were at N == CWM == P. some pkts transmitted in the profile have
     * not been acked. so tcp entered loss, and we set CWM = N+1 to allow
     * just one pkt to be (re)transmitted from tcp. this actually requires
     * (i) creating a new slot (since all P slots have already been consumed),
     * (ii) and opening the hypace cwnd (similar to opening on an incoming ack).
     * below hypercall does (ii) only when there are pending slots in P to shift.
     * so we first create a slot in P, similar to the way we create a slot for
     * a retransmission pkt from tcp.
     *
     * also do this only on the newest outstanding profile.
     *
     * XXX: remember, never to extend the profile on loss, if the loss is
     * due to exhausting profile slots with sending all dummies. i.e. if
     * the pkts have never even gone out of the host, then we cannot allow
     * for their retransmission just like that -- it is insecure.
     */
    pcm->num_loss_extn++;
    old_prof = prof_p->num_timers;
    parg.profq_idx = pcm->profq_idx;
    parg.hypace_idx = pcm->hypace_idx;
    parg.cmd_type = P_EXTEND_PROF_RETRANS;
    prof_extend_ts = get_current_time(SCALE_RDTSC);

    ret = HYPERVISOR_sme_pacer(PACER_OP_PROF_UPDATE, &parg);
    iprint(LVL_INFO, "LOSS EXTN %pS ret %d sk state %d\n"
      "[%pI4 %u, %pI4 %u] G%lld state %d paused %d CWM %d => %d "
      "NXT %d => %d P %d => %d T %d R %d D %d\n"
      , __builtin_return_address(2), ret, sk->sk_state
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->profq_idx, prof_p->state, atomic_read(&prof_p->is_paused)
      , old_cwnd, prof_p->cwnd_watermark, old_next, prof_p->next_timer_idx
      , old_prof, prof_p->num_timers, prof_p->tail
      , atomic_read(&prof_p->real_counter), prof_p->dummy_counter
      );
    end_prof_ts = get_current_time(SCALE_RDTSC);
    rtx_end_prof_flag = 1;
  }

  old_next = prof_p->next_timer_idx;

  // Hypervisor invariant:
  // nxt timer idx <= watermark to allow transmission
  // TODO: check if there is a need for is_paused variable
  if ((atomic_read(&prof_p->is_paused) == 1 || old_cwnd <= prof_p->next_timer_idx)
      && prof_p->cwnd_watermark > old_cwnd) {
    parg.profq_idx = pidx;
    parg.hypace_idx = pcm->hypace_idx;
    do_hypercall = 1;
    if (caller == C_TCP_ENTER_LOSS) {
      parg.cmd_type = P_UPDATE_CWND_LOSS;
    } else {
      // both for normal ack and ack causing undo_cwnd_reduction
      parg.cmd_type = P_UPDATE_CWND_ACK;
    }
  }

  if (do_hypercall) {
    ret = HYPERVISOR_sme_pacer(PACER_OP_CWND_UPDATE, &parg);
    if (prof_p->cwnd_watermark < 0) {
      iprint(LVL_EXP, "%pS %pS CMD %d\n"
        "[%pI4 %u, %pI4 %u] G%llu state %d paused %d "
        "CWM %d => %d NXT %d => %d P %d T %d R %d D %d ret %d"
        , __builtin_return_address(2), __builtin_return_address(3), parg.cmd_type
        , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
        , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
        , pidx, prof_p->state, atomic_read(&prof_p->is_paused)
        , old_cwnd, prof_p->cwnd_watermark, old_next, prof_p->next_timer_idx
        , prof_p->num_timers, prof_p->tail, atomic_read(&prof_p->real_counter)
        , prof_p->dummy_counter
        , ret);
    }

    if (parg.cmd_type == P_UPDATE_CWND_LOSS) {
      end_cwnd_ts = get_current_time(SCALE_RDTSC);
      SME_ADD_COUNT_POINT_ARR(loss_cwnd_update,
          ((end_cwnd_ts - loss_cwnd_ts) * RDTSC_MUL_FACTOR) / RDTSC_DIV_FACTOR,
          smp_processor_id());
    } else if (parg.cmd_type == P_UPDATE_CWND_ACK) {
      end_cwnd_ts = get_current_time(SCALE_RDTSC);
      ack_delta = ((end_cwnd_ts-ack_cwnd_ts)*RDTSC_MUL_FACTOR) / RDTSC_DIV_FACTOR;
      SME_ADD_COUNT_POINT_ARR(ack_cwnd_update, ack_delta, smp_processor_id());
#if 0
      if (ack_delta > 10000000) {
        iprint(LVL_EXP, "caller %d PORT %u pcm last ack %u skb ack %u\n"
            " cwnd %u => %u CWM %u => %u NXT %u => %u inflight %u => %u delta1 %d"
            , caller, pcm->sec_conn_id.dst_port
            , pcm->last_real_ack, (skb ? TCP_SKB_CB(skb)->ack_seq : 0)
            , b_cwnd, a_cwnd, old_cwnd, new_cwnd
            , old_next, prof_p->next_timer_idx
            , b_inflight, a_inflight, delta1
            );
      }
#endif
    }

    if (rtx_end_prof_flag == 1) {
      SME_ADD_COUNT_POINT_ARR(rtx_prof_extend,
          ((end_prof_ts - prof_extend_ts) * RDTSC_MUL_FACTOR) / RDTSC_DIV_FACTOR,
          smp_processor_id());
    }
  }

skip_prof:

  pcm->tcp_cwnd = tp->snd_cwnd;

  // tcp sock vars start uninitialized and have garbage values.
  // do not assign from tcp sock vars until they are initialized.
  if (pcm->stage > PCM_STAGE_SSL)
    pcm->tcp_segs_out = tp->segs_out;

  return;
}

#endif

int
create_syn_marker_buf(char **marker_p, int *marker_len_p, ihash_t *pid)
{
  char *mbuf = NULL;
  int moff = 0;
  int num_pub = 1;
  int num_priv = 0;
  int marker_len = 0;
  ihash_t *pubhash = NULL;

  if (!marker_p || !marker_len_p)
    return -EINVAL;

  marker_len = 2 * sizeof(uint32_t) /* num_pub, num_priv */
    + sizeof(ihash_t) /* req ID */
    + sizeof(uint64_t) /* marker TS */
    + num_pub * sizeof(ihash_t)
    + num_priv * sizeof(ihash_t);

  mbuf = kzalloc(marker_len, GFP_KERNEL);
  if (!mbuf) {
    *marker_p = NULL;
    *marker_len_p = 0;
    return -ENOMEM;
  }

  ((uint32_t *) (mbuf + moff))[0] = num_pub;
  ((uint32_t *) (mbuf + moff))[1] = num_priv;
  moff += 2 * sizeof(uint32_t);

  // skip req ID
  moff += sizeof(ihash_t);

  ((uint64_t *) (mbuf + moff))[0] = get_current_time(SCALE_NS);
  moff += sizeof(uint64_t);

  pubhash = (ihash_t *) (mbuf + moff);
  if (pid)
    memcpy(pubhash->byte, pid->byte, sizeof(ihash_t));
  else
    memset(pubhash->byte, '0', sizeof(ihash_t));
  moff += sizeof(ihash_t);

  *marker_p = mbuf;
  *marker_len_p = marker_len;

  return 0;
}

void
mod_print_sk_buff(struct sk_buff *skb, struct sock *sk, char *extra_dbg_string)
{
  struct inet_sock *inet = inet_sk(sk);
  struct tcp_sock *tcp = tcp_sk(sk);

  unsigned int daddr = inet->inet_daddr;
  unsigned int saddr = inet->inet_saddr;

  char dbg_buff[8];
  memset(dbg_buff, 0, 8);
  if (!extra_dbg_string)
    extra_dbg_string = dbg_buff;
#if 0
  uint32_t user_data_len = 0;
  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  struct udphdr *udph = NULL;
  int ret = 0;

  if (skb) {
    ret = parse_skb_headers(skb, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, &user_data_len, NULL, (void **) &eth, (void **) &iph,
        (void **) &tcph, (void **) &udph);
  }
#endif

  if (strncmp(extra_dbg_string, "bnx2x_msix_sp_int", strlen("bnx2x_msix_sp_int"))
    == 0)
      return;

  if(ntohs(inet->inet_sport) == 443) {
    iprint2(LVL_INFO, "%pS caller1 %pS %s\n"
        "[INET_SOCK] daddr %pI4/%u saddr %pI4/%u\n"
        "[TCP_SOCK] snd_ssthresh %u snd_cwnd %u snd_cwnd_cnt %u "
        "snd_cwnd_used %u packets_out %u max_packets_out %u\n"
        "is_cwnd_limited %u snd_cwnd_stamp %u snd_una %u snd_nxt %u "
        "inflight %u\n"
        "prior_cwnd %u rcv_wnd %u snd_wnd %u app_limited %u delivered %u "
        "bytes_acked %llu [SKB] truesize %d\n"
        "[SK_SOCK] sk_sndbuf %u wmem_queued %u wmem_alloc %u prot_mem_alloc %lu "
        "sk_pacing_rate %d tsq_flags %lu\n"

//        " [TCP_HDR] seq: %u, ack_seq: %u"
				,__builtin_return_address(2), __builtin_return_address(3)
        ,extra_dbg_string
        ,(void*)&daddr
				,ntohs(inet->inet_dport)
				,(void*)&saddr
				,ntohs(inet->inet_sport)
				,tcp->snd_ssthresh
        ,tcp->snd_cwnd
        ,tcp->snd_cwnd_cnt
        ,tcp->snd_cwnd_used
        ,tcp->packets_out
        ,tcp->max_packets_out
        ,tcp->is_cwnd_limited
        ,tcp->snd_cwnd_stamp
        ,tcp->snd_una
        ,tcp->snd_nxt
        ,tcp_packets_in_flight(tcp)
        ,tcp->prior_cwnd
        ,tcp->rcv_wnd
        ,tcp->snd_wnd
        ,tcp->app_limited
        ,tcp->delivered
        ,tcp->bytes_acked
        ,(skb ? skb->truesize : -1)
        ,sk->sk_sndbuf
        ,sk->sk_wmem_queued
        ,sk_wmem_alloc_get(sk)
        ,(sk->sk_prot ? atomic_long_read(sk->sk_prot->memory_allocated) : -1)
        ,sk->sk_pacing_rate, tcp->tsq_flags
//        ,htonl(tcph->seq)
//        ,htonl(tcph->ack_seq)
        );
 }

  return;

#if 0
#if SME_DEBUG_LVL <= LVL_DBG
  int ret = 0;
  struct iphdr *iph;
  struct tcphdr *tcph;
  unsigned int s_addr, s_port, d_addr, d_port;
  char dbg_string[64];

  memset(dbg_string, 0, 64);
  sprintf(dbg_string, "%s:%d", __func__, __LINE__);
  if (!extra_dbg_string)
    extra_dbg_string = dbg_string;

  if (!skb) {
    iprint2(LVL_DBG, "%s -- Empty skb", extra_dbg_string);
    return;
  }

  iph = ip_hdr(skb);
  if ((unsigned char *) iph == skb->head)
    return;

  ret = check_valid_protocol(iph->protocol);
  if (!ret)
    return;

  s_addr = iph->saddr;
  d_addr = iph->daddr;

  tcph = tcp_hdr(skb);
  if ((unsigned char *) tcph == skb->head)
    return;

  s_port = ntohs((unsigned short int) tcph->source);
  d_port = ntohs((unsigned short int) tcph->dest);
  ret = check_relevant_ports(s_port, d_port);
  if (!ret)
    return;

  __mod_print_sk_buff_int(skb, extra_dbg_string);
#endif
#endif
}

int
mod_intercept_select_queue(struct net_device *dev, struct sk_buff *skb,
    char *extra_dbg_string)
{
  int qidx = -1;
//#if SME_CONFIG != SME_DBG_LOG
#if RX_NCPUS > 1 && CONFIG_SELECT_TXQ
  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  struct udphdr *udph = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  int ret = 0;
  ipport_t key;
  uint32_t phash = 0;

  ret = parse_skb_headers(skb, NULL, NULL, &s_addr, &d_addr, &s_port, &d_port,
      NULL, NULL, NULL, NULL, NULL, (void **) &eth, (void **) &iph,
      (void **) &tcph, (void **) &udph);

  /*
   * unpaced traffic, qidx belongs to [1, max_ncpus-1]
   */
  if (ret == -EINVAL) {
    qidx = skb_tx_hash(dev, skb);
    if (qidx >= SME_PACER_CORE && qidx < (SME_PACER_CORE + N_HYPACE))
      qidx = (SME_PACER_CORE + N_HYPACE);
    return qidx;
  }

  key.src_ip = s_addr;
  key.src_port = s_port;
  key.dst_ip = d_addr;
  key.dst_port = d_port;
  phash = pcm_hash((void *) &key, sizeof(ipport_t), 0);
  qidx = phash % N_HYPACE;

#endif /* CONFIG_SELECT_TXQ */
//#endif /* SME_CONFIG */

#if SME_DEBUG_LVL <= LVL_DBG
  char dbg_buff[256];
  memset(dbg_buff, 0, 256);
  sprintf(dbg_buff, "NEW QIDX %d", qidx);
  __mod_print_sk_buff_int(skb, dbg_buff);
#endif
  return qidx;
}

void
mod_intercept_tx_sent_queue(struct netdev_queue *txq, unsigned int bytes)
{
  netdev_tx_sent_queue(txq, bytes);
}

void
mod_intercept_tx_completed_queue(struct netdev_queue *txq,
    unsigned int pkts, unsigned int bytes)
{
  netdev_tx_completed_queue(txq, pkts, bytes);
}

int
mod_intercept_sk_buff(struct sk_buff *skb, char *extra_dbg_string)
{
  int ret = -EBADF;
  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  uint32_t s_addr, d_addr;
  uint16_t s_port, d_port;
  uint8_t protocol;
  uint32_t tot_hdr_len = 0;
  char *user_data = NULL;
  uint32_t user_data_len = 0;
  unsigned int seqno, seqack;
  uint64_t now;

  partial_sk_buff_t pskb;

#if CONFIG_XEN_PACER
  sme_sk_buff_t sskb;
  sme_channel_map_t *scm = NULL;
  profile_conn_map_t *pcm = NULL;
  struct sock *sk = NULL;
  struct tcp_sock *tcp_sock = NULL;
#if CONFIG_XEN_PACER_DB
  profq_elem_t *pelem = NULL;
#endif
#if CONFIG_PROF_LOG
  log_elem_t *le = NULL;
#endif

#endif

#if SME_DEBUG_LVL <= LVL_ERR
  char dbg_string[256];
  memset(dbg_string, 0, 256);
  sprintf(dbg_string, "%s:%d", __func__, __LINE__);
  if (!extra_dbg_string)
    extra_dbg_string = dbg_string;

#endif

  ret = parse_skb_headers(skb, NULL, NULL, &s_addr, &d_addr, &s_port, &d_port,
      &protocol, &tot_hdr_len, NULL, &user_data_len, &user_data,
      (void **) &eth, (void **) &iph, (void **) &tcph, NULL);
  if (ret == -EINVAL)
    return -EBADF;

#if SME_CONFIG == SME_DBG_LOG
  __mod_print_sk_buff_int(skb, extra_dbg_string);
  return -EBADF;
#endif


  now = get_current_time(SCALE_NS);
  seqno = htonl(tcph->seq);
  seqack = htonl(tcph->ack_seq);

  if (protocol == IPPROTO_TCP) {
    init_partial_skb(&pskb, s_addr, d_addr, s_port, d_port, seqno, seqack,
        eth->h_source, ETH_HEADER_LEN, eth->h_dest, ETH_HEADER_LEN,
        tcph->fin, tcph->syn, tcph->rst, tcph->psh,
        tcph->ack, tcph->urg, tcph->ece, tcph->cwr, ntohs(iph->id),
        iph->protocol, now, skb->dev);
    pskb.data_len = user_data_len;
    set_pskb_cpuid(&pskb, (int8_t) smp_processor_id());
  } else {
    init_partial_skb(&pskb, s_addr, d_addr, s_port, d_port,
        0, 0, eth->h_source, ETH_HEADER_LEN, eth->h_dest, ETH_HEADER_LEN,
        0, 0, 0, 0, 0, 0, 0, 0, ntohs(iph->id), iph->protocol, now, skb->dev);
    pskb.data_len = user_data_len;
    set_pskb_cpuid(&pskb, (int8_t) smp_processor_id());
  }

#if CONFIG_XEN_PACER
  init_sme_skb(&sskb, SKB_FULL, skb, skb->dev, &pskb);
  ret = get_profile_conn_info_from_skb_header(sskb.pskb.saddr, sskb.pskb.daddr,
      sskb.pskb.source, sskb.pskb.dest, &scm_list[0], &scm, &pcm, 0, 1, 1, &sskb);
  if (ret < 0 && scm && !pcm) {
    init_new_profile_conn_info(sskb.pskb.saddr, sskb.pskb.daddr,
        sskb.pskb.source, sskb.pskb.dest, sskb.dev,
        sskb.pskb.s_mac, sskb.pskb.d_mac, smp_processor_id(), scm, &pcm, 0);
    update_pcm_state_send(pcm, &sskb);
    printk(KERN_ERR "(%d:%.16s,core:%d) %s:%d BAD PCM!!! [%pI4 %u, %pI4 %u] "
        "sk state %d CA %d\n"
        "SKB ip ID %u [%d %d %d %d %d %d %d %d] (%u:%u) len %d padding %d "
        "snd_nxt %u snd_una %u pending %d\nscm %p %p pcm %p state %d\n"
        , task_pid_nr(current), current->comm, smp_processor_id()
        , __func__, __LINE__, (void *) &sskb.pskb.saddr, sskb.pskb.source
        , (void *) &sskb.pskb.daddr, sskb.pskb.dest
        , (skb->sk ? skb->sk->sk_state : -1)
        , (skb->sk ? inet_csk(skb->sk)->icsk_ca_state : 0)
        , sskb.pskb.ip_id
        , sskb.pskb.fin, sskb.pskb.syn, sskb.pskb.rst, sskb.pskb.psh
        , sskb.pskb.ack, sskb.pskb.urg, sskb.pskb.ece, sskb.pskb.cwr
        , sskb.pskb.seqno, sskb.pskb.seqack, skb->len
        , TCP_SKB_CB(skb)->tx_padding
        , (skb->sk ? tcp_sk(skb->sk)->snd_nxt : 0)
        , (skb->sk ? tcp_sk(skb->sk)->snd_una : 0)
        , (skb->sk ? inet_csk(skb->sk)->icsk_pending : 0)
        , scm, (pcm ? pcm->s : NULL), pcm, (pcm ? pcm->state : -1)
        );
//    BUG();
  }

  sk = skb->sk;
#if CONFIG_XEN_PACER_DB
  pelem = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
#endif
  if (sk) {
    tcp_sock = tcp_sk(sk);

    // tcp sock vars start uninitialized and have garbage values.
    // do not assign from tcp sock vars until they are initialized.
    if (pcm->stage > PCM_STAGE_SSL)
      pcm->tcp_segs_out = tcp_sock->segs_out;
    pcm->tcp_rcv_wnd = tcp_sock->rcv_wnd;
    pcm->tcp_rcv_wscale = tcp_sock->rx_opt.rcv_wscale;
    if (!tcph->syn) {
      pcm->packets_out_p = &tcp_sock->packets_out;
      pcm->retrans_out_p = &tcp_sock->retrans_out;
      pcm->sacked_out_p = &tcp_sock->sacked_out;
      pcm->lost_out_p = &tcp_sock->lost_out;
      pcm->tcp_sock = sk;
#if CONFIG_DUMMY == DUMMY_OOB
      pcm->shared_seq_p = &tcp_sock->shared_seq;
      pcm->shared_dummy_out_p = &tcp_sock->shared_dummy_out;
      pcm->shared_dummy_lsndtime_p = &tcp_sock->shared_dummy_lsndtime;
#if CONFIG_XEN_PACER_DB
      // shared pointers in pelem should be set only in the hypervisor
      pelem->shared_seq_maddr = (virt_to_machine(pcm->shared_seq_p)).maddr;
      pelem->shared_dummy_out_maddr =
        (virt_to_machine(pcm->shared_dummy_out_p)).maddr;
      pelem->shared_dummy_lsndtime_maddr =
        (virt_to_machine(pcm->shared_dummy_lsndtime_p)).maddr;
#endif /* CONFIG_XEN_PACER_DB */
#endif /* CONFIG_DUMMY == DUMMY_OOB */
    }
  }

#if CONFIG_XEN_PACER_DB
  pelem->last_skb_snd_window = ntohs(tcph->window);
#endif

  if (tcph && tcph->syn) {
    if (pcm->sec_conn_id.out_port == 443) {
      memcpy(&pcm->default_pid, &DEFAULT_HTTPS_PROF_IHASH, sizeof(ihash_t));
      pcm->stage = PCM_STAGE_SSL;
    } else if (pcm->sec_conn_id.out_port == 80) {
      // technically the web server should send ioctl here as well, but we
      // currently do not have an interception for http traffic.
      memcpy(&pcm->default_pid, &DEFAULT_MW_PROF_IHASH, sizeof(ihash_t));
      pcm->stage = PCM_STAGE_SSL_END;
    }
  }

  // caller is already holding locks
#if CONFIG_XEN_PACER_SLOTSHARE || !CONFIG_XEN_PACER_DB
  /*
   * profiling (or more generally whenever hypace is disabled),
   * or enforcement with slot sharing config
   */
  ret = sme_bnx2x_send_dma(sskb.skb, sskb.skb->dev, pcm);
#else
  /*
   * enforcement without slot sharing
   */
  ret = pacer_bnx2x_prep_dma(sskb.skb, sskb.skb->dev, pcm);
#endif

#if CONFIG_PROF_LOG

#if CONFIG_PROF_MINI_LOG
  /*
   * Log the first response packet only
   * Log the Request ack if there is one
   * Keep track of subsequent packets count
   */
  if (pskb.data_len == 0 && !pcm->logged_first_rsp_pkt) {
    alloc_init_log_elem(&le, &sskb.pskb, NULL, 0);
    add_log_elem(&pcm->prof_log, le);
  } else {
    if (!pcm->logged_first_rsp_pkt && pcm->count_pkts_out == 0
        && seqno >= pcm->last_req_pkt.seqack) {
      pcm->count_pkts_out = 1;
      pcm->logged_first_rsp_pkt = 1;
      alloc_init_log_elem(&le, &sskb.pskb, NULL, 0);
      le->count = pcm->count_pkts_out;
      add_log_elem(&pcm->prof_log, le);
    } else {
      pcm->count_pkts_out += 1;
      if (sskb.pskb.fin || sskb.pskb.rst) {
        copy_pskb(&(pcm->last_rsp_pkt), &(pcm->rec_rsp_pkt), 1);
        alloc_init_log_elem(&le, &(pcm->last_rsp_pkt), NULL, 0);
        le->count = pcm->count_pkts_out;
        add_log_elem(&pcm->prof_log, le);
        pcm->count_pkts_out = 0;
        //pcm->logged_first_rsp_pkt = 0;
      }
    }
  }

  copy_pskb(&(pcm->rec_rsp_pkt), &(sskb.pskb), 1);
#else
  alloc_init_log_elem(&le, &sskb.pskb, NULL, 0);
  add_log_elem(&pcm->prof_log, le);
#endif /* CONFIG_PROF_MINI_LOG */
#endif /* CONFIG_PROF_LOG */

  if (atomic_dec_and_test(&pcm->refcount) && pcm->written_proflog) {
#if CONFIG_PROF_LOG && CONFIG_PROF_MINI_LOG
      //pcm->count_pkts_out += 1;
      iprint(LVL_DBG, "TX Enqueue pcm [%pI4 %u, %pI4 %u]\n"
          "state %d stage %d #logs %d logged %d refcnt %d"
          , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
          , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
          , pcm->state, pcm->stage, pcm->count_pkts_out
          , pcm->written_proflog, atomic_read(&pcm->refcount)
          );
      copy_pskb(&(pcm->last_rsp_pkt), &(pcm->rec_rsp_pkt), 1);
      //alloc_init_log_elem(&le, &(pcm->rec_rsp_pkt), NULL, 0);
      alloc_init_log_elem(&le, &(pcm->last_rsp_pkt), NULL, 0);
      le->count = pcm->count_pkts_out;
      add_log_elem(&pcm->prof_log, le);
      pcm->count_pkts_out = 0;
      pcm->logged_first_rsp_pkt = 0;
#endif

    iprint(LVL_DBG, "TX Enqueue [%pI4 %u, %pI4 %u] G%llu state %d stage %d\n"
        , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
        , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
        , pcm->profq_idx, pcm->state, pcm->stage
        );
    enqueue_pcm_q(&pcm_txq[smp_processor_id()], (void *) pcm);
  }

  return 0;
#endif /* CONFIG_XEN_PACER */

  ret = -EBADF;

  return ret;
}

void
mod_intercept_xmit_stop_queue(struct net_device *dev, struct netdev_queue *txq,
    struct bnx2x_fp_txdata *txdata)
{
  struct bnx2x *bp = netdev_priv(dev);

  if (unlikely(bnx2x_tx_avail(bp, txdata) < MAX_DESC_PER_TX_PKT)) {
    netif_tx_stop_queue(txq);
    smp_mb();
    bnx2x_fp_qstats(bp, txdata->parent_fp)->driver_xoff++;
    if (bnx2x_tx_avail(bp, txdata) >= MAX_DESC_PER_TX_PKT)
      netif_tx_wake_queue(txq);
  }
  return;
}

int
mod_intercept_tx_int(struct net_device *dev, struct bnx2x_fp_txdata *txdata)
{
//  int ret = 0;
  struct bnx2x *bp = netdev_priv(dev);
//  if (txdata->txq_index == 1)
//    return -1;

#if !CONFIG_XEN_PACER || SME_CONFIG == SME_DBG_LOG
  return -1;
#endif

#if CONFIG_XEN_PACER_SLOTSHARE || !CONFIG_XEN_PACER_DB
  if (txdata->txq_index >= SME_PACER_CORE
      && txdata->txq_index < (SME_PACER_CORE + N_HYPACE)) {
    return sme_bnx2x_tx_int(bp, txdata);
  }
  return -1;
#else
  if (txdata->txq_index >= SME_PACER_CORE
      && txdata->txq_index < (SME_PACER_CORE + N_HYPACE)) {
    return pacer_merged_nicq_bnx2x_tx_int(bp, txdata);
  }
  return -1;
#endif

//  iprint(LVL_DBG, "CALLED sme_bnx2x_tx_int bp %p ret %d", bp, ret);
}

void
mod_intercept_free_tx_skbs_queue(struct bnx2x_fastpath *fp)
{
  sme_bnx2x_free_tx_skbs_queue(fp);
}

void
mod_parse_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod, uint16_t rx_bd_cons,
    uint16_t rx_comp_prod, uint16_t rx_comp_cons, char *data, int data_len,
    char *extra_dbg_string)
{
  int ret;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  uint32_t seqno = 0, seqack = 0;
  unsigned char smac[ETH_HEADER_LEN], dmac[ETH_HEADER_LEN];
//  unsigned char print_s_mac[ETH_PRINT_HDR_LEN], print_d_mac[ETH_PRINT_HDR_LEN];
  uint8_t protocol = -1;
  int tot_hdr_len = 0, user_data_len = 0;
  int real_data_len = 0;
  char *user_data = NULL;

//  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  struct udphdr *udph = NULL;
  uint32_t mac_hdr_off = 0, network_hdr_off = 0, transport_hdr_off = 0;
  uint32_t transport_hdr_len = 0;
  uint32_t ip_id = 0;

  struct net_device *net_dev = (struct net_device *) dev;
  struct bnx2x *bp = netdev_priv(net_dev);
  struct bnx2x_fastpath *fp = &bp->fp[fp_idx];
  uint16_t cqe_idx = RCQ_BD(rx_comp_cons);
  union eth_rx_cqe *cqe = &fp->rx_comp_ring[cqe_idx];
  struct eth_fast_path_rx_cqe *cqe_fp = &cqe->fast_path_cqe;
  int user_data_off = cqe_fp->placement_offset + NET_SKB_PAD;
  int qid = flow_fpidx_to_tsq_idx(fp->index);
  int cqe_fp_flags = cqe_fp->type_error_flags;
  int cqe_fp_type = cqe_fp_flags & ETH_FAST_PATH_RX_CQE_TYPE;
  struct bnx2x_fp_txdata *txdata = fp->txdata_ptr[0];
  int slow = CQE_TYPE_SLOW(cqe_fp_type);
  int fast = CQE_TYPE_FAST(cqe_fp_type);
  int start = CQE_TYPE_START(cqe_fp_type);
  int stop = CQE_TYPE_STOP(cqe_fp_type);
  char dbg_buff[DBG_BUF_SIZE];

  sme_q_t *irq_q = NULL;
  sme_sk_buff_t sskb3;

#if CONFIG_XEN_PACER
  ihash_t *profq_pid = NULL;
  sme_channel_map_t *scm = NULL;
  profile_conn_map_t *pcm = NULL;
  int alloc_marker = 0;
#if CONFIG_XEN_PACER_DB
  uint64_t inc_ts = 0;
  uint64_t end_prof_install_ts = 0;
#if CONFIG_PROF_FREE == PROF_FREE_TCP
  prof_update_arg_t parg;
#endif

#if SME_DEBUG_LVL <= LVL_INFO
  profq_elem_t *insert_prof_p = NULL;
#endif

  profq_elem_t *prev_prof_p = NULL;
  int insert_cwm = 0;
  struct tcp_sock *tp = NULL;
  int last_profq_idx, CWM = 0, NXT = 0, P = 0, T = 0, R = 0, D = 0, is_paused;
  int max_seqno = 0;
  int curr_head = 0;
  int p_ret = 0;
  int ins_idx = 0;
  profile_t *p = NULL;
  char pid_hash[64];
  int64_t num_default_frames = 0;
#endif
#if CONFIG_PROF_LOG
  char *marker_buf = NULL;
  int marker_buf_len = 0;
  int marker_ret = 0;
  log_elem_t *le = NULL;
#endif
#endif /* CONFIG_XEN_PACER */

  irq_q = &ts_irq_q[qid];
  ret = dequeue_sme_q(irq_q, &sskb3);

  if (ret < 0) {
    return;
  }

  memset(dbg_buff, 0, DBG_BUF_SIZE);

  txdata = txdata;

  /*
   * condition for matching slot between hw irq and sw irq
   */
  if (!(sskb3.fp_index == fp_idx &&
      sskb3.rx_bd_prod == rx_bd_prod &&
      sskb3.rx_bd_cons == rx_bd_cons &&
      sskb3.rx_comp_prod == rx_comp_prod &&
      sskb3.rx_comp_cons == rx_comp_cons)) { // does not match
    iprint(LVL_ERR, "unfound fpsb [%u %u %u] %d %d hc idx %d "
      "cqe flags %x [%d %d %d %d] dev %s QID %d\n"
      "bd_prod %d %d %d bd_cons %d %d %d "
      "comp_prod %d %d %d comp_cons %d %d %d"
      , fp->index, fp->fw_sb_id, fp->igu_sb_id, sskb3.fp_index, fp_idx
      , fp->fp_hc_idx, cqe_fp_flags, slow, fast, start, stop
      , bp->dev->name, fp->index
      , rx_bd_prod, fp->rx_bd_prod, sskb3.rx_bd_prod
      , rx_bd_cons, fp->rx_bd_cons, sskb3.rx_bd_cons
      , rx_comp_prod, fp->rx_comp_prod, sskb3.rx_comp_prod
      , rx_comp_cons, fp->rx_comp_cons, sskb3.rx_comp_cons
      );

    return;
  }

  if (!data || !data_len) {
    ret = -1;
  } else {
    ret = parse_raw_skb_headers(data+user_data_off, data_len, smac, dmac,
      &s_addr, &d_addr, &s_port, &d_port, &protocol,
      &tot_hdr_len, &user_data_len, &user_data,
      &mac_hdr_off, &network_hdr_off, &transport_hdr_off, &transport_hdr_len);

    if (!ret) {
//      eth = (struct ethhdr *) (data + user_data_off);
//      print_mac_addr(eth->h_source, print_s_mac, ETH_PRINT_HDR_LEN);
//      print_mac_addr(eth->h_dest, print_d_mac, ETH_PRINT_HDR_LEN);
      iph = (struct iphdr *) (data + user_data_off + network_hdr_off);
      ip_id = ntohs(iph->id);
      if (protocol == IPPROTO_TCP) {
        tcph = (struct tcphdr *) (data + user_data_off + transport_hdr_off);
        seqno = htonl(tcph->seq);
        seqack = htonl(tcph->ack_seq);
        real_data_len = (ntohs(iph->tot_len) - iph->ihl*4 - tcph->doff*4);
      } else if (protocol == IPPROTO_UDP) {
        udph = (struct udphdr *) (data + user_data_off + transport_hdr_off);
        real_data_len = ntohs(iph->tot_len) - sizeof(struct udphdr);
      }

#if SME_DEBUG_LVL <= LVL_INFO
      if (iph->protocol == IPPROTO_TCP) {
        sprintf(dbg_buff, "PROTO %d [%pI4 %u, %pI4 %u] "
          "seq (%u:%u) hlen %d DELTA %d olen %d\n"
          "ip ID %d, [%d %d %d %d %d %d %d %d] urg_ptr %d window %u %u"
//          ", check %d %d"
          , iph->protocol
          , (void *) &s_addr, s_port, (void *) &d_addr, d_port, seqno, seqack
          , tot_hdr_len, real_data_len, user_data_len
          , ntohs(iph->id)
          , tcph->fin, tcph->syn, tcph->rst, tcph->psh
          , tcph->ack, tcph->urg, tcph->ece, tcph->cwr
          , ntohs(tcph->urg_ptr), tcph->window, ntohs(tcph->window)
//          , tcph->check, le16_to_cpu(tcph->check)
          );
      } else if (iph->protocol == IPPROTO_UDP) {
        sprintf(dbg_buff,
          "src %pI4 %u dst %pI4 %u SMAC %s DMAC %s\n"
          "len %d udp hlen %d data hlen %d ip hlen %d ip tot len %d "
          "ip ID %d frag %d\n"
          "ip check %d udp check %d ret %d #####"
          , (void *) &s_addr, s_port, (void *) &d_addr, d_port, smac, dmac
          , data_len, transport_hdr_len, tot_hdr_len, iph->ihl, ntohs(iph->tot_len)
          , ntohs(iph->id), ntohs(iph->frag_off)
          , iph->check, udph->check, ret);
      }
#endif
    }
  }

#if (SME_CONFIG == SME_DBG_LOG)
  iprint(LVL_DBG, "DBG fpsb [%u %u %u] hc idx %d\n"
      "cqe flags %x [%d %d %d %d] off %d len %d fp mode %d "
      "rx_frag_size %d rx_buf_size %d size ssi %d\n"
//      "bd_prod %u (%u) %u %u bd_cons %u (%u) %u %u "
//      "comp_prod %u (%u) %u %u comp_cons %u (%u) %u %u\n"
//      "tx bd prod %u (%u) pkt prod %u (%u) "
//      "tx bd cons %u pkt cons %u\n"
      "%s"
      , fp->index, fp->fw_sb_id, fp->igu_sb_id, fp->fp_hc_idx
      , cqe_fp_flags, slow, fast, start, stop, user_data_off, data_len, fp->mode
      , fp->rx_frag_size, fp->rx_buf_size, (int) sizeof(struct skb_shared_info)
//      , rx_bd_prod, (int) RX_BD(rx_bd_prod), fp->swi_bd_prod, fp->rx_bd_prod
//      , rx_bd_cons, (int) RX_BD(rx_bd_cons), fp->swi_bd_cons, fp->rx_bd_cons
//      , rx_comp_prod, (int) RCQ_BD(rx_comp_prod)
//      , fp->swi_comp_prod, fp->rx_comp_prod
//      , rx_comp_cons, (int) RCQ_BD(rx_comp_cons)
//      , fp->swi_comp_cons, fp->rx_comp_cons
//      , txdata->tx_bd_prod, (int) TX_BD(txdata->tx_bd_prod)
//      , txdata->tx_pkt_prod, (int) TX_BD(txdata->tx_pkt_prod)
//      , txdata->tx_bd_cons, txdata->tx_pkt_cons
      , dbg_buff);

  return;
#endif

  // slots match, but not a valid packet
  if (ret < 0 || !is_destination_ip_me(d_addr)) {
    iprint(LVL_DBG, "reset irq idx %d fpsb [%u %u %u] "
      "cqe flags %x [%d %d %d %d]\n"
      "bd_prod %u (%u) %u bd_cons %u (%u) %u "
      "comp_prod %u (%u) %u comp_cons %u (%u) %u\n"
      ", sskb type %d"
      , irq_q->cons_idx, fp_idx, fp->fw_sb_id, fp->igu_sb_id
      , cqe_fp_flags, slow, fast, start, stop
      , rx_bd_prod, (int) RX_BD(rx_bd_prod), fp->rx_bd_prod
      , rx_bd_cons, (int) RX_BD(rx_bd_cons), fp->rx_bd_cons
      , rx_comp_prod, (int) RCQ_BD(rx_comp_prod), fp->rx_comp_prod
      , rx_comp_cons, (int) RCQ_BD(rx_comp_cons), fp->rx_comp_cons
      , sskb3.type);

    return;
  }

  // slots match, and packet of a relevant flow
  if (tcph)
    set_sskb_flow_info(&sskb3, s_addr, d_addr, s_port, d_port,
      seqno, seqack, tcph->fin, tcph->syn, tcph->rst, tcph->psh,
      tcph->ack, tcph->urg, tcph->ece, tcph->cwr, ntohs(iph->id),
      smac, ETH_HEADER_LEN, dmac, ETH_HEADER_LEN, protocol, net_dev);
  else
    set_sskb_flow_info(&sskb3, s_addr, d_addr, s_port, d_port,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ntohs(iph->id),
        smac, ETH_HEADER_LEN, dmac, ETH_HEADER_LEN, protocol, net_dev);
  sskb3.pskb.data_len = real_data_len;
  set_pskb_cpuid(&(sskb3.pskb), (int8_t) smp_processor_id());

#if CONFIG_XEN_PACER
  // init local pcm datastructure for the conn
  ret = get_profile_conn_info_from_skb_header(sskb3.pskb.daddr, sskb3.pskb.saddr,
      sskb3.pskb.dest, sskb3.pskb.source, &scm_list[0], &scm, &pcm, qid, 1, 0, &sskb3);
  if (ret < 0 && scm && !pcm && tcph  && !tcph->rst) {
    init_new_profile_conn_info(sskb3.pskb.daddr, sskb3.pskb.saddr,
        sskb3.pskb.dest, sskb3.pskb.source, sskb3.dev,
        sskb3.pskb.d_mac, sskb3.pskb.s_mac, smp_processor_id(), scm, &pcm, qid);
    update_pcm_state_recv(pcm, &sskb3);
    SME_ADD_COUNT_POINT_ARR(phash_dist, pcm->hypace_idx, smp_processor_id());
  } else if (ret < 0 && scm && !pcm && tcph && tcph->rst) {
    return;
  }

  // invalid
  if (!pcm) {
    return;
  }

  if (tcph && tcph->syn && !tcph->ack) {
    memcpy(&pcm->default_pid, &DEFAULT_HTTP_PROF_IHASH, sizeof(ihash_t));
    pcm->stage = PCM_STAGE_SYN;
    profq_pid = &pcm->default_pid;
    alloc_marker = 1;
  } else if (tcph && tcph->fin && !tcph->syn && !tcph->rst) {
    memcpy(&pcm->default_pid, &DEFAULT_SSLFIN_PROF_IHASH, sizeof(ihash_t));
    profq_pid = &pcm->default_pid;
  } else if (pcm->stage == PCM_STAGE_SSL && tcph && !tcph->rst && !tcph->syn
      && tcph->ack && tcph->psh) {
    if (!pcm->is_req_prof_set) {
      profq_pid = &pcm->default_pid;
      pcm->is_req_prof_set = 1;
      pcm->rem_initial_seq = seqack;
    }
  } else if (pcm->stage == PCM_STAGE_SSL_END && tcph && !tcph->rst && !tcph->syn
      && tcph->ack && tcph->psh) {
    profq_pid = &pcm->default_pid;
  }

  if (tcph) {
    // with end-to-end padding real_data_len should always be equal to MTU
    pcm->last_real_ack = htonl(ntohl(tcph->seq) + real_data_len);
  }

#if CONFIG_XEN_PACER_DB
  if (profq_pid) {
    p = htable_lookup(&scm->pmap_htbl, (void *) profq_pid, sizeof(ihash_t));
    if (!p) {
      memset(pid_hash, 0, 64);
      prt_hash((char *) profq_pid->byte, sizeof(ihash_t), pid_hash);
      iprint(LVL_EXP, "Could not find profile %s", pid_hash);
    }
    num_default_frames = p ? get_total_frames(p) : 2;
    // insert default profiles only when a packet contains a TCP push flag
    // TODO: update in place, the last request timestamp in
    // one of the existing profile slots in the profile queue
    // if this is a multi-packet request. should be required
    // only with large POST requests, which we do not handle yet.

    /*
     * init max_seqno to the max_seqno in previous profile, since we know
     * previous profile atleast transmitted upto here.. helps to handle
     * the case where no slots of a profile are played out before a
     * request times out and is retransmitted by the client.
     */
    pcm->prev_profq_idx = pcm->profq_idx;
    if (pcm->stage >= PCM_STAGE_SSL_END) {
      do_read_sync(pcm->tcp_sock, NULL);
      tp = tcp_sk(pcm->tcp_sock);
      pcm->rem_allowance = (tp->snd_una - pcm->rem_initial_seq)/SME_TCP_MTU;
      insert_cwm = tp->snd_cwnd;
    } else {
      insert_cwm = pcm->tcp_cwnd;
    }

#if CONFIG_PROF_FREE == PROF_FREE_TCP
    if (pcm->prev_profq_idx > 0) {
      prev_prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->prev_profq_idx];
      CWM = prev_prof_p->cwnd_watermark;
      NXT = prev_prof_p->next_timer_idx;
      P = prev_prof_p->num_timers;
      T = prev_prof_p->tail;
      R = atomic_read(&prev_prof_p->real_counter);
      D = prev_prof_p->dummy_counter;
      parg.profq_idx = pcm->prev_profq_idx;
      parg.hypace_idx = pcm->hypace_idx;
      ret = HYPERVISOR_sme_pacer(PACER_OP_PROF_FREE, &parg);
    }
#endif

  inc_ts = sskb3.pskb.in_timestamp;

    p_ret = profq_insert(profq_hdr[pcm->hypace_idx], profq_fl2[pcm->hypace_idx], pcm, profq_pid,
      sskb3.pskb.in_timestamp, max_seqno, insert_cwm, &ins_idx);

    if (p_ret > 0) {
      pcm->profq_idx = (int64_t) ins_idx;
#if SME_DEBUG_LVL <= LVL_INFO
      insert_prof_p = profq_hdr[pcm->hypace_idx]->queue_p[ins_idx];
      memset(pid_hash, 0, 64);
      prt_hash((char *) insert_prof_p->id_ihash.byte, sizeof(ihash_t), pid_hash);
      curr_head = atomic_read(&profq_hdr[pcm->hypace_idx]->queue_p[0]->next);
      iprint(LVL_INFO, "INSPROF (%d:%d) #slots %lld ret %d "
          "maddr %0llx state %d stage %d\n%s "
          "cwnd %d retrans %d lost %d sack_out %d pkts %d\n"
          "%s TS %llu reqts %llu CWM %d NXT %d P %d T %d R %d D %d last ack %u"
          , curr_head, ins_idx, num_default_frames, p_ret
          , (virt_to_machine(insert_prof_p)).maddr
          , pcm->state, pcm->stage, dbg_buff, pcm->tcp_cwnd
          , (pcm->retrans_out_p ? *(pcm->retrans_out_p) : -1)
          , (pcm->lost_out_p ? *(pcm->lost_out_p) : -1)
          , (pcm->sacked_out_p ? *(pcm->sacked_out_p) : -1)
          , (pcm->packets_out_p ? *(pcm->packets_out_p) : -1)
          , pid_hash, insert_prof_p->req_ts, pcm->rec_req_pkt.in_timestamp
          , insert_prof_p->cwnd_watermark, insert_prof_p->next_timer_idx
          , insert_prof_p->num_timers, insert_prof_p->tail
          , atomic_read(&insert_prof_p->real_counter)
          , insert_prof_p->dummy_counter, pcm->last_real_ack
          );
#endif
    }
    curr_head = atomic_read(&profq_hdr[pcm->hypace_idx]->queue_p[0]->next);

#if HYPACE_CFG < HP_BATCH
    if (p_ret == 1) {
      ret = HYPERVISOR_sme_pacer(PACER_OP_WAKEUP, NULL);
#if SME_DEBUG_LVL <= LVL_INFO
      iprint(LVL_INFO, "HYPERCALL (%d:%d) #slots %lld ret %d "
          "maddr %0llx state %d stage %d\n%s "
          "cwnd %d retrans %d lost %d sack_out %d pkts %d\n"
          "%s TS %llu CWM %d NXT %d P %d T %d R %d D %d"
          , curr_head, ins_idx, num_default_frames, ret
          , (virt_to_machine(insert_prof_p)).maddr
          , pcm->state, pcm->stage, dbg_buff, pcm->tcp_cwnd
          , (pcm->retrans_out_p ? *(pcm->retrans_out_p) : -1)
          , (pcm->lost_out_p ? *(pcm->lost_out_p) : -1)
          , (pcm->sacked_out_p ? *(pcm->sacked_out_p) : -1)
          , (pcm->packets_out_p ? *(pcm->packets_out_p) : -1)
          , pid_hash, insert_prof_p->req_ts
          , insert_prof_p->cwnd_watermark, insert_prof_p->next_timer_idx
          , insert_prof_p->num_timers, insert_prof_p->tail
          , atomic_read(&insert_prof_p->real_counter)
          , insert_prof_p->dummy_counter
          );
    } else if (p_ret < 0) {
        iprint(LVL_INFO, "INSERT ERR (%d:%d) #slots %lld ret %d "
          "fp %d state %d stage %d\n%s\n%s TS %llu"
          , curr_head, ins_idx, num_default_frames, p_ret
          , fp->index, pcm->state, pcm->stage, dbg_buff, pid_hash
          , get_current_time(SCALE_RDTSC)
          );
    } else {
      iprint(LVL_INFO, "NO HYPERCALL (%d:%d) #slots %lld ret %d "
          "fp %d state %d stage %d\n%s "
          "cwnd %d retrans %d lost %d sack_out %d pkts %d\n"
          "%s TS %llu CWM %d NXT %d P %d T %d R %d D %d"
          , curr_head, ins_idx, num_default_frames, p_ret
          , fp->index, pcm->state, pcm->stage, dbg_buff, pcm->tcp_cwnd
          , (pcm->retrans_out_p ? *(pcm->retrans_out_p) : -1)
          , (pcm->lost_out_p ? *(pcm->lost_out_p) : -1)
          , (pcm->sacked_out_p ? *(pcm->sacked_out_p) : -1)
          , (pcm->packets_out_p ? *(pcm->packets_out_p) : -1)
          , pid_hash, insert_prof_p->req_ts
          , insert_prof_p->cwnd_watermark, insert_prof_p->next_timer_idx
          , insert_prof_p->num_timers, insert_prof_p->tail
          , atomic_read(&insert_prof_p->real_counter)
          , insert_prof_p->dummy_counter
          );
#endif
    }
#else
    SME_ADD_COUNT_POINT_ARR(prev_pkt_cnt, NXT, smp_processor_id());
    if (P - NXT != 0) {
      SME_ADD_COUNT_POINT_ARR(prev_unused_cnt, P - NXT, smp_processor_id());
    }
    iprint(LVL_DBG, "prev prof W %d N %d P %d T %d R %d D %d\n%s"
        , CWM, NXT, P, T, R, D, dbg_buff);
#endif /* HYPACE_CFG < HP_BATCH */

    if (p_ret == 1) {
      end_prof_install_ts = get_current_time(SCALE_RDTSC);
      SME_ADD_COUNT_POINT_ARR(defprof_install,
          ((end_prof_install_ts - inc_ts) * RDTSC_MUL_FACTOR) / RDTSC_DIV_FACTOR,
          smp_processor_id());
    }
  } else {
    prev_prof_p = profq_hdr[pcm->hypace_idx]->queue_p[pcm->profq_idx];
    prev_prof_p->ack_ts = sskb3.pskb.in_timestamp;
    if (tcph && tcph->rst) {
    iprint(LVL_DBG, "ACK pcm stage %d\n"
        "[%pI4 %u, %pI4 %u] G%llu state %d paused %d CWM %d NXT %d P %d T %d"
        " R %d D %d\n%s"
        , pcm->stage
        , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
        , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
        , pcm->profq_idx
        , prev_prof_p->state, atomic_read(&prev_prof_p->is_paused)
        , prev_prof_p->cwnd_watermark, prev_prof_p->next_timer_idx
        , prev_prof_p->num_timers, prev_prof_p->tail
        , atomic_read(&prev_prof_p->real_counter), prev_prof_p->dummy_counter
        , dbg_buff);
    }
  }

  last_profq_idx = pcm->profq_idx;
  prev_prof_p = profq_hdr[pcm->hypace_idx]->queue_p[last_profq_idx];
  CWM = prev_prof_p->cwnd_watermark;
  NXT = prev_prof_p->next_timer_idx;
  P = prev_prof_p->num_timers;
  T = prev_prof_p->tail;
  R = atomic_read(&prev_prof_p->real_counter);
  D = prev_prof_p->dummy_counter;
  is_paused = atomic_read(&prev_prof_p->is_paused);
#endif /* CONFIG_XEN_PACER_DB */

  // XXX: we no longer support two-tier architectures, so only need
  // to insert one profile in only one profile queue.

  add_timestamp_to_profile_conn(&sskb3, pcm, 0);

#if CONFIG_PROF_LOG
  if (alloc_marker) {
    marker_ret = create_syn_marker_buf(&marker_buf, &marker_buf_len, profq_pid);
    alloc_init_log_elem(&le, &sskb3.pskb, marker_buf, marker_buf_len);
    add_log_elem(&pcm->prof_log, le);
  }

#if CONFIG_PROF_MINI_LOG
  /*
   * Identifying a new request on the same connection
   */
  iprint(LVL_DBG, "pcm [%pI4 %u, %pI4 %u]\n"
      "state %d stage %d #logs %d logged %d refcnt %d alloc_marker %d"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->state, pcm->stage, pcm->count_pkts_out
      , pcm->written_proflog, atomic_read(&pcm->refcount)
      , alloc_marker
      );
  if (pcm->stage == PCM_STAGE_SSL_END && !alloc_marker && tcph
      && ((real_data_len > 0 && !tcph->syn && tcph->ack && tcph->psh)
          || (tcph->fin || tcph->rst))
      && pcm->count_pkts_out > 0) { //&& !tcph->rst
    copy_pskb(&(pcm->last_rsp_pkt), &(pcm->rec_rsp_pkt), 1);
    alloc_init_log_elem(&le, &(pcm->last_rsp_pkt), NULL, 0);
    le->count = pcm->count_pkts_out;
    add_log_elem(&pcm->prof_log, le);
    pcm->count_pkts_out = 0;
    pcm->logged_first_rsp_pkt = 0;
  }

#endif /* CONFIG_PROF_MINI_LOG */
#endif /* CONFIG_PROF_LOG */

  if (atomic_dec_and_test(&pcm->refcount) && pcm->written_proflog) {
#if CONFIG_PROF_LOG && CONFIG_PROF_MINI_LOG
    copy_pskb(&(pcm->last_rsp_pkt), &(pcm->rec_rsp_pkt), 1);
    alloc_init_log_elem(&le, &(pcm->last_rsp_pkt), NULL, 0);
    le->count = pcm->count_pkts_out;
    add_log_elem(&pcm->prof_log, le);
    pcm->count_pkts_out = 0;
    pcm->logged_first_rsp_pkt = 0;
#endif

#if CONFIG_XEN_PACER_DB
    parg.profq_idx = pcm->profq_idx;
    parg.hypace_idx = pcm->hypace_idx;
    ret = HYPERVISOR_sme_pacer(PACER_OP_PROF_FREE, &parg);
    iprint(LVL_DBG, "RX Enqueue\n"
        "[%pI4 %u, %pI4 %u] G%u state %d stage %d paused %d CWM %d NXT %d "
        "P %d T %d R %d D %d\n%s"
        , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
        , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
        , last_profq_idx, pcm->state, pcm->stage, is_paused, CWM, NXT
        , P, T, R, D, dbg_buff
        );
#endif
    enqueue_pcm_q(&pcm_rxq[smp_processor_id()], (void *) pcm);
  }
#endif /* CONFIG_XEN_PACER */

  if (ret < 0)
    return;
}

void
mod_intercept_rx_path(void *dev, int fp_idx, uint16_t rx_bd_prod,
    uint16_t rx_bd_cons, uint16_t rx_comp_prod, uint16_t rx_comp_cons, int irq,
    char *extra_dbg_string)
{
  struct net_device *net_dev = (struct net_device *) dev;
  struct bnx2x *bp = netdev_priv(net_dev);
  struct bnx2x_fastpath *fp = &bp->fp[fp_idx];
  union eth_rx_cqe *cqe = NULL;
  struct eth_fast_path_rx_cqe *cqe_fp = NULL;
  u8 cqe_fp_flags;
  enum eth_rx_cqe_type cqe_fp_type;
  int slow, fast, start, stop;

  int cqe_idx = RCQ_BD(rx_comp_cons);
  uint16_t tmp_bd_prod, tmp_bd_cons;
  uint16_t tmp_comp_prod, tmp_comp_cons;

//#if SME_CONFIG != SME_DBG_LOG
  sme_sk_buff_t sskb;
  uint64_t now = get_current_time(SCALE_RDTSC);

  int qid = flow_fpidx_to_tsq_idx(fp->index);
//#endif

#if SME_DEBUG_LVL <= LVL_ERR
  int num_hw_pkts = 0;
#endif

#if SME_DEBUG_LVL <= LVL_DBG
  char dbg_string[64];
  if (!extra_dbg_string) {
    memset(dbg_string, 0, 64);
    sprintf(dbg_string, "%s:%d", __func__, __LINE__);
    extra_dbg_string = dbg_string;
  }
#endif

  tmp_bd_prod = fp->rx_bd_prod;
  tmp_bd_cons = fp->rx_bd_cons;
  tmp_comp_prod = fp->rx_comp_prod;
  tmp_comp_cons = fp->rx_comp_cons;

  cqe_idx = RCQ_BD(tmp_comp_cons);
  cqe = &fp->rx_comp_ring[cqe_idx];
  cqe_fp = &cqe->fast_path_cqe;
  cqe_fp_flags = cqe_fp->type_error_flags;
  cqe_fp_type = cqe_fp_flags & ETH_FAST_PATH_RX_CQE_TYPE;
  slow = CQE_TYPE_SLOW(cqe_fp_type);
  fast = CQE_TYPE_FAST(cqe_fp_type);
  start = CQE_TYPE_START(cqe_fp_type);
  stop = CQE_TYPE_STOP(cqe_fp_type);

  while (BNX2X_IS_CQE_COMPLETED(cqe_fp)) {
    cqe_fp_flags = cqe_fp->type_error_flags;
    cqe_fp_type = cqe_fp_flags & ETH_FAST_PATH_RX_CQE_TYPE;
    slow = CQE_TYPE_SLOW(cqe_fp_type);
    fast = CQE_TYPE_FAST(cqe_fp_type);
    start = CQE_TYPE_START(cqe_fp_type);
    stop = CQE_TYPE_STOP(cqe_fp_type);

    if (!(slow || start || stop)) {
//#if SME_CONFIG != SME_DBG_LOG
    memset(&sskb, 0, sizeof(sme_sk_buff_t));
    now = get_current_time(SCALE_RDTSC);
    set_sskb_idx_ts(&sskb, net_dev, fp_idx, tmp_bd_prod, tmp_bd_cons,
        tmp_comp_prod, tmp_comp_cons, now);
    enqueue_sme_q(&ts_irq_q[qid], &sskb);
//#endif

    tmp_bd_prod = NEXT_RX_IDX(tmp_bd_prod);
    tmp_bd_cons = NEXT_RX_IDX(tmp_bd_cons);
    } else if (!slow && !fast && !start && stop) {
      tmp_bd_prod = NEXT_RX_IDX(tmp_bd_prod);
      tmp_bd_cons = NEXT_RX_IDX(tmp_bd_cons);
    }

    tmp_comp_prod = NEXT_RCQ_IDX(tmp_comp_prod);
    tmp_comp_cons = NEXT_RCQ_IDX(tmp_comp_cons);

    cqe_idx = RCQ_BD(tmp_comp_cons);
    cqe = &fp->rx_comp_ring[cqe_idx];
    cqe_fp = &cqe->fast_path_cqe;

#if SME_DEBUG_LVL <= LVL_ERR
    rx_bd_prod = RX_BD(tmp_bd_prod);
    rx_bd_cons = RX_BD(tmp_bd_cons);

    num_hw_pkts++;
    // sanity check: RX_BD(hwi*) != RX_BD(swi*)
    // this would ensure that hw handler does not wrap around
    // the ring buffer twice before the consumer moves
    if (rx_bd_prod == RX_BD(fp->swi_bd_prod)
        && rx_bd_cons == RX_BD(fp->swi_bd_cons)
        && RCQ_BD(tmp_comp_prod) == RCQ_BD(fp->swi_comp_prod)
        && RCQ_BD(tmp_comp_cons) == RCQ_BD(fp->swi_comp_cons)) {
      iprint2(LVL_INFO, "WRAP AROUND fpsb [%u %u %u] hc idx %d #hw pkts: %d\n"
          "bd_prod %u (%u) %u %u bd_cons %u (%u) %u %u "
          "comp_prod %u (%u) %u %u comp_cons %u (%u) %u %u\n"
          , fp->index, fp->fw_sb_id, fp->igu_sb_id, fp->fp_hc_idx, num_hw_pkts
          , tmp_bd_prod, rx_bd_prod, fp->swi_bd_prod, (int) RX_BD(fp->swi_bd_prod)
          , tmp_bd_cons, rx_bd_cons, fp->swi_bd_cons, (int) RX_BD(fp->swi_bd_cons)
          , tmp_comp_prod, (int) RCQ_BD(tmp_comp_prod)
          , fp->swi_comp_prod, (int) RCQ_BD(fp->swi_comp_prod)
          , tmp_comp_cons, (int) RCQ_BD(tmp_comp_cons)
          , fp->swi_comp_cons, (int) RCQ_BD(fp->swi_comp_cons)
          );
    }
#endif
  }

  fp->rx_bd_prod = tmp_bd_prod;
  fp->rx_bd_cons = tmp_bd_cons;
  fp->rx_comp_prod = tmp_comp_prod;
  fp->rx_comp_cons = tmp_comp_cons;

#if SME_DEBUG_LVL <= LVL_DBG
  if (num_hw_pkts > 0)
    iprint2(LVL_DBG, "%s:%d fpsb [%u %u %u] hc idx %d #hw pkts: %d "
        "cqe flags %x [%d %d %d %d] dev %s IRQ %d\n"
        "prod %d cons %d fp %u (%u) %u, %u (%u) %u , %u (%u) %u, %u (%u) %u"
      , __func__, __LINE__
      , fp->index, fp->fw_sb_id, fp->igu_sb_id, fp->fp_hc_idx, num_hw_pkts
      , cqe_fp_flags, slow, fast, start, stop
      , bp->dev->name, irq, ts_irq_q[qid].prod_idx, ts_irq_q[qid].cons_idx
      , fp->swi_bd_prod, (int) RX_BD(fp->swi_bd_prod), fp->rx_bd_prod
      , fp->swi_bd_cons, (int) RX_BD(fp->swi_bd_cons), fp->rx_bd_cons
      , fp->swi_comp_prod, (int) RCQ_BD(fp->swi_comp_prod), fp->rx_comp_prod
      , fp->swi_comp_cons, (int) RCQ_BD(fp->swi_comp_cons), fp->rx_comp_cons
      );
#endif
}

void
mod_print_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod, uint16_t rx_bd_cons,
    uint16_t rx_comp_prod, uint16_t rx_comp_cons, char *extra_dbg_string)
{
#if SME_DEBUG_LVL <= LVL_DBG
  char dbg_string[64];
  struct net_device *net_dev = (struct net_device *) dev;
  struct bnx2x *bp = netdev_priv(net_dev);
  struct bnx2x_fastpath *fp = &bp->fp[fp_idx];
  union eth_rx_cqe *cqe = &fp->rx_comp_ring[RCQ_BD(fp->rx_comp_cons)];
  struct eth_fast_path_rx_cqe *cqe_fp = &cqe->fast_path_cqe;
  int cqe_fp_flags = cqe_fp->type_error_flags;
  int cqe_fp_type = cqe_fp_flags & ETH_FAST_PATH_RX_CQE_TYPE;
  int slow, fast, start, stop;

  slow = CQE_TYPE_SLOW(cqe_fp_type);
  fast = CQE_TYPE_FAST(cqe_fp_type);
  start = CQE_TYPE_START(cqe_fp_type);
  stop = CQE_TYPE_STOP(cqe_fp_type);

  if (!extra_dbg_string) {
    memset(dbg_string, 0, 64);
    sprintf(dbg_string, "%s:%d", __func__, __LINE__);
    extra_dbg_string = dbg_string;
  }

	iprint2(LVL_DBG, "%s fpsb [%d %d %d] hc idx %d "
						 "cqe flags %x [%d %d %d %d] fp mode %d, tpa idx start %d end %d\n"
             "bd_prod %u (%u) %u bd_cons %u (%u) %u "
             "comp_prod %u (%u) %u comp_cons %u (%u) %u"
						 //"bd_prod: %u (%u) %u (%u) %u bd_cons: %u (%u) %u (%u) %u\n"
						 //"comp_prod: %u (%u) %u (%u) %u comp_cons: %u (%u) %u (%u) %u"
						 , extra_dbg_string
						 , fp->index, fp->fw_sb_id, fp->igu_sb_id, fp->fp_hc_idx
						 , cqe_fp_flags, slow, fast, start, stop, fp->mode
             , cqe_fp->queue_index, cqe->end_agg_cqe.queue_index
						 //, fp->hwi_bd_prod, (unsigned int) RX_BD(fp->hwi_bd_prod)
						 , fp->swi_bd_prod, (unsigned int) RX_BD(fp->swi_bd_prod), fp->rx_bd_prod
						 //, fp->hwi_bd_cons, (unsigned int) RX_BD(fp->hwi_bd_cons)
						 , fp->swi_bd_cons, (unsigned int) RX_BD(fp->swi_bd_cons), fp->rx_bd_cons
						 //, fp->hwi_comp_prod, (unsigned int) RCQ_BD(fp->hwi_comp_prod)
						 , fp->swi_comp_prod, (unsigned int) RCQ_BD(fp->swi_comp_prod), fp->rx_comp_prod
						 //, fp->hwi_comp_cons, (unsigned int) RCQ_BD(fp->hwi_comp_cons)
						 , fp->swi_comp_cons, (unsigned int) RCQ_BD(fp->swi_comp_cons), fp->rx_comp_cons
						 );
#endif
}
