## Client setup instructions
* copy `configs/nativeos.grub` to `/etc/default/grub` in dom0
* compile gpace/linux-4.9.5` and install from source.
  ```
  cd pacer/client/gpace/linux-client-4.9.5
  cp dom0.config.client.ptcp .config
  make menuconfig ## Simply exit
  ./build.sh
  ./install.sh
  ```
* configure the `KERNELDIR` path in the Makefile of `gpace/kmod` before compiling.
* disable hyperthreading and turbo boost in BIOS
* set power profile to "performance" in BIOS
* disable DVFS:
  * `update-rc.d ondemand disable`
  * edit /etc/default/cpufrequtils: GOVERNOR="ondemand" ==> "performance"

## At each reboot
* run `irqsetup.sh`: configures proper IRQ affinity for NIC Tx and Rx interrupts, depending on number of VCPUs configured for the VM. This script must be executed in a guest VM everytime upon first bootup.

## For each experiment
* run `ethdev_enable.sh`: can be used to enable various debug prints from the kerne network stack. Run this script at the start of any experiment, and be sure to run the counterpart `ethdev_disable.sh` after the experiment to ensure that the system is not overwhelmed with too many prints.
* run `insmod` to install kernel module and `rmmod` to uninstall it.
* look at `run_host_setup` in the scripts in `eval/video/experiment` directory for exact parameters to the `insmod` command on various nodes.
