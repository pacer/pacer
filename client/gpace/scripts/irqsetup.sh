#!/bin/bash

if [[ $# -lt 1 ]]; then
	echo "script sets irq affinity mask for ethernet nic, and disables TX,SG,GSO,TSO"
	echo "Usage: ./irqsetup.sh <affinity_config (0|1)>"
	echo "config 0 -- all irqs with smp_affinity 0"
	echo "config 1 -- all irqs with smp_affinity set to "
	echo "a separate single CPU starting from CPU0 in a round robin fashion"
	exit -1
fi

config=$1

PFID="168A"
VFID="16A9"

ncpu=`nproc`

# ==== check iface name ====

for ifstr in "eno1" "enp0s0" "eth0"; do
  ifline=`ifconfig | grep $ifstr`
  if [[ "x$ifline" != "x" ]]; then
    iface=`echo $ifline | cut -d' ' -f1`
    break
  fi
done

pciid=`cat /sys/class/net/$iface/device/uevent | grep PCI_ID`
pci_devid=`echo $pciid | cut -d':' -f2`

# ==== disable irqbalance only for NIC PF ====
if [[ "$pci_devid" == "$PFID" ]]; then
  echo "==== disabling irqbalance ===="
  echo "service irqbalance stop"
  service irqbalance stop
fi

edev=$iface

# ==== setting NIC config ===
echo ""
echo "==== setting dev config ===="
if [[ "$pci_devid" == "$VFID" ]]; then
  if [[ $ncpu == 1 ]]; then
    numqueues=$(( $ncpu + 2 ))
  else
    numqueues=$(( $ncpu + 0 ))
  fi
  larg="combined $numqueues"
  echo "ethtool -L $edev $larg"
  ethtool -L $edev $larg
fi

#etarg="sg off gso off tso off"
etarg="sg off gso off tso off gro off lro off tx off"
#etarg="tx off sg off gso off tso off"
#etarg="tx off sg off gso off tso off gro off"
echo "ethtool -K $edev $etarg"
ethtool -K $edev $etarg

carg="rx-usecs 0"
echo "ethtool -C $edev $carg"
ethtool -C $edev $carg

# ==== display NIC queue config ====
host=`hostname`
nirq=`cat /proc/interrupts | grep "$edev-fp" | wc -l`
echo "HOST: $host, DEV: $edev, DEVID $pci_devid, #IRQs $nirq, #CPUs $ncpu"

# ==== setting interrupt affinities ====
#startirq=`cat /proc/interrupts | grep "$edev-fp" | cut -d':' -f1 | head -n1 | tr -d '[:space:]'`
#endirq=$(( $startirq + $nirq - 1))

irqlist=`cat /proc/interrupts | grep "$edev-fp" | cut -d':' -f1 | sed -e 's/^[[:space:]]*//'`
irqarr=( $irqlist )

#echo "IRQ list: "${irqlist}

#let i=0
#while (( ${#irqarr[@]} > i )); do
#  echo "$i irq# ${irqarr[i++]}"
#done
#for irqid in "${irqarr[@]}"; do
#  echo "irq# $irqid"
#done

irqbit=1
shiftmask=0
irqmask=0
echo ""
#echo "==== setting affinity for IRQs [$startirq - $endirq] ===="
echo "==== setting affinity for IRQs ["${irqlist}"] ===="

if [[ $nirq -gt $ncpu ]] && [[ $ncpu == 1 ]]; then
  echo "Only one CPU, all IRQs mapped to cpu 0"
  exit
fi

#for i in `seq $startirq $endirq`; do
#	if [[ $config -eq 0 ]]; then
#		irqmask=1
#	else
#		irqmask=$(( $irqbit << $shiftmask ))
#	fi
#	irqhex=`printf '%x\n' $irqmask`
#	echo "IRQ $i, shift: $shiftmask, irqmask: $irqmask, irqhex: $irqhex"
#	echo $irqhex > /proc/irq/$i/smp_affinity
#  if [[ $config -eq 1 ]]; then
#    shiftmask=$(( $(( $shiftmask + 1 )) % $ncpu ))
#  fi
#done

#if [[ "$pci_devid" == "$PFID" ]]; then
#  enoirq=$(( $startirq - 2))
#  if [[ $config -eq 0 ]]; then
#    irqmask=1
#  else
#    irqmask=$(( $irqbit << $shiftmask ))
#    shiftmask=$(( $(( $shiftmask + 1 )) % $ncpu ))
#  fi
#  irqhex=`printf '%x\n' $irqmask`
#  echo "IRQ $enoirq, shift: $shiftmask, irqmask: $irqmask, irqhex: $irqhex"
#  echo $irqhex > /proc/irq/$enoirq/smp_affinity
#fi

#echo ""
#echo "==== verifying IRQ affinities ===="
#for i in `seq $startirq $endirq`; do
#	aff=`cat /proc/irq/$i/smp_affinity`
#	echo "IRQ $i -- aff $aff"
#done
#
#if [[ "$pci_devid" == "$PFID" ]]; then
#  aff=`cat /proc/irq/$enoirq/smp_affinity`
#  echo "irq $enoirq -- aff $aff"
#fi

irqit=0
for i in "${irqarr[@]}"; do
	if [[ $config -eq 0 ]]; then
		irqmask=1
	else
		irqmask=$(( $irqbit << $shiftmask ))
	fi
	irqhex=`printf '%x\n' $irqmask`
	echo "[$irqit] IRQ $i, shift: $shiftmask, irqmask: $irqmask, irqhex: $irqhex"
	echo $irqhex > /proc/irq/$i/smp_affinity
  if [[ $config -eq 1 ]]; then
    if [[ $irqit -eq $(( $ncpu - 1 )) ]]; then
      shiftmask=$(( $(( $shiftmask + 2 )) % $ncpu ))
    else
      shiftmask=$(( $(( $shiftmask + 1 )) % $ncpu ))
    fi
  fi
  irqit=$((irqit + 1 ))
done

echo ""
echo "==== verifying IRQ affinities ===="
for i in "${irqarr[@]}"; do
	aff=`cat /proc/irq/$i/smp_affinity`
	echo "IRQ $i -- aff $aff"
done

# ====

#num_txq=24
#starttxq=0
#endtxq=$(( $starttxq + $num_txq - 1 ))
#
#irqbit=1
#echo ""
#echo "==== setting affinity for TXQs [$starttxq - $endtxq] ===="
#
#for i in `seq $starttxq $endtxq`; do
#  shiftmask=$(( $i % $nirq ))
#  shiftmask=$(( $shiftmask * 2 ))
#  irqmask=$(( $irqbit << $shiftmask ))
#  irqhex=`printf '%x\n' $irqmask`
#  echo "TXQ $i, shift: $shiftmask, irqmask: $irqmask, irqhex: $irqhex"
#  echo $irqhex > /sys/class/net/$edev/queues/tx-$i/xps_cpus
#done
#
#echo ""
#echo "==== verifying TXQ affinities ===="
#for i in `seq $starttxq $endtxq`; do
#  aff=`cat /sys/class/net/$edev/queues/tx-$i/xps_cpus`
#  echo "TXQ $i -- aff $aff"
#done

# ====

#irqmask=256
#echo $irqhex
