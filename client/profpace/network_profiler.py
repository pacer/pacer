import os
import sys
import time
import subprocess
import argparse

sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

pids = []


def start_tcpdump(client_ip, server_ip, server_port, iface, wfile, multi_tier,
    tier2_ip, tier2_port, dom_name="", transport="tcp"):
    global pids
    cmd = ("tcpdump -B 1048576 --time-stamp-precision=nano -i %s -s 0 -nSSXU"   \
            " -A '(%s) and (src host %s and dst host %s and dst port %d)"   \
            " or (dst host %s and src host %s and src port %d)" %
            (iface, transport, client_ip, server_ip, server_port,
            client_ip, server_ip, server_port)
        )
    if multi_tier:
        cmd = ("%s or ((src host %s and dst host %s and dst port %d)"  \
                " or (dst host %s and src host %s and src port %d))" %
                (cmd, server_ip, tier2_ip, tier2_port, server_ip, tier2_ip, tier2_port)
            )
    cmd = ("%s' -w %s" % (cmd, wfile))
    cmd2 = (" >> %s.out 2>&1 &" % wfile)

#    #cmd += " and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"

#    print(cmd)
    subprocess_opened = subprocess.Popen(cmd+cmd2, shell=True)
    pids.append(subprocess_opened.pid)

#    print(subprocess_opened.pid, subprocess_opened.pid + 1)
    subprocess_opened.wait()

#    grepstr = "ps aux | grep \"%s\"" % cmd
    grepstr = "ps aux | grep \"tcpdump -B\" | awk '$11 == \"tcpdump\" {print $2}'"
    subprocess_grep = subprocess.Popen(grepstr, shell=True)

    return subprocess_opened.pid + 1


def stop_tcpdump(pid):
    # process created in bash,
    # so pid of tcpdump process is 1
    # greater than pid returned above
    kill_cmd = "kill -15 "
    kill_cmd += str(pid)
    print(kill_cmd)
    subprocess.call(kill_cmd, shell=True)


def gen_tcpdump(wfile, rfile, transport="tcp"):
    cmd = ""
    #cmd = "tcpdump -nvr " + wfile + " > " + rfile
    # print cmd
    if (transport == "tcp"):
        cmd = ("tshark -Tfields -e frame.number"  \
                " -e frame.time_relative -e frame.time_epoch"  \
                " -e tcp.seq -e tcp.ack -e ip.id" \
                " -e ip.src -e tcp.srcport -e ip.dst -e tcp.dstport" \
                " -e tcp.len -e ssl.record.content_type"    \
                " -e tcp.flags.fin -e tcp.flags.syn" \
                " -e tcp.flags.reset -e tcp.flags.push"  \
                " -e tcp.flags.ack -e tcp.flags.urg" \
                " -e tcp.flags.ecn -e tcp.flags.cwr" \
                " -e tcp.checksum -e tcp.urgent_pointer" \
                " -r %s > %s.tshark" % (wfile, rfile)
            )
#        "-o tcp.relative_sequence_numbers:FALSE " +   \
#                " -e tcp.flags"   \
#          "-E separator='/t' " +    \
#          "-e tcp.window_size_value -e tcp.window_size " +   \
    elif (transport == "udp"):
        cmd = ("tshark -Tfields -e frame.number"    \
                " -e frame.time_relative -e frame.time_epoch" \
                " -e ip.id -e ip.src -e ip.dst -e udp.dstport -e udp.length"  \
                " -r %s > %s.tshark" % (wfile, rfile)
            )
    print(cmd)
    subprocess.call(cmd, shell=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='TCPDump driver')
    parser.add_argument('--client-ip', dest='client_ip', action='store',
        required=True, help='ip of client driver')
    parser.add_argument('--server-ip', dest='server_ip', action='store',
        required=True, help='server ip')
    parser.add_argument('--server-port', dest='server_port', action='store',
        required=True, type=int, help='server port')
    parser.add_argument('--transport', dest='transport', action='store',
        default='tcp', help='transport layer protocol (TCP/UDP)')
    parser.add_argument('--iface', dest='iface', action='store',
        default='enp0s0', help='nic interface name on this host')
    parser.add_argument('--dumpfile', dest='wfile', action='store',
        required=True, help='raw tcpdump output file name')
    parser.add_argument('--outfile', dest='rfile', action='store',
        required=True, help='tshark output file name')

    parser.add_argument('-m', '--multi-tier', dest='is_multi_tier', type=int,
        default=0, help=('use if this host communicates with two endpoints.'  \
                        'e.g. frontend communicates with client and backend'))
    parser.add_argument('-t2ip', '--tier2-ip', dest='tier2_ip', action='store',
        help='ip of second tier if exists')
    parser.add_argument('-t2port', '--tier2-port', dest='tier2_port', type=int,
        default=11211, help='port of second tier if exists')
    parser.add_argument('-a', '--action', dest='start_or_pid', action='store',
        required=True, type=int, help='0: start tcpdump; pid of a tcpdump process: kill it')


    args = parser.parse_args()
    dom_name = ""
    client_ip = args.client_ip
    server_ip = args.server_ip
    server_port = args.server_port
    iface = args.iface
    wfile = args.wfile
    rfile = args.rfile
    is_multi_tier = int(args.is_multi_tier)
    tier2_ip = args.tier2_ip
    tier2_port = int(args.tier2_port)
    start_or_pid = int(args.start_or_pid)
    transport = args.transport

    if (start_or_pid == 0):
        start_tcpdump(client_ip, server_ip, server_port, iface, wfile, is_multi_tier,
            tier2_ip, tier2_port, dom_name, transport)
    elif (start_or_pid > 0):
        stop_tcpdump(start_or_pid)
    else:
        gen_tcpdump(wfile, rfile, transport)
