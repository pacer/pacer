/*
 * profile_conn_msg.h
 *
 * created on: Mar 21, 2017
 * author: aasthakm
 *
 * serialized buffer format for profile to be
 * sent from application to kernel to set on a socket
 */
#ifndef __PROFILE_CONN_MSG_H__
#define __PROFILE_CONN_MSG_H__

void
prepare_profile_buf_ihash(char *buf, uint64_t *buf_len, ihash_t prof_id,
    uint16_t num_out_reqs, uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *frame_spacing, uint64_t *first_frame_latency);

//void
//prepare_profile_buf_ihash(char *buf, uint16_t *buf_len, char* prof_id,
//    uint16_t num_out_reqs, uint16_t *num_extra_slots, uint64_t *num_out_frames,
//    uint64_t *frame_spacing, uint64_t *first_frame_latency);

void
prepare_profile_buf(char *buf, uint64_t *buf_len, uint16_t prof_id,
    uint16_t num_out_reqs, uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *frame_spacing, uint64_t *first_frame_latency);

void
prepare_profile_conn_buf(char *buf, uint64_t *buf_len,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    uint16_t prof_id, uint16_t num_out_reqs, uint16_t *num_extra_slots,
    uint64_t *num_out_frames, uint64_t *frame_spacing, uint64_t *first_frame_latency);

void
prepare_n_profile_map_buf_from_mem(char *buf, uint64_t *buf_len, uint16_t n_profile,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    char **profile, uint16_t *profile_len);

void
prepare_profile_id_conn_buf(char *buf, uint64_t *buf_len,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    uint16_t prof_id);

void
prepare_profile_marker_buf(char *buf, uint64_t *buf_len,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    uint32_t num_public, uint32_t num_private, uint16_t hash_len,
    ihash_t *pubhash, ihash_t *privhash);

void
write_profile_conn_to_file(char *fname, char *buf, int buf_len);

void
read_profile_conn_from_file(char *fname, char **buf, int *buf_len);

void
print_profile_local_conn_buf(char *buf, int buf_len);

void
print_profile_buf(char *buf, int buf_len);

void
print_profile_conn_buf(char *buf, int buf_len);

void
print_profile_map_buf(char *buf, int buf_len);

void
print_profile_map_buf_ihash(char *buf, int buf_len);

void
print_profile_id_conn_buf(char *buf, int buf_len);

void
print_profile_marker_buf(char *buf, int buf_len);

void
dbg_prt(char *buf, int buf_len);

void
prt_hash(char *hbuf, int hlen, char *out);

#endif /* __PROFILE_CONN_MSG_H__ */
