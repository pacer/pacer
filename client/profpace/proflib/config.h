/*
 * config.h
 *
 * created on: Sep 26, 2017
 * author: aasthakm
 *
 * config macros for userspace C code
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

#define DBG 0
#define CONFIG_PROF_MINI_LOG 1
#endif /* __CONFIG_H__ */
