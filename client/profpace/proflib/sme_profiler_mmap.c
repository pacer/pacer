/*
 * sme_profiler_mmap.c
 *
 * created on: Sep 25, 2017
 * author: aasthakm
 *
 * test driver program for consuming profiler logs
 * from ring buffer shared with the kernel
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include "profiler_mmap.h"

#define MAX_FNAME_LEN 512

#define CONFIG_FILE_WRITE 0
#define DBG 0

const char *sme_dev = "/dev/SMECTRL";

const char *smedir = "/local/sme/syslogs/proflog";

extern void dbg_prt(char *buf, int buf_len);

int main(int argc, char *argv[])
{
	char *ptr = NULL;
	int ret = 0;

  sme_log_q_info_t slq_info;
  memset(&slq_info, 0, sizeof(sme_log_q_info_t));
  ret = prepare_log_consumer((char *) sme_dev, &slq_info);
	if (ret < 0)
		return EXIT_FAILURE;

#if CONFIG_FILE_WRITE
  char fname[MAX_FNAME_LEN];
  memset(fname, 0, MAX_FNAME_LEN);
  sprintf(fname, "%s/kproflog-r11.txt", smedir);
  FILE *fp = fopen(fname, "w");
#endif

  do_consume(&slq_info);

#if 0
  uint64_t prod_idx, cons_idx, new_cons_idx, new_cons_idx_modulo;
  char *log_buf = NULL;
  uint64_t log_size = 0;
  int qmask = *(slq_info.max_qbuf) - 1;

  prod_idx = *(slq_info.prod_idx);
  cons_idx = slq_info.cons_idx;
  while (cons_idx < prod_idx) {
  //  dbg_prt(log_buf, log_size);
    log_buf = slq_info.ring_addr + (cons_idx * MAX_QBUF_SIZE);
    log_size = ((uint64_t *) log_buf)[0];
    print_consumed_log(log_buf, log_size);

#if CONFIG_FILE_WRITE
    if (fp) {
      ret = fwrite(log_buf, log_size, 1, fp);
      printf("fwrite len: %lu, ret: %d\n", log_size, ret);
    }
#endif

    // TODO: modulo
    new_cons_idx = cons_idx + ((log_size-1)/MAX_QBUF_SIZE+1);
    new_cons_idx_modulo = new_cons_idx & qmask;
    printf("prod idx: %lu, old cons idx: %lu, new cons idx: %lu mod: %lu\n"
        , prod_idx, cons_idx, new_cons_idx, new_cons_idx_modulo);
    cons_idx = new_cons_idx_modulo;
  }
#endif

#if CONFIG_FILE_WRITE
  if (fp)
      fclose(fp);
#endif

  ret = cleanup_log_consumer(&slq_info);
  return ret;
}
