/*
 * pacer_dbg_logger.c
 *
 * created on: May 29, 2019
 * author: aasthakm
 *
 * driver program for consuming debug logs
 * from ring buffer shared with the kernel
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>

#include "../../gpace/kmod-client/generic_q_inst.h"

#define MAX_FNAME_LEN 512

#define PAGE_SIZE 4096

#define LVL_DBG 0
#define LVL_EXP 1
#define PDBG_LVL  LVL_EXP

#define CONFIG_FILE_WRITE 0
#define CONFIG_BIN_WRITE  1

#define dprint(LVL, F, A...)  \
  do {  \
    if (PDBG_LVL <= LVL) { \
      printf("%s:%d " F "\n", __func__, __LINE__, A); \
    } \
  } while (0)


#define MAX_STATQ_ARRAYS  257

char *sme_dev = "/dev/PDBGLOG";

const char *smedir = "/local/sme/syslog";

extern void dbg_prt(char *buf, int buf_len);

typedef struct kern_q_hdr {
  uint64_t *prod_idx_p;
  uint64_t cons_idx;
  int qidx;
  int size;
  int elem_size;
  uint16_t port;
  uint16_t has_negative;
  char *ring_addr;
  char *hdr_addr;
} kern_q_hdr_t;

typedef struct usr_generic_q {
  char *devname;
  char *bin_fname;
  char *ascii_fname;
  int devfd;
  FILE *binfp;
  FILE *asciifp;
  kern_q_hdr_t kqh[MAX_STATQ_ARRAYS];
  char *binbuf[MAX_STATQ_ARRAYS];
  int64_t binbuflen[MAX_STATQ_ARRAYS];
} usr_generic_q_t;

int setup_log_consumer(char *devname, char *binfname, char *asciifname,
    usr_generic_q_t *u_pkts_q)
{
  int i;
  int ret = 0;
  char *address = NULL;
  int total_ring_size = 0;
  int64_t offset = 0;

  int devfd = open(devname, O_RDWR);
  if (devfd < 0)
    return -ENODEV;

  u_pkts_q->devname = sme_dev;
  u_pkts_q->bin_fname = binfname;
  u_pkts_q->ascii_fname = asciifname;
  u_pkts_q->devfd = devfd;

  u_pkts_q->binfp = fopen(u_pkts_q->bin_fname, "w");
  u_pkts_q->asciifp = fopen(u_pkts_q->ascii_fname, "w");

  for (i = 0; i < MAX_STATQ_ARRAYS; i++) {
    address = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE, devfd,
        offset);
    if (address == MAP_FAILED) {
      close(devfd);
      return -EIO;
    }

    u_pkts_q->kqh[i].hdr_addr = address;
    u_pkts_q->kqh[i].prod_idx_p = &((uint64_t *) address)[0];
    u_pkts_q->kqh[i].cons_idx = 0;
    u_pkts_q->kqh[i].qidx = ((uint32_t *) address)[4];
    u_pkts_q->kqh[i].size = ((uint32_t *) address)[5];
    u_pkts_q->kqh[i].elem_size = ((uint32_t *) address)[6];
    u_pkts_q->kqh[i].port = ((uint16_t *) address)[14];
    u_pkts_q->kqh[i].has_negative = ((uint16_t *) address)[15];

    offset += PAGE_SIZE;
    total_ring_size = (((u_pkts_q->kqh[i].size * u_pkts_q->kqh[i].elem_size)-1)
                        / PAGE_SIZE + 1) * PAGE_SIZE;
    dprint(LVL_DBG, "size %d elem size %d total ring size %d"
        , u_pkts_q->kqh[i].size, u_pkts_q->kqh[i].elem_size, total_ring_size
        );
    address = mmap(NULL, total_ring_size, PROT_READ, MAP_PRIVATE, devfd,
        offset);
    if (address == MAP_FAILED) {
      munmap(u_pkts_q->kqh[i].hdr_addr, PAGE_SIZE);
      close(devfd);
      return -EIO;
    }

    u_pkts_q->kqh[i].ring_addr = address;

    offset += total_ring_size;
  }

  return 0;
}

int close_log_consumer(usr_generic_q_t *u_pkts_q)
{
  int i;
  int total_ring_size = 0;
  for (i = 0; i < MAX_STATQ_ARRAYS; i++) {
    total_ring_size = (((u_pkts_q->kqh[i].size * u_pkts_q->kqh[i].elem_size)-1)
                        / PAGE_SIZE + 1) * PAGE_SIZE;
    if (u_pkts_q->kqh[i].ring_addr)
      munmap(u_pkts_q->kqh[i].ring_addr, total_ring_size);

    if (u_pkts_q->kqh[i].hdr_addr)
      munmap(u_pkts_q->kqh[i].hdr_addr, PAGE_SIZE);
  }

  if (u_pkts_q->devfd >= 0)
    close(u_pkts_q->devfd);

  if (u_pkts_q->binfp != NULL)
    fclose(u_pkts_q->binfp);

  if (u_pkts_q->asciifp != NULL)
    fclose(u_pkts_q->asciifp);

  return 0;
}

int get_binlog_kern(usr_generic_q_t *u_pkts_q, int qidx,
    char **buf_p, int64_t *buflen_p)
{
  int qsize = u_pkts_q->kqh[qidx].size;
  int elem_size = u_pkts_q->kqh[qidx].elem_size;
  uint64_t prod_idx = *(u_pkts_q->kqh[qidx].prod_idx_p);
  uint64_t cons_idx = u_pkts_q->kqh[qidx].cons_idx;
  uint64_t mod_cons_idx = cons_idx % qsize;
  int64_t num_elems = prod_idx - cons_idx;
  int totlen = num_elems * elem_size;
  char *buf = NULL;
  char *ptr = NULL;
  char *next_ptr = NULL;
  int offset = 0;
  int rlen = 0;
  int mlen = 0;

  if (totlen <= 0)
    return -ENOENT;

  buf = malloc(totlen);
  if (!buf)
    return -ENOMEM;

  dprint(LVL_DBG, "[%d] prod %lu cons %lu #elems %ld"
      , qidx, prod_idx, cons_idx, num_elems);
  ptr = u_pkts_q->kqh[qidx].ring_addr + (mod_cons_idx * elem_size);
  while (rlen < totlen) {
    if ((qsize - mod_cons_idx) >= num_elems) {
      mlen = totlen - rlen;
      next_ptr = NULL;
    } else {
      // case where prod wrapped around and is "behind" cons
      mlen = (qsize - mod_cons_idx) * elem_size;
      cons_idx += (mlen/elem_size);
      mod_cons_idx = cons_idx % qsize;
      next_ptr = u_pkts_q->kqh[qidx].ring_addr + (mod_cons_idx * elem_size);
    }
    memcpy(buf + offset, ptr, mlen);
    offset += mlen;
    rlen += mlen;
    ptr = next_ptr;
  }
  *buflen_p = num_elems * elem_size;
  *buf_p = buf;

  return 0;
}

int gen_asciilog_user(usr_generic_q_t *u_pkts_q, int qidx,
    char *binbuf, int64_t binbuflen)
{
  uint64_t i;
  largef_elem_t *e = NULL;
  uint64_t prod_idx = *(u_pkts_q->kqh[qidx].prod_idx_p);
  uint64_t cons_idx = u_pkts_q->kqh[qidx].cons_idx;
  int64_t num_elems = prod_idx - cons_idx;
  int elem_size = u_pkts_q->kqh[qidx].elem_size;

  if (num_elems == 0)
    return 0;

  fprintf(u_pkts_q->asciifp, "=== [%d] port %u #elems %ld prod %lu cons %lu ===\n"
      , u_pkts_q->kqh[qidx].qidx, u_pkts_q->kqh[qidx].port, num_elems, prod_idx, cons_idx
      );
  for (i = 0; i < num_elems; i++) {
    e = (largef_elem_t *) ((unsigned long) binbuf + (i * elem_size));
    fprintf(u_pkts_q->asciifp, "%lu [%d %d %d %d] T %u %u %u %u %u %u %u W %u %u %u "
        "G %u H %u %u %u %u %u %u RTO %u U %u %d %u\n"
        , e->ts, e->coreid, e->owner, e->ca_state, e->caller
        , e->packets_out, e->lost_out, e->sacked_out, e->retrans_out
        , e->snd_una, e->snd_nxt, e->rcv_nxt, e->cwnd, e->snd_wnd, e->rcv_wnd
        , e->pidx, e->tail, e->real, e->dummy, e->nxt, e->cwm, e->num_timers
        , e->icsk_rto, e->undo_marker, e->undo_retrans, e->high_seq
        );
  }
  return 0;
}

int main(int argc, char *argv[])
{
  char *ptr = NULL;
  int ret = 0;
  int qidx = 0;
//  char *binbuf = NULL;
//  int64_t binbuflen = 0;

  char binfname[MAX_FNAME_LEN];
  memset(binfname, 0, MAX_FNAME_LEN);
  sprintf(binfname, "%s/log.bin", smedir);

  char asciifname[MAX_FNAME_LEN];
  memset(asciifname, 0, MAX_FNAME_LEN);
  sprintf(asciifname, "%s/log.ascii", smedir);

  usr_generic_q_t u_pkts_q;
  memset(&u_pkts_q, 0, sizeof(usr_generic_q_t));
  ret = setup_log_consumer((char *) sme_dev, binfname, asciifname, &u_pkts_q);
  if (ret < 0)
    return EXIT_FAILURE;

  for (qidx = 0; qidx < MAX_STATQ_ARRAYS; qidx++) {
    ret = get_binlog_kern(&u_pkts_q, qidx,
        &u_pkts_q.binbuf[qidx], &u_pkts_q.binbuflen[qidx]);

#if CONFIG_BIN_WRITE
    int fret = 0;
    if (ret == 0) {
      fret = fwrite(u_pkts_q.binbuf[qidx], u_pkts_q.binbuflen[qidx], 1,
          u_pkts_q.binfp);
      dprint(LVL_DBG, "fwrite len %ld ret %d", u_pkts_q.binbuflen[qidx], fret);
    }
#endif

    ret = gen_asciilog_user(&u_pkts_q, qidx,
        u_pkts_q.binbuf[qidx], u_pkts_q.binbuflen[qidx]);

    if (u_pkts_q.binbuf[qidx] && u_pkts_q.binbuflen[qidx])
      free(u_pkts_q.binbuf[qidx]);
    u_pkts_q.binbuf[qidx] = NULL;
    u_pkts_q.binbuflen[qidx] = 0;
  }

  ret = close_log_consumer(&u_pkts_q);

  return EXIT_SUCCESS;
}
