/*
 * profile_conn_format.c
 *
 * created on: Mar 21, 2017
 * author: aasthakm
 *
 * serialized buffer format for profile to be
 * sent from application to kernel to set on a socket
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <profile_conn_format.h>
#include "profile_conn_msg.h"
#include "config.h"
//#include "../include/msg_hdr.h"

#define MAX_BUF_LEN 4096

// forward declaration / declared in .h file ====
// void dbg_prt(char *buf, int buf_len);

void
prt_hash(char *hbuf, int hlen, char *out)
{
  if (!hbuf || !hlen || !out)
    return;

  int i;
  for (i = 0; i < hlen; i++) {
    sprintf(out + (i*2), "%02x", *(unsigned char *) (hbuf + i));
  }
}

void
prepare_profile_buf_ihash(char *buf, uint64_t *buf_len, ihash_t prof_id,
    uint16_t num_out_reqs, uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *frame_spacing, uint64_t *first_frame_latency)
{
  int i;
  uint16_t off = 0;
  uint64_t off2 = 0;
  char *p = NULL;

  off = 0;
  p = buf;
  ((ihash_t *) (p + off))[0] = prof_id;
  off += sizeof(ihash_t);
  ((uint16_t *) (p + off))[0] = num_out_reqs;
  off += sizeof(uint16_t);

  p = p + off;

  off2 = 0;

  for (i = 0; i < num_out_reqs; i++) {
    ((uint16_t *) (p + off2))[0] = num_extra_slots[i];
    off2 += sizeof(uint16_t);
  }
  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *) (p + off2))[0] = num_out_frames[i];
    off2 += sizeof(uint64_t);
  }

  int j;
  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *) (p + off2))[0] = frame_spacing[i];
    off2 += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *) (p + off2))[0] = first_frame_latency[i];
    off2 += sizeof(uint64_t);
  }

  off += off2;
  *buf_len = off;
#if DBG
	printf("DBG#SME -- [%s:%s:%d] Printing generated buffer\n", __FILE__,__func__, __LINE__ );
	dbg_prt(buf, (int) *buf_len);
#endif
}

void
prepare_profile_buf(char *buf, uint64_t *buf_len, uint16_t prof_id,
    uint16_t num_out_reqs, uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *frame_spacing, uint64_t *first_frame_latency)
{
  int i;
  uint16_t off = 0;
  uint16_t off2 = 0;
  char *p = NULL;

  off = 0;
  p = buf;

  ((uint16_t *) (p + off))[0] = prof_id;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = num_out_reqs;
  off += sizeof(uint16_t);

  p = p + off;

  off2 = 0;

  for (i = 0; i < num_out_reqs; i++) {
    ((uint16_t *) (p + off2))[0] = num_extra_slots[i];
    off2 += sizeof(uint16_t);
  }
  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *) (p + off2))[0] = num_out_frames[i];
    off2 += sizeof(uint64_t);
  }

  int j;
  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *) (p + off2))[0] = frame_spacing[i];
    off2 += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *) (p + off2))[0] = first_frame_latency[i];
    off2 += sizeof(uint64_t);
  }

  off += off2;
  *buf_len = off;
}

/*
 * set profile for a fully qualified connection
 * mostly not required
 */
void
prepare_profile_conn_buf(char *buf, uint64_t *buf_len,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    uint16_t prof_id, uint16_t num_out_reqs, uint16_t *num_extra_slots,
    uint64_t *num_out_frames, uint64_t *frame_spacing, uint64_t *first_frame_latency)
{
  int i;
  uint16_t off = 0;
  uint64_t off2 = 0;
  char *p = NULL;

  off = 0;
  p = buf;

  ((uint32_t *) (p + off))[0] = src_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = in_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = out_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = dst_ip;
  off += sizeof(uint32_t);
  ((uint16_t *) (p + off))[0] = src_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = in_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = out_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = dst_port;
  off += sizeof(uint16_t);

  p = p + off;

  prepare_profile_buf(p, &off2, prof_id, num_out_reqs, num_extra_slots,
      num_out_frames, frame_spacing, first_frame_latency);
  off += off2;
  *buf_len = off;
  print_profile_conn_buf(buf, *buf_len);
}

/*
 * serialize an array of profile buffers read from file
 */
void
prepare_n_profile_map_buf_from_mem(char *buf, uint64_t *buf_len, uint16_t n_profile,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    char **profile, uint16_t *profile_len)
{
  int i;
  uint64_t off = 0;
  uint64_t off2 = 0;
  uint64_t total_len = 0;
  char *p = buf;
  char *prof = NULL;

  if (!buf || !buf_len || !n_profile || !profile || !profile_len)
    return;

  ((uint32_t *) (p + off))[0] = src_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = in_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = out_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = dst_ip;
  off += sizeof(uint32_t);

  ((uint16_t *) (p + off))[0] = src_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = in_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = out_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = dst_port;
  off += sizeof(uint16_t);

  ((uint16_t *) (p + off))[0] = n_profile;
  off += sizeof(uint16_t);

  // offset of the variable length profiles
  // from the start of the serial buffer
  // including n_profile + (n_profile * sizeof offset)
  total_len = off + (n_profile * sizeof(uint64_t));
  for (i = 0; i < n_profile; i++) {
    ((uint64_t *) (p + off))[i] = total_len;
    total_len += profile_len[i];
  }
  off += (n_profile * sizeof(uint64_t));

  // start of the variable length profile bufs
  prof = p + off;
  off2 = 0;
  for (i = 0; i < n_profile; i++) {
    memcpy(prof + off2, profile[i], profile_len[i]);
    off2 += profile_len[i];
  }

  *buf_len = off + off2;

#if DBG
  print_profile_map_buf(buf, *buf_len);
#endif
}

void
prepare_profile_id_conn_buf(char *buf, uint64_t *buf_len,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    uint16_t prof_id)
{
  int i;
  uint16_t off = 0;
  uint16_t off2 = 0;
  char *p = NULL;

  off = 0;
  p = buf;

  ((uint32_t *) (p + off))[0] = src_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = in_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = out_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = dst_ip;
  off += sizeof(uint32_t);
  ((uint16_t *) (p + off))[0] = src_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = in_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = out_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = dst_port;
  off += sizeof(uint16_t);

  ((uint16_t *) (p + off))[0] = prof_id;
  off += sizeof(uint16_t);
  *buf_len = off;
}

/*
 * pubhash is a single buffer containing num_pub
 * serialized hash entries. similarly, privhash
 */
void
prepare_profile_marker_buf(char *buf, uint64_t *buf_len,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    uint32_t num_pub, uint32_t num_priv, uint16_t hash_len,
    ihash_t *pubhash, ihash_t *privhash)
{
  int i;
  uint16_t off = 0;
  uint16_t off2 = 0;
  char *p = NULL;
  void *it = NULL;

  off = 0;
  p = buf;

  ((uint32_t *) (p + off))[0] = src_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = in_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = out_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = dst_ip;
  off += sizeof(uint32_t);
  ((uint16_t *) (p + off))[0] = src_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = in_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = out_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = dst_port;
  off += sizeof(uint16_t);

  ((uint32_t *) (p + off))[0] = num_pub;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = num_priv;
  off += sizeof(uint32_t);

  /*
   * hash(public inputs#, private inputs#)
   * acts as a unique request ID
   */
  if (num_pub > 0) {
    it = pubhash[0].byte;
    memcpy(p + off, it, (hash_len/2));
  }
  off += (hash_len/2);
  if (num_priv > 0) {
    it = privhash[num_priv-1].byte;
    memcpy(p + off, it, (hash_len/2));
  }
  off += (hash_len/2);

  // spare field for timestamp in kernel
  off += sizeof(uint64_t);

  for (i = 0; i < num_pub; i++) {
    it = pubhash[i].byte;
    memcpy(p + off, it, hash_len);
    off += hash_len;
    it += hash_len;
  }

  for (i = 0; i < num_priv; i++) {
    it = privhash[i].byte;
    memcpy(p + off, it, hash_len);
    off += hash_len;
    it += hash_len;
  }

  *buf_len = off;
}

void
write_profile_conn_to_file(char *fname, char *buf, int buf_len)
{
  int ret = 0;
  if (!fname || !buf || !buf_len)
    return;

  FILE *f = fopen(fname, "w");
  assert(f);
  ret = fwrite(buf, buf_len, 1, f);
  assert(ret == 1);
  fclose(f);
}

void
read_profile_conn_from_file(char *fname, char **buf, int *buf_len)
{
  int ret = 0;
  if (!fname || !buf || !buf_len)
    return;

  struct stat f_st;
  ret = stat(fname, &f_st);
  assert(!ret);
  int prof_len = f_st.st_size;
  printf("fname: %s, read len: %d\n", fname, prof_len);
  char *p = (char *) malloc(prof_len+1);
  memset(p, 0, prof_len+1);

  FILE *f = fopen(fname, "r");
  assert(f);
  ret = fread(p, prof_len, 1, f);
  printf("read ret: %d\n", ret);
  assert(ret == 1);
  fclose(f);

  *buf_len = prof_len;
  *buf = p;
}

// print functions ====

void
dbg_prt(char *buf, int buf_len)
{
  int it = 0;
  int s_it = 0;
  char sbuf[MAX_BUF_LEN];
  memset(sbuf, 0, MAX_BUF_LEN);
  for (it = 0; it < buf_len; it++) {
    sprintf(sbuf+s_it, "%02X", *(unsigned char *) (buf+it));
    s_it += 2;
  }
  //sprintf(sbuf+s_it, "'\0'");
  printf("%d: %s\n", buf_len, sbuf);
}


void
print_profile_buf_ihash(char *buf, int buf_len)
{
  int i, j;
  uint16_t off = 0;
  uint16_t prof_id;
  uint16_t num_out_reqs;
	//dbg_prt(buf, buf_len);
  ihash_t prof_id_ihash = ((ihash_t *) (buf + off))[0];
	//int k;
	//for(k = 0; k < 16; k++){
	//	prof_id_ihash.byte[k] = ((char*) (buf + off+k))[0];
	//}
  off += sizeof(ihash_t);
	char hbuf[64];
	int hbuf_len = 64;
	memset(hbuf, 0, hbuf_len);
	prt_hash(prof_id_ihash.byte, sizeof(ihash_t), hbuf );
	printf("prof id (hex): ");
  printf("\t%s\n", hbuf);

	//prt_hash(prof_id_ihash.byte, sizeof(ihash_t), hbuf);
	//printf("\n");
	
	//printf("Printed ihash successfully \n");
  num_out_reqs = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);
  printf("#reqs: %d\n", num_out_reqs);

  printf("#extra slots --- ");
  for (i = 0; i < num_out_reqs; i++) {
    uint16_t num_extra_slots = ((uint16_t *) (buf + off))[0];
    printf("%d ", num_extra_slots);
    off += sizeof(uint16_t);
  }
  printf("\n");

  printf("#frames --- ");
  for (i = 0; i < num_out_reqs; i++) {
    uint64_t num_out_frames = ((uint64_t *) (buf + off))[0];
    printf("%lu ", num_out_frames);
    off += sizeof(uint64_t);
  }
  printf("\n");
  
  printf("frame_spacing --- ");
  for (i = 0; i < num_out_reqs; i++) {
    uint64_t frame_spacing = 0;
    frame_spacing = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    printf("%lu ", frame_spacing);
  }
  printf("\n");
  
  printf("first frame latency --- ");
  for (i = 0; i < num_out_reqs; i++) {
    uint64_t lat = 0;
    lat = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    printf("%lu ", lat);
  }
  printf("\n");
}

void
print_profile_buf(char *buf, int buf_len)
{
  int i, j;
  uint16_t off = 0;
  uint16_t prof_id;
  uint16_t num_out_reqs;

  prof_id = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);
  printf("prof ID: %d\n", prof_id);

  num_out_reqs = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);
  printf("#reqs: %d\n", num_out_reqs);

  printf("#extra slots --- ");
  for (i = 0; i < num_out_reqs; i++) {
    uint16_t num_extra_slots = ((uint16_t *) (buf + off))[0];
    printf("%d ", num_extra_slots);
    off += sizeof(uint16_t);
  }
  printf("\n");

  printf("#frames --- ");
  for (i = 0; i < num_out_reqs; i++) {
    uint64_t num_out_frames = ((uint64_t *) (buf + off))[0];
    printf("%lu ", num_out_frames);
    off += sizeof(uint64_t);
  }
  printf("\n");
  
  printf("frame_spacing --- ");
  for (i = 0; i < num_out_reqs; i++) {
    uint64_t frame_spacing = 0;
    frame_spacing = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    printf("%lu ", frame_spacing);
  }
  printf("\n");
  
  printf("first frame latency --- ");
  for (i = 0; i < num_out_reqs; i++) {
    uint64_t lat = 0;
    lat = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    printf("%lu ", lat);
  }
  printf("\n");

  printf("\n");
}

void
print_profile_conn_buf(char *buf, int buf_len)
{
  int i;
  int off = 0;
  uint16_t src_port, in_port, out_port, dst_port;
  uint16_t num_out_reqs;
  struct sockaddr_in src_addr, in_addr, out_addr, dst_addr;
  char sip[INET_ADDRSTRLEN];
  char iip[INET_ADDRSTRLEN];
  char oip[INET_ADDRSTRLEN];
  char dip[INET_ADDRSTRLEN];
  memset(&src_addr, 0, sizeof(struct sockaddr_in));
  memset(&in_addr, 0, sizeof(struct sockaddr_in));
  memset(&out_addr, 0, sizeof(struct sockaddr_in));
  memset(&dst_addr, 0, sizeof(struct sockaddr_in));

  //dbg_prt(buf, buf_len);
  src_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[0];
  in_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[1];
  out_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[2];
  dst_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[3];
  off += (4 * sizeof(uint32_t));

  src_port = ntohs(((uint16_t *) (buf + off))[0]);
  in_port = ntohs(((uint16_t *) (buf + off))[1]);
  out_port = ntohs(((uint16_t *) (buf + off))[2]);
  dst_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += (4 * sizeof(uint16_t));

  inet_ntop(AF_INET, &(src_addr.sin_addr), sip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(in_addr.sin_addr), iip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(out_addr.sin_addr), oip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(dst_addr.sin_addr), dip, INET_ADDRSTRLEN);
  printf("SRC: %s, %u, %u, IN: %s, %u, %u, OUT: %s, %u, %u, DST: %s, %u, %u\n",
      sip, src_addr.sin_addr.s_addr, (uint32_t) src_port,
      iip, in_addr.sin_addr.s_addr, (uint32_t) in_port,
      oip, out_addr.sin_addr.s_addr, (uint32_t) out_port,
      dip, dst_addr.sin_addr.s_addr, (uint32_t) dst_port);

  print_profile_buf(buf+off, buf_len-off);
}

void
print_profile_map_buf(char *buf, int buf_len)
{
  int i, j;
  int off = 0;
  int prof_len_off = 0;
  int prof_len = 0;
  uint16_t n_prof, num_out_reqs;
  struct sockaddr_in src_addr, in_addr, out_addr, dst_addr;
  char sip[INET_ADDRSTRLEN];
  char iip[INET_ADDRSTRLEN];
  char oip[INET_ADDRSTRLEN];
  char dip[INET_ADDRSTRLEN];
  memset(&src_addr, 0, sizeof(struct sockaddr_in));
  memset(&in_addr, 0, sizeof(struct sockaddr_in));
  memset(&out_addr, 0, sizeof(struct sockaddr_in));
  memset(&dst_addr, 0, sizeof(struct sockaddr_in));

  //dbg_prt(buf, buf_len);
  src_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[0];
  in_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[1];
  out_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[2];
  dst_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[3];
  off += (4 * sizeof(uint32_t));

  src_addr.sin_port = ntohs(((uint16_t *) (buf + off))[0]);
  in_addr.sin_port = ntohs(((uint16_t *) (buf + off))[1]);
  out_addr.sin_port = ntohs(((uint16_t *) (buf + off))[2]);
  dst_addr.sin_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += (4 * sizeof(uint16_t));

  inet_ntop(AF_INET, &(src_addr.sin_addr), sip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(in_addr.sin_addr), iip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(out_addr.sin_addr), oip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(dst_addr.sin_addr), dip, INET_ADDRSTRLEN);
  printf("SRC: %s %u %u, IN: %s %u %u, OUT: %s %u %u, DST: %s %u %u\n",
      sip, src_addr.sin_addr.s_addr, src_addr.sin_port,
      iip, in_addr.sin_addr.s_addr, in_addr.sin_port,
      oip, out_addr.sin_addr.s_addr, out_addr.sin_port,
      dip, dst_addr.sin_addr.s_addr, dst_addr.sin_port);

  n_prof = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  prof_len_off = off;
  for (i = 0; i < n_prof; i++) {
    uint16_t prof_off = ((uint16_t *) (buf + prof_len_off))[i];
    char *prof = buf + prof_off;
    if (i == (n_prof - 1))
      prof_len = buf_len - prof_off;
    else
      prof_len = ((uint16_t *) (buf + prof_len_off))[i+1] - prof_off;
    print_profile_buf(prof, prof_len);
  } 
}

void
print_profile_map_buf_ihash(char *buf, int buf_len)
{
  int i, j;
  int off = 0;
  int prof_len_off = 0;
  int prof_len = 0;
  uint16_t n_prof, num_out_reqs;
//  char *pmap_buf = NULL;
//  uint64_t msg_type, msg_len;
  struct sockaddr_in src_addr, in_addr, out_addr, dst_addr;
  char sip[INET_ADDRSTRLEN];
  char iip[INET_ADDRSTRLEN];
  char oip[INET_ADDRSTRLEN];
  char dip[INET_ADDRSTRLEN];
  memset(&src_addr, 0, sizeof(struct sockaddr_in));
  memset(&in_addr, 0, sizeof(struct sockaddr_in));
  memset(&out_addr, 0, sizeof(struct sockaddr_in));
  memset(&dst_addr, 0, sizeof(struct sockaddr_in));
#if DBG
	printf("DBG#SME -- [%s:%s:%d] Printing generated buffer\n", __FILE__,__func__, __LINE__ );
  dbg_prt(buf, buf_len);
#endif

#if 0
  pmap_buf = buf + sizeof(msg_hdr_t);
  get_msg_cmd(buf, msg_type);
  get_msg_args_len(buf, msg_len);
  off += (2 * sizeof(uint64_t));
  printf("MSG TYPE %lu LEN %lu szof msg_hdr_t %lu off %d\n"
      , msg_type, msg_len, sizeof(msg_hdr_t), off);
#endif

  src_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[0];
  in_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[1];
  out_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[2];
  dst_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[3];
  off += (4 * sizeof(uint32_t));

  src_addr.sin_port = ntohs(((uint16_t *) (buf + off))[0]);
  in_addr.sin_port = ntohs(((uint16_t *) (buf + off))[1]);
  out_addr.sin_port = ntohs(((uint16_t *) (buf + off))[2]);
  dst_addr.sin_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += (4 * sizeof(uint16_t));

  inet_ntop(AF_INET, &(src_addr.sin_addr), sip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(in_addr.sin_addr), iip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(out_addr.sin_addr), oip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(dst_addr.sin_addr), dip, INET_ADDRSTRLEN);
  printf("SRC: %s %u %u, IN: %s %u %u, OUT: %s %u %u, DST: %s %u %u\n",
      sip, src_addr.sin_addr.s_addr, src_addr.sin_port,
      iip, in_addr.sin_addr.s_addr, in_addr.sin_port,
      oip, out_addr.sin_addr.s_addr, out_addr.sin_port,
      dip, dst_addr.sin_addr.s_addr, dst_addr.sin_port);

  n_prof = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  printf("# profiles %d, off %d\n", n_prof, off);

  prof_len_off = off;
  for (i = 0; i < n_prof; i++) {
    uint16_t prof_off = ((uint16_t *) (buf + prof_len_off))[i];
    char *prof = buf + prof_off;
    //char *prof = pmap_buf + prof_off;
    if (i == (n_prof - 1))
      prof_len = buf_len - prof_off;
    else
      prof_len = ((uint16_t *) (buf + prof_len_off))[i+1] - prof_off;
      //prof_len = ((uint16_t *) (pmap_buf + prof_len_off))[i+1] - prof_off;
		//printf("Printing profile ... \n");
    print_profile_buf_ihash(prof, prof_len);
  } 
}

void
print_profile_id_conn_buf(char *buf, int buf_len)
{
  int i;
  int off = 0;
  uint16_t src_port, in_port, out_port, dst_port;
  uint16_t prof_id;
  struct sockaddr_in src_addr, in_addr, out_addr, dst_addr;
  char sip[INET_ADDRSTRLEN];
  char iip[INET_ADDRSTRLEN];
  char oip[INET_ADDRSTRLEN];
  char dip[INET_ADDRSTRLEN];
  memset(&src_addr, 0, sizeof(struct sockaddr_in));
  memset(&in_addr, 0, sizeof(struct sockaddr_in));
  memset(&out_addr, 0, sizeof(struct sockaddr_in));
  memset(&dst_addr, 0, sizeof(struct sockaddr_in));

  src_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[0];
  in_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[1];
  out_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[2];
  dst_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[3];
  off += (4 * sizeof(uint32_t));

  src_port = ntohs(((uint16_t *) (buf + off))[0]);
  in_port = ntohs(((uint16_t *) (buf + off))[1]);
  out_port = ntohs(((uint16_t *) (buf + off))[2]);
  dst_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += (4 * sizeof(uint16_t));

  inet_ntop(AF_INET, &(src_addr.sin_addr), sip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(in_addr.sin_addr), iip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(out_addr.sin_addr), oip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(dst_addr.sin_addr), dip, INET_ADDRSTRLEN);
  printf("SRC: %s, %u, %u, IN: %s, %u, %u, OUT: %s, %u, %u, DST: %s, %u, %u\n",
      sip, src_addr.sin_addr.s_addr, (uint32_t) src_port,
      iip, in_addr.sin_addr.s_addr, (uint32_t) in_port,
      oip, out_addr.sin_addr.s_addr, (uint32_t) out_port,
      dip, dst_addr.sin_addr.s_addr, (uint32_t) dst_port);

  prof_id = ((uint16_t *) (buf + off))[0];
  printf("prof ID: %d\n", prof_id);
}

void
print_profile_marker_buf(char *buf, int buf_len)
{
  int i;
  int off = 0;
  uint16_t src_port, in_port, out_port, dst_port;
  uint32_t num_pub, num_priv;
  ihash_t *pubhash = NULL, *privhash = NULL;
  ihash_t reqid;
  struct sockaddr_in src_addr, in_addr, out_addr, dst_addr;
  char sip[INET_ADDRSTRLEN];
  char iip[INET_ADDRSTRLEN];
  char oip[INET_ADDRSTRLEN];
  char dip[INET_ADDRSTRLEN];
  memset(&src_addr, 0, sizeof(struct sockaddr_in));
  memset(&in_addr, 0, sizeof(struct sockaddr_in));
  memset(&out_addr, 0, sizeof(struct sockaddr_in));
  memset(&dst_addr, 0, sizeof(struct sockaddr_in));

  src_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[0];
  in_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[1];
  out_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[2];
  dst_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[3];
  off += (4 * sizeof(uint32_t));

  src_port = ntohs(((uint16_t *) (buf + off))[0]);
  in_port = ntohs(((uint16_t *) (buf + off))[1]);
  out_port = ntohs(((uint16_t *) (buf + off))[2]);
  dst_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += (4 * sizeof(uint16_t));

  inet_ntop(AF_INET, &(src_addr.sin_addr), sip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(in_addr.sin_addr), iip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(out_addr.sin_addr), oip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(dst_addr.sin_addr), dip, INET_ADDRSTRLEN);
  printf("SRC: %s, %u, %u, IN: %s, %u, %u, OUT: %s, %u, %u, DST: %s, %u, %u\n",
      sip, src_addr.sin_addr.s_addr, (uint32_t) src_port,
      iip, in_addr.sin_addr.s_addr, (uint32_t) in_port,
      oip, out_addr.sin_addr.s_addr, (uint32_t) out_port,
      dip, dst_addr.sin_addr.s_addr, (uint32_t) dst_port);

  num_pub = ((uint32_t *) (buf + off))[0];
  num_priv = ((uint32_t *) (buf + off))[1];
  off += (2 * sizeof(uint32_t));

  printf("#pub: %d, #priv: %d\n", num_pub, num_priv);

  memset(&reqid, 0, sizeof(ihash_t));
  memcpy(reqid.byte, buf + off, sizeof(ihash_t));
  off += sizeof(ihash_t);
  dbg_prt(reqid.byte, sizeof(ihash_t));

  pubhash = (ihash_t *) malloc(sizeof(ihash_t)*num_pub);
  privhash = (ihash_t *) malloc(sizeof(ihash_t)*num_priv);
  for (i = 0; i < num_pub; i++) {
    memcpy(&pubhash[i], buf + off, sizeof(ihash_t));
    dbg_prt(pubhash[i].byte, sizeof(ihash_t));
    printf("int version: %u\n", ((uint8_t *) pubhash[i].byte)[0]);
    off += sizeof(ihash_t);
  }

  for (i = 0; i < num_priv; i++) {
    memcpy(&privhash[i], buf + off, sizeof(ihash_t));
    dbg_prt(privhash[i].byte, sizeof(ihash_t));
    off += sizeof(ihash_t);
  }

  if (pubhash)
    free(pubhash);
  if (privhash)
    free(privhash);
  pubhash = privhash = NULL;
}
