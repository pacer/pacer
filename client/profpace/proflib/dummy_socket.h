/*
 * dummy_socket.h
 *
 * created on: Mar 28, 2017
 * author: aasthakm
 *
 * dummy socket library
 */
#ifndef __DUMMY_SOCKET_H__
#define __DUMMY_SOCKET_H__

#define MAX_BUF_LEN 512

enum {
  PROTO_UDP = 0,
  PROTO_TCP,
  MAX_PROTO
};

int create_client_socket(char *ip, int port, int proto);
int close_client_socket(int socket);
int create_server_tcp_socket(char *ip, int port);
int create_server_udp_socket(char *ip, int port);
int write_to_socket(int sock, char *write_buf, int write_len);
int read_from_socket(int sock, char *read_buf, int read_len);
int sendto_socket(int sock, char *ip, int port, char *write_buf, int write_len);
int recvfrom_socket(int sock, char *ip, int port, char *read_buf, int read_len,
    uint32_t *other_ip, uint16_t *other_port);

#endif /* __DUMMY_SOCKET_H__ */
