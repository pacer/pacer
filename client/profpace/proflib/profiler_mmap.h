/*
 * profiler_mmap.h
 *
 * created on: Sep 25, 2017
 * author: aasthakm
 *
 * API for accessing profile logs from
 * shared memory ring buffer
 */

#ifndef __PROFILER_MMAP_H__
#define __PROFILER_MMAP_H__

typedef struct sme_log_q_info {
  uint64_t *prod_idx;
  uint64_t *max_qbuf;
  uint64_t *head_idx;
  uint64_t *gen_id;
  int fd;
  int total_ring_size;
  uint64_t cons_idx;
  uint64_t cons_gen_id;
  char *hdr_addr;
  char *ring_addr;
} sme_log_q_info_t;

void alloc_log_consumer(sme_log_q_info_t **slq_infop);
void free_log_consumer(sme_log_q_info_t **slq_infop);
int prepare_log_consumer(char *dev, sme_log_q_info_t *slq_info);
int cleanup_log_consumer(sme_log_q_info_t *slq_info);
void do_consume(sme_log_q_info_t *slq_info);
int get_next_log(sme_log_q_info_t *slq_info, char **log, int *log_len);
void print_consumed_log(char *log_buf, uint64_t log_size);

#endif /* __PROFILER_MMAP_H__ */
