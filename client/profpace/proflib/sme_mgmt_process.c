/*
 * sme_mgmt_process.c
 *
 * created on: May 24, 2017
 * author: aasthakm
 *
 * sends the profile databases for different
 * interfaces to the dom0 kernel module.
 * can be invoked periodically, if the profile
 * database changes on any interface
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <argp.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>

#include <fcntl.h>
#include <sys/ioctl.h>

#include <msg_hdr.h>
#include <profile_conn_format.h>
#include "profile_conn_msg.h"
#include "dummy_socket.h"
#include "dummy_profile.h"

#define COMM_UDP 0
#define COMM_IOCTL 1

#define CONFIG_COMM COMM_IOCTL

#define DEFAULT_SERVER_PORT 2230

#define SME_IOCTL _IO('S', 0)

const char *SME_CTRL_DEV = "/dev/SMECTRL";

uint16_t DEFAULT_DUMMY_PORT = 2231;
const char *DEFAULT_DUMMY_IP = "139.19.168.173";
const char *DEFAULT_FILE = "/local/sme/profiles/profileFE.txt";

const char *arg_prog_version = "sme_mgmt_proc 1.0";
const char *arg_prog_bug_address = "xxx";
static char doc[] = "yyy";
static char args_doc[] = "--dummy_ip x.x.x.x --dummy_port 2231 "
"--ep_ip1 0.0.0.0 --ep_port1 xxx --ep_ip2 a.a.a.a --ep_port2 80 "
"--ep_ip3 a.a.a.a --ep_port3 xxx --ep_ip4 b.b.b.b --ep_port4 3306 "
"--rw_prof 0 --prof_file \"/local/sme/profiles\" --n_prof 10 ...";
static struct argp_option options[] = {
  { "dummy_ip", 's', "dummy_serv_ip", 0, "IP address of the dummy server" },
  { "dummy_port", 'S', "dummy_serv_port", 0, "Port of the dummy server" },
  { "ep_ip1", 'a', "ep_ip1", 0, "IP address of the connection endpoint for which profile map installed" },
  { "ep_port1", 'A', "ep_port1", 0, "Port of the connection endpoint for which profile map installed" },
  { "ep_ip2", 'b', "ep_ip2", 0, "IP address of the connection endpoint for which profile map installed" },
  { "ep_port2", 'B', "ep_port2", 0, "Port of the connection endpoint for which profile map installed" },
  { "ep_ip3", 'c', "ep_ip3", 0, "IP address of the connection endpoint for which profile map installed" },
  { "ep_port3", 'C', "ep_port3", 0, "Port of the connection endpoint for which profile map installed" },
  { "ep_ip4", 'd', "ep_ip4", 0, "IP address of the connection endpoint for which profile map installed" },
  { "ep_port4", 'D', "ep_port4", 0, "Port of the connection endpoint for which profile map installed" },
  { "rw_prof", 'r', "is_read", 0, "read profile from file (1)/write profile to file (0)" },
  { "prof_file", 'f', "pdir", 0, "directory to read/write profile file" },
  { "n_prof", 'n', "n_prof", 0, "# profiles in a database (useful only for writing a new profile to file" },
  { "ntype", 't', "node_type", 0, "Node type - FE (0), BE (1)" },
  { 0 }
};

enum {
  /* ip address of the dummy server */
  ARG_DUMMY_SERV_IP = 1,
  /* port of the dummy server */
  ARG_DUMMY_SERV_PORT,
  /* ip address of the connection endpoint for which profile map generated */
  ARG_EP_IP1,
  /* port of the connection endpoint for which profile map generated */
  ARG_EP_PORT1,
  ARG_EP_IP2,
  ARG_EP_PORT2,
  ARG_EP_IP3,
  ARG_EP_PORT3,
  ARG_EP_IP4,
  ARG_EP_PORT4,
  ARG_PROF_READ,
  ARG_PROF_FILE,
  ARG_N_PROF,
  ARG_N_TYPE,
  MAX_ARGS
};

struct arguments {
  char *dummy_serv_ip;
  uint16_t dummy_serv_port;
  char *ep_ip1;
  uint16_t ep_port1;
  char *ep_ip2;
  uint16_t ep_port2;
  char *ep_ip3;
  uint16_t ep_port3;
  char *ep_ip4;
  uint16_t ep_port4;
  uint8_t rw;
  char *prof_file;
  uint16_t n_prof;
  uint8_t node_type;
};

static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
#if 0
  printf("state # args: %d, argc: %d, max req: %d, key: %d, val: %s\n"
      , state->arg_num, state->argc, MAX_ARGS*2, key, arg);
#endif
  switch (key) {
    case 's':
      arguments->dummy_serv_ip = arg;
      break;
    case 'S':
      arguments->dummy_serv_port = atoi(arg);
      break;
    case 'a':
      arguments->ep_ip1 = arg;
      break;
    case 'A':
      arguments->ep_port1 = atoi(arg);
      break;
    case 'b':
      arguments->ep_ip2 = arg;
      break;
    case 'B':
      arguments->ep_port2 = atoi(arg);
      break;
    case 'c':
      arguments->ep_ip3 = arg;
      break;
    case 'C':
      arguments->ep_port3 = atoi(arg);
      break;
    case 'd':
      arguments->ep_ip4 = arg;
      break;
    case 'D':
      arguments->ep_port4 = atoi(arg);
      break;
    case 'r':
      arguments->rw = atoi(arg);
      break;
    case 'f':
      arguments->prof_file = arg;
      break;
    case 'n':
      arguments->n_prof = atoi(arg);
      break;
    case 't':
      arguments->node_type = atoi(arg);
      break;
    case ARGP_KEY_ARG: return 0;
    case ARGP_KEY_END:
#if 1
      if (state->argc < (2*MAX_ARGS-1))
        argp_usage(state);
#endif
      break;
    default: return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

static struct argp argp = {
  options,
  parse_opt,
  args_doc,
  doc,
  0, 0, 0
};

int
main(int argc, char *argv[])
{
  int ret = 0;
  int prof_len = 0;
  char *prof_buf = NULL;
#if CONFIG_COMM == COMM_IOCTL
  int sme_ctrl_fd = 0;
#else
  int dummy_client_sock = 0;
#endif

  char rcv_buf[32];

  uint32_t other_ip = 0;
  uint16_t other_port = 0;

  struct arguments arguments;
  arguments.rw = 0;
  arguments.dummy_serv_ip = (char *) DEFAULT_DUMMY_IP;
  arguments.dummy_serv_port = DEFAULT_DUMMY_PORT;
  arguments.prof_file = (char *) DEFAULT_FILE;
  arguments.n_prof = 0;
  
  argp_parse(&argp, argc, argv, 0, 0, &arguments);
  printf("dummy IP: %s, dummy port: %d, EP IP: %s, EP port: %d"
      ", RW: %d, prof dir: %s, #profiles: %d\n"
      , arguments.dummy_serv_ip, arguments.dummy_serv_port
      , arguments.ep_ip1, arguments.ep_port1
      , arguments.rw, arguments.prof_file, arguments.n_prof
      );

#if CONFIG_COMM == COMM_IOCTL
  sme_ctrl_fd = open(SME_CTRL_DEV, O_RDWR);
  printf("IOCTL DEV OPEN ret %d\n", sme_ctrl_fd);
#else
  dummy_client_sock =
    create_client_socket(arguments.dummy_serv_ip, arguments.dummy_serv_port, 0);
  printf("UDP SOCKET IP: %s, port: %d, sock: %d\n", arguments.dummy_serv_ip,
      arguments.dummy_serv_port, dummy_client_sock);
#endif

  int n_prof = arguments.n_prof;
  uint16_t *num_out_reqs = NULL;
  char **profile_ids = NULL;
  uint16_t **num_extra_slots = NULL;
  uint64_t **num_out_frames = NULL;
  uint64_t **spacing = NULL;
  uint64_t **latency = NULL;
  uint16_t n_req;
  int prof_it, req_it;
  char pid[16] = { '1', 'a', 'a', 'a', 'a', 'a', 'a', 'a',
                   'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a' };

  num_out_reqs = malloc(sizeof(uint16_t) * n_prof);
  profile_ids = malloc(sizeof(char *) * n_prof);
  num_extra_slots = malloc(sizeof(uint16_t *) * n_prof);
  num_out_frames = malloc(sizeof(uint64_t *) * n_prof);
  spacing = malloc(sizeof(uint64_t *) * n_prof);
  latency = malloc(sizeof(uint64_t *) * n_prof);

  for (prof_it = 0; prof_it < n_prof; prof_it++) {
    n_req = (rand() % 5) + 1;

    profile_ids[prof_it] = malloc(sizeof(char) * sizeof(ihash_t));
    memcpy(profile_ids[prof_it], pid, sizeof(ihash_t));
    pid[0] += 1;

    num_out_reqs[prof_it] = n_req;
    num_extra_slots[prof_it] = malloc(sizeof(uint16_t) * n_req);
    num_out_frames[prof_it] = malloc(sizeof(uint64_t) * n_req);
    spacing[prof_it] = malloc(sizeof(uint64_t) * n_req);
    latency[prof_it] = malloc(sizeof(uint64_t) * n_req);

    for (req_it = 0; req_it < n_req; req_it++) {
      num_extra_slots[prof_it][req_it] = 0;
      num_out_frames[prof_it][req_it] = (rand() % 8) + 1;
      spacing[prof_it][req_it] = ((rand() % 200) + 1) * 1000; // in us
      latency[prof_it][req_it] = ((rand() % 10000) + 1) * 1000; // in ms
    }
  }

  create_profile_map_ihash(&prof_buf, &prof_len,
      arguments.ep_ip1, arguments.ep_port1,
      arguments.ep_ip2, arguments.ep_port2,
      arguments.ep_ip3, arguments.ep_port3,
      arguments.ep_ip4, arguments.ep_port4,
      n_prof, profile_ids, num_out_reqs,
      num_extra_slots, num_out_frames, spacing, latency);

  printf("PMAP buf len %d, len in hdr %d\n", prof_len, ((uint16_t *) prof_buf)[1]);

#if CONFIG_COMM == COMM_UDP
  ret = sendto_socket(dummy_client_sock, arguments.dummy_serv_ip,
      arguments.dummy_serv_port, prof_buf, prof_len);
#if 0
  memset(rcv_buf, 0, 32);
  ret = recvfrom_socket(dummy_client_sock, arguments.dummy_serv_ip,
      arguments.dummy_serv_port, rcv_buf, 4, &other_ip, &other_port);
#endif
#else /* COMM_IOCTL */
  ret = ioctl(sme_ctrl_fd, SME_IOCTL, prof_buf);
  printf("IOCTL DEV ret %d len %d\n", ret, prof_len);
  //ret = write(sme_ctrl_fd, prof_buf, prof_len);
  //printf("WRITE DEV ret %d len %d\n", ret, prof_len);
#endif

  if (prof_buf)
    free(prof_buf);
  prof_buf = NULL;

#if CONFIG_COMM == COMM_IOCTL
  if (sme_ctrl_fd > 0)
    close(sme_ctrl_fd);
#else
  close_client_socket(dummy_client_sock);
#endif

  return EXIT_SUCCESS;
}
