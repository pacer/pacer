/*
 * dummy_msges.h
 *
 * created on: Apr 23, 2017
 * author: aasthakm
 *
 * some dummy messages for client server traffic
 */
#ifndef __DUMMY_MSGES_H__ 
#define __DUMMY_MSGES_H__

#include <sys/socket.h>

void prepare_client_addr_msg(char **buf, int *buf_len, struct sockaddr_in *caddr);
void prepare_multi_mtu_msg(char **buf, int *buf_len, int n_mtu);
void prepare_ping_msg(char **buf, int *buf_len, int n_mtu, double time);
void prepare_non_mtu_msg(char **buf, int *buf_len, int data_len);

#endif /* __DUMMY_MSGES_H__ */
