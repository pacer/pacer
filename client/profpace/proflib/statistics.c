/*
 * statistics.c
 *
 *	created on: Oct 23, 2017
 *	author: aasthakm
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "statistics.h"

#if CONFIG_STAT == 1
int
init_usr_stats(usr_stat_t *stat, char *name, double start, double end, double step)
{
	int size;
	memset(stat, 0, sizeof(stat));
	strncpy(stat->name, name, 64);
	stat->start = start;
	stat->end = end;
	stat->step = step;
	stat->count = 0;
	stat->min = 9999999999;
	stat->max = 0;
	size = (end - start) / step + 1;
	stat->dist = malloc(size * sizeof(uint32_t));
	memset(stat->dist, 0, size * sizeof(uint32_t));
	if (!stat->dist)
		return -ENOMEM;

	return 0;
}

void
add_point(usr_stat_t *stat, double value)
{
	int index;
	if (value > stat->max)
		stat->max = value;

	if (value < stat->min)
		stat->min = value;

	index = (value - stat->start) / stat->step;
	if (value >= stat->end) {
		// append to the last bucket
		index = ((stat->end - stat->start) / stat->step) - 1;
	} else if (value < stat->start) {
		// append to the first bucket
		index = 0;
	}

	stat->dist[index]++;
	stat->count++;
	stat->sum += value;
	stat->sqr_sum += (value * value);
}

void
print_distribution(usr_stat_t *stat, FILE *fp)
{
	if (!fp || !stat)
		return;

	if (stat->count <= 0)
		return;

	int i;
	double bucket;
	int size = (stat->end - stat->start) / stat->step + 1;

	double avg = 0.0;
	double stddev = 0.0;
	avg = stat->sum / stat->count;
	stddev = sqrt((stat->sqr_sum - 2 * avg * stat->sum
				+ stat->count * avg * avg) / stat->count);

	fprintf(fp, "%s (%lf, %lf, %lf)\n", stat->name, stat->start, stat->end, stat->step);
	fprintf(fp, "--------------------\n");

	fprintf(fp, "%s Total time (ms): %lf\n", stat->name, stat->sum);
	fprintf(fp, "%s Total count: %d\n", stat->name, stat->count);
	fprintf(fp, "%s Range: [%lf - %lf]\n", stat->name, stat->min, stat->max);
	fprintf(fp, "%s avg: %lf, stddev: %lf\n", stat->name, avg, stddev);

	for (i = 0; i < size; i++) {
		if (stat->dist[i] == 0)
			continue;
		bucket = (double) stat->start + i * stat->step;
		fprintf(fp, "%lf\t%d\n", bucket, stat->dist[i]);
	}
}
#endif
