/*
 * hash_func.h
 *
 * created on: Aug 30, 2017
 * author: aasthakm
 *
 * wrapper around standard library hash function
 */

#ifndef __HASH_FUNC_H__
#define __HASH_FUNC_H__

#include "MurmurHash3.h"

void murmur_hash(void *key, int key_len, int modulo, void *hbuf);

#endif /* __HASH_FUNC_H__ */
