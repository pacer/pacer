/*
 * dummy_profile.c
 *
 * created on: Mar 28, 2017
 * author: aasthakm
 *
 * small randomly generated profile serialized into a buffer
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include <sys/socket.h>
#include <arpa/inet.h>

#include <msg_hdr.h>
#include <profile_conn_format.h>

#include "config.h"
#include "profile_conn_msg.h"

#ifndef MAX_BUF_SIZE
#define MAX_BUF_SIZE 512

//MAX_BUF_LEN is only to support dbg_prt
#endif

//#define MAX_BUF_LEN 4096


void
create_tmp_profile(char **buf, int *buf_len, char *server_ip, int server_port,
    char *client_ip, int client_port, int client_sock, char *fname, int read)
{
  int ret = 0;
  int i, j;

  if (read == 0) {
    struct sockaddr_in localaddr, serveraddr;
    uint32_t remote_ip, self_ip;
    uint16_t remote_port, self_port;
    uint64_t *msg_len = NULL;
    //int c_port, s_port;
    //c_port = rand() % 10000;
    //s_port = rand() % 10000;

    memset(&localaddr, 0, sizeof(struct sockaddr_in));
    memset(&serveraddr, 0, sizeof(struct sockaddr_in));
    inet_aton(client_ip, &localaddr.sin_addr);
    inet_aton(server_ip, &serveraddr.sin_addr);
    //localaddr.sin_port = htons(c_port);
    //serveraddr.sin_port = htons(s_port);
    localaddr.sin_port = htons(client_port);
    serveraddr.sin_port = htons(server_port);

    self_ip = localaddr.sin_addr.s_addr;
    self_port = localaddr.sin_port;
    remote_ip = serveraddr.sin_addr.s_addr;
    remote_port = serveraddr.sin_port;

    printf("[%s:%d] local: %s %u %u, remote: %s %u %u\n", __func__, __LINE__,
        inet_ntoa(localaddr.sin_addr), self_ip, self_port,
        inet_ntoa(serveraddr.sin_addr), remote_ip, remote_port);

    int prof_len = MSG_HDR_LEN + PROF_CONN_IP_PORT_HDR_LEN + PROF_CONN_LEN(1);
    uint16_t num_extra_slots[1] = { 1 };
    uint64_t num_out_frames[1] = { 2 };
    uint64_t spacing[1] = { 5000000000 }; // time in nanoseconds
    uint64_t latency[1] = { 8000000000 }; // time in nanoseconds
    char *p = (char *) malloc(prof_len+1);
    memset(p, 0, prof_len+1);
    init_msg(p, MSG_T_PROFILE, &msg_len);
    prepare_profile_conn_buf(p+MSG_HDR_LEN, msg_len,
        self_ip, remote_ip, remote_ip, self_ip,
        self_port, remote_port, remote_port, self_port,
        1, 1, num_extra_slots, num_out_frames, spacing, latency);
    *buf_len = prof_len;
    *buf = p;
    printf("buf len: %d, msg len: %ld\n", *buf_len, *msg_len);

    write_profile_conn_to_file(fname, p+MSG_HDR_LEN, (int) *msg_len);
  } else {
    char *p = NULL, *tmp = NULL;
    int tmp_len = 0;
    uint16_t prof_len = 0;
    uint64_t *msg_len = NULL;
    read_profile_conn_from_file(fname, &tmp, &tmp_len);
    prof_len = tmp_len + MSG_HDR_LEN;
    p = (char *) malloc(prof_len+1);
    memset(p, 0, prof_len+1);
    init_msg(p, MSG_T_PROFILE, &msg_len);
    memcpy(p+MSG_HDR_LEN, tmp, tmp_len);
    *msg_len = tmp_len;
    *buf = p;
    *buf_len = prof_len;
  }
}

void
create_tmp_profile_map2(char **buf, int *buf_len,
    char *server_ip, int server_port, char *client_ip, int client_port,
    int client_sock, uint16_t n_prof, char *fname, int read)
{
  int ret = 0;
  int i, j;

  if (read == 0) {
    struct sockaddr_in localaddr, serveraddr;
    uint32_t remote_ip, self_ip;
    uint16_t remote_port, self_port;
    int *num_out_reqs = (int *) malloc(sizeof(int)*n_prof);
    int total_msg_len = 0;
    uint64_t *msg_len = NULL;
    char **prof_buf = (char **) malloc(sizeof(char *)*n_prof);
    uint16_t *prof_len = (uint16_t *) malloc(sizeof(uint16_t)*n_prof);
    uint16_t *num_extra_slots = NULL;
    uint64_t *num_out_frames = NULL;
    uint64_t *spacing = NULL;
    uint64_t *latency = NULL;

    memset(&localaddr, 0, sizeof(struct sockaddr_in));
    memset(&serveraddr, 0, sizeof(struct sockaddr_in));
    inet_aton(client_ip, &localaddr.sin_addr);
    inet_aton(server_ip, &serveraddr.sin_addr);
    localaddr.sin_port = htons(client_port);
    serveraddr.sin_port = htons(server_port);

    self_ip = localaddr.sin_addr.s_addr;
    remote_ip = serveraddr.sin_addr.s_addr;
    self_port = localaddr.sin_port;
    remote_port = serveraddr.sin_port;

    printf("%s:%d local addr: %s %u %u, remote addr: %s %u %u"
        ", client: %s %u, server %s %u\n"
        , __func__, __LINE__, inet_ntoa(localaddr.sin_addr), self_ip
        , ntohs(self_port), inet_ntoa(serveraddr.sin_addr), remote_ip
        , ntohs(remote_port), client_ip, client_port, server_ip, server_port
        );

    for (i = 0; i < n_prof; i++) {
      num_out_reqs[i] = (rand() % 5)+1;
      //printf("#req[%d]: %d\n", i, num_out_reqs[i]);
      prof_len[i] = PROF_CONN_LEN(num_out_reqs[i]);
      total_msg_len += prof_len[i];
      prof_buf[i] = (char *) malloc(prof_len[i]+1);
    }
    total_msg_len += PROF_CONN_IP_HDR_LEN;
    total_msg_len += PROF_CONN_PORT_HDR_LEN;
    total_msg_len += PROF_CONN_PMAP_HDR_LEN(n_prof);
    total_msg_len += MSG_HDR_LEN;
    //printf("total msg len: %d\n", total_msg_len);

    char *p = (char *) malloc(total_msg_len+1);
    memset(p, 0, total_msg_len+1);
    init_msg(p, MSG_T_PMAP, &msg_len);

    for (i = 0; i < n_prof; i++) {
      num_extra_slots = (uint16_t *) malloc(sizeof(uint16_t)*num_out_reqs[i]);
      num_out_frames = (uint64_t *) malloc(sizeof(uint64_t)*num_out_reqs[i]);
      spacing = (uint64_t *) malloc(sizeof(uint64_t)*num_out_reqs[i]);
      latency = (uint64_t *) malloc(sizeof(uint64_t)*num_out_reqs[i]);
      for (j = 0; j < num_out_reqs[i]; j++) {
        num_extra_slots[j] = rand() % 5;
        num_out_frames[j] = (rand() % 20)+1;
        spacing[j] = ((rand() % 7)+1)*100000;
        latency[j] = ((rand() % 20)+1)*1000000;
        //spacing[j] = ((rand() % 7)+1)*1000000;
        //latency[j] = ((rand() % 20)+1)*1000000;
        //printf("slots %d frames %d spacing %ld\n", num_extra_slots[j],
        //    num_out_frames[j], spacing[j]);
      }

      prepare_profile_buf(prof_buf[i], msg_len, i, num_out_reqs[i],
          num_extra_slots, num_out_frames, spacing, latency);

      free(num_extra_slots);
      free(num_out_frames);
      free(spacing);
      free(latency);
    }

    prepare_n_profile_map_buf_from_mem(p+MSG_HDR_LEN, msg_len, n_prof,
        self_ip, remote_ip, remote_ip, self_ip, self_port, remote_port,
        remote_port, self_port, prof_buf, prof_len);
    *buf = p;
    *buf_len = total_msg_len;
    //printf("buf len: %d, msg len: %d\n", *buf_len, *msg_len);

    write_profile_conn_to_file(fname, p+MSG_HDR_LEN, (int) *msg_len);
    for (i = 0; i < n_prof; i++)
      free(prof_buf[i]);

    free(prof_buf);
    free(prof_len);
    free(num_out_reqs);

  } else {
    char *p = NULL, *tmp = NULL;
    int tmp_len = 0;
    uint16_t prof_len = 0;
    uint64_t *msg_len = NULL;
    read_profile_conn_from_file(fname, &tmp, &tmp_len);
    prof_len = tmp_len + MSG_HDR_LEN;
    p = (char *) malloc(prof_len+1);
    memset(p, 0, prof_len+1);
    init_msg(p, MSG_T_PROFILE, &msg_len);
    memcpy(p+MSG_HDR_LEN, tmp, tmp_len);
    *msg_len = tmp_len;
    *buf = p;
    *buf_len = prof_len;
    if (tmp)
      free(tmp);
  }
}

void
create_tmp_profile_id(char **buf, int *buf_len, char *server_ip, int server_port,
    char *client_ip, int client_port, int client_sock, uint16_t prof_id)
{
  int ret = 0;
  struct sockaddr_in localaddr, serveraddr;
  uint32_t remote_ip, self_ip;
  uint16_t remote_port, self_port;
  uint64_t *msg_len = NULL;
  memset(&localaddr, 0, sizeof(struct sockaddr_in));
  memset(&serveraddr, 0, sizeof(struct sockaddr_in));
  inet_aton(client_ip, &localaddr.sin_addr);
  inet_aton(server_ip, &serveraddr.sin_addr);
  localaddr.sin_port = htons(client_port);
  serveraddr.sin_port = htons(server_port);

  self_ip = localaddr.sin_addr.s_addr;
  remote_ip = serveraddr.sin_addr.s_addr;
  self_port = localaddr.sin_port;
  remote_port = serveraddr.sin_port;

  printf("%s:%d local addr: %s %u %u, remote addr: %s %u %u"
      ", client: %s %u, server %s %u\n"
      , __func__, __LINE__, inet_ntoa(localaddr.sin_addr), self_ip
      , ntohs(self_port), inet_ntoa(serveraddr.sin_addr), remote_ip
      , ntohs(remote_port), client_ip, client_port, server_ip, server_port
      );

  int total_msg_len = MSG_HDR_LEN + PROF_CONN_IP_PORT_HDR_LEN + sizeof(uint16_t);
  char *p = (char *) malloc(total_msg_len+1);
  memset(p, 0, total_msg_len+1);
  init_msg(p, MSG_T_PROF_ID, &msg_len);
  prepare_profile_id_conn_buf(p+MSG_HDR_LEN, msg_len, self_ip, remote_ip,
      remote_ip, self_ip, self_port, remote_port, remote_port, self_port, prof_id);
  *buf_len = total_msg_len;
  *buf = p;
  print_profile_id_conn_buf(p+MSG_HDR_LEN, *msg_len);
}

void
create_tmp_profile_marker_h64(char **buf, int *buf_len,
    char *src_ip, int src_port, char *in_ip, int in_port,
    char *out_ip, int out_port, char *dst_ip, int dst_port,
    uint32_t num_pub, uint32_t num_priv,
    ihash_t *pubhash, ihash_t *privhash)
{
  int ret = 0;
  struct sockaddr_in srcaddr, inaddr, outaddr, dstaddr;
  uint32_t sip, iip, oip, dip;
  uint16_t sport, iport, oport, dport;
  uint64_t *msg_len = NULL;
  memset(&srcaddr, 0, sizeof(struct sockaddr_in));
  memset(&inaddr, 0, sizeof(struct sockaddr_in));
  memset(&outaddr, 0, sizeof(struct sockaddr_in));
  memset(&dstaddr, 0, sizeof(struct sockaddr_in));
  inet_aton(src_ip, &srcaddr.sin_addr);
  inet_aton(in_ip, &inaddr.sin_addr);
  inet_aton(out_ip, &outaddr.sin_addr);
  inet_aton(dst_ip, &dstaddr.sin_addr);
  srcaddr.sin_port = htons(src_port);
  inaddr.sin_port = htons(in_port);
  outaddr.sin_port = htons(out_port);
  dstaddr.sin_port = htons(dst_port);

  sip = srcaddr.sin_addr.s_addr;
  iip = inaddr.sin_addr.s_addr;
  oip = outaddr.sin_addr.s_addr;
  dip = dstaddr.sin_addr.s_addr;
  sport = srcaddr.sin_port;
  iport = inaddr.sin_port;
  oport = outaddr.sin_port;
  dport = dstaddr.sin_port;

#if DBG
  printf("%s:%d src: %s %u %u, in: %s %u %u, out: %s %u %u, dst: %s %u %u\n"
      , __func__, __LINE__
      , inet_ntoa(srcaddr.sin_addr), sip, ntohs(sport)
      , inet_ntoa(inaddr.sin_addr), iip, ntohs(iport)
      , inet_ntoa(outaddr.sin_addr), oip, ntohs(oport)
      , inet_ntoa(dstaddr.sin_addr), dip, ntohs(dport)
      );
#endif

  int total_msg_len = MSG_HDR_LEN + PROF_CONN_IP_PORT_HDR_LEN
    + (2*sizeof(uint32_t)) /* num_pub, num_priv fields */
    + sizeof(ihash_t) /* req ID hash */
    + sizeof(uint64_t) /* spare field for marker timestamp in kernel */
    + (num_pub*sizeof(ihash_t)) /* list of public input hashes */
    + (num_priv*sizeof(ihash_t)) /* list of private input hashes */
    ;

  char *p = (char *) malloc(total_msg_len+1);
  memset(p, 0, total_msg_len+1);
  init_msg(p, MSG_T_MARKER, &msg_len);
  prepare_profile_marker_buf(p+MSG_HDR_LEN, msg_len, sip, iip, oip, dip,
      sport, iport, oport, dport, num_pub, num_priv, sizeof(ihash_t),
      pubhash, privhash);

  *buf_len = total_msg_len;
  *buf = p;
#if DBG
  print_profile_marker_buf(p+MSG_HDR_LEN, *msg_len);
#endif
}

void
create_tmp_profile_map(char **buf, int *buf_len, char *ip1, int port1,
    char *ip2, int port2, char *ip3, int port3, char *ip4, int port4,
    int client_sock, uint16_t n_prof, char *fname, int read)
{
  int ret = 0;
  int i, j;

  if (read == 0) {
    struct sockaddr_in addr[4];
    uint32_t u_ip[4];
    uint16_t u_port[4];
    int *num_out_reqs = (int *) malloc(sizeof(int)*n_prof);
    int total_msg_len = 0;
    uint64_t *msg_len = NULL;
    char **prof_buf = (char **) malloc(sizeof(char *)*n_prof);
    uint16_t *prof_len = (uint16_t *) malloc(sizeof(uint16_t)*n_prof);
    uint16_t *num_extra_slots = NULL;
    uint64_t *num_out_frames = NULL;
    uint64_t *spacing = NULL;
    uint64_t *latency = NULL;

    char p_buf[MAX_BUF_SIZE];
    int p_len = 0;

    int i;
    for (i = 0; i < 4; i++) {
      memset(&addr[i], 0, sizeof(struct sockaddr_in));
    }

    inet_aton(ip1, &addr[0].sin_addr);
    inet_aton(ip2, &addr[1].sin_addr);
    inet_aton(ip3, &addr[2].sin_addr);
    inet_aton(ip4, &addr[3].sin_addr);
    addr[0].sin_port = htons(port1);
    addr[1].sin_port = htons(port2);
    addr[2].sin_port = htons(port3);
    addr[3].sin_port = htons(port4);

    memset(p_buf, 0, MAX_BUF_SIZE);

    for (i = 0; i < 4; i++) {
      u_ip[i] = addr[i].sin_addr.s_addr;
      u_port[i] = addr[i].sin_port;
      sprintf(p_buf + p_len, "conn[%d]: %s %u %u\n\t"
          , i, inet_ntoa(addr[i].sin_addr), u_ip[i], ntohs(u_port[i]));
      p_len = strlen(p_buf);
    }

    printf("%s:%d CONN PAIR:\n\t%s", __func__, __LINE__, p_buf);

    for (i = 0; i < n_prof; i++) {
      num_out_reqs[i] = (rand() % 5)+1;
      //printf("#req[%d]: %d\n", i, num_out_reqs[i]);
      prof_len[i] = PROF_CONN_LEN(num_out_reqs[i]);
      total_msg_len += prof_len[i];
      prof_buf[i] = (char *) malloc(prof_len[i]+1);
    }
    total_msg_len += PROF_CONN_IP_HDR_LEN;
    total_msg_len += PROF_CONN_PORT_HDR_LEN;
    total_msg_len += PROF_CONN_PMAP_HDR_LEN(n_prof);
    total_msg_len += MSG_HDR_LEN;
    //printf("total msg len: %d\n", total_msg_len);

    char *p = (char *) malloc(total_msg_len+1);
    memset(p, 0, total_msg_len+1);
    init_msg(p, MSG_T_PMAP, &msg_len);

    for (i = 0; i < n_prof; i++) {
      num_extra_slots = (uint16_t *) malloc(sizeof(uint16_t)*num_out_reqs[i]);
      num_out_frames = (uint64_t *) malloc(sizeof(uint64_t)*num_out_reqs[i]);
      spacing = (uint64_t *) malloc(sizeof(uint64_t)*num_out_reqs[i]);
      latency = (uint64_t *) malloc(sizeof(uint64_t)*num_out_reqs[i]);
      for (j = 0; j < num_out_reqs[i]; j++) {
        num_extra_slots[j] = rand() % 5;
        num_out_frames[j] = (rand() % 20)+1;
        spacing[j] = ((rand() % 7)+1)*100000;
        latency[j] = ((rand() % 20)+1)*1000000;
        //spacing[j] = ((rand() % 7)+1)*1000000;
        //latency[j] = ((rand() % 20)+1)*1000000;
        //printf("slots %d frames %d spacing %ld\n", num_extra_slots[j],
        //    num_out_frames[j], spacing[j]);
      }

      prepare_profile_buf(prof_buf[i], msg_len, i, num_out_reqs[i],
          num_extra_slots, num_out_frames, spacing, latency);

      free(num_extra_slots);
      free(num_out_frames);
      free(spacing);
      free(latency);
    }

    prepare_n_profile_map_buf_from_mem(p+MSG_HDR_LEN, msg_len, n_prof,
        u_ip[0], u_ip[1], u_ip[2], u_ip[3], u_port[0], u_port[1],
        u_port[2], u_port[3], prof_buf, prof_len);
    *buf = p;
    *buf_len = total_msg_len;
    //printf("buf len: %d, msg len: %d\n", *buf_len, *msg_len);

    write_profile_conn_to_file(fname, p+MSG_HDR_LEN, (int) *msg_len);
    for (i = 0; i < n_prof; i++)
      free(prof_buf[i]);

    free(prof_buf);
    free(prof_len);
    free(num_out_reqs);

  } else {
    char *p = NULL, *tmp = NULL;
    int tmp_len = 0;
    uint16_t prof_len = 0;
    uint64_t *msg_len = NULL;
    read_profile_conn_from_file(fname, &tmp, &tmp_len);
    prof_len = tmp_len + MSG_HDR_LEN;
    p = (char *) malloc(prof_len+1);
    memset(p, 0, prof_len+1);
    init_msg(p, MSG_T_PROFILE, &msg_len);
    memcpy(p+MSG_HDR_LEN, tmp, tmp_len);
    *msg_len = tmp_len;
    *buf = p;
    *buf_len = prof_len;
    if (tmp)
      free(tmp);
  }
}

void
create_profile_map(char **buf, int *buf_len,
    char *ip1, uint16_t port1, char *ip2, uint16_t port2,
    char *ip3, uint16_t port3, char *ip4, uint16_t port4,
    uint16_t n_prof, uint16_t *profile_id, uint16_t *num_out_reqs,
    uint16_t **num_extra_slots, uint64_t **num_out_frames,
    uint64_t **spacing, uint64_t **first_frame_latency)
{
  if (!buf || !buf_len)
    return;

  if (!ip1 || !ip2 || !ip3 || !ip4)
    return;

  if (!n_prof || !profile_id || !num_out_reqs || !num_extra_slots
      || !num_out_frames || !spacing || !first_frame_latency)
    return;

#if DBG
  printf("buf: %p, buf_len: %p, ip1: %s %u, ip2: %s %u, ip3: %s %u, ip4: %s %u"
      ", n_prof: %u, pid: %p, reqs: %p, slots: %p, frames: %p, spacing: %p"
      ", latency: %p\n"
      , buf, buf_len, ip1, port1, ip2, port2, ip3, port3, ip4, port4
      , n_prof, profile_id, num_out_reqs, num_extra_slots, num_out_frames
      , spacing, first_frame_latency);
#endif

  struct sockaddr_in addr[4];
  uint32_t u_ip[4];
  uint16_t u_port[4];
  int total_msg_len = 0;
  uint64_t *msg_len = NULL;
  uint16_t *prof_len = NULL;
  char **prof_buf = NULL;
  int i;

  for (i = 0; i < 4; i++) {
    memset(&addr[i], 0, sizeof(struct sockaddr_in));
  }

  inet_aton(ip1, &addr[0].sin_addr);
  inet_aton(ip2, &addr[1].sin_addr);
  inet_aton(ip3, &addr[2].sin_addr);
  inet_aton(ip4, &addr[3].sin_addr);
  addr[0].sin_port = htons(port1);
  addr[1].sin_port = htons(port2);
  addr[2].sin_port = htons(port3);
  addr[3].sin_port = htons(port4);

  char p_buf[MAX_BUF_SIZE];
  int p_len = 0;

  memset(p_buf, 0, MAX_BUF_SIZE);

  for (i = 0; i < 4; i++) {
    u_ip[i] = addr[i].sin_addr.s_addr;
    u_port[i] = addr[i].sin_port;
#if DBG
    sprintf(p_buf + p_len, "conn[%d]: %s %u %u\n\t"
        , i, inet_ntoa(addr[i].sin_addr), u_ip[i], ntohs(u_port[i]));
    p_len = strlen(p_buf);
#endif
  }
#if DBG
  printf("[%s:%d] len: %d\n%s\n", __func__, __LINE__, p_len, p_buf);

  for (i = 0; i < n_prof; i++) {
    int j = 0;
    uint16_t n_req = num_out_reqs[i];
    printf("prof id %u: #req: %u\n", i, n_req);
    printf("\textra slots: ");
    for (j = 0; j < n_req; j++) {
      printf("%u ", num_extra_slots[i][j]);
    }
    printf("\n");
    printf("\tnum frames: ");
    for (j = 0; j < n_req; j++) {
      printf("%llu ", num_out_frames[i][j]);
    }
    printf("\n");
    printf("\tspacing: ");
    for (j = 0; j < n_req; j++) {
      printf("%lu ", spacing[i][j]);
    }
    printf("\n");
    printf("\tlatency: ");
    for (j = 0; j < n_req; j++) {
      printf("%lu ", first_frame_latency[i][j]);
    }
    printf("\n");
  }
#endif

  prof_len = (uint16_t *) malloc(sizeof(uint16_t) * n_prof);
  prof_buf = (char **) malloc(sizeof(char *) * n_prof);
  for (i = 0; i < n_prof; i++) {
    prof_len[i] = PROF_CONN_LEN(num_out_reqs[i]);
    total_msg_len += prof_len[i];
    prof_buf[i] = (char *) malloc(prof_len[i]+1);
    memset(prof_buf[i], 0, prof_len[i]+1);
  }
  total_msg_len += PROF_CONN_IP_HDR_LEN;
  total_msg_len += PROF_CONN_PORT_HDR_LEN;
  total_msg_len += PROF_CONN_PMAP_HDR_LEN(n_prof);
  total_msg_len += MSG_HDR_LEN;

  char *p = (char *) malloc(total_msg_len+1);
  memset(p, 0, total_msg_len+1);
  init_msg(p, MSG_T_PMAP, &msg_len);

  for (i = 0; i < n_prof; i++) {
    prepare_profile_buf(prof_buf[i], msg_len, i, num_out_reqs[i],
        num_extra_slots[i], num_out_frames[i], spacing[i], first_frame_latency[i]);
  }

  prepare_n_profile_map_buf_from_mem(p+MSG_HDR_LEN, msg_len, n_prof,
      u_ip[0], u_ip[1], u_ip[2], u_ip[3],
      u_port[0], u_port[1], u_port[2], u_port[3], prof_buf, prof_len);
  *buf = p;
  *buf_len = total_msg_len;

  free(prof_buf);
  free(prof_len);

#if DBG
  printf("msg len: %d, total msg len: %d\n", *msg_len, total_msg_len);
#endif
}


//TODO: Move to a common file
//static inline void
//prt_hash(char *hbuf, int hlen, char *out)
//{
//  if (!hbuf || !hlen || !out)
//    return;
//
//  int i;
//  for (i = 0; i < hlen; i++) {
//    sprintf(out + (i*2), "%02x", *(unsigned char *) (hbuf + i));
//  }
//}

void
create_profile_map_ihash(char **buf, int *buf_len,
    char *ip1, uint16_t port1, char *ip2, uint16_t port2,
    char *ip3, uint16_t port3, char *ip4, uint16_t port4,
    uint16_t n_prof, char **profile_id, uint16_t *num_out_reqs,
    uint16_t **num_extra_slots, uint64_t **num_out_frames,
    uint64_t **spacing, uint64_t **first_frame_latency)
{
  if (!buf || !buf_len)
    return;

  if (!ip1 || !ip2 || !ip3 || !ip4)
    return;

  if (!n_prof || !profile_id || !num_out_reqs || !num_extra_slots
      || !num_out_frames || !spacing || !first_frame_latency)
    return;

#if DBG
  printf("buf: %p, buf_len: %p, ip1: %s %u, ip2: %s %u, ip3: %s %u, ip4: %s %u"
      ", n_prof: %u, pid: %p, reqs: %p, slots: %p, frames: %p, spacing: %p"
      ", latency: %p\n"
      , buf, buf_len, ip1, port1, ip2, port2, ip3, port3, ip4, port4
      , n_prof, profile_id, num_out_reqs, num_extra_slots, num_out_frames
      , spacing, first_frame_latency);
#endif

  struct sockaddr_in addr[4];
  uint32_t u_ip[4];
  uint16_t u_port[4];
  int64_t total_msg_len = 0;
  uint64_t *msg_len = NULL;
  uint16_t *prof_len = NULL;
  char **prof_buf = NULL;
  int i;

  for (i = 0; i < 4; i++) {
    memset(&addr[i], 0, sizeof(struct sockaddr_in));
  }

  inet_aton(ip1, &addr[0].sin_addr);
  inet_aton(ip2, &addr[1].sin_addr);
  inet_aton(ip3, &addr[2].sin_addr);
  inet_aton(ip4, &addr[3].sin_addr);
  addr[0].sin_port = htons(port1);
  addr[1].sin_port = htons(port2);
  addr[2].sin_port = htons(port3);
  addr[3].sin_port = htons(port4);

  char p_buf[MAX_BUF_SIZE];
  int p_len = 0;

  memset(p_buf, 0, MAX_BUF_SIZE);

  for (i = 0; i < 4; i++) {
    u_ip[i] = addr[i].sin_addr.s_addr;
    u_port[i] = addr[i].sin_port;
#if DBG
    sprintf(p_buf + p_len, "conn[%d]: %s %u %u\n\t"
        , i, inet_ntoa(addr[i].sin_addr), u_ip[i], ntohs(u_port[i]));
    p_len = strlen(p_buf);
#endif
  }
#if DBG
  printf("[%s:%d] len: %d\n%s\n", __func__, __LINE__, p_len, p_buf);

  for (i = 0; i < n_prof; i++) {
    int j = 0;
    uint16_t n_req = num_out_reqs[i];
    char hbuf[64];
    int hbuf_len = 64;
    memset(hbuf, 0, hbuf_len);
    prt_hash(profile_id[i], sizeof(ihash_t), hbuf);

    printf("prof id (hex): %s\n", hbuf);
    printf("#req: %u\n", n_req);
    printf("\textra slots: ");
    for (j = 0; j < n_req; j++) {
      printf("%u ", num_extra_slots[i][j]);
    }
    printf("\n");
    printf("\tnum frames: ");
    for (j = 0; j < n_req; j++) {
      printf("%lu ", num_out_frames[i][j]);
    }
    printf("\n");
    printf("\tspacing: ");
    for (j = 0; j < n_req; j++) {
      printf("%lu ", spacing[i][j]);
    }
    printf("\n");
    printf("\tlatency: ");
    for (j = 0; j < n_req; j++) {
      //printf("i is: %i, j is %i\n",i,j);
      printf("%lu ", first_frame_latency[i][j]);
    }
    printf("\n");
  }
#endif
  prof_len = (uint16_t *) malloc(sizeof(uint16_t) * n_prof);
  prof_buf = (char **) malloc(sizeof(char *) * n_prof);
  for (i = 0; i < n_prof; i++) {
    prof_len[i] = PROF_CONN_LEN_IHASH(num_out_reqs[i]);
    total_msg_len += prof_len[i];
    prof_buf[i] = (char *) malloc(prof_len[i]+1);
    memset(prof_buf[i], 0, prof_len[i]+1);
  }
  total_msg_len += PROF_CONN_IP_HDR_LEN;
  total_msg_len += PROF_CONN_PORT_HDR_LEN;
  total_msg_len += PROF_CONN_PMAP_HDR_LEN(n_prof);
  total_msg_len += MSG_HDR_LEN;

  char *p = (char *) malloc(total_msg_len+1);
  memset(p, 0, total_msg_len+1);
  init_msg(p, MSG_T_PMAP_IHASH, &msg_len);

  for (i = 0; i < n_prof; i++) {
    ihash_t *pid = ((ihash_t **) profile_id)[i];

    prepare_profile_buf_ihash(prof_buf[i], msg_len, *pid, num_out_reqs[i],
        num_extra_slots[i], num_out_frames[i], spacing[i], first_frame_latency[i]);
#if DBG
    printf("DBG#SME -- [%s:%s:%d] Printing generated buffer\n",
        __FILE__,__func__, __LINE__ );
    dbg_prt(prof_buf[i], (int) *msg_len);
#endif
  }

  prepare_n_profile_map_buf_from_mem(p+MSG_HDR_LEN, msg_len, n_prof,
      u_ip[0], u_ip[1], u_ip[2], u_ip[3],
      u_port[0], u_port[1], u_port[2], u_port[3], prof_buf, prof_len);
  *buf = p;
  //TODO:CLEAN IT UP!!!
  *buf_len = total_msg_len;

#if DBG
  dbg_prt(p, total_msg_len);
#endif

  free(prof_buf);
  free(prof_len);

#if DBG
  printf("msg len: %d, total msg len: %d\n", *msg_len, total_msg_len);
#endif
}


void
free_buf(char **buf, int buf_len)
{
  if (!buf || !(*buf) || !buf_len)
    return;

#if DBG
  printf("[%s:%d] buf: %p, *buf: %p, len: %d\n"
      , __func__, __LINE__, buf, *buf, buf_len);
#endif
  free(*buf);
  *buf = NULL;
#if DBG
  printf("[%s:%d] buf: %p, *buf: %p, len: %d\n"
      , __func__, __LINE__, buf, *buf, buf_len);
#endif
}

