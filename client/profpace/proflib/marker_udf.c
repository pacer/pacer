/*
 * udf.c
 *
 * created on: Sep 4, 2017
 * author: aasthakm
 *
 * sample UDF to perform socket I/O
 * can be tested by using dummy_sock_server
 */
#include <mysql.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "dummy_socket.h"

#include <sys/ioctl.h>

#define SME_IOCTL _IO('S', 0)

#ifdef __cplusplus
extern "C" {
#endif

  my_bool udpsock_init(UDF_INIT *initid, UDF_ARGS *args, char *message);
  void udpsock_deinit(UDF_INIT *initid);
  long long udpsock(UDF_INIT *initid, UDF_ARGS *args, char *result,
      unsigned long *length, char *is_null, char *error);
#ifdef __cplusplus
}
#endif


#define MAX_ARGS 4
#define USE_IOCTL 1


typedef struct sock_info {
  int sockfd;
  char sendto_ip[32];
  int sendto_port;
} sock_info_t;

typedef struct ioctl_info {
  int ioctlfd;
} ioctl_info_t;

int
dev_ioctl(int fd, int cmd, char *buf, int len)
{
  int ret = ioctl(fd, SME_IOCTL, buf);
  return ret;
}


  
static void
prt_binary(char *buf, int buflen, char *prtstr, int *prtlen)
{
  int it = 0;
  int p_it = 0;
  for (it = 0; it < buflen; it++) {
    sprintf(prtstr + p_it, "%02X", *(unsigned char *) (buf + it));
    p_it += 2;
  }
  *prtlen = p_it;
}


static char *
php_hex2bin(const unsigned char *old, const size_t oldlen)
{
  size_t target_length = oldlen >> 1;
  char *str = (char *) malloc(target_length);
  unsigned char *ret = str;
  size_t i, j;

  for (i = j = 0; i < target_length; i++) {
    unsigned char c = old[j++];
    unsigned char d;

    if (c >= '0' && c <= '9') {
      d = (c - '0') << 4;
    } else if (c >= 'a' && c <= 'f') {
      d = (c - 'a' + 10) << 4;
    } else if (c >= 'A' && c <= 'F') {
      d = (c - 'A' + 10) << 4;
    } else {
      free(str);
      return NULL;
    }
    c = old[j++];
    if (c >= '0' && c <= '9') {
      d |= c - '0';
    } else if (c >= 'a' && c <= 'f') {
      d |= c - 'a' + 10;
    } else if (c >= 'A' && c <= 'F') {
      d |= c - 'A' + 10;
    } else {
      free(str);
      return NULL;
    }
    ret[i] = d;
  }
  ret[i] = '\0';

  return str;
}

my_bool
udpsock_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
  if (args->arg_count != MAX_ARGS) {
    sprintf(message, "Expected %d args, received %d\n"
        "Usage: <marker> <marker len> <dummy ip> <dummy port>"
        , MAX_ARGS, args->arg_count);
    return 1;
  }

  if (args->arg_type[0] != STRING_RESULT ||
      args->arg_type[1] != INT_RESULT ||
      args->arg_type[2] != STRING_RESULT ||
      args->arg_type[3] != INT_RESULT) {
    strcpy(message,
        "Expected args: <write buf> <write len> <dummy ip> <dummy port>");
    return 1;
  }
#if USE_IOCTL

  int ioctl_fd;
  ioctl_fd = open("/dev/SMECTRL", O_RDWR);

  ioctl_info_t *ioctl_info = (ioctl_info_t *) malloc(sizeof(ioctl_info_t));
  memset(ioctl_info, 0, sizeof(ioctl_info_t));
  ioctl_info->ioctlfd = ioctl_fd;
  initid->ptr = (char *) ioctl_info;

#else

  int sockfd;
  struct sockaddr_in addr;

  sockfd = create_client_socket(args->args[2], *(int *) args->args[3], 0);
  if (sockfd < 0) {
    sprintf(message, "error opening client UDP sock to %s, %d",
        args->args[2], *(int *) args->args[3]);
    return 1;
  }

  sock_info_t *sinfo = (sock_info_t *) malloc(sizeof(sock_info_t));
  memset(sinfo, 0, sizeof(sock_info_t));
  sinfo->sockfd = sockfd;
  memcpy(sinfo->sendto_ip, args->args[2], args->lengths[2]);
  sinfo->sendto_port = *(int *) args->args[3];
  initid->ptr = (char *) sinfo;

#endif
  return 0;
}

void
udpsock_deinit(UDF_INIT *initid)
{
  if (!initid || !initid->ptr)
    return;

#if USE_IOCTL

  ioctl_info_t *ioctl_info = (ioctl_info_t *) initid->ptr;
  if (ioctl_info->ioctlfd > 0)
    close(ioctl_info->ioctlfd);

  free(ioctl_info);

#else

  sock_info_t *sinfo = (sock_info_t *) initid->ptr;
  if (sinfo->sockfd > 0)
    close(sinfo->sockfd);

  free(sinfo);

#endif
}

/*
 * return 0 on success
 */
long long
udpsock(UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length,
    char *is_null, char *error)
{
  if (!initid || !initid->ptr) {
    sprintf(result, "invalid socket");
    return -1;
  }
  
  int ret = 0;
  uint32_t other_ip = 0;
  uint16_t other_port = 0;

  char* data = args->args[0];
  int write_len = *(int *) args->args[1];
  char* binary = php_hex2bin(data, write_len*2);

#if USE_IOCTL
  ioctl_info_t *ioctl_info = (ioctl_info_t *) initid->ptr;
  int ioctlfd = ioctl_info->ioctlfd;
  ret = ioctl(ioctlfd, SME_IOCTL, binary);
#else
  sock_info_t *sinfo = (sock_info_t *) initid->ptr;
  int sockfd = sinfo->sockfd;
  ret = sendto_socket(sockfd, sinfo->sendto_ip, sinfo->sendto_port,
      binary, write_len);
#endif
  //sprintf(result, "err in udp write len: %d, ret: %d, err: %d\n"
  //    , write_len, ret, errno);

  //ret = recvfrom_socket(sockfd, sinfo->sendto_ip, sinfo->sendto_port,
  //    dbgprt, 8, &other_ip, &other_port);

  //return result;
  return (ret != write_len);
}
