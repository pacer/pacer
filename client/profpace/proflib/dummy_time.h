#include <sys/time.h>

#ifndef __DUMMY_TIME_H__
#define __DUMMY_TIME_H__

static inline double get_time(void)
{
  struct timeval t;
  gettimeofday(&t, NULL);
  return (double)t.tv_sec * 1000.0 + (double)t.tv_usec/1000.0;
}

#define INIT_TIMER(name) double t_start_##name = 0.0, t_end_##name = 0.0
#define START_TIMER(name) t_start_##name = get_time()
#define END_TIMER(name) t_end_##name = get_time()
#define START_TIME(name)  (t_start_##name)
#define END_TIME(name)  (t_end_##name)
#define SPENT_TIME(name)  (END_TIME(name) - START_TIME(name))

#endif /* __DUMMY_TIME_H__ */
