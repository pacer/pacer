/*
 * profile_conn_format.h
 *
 * created on: Mar 24, 2017
 * author: aasthakm
 *
 * serialized buffer format for profile to be
 * sent from application to kernel to set on a socket
 */

#ifndef __PROFILE_CONN_FORMAT_H__
#define __PROFILE_CONN_FORMAT_H__

#define NUM_OCTETS64 (sizeof(uint64_t)/sizeof(uint8_t))

#define PROF_CONN_IP_HDR_LEN (4 * sizeof(uint32_t))
#define PROF_CONN_PORT_HDR_LEN (4 * sizeof(uint16_t))
#define PROF_CONN_IP_PORT_HDR_LEN PROF_CONN_IP_HDR_LEN + PROF_CONN_PORT_HDR_LEN

#define PROF_CONN_PMAP_HDR_LEN(n) \
  (sizeof(uint16_t) + ((n) * sizeof(uint64_t)))

/*
 * u16 extra slots[n]
 * u64 frames[n]
 * u64 spacing[n]
 * u64 latency[n]
 */
#define PROF_CONN_VAR_LEN_BUF_LEN(n)  \
  (((n) * sizeof(uint16_t)) + \
  ((n) * sizeof(uint64_t)) +  \
  ((n) * sizeof(uint64_t)) +  \
  ((n) * sizeof(uint64_t)))

#define PROF_CONN_LEN(n)  \
  ((2*sizeof(uint16_t)) + PROF_CONN_VAR_LEN_BUF_LEN(n))

//added the size of ihash_t instead of uint16_t for the new profile_id
#define PROF_CONN_LEN_IHASH(n)  \
((sizeof(ihash_t) + sizeof(uint16_t)) + PROF_CONN_VAR_LEN_BUF_LEN(n))

//added the size of ihash_t instead of uint16_tfor the new profile_id
// added an additional hashtable offset  for linking the profiles 
#define PROF_CONN_LEN_IHASH_HASHTABLE(n)  \
((sizeof(ihash_t) + sizeof(uint16_t)) + sizeof(hashtable_offset_t) + PROF_CONN_VAR_LEN_BUF_LEN(n))

typedef struct ihash {
  char byte[16];
} ihash_t;

#endif /* __PROFILE_CONN_FORMAT_H__ */
