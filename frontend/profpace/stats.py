import sys
import numpy
#from scipy.interpolate import interp1d
from datetime import datetime
from datetime import timedelta

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm


#### time utilities ####
prog_start_time = datetime.now()


def gettime():
    # returns elapsed milliseconds since start of the program
    # this has better precision than time.time()
    dt = datetime.now() - prog_start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000.0 + \
        dt.microseconds / 1000.0
    return ms


def converttime(ts_str):
    # str format: %H:%M:%S.%f; f in nanoseconds
    dt = datetime.strptime(ts_str[:-3], '%H:%M:%S.%f')
    ms = (dt.hour * 60 * 60 + dt.minute * 60 + dt.second) * 1000.0 + \
        dt.microsecond / 1000.0
    return ms


def get_color_index(i):
    #    cmap = cm.get_cmap('jet', num_ticks)
    #    ckeys=matplotlib.colors.cnames.keys()
    #    num_colors = len(ckeys)
    #    print ckeys

    map = [
        'blue',
        'red',
        'green',
        'yellow',
        'darkturquoise',
        'orange',
        'lime',
        'darkslategrey',
        'fuchsia',
        'gray',
        'teal',
    ]
    len_map = len(map)
    return map[i % len_map]

#### compute statistics at runtime ####


def compute_histogram(oarr, numbins, attr):
    if len(oarr) > 0:
        obj = oarr[0]
        if hasattr(obj, attr):
            data = [getattr(o, attr) for o in oarr]
            [hist, bin_edges] = numpy.histogram(data, bins=numbins)
            return hist, bin_edges


def compute_percentile(oarr, npercent, attr):
    if len(oarr) > 0:
        obj = oarr[0]
        if hasattr(obj, attr):
            data = [getattr(o, attr) for o in oarr]
            percentile = numpy.percentile(data, npercent)
            return percentile


def compute_aggr_stats(oarr, npercent, attr):
    avg = 0.0
    std = 0.0
    maximum = 0.0
    minimum = 0.0
    percentile = 0.0
    if len(oarr) > 0:
        obj = oarr[0]
        if hasattr(obj, attr):
            data = [getattr(o, attr) for o in oarr]
            avg = numpy.mean(data, dtype=numpy.float64)
            std = numpy.std(data, dtype=numpy.float64)
            maximum = numpy.amax(data)
            minimum = numpy.amin(data)
            percentile = numpy.percentile(data, npercent)
    return [avg, std, maximum, minimum, percentile]


def compute_all_stats(oarr, numbins, npercent, attr):
    avg = 0.0
    std = 0.0
    maximum = 0.0
    minimum = 0.0
    percentile = 0.0
    hist = []
    bin_edges = []
    if len(oarr) > 0:
        obj = oarr[0]
        if hasattr(obj, attr):
            data = [getattr(o, attr) for o in oarr]
            avg = numpy.mean(data, dtype=numpy.float64)
            std = numpy.std(data, dtype=numpy.float64)
            maximum = numpy.amax(data)
            minimum = numpy.amin(data)
            percentile = numpy.percentile(data, npercent)
            [hist, bin_edges] = numpy.histogram(data, bins=numbins)
    return [avg, std, maximum, minimum, percentile, hist, bin_edges]


#### compute statistics from data from file ####
def compute_histogram_from_raw(data, numbins):
    hist = []
    bin_edges = []
    [hist, bin_edges] = numpy.histogram(data, bins=numbins)
    return hist, bin_edges


def compute_percentile_from_raw(data, npercent):
    percentile = numpy.percentile(data, npercent)
    return percentile


def compute_aggr_stats_from_raw(data, npercent):
    avg = 0.0
    std = 0.0
    maximum = 0.0
    minimum = 0.0
    percentile = 0.0
    avg = numpy.mean(data, dtype=numpy.float64)
    std = numpy.std(data, dtype=numpy.float64)
    maximum = numpy.amax(data)
    minimum = numpy.amin(data)
    percentile = numpy.percentile(data, npercent)
    return [avg, std, maximum, minimum, percentile]


def compute_all_stats_from_raw(data, numbins, npercent):
    avg = 0.0
    std = 0.0
    maximum = 0.0
    minimum = 0.0
    percentile = 0.0
    hist = []
    bin_edges = []
    avg = numpy.mean(data, dtype=numpy.float64)
    std = numpy.std(data, dtype=numpy.float64)
    maximum = numpy.amax(data)
    minimum = numpy.amin(data)
    percentile = numpy.percentile(data, npercent)
    [hist, bin_edges] = numpy.histogram(data, bins=numbins)
    return [avg, std, maximum, minimum, percentile, hist, bin_edges]


def compute_all_stats_from_raw_all_percentiles(data, numbins, fname, stats_name):
    avg = 0.0
    std = 0.0
    maximum = 0.0
    minimum = 0.0
    percentile = 0.0
    hist = []
    bin_edges = []
    avg = numpy.mean(data, dtype=numpy.float64)
    std = numpy.std(data, dtype=numpy.float64)
    maximum = numpy.amax(data)
    minimum = numpy.amin(data)
    p50 = numpy.percentile(data, 50)
    p90 = numpy.percentile(data, 90)
    p95 = numpy.percentile(data, 95)
    p99 = numpy.percentile(data, 99)
    p100 = numpy.percentile(data, 100)

    [hist, bin_edges] = numpy.histogram(data, bins=numbins)
    print_all_stats_all_percentiles(fname, stats_name, len(data), avg, std,
                                    maximum, minimum, p50, p90, p95, p99, p100, hist, bin_edges)
    return [avg, std, maximum, minimum, p50, p90, p95, p99, p100, hist, bin_edges]


#### output formatting utilities ####
def print_histogram(hist, bin_edges, fname):
    assert(len(bin_edges) == len(hist) + 1)
    alldata = [[(bin_edges[i] + bin_edges[i + 1]) / 2.0, hist[i]]
               for i in range(0, len(bin_edges) - 1)]
    if (fname == "stdout"):
        numpy.savetxt(sys.stdout, alldata, fmt='%5.3f %df', newline="\n")
    else:
        numpy.savetxt(fname, alldata, fmt='%5.3f %df', newline="\n")


def print_multiple_histograms(hists, bin_mids, numcols, fname):
    assert (len(bin_mids) == len(hists[0]))
    numrows = len(bin_mids)
    alldata = []
    for i in range(0, numrows):
        row = [bin_mids[i]]
        row.extend([hists[j][i] for j in range(0, numcols)])
        alldata.append(row)
    fmt_str = "%5.3f "
    comment_str = "# bucket-mean "
    for i in range(0, numcols):
        fmt_str += "%d "
        comment_str += "req" + str(i) + " "

    f = open(fname, 'w')
    f.write(comment_str + '\n')
    for i in range(0, len(alldata)):
        row = alldata[i]
        for j in range(0, len(row)):
            f.write(str(row[j]) + ' ')
        f.write('\n')
    f.close()
#     numpy.savetxt(fname, alldata, fmt=fmt_str, newline="\n", comments=comment_str)


def print_all_stats_all_percentiles(fname, stats_name, count, avg, std,
                                    maxval, minval, p50, p90, p95, p99, p100, hist, bin_edges):

    alldata = [[(bin_edges[i] + bin_edges[i + 1]) / 2.0, hist[i]]
               for i in range(0, len(bin_edges) - 1)]

    f = open(fname, 'w')
    f.write('# aggregate stats\n')
    f.write('# %s\n\n' % stats_name)
    f.write('count: %d\n' % count)
    f.write('average: %5.3f\n' % avg)
    f.write('stddev: %5.3f\n' % std)
    f.write('max: %5.3f\n' % maxval)
    f.write('min: %5.3f\n' % minval)
    f.write('50p: %5.3f\n' % p50)
    f.write('90p: %5.3f\n' % p90)
    f.write('95p: %5.3f\n' % p95)
    f.write('99p: %5.3f\n' % p99)
    f.write('100p: %5.3f\n\n' % p100)
    f.write('Statistics summary: count/average/stddev/max/min/50p/90p/95p/99p/100p:\n')
    f.write('%d/%5.3f/%5.3f/%5.3f/%5.3f/%5.3f/%5.3f/%5.3f/%5.3f/%5.3f\n\n' %
            (count, avg, std, maxval, minval, p50, p90, p95, p99, p100))
    f.write('distribution\n')
    f.write('------------\n')
    for i in range(0, len(alldata)):
        f.write('%5.3f %d\n' % (alldata[i][0], alldata[i][1]))
    f.close()


def print_all_stats(fname, stats_name, count, avg, std,
                    maxval, minval, p95, p99, hist, bin_edges):

    alldata = [[(bin_edges[i] + bin_edges[i + 1]) / 2.0, hist[i]]
               for i in range(0, len(bin_edges) - 1)]

    f = open(fname, 'w')
    f.write('# aggregate stats\n')
    f.write('# %s\n\n' % stats_name)
    f.write('count: %d\n' % count)
    f.write('average: %5.3f\n' % avg)
    f.write('stddev: %5.3f\n' % std)
    f.write('max: %5.3f\n' % maxval)
    f.write('min: %5.3f\n' % minval)
    f.write('95p: %5.3f\n' % p95)
    f.write('99p: %5.3f\n\n' % p99)
    f.write('distribution\n')
    f.write('------------\n')
    for i in range(0, len(alldata)):
        f.write('%5.3f %d\n' % (alldata[i][0], alldata[i][1]))
    f.close()


def print_client_raw(oarr, fname):
    alldata = []
    for o in oarr:
        alldata.append([o.rlen, o.rtime])
    if (fname == "stdout"):
        numpy.savetxt(sys.stdout, alldata, fmt='%d %5.3f', newline="\n",
                      header="timestamp response-time size")
    else:
        numpy.savetxt(fname, alldata, fmt='%d %5.3f', newline="\n",
                      header="timestamp response-time size")


def print_server_raw(oarr, fname):
    alldata = []
    for o in oarr:
        alldata.append([o[1].rlen, o[0].rtime, o[1].rtime])
    if (fname == "stdout"):
        numpy.savetxt(sys.stdout, alldata, fmt='%d %5.3f %5.3f', newline="\n",
                      header="size first-response-time last-response-time")
    else:
        numpy.savetxt(fname, alldata, fmt='%d %5.3f %5.3f', newline="\n",
                      header="size first-response-time last-response-time")


def print_client_raw_rows(file_ids, arr_oarr, attr, fname, print_raw=True):
    if len(arr_oarr) <= 0:
        print("Error")
        return

    oarr = arr_oarr[0]
    obj = oarr[0]
    if hasattr(obj, attr) == False:
        print("attr error")
        return

    obs_id = [str(o) for o in file_ids]

    if (fname == "stdout"):
        print((' '.join(obs_id)))
        for oarr in arr_oarr:
            data = [str(getattr(o, attr)) for o in oarr]
            print((' '.join(data)))
    else:
        if print_raw:
            f = open(fname, 'w')
            f.write(' '.join(obs_id) + '\n')
            for oarr in arr_oarr:
                data = [str(getattr(o, attr)) for o in oarr]
                f.write(' '.join(data) + '\n')
            f.close()


def print_server_raw_rows(file_ids, arr_oarr, attr, split_arr, fname):
    if len(arr_oarr) <= 0:
        print("Error")
        return

    oarr = arr_oarr[0]
    if (len(oarr) < 0):
        print("elem error")
        return

    if (len(oarr) == 0):
        return

    obj = oarr[0]
    if hasattr(obj, attr) == False:
        print("attr error")
        return

    obs_id = [str(o) for o in file_ids]

    if (fname == "stdout"):
        print((' '.join(obs_id)))
        for oarr in arr_oarr:
            data = [str(getattr(o, attr)) for o in oarr]
            for i in range(0, len(oarr) / split):
                print((' '.join(data[i * split: (i + 1) * split])))
    else:
        f = open(fname, 'w')
        f.write(' '.join(obs_id) + '\n')
        oarr_it = 0
        num_split = len(split_arr)
        for oarr in arr_oarr:
            split_it = oarr_it % num_split
            split = split_arr[split_it]
            data = [str(getattr(o, attr)) for o in oarr]
            for i in range(0, len(oarr) / split):
                f.write(' '.join(data[i * split: (i + 1) * split]) + ' ')
            if (split_it == num_split - 1):
                f.write('\n')
            oarr_it += 1
        f.close()

def print_stats(observations, num_iter, host, proto, path, exptime_ms=-1, num_clients = 1):

    totalreq = 0
    totalxfer = 0
    totalrtt = 0.0
    pc_avrtt = []
    for obs in observations:
        totalreq += len(obs)
        pc_rtt = 0.0
        for o in obs:
            totalxfer += o.rlen
            totalrtt += o.rtime
            pc_rtt += o.rtime
        pc_av = pc_rtt/len(obs)
        pc_avrtt.append(pc_av)

    maxreq = num_iter
    totaltime_s = exptime_ms/1000.0
    xput = totalreq/totaltime_s
    xput_kbps = (totalxfer/1024)/totaltime_s
    perclient_avg_rtt = numpy.mean(pc_avrtt)
    #global_avg_rtt = (totalrtt/totalreq)
    global_avg_rtt = (perclient_avg_rtt/num_clients)

    print("")
    print("%#-25s %s" % ("Server Hostname:", host))
    print("%#-25s %d" % ("Server Port:", proto))
    print("%#-25s %s" % ("Document Path:", ("/"+path)))

    print("")
    print("%#-25s %d" % ("Concurrency Level:", num_clients))
    print("%#-25s %#-6.3f seconds" % ("Time taken for tests:", totaltime_s))
    print("%#-25s %d" % ("Complete requests:", totalreq))
    print("%#-25s %d" % ("Failed requests:", (maxreq - totalreq)))
    print("%#-25s %d" % ("HTML transferred:", totalxfer))
    print("%#-25s %#-6.3f" % ("Requests per second:", xput))
    print("%#-25s %#-6.3f [ms] (mean)" \
        % ("Time per request:", perclient_avg_rtt))
    print("%#-25s %#-6.3f [ms] (mean, across all concurrent requests)" \
        % ("Time per request:", global_avg_rtt))
    print("%#-25s %#-6.3f [Kbytes/sec] received"   \
        % ("Transfer rate:", xput_kbps))
    print("")

    allobs = sum(observations, [])
    rtime_arr = [o.rtime for o in allobs]
    avg_rtt = numpy.mean(rtime_arr)
    std_rtt = numpy.std(rtime_arr)
    p50_rtt = numpy.percentile(rtime_arr, 50)
    p90_rtt = numpy.percentile(rtime_arr, 90)
    p95_rtt = numpy.percentile(rtime_arr, 95)
    p99_rtt = numpy.percentile(rtime_arr, 99)
    #p100_rtt = numpy.percentile(rtime_arr, 100)
    max_rtt = numpy.amax(rtime_arr)
    min_rtt = numpy.amin(rtime_arr)

    print("Latency statistics (ms)")
    print("-----------------------")
    print("RTT avg:\t%#-6.3f" % avg_rtt)
    print("RTT 50p:\t%#-6.3f" % p50_rtt)
    print("RTT 90p:\t%#-6.3f" % p90_rtt)
    print("RTT 95p:\t%#-6.3f" % p95_rtt)
    print("RTT 99p:\t%#-6.3f" % p99_rtt)
    #print "RTT 100p:\t%#-6.3f" % p100_rtt
    print("RTT max:\t%#-6.3f, req#: %d" % (max_rtt, rtime_arr.index(max_rtt)))
    print("RTT min:\t%#-6.3f, req#: %d" % (min_rtt, rtime_arr.index(min_rtt)))

    rlen_arr = [o.rlen for o in allobs]
    avg_rlen = numpy.mean(rlen_arr)
    std_rlen = numpy.std(rlen_arr)
    p50_rlen = numpy.percentile(rlen_arr, 50)
    p90_rlen = numpy.percentile(rlen_arr, 90)
    p95_rlen = numpy.percentile(rlen_arr, 95)
    p99_rlen = numpy.percentile(rlen_arr, 99)
    #p100_rlen = numpy.percentile(rlen_arr, 100)
    max_rlen = numpy.amax(rlen_arr)
    min_rlen = numpy.amin(rlen_arr)

    print("")
    print("Size statistics (bytes)")
    print("-----------------------")
    print("RSIZE avg:\t%#-6.3f" % avg_rlen)
    print("RSIZE 50p:\t%#-6.3f" % p50_rlen)
    print("RSIZE 90p:\t%#-6.3f" % p90_rlen)
    print("RSIZE 95p:\t%#-6.3f" % p95_rlen)
    print("RSIZE 99p:\t%#-6.3f" % p99_rlen)
    #print "RSIZE 100p:\t%#-6.3f" % p100_rlen
    print("RSIZE max:\t%#-6.3f, req#: %d" % (max_rlen, rlen_arr.index(max_rlen)))
    print("RSIZE min:\t%#-6.3f, req#: %d" % (min_rlen, rlen_arr.index(min_rlen)))

    #print ""
    #print "Individual observations"
    #for o in allobs:
    #    print "[%12.3f]: status = %d, rsize = %d, rtime = %6.3f"    \
    #        % (o.timestamp, o.status, o.rlen, o.rtime)
    
    
    
def print_trace(oarr, fname, print_raw=True, total_exp_time_ms=-1, num_clients = 1):
    f = open(fname, 'w')

    f.write('------------------- Latency Statistics (ms) -------------------\n')

    obs_rtime = [o.rtime for o in oarr]
    count = len(oarr)
    print("count: " + str(count))
    max_rtime = max(obs_rtime)
    min_rtime = min(obs_rtime)
    avg = numpy.mean(obs_rtime)
    std = numpy.std(obs_rtime)
    p50 = numpy.percentile(obs_rtime, 50)
    p90 = numpy.percentile(obs_rtime, 90)
    p95 = numpy.percentile(obs_rtime, 95)
    p99 = numpy.percentile(obs_rtime, 99)
    p100 = numpy.percentile(obs_rtime, 100)

    f.write("count: " + str(count) + "\n")
    f.write("max run time: " + str(max_rtime) + ", Request #: " +
            str(obs_rtime.index(max_rtime)) + "\n")
    f.write("min run time: " + str(min_rtime) + ", Request #: " +
            str(obs_rtime.index(min_rtime)) + "\n")
    f.write("avg time: " + str(avg) + "\n")
    f.write("std dev: " + str(std) + "\n")
    f.write('50p: %5.3f\n' % p50)
    f.write('90p: %5.3f\n' % p90)
    f.write('95p: %5.3f\n' % p95)
    f.write('99p: %5.3f\n' % p99)
    f.write('100p: %5.3f\n\n' % p100)
    f.write('Statistics summary: count / average / stddev / max / min / 50p / 90p / 95p / 99p / 100p:\n')
    f.write('%d / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f\n\n' %
            (count, avg, std, max_rtime, min_rtime, p50, p90, p95, p99, p100))

    f.write('------------------- Size Statistics (bytes) -------------------\n')

    obs_rlen = [o.rlen for o in oarr]
    count = len(oarr)
    max_rlen = max(obs_rlen)
    min_rlen = min(obs_rlen)
    avg = numpy.mean(obs_rlen)
    std = numpy.std(obs_rlen)
    p50 = numpy.percentile(obs_rlen, 50)
    p90 = numpy.percentile(obs_rlen, 90)
    p95 = numpy.percentile(obs_rlen, 95)
    p99 = numpy.percentile(obs_rlen, 99)
    p100 = numpy.percentile(obs_rlen, 100)

    f.write("count: " + str(count) + "\n")
    f.write("max size " + str(max_rlen) + ", request #: " +
            str(obs_rlen.index(max_rlen)) + "\n")
    f.write("min size: " + str(min_rlen) + ", request #: " +
            str(obs_rlen.index(min_rlen)) + "\n")
    f.write("avg size: " + str(avg) + "\n")
    f.write("std dev: " + str(std) + "\n")
    f.write('50p: %5.3f\n' % p50)
    f.write('90p: %5.3f\n' % p90)
    f.write('95p: %5.3f\n' % p95)
    f.write('99p: %5.3f\n' % p99)
    f.write('100p: %5.3f\n\n' % p100)
    f.write('Statistics summary: count / average / stddev / max / min / 50p / 90p / 95p / 99p / 100p:\n')
    f.write('%d / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f / %5.3f\n\n' %
            (count, avg, std, max_rlen, min_rlen, p50, p90, p95, p99, p100))

    
    f.write('------------------- Throughput -------------------\n')
    total_time = sum(obs_rtime)

    f.write('Total time of requests (sum of requests latency): '+ str(total_time) + '\n')

    throughput1 = count/(total_time/(num_clients*1000))

    f.write('Throughput (total time as sum of requests latency): '+ str(throughput1) + '\n')

    if total_exp_time_ms != -1:
        
        f.write('Total time of requests (total_client_time): '+ str(total_exp_time_ms) + '\n')
        throughput2 = count/(total_exp_time_ms/1000)
        f.write('Throughput (total client time): '+ str(throughput2) + '\n')


    
    f.write("\n")
    if print_raw:
        f.write('------------------- Individual Observations  -------------------\n')
        for o in oarr:
            f.write("[" + str(o.timestamp) + "]: " + o.url +
                    ", status = " + str(o.status) +
                    ", rsize = " + str(o.rlen) +
                    ", rtime = " + str(o.rtime) + "\n")
    f.close()


#### graph plotting utilities ####

def plot_histogram_png(data, numbins, attr, fname):
    if len(oarr) <= 0:
        print("error")
        return

    obj = oarr[0]
    if hasattr(obj, attr) == False:
        print("attr error")
        return

    data = [getattr(o, attr) for o in oarr]
    plt.hist(data, numbins)
    plt.savefig(fname + '.png', dpi=None, facecolor='w', edgecolor='w')
    plt.clf()
    plt.cla()
    plt.close()


# plot one or more histograms on same axis (non-overlapping, but no legend printed)
def plot_hist_png_from_raw(fname, data, numbins, title, xlabel, ylabel):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.hist(data, numbins)
    plt.legend(loc="upper right")
    plt.savefig(fname + '.png', dpi=None, facecolor='w', edgecolor='w')
    plt.clf()
    plt.cla()
    plt.close()

# plot multiple histograms on same axis (overlapping)


def plot_multiple_hist_from_raw(fname, data, numbins, title, xlabel, ylabel):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    res = []
    for i in range(1, len(data)):
        res.append(plt.hist(data[i], numbins, alpha=0.5,
                            #             cumulative=True,
                            label="req" + str(data[0][i - 1])))

#     plt.hist(data[1:len(data)], numbins, rwidth=5, alpha=0.5,
#         label=[ "req" + str(data[0][i]) for i in range(0, len(data[0])) ])

    plt.legend(loc="upper right")
    plt.savefig(fname + ".png", dpi=None)
    plt.clf()
    plt.cla()
    plt.close()

# plot frequency lines on same axis (overlapping)


def plot_multiple_hist_lines_from_raw(fname, data, numbins, title, xlabel, ylabel):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    res = []
    stats = []
    for i in range(1, len(data)):
        hist, bin_edges = numpy.histogram(data[i], bins=numbins)
        bin_mids = [(bin_edges[i] + bin_edges[i + 1]) / 2 for i in range(0, len(bin_edges) - 1)]
        stats.append([hist, bin_mids])

    xrow = stats[0][1]
    x = list(range(len(xrow)))
    plt.xticks(x, xrow, rotation=45)

#     xnew = numpy.linspace(numpy.amin(xrow), numpy.amax(xrow), len(xrow), endpoint=True)
#     print xnew
    for i in range(1, len(data)):
        #         f = interp1d(x, stats[i-1][0])
        #         f2 = interp1d(x, stats[i-1][0], kind='cubic')
        #         print stats[i-1][0]
        #         print f2(xnew)
        #         plt.plot(x, stats[i-1][0], xnew, f2(xnew), '--')
        plt.plot(x, stats[i - 1][0], label="req" + str(data[0][i - 1]))

    plt.legend(loc="upper right")
    plt.savefig(fname + ".png", dpi=None)
    plt.clf()
    plt.cla()
    plt.close()

# plot multiple histograms as bubble plot on y axis, request IDs on x axis


def plot_multiple_hist_points_from_raw(fname, data, numbins, title, xlabel, ylabel):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    stats = []
    for i in range(1, len(data)):
        hist, bin_edges = numpy.histogram(data[i], bins=numbins)
        bin_mids = [(bin_edges[i] + bin_edges[i + 1]) / 2 for i in range(0, len(bin_edges) - 1)]
        stats.append([hist, bin_mids])

    xrow = data[0]
    x = list(range(len(xrow) + 1))
    plt.xticks(x, xrow)
    yrow = stats[0][1]

    xformed_hists = []
    xformed_hists.append([stats[r][0] for r in range(0, len(stats))])
    xformed_hists_v = numpy.vstack(xformed_hists).T

    for i in range(0, len(xformed_hists_v)):
        for j in range(0, len(xrow)):
            xelem = xrow[j]
            yelem = yrow[i]
            msize = xformed_hists_v[i][j] * 1000
            plt.plot(xelem, yelem, marker='.', markersize=msize)

    plt.legend(loc="upper right")
    plt.savefig(fname + ".png", dpi=None)
    plt.clf()
    plt.cla()
    plt.close()

# rows[0] -- x axis
# rows[1] -- y axis


def plot_bar_png(rows, fname, title='Latency distribution single request'):

    width = 0.5
    fig_width = 15
    fig_height = 10
    plt.figure(figsize=(fig_width, fig_height))

    xrow = rows[0]
    yrow = rows[1]
    x = list(range(len(xrow)))

    plt.title(title)
    plt.xlabel('Latencies (ms)')
    plt.ylabel('Frequency')

    plt.xticks(x, xrow, rotation=45)
    plt.bar(x, yrow, width, color='r')
    plt.savefig(fname + ".png", dpi=None, facecolor='w', edgecolor='w')
    plt.clf()
    plt.cla()
    plt.close()

# data[0]:  histogram mid points
# data[1..n]: histogram data for n requests


def plot_multiple_bar_png(fname, data, title, xlabel, ylabel, legend_prefix='req'):
    opacity = 1.0
    margin = 0.5  # gap between two adjacent bar clusters
#     max_bar_width = 0.5
#     actual_bar_width = 0.1
#     half_width = max_bar_width/2

    fig_width = 15
    fig_height = 10
    plt.figure(figsize=(fig_width, fig_height))
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    bin_mids = data[0]
    num_ticks = len(bin_mids)
    x = list(range(num_ticks))
    plt.xticks(x, bin_mids, rotation=45)  # xtics for the histogram mid points

    num_reqs = len(data) - 1

    xtick_dist = (float(fig_width) / (num_ticks)) + margin
    req_bar_width = (xtick_dist - margin) / num_reqs
    req_half_width = req_bar_width / 2
#     print "max bw: " + str(max_bar_width) + \
#     ", act bw: " + str(actual_bar_width) +  \
#     ", xtick dist: " + str(xtick_dist) +    \
#     ", req bw: " + str(req_bar_width)

    # prepare positions for xtics of each request around the main histogram xtic
    xticks = []
    lreq = num_reqs - 1
    for i in range(num_reqs):
        #         xticks_i = [ (x[j] - (lreq*half_width) + (i*actual_bar_width))    \
        xticks_i = [(x[j] - (lreq * req_half_width) + (i * req_bar_width))
                    for j in range(len(x))]
#         print xticks_i
        xticks.append(xticks_i)

    # plot histogram for each request
    for i in range(1, len(data)):
        xticks_i = xticks[i - 1]
        hist = data[i]
        ckey_iter = get_color_index(i - 1)
#         res = plt.bar(xticks_i, hist, actual_bar_width,  \
        res = plt.bar(xticks_i, hist, req_bar_width,
                      alpha=opacity,
                      label=legend_prefix + str(i),
                      color=ckey_iter)

    plt.legend(loc="upper right")
    plt.savefig(fname + '.png', dpi=None, facecolor='w', edgecolor='w')
    plt.clf()
    plt.cla()
    plt.close()


# rows[0] -- x-axis
# rows[1-n] -- y-axis
def plot_line_png(fname, rows, title, xlabel, ylabel, p95arr, p99arr):
    nrows = len(rows)
    xrow = [int(rows[0][c]) for c in range(0, len(rows[0]))]
    x = list(range(len(xrow)))

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.xticks(x, xrow)
    for i in range(1, nrows):
        #         plt.plot(x, rows[i], marker='.')
        plt.plot(x, rows[i], marker='.', linestyle='')
#         plt.plot(x, rows[i], marker='.', linestyle='-')

    if p95arr:
        plt.plot(x, p95arr, marker='.', linestyle='-')
    if p99arr:
        plt.plot(x, p99arr, marker='.', linestyle='-')

    plt.savefig(fname + ".png", dpi=None, facecolor='w', edgecolor='w')
    plt.clf()
    plt.cla()
    plt.close()
