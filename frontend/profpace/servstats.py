#!/usr/bin/python

import sys
import os
import numpy
import datetime
import time
from observations import observation
from stats import *

observations = []


def check_client_request(buf_arr, serverip, clientip):
    src_ip = buf_arr[4]
    dst_ip = buf_arr[6]
    if (src_ip == clientip and dst_ip == serverip):
        return True

    return False


def check_server_response(buf_arr, serverip, clientip):
    src_ip = buf_arr[4]
    dst_ip = buf_arr[6]
    if (src_ip == serverip and dst_ip == clientip):
        return True

    return False


def check_is_https_msg_type(buf_arr, serverip, clientip, mtype):
    if mtype == 0:
        is_server_response = check_server_response(buf_arr, serverip, clientip)
        if is_server_response == False:
            return False
    else:
        is_client_request = check_client_request(buf_arr, serverip, clientip)
        if is_client_request == False:
            return False

    ctype = buf_arr[-1].split(",")
    if (len(ctype) == 0 or ctype[0] == ""):
        return True

    if int(ctype[0]) == 23:
        return True

    return False


def get_msg_seqack(buf_arr):
    seqno = buf_arr[1]
    ackno = buf_arr[2]
    return seqno, ackno


def get_server_response_seqack(buf_arr, serverip, clientip):
    is_server_response = check_server_response(buf_arr, serverip, clientip)
    if is_server_response == False:
        return "", ""

    return get_msg_seqack(buf_arr)


def get_https_msg_seqack(buf_arr, serverip, clientip, mtype):
    is_valid_msg = check_is_https_msg_type(buf_arr, serverip, clientip, mtype)
    if is_valid_msg == False:
        return "", ""

    return get_msg_seqack(buf_arr)


def add_first_response(rows, serverip, clientip, oarr):
    for i in range(0, len(rows)):
        req = rows[i]
        req_arr = req.split("\t")
        cseq, cack = get_https_msg_seqack(req_arr, serverip, clientip, 1)
        if cseq == "" and cack == "":
            continue

        # for a response that is split into multiple network packets,
        # we use the latency of the first packet from the request
        # this is because the latency of the first packet is likely to
        # be most closely related with the application's execution on
        # a secret input. latencies for subsequent packets may have
        # additional noise due to network congestion, etc.
#         for j in range(i+1, len(rows)):
        j = i + 1
        if j >= len(rows):
            break

        rsp = rows[j]
        resp_arr = rsp.split("\t")
        sseq, sack = get_https_msg_seqack(resp_arr, serverip, clientip, 0)
        if sseq == "" and sack == "":
            continue

        assert(sseq == cack)
#         print str(i) + ", " + str(j) + ", seq: " + str(sseq) + ", ack: " + str(sack)

        obs = observation("", i, j, -1, 0, 0.0, 0.0)
        oarr.append(obs)
#             break
    return


def add_last_response(rows, serverip, clientip, oarr):
    for i in range(0, len(rows)):
        req = rows[i]
        req_arr = req.split("\t")
        cseq, cack = get_https_msg_seqack(req_arr, serverip, clientip, 1)
        if cseq == "" and cack == "":
            continue

        j = i + 1
        if j >= len(rows):
            break

        rsp = rows[j]
        resp_arr = rsp.split("\t")
        sseq, sack = get_https_msg_seqack(resp_arr, serverip, clientip, 0)
        assert(sseq != "" and sack != "")

        assert(sseq == cack)
        prev_ack = sack

        for j in range(i + 2, len(rows)):
            rsp = rows[j]
            resp_arr = rsp.split("\t")
            sseq, sack = get_https_msg_seqack(resp_arr, serverip, clientip, 0)
            if (sack != prev_ack):
                break

        if (sack == prev_ack and j == (len(rows) - 1)):
            j += 1

        if (sack != prev_ack or j >= len(rows)):
            #             last_rsp = rows[j-1]
            #             last_resp_arr = last_r.split("\t")
            #             print str(i) + ", " + str(j-1) + ", seq: " + str(sseq) + ", ack: " + str(sack)
            obs = observation("", i, j - 1, -1, 0, 0.0, 0.0)
            oarr.append(obs)
            i = j - 1

    return


def compute_response_time(rows, serverip, clientip, oarr):
    for oiter in range(0, len(oarr)):
        reqid = oarr[oiter].reqid
        rspid = oarr[oiter].rspid
        assert(reqid + 1 <= rspid)
        req_arr = rows[reqid].split("\t")
        resp_arr = rows[rspid].split("\t")
        reqtime_str = req_arr[3].split(" ")
        resptime_str = resp_arr[3].split(" ")
        reqms = converttime(reqtime_str[-2])
        rspms = converttime(resptime_str[-2])
#         reqms = converttime(reqtime_str[3])
#         rspms = converttime(resptime_str[3])
        lat = rspms - reqms
        oarr[oiter].set_time(reqms, lat)
#         print str(reqid) + ", " + str(rspid) + ", lat: " +  \
#         str(oarr[oiter].rtime) + ", size: " +    \
#         str(oarr[oiter].rlen)

    return


def compute_response_size(rows, serverip, clientip, oarr):
    for oiter in range(0, len(oarr)):
        total_resp_size = 0
        reqid = oarr[oiter].reqid
        rspid = oarr[oiter].rspid
        assert(reqid + 1 <= rspid)
        for i in range(reqid + 1, rspid + 1):
            resp_arr = rows[i].split("\t")
            total_resp_size += int(resp_arr[-2])
        oarr[oiter].set_len(total_resp_size)
#         print str(reqid) + ", " + str(rspid) + ", lat: " +  \
#         str(oarr[oiter].rtime) + ", size: " +    \
#         str(oarr[oiter].rlen)

    return


def get_unique_port_ids(statsfile, serverip, clientip):
    ports = []
    statsf = open(statsfile, 'r')
    for line in statsf:
        line = line.strip("\n")
        row = line.split("\t")
        is_valid_serv_msg = check_server_response(row, serverip, clientip)
        is_valid_client_msg = check_client_request(row, serverip, clientip)
        if is_valid_serv_msg == is_valid_client_msg:  # both should not be true or false together
            continue

        if is_valid_client_msg:
            ports.append(row[-5])
        if is_valid_serv_msg:
            ports.append(row[-3])
    ports_unique = list(set(ports))
    statsf.close()
#     print ports
#     print ports_unique
    return ports_unique


def filter_msges_by_port_id(statsfile, serverip, clientip, port_id):
    reqarr = []
    statsf = open(statsfile, 'r')
    for line in statsf:
        line = line.strip("\n")
        row = line.split("\t")
        is_valid_serv_msg = check_server_response(row, serverip, clientip)
        is_valid_client_msg = check_client_request(row, serverip, clientip)
        if is_valid_serv_msg == is_valid_client_msg:  # both should not be true or false together
            continue

        if is_valid_client_msg:
            if row[-5] != port_id:
                continue
            reqarr.append(line)
            ctype = row[-1].split(",")
            if len(ctype) and ctype[0] != "" and int(ctype[0]) == 21:
                statsf.close()
                return reqarr

        if is_valid_serv_msg:
            if row[-3] != port_id:
                continue
            reqarr.append(line)
            ctype = row[-1].split(",")
            if len(ctype) and ctype[0] != "" and int(ctype[0]) == 21:
                statsf.close()
                return reqarr
    statsf.close()
    return reqarr


def get_requests_per_session_array(statsfile, serverip, clientip):
    reset = 1
    init = 1
    reqarr = []
    arr_reqarr = []
    # debugging info
    cnt = 0
    reqs_per_session = 0
    rps_arr = []

    statsf = open(statsfile, "r")
    for line in statsf:
        line = line.strip("\n")
        row = line.split("\t")
        if (int(row[1]) == 1 and int(row[2]) == 1):
            if init == 0:
                cnt += 1

            if reset == 1 and init == 0:
                reset = 0
            else:
                reqarr.append(line)
                reqs_per_session += 1

            if init == 1:
                init = 0

        else:
            reqarr.append(line)
            reqs_per_session += 1

        if reset == 0:
            arr_reqarr.append(reqarr)
            reqarr = []
            reqarr.append(line)
            reset = 1
            rps_arr.append(reqs_per_session)
#             print "sess: " + str(cnt) + ", rps: " + str(reqs_per_session)
            reqs_per_session = 1  # account for the first 1,1 seqack that we already consumed

    cnt += 1  # last request needs to be counted
    arr_reqarr.append(reqarr)
    rps_arr.append(reqs_per_session)
#     print "sess: " + str(cnt) + ", rps: " + str(reqs_per_session)
    statsf.close()

    print(("# reqs: " + str(cnt) + ", total reqs: " + str(numpy.sum(rps_arr))))
    return arr_reqarr


def compute_server_stats_static_page(outdir, statsf_name, serverip, clientip,
                                     reqs_per_session, nreq=0):
    statsfile = outdir + "/" + statsf_name
    rawf = statsfile + ".raw.txt"
    rawf_time = statsfile + ".rawf_time.txt"
    rawl_time = statsfile + ".rawl_time.txt"
    rawf_size = statsfile + ".raw_size.txt"

    arr_reqarr = get_requests_per_session_array(statsfile, serverip, clientip)

    if reqs_per_session == 1:
        cnt = 0
        f_obs_arr = []
        l_obs_arr = []
        for rarr in arr_reqarr:
            #            print "req# " + str(cnt)
            f_obs = []
            add_first_response(rarr, serverip, clientip, f_obs)
            l_obs = []
            add_last_response(rarr, serverip, clientip, l_obs)

            compute_response_time(rarr, serverip, clientip, f_obs)
            compute_response_time(rarr, serverip, clientip, l_obs)
            compute_response_size(rarr, serverip, clientip, l_obs)

            f_obs_arr.append(f_obs)
            l_obs_arr.append(l_obs)
#            if cnt == 0:
#                for f in f_obs:
#                    print str(f.reqid) + ", " + str(f.rspid)
            cnt += 1
#            print "req# " + str(cnt) + ", len fobs: " + str(len(f_obs)) + ", len lobs: " + str(len(l_obs))

        if nreq != 0:
            #            workload_size = nreq
            #            num_files = workload_size
            workload_size = [int(nreq[i]) for i in range(0, len(nreq))]
            num_files = sum(workload_size)
            print_server_raw_rows(list(range(num_files)), f_obs_arr, 'rtime',
                                  workload_size, rawf_time)
            print_server_raw_rows(list(range(num_files)), l_obs_arr, 'rtime',
                                  workload_size, rawl_time)
            print_server_raw_rows(list(range(num_files)), l_obs_arr, 'rlen',
                                  workload_size, rawf_size)
        else:
            workload_size = 1
            arr_oarr = []
            assert(len(f_obs_arr) == len(l_obs_arr))
            for i in range(0, len(f_obs_arr)):
                fit = f_obs_arr[i]
                lit = l_obs_arr[i]
    #            print str(i) + ": " + str(len(fit)) + ", " + str(len(lit))
                assert(len(fit) == len(lit))
                for j in range(0, len(fit)):
                    arr_oarr.append([fit[j], lit[j]])
            print_server_raw(arr_oarr, rawf)
    else:
        f_obs_arr = []
        l_obs_arr = []
        num_files = nreq if nreq != 0 else 1
        workload_size = [num_files]
#        workload_size = [ nreq if nreq != 0 else 1 ]
        split = len(arr_reqarr) / num_files
        for i in range(0, split):
            #            rarr = arr_reqarr[ (i*workload_size) : ((i+1)*workload_size) ]
            rarr = []
            split_range_start = i * num_files
            split_range_end = (i + 1) * num_files
            for j in range(split_range_start, split_range_end):
                rarr += arr_reqarr[j]
            f_obs = []
            add_first_response(rarr, serverip, clientip, f_obs)
            l_obs = []
            add_last_response(rarr, serverip, clientip, l_obs)

            compute_response_time(rarr, serverip, clientip, f_obs)
            compute_response_time(rarr, serverip, clientip, l_obs)
            compute_response_size(rarr, serverip, clientip, l_obs)

            f_obs_arr.append(f_obs)
            l_obs_arr.append(l_obs)

        if nreq != 0:
            print_server_raw_rows(list(range(num_files)), f_obs_arr, 'rtime',
                                  workload_size, rawf_time)
            print_server_raw_rows(list(range(num_files)), l_obs_arr, 'rtime',
                                  workload_size, rawl_time)
            print_server_raw_rows(list(range(num_files)), l_obs_arr, 'rlen',
                                  workload_size, rawf_size)
        else:
            arr_oarr = []
            assert(len(f_obs_arr) == len(l_obs_arr))
            for i in range(0, len(f_obs_arr)):
                fit = f_obs_arr[i]
                lit = l_obs_arr[i]
                assert(len(fit) == len(lit))
                for j in range(0, len(fit)):
                    arr_oarr.append([fit[j], lit[j]])
            print_server_raw(arr_oarr, rawf)


def compute_server_stats_multiple_objects(outdir, statsf_name, serverip, clientip, nreq=0):
    statsfile = outdir + "/" + statsf_name
    rawf = statsfile + ".raw.txt"
    rawf_time = statsfile + ".rawf_time.txt"
    rawl_time = statsfile + ".rawl_time.txt"
    rawf_size = statsfile + ".raw_size.txt"

    arr_reqarr = []
    ports_arr = get_unique_port_ids(statsfile, serverip, clientip)
    cnt = 0
    for p in ports_arr:
        reqarr = filter_msges_by_port_id(statsfile, serverip, clientip, p)
        arr_reqarr.append(reqarr)
#         if cnt < 2:
#             print reqarr
        cnt += 1

    cnt = 0
    f_obs_arr = []
    l_obs_arr = []
    for rarr in arr_reqarr:
        f_obs = []
        add_first_response(rarr, serverip, clientip, f_obs)
        l_obs = []
        add_last_response(rarr, serverip, clientip, l_obs)

        compute_response_time(rarr, serverip, clientip, f_obs)
        compute_response_time(rarr, serverip, clientip, l_obs)
        compute_response_size(rarr, serverip, clientip, l_obs)

        f_obs_arr.append(f_obs)
        l_obs_arr.append(l_obs)
#         print "req# " + str(cnt) + ", len fobs: " + str(len(f_obs)) + ", len lobs: " + str(len(l_obs))
        cnt += 1

    if nreq != 0:
        workload_size = nreq
        print_server_raw_rows(list(range(workload_size)), f_obs_arr, 'rtime',
                              workload_size, rawf_time)
        print_server_raw_rows(list(range(workload_size)), l_obs_arr, 'rtime',
                              workload_size, rawl_time)
        print_server_raw_rows(list(range(workload_size)), l_obs_arr, 'rlen',
                              workload_size, rawf_size)
    else:
        workload_size = 1
        arr_oarr = []
        assert(len(f_obs_arr) == len(l_obs_arr))
        for i in range(0, len(f_obs_arr)):
            fit = f_obs_arr[i]
            lit = l_obs_arr[i]
#            print str(i) + ": " + str(len(fit)) + ", " + str(len(lit))
            assert(len(fit) == len(lit))
            for j in range(0, len(fit)):
                arr_oarr.append([fit[j], lit[j]])
        print_server_raw(arr_oarr, rawf)
