/*
 * dummy_msges.c
 *
 * created on: Apr 23, 2017
 * author: aasthakm
 *
 * some dummy messages for client server traffic
 * TODO: link with application
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include <arpa/inet.h>

#include <msg_hdr.h>
#include <profile_conn_format.h>
#include "profile_conn_msg.h"
#include "dummy_profile.h"
#include "dummy_msges.h"

#define SIZE_MTU 1460

void
prepare_client_addr_msg(char **buf, int *buf_len, struct sockaddr_in *caddr)
{
  uint16_t off = 0;
  uint64_t *msg_len = NULL;
  int prof_len = MSG_HDR_LEN + sizeof(uint32_t) + sizeof(uint16_t);
  char *p = (char *) malloc(prof_len+1);
  memset(p, 0, prof_len+1);

  init_msg(p, MSG_T_IPPORT, &msg_len);
  off += MSG_HDR_LEN;
  ((uint32_t *) (p + off))[0] = caddr->sin_addr.s_addr;
  off += sizeof(uint32_t);
  ((uint16_t *) (p + off))[0] = caddr->sin_port;
  off += sizeof(uint16_t);
  *msg_len = off-MSG_HDR_LEN;

  *buf = p;
  *buf_len = prof_len;
}

void
prepare_multi_mtu_msg(char **buf, int *buf_len, int n_mtu)
{
  uint16_t off = 0;
  uint64_t *msg_len = NULL;
  int data_len = n_mtu * SIZE_MTU;
  int prof_len = MSG_HDR_LEN + sizeof(uint16_t) + data_len;
  char *p = (char *) malloc(prof_len+1);
  memset(p, 0, prof_len+1);

  init_msg(p, MSG_T_DATA, &msg_len);
  off += MSG_HDR_LEN;
  ((uint16_t *) (p + off))[0] = n_mtu;
  off += sizeof(uint16_t);
  memset(p+off, 'a', data_len);
  off += data_len;
  *msg_len = off-MSG_HDR_LEN;

  *buf = p;
  *buf_len = prof_len;
}

void
prepare_ping_msg(char **buf, int *buf_len, int n_mtu, double time)
{
  uint16_t off = 0;
  uint64_t *msg_len = NULL;
  int data_len = n_mtu * SIZE_MTU;
  int prof_len = MSG_HDR_LEN + 2*sizeof(double) + data_len;
  char *p = (char *) malloc(prof_len+1);
  memset(p, 0, prof_len+1);

  init_msg(p, MSG_T_DATA, &msg_len);
  off += MSG_HDR_LEN;
  ((double *) (p + off))[0] = time;
  off += sizeof(double);
  *msg_len = prof_len-MSG_HDR_LEN;

  *buf = p;
  *buf_len = prof_len;
}

void
prepare_non_mtu_msg(char **buf, int *buf_len, int data_len)
{
  uint16_t off = 0;
  uint64_t *msg_len = NULL;
  int prof_len = MSG_HDR_LEN + sizeof(uint16_t) + data_len;
  char *p = (char *) malloc(prof_len+1);
  memset(p, 0, prof_len+1);

  init_msg(p, MSG_T_DATA, &msg_len);
  off += MSG_HDR_LEN;
  ((uint16_t *) (p + off))[0] = data_len;
  off += sizeof(uint16_t);
  memset(p+off, 'a', data_len);
  off += data_len;
  *msg_len = off-MSG_HDR_LEN;

  *buf = p;
  *buf_len = prof_len;
}
