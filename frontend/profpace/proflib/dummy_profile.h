/*
 * dummy_profile.h
 *
 * created on: Mar 28, 2017
 * author: aasthakm
 *
 * small randomly generated profile serialized into a buffer
 */

#ifndef __DUMMY_PROFILE_H__
#define __DUMMY_PROFILE_H__

#define MAX_FILENAME_LEN 512

#include "../include/profile_conn_format.h"

void create_tmp_profile(char **buf, int *buf_len, char *server_ip, int server_port,
    char *client_ip, int client_port, int client_sock, char *fname, int read);

void create_tmp_profile_id2(char **buf, int *buf_len, char *server_ip,
    int server_port, char *client_ip, int client_port, int client_sock,
    uint16_t prof_id);

void create_tmp_profile_map(char **buf, int *buf_len, char *ip1, int port1,
    char *ip2, int port2, char *ip3, int port3, char *ip4, int port4,
    int cilent_sock, uint16_t n_prof, char *fname, int read);
void create_tmp_profile_id(char **buf, int *buf_len, char *ip1, int port1,
    char *ip2, int port2, char *ip3, int port3, char *ip4, int port4,
    int client_sock, int prof_id);
void create_tmp_profile_marker_h64(char **buf, int *buf_len,
    char *src_ip, int src_port, char *in_ip, int in_port,
    char *out_ip, int out_port, char *dst_ip, int dst_port,
    uint32_t num_pub, uint32_t num_priv,
    ihash_t *pubhash, ihash_t *privhash);

void create_profile_map(char **buf, int *buf_len,
    char *ip1, uint16_t port1, char *ip2, uint16_t port2,
    char *ip3, uint16_t port3, char *ip4, uint16_t port4,
    uint16_t n_prof, uint16_t *profile_id, uint16_t *num_out_reqs,
    uint16_t **num_extra_slots, uint64_t **num_out_frames,
    uint64_t **spacing, uint64_t **first_frame_latency);

void create_profile_map_ihash(char **buf, int *buf_len,
    char *ip1, uint16_t port1, char *ip2, uint16_t port2,
    char *ip3, uint16_t port3, char *ip4, uint16_t port4,
    uint16_t n_prof, char **profile_id, uint16_t *num_out_reqs,
    uint16_t **num_extra_slots, uint64_t **num_out_frames,
    uint64_t **spacing, uint64_t **first_frame_latency);


void print_char(char c);

void free_buf(char *buf, int buf_len);
#endif /* __DUMMY_PROFILE_H__ */
