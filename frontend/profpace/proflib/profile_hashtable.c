/**
 * File  : profile_hashtable.c
 * Author: Mohamed Alzayat <alzayat@mpi-sws.org>
 * Date  : 30.03.2018
 */

#include "profile_hashtable.h"

#include "../include/profile_conn_format.h"
#include "profile_conn_msg.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

/*Cretes and initializes a new hashtable
 * returns a hashtable struct
 *
 */
hashtable_t init_profiles_hashtable() {
  hashtable_t hashtable;
  hashtable.size = (hashtable_key_t)(((hashtable_key_t)1 << HT_KEY_SIZE) - 1);
  hashtable.n_prof = 0;
#if DBG_LVL <= LVL_INFO
  printf("Created a Hashtable of SIZE: %" PRINT_KEY " \n", hashtable.size);
#endif

  hashtable.next_offset = PROFILE_MAP_HDR_IP_PORT_HASHTABLE_LEN_BYTES;
  hashtable.memory_limit =
      (hashtable_offset_t)(
          ((hashtable_offset_t)1 << (HT_OFFSET_SIZE - 1)) +
          (((hashtable_offset_t)1 << (HT_OFFSET_SIZE - 1)) - 1)) -
      PROFILE_MAP_HDR_IP_PORT_HASHTABLE_LEN_BYTES;
#if DBG_LVL <= LVL_INFO
  printf("INIT PROFILE PROFILES MAP header + constant size: %" PRINT_OFFSET
         "\n",
         PROFILE_MAP_HDR_IP_PORT_HASHTABLE_LEN_BYTES);
  printf("INIT PROFILE PROFILES MAP HT OFFSETS SIZE: %" PRINT_OFFSET "\n",
         PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  printf("INIT PROFILE PROFILES MEM LIMIT: %" PRINT_OFFSET "\n",
         hashtable.memory_limit);
#endif

  hashtable.hashtable_offsets =
      (hashtable_offset_t *)malloc(PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  memset(hashtable.hashtable_offsets, 0, PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  return hashtable;
}

/* Maybe requiresd eventually?
 */
hashtable_offset_t set_initial_offset(hashtable_t *ht,
                                      hashtable_offset_t init_offset) {
  ht->next_offset = init_offset;
  return ht->next_offset;
}

/* The hash function used for hashing and lookup
 */
hashtable_key_t hash_pid(ihash_t pid) {
  // http://www.cse.yorku.ca/~oz/hash.html
  hashtable_key_t hash = 7; // 381;
  int c = 0;

  for (c; c < sizeof(pid.byte); c++)
    hash = ((hash << 5) + hash) + pid.byte[c]; /* hash * 33 + c */

#if DBG_LVL <= LVL_DBG
  printf("HASH of the PID is %" PRINT_KEY "\n", hash);
#endif
  return hash;
}

/* Maybe requiresd eventually, to abide to an MMAP limit
 */
hashtable_t set_hashtable_memory_limit(hashtable_t *ht,
                                       hashtable_offset_t max_memory) {
  ht->memory_limit = max_memory;
}

/* Insert the profile into a hashtable (doesn't affect the MMAP
 */
hashtable_offset_t add_profile_to_hashtable(hashtable_t *hashtable, ihash_t pid,
                                            int p_len) {

  if (hashtable->memory_limit - hashtable->next_offset < p_len) {
    printf("\n[ERROR] Maximum memory limit reached, please consider increasing "
           "the HT_OFFSET_SIZE\n");
    return -2;
  }
  char hbuf[64];
  int hbuf_len = 64;
  memset(hbuf, 0, hbuf_len);
  prt_hash(pid.byte, sizeof(ihash_t), hbuf);

  hashtable_key_t hash = hash_pid(pid);

#if DBG_LVL <= LVL_INFO
  printf("Hash of profile (%s) shoule ba at entry: %" PRINT_KEY " \n", hbuf,
         hash);
  printf("Hashtable -> hashtable_offsets[%" PRINT_KEY "] = %" PRINT_OFFSET
         " \n",
         hash, hashtable->hashtable_offsets[hash]);
#endif

  if (hashtable->hashtable_offsets[hash] != 0) {
#if DBG_LVL <= LVL_INFO
    printf("[WARN] Hash collision detected for pid (hex): %s \n", hbuf);
    printf("Hash collision at entry: %" PRINT_KEY " \n", hash);
#endif
    // Returning -1, you should fetch the offset and start chaining!
    return -1;
  }
  hashtable->hashtable_offsets[hash] = hashtable->next_offset;

// TODO: Actually insert the profile

#if DBG_LVL <= LVL_INFO
  printf("Inserted profile at offset: %" PRINT_OFFSET " \n",
         hashtable->next_offset);
#endif

  hashtable_offset_t result = hashtable->next_offset;
  hashtable->next_offset += p_len;

  hashtable->n_prof += 1;
  return result;
}

void deinit_hashtable(hashtable_t *ht) { free(ht->hashtable_offsets); }

/* Get offset of the first element of the chain */
hashtable_offset_t get_offset(hashtable_t *hashtable, ihash_t pid) {

  hashtable_key_t hash = hash_pid(pid);
  return hashtable->hashtable_offsets[hash];
}

/*If we add a chained profile we need to update the offset of the next entry*/
hashtable_offset_t add_profile_offset(hashtable_t *hashtable, ihash_t pid,
                                      hashtable_offset_t p_len) {
  if (hashtable->memory_limit - hashtable->next_offset < p_len) {
    printf("[ERROR] Maximum memory limit reached, please consider increasing "
           "the HT_OFFSET_SIZE\n");
    return -2;
  }
  hashtable_offset_t result = hashtable->next_offset;
  hashtable->next_offset += p_len;

  hashtable->n_prof += 1;
#if DBG_LVL <= LVL_INFO
  printf("Updatet next profile offset to be: %" PRINT_OFFSET "\n",
         hashtable->next_offset);
#endif
  return result;
}

void printf_hashtable(hashtable_t *ht) {

  printf("Hashtable HT of size (based on key length): %d, offsets length is: "
         "%d \n",
         ht->size, HT_OFFSET_SIZE);
  hashtable_offset_t i = 0;
  for (i; i < ht->size; i++) {
    printf("HT[%" PRINT_OFFSET "] = %" PRINT_OFFSET " \n", i,
           ht->hashtable_offsets[i]);
  }
}
