/*
 * hash_func.c
 *
 * created on: Aug 30, 2017
 * author: aasthakm
 */

#include "hash_func.h"
#include "../include/profile_conn_format.h"

#include <string.h>

#define HASH_SEED 7

void
murmur_hash(void *key, int key_len, int modulo, void *hbuf)
{
  uint64_t idx[2] = { 0, 0 };
  ihash_t *i_hbuf = (ihash_t *) hbuf;

  if (!key || !key_len || !hbuf)
    return;

  MurmurHash3_x64_128((const void *) key, key_len, HASH_SEED, (uint64_t *) idx);
  memcpy(i_hbuf->byte, (void *) idx, 2*sizeof(uint64_t));
}
