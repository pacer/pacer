/**
 * File  : profile_hashtable.h
 * Author: Mohamed Alzayat <alzayat@mpi-sws.org>
 * Date  : 30.03.2018
 */
#include <inttypes.h>
#include "../include/msg_hdr.h"
#include "../include/profile_conn_format.h"
#include "config.h"
#include "profile_conn_msg.h"
#include <math.h>

#define LVL_DBG 0
#define LVL_INFO 1
#define LVL_ERR 2
#define LVL_EXP 3

#define DBG_LVL LVL_INFO

#define HT_KEY_SIZE 8
#define HT_OFFSET_SIZE 16

// Hashtable (HT) key size in bits, a key of 16 bits supports up to 65535
// different profiles
#if HT_KEY_SIZE == 8
typedef uint8_t hashtable_key_t;
#define PRINT_KEY PRIu8
#elif HT_KEY_SIZE == 16
typedef uint16_t hashtable_key_t;
#define PRINT_KEY PRIu16
#elif HT_KEY_SIZE == 32
typedef uint32_t hashtable_key_t;
#define PRINT_KEY PRIu32
#elif HT_KEY_SIZE == 64
typedef uint64_t hashtable_key_t;
#define PRINT_KEY PRIu64
#endif

// HT offset size (in bits) should be more than the keysize if
// HT_AND_DATA_TOGETHER is set to 1
// if HT_AND_DATA_TOGETHER is set to 0, the HT_OFFSET_SIZE can be any value
// The HT offset size sets a limit on the maximum memory size required to
// accomodate all profiles

#if HT_OFFSET_SIZE == 16
typedef uint16_t hashtable_offset_t;
#define PRINT_OFFSET PRIu16
#elif HT_OFFSET_SIZE == 32
typedef uint32_t hashtable_offset_t;
#define PRINT_OFFSET PRIu32
#elif HT_OFFSET_SIZE == 64
typedef uint64_t hashtable_offset_t;
#define PRINT_OFFSET PRIu64
#endif

typedef struct {
  uint16_t cmd;
  hashtable_offset_t length;
} mmap_msg_hdr_t;

#define MMAP_MSG_HDR_LEN sizeof(mmap_msg_hdr_t)

#define init_mmap_msg(buf, MSG, len_p)                                         \
  {                                                                            \
    mmap_msg_hdr_t *h = (mmap_msg_hdr_t *)buf;                                 \
    memset(h, 0, sizeof(mmap_msg_hdr_t));                                      \
    h->cmd = MSG;                                                              \
    *len_p = &h->length;                                                       \
  }

#define get_mmap_msg_args_len(buf, len)                                        \
  {                                                                            \
    msg_hdr_t *h = (mmap_msg_hdr_t *)buf;                                      \
    len = h->length;                                                           \
  }

#define get_mmap_msg_cmd(buf, msg_type)                                        \
  {                                                                            \
    msg_hdr_t *h = (mmap_msg_hdr_t *)buf;                                      \
    msg_type = h->cmd;                                                         \
  }

//#define PROFILE_HASHTABLE_OFFSETS_LEN_BYTES (hashtable_offset_t)
//(((((hashtable_offset_t)1) << HT_KEY_SIZE) -1) * sizeof(HT_OFFSET_SIZE/8))
#define PROFILE_HASHTABLE_OFFSETS_LEN_BYTES                                    \
  (hashtable_offset_t)(((1 << HT_KEY_SIZE) - 1) * (HT_OFFSET_SIZE / 8))
#define PROFILE_HASHTABLE_LEN_BYTES                                            \
  (hashtable_offset_t)(PROFILE_HASHTABLE_OFFSETS_LEN_BYTES +                   \
                       sizeof(hashtable_t))
#define PROFILE_MAP_HDR_IP_PORT_HASHTABLE_LEN_BYTES                            \
  (hashtable_offset_t)(MMAP_MSG_HDR_LEN + PROF_CONN_IP_PORT_HDR_LEN +          \
                       PROFILE_HASHTABLE_LEN_BYTES)

typedef struct hashtable {
  // Size as in number of entries
  hashtable_key_t size;
  // Number of profiles in the hashtable
  int n_prof;

  // Next offset to add a profile to
  hashtable_offset_t next_offset;
  hashtable_offset_t memory_limit;

  // Array of all offsets
  hashtable_offset_t *hashtable_offsets;
} hashtable_t;


hashtable_t init_profiles_hashtable();
hashtable_t set_hashtable_memory_limit(hashtable_t *ht,
                                       hashtable_offset_t max_memory);
hashtable_offset_t add_profile_to_hashtable(hashtable_t *hashtable, ihash_t pid,
                                            int p_len);
void deinit_hashtable(hashtable_t *ht);
hashtable_offset_t get_offset(hashtable_t *hashtable, ihash_t pid);
hashtable_offset_t add_profile_offset(hashtable_t *hashtable, ihash_t pid,
                                      hashtable_offset_t plen);
void printf_hashtable(hashtable_t *ht);
