/**
 * File  : profile_database.c
 * Author: Mohamed Alzayat <alzayat@mpi-sws.org>
 * Date  : 30.03.2018
 */
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "profile_database.h"

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include "../include/conn_common.h"
#include "../include/msg_hdr.h"
#include "../include/profile_conn_format.h"
#include "config.h"
#include "profile_conn_msg.h"

#define MAX_BUF_LEN 4096

#ifndef MAX_BUF_SIZE
#define MAX_BUF_SIZE 512
#endif

/*
 * serialize an array of profile buffers read from file
 */
void update_linked_profile(char **profile, hashtable_offset_t *prof_len,
                           int n_prepared_profiles,
                           hashtable_offset_t profile_offset,
                           hashtable_offset_t new_offset) {

  int i;
  // This offset is to compare with HASHTABLE specified offset
  hashtable_offset_t off = PROFILE_MAP_HDR_IP_PORT_HASHTABLE_LEN_BYTES;

#if DBG_LVL <= LVL_INFO
  printf("[CHECK] First offset of profiles is at: %" PRINT_OFFSET
         ", expected_offset_in the linearized map is: %" PRINT_OFFSET "\n",
         off, profile_offset);
#endif

  // skip over the profiles till we reach the offset of the profile we would
  // like to update with the chaining
  i = 0;
  while (i < n_prepared_profiles && off < profile_offset) {
    off += prof_len[i];
    i++;
  }

  if (off != profile_offset) {
    printf(
        "[ERROR] the mentioned offset doesn't match the beginning of a profile,\
        referenced conflicting profile offset: %" PRINT_OFFSET
        ", computed offset: %" PRINT_OFFSET "\n",
        profile_offset, off);
    return;
  }

  char *p = profile[i];
  char *s_buf;
  int s_buf_off = 0;
  // print_profile_from_buf_ihash_hashtable(s_buf, &s_buf_off, p, prof_len[i]);
  // printf("Parsing the profile: \n %s \n", s_buf);

  off = 0;
  off += sizeof(ihash_t);
  uint16_t num_out_reqs = ((uint16_t *)(p + off))[0];
  off += sizeof(uint16_t);

  p = p + off;

  hashtable_offset_t off2 = 0;

  // number of bursts and number of packets in each burst
  off2 += 2 * num_out_reqs * sizeof(uint16_t);

  // inter packet latencies and first out frame delay
  off2 += 2 * num_out_reqs * sizeof(uint64_t);

  hashtable_offset_t current_offset = ((hashtable_offset_t *)(p + off2))[0];
  if (current_offset == 0) {
    ((hashtable_offset_t *)(p + off2))[0] = new_offset;

#if DBG_LVL <= LVL_INFO
    printf("[INFO] Successfully linked to the new profile, new profile will be "
           "inserted at: %" PRINT_OFFSET "\n",
           new_offset);
#endif
  } else {
#if DBG_LVL <= LVL_INFO
    printf(
        "[INFO] Will do the linking in the chained profile at: %" PRINT_OFFSET
        "\n",
        current_offset);
#endif
    update_linked_profile(profile, prof_len, n_prepared_profiles,
                          current_offset, new_offset);
  }
}

void prepare_n_profile_map_and_hashtable_buf_from_mem(
    char *buf, hashtable_offset_t *buf_len, uint16_t n_profile, uint32_t src_ip,
    uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip, uint16_t src_port,
    uint16_t in_port, uint16_t out_port, uint16_t dst_port, char **profile,
    hashtable_offset_t *profile_len, hashtable_t *ht) {
  int i;
  hashtable_offset_t off = 0;
  hashtable_offset_t off2 = 0;
  hashtable_offset_t total_len = 0;
  char *p = buf;
  char *prof = NULL;

  if (!buf || !buf_len || !n_profile || !profile || !profile_len)
    return;

  ((uint32_t *)(p + off))[0] = src_ip;
  off += sizeof(uint32_t);
  ((uint32_t *)(p + off))[0] = in_ip;
  off += sizeof(uint32_t);
  ((uint32_t *)(p + off))[0] = out_ip;
  off += sizeof(uint32_t);
  ((uint32_t *)(p + off))[0] = dst_ip;
  off += sizeof(uint32_t);

  ((uint16_t *)(p + off))[0] = src_port;
  off += sizeof(uint16_t);
  ((uint16_t *)(p + off))[0] = in_port;
  off += sizeof(uint16_t);
  ((uint16_t *)(p + off))[0] = out_port;
  off += sizeof(uint16_t);
  ((uint16_t *)(p + off))[0] = dst_port;
  off += sizeof(uint16_t);

  // Add the hashtable
  ((hashtable_t *)(p + off))[0] = *ht;
  off += sizeof(hashtable_t);

  // Add hashtable offsets
  memcpy((p + off), (ht->hashtable_offsets),
         PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  off += PROFILE_HASHTABLE_OFFSETS_LEN_BYTES;

#if DBG_LVL <= LVL_DBG
  printf("HT of size: %d going to memory:\n", ht->size);
  printf("MEM WRITE: HT MAX MEM LIMIT: %" PRINT_OFFSET
         " came out from  memory:\n",
         ht->memory_limit);
  printf("MEM WRITE: HT NEXT OFFSET: %" PRINT_OFFSET
         " came out from  memory:\n",
         ht->next_offset);
  printf_hashtable(ht);
  printf("Variable profiles insertion starting at a relative offset of: "
         "%" PRINT_OFFSET " \n",
         off);
#endif

  // Add the profiles buffers
  prof = p + off;
  off2 = 0;
  for (i = 0; i < n_profile; i++) {
    memcpy(prof + off2, profile[i], profile_len[i]);
    off2 += profile_len[i];
  }

  *buf_len = off + off2;

#if 0
  hashtable_t ht_test;// = (hashtable_t *) malloc(sizeof(hashtable_t));
  memcpy(&ht_test, p + PROF_CONN_IP_PORT_HDR_LEN , sizeof(hashtable_t));;
  off+= sizeof(hashtable_t);
  ht_test.hashtable_offsets = (hashtable_offset_t*) malloc(PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  printf("TEST MEM WRITE: HT of size: %"PRINT_KEY" came out from  memory:\n", ht_test.size);
  printf("TEST MEM WRITE: HT MAX MEM LIMIT: %"PRINT_OFFSET" came out from  memory:\n", ht_test.memory_limit);
  printf("TEST MEM WRITE: HT NEXT OFFSET: %"PRINT_OFFSET" came out from  memory:\n", ht_test.next_offset);
  memcpy(ht_test.hashtable_offsets, p + PROF_CONN_IP_PORT_HDR_LEN + sizeof(hashtable_t), PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  printf_hashtable(&ht_test);
#endif
}

void prepare_profile_buf_ihash_hashtable(char *buf, hashtable_offset_t *buf_len,
                                         ihash_t prof_id, uint16_t num_out_reqs,
                                         uint16_t *num_extra_slots,
                                         uint64_t *num_out_frames,
                                         uint64_t *frame_spacing,
                                         uint64_t *first_frame_latency) {
  int i;
  hashtable_offset_t off = 0;
  hashtable_offset_t off2 = 0;
  char *p = NULL;

  off = 0;
  p = buf;
  ((ihash_t *)(p + off))[0] = prof_id;
  off += sizeof(ihash_t);
  ((uint16_t *)(p + off))[0] = num_out_reqs;
  off += sizeof(uint16_t);

  p = p + off;

  off2 = 0;

  for (i = 0; i < num_out_reqs; i++) {
    ((uint16_t *)(p + off2))[0] = num_extra_slots[i];
    off2 += sizeof(uint16_t);
  }
  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *)(p + off2))[0] = num_out_frames[i];
    off2 += sizeof(uint64_t);
  }

  int j;
  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *)(p + off2))[0] = frame_spacing[i];
    off2 += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    ((uint64_t *)(p + off2))[0] = first_frame_latency[i];
    off2 += sizeof(uint64_t);
  }

  // Add the chaining offset, by default it is 0
  p = p + off2;
  ((hashtable_offset_t *)(p))[0] = 0;
  off2 += sizeof(hashtable_offset_t);

  off += off2;
  *buf_len = off;
#if DBG_LVL <= LVL_DBG
  printf("DBG#SME -- [%s:%s:%d] Printing generated buffer\n", __FILE__,
         __func__, __LINE__);
  dbg_prt(buf, (int)*buf_len);
#endif
}
void create_profile_map_ihash_hashtable(
    char **buf, hashtable_offset_t *buf_len, char *ip1, uint16_t port1,
    char *ip2, uint16_t port2, char *ip3, uint16_t port3, char *ip4,
    uint16_t port4, uint16_t n_prof, char **profile_id, uint16_t *num_out_reqs,
    uint16_t **num_extra_slots, uint64_t **num_out_frames, uint64_t **spacing,
    uint64_t **first_frame_latency) {
  if (!buf || !buf_len)
    return;

  if (!ip1 || !ip2 || !ip3 || !ip4)
    return;

  if (!n_prof || !profile_id || !num_out_reqs || !num_extra_slots ||
      !num_out_frames || !spacing || !first_frame_latency)
    return;

#if DBG_LVL <= LVL_DBG
  printf("buf: %p, buf_len: %p, ip1: %s %u, ip2: %s %u, ip3: %s %u, ip4: %s %u"
         ", n_prof: %u, pid: %p, reqs: %p, slots: %p, frames: %p, spacing: %p"
         ", latency: %p\n",
         buf, buf_len, ip1, port1, ip2, port2, ip3, port3, ip4, port4, n_prof,
         profile_id, num_out_reqs, num_extra_slots, num_out_frames, spacing,
         first_frame_latency);
#endif

  struct sockaddr_in addr[4];
  uint32_t u_ip[4];
  uint16_t u_port[4];
  hashtable_offset_t total_msg_len = 0;
  hashtable_offset_t *msg_len = NULL;
  hashtable_offset_t *prof_len = NULL;
  char **prof_buf = NULL;
  int i;

  for (i = 0; i < 4; i++) {
    memset(&addr[i], 0, sizeof(struct sockaddr_in));
  }

  inet_aton(ip1, &addr[0].sin_addr);
  inet_aton(ip2, &addr[1].sin_addr);
  inet_aton(ip3, &addr[2].sin_addr);
  inet_aton(ip4, &addr[3].sin_addr);
  addr[0].sin_port = htons(port1);
  addr[1].sin_port = htons(port2);
  addr[2].sin_port = htons(port3);
  addr[3].sin_port = htons(port4);

  char p_buf[MAX_BUF_SIZE];
  int p_len = 0;

  memset(p_buf, 0, MAX_BUF_SIZE);

  for (i = 0; i < 4; i++) {
    u_ip[i] = addr[i].sin_addr.s_addr;
    u_port[i] = addr[i].sin_port;
#if DBG_LVL <= LVL_DBG
    sprintf(p_buf + p_len, "conn[%d]: %s %u %u\n\t", i,
            inet_ntoa(addr[i].sin_addr), u_ip[i], ntohs(u_port[i]));
    p_len = strlen(p_buf);
#endif
  }

#if DBG_LVL <= LVL_DBG
  printf("[%s:%d] len: %d\n%s\n", __func__, __LINE__, p_len, p_buf);

  for (i = 0; i < n_prof; i++) {
    int j = 0;
    uint16_t n_req = num_out_reqs[i];
    printf("prof id (hex): ");
    char hbuf[64];
    int hbuf_len = 64;
    memset(hbuf, 0, hbuf_len);
    prt_hash((profile_id[i]), sizeof(ihash_t), hbuf);

    printf("\n");
    printf("#req: %u\n", n_req);
    printf("\textra slots: ");
    for (j = 0; j < n_req; j++) {
      printf("%u ", num_extra_slots[i][j]);
    }
    printf("\n");
    printf("\tnum frames: ");
    for (j = 0; j < n_req; j++) {
      printf("%lu ", num_out_frames[i][j]);
    }
    printf("\n");
    printf("\tspacing: ");
    for (j = 0; j < n_req; j++) {
      printf("%lu ", spacing[i][j]);
    }
    printf("\n");
    printf("\tlatency: ");
    for (j = 0; j < n_req; j++) {
      // printf("i is: %i, j is %i\n",i,j);
      printf("%lu ", first_frame_latency[i][j]);
    }
    printf("\n");
  }
#endif

  total_msg_len += PROFILE_MAP_HDR_IP_PORT_HASHTABLE_LEN_BYTES;

  prof_len = (hashtable_offset_t *)malloc(sizeof(uint16_t) * n_prof);
  prof_buf = (char **)malloc(sizeof(char *) * n_prof);
  for (i = 0; i < n_prof; i++) {
    prof_len[i] = PROF_CONN_LEN_IHASH_HASHTABLE(num_out_reqs[i]);
    total_msg_len += prof_len[i];
    prof_buf[i] = (char *)malloc(prof_len[i] + 1);
    memset(prof_buf[i], 0, prof_len[i] + 1);
  }

  char *p = (char *)malloc(total_msg_len + 1);
  memset(p, 0, total_msg_len + 1);
  init_mmap_msg(p, MSG_T_PMAP_IHASH_HASHTABLE, &msg_len);

  hashtable_t profiles_hashtable = init_profiles_hashtable();

  // printf("New clean table\n");
  // printf_hashtable(&profiles_hashtable);
  for (i = 0; i < n_prof; i++) {
    ihash_t pid = ((ihash_t *)profile_id[i])[0];

    hashtable_offset_t offset =
        add_profile_to_hashtable(&profiles_hashtable, pid, prof_len[i]);

#if DBG_LVL <= LVL_INFO
    printf("[CHECK/AddProfile] First offset of profiles is at: %" PRINT_OFFSET
           "\n",
           offset);
#endif
    if (offset == (hashtable_offset_t)-2) {
      printf("[ERROR] MEMORY limit exceeded");
      return;
    }

    if (offset == (hashtable_offset_t)-1) {

#if DBG_LVL <= LVL_INFO
      printf("Hash collision detected, will start chaining\n");
#endif
      // HASH Collision
      // get the offset first profile in the chaining
      offset = get_offset(&profiles_hashtable, pid);
      update_linked_profile(prof_buf, prof_len, i, offset,
                            profiles_hashtable.next_offset);
      // sync hashmap wrt the next offset
      add_profile_offset(&profiles_hashtable, pid, prof_len[i]);
    }

    prepare_profile_buf_ihash_hashtable(
        prof_buf[i], msg_len, pid, num_out_reqs[i], num_extra_slots[i],
        num_out_frames[i], spacing[i], first_frame_latency[i]);

#if DBG_LVL <= LVL_DBG
    printf("DBG#SME -- [%s:%s:%d] Printing generated buffer\n", __FILE__,
           __func__, __LINE__);
    dbg_prt(prof_buf[i], (hashtable_offset_t)*msg_len);
#endif
  }

#if DBG_LVL <= LVL_DBG
  printf("msg len: %" PRINT_OFFSET ", total msg len: %" PRINT_OFFSET "\n",
         *msg_len, total_msg_len);
  printf("CREATE MAP: Hash table after adding %d profiles\n", n_prof);
  printf_hashtable(&profiles_hashtable);
#endif

  prepare_n_profile_map_and_hashtable_buf_from_mem(
      p + MMAP_MSG_HDR_LEN, msg_len, n_prof, u_ip[0], u_ip[1], u_ip[2], u_ip[3],
      u_port[0], u_port[1], u_port[2], u_port[3], prof_buf, prof_len,
      &profiles_hashtable);

#if DBG_LVL <= LVL_INFO
  printf("msg len: %" PRINT_OFFSET ", total msg len: %" PRINT_OFFSET "\n",
         *msg_len, total_msg_len);
#endif

  *buf = p;
  *buf_len = total_msg_len;

  free(prof_buf);
  free(prof_len);
  deinit_hashtable(&profiles_hashtable);
}

char *get_profile_by_pid_from_mem_buf_hashtable(char *buf,
                                                hashtable_offset_t buf_len,
                                                ihash_t pid) {
  hashtable_offset_t off = MMAP_MSG_HDR_LEN + PROF_CONN_IP_PORT_HDR_LEN;

  hashtable_t ht;
  memcpy(&ht, buf + off, sizeof(hashtable_t));
  ;
  off += sizeof(hashtable_t);

  ht.hashtable_offsets =
      (hashtable_offset_t *)malloc(PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  memcpy(ht.hashtable_offsets, buf + off, PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  off += PROFILE_HASHTABLE_OFFSETS_LEN_BYTES;

  hashtable_offset_t profile_offset = get_offset(&ht, pid);

#if DBG_LVL <= LVL_DBG
  printf("Expecting offset @: %" PRINT_OFFSET "\n", profile_offset);
  printf_hashtable(&ht);
#endif

  char *found_profile_buffer = NULL;
  char *s_buf = (char *)malloc(MAX_BUF_LEN);
  int s_buf_off = 0;
  int found_hash = 0;
  uint16_t num_reqs = 0;
  ihash_t *ihash = (ihash_t *)malloc(sizeof(ihash_t));
  int k = 0;

  // Maximum chaining level 100
  while (!found_hash && k < 100) {

    if (profile_offset == 0) {
      printf("[ERROR] Profile doesn't exist in map\n");
      goto end_get_pid;
    }

    memcpy(ihash, buf + profile_offset, sizeof(ihash_t));
    num_reqs = ((uint16_t) * (buf + profile_offset + sizeof(ihash_t)));

#if DBG_LVL <= LVL_INFO
    printf("Profile lookup: numreqs = %d\n", num_reqs);
#endif

    if (memcmp(&(ihash->byte), &(pid.byte), sizeof(ihash_t)) == 0) {
      found_profile_buffer =
          (char *)malloc(PROF_CONN_LEN_IHASH_HASHTABLE(num_reqs));
      memcpy(found_profile_buffer, buf + profile_offset,
             PROF_CONN_LEN_IHASH_HASHTABLE(num_reqs));
#if DBG_LVL <= LVL_INFO
      printf("*** Profile Found! ***\n");
#endif
      break;
    } else {
#if DBG_LVL <= LVL_INFO
      printf("*** Profile not found! Will check the chaining ***\n");
      print_profile_from_buf_ihash_hashtable(
          s_buf, &s_buf_off, buf + profile_offset,
          PROF_CONN_LEN_IHASH_HASHTABLE(num_reqs));
      printf("Current profile in the chain: \n%s \n", s_buf);
      printf("Current profile length = %lu\n",
             PROF_CONN_LEN_IHASH_HASHTABLE(num_reqs));
#endif

      profile_offset =
          ((hashtable_offset_t *)(buf + profile_offset +
                                  PROF_CONN_LEN_IHASH_HASHTABLE(num_reqs) -
                                  sizeof(hashtable_offset_t)))[0];

#if DBG_LVL <= LVL_INFO
      printf("Expecting offset @: %" PRINT_OFFSET "\n", profile_offset);
#endif
    }
    k++;
  }

#if DBG_LVL <= LVL_INFO
  char hbuf[64];
  int hbuf_len = 64;
  memset(hbuf, 0, hbuf_len);
  prt_hash(ihash->byte, sizeof(ihash_t), hbuf);
  printf("PID LOOKUP: prof ID (Hex): %s, # out reqs: %d\n", hbuf, num_reqs);
  print_profile_from_buf_ihash_hashtable(
      s_buf, &s_buf_off, buf + profile_offset,
      PROF_CONN_LEN_IHASH_HASHTABLE(num_reqs));
  printf("Parsing the profile: \n %s \n", s_buf);
#endif

end_get_pid:
  free(s_buf);
  free(ihash);
  deinit_hashtable(&ht);
  return found_profile_buffer;
}

void print_profile_from_buf_ihash_hashtable(char *s_buf, int *s_buf_len,
                                            char *buf, int buf_len) {
  int i;
  uint16_t off = 0;
  ihash_t prof_id_ihash;
  uint16_t num_out_reqs;
  uint16_t num_extra_slots = 0;
  uint64_t num_out_frames = 0;
  uint64_t spacing = 0;
  uint64_t latency = 0;
  int s_buf_off = 0;
  char hbuf[64];
  int hbuf_len = 64;

  if (!s_buf || !s_buf_len || !buf || !buf_len)
    return;

  prof_id_ihash = ((ihash_t *)(buf + off))[0];
  off += sizeof(ihash_t);

  num_out_reqs = ((uint16_t *)(buf + off))[0];
  off += sizeof(uint16_t);

  memset(hbuf, 0, hbuf_len);
  prt_hash(prof_id_ihash.byte, sizeof(ihash_t), hbuf);

  sprintf(s_buf + s_buf_off, "prof ID (Hex): %s, # out reqs: %d\n", hbuf,
          num_out_reqs);
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "extra slots --- ");
  s_buf_off = strlen(s_buf);

  for (i = 0; i < num_out_reqs; i++) {
    num_extra_slots = ((uint16_t *)(buf + off))[0];
    off += sizeof(uint16_t);
    sprintf(s_buf + s_buf_off, "%d ", num_extra_slots);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "out frames --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    num_out_frames = ((uint64_t *)(buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%lu ", num_out_frames);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "spacing --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    spacing = ((uint64_t *)(buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%lu ", spacing);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "latency --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    latency = ((uint64_t *)(buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%lu ", latency);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  hashtable_offset_t next_profile_offset =
      ((hashtable_offset_t *)(buf + off))[0];
  sprintf(s_buf + s_buf_off, "linked profile offset --- %" PRINT_OFFSET " \n",
          next_profile_offset);
  s_buf_off = strlen(s_buf);

  *s_buf_len = s_buf_off;
}

//depricated
void create_tmp_profile_hashtable(char **buf, hashtable_offset_t *buf_len) {
  int ret = 0;
  int i, j;

  hashtable_offset_t *msg_len = NULL;

  int prof_len = MMAP_MSG_HDR_LEN + PROF_CONN_IP_PORT_HDR_LEN +
                 PROF_CONN_LEN_IHASH_HASHTABLE(1);
  ihash_t pid;
  char ihash[16] = {'n', 'a', 'a', 'a', 'a', 'a', 'c', 'a',
                    'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};
  strncpy(pid.byte, ihash, 16);

  uint16_t num_extra_slots[1] = {1};
  uint64_t num_out_frames[1] = {2};
  uint64_t spacing[1] = {5000000000}; // time in nanoseconds
  uint64_t latency[1] = {8000000000}; // time in nanoseconds
  char *p = (char *)malloc(prof_len + 1);
  memset(p, 0, prof_len + 1);
  init_mmap_msg(p, MSG_T_PMAP_IHASH, &msg_len);

  prepare_profile_buf_ihash_hashtable(p + MMAP_MSG_HDR_LEN, msg_len, pid, 1,
                                      num_extra_slots, num_out_frames, spacing,
                                      latency);

  *buf_len = prof_len;
  *buf = p;
  printf("buf len: %" PRINT_OFFSET ", msg len: %" PRINT_OFFSET "\n", *buf_len,
         *msg_len);
  char *s_buf;
  int s_buf_len = 0;
  print_profile_from_buf_ihash_hashtable(s_buf, &s_buf_len,
                                         p + MMAP_MSG_HDR_LEN, prof_len);
  printf("Generated profile is: %s \n", s_buf);
}
void print_profile_map_buf_ihash_hashtable(char *buf,
                                           hashtable_offset_t buf_len) {
  int i, j;
  hashtable_offset_t off = 0;
  hashtable_offset_t prof_len_off = 0;
  hashtable_offset_t prof_len = 0;
  uint16_t num_out_reqs;
  struct sockaddr_in src_addr, in_addr, out_addr, dst_addr;
  char sip[INET_ADDRSTRLEN];
  char iip[INET_ADDRSTRLEN];
  char oip[INET_ADDRSTRLEN];
  char dip[INET_ADDRSTRLEN];
  memset(&src_addr, 0, sizeof(struct sockaddr_in));
  memset(&in_addr, 0, sizeof(struct sockaddr_in));
  memset(&out_addr, 0, sizeof(struct sockaddr_in));
  memset(&dst_addr, 0, sizeof(struct sockaddr_in));
#if DBG
  printf("DBG#SME -- [%s:%s:%d] Printing generated buffer\n", __FILE__,
         __func__, __LINE__);
  dbg_prt(buf, buf_len);
#endif
  src_addr.sin_addr.s_addr = ((uint32_t *)(buf + off))[0];
  in_addr.sin_addr.s_addr = ((uint32_t *)(buf + off))[1];
  out_addr.sin_addr.s_addr = ((uint32_t *)(buf + off))[2];
  dst_addr.sin_addr.s_addr = ((uint32_t *)(buf + off))[3];
  off += (4 * sizeof(uint32_t));

  src_addr.sin_port = ntohs(((uint16_t *)(buf + off))[0]);
  in_addr.sin_port = ntohs(((uint16_t *)(buf + off))[1]);
  out_addr.sin_port = ntohs(((uint16_t *)(buf + off))[2]);
  dst_addr.sin_port = ntohs(((uint16_t *)(buf + off))[3]);
  off += (4 * sizeof(uint16_t));

  inet_ntop(AF_INET, &(src_addr.sin_addr), sip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(in_addr.sin_addr), iip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(out_addr.sin_addr), oip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(dst_addr.sin_addr), dip, INET_ADDRSTRLEN);
  printf("SRC: %s %u %u, IN: %s %u %u, OUT: %s %u %u, DST: %s %u %u\n", sip,
         src_addr.sin_addr.s_addr, src_addr.sin_port, iip,
         in_addr.sin_addr.s_addr, in_addr.sin_port, oip,
         out_addr.sin_addr.s_addr, out_addr.sin_port, dip,
         dst_addr.sin_addr.s_addr, dst_addr.sin_port);

  hashtable_t ht_test; // = (hashtable_t *) malloc(sizeof(hashtable_t));
  memcpy(&ht_test, buf + PROF_CONN_IP_PORT_HDR_LEN, sizeof(hashtable_t));
  ;
  off += sizeof(hashtable_t);
  ht_test.hashtable_offsets =
      (hashtable_offset_t *)malloc(PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  memcpy(ht_test.hashtable_offsets,
         buf + PROF_CONN_IP_PORT_HDR_LEN + sizeof(hashtable_t),
         PROFILE_HASHTABLE_OFFSETS_LEN_BYTES);
  printf_hashtable(&ht_test);
  off += PROFILE_HASHTABLE_OFFSETS_LEN_BYTES;

  prof_len_off = off;
  int n_prof = ht_test.n_prof;
  printf("[INFO] THIS MAP HAS %d PROFILES \n", n_prof);
  ihash_t prof_id_ihash;
  char *s_buf = (char *)malloc(MAX_BUF_LEN);
  int s_buf_len = 0;
  for (i = 0; i < n_prof; i++) {
    prof_id_ihash = ((ihash_t *)(buf + prof_len_off))[0];
    num_out_reqs = ((uint16_t *)(buf + prof_len_off + sizeof(ihash_t)))[0];
    print_profile_from_buf_ihash_hashtable(
        s_buf, &s_buf_len, buf + prof_len_off,
        PROF_CONN_LEN_IHASH_HASHTABLE(num_out_reqs));
    printf("Profile %d:\n %s \n", i, s_buf);
    prof_len_off += PROF_CONN_LEN_IHASH_HASHTABLE(num_out_reqs);
  }
  free(s_buf);
}
void free_buffer_hashtable(char **buf, hashtable_offset_t buf_len) {
  if (!buf || !(*buf) || !buf_len)
    return;

#if DBG
  printf("[%s:%d] buf: %p, *buf: %p, len: %d\n", __func__, __LINE__, buf, *buf,
         buf_len);
#endif
  free(*buf);
  *buf = NULL;
#if DBG
  printf("[%s:%d] buf: %p, *buf: %p, len: %d\n", __func__, __LINE__, buf, *buf,
         buf_len);
#endif
}
