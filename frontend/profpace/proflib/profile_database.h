/**
 * File  : profile_database.h
 * Author: Mohamed Alzayat <alzayat@mpi-sws.org>
 * Date  : 30.03.2018
 */

#include "../include/profile_conn_format.h"

#include "../include/conn_common.h"
#include "profile_hashtable.h"
#include <stdint.h>
// void prt_hash(char *hbuf, int hlen, char *out);
/*
 * serialize an array of profile buffers read from file
 */
void prepare_n_profile_map_and_hashtable_buf_from_mem(
    char *buf, hashtable_offset_t *buf_len, uint16_t n_profile, uint32_t src_ip,
    uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip, uint16_t src_port,
    uint16_t in_port, uint16_t out_port, uint16_t dst_port, char **profile,
    hashtable_offset_t *profile_len, hashtable_t *ht);

void prepare_profile_buf_ihash_hashtable(char *buf, hashtable_offset_t *buf_len,
                                         ihash_t prof_id, uint16_t num_out_reqs,
                                         uint16_t *num_extra_slots,
                                         uint64_t *num_out_frames,
                                         uint64_t *frame_spacing,
                                         uint64_t *first_frame_latency);

void create_profile_map_ihash_hashtable(
    char **buf, hashtable_offset_t *buf_len, char *ip1, uint16_t port1,
    char *ip2, uint16_t port2, char *ip3, uint16_t port3, char *ip4,
    uint16_t port4, uint16_t n_prof, char **profile_id, uint16_t *num_out_reqs,
    uint16_t **num_extra_slots, uint64_t **num_out_frames, uint64_t **spacing,
    uint64_t **first_frame_latency);

void update_linked_profile(char **profile, hashtable_offset_t *prof_len,
                           int n_prepared_profiles,
                           hashtable_offset_t profile_offset,
                           hashtable_offset_t new_offset);

void print_profile_from_buf_ihash_hashtable(char *s_buf, int *s_buf_len,
                                            char *buf, int buf_len);

char *get_profile_by_pid_from_mem_buf_hashtable(char *buf,
                                                hashtable_offset_t buf_len,
                                                ihash_t pid);
// void create_tmp_profile_hashtable(char **buf, int *buf_len, char *fname, int
// read);
void create_tmp_profile_hashtable(char **buf, hashtable_offset_t *buf_len);

void print_profile_map_buf_ihash_hashtable(char *buf,
                                           hashtable_offset_t buf_len);
void free_buffer_hashtable(char **buf, hashtable_offset_t buf_len);
