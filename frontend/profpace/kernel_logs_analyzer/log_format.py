class log_record:
    def __init__(self, timestamp, payload_len, seqno, seqack, direction,
        num_public_inputs, num_private_inputs, count = 1, opt_log_record=None):

        """
        timestamp of sending/receiving the packet
        """
        # uint64_t
        self.timestamp = timestamp

        """
        true length of the packet payload
        (i.e. without size padding)
        """
        # uint32_t
        self.payload_len = payload_len

        """
        seq and ack numbers (valid only for TCP packets)
        """
        # uint32_t
        self.seqno = seqno
        # uint32_t
        self.seqack = seqack

        """
        direction of the logged packet
        0 - source is s_addr, s_port
        1 - source is d_addr, d_port
        """
        # uint8_t
        self.direction = direction
        
        # uint32_t
        #this entry represents "count" number of  entries
        self.count = count

        """
        # of public input hashes following the log record header
        """
        # uint16_t
        self.num_public_inputs = num_public_inputs

        """
        # of private input hashes following the log record header
        """
        # uint16_t
        self.num_private_inputs = num_private_inputs

        self.opt_log_record = opt_log_record

# ==========================
# the following fields may
# occur only in some records
# ==========================

class opt_log_record:

    def __init__(self, req_id, marker_timestamp, pub_hashes = [], priv_hashes = []):
        """
        hash(TS, public inputs#, private inputs#)
        used to link traffic across multiple connection interfaces,
        and separate packets of one request from another
        """
        # ihash_t
        self.req_id = req_id

        """
        timestamp of when the profile marker packet (containing the
        public and private inputs hashes) reached the kernel
        may use this to profile the latencies of the application marker
        """
        # uint64_t
        self.marker_timestamp = marker_timestamp

        """
        list of public and private
        input hashes follow here
        """
        self.pub_hashes = pub_hashes
        self.priv_hashes = priv_hashes


class log_conn_header:
    def __init__(self, conn, n_recs):

        """
        pair of connection 5-tuple
        """
        # conn_t
        self.conn = conn

        """
        #records following the header
        """
        # uint32_t
        self.n_recs = n_recs

    def __str__(self):
        p_str = ""
        p_str += "conn: " + str(self.conn)+ ", " 
        p_str += " #recs: " + str(self.n_recs)
        return p_str

class log_header:
    def __init__(self, total_log_size, n_conn):

        """
        total size of log from the start of this header
        till the end of the buffers of all n_conn listed here
        """
        # uint64_t
        self.total_log_size = total_log_size

        """
        # pcm's whose logs follow this header
        """
        # uint16_t
        self.n_conn = n_conn


    def __str__(self):
        p_str = ""
        p_str += "log size: " + str(self.total_log_size) + ", "
        p_str += "num conns: " + str(self.n_conn)
        return p_str

class conn:
    def __init__(self, src_ip, in_ip, out_ip, dst_ip, src_port, in_port,
        out_port, dst_port):

        # uint32_t
        self.src_ip = src_ip

        """
        in_ip should be equal to out_ip,
        unless there are multiple NICs
        configured with diff. IP addresses
        """
        # uint32_t
        self.in_ip = in_ip
        # uint32_t
        self.out_ip = out_ip
        # uint32_t
        self.dst_ip = dst_ip
        # uint16_t
        self.src_port = src_port
        # uint16_t
        self.in_port = in_port
        # uint16_t
        self.out_port = out_port
        # uint16_t
        self.dst_port = dst_port


    def __str__(self):
        p_str = ""
        p_str += "src: " + str(self.src_ip) + " " + str(self.src_port) + ", "
        p_str += "in: " + str(self.in_ip) + " " + str(self.in_port) + ", "
        p_str += "out: " + str(self.out_ip) + " " + str(self.out_port) + ", "
        p_str += "dst: " + str(self.dst_ip) + " " + str(self.dst_port)
        return p_str

class log_conn:
    def __init__(self, log_header):
        self.log_header = log_header
        self.log_conn_header = None
        self.marker_records = []
        self.log_records = []
        self.conn_req_ids = []
        self.is_crosstier = False
        self.crootier_connections = []
        self.log_observations = []
        self.request_count = 0


    def __str__(self):
        p_str = ""
        p_str += str(hex(id(self))) + ": "
        p_str += "header: " + str(self.log_conn_header) + ", "
        p_str += "is crosstier: " + str(self.is_crosstier) + ", "
        p_str += "req count: " + str(self.request_count)
        return p_str
