from operator import itemgetter
import numpy
import os
import sys
import argparse
import re
import time
from ctypes import *
from imports import create_profile_map, print_profile_map_buf
import socket
import math
import pprint
import collections
from profile import profile
from lib_binary_profiles import prepare_profile_map_binary
from config_sme_profiler import FINAL_EXPERIMENTS, PREP_PROFILE_DBG_LVL, PREPARE_PROFILE_STORE_GENERATED_PROFILES_ASCII, LVL_DBG, LVL_WARN, LVL_ERR, LVL_EXP
STAT_PROFILES = 1
MULT_FACTOR = 0.000001

REQUEST_ID=0
FRAME_ID=1
FIRST_IN_TIMETAMP=2
LAST_IN_TIMETAMP=3
IN_SIZE=4
IN_PACKET_COUNT=5
FIRST_OUT_TIMESTAMP=6
LAST_OUT_TIMESTAMP=7
OUT_SIZE=8
OUT_PACKET_COUNT=9
SRC_IP=10
SRC_PORT=11
DST_IP=12
DST_PORT=13
FIRST_RESPONSE_PACKET_LATENCY=14
LAST_RESPONSE_PACKET_LATENCY=15
INTER_ARRIVAL_TIME=16
REQ_PACING=17
RSP_PACING=18
SUBSEQUENT_REQUESTS_INTER_ARR_TIME=19
CROSSTIER_REQ_INTER_ARRIVAL_TIME=20
CROSSTIER_RSP_INTER_ARRIVAL_TIME=21

default_column_legends = [
    "request_ID",
    "frame_ID",
    "first_req_timestamp",
    "last_req_timetamp",
    "req_size",
    "req_packet_count",
    "first_resp_timestamp",
    "last_resp_timestamp",
    "rsp_size",
    "rsp_packet_count",
    "src_IP",
    "src_Port",
    "dst_IP",
    "dst_Port",
    "first_response_packet_latency",
    "last_response_packet_latency",
    "req_inter_arrival_time",
    "req_pacing",
    "rsp_pacing",
    "subsequent_req_inter_arrival_time",
    "crosstier_req_inter_arrival_time",
    "crosstier_rsp_inter_arrival_time"
    ] 



def parse_profile(path_to_ascii_file):
    parsed_profile = profile()

    clean_f = open(path_to_ascii_file+ '.cleaned_profile', 'w')
    for line in open(path_to_ascii_file):
        if line.startswith("#"):
            continue
        clean_f.write(line)
    clean_f.close()

    with open(path_to_ascii_file + '.cleaned_profile', 'r') as f:
        #profile_id = hex_to_binary_profile_id(f.readline())
        #profile_id = str(bytes.fromhex(str(f.readline)))
        profile_id = f.readline().strip()
        profile_id = profile_id.decode('hex')

        num_out_reqs = int(f.readline())
        num_extra_slots = int(f.readline())
        num_out_frames = [int(f.readline()) for i in range(num_out_reqs)]
        frame_spacing = [float(f.readline()) for i in range(num_out_reqs)]
        frame_latency = [float(f.readline()) for i in range(num_out_reqs)]
        parsed_profile.set_profile(
            profile_id = profile_id,
            num_out_reqs = num_out_reqs, 
            num_extra_slots = num_extra_slots,
            num_out_frames = num_out_frames,
            frame_spacing = frame_spacing,
            frame_latency = frame_latency,
            public_hashes = profile_id,
            )
    return parsed_profile

def write_profile(epoch, outdir, prefix,  profile_id, num_out_reqs,
        num_extra_slots, num_out_frames, frame_spacing, frame_latency,
        stats=False, marker_delays=[], inter_spacing=[]):

    if PREPARE_PROFILE_STORE_GENERATED_PROFILES_ASCII == 0:
        return

    if stats:
        if FINAL_EXPERIMENTS:
            f = open(outdir + "/" + prefix + "." + str(profile_id) + '.' + str(epoch) + '.profile.non_overprovisioned.stats', 'w')
        else:
            f = open(outdir + "/" + prefix + '.profile.stats', 'w')

        if PREP_PROFILE_DBG_LVL <= LVL_DBG:
            print "writing profile to: " + outdir +"/"+ prefix + "." + str(profile_id)   \
                + '.' + str(epoch) + '.profile.stats'
    else:
        if FINAL_EXPERIMENTS:
            f = open(outdir + '/' + prefix + "." + str(profile_id) + '.' + str(epoch) + '.profile', 'w')
            f1 = open(outdir + "/" + prefix + "." + str(profile_id) + '.' + str(epoch) + '.profile.csv', 'w')
        else:
            f = open(outdir + '/' + prefix + '.profile', 'w')
        if PREP_PROFILE_DBG_LVL <= LVL_DBG:
            print "writing profile to: " + outdir + "/" +prefix + "." + str(profile_id)   \
                + '.' + str(epoch) + '.profile'

    f.write("#Profile ID: \n")
    f.write(str(profile_id))
    f.write("\n#------------------------------------\n")
    if stats:
        f.write("#Number of out requests (MAX, MIN_IDX, MAX_IDX): \n")
        f.write(str(num_out_reqs))
    else:
        f.write("#Number of out requests: \n" )
        f.write(str(num_out_reqs))
    f.write('\n')
    f.write("#Number of overprovisioned slots: \n" )
    f.write(str(num_extra_slots))
    f.write('\n')

    f.write("#------------------------------------\n")
    f.write("#Number Of Out Frames:\n")
    f.write("#------------------------------------\n")
    if stats:
        f.write("#Count, AVG, MIN, "    \
            + "50%, 80%, 90%, 95%, 98%, 99%, 100%, std_dev, MIN_IDX, MAX_IDX\n")
        f.write("#------------------------------------\n")
    for n in num_out_frames:
        if stats:
            f.write(', '.join(str(k) for k in n))
        else:
            f.write(str(n))
        f.write('\n')

    f.write("#------------------------------------\n")
    f.write("#Out Frame Spacing (ms):\n")
    f.write("#------------------------------------\n")
    if stats:
        f.write("#Count, AVG, MIN, "    \
            + "50%, 80%, 90%, 95%, 98%, 99%, 100%, std_dev, MIN_IDX, MAX_IDX\n")
        f.write("#------------------------------------\n")

    for n in frame_spacing:
        if stats:
            f.write(', '.join(str(k) for k in n))
        else:
            f.write(str('%f' % n))
        f.write('\n')

    f.write("#------------------------------------\n")
    f.write("#First Out Frame Latency (ms):\n")
    f.write("#------------------------------------\n")
    if stats:
        f.write("#Count, AVG, MIN, "    \
            + "50%, 80%, 90%, 95%, 98%, 99%, 100%, std_dev, MIN_IDX, MAX_IDX\n")
        f.write("#------------------------------------\n")

    for n in frame_latency:
        if stats:
            f.write(', '.join(str(k) for k in n))
        else:
            f.write(str('%f' % n))
        f.write('\n')

    if stats and len(marker_delays) > 0:
        f.write("#------------------------------------\n")
        f.write("# Marker delay wrt. to last req packet (ms):\n")
        f.write("#------------------------------------\n")
        if stats:
            f.write("#Count, AVG, MIN, "    \
                + "50%, 80%, 90%, 95%, 98%, 99%, 100%, std_dev, MIN_IDX, MAX_IDX\n")
            f.write("#------------------------------------\n")
        for n in marker_delays:
            f.write(', '.join(str(k) for k in n))
            f.write('\n')
        f.write("#------------------------------------\n")
    if stats and len(inter_spacing) > 0:
        f.write("# Inter packet spacing (ms):\n")
        f.write("#------------------------------------\n")
        for n in inter_spacing:
            f.write(str(n))
            f.write('\n')
        f.write("#------------------------------------\n")

    f.close()
    if FINAL_EXPERIMENTS and not stats:
        f1.write(str(profile_id)+", "+ str(num_out_reqs)+", "+
                ' | '.join([str(k) for k in num_out_frames])+ ", "+
                ' | '.join([str('%f' % n) for n in frame_spacing]) +", "+
                    ' | '.join([str('%f' % n) for n in frame_latency]))

        f1.close()

def datastream_splitting_oo(backend_observations):
    i = 0
    #print len(ds)
    #print ("Num of out streams "+str(n_out_streams))
    length_of_streams = [len(obs) for obs in backend_observations]

    #flatten backend observations
    combined_backend_streams = [
        obs for subset in backend_observations for obs in subset]

    n_out_streams = max(length_of_streams)
    if PREP_PROFILE_DBG_LVL <= LVL_DBG:
        print "(Number_of_streams, MAX_IDX) :"  \
            + str((n_out_streams, numpy.argmax(length_of_streams)))

    generated_streams = [[] for _ in range(0, n_out_streams)]
    j = 0
    count = 0
    for r in combined_backend_streams:
        generated_streams[count % n_out_streams].append(r)
        count += 1
        if count >= int(length_of_streams[j]):
            j += 1
            count = 0
    if n_out_streams == 1:
        generated_streams = [generated_streams]
    #print "[SME_PREPARE_PROFILE] n_out_streams: " + str(n_out_streams)
    #pprint.PrettyPrinter( generated_streams)
    return numpy.array(generated_streams)


def get_all_percentiles(array):
    return [len(array),
        str('%f' % numpy.average(array)),
        str('%f' % min(array)),
        str('%f' % numpy.percentile(array,50, interpolation='lower')),
        str('%f' % numpy.percentile(array,80, interpolation='lower')),
        str('%f' % numpy.percentile(array,90, interpolation='lower')),
        str('%f' % numpy.percentile(array,95, interpolation='lower')),
        str('%f' % numpy.percentile(array,98, interpolation='lower')),
        str('%f' % numpy.percentile(array,99, interpolation='lower')),
        str('%f' % numpy.percentile(array,100, interpolation='lower')),
        str('%f' % numpy.std(array)),
        numpy.argmin(array), numpy.argmax(array),
        ]


def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, basestring):
            for sub in flatten(el):
                yield sub
        else:
            yield el

def prepare_profile_oo(epoch, frontend_analysed_logs, outdir, percentile,
    over_provisioning, backend_crosstier_profile = True):
    # log_obs_stream is the set of front end logs,
    # each having the corresponding BE logs

    # preparing a FE-Client profile

    num_out_frames = []
    frame_spacing = []
    frame_latency = []
    inter_spacing = []
    non_decimal = re.compile(r'[^\d.]+')
    # In case the frontend_analysed_logs was an array of arrays of observations,
    # we flatten it
    combined_frontend_streams =  [obs for obs in flatten(frontend_analysed_logs)]
    
    #makesure the frontend connections have public hashes before
    #actually using them in the profile id
    if len(combined_frontend_streams) > 0 and \
        len(combined_frontend_streams[0].public_hashes) > 0:
        public_hashes_of_stream = combined_frontend_streams[0].public_hashes
    else:
        print "BUG: Why are you trying to profile something without a public hash?!"
        print "you know what: I will intentionally give you an exception!!!"
        print "Are you profiling Crosstier connections as if they were FE ones?!!!"
        combined_frontend_streams[0].public_hashes[0]
        public_hashes_of_stream = ['0000000000000000']
    
    
    if PREP_PROFILE_DBG_LVL <= LVL_DBG:
        print "Profiling for # client requests:"
        print len(combined_frontend_streams)
    if PREP_PROFILE_DBG_LVL <= LVL_DBG:
        print "Generating profiles based on the following Frontend streams:"
        for s in combined_frontend_streams:
            print str(s)
        print "Number of streams: " , len(frontend_analysed_logs)
        print "Number of frontend requests: " , len(combined_frontend_streams)
    
    obs_with_ack_pkts = [log_obs.ack_observation for log_obs in combined_frontend_streams if log_obs.ack_observation]
    if obs_with_ack_pkts:
        if PREP_PROFILE_DBG_LVL <= LVL_DBG:
            print "Profiling for ACKs:"
            print obs_with_ack_pkts 
        #This will be 1 anyway
        out_ack_pkts = int(numpy.percentile([ack_obs.rsp_packet_count
            for ack_obs in obs_with_ack_pkts],
             percentile["fe_pkt"], interpolation='lower'))
        num_out_frames.append(out_ack_pkts)
        spc_ack_val = numpy.percentile([ack_obs.rsp_pacing
            for ack_obs in obs_with_ack_pkts], percentile["fe_pac"],
            interpolation='lower')
        spc_ack_val = (spc_ack_val + (spc_ack_val * over_provisioning["fe_pac"])/100)
        frame_spacing.append(spc_ack_val)

        lat_ack_val = numpy.percentile([ack_obs.first_rsp_packet_latency
            for ack_obs in obs_with_ack_pkts], percentile["fe_lat"],
            interpolation='lower')
        lat_ack_val = (lat_ack_val + (lat_ack_val * over_provisioning["fe_lat"])/100)
        frame_latency.append(lat_ack_val)
        num_out_reqs = 2
    else:
        num_out_reqs = 1


    out_pkt_val = int(numpy.percentile(
        [fe_log_obs.rsp_packet_count
            for fe_log_obs in combined_frontend_streams],
             percentile["fe_pkt"], interpolation='lower'))
    #out_pkt_val = (out_pkt_val + (out_pkt_val * over_provisioning["fe_pkt"])/100)

    num_out_frames.append(out_pkt_val)

    spc_val = numpy.percentile([fe_log_obs.rsp_pacing
        for fe_log_obs in combined_frontend_streams], percentile["fe_pac"],
        interpolation='lower')
    spc_val = (spc_val + (spc_val * over_provisioning["fe_pac"])/100)
    frame_spacing.append(spc_val)

    lat_val = numpy.percentile([fe_log_obs.first_rsp_packet_latency
        for fe_log_obs in combined_frontend_streams], percentile["fe_lat"],
        interpolation='lower')
    lat_val = (lat_val + (lat_val * over_provisioning["fe_lat"])/100)
    frame_latency.append(lat_val)

    num_extra_slots = int(max(num_out_frames) * over_provisioning["fe_pkt"]/100)
    
    #create a new profile instance
    fe_client_profile = profile()
    fe_client_profile.set_profile(
        profile_id = public_hashes_of_stream[0],
        num_out_reqs = num_out_reqs, num_extra_slots = num_extra_slots,
        num_out_frames = num_out_frames,
        frame_spacing = frame_spacing,
        frame_latency = frame_latency,
        public_hashes = public_hashes_of_stream,
        private_hashes = [])

    write_profile(epoch, outdir, "FE-Client",
        '_'.join([str(s).encode('hex') for s in public_hashes_of_stream]),
        num_out_reqs, num_extra_slots, num_out_frames,
        frame_spacing, frame_latency)

    if STAT_PROFILES:
        num_extra_slots = int(max(num_out_frames) * over_provisioning["fe_pkt"]/100)
        num_out_frames = []
        frame_spacing = []
        frame_latency = []
        marker_delays = []
        if obs_with_ack_pkts:
            if PREP_PROFILE_DBG_LVL <= LVL_DBG:
                print "Profiling for ACKs:"
                print obs_with_ack_pkts 
            #This will be 1 anyway
            out_ack_pkts = [ack_obs.rsp_packet_count
                for ack_obs in obs_with_ack_pkts]
            num_out_frames.append(get_all_percentiles(out_ack_pkts))
            spc_ack_val = [ack_obs.rsp_pacing
                for ack_obs in obs_with_ack_pkts]
            frame_spacing.append(get_all_percentiles(spc_ack_val))

            lat_ack_val = [ack_obs.first_rsp_packet_latency
                for ack_obs in obs_with_ack_pkts]
            frame_latency.append(get_all_percentiles(lat_ack_val))
            num_out_reqs = 2
        else:
            num_out_reqs = 1

        marker_delay = [fe_log_obs.marker_delay for fe_log_obs in combined_frontend_streams]
        marker_delays.append(get_all_percentiles(marker_delay))

        out_frames = [fe_log_obs.rsp_packet_count
            for fe_log_obs in combined_frontend_streams]
        num_out_frames.append(get_all_percentiles(out_frames))

        out_spacing = [fe_log_obs.rsp_pacing
            for fe_log_obs in combined_frontend_streams]
        frame_spacing.append(get_all_percentiles(out_spacing))

        out_frame_latencies =  [fe_log_obs.first_rsp_packet_latency
                for fe_log_obs in combined_frontend_streams]
        frame_latency.append(get_all_percentiles(out_frame_latencies))

        out_interpacket_spacing =  [obs for obs in flatten([fe_log_obs.rsp_inter_packet_time
                for fe_log_obs in combined_frontend_streams])]
        
        #print "Number of inter-packet observations: " + str( len(out_interpacket_spacing))
        if len(out_interpacket_spacing) > 0:
            inter_spacing.append(get_all_percentiles(out_interpacket_spacing))
        

        #num_extra_slots = int(max(num_out_frames) * over_provisioning["fe_pkt"] / 100)
        num_out_reqs = 1

        write_profile(epoch, outdir, "FE-Client",
            '_'.join([str(s).encode('hex') for s in public_hashes_of_stream]),
            num_out_reqs, num_extra_slots, num_out_frames,
            frame_spacing, frame_latency, 1, marker_delays, inter_spacing)


    if not backend_crosstier_profile:
        return (fe_client_profile, None, None)

    #preparing a FE-BE profile

    combined_backend_streams = [fe_obs.backend_observations
        for fe_obs in combined_frontend_streams]
    backend_buckets = datastream_splitting_oo(combined_backend_streams)

    if len(backend_buckets) == 0:
        if PREP_PROFILE_DBG_LVL <= LVL_ERR:
            print "[WARNING] No Backend Queries"
        return (fe_client_profile, None, None)

    num_out_frames = []
    frame_spacing = []
    frame_latency = []

    if PREP_PROFILE_DBG_LVL <= LVL_DBG:
        print "Number of backend streams: " , len(backend_buckets)
    for curr_backend_bucket in backend_buckets:
        out_pkt_val = int(numpy.percentile(
            [log_obs.req_packet_count
                for log_obs in curr_backend_bucket],
                 percentile["ct_pkt"], interpolation='lower'))
        
        #out_pkt_val = (out_pkt_val + (out_pkt_val * over_provisioning["ct_pkt"])/100)
        num_out_frames.append(out_pkt_val)
        
        spc_val = numpy.percentile([log_obs.req_pacing
            for log_obs in curr_backend_bucket], percentile["ct_pac"],
            interpolation='lower')
        spc_val = (spc_val + (spc_val * over_provisioning["ct_pac"])/100)
        frame_spacing.append(spc_val)

        lat_val = numpy.percentile([log_obs.crosstier_req_inter_arrival_time
            for log_obs in curr_backend_bucket], percentile["ct_lat"],
            interpolation='lower')
        lat_val = (lat_val + (lat_val * over_provisioning["ct_lat"])/100)
        frame_latency.append(lat_val)

    ##overprovisioning for extra queries
    #num_extra_queries = int(len(backend_buckets)*over_provisioning["extra_queries"]/100)
    #for ex in range(num_extra_queries):
    #    num_out_frames.append(int(max(num_out_frames)))
    #    frame_spacing.append(max(frame_spacing))
    #    #assume that the first packet will be sent at time = 
    #    #previous_query_response_time + previous_query_response_number_of_packet*previous_query_response_pacing + max_inter_query_delay
    #    frame_latency.append(frame_latency[-1] + frame_spacing[-1]*num_out_frames[-1] +  


    num_extra_slots = int(max(num_out_frames) * over_provisioning["ct_pkt"] / 100)
    num_out_reqs = len(backend_buckets)

    fe_be_profile = profile()
    fe_be_profile.set_profile(
        profile_id = public_hashes_of_stream[0],
        num_out_reqs = num_out_reqs,
        num_extra_slots = num_extra_slots,
        num_out_frames = num_out_frames,
        frame_spacing = frame_spacing,
        frame_latency = frame_latency,
        public_hashes = public_hashes_of_stream,
        private_hashes = [])

    write_profile(epoch, outdir, "FE-BE",
        '_'.join([str(s).encode('hex') for s in public_hashes_of_stream]),
        num_out_reqs, num_extra_slots, num_out_frames,
        frame_spacing, frame_latency)

    if STAT_PROFILES:
        num_out_frames = []
        frame_spacing = []
        frame_latency = []
        for curr_backend_bucket in backend_buckets:
            out_frames =  [log_obs.req_packet_count
                    for log_obs in curr_backend_bucket]
            num_out_frames.append(get_all_percentiles(out_frames))

            out_spacing = [log_obs.req_pacing
                    for log_obs in curr_backend_bucket]
            frame_spacing.append(get_all_percentiles(out_spacing))

            out_frame_latencies =  [log_obs.crosstier_req_inter_arrival_time
                    for log_obs in curr_backend_bucket]
            frame_latency.append(get_all_percentiles(out_frame_latencies))
        #reusing num_extra_slots, and out_requests
        #num_extra_slots = int(max(num_out_frames) * over_provisioning["ct_pkt"] / 100)
        #num_out_reqs = 1

        num_out_reqs = (
            len(backend_buckets),
            numpy.argmin([len(bb) for bb in backend_buckets]),
            numpy.argmax([len(bb) for bb in backend_buckets]))

        write_profile(epoch, outdir, "FE-BE",
            '_'.join([str(s).encode('hex') for s in public_hashes_of_stream]),
            num_out_reqs, num_extra_slots, num_out_frames,
            frame_spacing, frame_latency, 1)


    #preparing a BE-FE profile
    num_out_frames = []
    frame_spacing = []
    frame_latency = []

    if PREP_PROFILE_DBG_LVL <= LVL_DBG:
        print "Number of backend streams: " , len(backend_buckets)
    for curr_backend_bucket in backend_buckets:
        out_pkt_val = int(numpy.percentile(
            [log_obs.rsp_packet_count
                for log_obs in curr_backend_bucket],
                 percentile["be_pkt"], interpolation='lower'))

        #out_pkt_val = (out_pkt_val + (out_pkt_val * over_provisioning["be_pkt"])/100)

        num_out_frames.append(out_pkt_val)
        
        spc_val = numpy.percentile([log_obs.rsp_pacing
            for log_obs in curr_backend_bucket], percentile["be_pac"],
            interpolation='lower')
        spc_val = (spc_val + (spc_val * over_provisioning["be_pac"])/100)
        frame_spacing.append(spc_val)

        lat_val = numpy.percentile([log_obs.crosstier_rsp_inter_arrival_time
            for log_obs in curr_backend_bucket], percentile["be_lat"])
        lat_val = (lat_val + (lat_val * over_provisioning["be_lat"])/100)
        frame_latency.append(lat_val)

    num_extra_slots = int(max(num_out_frames) * over_provisioning["be_pkt"] / 100)
    num_out_reqs = len(backend_buckets)
    be_fe_profile = profile()
    be_fe_profile.set_profile(
        profile_id = public_hashes_of_stream[0],
        num_out_reqs = num_out_reqs, num_extra_slots = num_extra_slots, \
        num_out_frames = num_out_frames,
        frame_spacing = frame_spacing,
        frame_latency = frame_latency, \
        public_hashes = public_hashes_of_stream,
        private_hashes = [])

    write_profile(epoch, outdir, "BE-FE_CROSSTIER",
        '_'.join([str(s).encode('hex') for s in public_hashes_of_stream]),
        num_out_reqs, num_extra_slots, num_out_frames,
        frame_spacing, frame_latency)

    if STAT_PROFILES:
        num_out_frames = []
        frame_spacing = []
        frame_latency = []
        for curr_backend_bucket in backend_buckets:
            out_frames =  [log_obs.rsp_packet_count
                for log_obs in curr_backend_bucket]
            num_out_frames.append(get_all_percentiles(out_frames))
            out_spacing = [log_obs.rsp_pacing
                for log_obs in curr_backend_bucket]
            frame_spacing.append(get_all_percentiles(out_spacing))

            out_frame_latencies =  [log_obs.crosstier_rsp_inter_arrival_time
                for log_obs in curr_backend_bucket]
            frame_latency.append(get_all_percentiles(out_frame_latencies))
        #reusing num_extra_slots and num_out_reqs
        #num_extra_slots = int(max(num_out_frames) * over_provisioning["be_pkt"] / 100)
        #num_out_reqs = 1
        num_out_reqs = (
            len(backend_buckets),
            numpy.argmin([len(bb) for bb in backend_buckets]),
            numpy.argmax([len(bb) for bb in backend_buckets]))

        write_profile(epoch, outdir, "BE-FE_CROSSTIER",
            '_'.join([str(s).encode('hex') for s in public_hashes_of_stream]),
            num_out_reqs, num_extra_slots, num_out_frames,
            frame_spacing, frame_latency, 1)


    return (fe_client_profile, fe_be_profile, be_fe_profile)


################################
#Everything below is deprecated
###############################
def datastream_splitting(data_stream, row_splits, n_out_streams):
    i = 0
    #print len(ds)
    #print ("Num of out streams "+str(n_out_streams))
    generated_streams = [[] for _ in range(0, n_out_streams)]
    j = 0
    count = 0
    for r in data_stream:
        generated_streams[count % n_out_streams].append(r)
        count += 1
        if count >= int(row_splits[j]):
            j += 1
            count = 0
    if n_out_streams == 1:
        generated_streams = [generated_streams]
    #print "[SME_PREPARE_PROFILE] n_out_streams: " + str(n_out_streams)
    #print len(generated_streams)
    return numpy.array(generated_streams)
    # return generated_streams

def prepare_profile(epoch, ip1, port1, ip2, port2, ip3, port3, ip4, port4,
    pmap_buf_ptr, pmap_buf_len_ptr,
    data_stream, outdir, row_splits, profile_id,
    percentile, over_provisioning, backend_crosstier_profile = False):
    #if data_stream is -1:
    #    data_stream = numpy.loadtxt(infile, dtype=str, comments='#', ndmin=1)
    #    if (str(data_stream.shape).startswith("(1,") or
    #       str(data_stream.shape).endswith(",)")):
    #        data_stream = [data_stream]
    #        print "Just one dimension? I'm making it 2D ;) "
    #        print txtfile
    #        data_streams.append(txtfile)

    if row_splits != -1:
        num_out_reqs = max(row_splits)
        #print "Preparing profile: max number of db queries: " + str(num_out_reqs)
        generated_streams = \
            datastream_splitting(data_stream, row_splits, num_out_reqs)
    else:
        num_out_reqs = 1
        #print "[SME_PREPARE_PROFILE] singled request"
        generated_streams = numpy.array([data_stream])
        #generated_streams = data_stream

    num_out_frames = []
    frame_spacing = []
    frame_latency = []
    non_decimal = re.compile(r'[^\d.]+')
    #print "GENERATED STREAMS: "
    #print generated_streams
    for stream in generated_streams:
        # if (str(stream.shape).startswith("(1,") or
        #   str(stream.shape).endswith(",)")):
        #    stream = [stream]
        # print stream

        if backend_crosstier_profile:
            #print "backend_crosstier_profile: " + str(len(stream))
            nof = []
            for r in range(0, len(stream)):
                #print float(non_decimal.sub("",
                #    stream[r][IN_PACKET_COUNT]))
                nof.append(float(non_decimal.sub("",
                    stream[r][IN_PACKET_COUNT])))

            num_out_frames.append(int(numpy.percentile(nof, percentile[0], interpolation='lower')))
            frame_spacing.append(numpy.percentile(
                [float(non_decimal.sub("", stream[r][REQ_PACING]))
                    for r in range(0, len(stream))],
                percentile[1], interpolation='lower'))
            if row_splits == -1:
                frame_latency.append(numpy.percentile(
                    [float(non_decimal.sub("",
                        stream[r][FIRST_RESPONSE_PACKET_LATENCY]))
                        for r in range(0, len(stream))],
                    percentile[2], interpolation='lower'))
            else:
                frame_latency.append(numpy.percentile(
                    [float(non_decimal.sub("",
                        stream[r][CROSSTIER_RSP_INTER_ARRIVAL_TIME]))
                        for r in range(0, len(stream))],
                    percentile[2], interpolation='lower'))
        else:
            #print "!backend_crosstier_profile: " + str(len(stream))
            #pp = pprint.PrettyPrinter(indent=4)
            #print(stream)
            #print type(stream)
            nof = []
            for r in range(0, len(stream)):
                #print stream[r]
                #print stream[r][OUT_PACKET_COUNT]
                nof.append(float(non_decimal.sub("",
                    stream[r][OUT_PACKET_COUNT])))

            num_out_frames.append(int(numpy.percentile(nof, percentile[0], interpolation='lower')))

            #num_out_frames.append(int(numpy.percentile(
            #    [float(non_decimal.sub("", stream[r][OUT_PACKET_COUNT]))
            #        for r in range(0, len(stream))],
            #    percentile[0], interpolation='lower')))
            frame_spacing.append(numpy.percentile(
                [float(non_decimal.sub("", stream[r][RSP_PACING]))
                    for r in range(0, len(stream))],
                percentile[1], interpolation='lower'))
            if row_splits == -1:
                frame_latency.append(numpy.percentile(
                    [float(non_decimal.sub("",
                        stream[r][FIRST_RESPONSE_PACKET_LATENCY]))
                        for r in range(0, len(stream))],
                    percentile[2], interpolation='lower'))
            else:
                frame_latency.append(numpy.percentile(
                    [float(non_decimal.sub("",
                        stream[r][CROSSTIER_REQ_INTER_ARRIVAL_TIME]))
                        for r in range(0, len(stream))],
                    percentile[2], interpolation='lower'))


    num_extra_slots = int(max(num_out_frames) * over_provisioning / 100)
    write_profile(epoch, outdir,"", profile_id, num_out_reqs, num_extra_slots,
          num_out_frames, frame_spacing, frame_latency)

    ## prepare_profile called as a script
    if ip1 == None:
        return
    prepare_profile_map_binary(pmap_buf_ptr, pmap_buf_len_ptr,
            ip1, port1, ip2, port2, ip3, port3, ip4, port4,
            num_out_reqs, num_extra_slots, num_out_frames,
            frame_spacing, frame_latency)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate profile file')
    parser.add_argument('-pid', '--profile_id', dest='profile_id',
        required=True,
        help='Profile id that will be used to name the profile file')
    parser.add_argument('-p', '--percentile', dest='percentile', nargs='+',
        required=True, help='Percentile to use for the profile,'    \
        ' in the following order: out_frames_percentile, '  \
        'frame_spacing_percentile, first_frame_latency_percentile')
    parser.add_argument('-op', '-over_provisioning', dest='over_provisioning',
        required=True, help='over provisioning for frames')

    parser.add_argument('-f', '--file', dest='infile', required=True,
        help='input file, which is the output of tcpdump_analyzer')
    parser.add_argument('-od', '--out_directory', dest='outdir', required=True,
        help='output directory where profile file with the name '   \
        'profile_id.profile will be created')
    parser.add_argument('-fsr', '--file_subseq_requests',
        dest='file_subseq_requests', required=False, default=-1,
        help='Backend query count file')
    parser.add_argument('-e', '--epoch', dest='epoch', required=True,
        help='epoch (time) for experiment naming')
    args = parser.parse_args()

    infile = args.infile
    outdir = args.outdir
    file_subseq_requests = args.file_subseq_requests
    profile_id = int(args.profile_id)
    percentile = [int(x) for x in args.percentile]
    over_provisioning = int(args.over_provisioning)
    epoch = int(args.epoch)

    data_stream = numpy.loadtxt(infile, dtype=str, comments='#', ndmin=1)
    if (str(data_stream.shape).startswith("(1,") or
        str(data_stream.shape).endswith(",)")):
        data_stream = [data_stream]

    if file_subseq_requests != -1:
        subseq_req_file = numpy.loadtxt(file_subseq_requests, dtype=str,
            comments='#', ndmin=1)
        row_splits = [int(x) for x in subseq_req_file]
    else:
        row_splits=-1

    prepare_profile(epoch=epoch, ip1=None, port1=None, ip2=None, port2=None,
        ip3=None, port3=None, ip4=None, port4=None,
        pmap_buf_ptr=None, pmap_buf_len_ptr=None,
        data_stream=data_stream, outdir=outdir, row_splits=row_splits,
        profile_id=profile_id, percentile=percentile,
        over_provisioning=over_provisioning)
