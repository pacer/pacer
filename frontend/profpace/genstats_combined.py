#!/usr/bin/python
# Documentation:
# Given n files, Generate a combined plot based on values of column c
# Files should include n lines corresponding to n observations
# columns should space separated
# c is the column index to be plottes
import os
import sys
import argparse
from servstats import *
#from post_processing import *
import numpy
from matplotlib import pyplot
from math import floor
from math import ceil
import re

cmap = plt.get_cmap('jet')
hsv = plt.get_cmap('hsv')

colors_array = ['blue', 'red', 'green', 'yellow', 'darkturquoise', 'orange', 'lime', 'darkslategrey', 'fuchsia',
                'gray', 'teal']
# markers = ['*', '+', ',', '.', '<', '>', 'D', 'H', 'P', 'X', '^', 'd', 'h', 'o', 'p', 's', 'v', 'x', '|', '1', '0',
#           '3', '2', '5', '4', '7', '6', '9', '8']
markers = ['.', '+', ',', '*', 'X', '^', 'd', 'h', 'o', 'p', 's',
           'v', 'x', '|', '1', '<', '>', 'D', 'H', 'P', '3', '2', '4']
linestyles = ['-', '--', '-.', ':']


def build_plot(data_streams, columns, timeseries_columns, list_legends, issize,
    ishist, autobins, nbins, output, xstart, xend, title,xlabel, ylabel,
    percentile=100, widefig = True):
    """
    Build combined histograms and statistics based on given datastreams

    Args:
        datastreams: the loaded files, from which data will be consumed
        column: list of columns, corresponding to the column to consider in each datastream
        timeseries_col: column indexes to use for timestamps in corresponding datastreams, if a time series plot is wanted
    output: Plots and stats files
    """

#    print("plotting .... + Percentile " + str(percentile) + "\n")
    non_decimal = re.compile(r'[^\d.]+')
    non_int = re.compile(r'[^\d]+')

    if (widefig):
        fig_width = 20
        fig_height = 12
        plt.figure(figsize=(fig_width, fig_height))

    plt.title(title + " (" + str(percentile) + "%)" + ", Based on " +
              str(max([len(data) for data in data_streams])) + " data points")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    ax = plt.subplot(111)

    if len(data_streams) == 1:
        colors_array_full = cmap(numpy.linspace(0, 1.0, len(data_streams) + 1))
    else:
        colors_array_full = hsv(numpy.linspace(0, 1.0, len(data_streams) + 1))

    if autobins:
        xstart = sys.maxsize
        xend = 0

    histograms = []

    i = 0
    #print len(data_streams)
    for data in data_streams:
        if issize:
            histogram_data = [float(non_decimal.sub(
                "", data[r][columns[i]])) / 1024.0 for r in range(0, len(data))]
        else:
            #print columns[i]
            histogram_data = [float(non_decimal.sub(
                "", data[r][columns[i]])) for r in range(0, len(data))]

        if timeseries_columns[0] > -1:
            histogram_timestamp = [float(non_int.sub(
                "", data[r][timeseries_columns[i]])) for r in range(0, len(data))]
            histograms.append((histogram_timestamp, histogram_data))
        else:
            histograms.append(histogram_data)

        # print len(data)
        # print histogram_data
        if autobins and timeseries_columns[0] < 0:
            xstart = min((xstart), min(histogram_data))
            xend = max((xend), numpy.percentile(histogram_data, percentile))
        elif timeseries_columns[0] > -1:
            xstart = min((xstart), min(histogram_timestamp))
            xend = max((xend), max(histogram_timestamp))
        i += 1
    # print xstart
    # print xend

    if xstart == xend:
        xstart = min(0, xend - nbins)
        # OR
        #xend +=1

    if nbins > 0:
        xstep = ((ceil(xend) - floor(xstart)) / nbins)

    idx = 0
    if (timeseries_columns[0] >= 0):
        for j in range(0, len(histograms)):
            pyplot.plot(histograms[j][0], histograms[j][1], label=list_legends[j],
                linestyle=linestyles[int(idx / len(markers)) % len(linestyles)],
                marker=markers[idx % len(markers)], color=colors_array_full[idx],
                markersize=1, linewidth=1)
            idx += 1
    else:
        # print "NUMPY ARRANGE"
        # print xstep
        # print max(xstart - xstep, 0)
        #print (xend + xstep)
        bins = numpy.arange((max(xstart - xstep, 0)), (xend + xstep), step=xstep)
        bin_mids = [((bins[i] + bins[i + 1]) / 2)
                    for i in range(0, len(bins) - 1)]

        if len(bins) < 15:
            ticks = [ round(x, 2) for x in bins ]
        else:
            ticks = [ round(x, 2) for x in bins[0::int(floor(len(bins) / 15)) ] ]

#        ticks = [round(x, 2) for x in bins] if len(bins) < 15 else [round(x, 2)
#                                                                    for x in bins[0::int(floor(len(bins) / 15))]]
        pyplot.xticks(ticks, ticks, rotation=45)

        if (ishist == False):
            for j in range(0, len(histograms)):
                if(normalized_hist):
                    plt.gca().set_ylim([0, 1.0])
                    weights = numpy.ones_like(
                        histograms[j]) / float(len(histograms[j]))
                    hist, bin_edges = numpy.histogram(
                        histograms[j], bins,  weights=weights)
                else:
                    hist, bin_edges = numpy.histogram(histograms[j], bins)

                #bin_mids = [(bin_edges[i] + bin_edges[i + 1]) / 2 for i in range(0, len(bin_edges) - 1)]
                pyplot.plot(bin_mids, hist, label=list_legends[j],
                    linestyle=linestyles[int(idx / len(markers)) % len(linestyles)],
                    marker=markers[idx % len(markers)],
                    color=colors_array_full[idx], markersize=10)
                idx += 1
                compute_all_stats_from_raw_all_percentiles(histograms[j],
                    len(bins), output + list_legends[j] + ".stats", title)

        else:
            # print bins
            # print ticks
            # print histograms
            if (normalized_hist):
                plt.gca().set_ylim([0, 1.0])
                weights = [numpy.ones_like(
                    histograms[j]) / float(len(histograms[j])) for j in range(len(histograms))]
                pyplot.hist(histograms, bins, alpha=0.5, label=list_legends,
                            color=colors_array_full[0:len(histograms)],
                            weights=weights)
            else:
                pyplot.hist(histograms, bins, alpha=0.5, label=list_legends,
                            color=colors_array_full[0:len(histograms)])
            i = 0
            for h in histograms:
                compute_all_stats_from_raw_all_percentiles(
                    h, len(h), output + "_" + list_legends[i] + ".stats.txt", title)
                i += 1

    #plt.legend(loc="upper right")

    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    plt.savefig(output + "_" + str(percentile) + "p.png", dpi=None)
    plt.clf()
    plt.cla()
    plt.close()


def row_based_file_splitting(data_streams, row_based_splits, row_based_legends,
    columns, column_list_legends, timeseries_columns):

    i = 0
    final_data_streams = []
    final_columns_legends = []
    final_columns = []
    final_timeseries_columns = []
    for i in range(0, len(data_streams)):
        ds = data_streams[i]
        n_out_streams = int(row_based_splits[i])
        print(("n_out_streams " + str(n_out_streams)))
        if row_based_legends[0] == -1:
            row_based_legends = list(range(0, n_out_streams, 1))
        generated_streams = [[] for _ in range(0, n_out_streams)]
        j = 0
        for r in ds:
            generated_streams[j % n_out_streams].append(r)
            j += 1
        j = 0
        for gs in generated_streams:
            final_data_streams.append(generated_streams[j])
            final_columns.append(columns[i])
            final_columns_legends.append(
                str(row_based_legends[j]) + "_" + column_list_legends[i])
            final_timeseries_columns.append(timeseries_columns[i])
            j += 1
    return (final_data_streams, final_columns, final_columns_legends, final_timeseries_columns)


def row_filebased_file_splitting(data_streams, row_filebased_splits,
    row_based_legends, columns, column_list_legends, timeseries_columns):

    i = 0
    final_data_streams = []
    final_columns_legends = []
    final_columns = []
    final_timeseries_columns = []
    final_row_splits = []
    #print("Row file based splits = " + str(row_filebased_splits))
    #print("Number of data streams =  " +str(len(data_streams)))
    for i in range(0, len(data_streams)):
        ds = data_streams[i]
        # print len(ds)
        row_splits = [int(x) for x in numpy.loadtxt(
            row_filebased_splits[i], dtype=str, comments='#', ndmin=1)]
        final_row_splits.extend(row_splits)
        n_out_streams = max(row_splits)
        #print ("Num of out streams "+str(n_out_streams))
        if row_based_legends[0] == -1:
            row_based_legends = list(range(0, n_out_streams, 1))
        generated_streams = [[] for _ in range(0, n_out_streams)]
        j = 0
        count = 0
        for r in ds:
            generated_streams[count % n_out_streams].append(r)
            count += 1
            if count >= int(row_splits[j]):
                j += 1
                count = 0
        j = 0
        count = 0
        for gs in generated_streams:
            final_data_streams.append(generated_streams[j])
            final_columns.append(columns[i])
            final_columns_legends.append(
                str(row_based_legends[j]) + "_" + column_list_legends[i])
            final_timeseries_columns.append(timeseries_columns[i])
            j += 1
    return (final_data_streams, final_columns, final_columns_legends, final_timeseries_columns, final_row_splits)


if __name__ == "__main__":
    default_column_legends = ["request_ID", "frame_ID", "first_req_timestamp", "last_req_timetamp", "in_size",
                              "in_packet_count", "first_resp_timestamp", "last_resp_timestamp", "out_size",
                              "out_packet_count", "src_IP", "src_Port", "dst_IP", "dst_Port", "first_response_packet_latency",
                              "last_response_packet_latency", "req_inter_arrival_time", "req_pacing", "rsp_pacing",
                              "subsequent_req_inter_arrival_time", "crosstier_inter_sending_time"]
    parser = argparse.ArgumentParser(
        description='Generating combined stats and plots...')
    parser.add_argument('-f', '--listfiles', nargs='+', dest='list_files',
                        required=True, help='list_of_histogram files')
    parser.add_argument('-c', '--column',  nargs='+', dest='columns', required=True,
                        help='Column indexes to consider in corresponding files, Rule: (#columns = #files) or (#files = 1) or (#columns=1)')
    parser.add_argument('-l', '--collistlegends',  nargs='+', dest='column_list_legends',
                        default=[-1], help='list of corresponding representable names for columns(columns legends). Rule #col_legend_labels = max(#columns, #files)')
    parser.add_argument('-o', '--output', dest='output',
                        action='store', required=True, help='output png file')
    parser.add_argument('-normalize', '--normalize', dest='normalized', action='store_true',
                        default=False, help='Should histogram plots be normalized?')

    #parser.add_argument('--iter', dest='iter', action='store', required=False, help='exp iter file suffix')
    parser.add_argument('-t', '--title', dest='title',
                        action='store', required=True, help='title of the figure')

    parser.add_argument('-xl', '--xlabel', dest='xlabel',
                        action='store', required=True, help='label of the x-axis')
    parser.add_argument('-yl', '--ylabel', dest='ylabel',
                        action='store', required=True, help='label of the y-axis')
    parser.add_argument('-xstart', '--xstart', dest='xstart', action='store',
                        default=-1, required=False, help='start of the x-axis range')
    parser.add_argument('-xend', '--xend', dest='xend',
                        action='store', default=-1, help='end of the x-axis range')
    parser.add_argument('-s', '--issize', dest='issize', action='store_true',
                        help='Is it a size plot -> auto convert bytes to KB')
    parser.add_argument('-g', '--isHistogram', dest='ishist',
                        action='store_true', help='Shall a histogram be plotted?')
    #parser.add_argument('-l', '--islineplot', dest='islineplot', action='store_true', help='Shall a line plot be plotted be plotted?')
    parser.add_argument('-a', '--autobins', dest='autobins',
                        action='store_true', help='Autogenerate bins?')
    parser.add_argument('-w', '--widefig', dest='widefig',
                        action='store_true', help='use wide figure?')
    parser.add_argument('-ts', '--timeseries', dest='timeseries_columns', nargs='+', action='store',
                        default=[-1], help='column indexes to use for timestamps in corresponding files, if a time series plot is wanted')

    parser.add_argument('-split', '--split_graphs', dest='split_graphs',
                        action='store', default=0, help='Create extra separated plots')
    parser.add_argument('-p', '--percentile', dest='additional_percentile', action='store',
                        default=98, help='Additional plot of the requested percentile')

    actionRows = parser.add_mutually_exclusive_group(required=False)

    actionRows.add_argument('-r', '--rbasedsplits', dest='row_based_splits', nargs='+', action='store',
                            default=[-1], help='Split a file to n data streams, each nth row would go to a different data stream, give 3 labels')
    parser.add_argument('-rl', '--rbasedlegend', dest='row_based_legends', nargs='+', action='store',
                        default=[-1], help='list of corresponding representable names for rows(rows legends). Rule #row_legend_labels = #row_based_splits')
    actionRows.add_argument('-rfile', '--rfilebased', nargs='+', dest='row_filebased_splits',
                            action='store', default=[-1], help='For each file, specify a row grouping based on file')

    action = parser.add_mutually_exclusive_group(required=True)
    action.add_argument('-xstep', '--xstep', dest='xstep',
                        action='store', default=-1, help='step of the x-axis')
    action.add_argument('-nb', '--nbins', dest='nbins',
                        action='store', default=-1, help='number of bins')

    args = parser.parse_args()

    widefigure = args.widefig
    list_files = args.list_files
    columns = [ int(x) for x in args.columns ]
    column_list_legends = args.column_list_legends
    output = args.output
    normalized_hist = args.normalized
    split_graphs = int(args.split_graphs)
    #iter = int(args.iter)
    title = args.title
    xlabel = args.xlabel
    ylabel = args.ylabel
#    print(xlabel)
#    print(ylabel)
    xstart = int(args.xstart)
    xend = int(args.xend)
    xstep = int(args.xstep)
    issize = args.issize
    ishist = args.ishist
    #islineplot = args.islineplot
    autobins = args.autobins
    nbins = int(args.nbins)
    additional_percentile = int(args.additional_percentile)
    timeseries_columns = [ int(x) for x in args.timeseries_columns ]

    row_based_splits = args.row_based_splits
    row_based_legends = args.row_based_legends
    row_filebased_splits = args.row_filebased_splits
    # colors_array = matplotlib.colors.cnames.keys()
    # print timeseries_col

    # plt.style.use('ggplot')

    non_decimal = re.compile(r'[^\d.]+')
    non_int = re.compile(r'[^\d]+')

    data_streams = []
    for f in list_files:
        txtfile = numpy.loadtxt(f, dtype=str, comments='#', ndmin=1)
        # print txtfile.shape
        # print  txtfile.shape
        if str(txtfile.shape).startswith("(1,") or str(txtfile.shape).endswith(",)"):
            txtfile = [txtfile]
            print "Just one dimension? I'm making it 2D ;) "
        # print txtfile
        data_streams.append(txtfile)

    if len(list_files) == 1 and len(columns) > 1:
        data_streams = data_streams * len(columns)
        # print "Length of datastreams:"+ str(len(data_streams))

    if len(columns) == 1:
        columns = columns * len(data_streams)

    if column_list_legends[0] == -1:
        column_list_legends = [default_column_legends[x] for x in columns]
        # print  column_list_legends

    # if len(column_list_legends) == 1:
    column_list_legends = column_list_legends * len(data_streams)

    # if len(row_based_splits) == 1:
    row_based_splits = row_based_splits * len(data_streams)

    # if len(timeseries_columns) == 1:
    timeseries_columns = timeseries_columns * len(data_streams)

    # print(row_filebased_splits)
    if len(row_filebased_splits) == 1:
        row_filebased_splits = row_filebased_splits * len(data_streams)

    if row_based_splits[0] != -1:
        (final_data_streams, final_columns, final_columns_legends, final_timeseries_columns) = \
            row_based_file_splitting(data_streams, row_based_splits,
                row_based_legends, columns, column_list_legends,
                timeseries_columns)
        build_plot(final_data_streams, final_columns, final_timeseries_columns,
            final_columns_legends, issize, ishist, autobins, nbins, output,
            xstart, xend, title, xlabel, ylabel, widefig = widefigure)
        # if not issize:
        # print "NOT SIZE,  additional percentile will be computed"
        build_plot(final_data_streams, final_columns, final_timeseries_columns,
            final_columns_legends, issize, ishist, autobins, nbins, output,
            xstart, xend, title, xlabel, ylabel, additional_percentile,
            widefig = widefigure)
        exit()

    if row_filebased_splits[0] != -1:
        (final_data_streams, final_columns, final_columns_legends, final_timeseries_columns, final_row_splits) = \
            row_filebased_file_splitting(data_streams, row_filebased_splits,
                row_based_legends, columns, column_list_legends, timeseries_columns)

        build_plot(final_data_streams, final_columns, final_timeseries_columns,
            final_columns_legends, issize, ishist, autobins, nbins, output,
            xstart, xend, title, xlabel, ylabel, widefig = widefigure)
        # if not issize:
        # print "NOT SIZE, additional percentile  will be computed"
        build_plot(final_data_streams, final_columns, final_timeseries_columns,
            final_columns_legends, issize, ishist, autobins, nbins, output,
            xstart, xend, title, xlabel, ylabel, additional_percentile,
            widefig = widefigure)
        #build_plot(final_data_streams, final_columns, final_timeseries_columns, final_columns_legends, issize, ishist, autobins, nbins, output, xstart, xend, title,additional_percentile)

        if split_graphs == 1:
            ds_idx = 0
            for ds in final_data_streams:
                #print [ds]
                #print [final_columns[ds_idx]]
                #print [final_timeseries_columns[ds_idx]]
                #print [final_columns_legends[ds_idx]]
                build_plot([ds], [final_columns[ds_idx]],
                    [ final_timeseries_columns[ds_idx] ],
                    [ final_columns_legends[ds_idx] ], issize, ishist, autobins,
                    nbins, output + "_" + str(ds_idx), xstart, xend,
                    title + "_" + str(ds_idx), xlabel, ylabel,
                    widefig = widefigure)

                build_plot([ds], [final_columns[ds_idx]],
                    [ final_timeseries_columns[ds_idx] ],
                    [ final_columns_legends[ds_idx] ], issize, ishist, autobins,
                    nbins, output + "_" + str(ds_idx), xstart, xend,
                    title + "_" + str(ds_idx), xlabel, ylabel,
                    additional_percentile, widefig = widefigure)
                ds_idx += 1
        exit()
    else:
#        print "Length of datastreams:" + str(len(data_streams))
        build_plot(data_streams, columns, timeseries_columns,
            column_list_legends, issize, ishist, autobins, nbins, output,
            xstart, xend, title, xlabel, ylabel, widefig = widefigure)

        # if not issize:
        # print "NOT SIZE, additional percentile will be computed"
        if len(timeseries_columns) ==  0:
            build_plot(data_streams, columns, timeseries_columns,
                column_list_legends, issize, ishist, autobins, nbins, output,
                xstart, xend, title, xlabel, ylabel, additional_percentile,
                widefig = widefigure)
