#!/usr/bin/env python

## imports from pre-defined libraries
import copy
import os
import subprocess
import sys
import time
import signal
import argparse
import resource
import struct
import pprint
import itertools
from os import listdir
from os.path import isfile, join
from ctypes import *
from mysql.connector import errorcode
from multiprocessing import Process
from genstats_combined import build_plot
import collections
sys.path.insert(0, "./kernel_logs_analyzer")

## ioctl number: _IO('S', 0)
SME_IOCTL = 21248

## imports from user libraries
from imports import (
    create_client_socket,
    close_client_socket,
    alloc_log_consumer,
    prepare_log_consumer,
    free_log_consumer,
    get_next_log,
    free_buf,
    free_buffer_hashtable,
    sendto_socket,
    recvfrom_socket,
    print_consumed_log,
    create_profile_map,
    print_profile_map_buf,
    print_profile_map_buf_ihash,
    print_profile_map_buf_ihash_hashtable,
    dev_open,
    dev_close,
    dev_ioctl
    )
from prepare_profile import (
    parse_profile,
    prepare_profile,
    prepare_profile_oo
    )
from lib_binary_profiles import (
    send_to_backend_slave,
    prepare_profile_map_binary
    )
import log_parser
from log_parser import *
from config_sme_profiler import (
    USE_MMAP_PROFILE_DATABASE_64bit,
    USE_IOCTL_PROFILE_DATABASE_64bit,
    MULTI_TIER_PROFILING,
    FINAL_EXPERIMENTS,
    COLLECT_LOG_RECORDS,
    SME_PROFILER_STATS,
    SME_PROFILER_STORE_KERNEL_MESSAGES,
    SME_PROFILER_DBG_LVL,
    LVL_DBG,
    LVL_WARN,
    LVL_ERR,
    LVL_EXP
    )

import fcntl

PROFILER_ROOT_DIR = "/local/sme/daemon_profiler"
my_sme_prof = None
my_sme_pace = None

def sme_sig_handler(signal, frame):
    global my_sme_prof, my_sme_pace
    print "rcvd signal: %d in %d" %  (signal, os.getpid())
    if (my_sme_prof and os.getpid() == my_sme_prof.thread.pid):
        my_sme_prof.cleanup_profiler()
    elif (my_sme_pace and os.getpid() == my_sme_pace.thread.pid):
        my_sme_pace.cleanup_profiler()
    sys.exit(0)


def run_forever(sme_obj):
    sme_obj.setup_profiler()
    my_log_buf = POINTER(c_char)()
    my_log_buf_len = c_int()
    sme_obj.consumed_reqs_so_far = 0
    empty_loop_cnt = 0

    while True:
        ## get log from kernel
        ret = get_next_log(sme_obj.sme_log_cons, byref(my_log_buf),
            byref(my_log_buf_len))

        if ret < 0:
            empty_loop_cnt += 1
            if (empty_loop_cnt % 10 == 0):
                print "[%d:%s] Logged %d of %d, waited for %d loops"  \
                    % (sme_obj.thread.pid, sme_obj.thread.name,
                        sme_obj.consumed_reqs_so_far, sme_obj.prof_for_n_req,
                        empty_loop_cnt)
            if (empty_loop_cnt >= 60):
                print "[%d:%s] Logged %d of %d, waited too long. Exiting.." \
                    % (sme_obj.thread.pid, sme_obj.thread.name,
                        sme_obj.consumed_reqs_so_far, sme_obj.prof_for_n_req)
                if sme_obj.only_dump_no_profiling:
                    break
                sme_obj.gen_profile()
                if (int(sme_obj.update_prof_freq) > 0):
                    sme_obj.send_profiles_to_kernel()
                break
            time.sleep(2)
            continue

        empty_loop_cnt = 0

        if SME_PROFILER_DBG_LVL <= LVL_DBG:
            print "===== [%d:%s, LOG_ID: %d] =====" % (sme_obj.thread.pid,
                sme_obj.thread.name, sme_obj.num_logs)
            print_consumed_log(my_log_buf, my_log_buf_len)

        log_buf_len = my_log_buf_len.value
        log_buf = my_log_buf[0:log_buf_len]

        if sme_obj.keep_logs:
            if FINAL_EXPERIMENTS:
                log_date_time = str(int(round(time.time() * 1000)))
                log_date_time += "."+ str(sme_obj.num_logs)
            else:
                log_date_time = str(sme_obj.num_logs)
            log_file = sme_obj.keep_logs_dir + "/" + log_date_time
            with open(log_file, 'ab') as f:
                f.write(log_buf)

        sme_obj.num_logs += 1
        
        if sme_obj.only_dump_no_profiling:
            if USE_MMAP_PROFILE_DATABASE_64bit:
                free_buffer_hashtable(byref(my_log_buf), my_log_buf_len)
            else:
                free_buf(byref(my_log_buf), my_log_buf_len)

            continue

        ## consume and profile
        consumed = sme_obj.consume_profile_and_send(log_buf, log_buf_len)
        if USE_MMAP_PROFILE_DATABASE_64bit:
            free_buffer_hashtable(byref(my_log_buf), my_log_buf_len)
        else:
            free_buf(byref(my_log_buf), my_log_buf_len)

        sme_obj.consumed_reqs_so_far += consumed

        if (sme_obj.prof_for_n_req <> -1 and
                sme_obj.consumed_reqs_so_far >= sme_obj.prof_for_n_req):
            print "[%d:%s] Done profiling! #requests specified %d, profiled %d" \
                % (sme_obj.thread.pid, sme_obj.thread.name,
                    sme_obj.prof_for_n_req, sme_obj.consumed_reqs_so_far)
            break

        ## end of while True loop
    sme_obj.cleanup_profiler()
    return


def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, basestring):
            for sub in flatten(el):
                yield sub
        else:
            yield el

class SMEProfiler:
    def __init__(self, dummy_ip, dummy_port, src_ip, src_port,
            in_ip, in_port, out_ip, out_port, dst_ip_list, dst_port, slave_ip, slave_port,
            update_prof_freq, generate_prof_freq, prof_for_n_req = -1,
            keep_logs = 0, keep_analysed_logs = 0,  use_logs_dir = "", reuse_older_logs = 1, only_dump_no_profiling = 1,
            dev = "/dev/SMECTRL"):
        self.percentile = {
                "fe_pkt": 100,
                "fe_pac": 100,
                "fe_lat": 99,
                "ct_pkt": 100,
                "ct_pac": 100,
                "ct_lat": 100,
                "be_pkt": 100,
                "be_pac": 100,
                "be_lat": 100,
            }
        self.overprov = {
                "fe_pkt": 5,
                "fe_pac": 0,
                "fe_lat": 0,
                "ct_pkt": 0,
                "ct_pac": 0,
                "ct_lat": 0,
                "be_pkt": 0,
                "be_pac": 0,
                "be_lat": 0,
                "extra_queries": 10,
            }

        self.sme_ctrl_fd = -1
        self.dummy_ip = dummy_ip
        self.sme_log_cons = c_char_p()
        self.sme_ctrldev = dev
        self.only_dump_no_profiling = only_dump_no_profiling
        self.dummy_port = dummy_port
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.src_ip = src_ip
        self.src_port = src_port
        self.in_ip = in_ip
        self.in_port = in_port
        self.out_ip = out_ip
        self.out_port = out_port
        self.dst_ip_list = dst_ip_list
        self.dst_port = dst_port
        self.update_prof_freq = update_prof_freq
        self.generate_prof_freq = generate_prof_freq
        self.prof_for_n_req = prof_for_n_req
        self.keep_logs = keep_logs
        self.keep_analysed_logs = keep_analysed_logs
        self.use_logs_dir = use_logs_dir
        self.profile_dir = ""
        self.keep_logs_dir = ""
        """keep_analysed_logs_dir is no longer in use"""
        self.keep_analysed_logs_dir = ""
        self.genprof_dir = ""
        self.msg_dir = ""
        self.collect_logs_dir = ""
        self.thread = None
        """Connections that have not been fully linked yet"""
        self.unlinked_connections = []
        
        """ Background compaitable array based serialization.
        Required for subsequent statistics"""
        self.client_observations = []
        self.crosstier_observations = []
        self.crosstier_reqs_per_session_count = []
        
        """
        This is the only set of logs required for profiling.
        Cross-tier logs are linked with their corresponding FE logs.
        """
        #self.frontend_logs = []
        """
        Dictionary for public_hashes, used to accumulate
        observations for each public hash till it is time for
        a profile to be generated for a given public hash.
        """
        self.public_dictionary =  dict()
        """
        A dictionary to keep track of #requests that have
        been used in previous profiles.
        Only used in case previous observations should be
        reused in profiling. If previous observations are
        dropped, this map will always have zero previousely
        profiled requests.
        """
        self.public_dictionary_profiled_requests = dict()
        self.reuse_older_logs = reuse_older_logs
        """
        Generated profiles for each tier, accumulated till
        it is time to send to kernel.
        """
        self.client_profiles = dict()
        self.fe_be_profiles = dict()
        self.be_fe_profiles = dict()
        self.consumed_reqs_so_far = 0
        self.num_logs = 0
        self.kernel_messages_count = 0
        """Statistics"""
        self.total_profile_generation_time = 0
        self.total_profile_generation_memory_usage = 0
        self.total_parsing_linking_analysis_time = 0
        self.total_parsing_linking_analysis_memory_usage = 0



    def __str__(self):
        p_str = ""
        p_str += "dummy: " + self.dummy_ip + ", " + str(self.dummy_port) + "\n"
        p_str += "src: " + self.src_ip + ", " + str(self.src_port) + "\n"
        p_str += "in: " + self.in_ip + ", " + str(self.in_port) + "\n"
        p_str += "out: " + self.out_ip + ", " + str(self.out_port) + "\n"
        p_str += "dst: " + str(self.dst_ip_list) + ", " + str(self.dst_port) + "\n"
        p_str += "sme_ctrl_fd: " + str(self.sme_ctrl_fd) + "\n"
        p_str += "sme_log_cons: " + str(self.sme_log_cons) + "\n"
        p_str += "profile update frequency: " + str(self.update_prof_freq) + "\n"
        return p_str


    def set_dirs(self, profdir=PROFILER_ROOT_DIR, keep_logs_dir="kernel_logs",
            keep_analysed_logs_dir="analysed_logs",
            genprof_dir="generated_profiles", msg_dir="buffers_sent", collect_logs_dir="observations"):
        self.profile_dir=profdir
        print "Profile directory: " + self.profile_dir
        if self.keep_logs != 0:
            self.keep_logs_dir = self.profile_dir + "/" + keep_logs_dir
            self.keep_analysed_logs_dir = (self.keep_logs_dir + "/"
                    + keep_analysed_logs_dir)
        else:
            self.keep_logs_dir = ""
        self.genprof_dir = self.profile_dir + "/" + genprof_dir
        self.msg_dir = self.profile_dir + "/" + msg_dir
        self.collect_logs_dir = self.profile_dir + "/" + collect_logs_dir

        if not os.path.exists(self.profile_dir):
            os.makedirs(self.profile_dir)
        if self.keep_logs != 0:
            if not os.path.exists(self.keep_logs_dir):
                os.makedirs(self.keep_logs_dir)
            if not os.path.exists(self.keep_analysed_logs_dir):
                os.makedirs(self.keep_analysed_logs_dir)
        if not os.path.exists(self.genprof_dir):
            os.makedirs(self.genprof_dir)
        if SME_PROFILER_STORE_KERNEL_MESSAGES:
            if not os.path.exists(self.msg_dir):
                os.makedirs(self.msg_dir)
        if COLLECT_LOG_RECORDS:
            if not os.path.exists(self.collect_logs_dir):
                os.makedirs(self.collect_logs_dir)



    def setup_profiler(self):
        if USE_IOCTL_PROFILE_DATABASE_64bit:
            self.sme_ctrl_fd = dev_open(self.sme_ctrldev)

        alloc_log_consumer(byref(self.sme_log_cons))
        ret = prepare_log_consumer(self.sme_ctrldev, self.sme_log_cons)
        if ret < 0:
            self.cleanup_profiler()
            sys.exit(ret)


    def cleanup_profiler(self):
        if USE_IOCTL_PROFILE_DATABASE_64bit:
            dev_close(self.sme_ctrl_fd)

        free_log_consumer(byref(self.sme_log_cons))
        print "[%d:%s] Logged %d, id %d " % (self.thread.pid, self.thread.name,
            self.consumed_reqs_so_far, self.num_logs)

        if self.only_dump_no_profiling:
            return

        if SME_PROFILER_STATS:
            print "LOG parsing, linking, and analysis time is " + str(self.total_parsing_linking_analysis_time)
            print "LOG parsing, linking, and analysis memory consumed is " + str(self.total_parsing_linking_analysis_memory_usage) + " bytes"
            print "Profile generations time is: " + str(self.total_profile_generation_time)
            print "Profile generation memory consumed is: " + str(self.total_profile_generation_memory_usage) + " bytes"
        print "List of stray connections: " +str(self.unlinked_connections)
        for uc in self.unlinked_connections:
            print [uc, uc.is_crosstier]
        print "[%d:%s] SME profiler: done cleanup!" % (self.thread.pid,
            self.thread.name)
        if self.keep_analysed_logs:
            self.store_analysed_logs()

    def store_analysed_logs(self):
        if self.keep_analysed_logs:
            if SME_PROFILER_DBG_LVL <= LVL_DBG:
                print "===== Logging analysed logs ====="
            keys = self.public_dictionary.keys()
            ## keys that are ready for generating a profile for
            keys_to_profile = []
            #if SME_PROFILER_DBG_LVL <= LVL_DBG:
            #    print "Serializing logs of keys:"
            #    print keys_to_profile

            for k in keys:
                (client, crosstier, count) = self.serialise_logs_observations(self.public_dictionary[k])
                self.client_observations = client
                self.crosstier_observations = crosstier
                self.crosstier_reqs_per_session_count = count

                #insert id for the frontend requests
                idx = 1
                for entry in self.client_observations:
                    entry[0] = str(idx)
                    idx = idx + 1
                    #print entry
                    #print idx
                #print len(self.client_observations)
                build_plot([self.client_observations], columns = [15],
                        timeseries_columns = [0],
                        list_legends = ["first response pkt latency"],
                        issize = False, ishist = False,
                        autobins = True, nbins = 1,
                        output = "first_client_response_pkt_latency_timeseries", 
                        xstart = -1, xend = -1, title = "Timeseries",
                        xlabel= "Request #", ylabel="latency (ms)", percentile=100)
                #TODO: K should be in the file name

                if FINAL_EXPERIMENTS:
                    log_date_time =  time.strftime("%d_%m_%Y-%H_%M_%S")
                else:
                    log_date_time = "dump"
                log_file = self.keep_analysed_logs_dir+"/"+log_date_time+".FE_CLIENT.analysed_logs.txt"
                print "writing logs to: " + log_file

                with open(log_file, 'w') as f:
                    f.write('\n'.join(['\t'.join(obs) for obs in self.client_observations]))
                log_file = self.keep_analysed_logs_dir+"/"+log_date_time+".FE_BE.analysed_logs.txt"
                print "writing logs to: " + log_file
                with open(log_file, 'w') as f:
                    f.write('\n'.join(['\t'.join(obs) for obs in self.crosstier_observations]))
                log_file = self.keep_analysed_logs_dir+"/"+log_date_time+".FE_BE_COUNT.analysed_logs.txt"
                print "writing logs to: " + log_file
                with open(log_file, 'w') as f:
                    f.write('\n'.join( [str(i) for i in self.crosstier_reqs_per_session_count]))


    def consume_profile_and_send(self, my_log_buf, my_log_buf_len):
        """
        This is the main function that takes as an input a set of kernel logs,
        orchestrates all other parts of the pipeline, decides whether to
        generate a profile and whether to send it to the kernel or not.
        """

        if SME_PROFILER_STATS:
            resource_before = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
            time_start = time.clock()

        ## Parse, link and analyse kernel logs
        if MULTI_TIER_PROFILING:

            if SME_PROFILER_DBG_LVL <= LVL_DBG:
                print "Starting new log consumption, carried over stray connections:"
                print self.unlinked_connections
            parsed_connections = log_parser.parse_logs(my_log_buf, my_log_buf_len)
            if SME_PROFILER_DBG_LVL <= LVL_DBG:
                print "Parsed Connections:"
                print parsed_connections
            
            if parsed_connections:
                analysed_standalone_connections  = log_parser.analyse_standalone_connections(parsed_connections)
                if SME_PROFILER_DBG_LVL <= LVL_DBG:
                    print "Analysed standalone connections:"
                    print analysed_standalone_connections
                self.unlinked_connections.extend(analysed_standalone_connections)
                if SME_PROFILER_DBG_LVL <= LVL_DBG:
                    print "Unlinked Connections so far"
                    for uc in self.unlinked_connections:
                        print uc
                (ready_frontend_connections, stray_connections) = log_parser.link_connections(self.unlinked_connections)
                self.unlinked_connections = stray_connections
                if SME_PROFILER_DBG_LVL <= LVL_DBG:
                    print "Stray connections after linking"
                    print self.unlinked_connections
                curr_frontend_logs = log_parser.analyse_combined_connections(ready_frontend_connections)
        else:
            parsed_connections = log_parser.parse_logs(my_log_buf, my_log_buf_len)
            if SME_PROFILER_DBG_LVL <= LVL_DBG:
                print "Parsed Connections:"
                print parsed_connections

            if parsed_connections:
                analysed_standalone_connections  = log_parser.analyse_standalone_connections(parsed_connections)
                if SME_PROFILER_DBG_LVL <= LVL_DBG:
                    print "Analysed standalone connections:"
                    print analysed_standalone_connections

                curr_frontend_logs = [c.log_observations for c in analysed_standalone_connections] 

        if SME_PROFILER_STATS:
            time_end = time.clock()
            time_elapsed = (time_end - time_start)
            resource_after= resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
            resources_used = resource_after - resource_before
            self.total_parsing_linking_analysis_time += time_elapsed
            self.total_parsing_linking_analysis_memory_usage += resources_used
        client_requests_consumed = 0

                
        
        ## if no front end logs have been generated, due to possibly corrupted
        ## kernel logs, we skip
        if not curr_frontend_logs and not MULTI_TIER_PROFILING:
            if SME_PROFILER_DBG_LVL <= LVL_WARN:
                print "[WARN] Current frontend logs empty"
            return client_requests_consumed

        ## extend the set of front end logs for later use,
        ## when a profile is to be generated
        #self.frontend_logs.extend(curr_frontend_logs)

        ## classify each public request into a bin based on its public hash
        for fe_logs in curr_frontend_logs:
            for fe_log in fe_logs:
                """Stop accounting for logs if prof_for_n_req limit is reached"""
                if (self.prof_for_n_req <> -1 and
                    self.consumed_reqs_so_far + client_requests_consumed >= self.prof_for_n_req):
                    break
                if SME_PROFILER_DBG_LVL <= LVL_DBG:
                    print "Adding Frontend Log to dictionary:"
                    print fe_log
                key = ','.join(fe_log.public_hashes)
                client_requests_consumed = client_requests_consumed + 1
                if self.public_dictionary.has_key(key):
                    self.public_dictionary[key].append(fe_log)
                else:
                    self.public_dictionary[key] = [fe_log]
                    self.public_dictionary_profiled_requests[key] = 0
                
               

        ## Check if we need to profile and send new profiles to kernel
        self.profile_and_send_new_profiles()
        if SME_PROFILER_DBG_LVL < LVL_DBG:
            print "Kernel Log consumed"
            print "Stray connections for next iteration:"
            print self.unlinked_connections

        return client_requests_consumed

    def profile_and_send_new_profiles(self):
        """
        This function checks if any public input have reached a
        threshold were it should be profiled,
        then checks if a profile should be sent to the kernel or not
        """


        if SME_PROFILER_DBG_LVL <= LVL_DBG:
            print "=== starting the profiling phase ==="
        keys = self.public_dictionary.keys()

        ## keys that are ready for generating a profile for
        keys_to_profile = []
        for k in keys:
            num_acc_reqs = (len(self.public_dictionary[k])
                    - self.public_dictionary_profiled_requests[k])
            if (num_acc_reqs >= self.generate_prof_freq):
                keys_to_profile.append(k)
    
        if SME_PROFILER_DBG_LVL <= LVL_DBG:
            print "Keys to profile:"
            print keys_to_profile

        for k in keys_to_profile:
            if SME_PROFILER_STATS:
                resource_before =   \
                    resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
                time_start = time.clock()

            ## prepare the profile i
            if self.reuse_older_logs:
                ## send all logs for profiling till the current batch
                nreqs = self.public_dictionary_profiled_requests[k] \
                        + self.generate_prof_freq
                self.prepare_profiles(self.public_dictionary[k][0:nreqs])
                self.public_dictionary_profiled_requests[k] = nreqs
            else:
                nreqs = self.generate_prof_freq
                self.prepare_profiles(self.public_dictionary[k][0:nreqs])
                self.public_dictionary[k] = self.public_dictionary[k][nreqs:]

            ## decide whether a profile should be sent to the kernel or not
            # if int(self.update_prof_freq) > 0:
            self.send_profiles_to_kernel()

            if SME_PROFILER_STATS:
                time_end = time.clock()
                time_elapsed = (time_end - time_start)

                resource_after =    \
                    resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
                resources_used = resource_after-resource_before

                self.total_profile_generation_time += time_elapsed 
                self.total_profile_generation_memory_usage += resources_used

            ## should this key be re-profiled?
            num_acc_reqs = (len(self.public_dictionary[k])
                    - self.public_dictionary_profiled_requests[k])
            if (num_acc_reqs >= self.generate_prof_freq):
                print "Need to profile more"
                keys_to_profile.append(k)


    def gen_profile(self):
        """
        Generate profile for all keys, irrespective of gen_prof_freq.
        Use this in run_forever, when kernel somehow does not generate
        as many logs as specified by gen_prof_freq.
        """
        keys = self.public_dictionary.keys()
        for k in keys:
            nreqs = len(self.public_dictionary[k][0:])
            self.prepare_profiles(self.public_dictionary[k][0:])
            self.public_dictionary_profiled_requests[k] = nreqs

    def serialise_logs_observations(self, frontend_logs):
        frontend_logs =  [obs for obs in flatten(frontend_logs)]
        #print frontend_logs
        serialization =  ([lo.serialise() for lo in frontend_logs],
                [lo.serialise() for lo in list(itertools.chain.from_iterable(
                    [fe.backend_observations for fe in frontend_logs]))],
            #[lo.serialise() for lo in [be_log for be_log in frontend_logs.backend_observations]],
            [len(fe.backend_observations) for fe in frontend_logs])
        return serialization


    def prepare_profiles(self, frontend_logs):

        #PROFILES_DIR = self.profile_dir + "generated_profiles/"
        #if not os.path.exists(PROFILES_DIR):
        #    os.makedirs(PROFILES_DIR)
        if len(frontend_logs) == 0:
            return

        epoch_now = int(round(time.time() * 1000))
        
        if COLLECT_LOG_RECORDS:
            with open(self.collect_logs_dir+"/"+'_'.join([str(s).encode('hex') for s in frontend_logs[0].public_hashes]) +"_"+str(epoch_now)+"_observations.txt", 'w+') as f:
                for log in frontend_logs:
                    f.write(log.mini_str() + "\n")

        ## prepare profiles
        (client_profile, fe_be_profile, be_fe_profile) = \
            prepare_profile_oo(epoch=epoch_now,
            frontend_analysed_logs=frontend_logs, outdir=self.genprof_dir,
            percentile=self.percentile, over_provisioning=self.overprov,
            backend_crosstier_profile=MULTI_TIER_PROFILING)

        ## accumulate profiles for sending later
        if client_profile:
            self.client_profiles[client_profile.profile_id] = client_profile
        if fe_be_profile:
            self.fe_be_profiles[fe_be_profile.profile_id] = fe_be_profile
        if be_fe_profile:
            self.be_fe_profiles[be_fe_profile.profile_id] = be_fe_profile

        # if SME_PROFILER_DBG_LVL <= LVL_DBG:
        #     print "In-Memory profile maps:"
        #     print self.client_profiles
        #     print self.fe_be_profiles
        #     print self.be_fe_profiles
        #profile_ids = [cp.profile_id for cp in self.client_profiles]


    def send_binary_profile_to_kernel(self, pmap_buf, pmap_buf_len, dst_ip,
        send_backend=0):
        """
        send a profile map to the kernel
        """

        
        if (int(self.update_prof_freq) <= 0):
            return

        if send_backend == 0:
            #fcntl.ioctl(self.sme_ctrl_fd, SME_IOCTL,
            #        pmap_buf[0:(pmap_buf_len.value)])
            dev_ioctl(self.sme_ctrl_fd, SME_IOCTL,
                    pmap_buf[0:(pmap_buf_len.value)], int(pmap_buf_len.value))
            ret = 0

            if self.thread:
                print "[%d:%s] SEND PMAP TO FE/CT (dst: %s) len %d ret %d"   \
                    % (self.thread.pid, self.thread.name, dst_ip, pmap_buf_len.value, ret)
            else:
                print "[%d] SEND PMAP TO FE/CT (dst: %s) len %d ret %d"   \
                    % (os.getpid(), dst_ip, pmap_buf_len.value, ret)


    def prepare_send_pmap(self, ip1, port1, ip2, port2, ip3, port3, ip4, port4,
            profiles, tier_string, send_backend=0):
        """ Prepare a profile map and send it to the kernel"""
        pmap_buf = POINTER(c_char)()
        if USE_MMAP_PROFILE_DATABASE_64bit:
            pmap_buf_len = c_uint64()
        else:
            pmap_buf_len = c_uint64()

        prepare_profile_map_binary(byref(pmap_buf), byref(pmap_buf_len),
            ip1, port1, ip2, port2, ip3, port3, ip4, port4,
            [profile.num_out_reqs for profile in profiles],
            [profile.num_extra_slots for profile in profiles],
            [profile.num_out_frames for profile in profiles],
            [profile.frame_spacing for profile in profiles],
            [profile.frame_latency for profile in profiles],
            [profile.profile_id for profile in profiles])

        if SME_PROFILER_DBG_LVL <= LVL_DBG:
            print "\n=== printing pmap buf of " + tier_string + " ==="
            if USE_MMAP_PROFILE_DATABASE_64bit:
                print_profile_map_buf_ihash_hashtable(pmap_buf[16:(pmap_buf_len.value)],
                    pmap_buf_len.value-16)
            else:
                print_profile_map_buf_ihash(pmap_buf[16:(pmap_buf_len.value)],
                        pmap_buf_len.value-16)
            print "\n=== printing binary buffer (%d) ===" % (pmap_buf_len.value)
            print (str(pmap_buf[0:(pmap_buf_len.value)]).encode('hex_codec')).upper()

        self.send_binary_profile_to_kernel(pmap_buf, pmap_buf_len, ip4,
            send_backend)

        if SME_PROFILER_STORE_KERNEL_MESSAGES:
            if FINAL_EXPERIMENTS:
                epoch_now = int(round(time.time() * 1000))
            else:
                epoch_now = "message"
            fpath = self.msg_dir+"/"+tier_string+"."+str(epoch_now)+".bytes"
            if SME_PROFILER_DBG_LVL == LVL_DBG:
                with open(fpath, 'wb+') as f:
                    f.write(pmap_buf[0:pmap_buf_len.value])
                    self.kernel_messages_count = self.kernel_messages_count + 1

        ## cleanup
        if USE_MMAP_PROFILE_DATABASE_64bit:
            free_buffer_hashtable(byref(pmap_buf), pmap_buf_len)
        else:
            free_buf(byref(pmap_buf), pmap_buf_len.value)


    def send_profiles_to_kernel(self):
        """
        Send all available profiles to kernel, delete all im memory profiles
        """

        if len(self.client_profiles) < 1:
            return

        if SME_PROFILER_DBG_LVL < LVL_DBG:
            print "Preparing profile messages for the following public hashes"
            print [client_profile.public_hashes
                for client_profile in self.client_profiles.values()]

        if len(self.be_fe_profiles) > 0:
            ip1=self.out_ip
            port1=int(self.out_port)
            ip2=self.dst_ip
            port2=int(self.dst_port)
            ip3=self.dst_ip
            port3=int(self.dst_port)
            ip4=self.out_ip
            port4=int(self.out_port)

            self.prepare_send_pmap(ip1, port1, ip2, port2, ip3, port3, ip4, port4,
                self.be_fe_profiles.values(), "BE-FE", send_backend=1)
            self.be_fe_profiles.clear()       
            
        if len(self.client_profiles) > 0:
            ip1=self.src_ip
            port1=int(self.src_port)
            ip2=self.in_ip
            port2=int(self.in_port)
            ip3=self.in_ip
            port3=int(self.in_port)
            ip4=self.src_ip
            port4=int(self.src_port)

            self.prepare_send_pmap(ip1, port1, ip2, port2, ip3, port3,
                ip4, port4, self.client_profiles.values(), "FE-Client")
            self.client_profiles.clear()

        if len(self.fe_be_profiles) > 0:
            for dst_ip in self.dst_ip_list:
                ip1=self.src_ip
                port1=int(self.src_port)
                ip2=self.in_ip
                port2=int(self.in_port)
                ip3=self.out_ip
                port3=int(self.out_port)
                ip4=dst_ip
                port4=int(self.dst_port)

                self.prepare_send_pmap(ip1, port1, ip2, port2, ip3, port3,
                    ip4, port4, self.fe_be_profiles.values(), "FE-BE")

            self.fe_be_profiles.clear()


    def run_on_directory(self):
        if USE_IOCTL_PROFILE_DATABASE_64bit:
            self.sme_ctrl_fd = dev_open(self.sme_ctrldev)

        ## read logs directory
        logs_to_consume = []
        for f in listdir(self.use_logs_dir):
            joined_filename = join(self.use_logs_dir, f)
            if isfile(joined_filename):
                logs_to_consume.append(joined_filename)
        logs_to_consume.sort(key=lambda x: os.path.getmtime(x))

        self.consumed_reqs_so_far = 0
        for log_file in logs_to_consume:
            with open(log_file,'rb') as f:
                buff = f.read()

            if SME_PROFILER_DBG_LVL <= LVL_DBG:
                print "Consuming %s" % log_file
                print "===== [LOG_ID: %d] =====" % self.num_logs
                print_consumed_log(buff, len(buff))
            self.num_logs += 1

            if len(buff) == 0:
                continue

            ## consume and profile
            consumed = my_sme_prof.consume_profile_and_send(buff, len(buff))
            if consumed <= 0 and SME_PROFILER_DBG_LVL <= LVL_WARN and not MULTI_TIER_PROFILING:
                print "[WARN] No valid frontend logs in file %s, file len %d"   \
                        % (log_file, len(buff))

            self.consumed_reqs_so_far += consumed

            if int(self.num_logs) % 1000 == 0:
                print "consumed logs so far: " + str(self.num_logs) 
                print "consumed requests so far: " + str(self.consumed_reqs_so_far)

            if (self.prof_for_n_req <> -1 and
                    self.consumed_reqs_so_far >= self.prof_for_n_req):

                if USE_IOCTL_PROFILE_DATABASE_64bit:
                    dev_close(self.sme_ctrl_fd)

                print "Done profiling! #requests specified: %d, profiled: %d" \
                    % (self.prof_for_n_req, self.consumed_reqs_so_far)
                break

        print "#client requests consumed %d" % self.consumed_reqs_so_far
        print "List of stray connections: " +str(self.unlinked_connections)
        
        print "Public dictionary has the following (key,count)"
        keys = self.public_dictionary.keys()
        
        for k in keys:
            print (k, len(self.public_dictionary[k]))

        self.gen_profile()

        self.send_profiles_to_kernel()

        for uc in self.unlinked_connections:
            print uc

        if USE_IOCTL_PROFILE_DATABASE_64bit:
            dev_close(self.sme_ctrl_fd)

        if self.keep_analysed_logs:
            self.store_analysed_logs()

    def send_ascii_profiles_dir(self, directory):

        """
        Send previously generated ascii profiles,
        which are stored in the given directory.
        """
        if USE_IOCTL_PROFILE_DATABASE_64bit:
            self.sme_ctrl_fd = dev_open(self.sme_ctrldev)

        profile_files_to_send = []
        for f in listdir(directory):
            ## ignore swp files
            if f.startswith("..") or f.startswith(".") or f.endswith(".swp"):
                continue

            if "stat" in f or "clean" in f or ".sh" in f:
                continue

            joined_filename = join(directory, f)
            if isfile(joined_filename):
                profile_files_to_send.append(joined_filename)

        profile_files_to_send.sort(key=lambda x: os.path.getmtime(x))
        if SME_PROFILER_DBG_LVL <= LVL_DBG:
            print "list of ascii profile files to be sent upfront:"
            print profile_files_to_send

        for profile_file_path in profile_files_to_send:
            profile_file = profile_file_path.split("/")[-1]
            if SME_PROFILER_DBG_LVL <= LVL_DBG:
                print "sending profile file: " + profile_file.split("/")[-1]
            
            curr_profile = parse_profile(profile_file_path)

            if profile_file.startswith("FE-C"):
                self.client_profiles[curr_profile.profile_id] = curr_profile
            if profile_file.startswith("FE-B"):
                self.fe_be_profiles[curr_profile.profile_id] = curr_profile
            if profile_file.startswith("BE"):
                self.be_fe_profiles[curr_profile.profile_id] = curr_profile
        
        self.send_profiles_to_kernel()

        if USE_IOCTL_PROFILE_DATABASE_64bit:
            dev_close(self.sme_ctrl_fd)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="SME Profiler")
    parser.add_argument("-s", "--dummy_ip", dest="dummy_serv_ip",
        action="store", required=True, help="IP of the dummy server")
    parser.add_argument("-S", "--dummy_port", dest="dummy_serv_port",
        action="store", required=True, help="Port of the dummy server")
    parser.add_argument("-a", "--ep_ip1", dest="ep_ip1",
        action="store", required=True, help="src ip w.r.t. host")
    parser.add_argument("-A", "--ep_port1", dest="ep_port1",
        action="store", required=True, help="src port w.r.t host")
    parser.add_argument("-b", "--ep_ip2", dest="ep_ip2",
        action="store", required=True, help="in ip w.r.t. host")
    parser.add_argument("-B", "--ep_port2", dest="ep_port2",
        action="store", required=True, help="in port w.r.t host")
    parser.add_argument("-c", "--ep_ip3", dest="ep_ip3",
        action="store", required=True, help="out ip w.r.t. host")
    parser.add_argument("-C", "--ep_port3", dest="ep_port3",
        action="store", required=True, help="out port w.r.t host")
    parser.add_argument("-d", "--ep_ip4_list", dest="ep_ip4_list",
        action="store", nargs="+", help="dst ip w.r.t. host")
    parser.add_argument("-D", "--ep_port4", dest="ep_port4",
        action="store", required=True, help="dst port w.r.t host")
    parser.add_argument("-e", "--slave_ip", dest="slave_ip",
        action="store", default=0, help="IP of the slave profiler")
    parser.add_argument("-E", "--slave_port", dest="slave_port",
        action="store", default=0, help="Port of the slave profiler")

    parser.add_argument("-p", "--update_prof_freq", dest="update_prof_freq",
        action="store", required=True,
        help="Flag to determine when to send profile DB to kernel\n"   \
            "0 - do not send, 1 - send after every profile generation (TODO)")

    parser.add_argument("-n", "--prof_for_n_req", dest="prof_for_n_req",
        action="store", required=False, default=-1,
        help="#requests up to which the profiler runs.")

    parser.add_argument("-g", "--gen_prof_freq", dest="generate_prof_freq",
        action="store", required=False, default=sys.maxint,
        help="Flag to determine how often to generate profile DB. \n"   \
            "e.g. -g 1000 generates profiles after 1000 observations. "    \
            "(TODO: Make them per public input / bin)")

    parser.add_argument("-r", "--reuse_older_logs", dest="reuse_older_logs",
        action="store", required=False, default=1,
        help="Flag to determine if old logs should be reused" \
            "in computing new profiles or not. \n")

    parser.add_argument("-l", "--keep_logs", dest="keep_logs",
        action="store", required=False, default=1,
        help="Flag to determine whether to store the raw kernel logs. " \
            "If true, logs will be stored in "   \
            "/local/sme/daemon_profiler/DATE-TIME/kernel_logs, "    \
            "DATE-TIME is dropped in non FINAL_EXPERIMENTS mode")
    
    parser.add_argument("-k", "--keep_analysed_logs", dest="keep_analysed_logs",
        action="store", required=False, default=1,
        help="Flag to determine whether to store the analyzed kernel logs "  \
            "for further statistics. If true, logs will be stored in "   \
            "/local/sme/daemon_profiler/DATE-TIME/analysed_logs, "  \
            "DATE-TIME is dropped in non FINAL_EXPERIMENTS mode")

    parser.add_argument("-u", "--use_logs_dir", dest="use_logs_dir",
        action="store", required=False, default="",
        help="Generate profile using logs in this directory")

    parser.add_argument("-ap", "--send_ascii_profiles_dir",
        dest="send_ascii_profiles_dir", action="store", required=False, default="",
        help="Read previously generated ascii profiles from this directory, "  \
            "send these profiles to the kernel. BE profiles "    \
            "should have a prefix BE in the file name")
    parser.add_argument("--pace_log", dest="pace_log", action="store",
        default=0, help="Log paced packets")

    parser.add_argument("--root_dir", dest="root_dir", action="store",
            default="/local/sme/daemon_profiler",
            help="Root directory to be used for this profiler run")

    parser.add_argument("--only_dump_no_profiling", dest="only_dump_no_profiling",
            action="store", default=1,
            help="Dump the binary logs into a file for offline profiling, " \
                "do not process them online")


    print "\n== SME profiler =="

    args = parser.parse_args()
    pace_log = int(args.pace_log)

    PROFILER_ROOT_DIR = args.root_dir

    ## init profiler class instance
    my_sme_prof = SMEProfiler(args.dummy_serv_ip, args.dummy_serv_port,
        args.ep_ip1, args.ep_port1, args.ep_ip2, args.ep_port2,
        args.ep_ip3, args.ep_port3, args.ep_ip4_list, args.ep_port4,
        args.slave_ip, args.slave_port,
        int(args.update_prof_freq), int(args.generate_prof_freq),
        int(args.prof_for_n_req), int(args.keep_logs),
        int(args.keep_analysed_logs), args.use_logs_dir,
        int(args.reuse_older_logs), int(args.only_dump_no_profiling),
        "/dev/SMECTRL")

    print "Profiler args: " + str(args)

    print "Starting experiment"

    if args.send_ascii_profiles_dir != "":
        my_sme_prof.send_ascii_profiles_dir(args.send_ascii_profiles_dir)
        sys.exit(0)

    ## create directories only if fresh profiling, or
    ## generating profiles from existing kernel logs.
    if FINAL_EXPERIMENTS:
        experiment_date = "/" +time.strftime("%d_%m_%Y-%H_%M_%S")
    else:
        experiment_date = ""
    profile_dir = PROFILER_ROOT_DIR + experiment_date
    my_sme_prof.set_dirs(profdir=profile_dir, keep_logs_dir="kernel_logs",
        keep_analysed_logs_dir="analysed_logs",
        genprof_dir="generated_profiles", msg_dir="buffers_sent")

    if args.use_logs_dir != "":
        print "Parsing from previously saved logs at: " + args.use_logs_dir
        my_sme_prof.run_on_directory()
        sys.exit(0)

    ## here onwards, we are doing fresh logging and profiling
    ## for both original, and paced traffic.
    signal.signal(signal.SIGTERM, sme_sig_handler)

    my_sme_prof.thread = Process(target=run_forever,
            args=(my_sme_prof, ), name="profiler")
    my_sme_prof.thread.start()

    if pace_log != 0:
        print "Starting pace logs profiling"
        my_sme_pace = SMEProfiler(args.dummy_serv_ip, args.dummy_serv_port,
            args.ep_ip1, args.ep_port1, args.ep_ip2, args.ep_port2,
            args.ep_ip3, args.ep_port3, args.ep_ip4_list, args.ep_port4,
            args.slave_ip, args.slave_port,
            0, int(args.generate_prof_freq), int(args.prof_for_n_req),
            int(args.keep_logs), int(args.keep_analysed_logs), args.use_logs_dir,
            int(args.reuse_older_logs), int(args.only_dump_no_profiling),
            "/dev/SMEPACE")

        my_sme_pace.set_dirs(profdir=profile_dir,
            keep_logs_dir="pace_kernel_logs",
            keep_analysed_logs_dir="pace_analysed_logs",
            genprof_dir="pace_generated_profiles", msg_dir="pace_buffers_sent",
            collect_logs_dir="pace_observations")

        my_sme_pace.thread = Process(target=run_forever,
                args=(my_sme_pace, ), name="pace_profiler")

        my_sme_pace.thread.start()
        my_sme_pace.thread.join()
    else:
        print "No pace logs"
    my_sme_prof.thread.join()
    print "After join"
