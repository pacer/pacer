#!/bin/bash

##########################################################
## author: aasthakm
##
## Supports configuring N VFs at a time.
## Requires macaddr-file to have at least N mac addresses.
##########################################################

if [[ $# -lt 2 ]]; then
  echo "Usage: ./vfsetup.sh <macaddr-file> <iface> <numvfs>"
  echo "E.g.: ./vfsetup.sh \"scripts/mac-ip-map.txt\" eno1 2"
  exit
fi

macaddr_file=$1
edev=$2
numvfs=$3

numargs_skip=0
declare -a macaddr_arr

index=0
while IFS= read -r line; do
#  echo "$line"
  mac=$(echo $line | cut -d',' -f1)
#  echo "$mac"
  macaddr_arr[$index]="$mac"
  index=$(( $index + 1 ))
done < $macaddr_file

#for i in `seq $numargs_skip $(( $numargs_skip + $numvfs - 1 ))`; do
#  index=$(( $i - $numargs_skip ))
#  echo "$i, $index, ${!i}"
#  macaddr_arr[$index]="${!i}"
#  echo ${macaddr_arr[$index]}
#done

echo "Configuring SRIOV on NIC.."
cmd="echo $numvfs > /sys/class/net/$edev/device/sriov_numvfs"
echo "$cmd"
eval "$cmd"
#echo 1 > /sys/class/net/$edev/device/sriov_numvfs

for i in `seq $numargs_skip $(( $numargs_skip + $numvfs - 1 ))`; do
  index=$(( $i - $numargs_skip ))
  cmd="ip link set $edev vf $index mac ${macaddr_arr[$index]}"
  echo "$cmd"
  eval "$cmd"
  #ip link set $edev vf 0 mac $macaddr
done

cmd="echo 0 > /sys/class/net/$edev/device/sriov_numvfs"
echo "$cmd"
eval "$cmd"
#echo 0 > /sys/class/net/$edev/device/sriov_numvfs

cmd="echo $numvfs > /sys/class/net/$edev/device/sriov_numvfs"
echo "$cmd"
eval "$cmd"
#echo 1 > /sys/class/net/$edev/device/sriov_numvfs

## sleep required because it takes time for the VF iface to get renamed
sleep 1;

taillen=$(( 2 * $numvfs ))
for i in `seq $numargs_skip $(( $numargs_skip + $numvfs - 1 ))`; do
  #ip link show
  index=$(( $i - $numargs_skip ))
  tailidx=$(( $taillen - $(( 2 * $index )) ))
  vf=$(ip link show | tail -n $tailidx | head -n1 | cut -d':' -f2 | cut -d' ' -f2)
  ip link show $vf
  ip link set $vf up
done

#lspci | grep "Ethernet Virtual Function"
vfpci_arr=($(lspci | grep "Ethernet Virtual Function" | cut -d' ' -f1))
for i in `seq 0 $(( ${#vfpci_arr[@]} - 1 ))`; do
  echo "Adding VF ${vfpci_arr[$i]} to Xen"
  xl pci-assignable-add ${vfpci_arr[$i]}
done
