#!/bin/bash

## Cleanup script for vfsetup.sh
## Even though vfsetup.sh is used to configure one VF at a time,
## we can cleanup all of them together.

edev=$1

vfpcilist=$(xl pci-assignable-list)
for vfpci in $vfpcilist; do
  echo "Removing VF $vfpci from Xen";
  xl pci-assignable-remove -r $vfpci > /dev/null 2>&1
done

echo 0 > /sys/class/net/$edev/device/sriov_numvfs
