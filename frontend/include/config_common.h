/*
 * config_common.h
 *
 * created on: Sep 19, 2017
 * author: aasthakm
 *
 * configurations that are shared between
 * kernel and userspace
 */

#ifndef __CONFIG_COMMON_H__
#define __CONFIG_COMMON_H__

#define MAX_QBUF_SIZE 64

// 512 MB of ring buffer shared between kernel and userspace
#define MAX_NUM_QBUFS (8*1024*1024)

#endif /* __CONFIG_COMMON_H__ */
