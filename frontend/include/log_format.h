/*
 * created on: Aug 25, 2017
 * author: aasthakm
 *
 * log_format.h
 * binary format of a single log entry
 * common between kernel and userspace
 */

#ifndef __LOG_FORMAT_H__
#define __LOG_FORMAT_H__

#include "conn_common.h"
#include "profile_conn_format.h"

typedef struct log_record {
  /*
   * timestamp of sending/receiving the packet
   */
  uint64_t timestamp;
  /*
   * true length of the packet payload
   * (i.e. without size padding)
   */
  uint32_t payload_len;
  /*
   * seq and ack numbers (valid only for TCP packets)
   */
  uint32_t seqno;
  uint32_t seqack;
  /*
   * direction of the logged packet
   * 0 - source is s_addr, s_port
   * 1 - source is d_addr, d_port
   */
  uint32_t dir;
#if CONFIG_PROF_MINI_LOG
  /*
   *This log entry represents "count" of response packets so far
   */
  uint32_t count;
#endif
  /*
   * # of public input hashes following the log record header
   */
  uint32_t num_public_inputs;
  /*
   * # of private input hashes following the log record header
   */
  uint32_t num_private_inputs;
} log_record_t;

/*
 * =============================
 * the log record may optionally
 * have the following fields if
 * at least one of num_pub or
 * num_priv is non-zero.
 * ==========================
 */

/*
 * ihash_t req_id
 * uint64_t marker_timestamp
 * ihash_t pubhash[num_public_inputs]
 * ihash_t privhash[num_private_inputs]
 */

typedef struct log_conn_header {
  /*
   * pair of connection 5-tuple
   */
  conn_t conn_id;
  /*
   * # records following the header
   */
  uint32_t n_recs;
} log_conn_header_t;

typedef struct log_header {
  /*
   * total size of log from the start of this header
   * till the end of the buffers of all n_conn listed here
   */
  uint64_t total_log_size;
  /*
   * # pcm's whose logs follow this header
   */
  uint16_t n_conn;
  uint16_t pad[3];
  /*
   * =================================
   * this will be followed by an array
   * of u32 offsets of size n_conn
   * =================================
   */
} log_header_t;

#endif /* __LOG_FORMAT_H__ */
