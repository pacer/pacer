/*
 * generic_q_inst.h
 *
 * created on: May 28, 2019
 * author: aasthakm
 *
 * instances of generic queue
 */

#ifndef __GENERIC_Q_INST_H__
#define __GENERIC_Q_INST_H__

typedef struct largef_elem {
  uint64_t ts;
  uint32_t snd_nxt;
  uint32_t snd_una;
  uint32_t rcv_nxt;
  uint32_t cwnd;
  uint32_t icsk_rto;
  uint32_t snd_wnd;
  uint32_t rcv_wnd;
  uint32_t cwm;
  uint32_t undo_marker;
  uint32_t undo_retrans;
  uint32_t high_seq;
  uint32_t packets_out;
  uint16_t lost_out;
  uint16_t sacked_out;
  uint16_t retrans_out;
  uint16_t tail;
  uint16_t real;
  uint16_t dummy;
  uint16_t num_timers;
  uint16_t nxt;
  uint16_t pidx;
  uint16_t ca_state:4,
          caller:4,
          coreid:4,
          owner:4;
} largef_elem_t;

#endif /* __GENERIC_Q_INST_H__ */
