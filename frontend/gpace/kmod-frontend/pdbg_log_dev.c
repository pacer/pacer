#include <linux/mm.h>
#include <linux/mm_types.h>

#include <linux/fs.h>
#include <linux/miscdevice.h>

#include "sme_debug.h"
#include "sme_time.h"

#include "sme_skb.h"
#include "sme_q_single.h"

#include "profile_map.h"
#include "sme_helper.h"

#include "generic_q.h"

#include "../../include/msg_hdr.h"

#include <asm/ioctl.h>
#include <asm/xen/hypercall.h>

#ifdef CONFIG_XEN_PACER
extern generic_q_t *largef_q[MAX_STATQ_ARRAYS];
#endif

extern list_t scm_list[NUM_PACERS];

#define SME_IOCTL _IO('S', 0)


enum {
  SME_CTRL_DEV = 0,
};

/*
 * =============================
 * misc device for app I/O calls
 * =============================
 */

int
pdbg_log_open(struct inode *inode, struct file *file)
{
  iprint2(LVL_EXP, "dev dir %s parent %s"
      , file->f_path.dentry->d_name.name
      , file->f_path.dentry->d_parent->d_name.name
      );
  return 0;
}

int
pdbg_log_release(struct inode *inode, struct file *file)
{
  return 0;
}

ssize_t
pdbg_log_read(struct file *file, char __user *buf, size_t len, loff_t *pos)
{
  return 0;
}

int
pdbg_log_mmap_common(struct file *filp, struct vm_area_struct *vma, int devtype)
{
  int err = 0;
  int size = 0;
  int i;
  struct page *pg = NULL;
  char *ptr = NULL;
  unsigned long uaddr = 0;
  int qidx = 0;
  int num_pages = ((sizeof(largef_elem_t) * NUM_REQTS_ELEMS) - 1) / PAGE_SIZE + 1;
  int total_ring_size = num_pages * PAGE_SIZE;
  generic_q_t *log_q = NULL;

  // cannot share the page with anyone
  if (vma->vm_flags & (VM_MAYSHARE | VM_SHARED))
    return -EINVAL;

  // mapping cannot be kept across forks, cannot be expanded,
  // and is not a "normal" page
  vma->vm_flags |= VM_DONTCOPY | VM_DONTEXPAND | VM_READ | VM_WRITE;
  //vma->vm_flags |= VM_DONTCOPY | VM_DONTEXPAND | VM_READ;

  // do not want the first write access to trigger a "minor" page fault
  // to mark the page dirty. this is transient, private memory, we do not
  // care if it was touched or not. PAGE_SHARED means RW access, but not
  // execute, and avoids copy-on-write behaviour.
  vma->vm_page_prot = PAGE_SHARED;

  qidx = vma->vm_pgoff / (num_pages+1);
  log_q = largef_q[qidx];

  uaddr = vma->vm_start;
  size = vma->vm_end - vma->vm_start;

  iprint(LVL_DBG, "mmap start: 0x%0lx, end: 0x%0lx, off: 0x%0lx, size: %d\n"
      "#pages %d ring sz %d qidx %d q %p elem size %d size %d"
      , vma->vm_start, vma->vm_end, vma->vm_pgoff, size
      , num_pages, total_ring_size, qidx, log_q, log_q->elem_size, log_q->size);

  // map header struct
  if (vma->vm_pgoff % (num_pages+1) == 0) {
    ptr = (char *) log_q;
    pg = vmalloc_to_page((void *) ptr);
    err = vm_insert_page(vma, uaddr, pg);
    if (size <= PAGE_SIZE)
      return err;

    uaddr += PAGE_SIZE;
  }

  // map the actual ring buffer
  ptr = (char *) log_q->elem_arr;
  for (i = 0; i < num_pages; i++) {
    pg = vmalloc_to_page((void *) ptr);
    err = vm_insert_page(vma, uaddr, pg);
    iprint(LVL_DBG, "%d. uaddr: %lu, ptr: %p, pg: %p", i, uaddr, ptr, pg);
    uaddr += PAGE_SIZE;
    ptr += PAGE_SIZE;
  }

  return err;
}

int
pdbg_log_mmap(struct file *filp, struct vm_area_struct *vma)
{
  return pdbg_log_mmap_common(filp, vma, SME_CTRL_DEV);
}

long
pdbg_log_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
  return 0;
}

struct file_operations pdbg_log_ops = {
  .owner = THIS_MODULE,
  .open = pdbg_log_open,
  .release = pdbg_log_release,
  .read = pdbg_log_read,
  .mmap = pdbg_log_mmap,
  .unlocked_ioctl = pdbg_log_ioctl,
};

struct miscdevice pdbg_log_dev = {
  .name = "PDBGLOG",
  .minor = MISC_DYNAMIC_MINOR,
  .fops = &pdbg_log_ops,
  .mode = 0666,
};
