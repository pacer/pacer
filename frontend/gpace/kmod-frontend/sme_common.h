/*
 * sme_common.h
 *
 * created on: May 13, 2017
 * author: aasthakm
 *
 * common functions between sme_core, sme_skb, pacer
 */

#ifndef __SME_COMMON_H__
#define __SME_COMMON_H__

#include <linux/skbuff.h>

/*
 * ================
 * helper functions
 * ================
 */
int check_valid_protocol(unsigned int protocol);
int is_sender_ip_me(uint32_t ip);
int is_destination_ip_me(uint32_t ip);
int check_relevant_ports(int s_port, int d_port);
int is_destination_port_dummy_server(int d_port);

void init_mac_addr(unsigned char *mac, int mac_len);
void print_mac_addr(unsigned char *mac, unsigned char *buf, int buflen);
void get_mac_from_string(char *buf, int buflen, unsigned char *mac);
void print_pkt_payload(char *data, int data_len, char *extra_dbg_string);
void __mod_print_sk_buff_int(struct sk_buff *skb, char *extra_dbg_string);

int parse_skb_headers(struct sk_buff *skb,
    unsigned char *src_mac, unsigned char *dst_mac,
    uint32_t *src_ip, uint32_t *dst_ip, uint16_t *src_port, uint16_t *dst_port,
    uint8_t *protocol, uint32_t *tot_hdr_len_p, uint32_t *delta_len_p,
    uint32_t *user_data_len_p, char **user_data_p,
    void **eth_p, void **iph_p, void **tcph_p, void **udph_p);
int parse_raw_skb_headers(char *data, int data_len,
    unsigned char *src_mac, unsigned char *dst_mac,
    uint32_t *src_ip, uint32_t *dst_ip, uint16_t *src_port, uint16_t *dst_port,
    uint8_t *protocol, uint32_t *tot_hdr_len_p, uint32_t *user_data_len_p,
    char **user_data_p, uint32_t *mac_hdr_off, uint32_t *ip_hdr_off,
    uint32_t *trans_hdr_off, uint32_t *trans_hdr_len);

#endif /* __SME_COMMON_H__ */
