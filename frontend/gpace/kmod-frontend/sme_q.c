#include "sme_config.h"
#include "sme_debug.h"
#include "sme_q_common.h"
#include "sme_skb.h"
#include "sme_statistics.h"

#include <linux/vmalloc.h>

#if 0
#if SME_CONFIG_STAT
extern sme_stat * stats_sme_q_full_cnt[32];
#endif
#endif

/*
 * =====
 * sme_q
 * =====
 */

// caller holds appropriate locks
void
init_sme_q(sme_q_t *q, int qsize)
{
  int i;
  int max_sskb = qsize;
  sme_sk_buff_t *sskb = NULL;

  if (q->is_init)
      return;

  q->max_sskb = max_sskb;

  q->sskb_list = (sme_sk_buff_t *) kzalloc(sizeof(sme_sk_buff_t) * max_sskb,
      GFP_KERNEL);
  if (!q->sskb_list)
    return;

  for (i = 0; i < qsize; i++) {
    sskb = &q->sskb_list[i];
    INIT_LIST_HEAD(&sskb->fifo_listp);
  }

  q->prod_idx = 0;
  q->cons_idx = 0;
  q->is_init = 1;
  q->cpuid = smp_processor_id();
}

void
cleanup_sme_q(sme_q_t *q)
{
  int idx;
  int max_sskb;
  int qmask;

  if (!q->is_init)
    return;

  max_sskb = q->max_sskb;

  if (sme_q_empty(q))
    goto cleanup;

  qmask = max_sskb - 1;

  iprint(LVL_DBG, "cleanup Q on cpu %d", smp_processor_id());
  print_sme_q(q);
  // TODO: figure out what to do with pending skb's
  idx = q->cons_idx;
  while (((idx+1) & qmask) <= q->prod_idx) {
    cleanup_sme_skb(&q->sskb_list[idx]);
    q->cons_idx = idx;
    idx = (idx+1) & qmask;
  }

cleanup:
  q->max_sskb = 0;
  if (q->sskb_list) {
    kfree(q->sskb_list);
  }
  q->sskb_list = NULL;
  q->prod_idx = 0;
  q->cons_idx = 0;
  q->is_init = 0;
}

int
sme_q_last_cons(sme_q_t *q)
{
  int last_cons_idx = 0;
  int max_sskb;
  int qmask;

  if (!q->is_init)
    return -1;

  max_sskb = q->max_sskb;
  qmask = max_sskb - 1;
  last_cons_idx = (q->cons_idx + max_sskb - 1) & qmask;
  return last_cons_idx;
}

void
print_sme_q(sme_q_t *q)
{
  sme_sk_buff_t *sskb = NULL;
  int idx;
  int max_sskb;
  int qmask;

  if (sme_q_empty(q)) {
    iprint(LVL_DBG, "empty Q on cpu %d", smp_processor_id());
    return;
  }

  max_sskb = q->max_sskb;
  if (!q->sskb_list)
    return;

  qmask = max_sskb - 1;

  iprint(LVL_DBG, "Qsize: %d, prod: %d, cons: %d", sme_q_num_elem(q),
      q->prod_idx, q->cons_idx);
  idx = q->cons_idx;
  while (((idx+1) & qmask) <= q->prod_idx) {
    sskb = &q->sskb_list[idx];
    print_sme_skb(sskb, idx);
    idx = (idx+1) & qmask;
  }
}

int
sme_q_num_elem(sme_q_t *q)
{
  int max_sskb;

  max_sskb = q->max_sskb;

  if (q->cons_idx <= q->prod_idx)
    return (q->prod_idx - q->cons_idx);
  else
    return (q->prod_idx + max_sskb - q->cons_idx);
}

/*
 * =========
 * sme_log_q
 * =========
 */
sme_log_q_t *
alloc_init_sme_log_q(int num_qbufs)
{
  int num_pages = 0;
  int alloc_size = 0;
  sme_log_q_t *q = NULL;

  num_pages = ((sizeof(sme_log_q_t)-1)/PAGE_SIZE+1)
    + (((num_qbufs*MAX_QBUF_SIZE)-1)/PAGE_SIZE + 1);
  alloc_size = num_pages * PAGE_SIZE;

  q = (sme_log_q_t *) vzalloc(alloc_size);
  iprint(LVL_INFO, "alloc size: %d, q: %p", alloc_size, q);
  if (!q)
    return NULL;

  q->max_qbuf = num_qbufs;
  q->prod_idx = 0;
  return q;
}

void
cleanup_sme_log_q(sme_log_q_t **qp)
{
  sme_log_q_t *q = NULL;

  if (!qp || !(*qp))
    return;

  q = *qp;
  vfree(q);
}

void
enqueue_sme_log_q(sme_log_q_t *q, char *buf, int buf_len)
{
  qbuf_t *elem = NULL;
  int qmask = 0;
  uint64_t prod_idx;
  uint64_t head_idx;
  int idx_it = 0;
  int pending = 0, wlen = 0, woff = 0;
  qbuf_t *qbuf_list = NULL;

  uint64_t eat_len = 0;
  int eat_slots = 0;

  if (!q || !buf || !buf_len)
    return;

  if (buf_len > (q->max_qbuf * MAX_QBUF_SIZE)) {
    iprint(LVL_INFO, "Log buffer too large %d, max allowed %llu"
        , buf_len, (q->max_qbuf * MAX_QBUF_SIZE));
    return;
  }

  qbuf_list = (qbuf_t *) ((char *) q + PAGE_SIZE);
  qmask = q->max_qbuf - 1;

  while (sme_log_q_full_head(q, buf_len)) {
    elem = &qbuf_list[q->head_idx];
    eat_len = ((uint64_t *) elem)[0];
    eat_slots = (eat_len - 1)/MAX_QBUF_SIZE + 1;
    head_idx = q->head_idx;
    for (idx_it = 0; idx_it < eat_slots; idx_it++) {
      elem = &qbuf_list[head_idx];
      memset(elem, 0, MAX_QBUF_SIZE);
      head_idx = (head_idx+1) & qmask;
    }
    iprint(LVL_INFO, "LOG FULL gen %llu head (%llu->%llu) prod %llu"
        " buf len %d, eat_len %llu, eat_slots %d"
        , q->gen_id, q->head_idx, head_idx, q->prod_idx
        , buf_len, eat_len, eat_slots);
    q->head_idx = head_idx;
  }

  prod_idx = q->prod_idx;
  pending = buf_len;
  woff = 0;
  while (pending > 0) {
    elem = &qbuf_list[prod_idx];
    if (pending > MAX_QBUF_SIZE)
      wlen = MAX_QBUF_SIZE;
    else
      wlen = pending;
    memcpy(elem->buf, buf + woff, wlen);
    woff += wlen;
    pending -= wlen;

    prod_idx = (prod_idx+1) & qmask;
    // starting with gen_id 0, increment
    // only when it wraps around again
    if (prod_idx == 0)
      q->gen_id++;
  }

  q->prod_idx = prod_idx;
  iprint(LVL_DBG, "gen %llu, head %llu, prod %llu"
      , q->gen_id, q->head_idx, q->prod_idx);
  return;
}

int sme_log_q_full_head(sme_log_q_t *q, int buf_len)
{
  int i;
  uint64_t prod_idx = q->prod_idx;
  int qmask = q->max_qbuf - 1;
  int n_qbuf = (buf_len-1)/MAX_QBUF_SIZE + 1;

  for (i = 0; i < n_qbuf; i++) {
    if (((prod_idx+1) & qmask) == q->head_idx)
      return 1;

    prod_idx = (prod_idx+1) & qmask;
  }

  return 0;
}

/*
 * =====
 * pcm_q
 * =====
 */

void
init_pcm_q(pcm_q_t *pcm_q, int num_pcms)
{
  if (!pcm_q)
    return;

  pcm_q->pcm_ptr_list = kzalloc(sizeof(pcm_q_elem_t) * num_pcms, GFP_KERNEL);
  if (!pcm_q->pcm_ptr_list)
    return;

  pcm_q->max_pcms = num_pcms;
  pcm_q->prod_idx = 0;
  pcm_q->cons_idx = 0;
  spin_lock_init(&pcm_q->prod_lock);
}

void
cleanup_pcm_q(pcm_q_t *pcm_q)
{
  if (!pcm_q || !pcm_q->pcm_ptr_list)
    return;

  if (pcm_q->pcm_ptr_list)
    kfree(pcm_q->pcm_ptr_list);
  pcm_q->prod_idx = 0;
  pcm_q->cons_idx = 0;
  pcm_q->max_pcms = 0;
}

int
pcm_q_full(pcm_q_t *pcm_q)
{
  int max_pcm = pcm_q->max_pcms;
  int qmask = max_pcm - 1;
  return (((pcm_q->prod_idx+1) & qmask) == pcm_q->cons_idx);
}

int
pcm_q_empty(pcm_q_t *pcm_q)
{
  return (pcm_q->prod_idx == pcm_q->cons_idx);
}

int
enqueue_pcm_q(pcm_q_t *pcm_q, void *pcm)
{
  int max_pcm, qmask;
  pcm_q_elem_t *elem;

  if (!pcm_q || !pcm)
    return -EINVAL;

  spin_lock(&pcm_q->prod_lock);

  if (pcm_q_full(pcm_q)) {
    spin_unlock(&pcm_q->prod_lock);
    return -ENOMEM;
  }

  max_pcm = pcm_q->max_pcms;
  qmask = max_pcm - 1;

  elem = &pcm_q->pcm_ptr_list[pcm_q->prod_idx];
  elem->pcm_ptr = (uint64_t) pcm;
  pcm_q->prod_idx = (pcm_q->prod_idx+1) & qmask;

  spin_unlock(&pcm_q->prod_lock);

  return 0;
}

int
dequeue_pcm_q(pcm_q_t *pcm_q, void **pcm)
{
  int max_pcm, qmask;
  pcm_q_elem_t *elem;

  if (!pcm_q || !pcm)
    return -EINVAL;

  if (pcm_q_empty(pcm_q))
    return -ENOENT;

  max_pcm = pcm_q->max_pcms;
  qmask = max_pcm - 1;

  elem = &pcm_q->pcm_ptr_list[pcm_q->cons_idx];
  *pcm = (void *) elem->pcm_ptr;

  pcm_q->cons_idx = (pcm_q->cons_idx+1) & qmask;
  return 0;
}
