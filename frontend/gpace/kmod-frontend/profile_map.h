/*
 * profile_map.h
 *
 * created on: Feb 21, 2017
 * author: aasthakm
 * 
 * in kernel profile database structure
 */

#ifndef __PROFILE_MAP_H__
#define __PROFILE_MAP_H__

#include "profile.h"
#include "sme_hashtable.h"
#include "profile_log.h"
#include "sme_nic.h"

#include <linux/list.h>
#include <linux/skbuff.h>
#include <uapi/linux/netdevice.h> /* for MAX_ADDR_LEN */
#include <net/tcp_states.h>

enum {
  SME_FE_CLIENT = 0, /* app -> client outgoing conn (on the FE host) */
  SME_FE_BE, /* app -> backend db outgoing conn (on the FE host) */
  SME_BE_CLIENT, /* backend -> app outgoing conn (on the BE host) */
  MAX_SME_TYPES
};

enum {
  PROF_LOG = 0,
  ENF_LOG,
  MAX_LOG_TYPES
};

enum {
  PSTATE_FREELIST = 0,
  PSTATE_PROFQ,
  PSTATE_XEN_COMPL,
};

enum {
  PCM_STAGE_SYN = 0,
  PCM_STAGE_SSL,
  PCM_STAGE_SSL_END,
};

typedef struct {
  uint32_t num_pub;
  uint32_t num_priv;
  ihash_t *pubhash;
  ihash_t *privhash;
} prof_key_t;

#define PROF_NOENT  1
#define PROF_INVAL  2
#define PROF_NOCHG  3
#define PROF_EXPRD  4
#define PROF_COMPL  5
#define PROF_PAUSE  6
#define PROF_UINIT  7

#define DEFAULT_PROFILE_ID 100
extern ihash_t DEFAULT_PROFILE_IHASH;
extern ihash_t DEFAULT_HTTP_PROF_IHASH;
extern ihash_t DEFAULT_SSLFIN_PROF_IHASH;
extern ihash_t DEFAULT_HTTPS_PROF_IHASH;
extern ihash_t DEFAULT_MW_PROF_IHASH;
extern ihash_t DEFAULT_VIDEO_PROF_IHASH;
extern ihash_t pid1, pid2, pid3;
extern profile_t default_profile;

// forward declaration
struct sme_channel_map;

/*
 * ================================
 * pointer structure for scm or pcm
 * (used to link FE and BE pcm's)
 * ================================
 */
typedef struct scm_pcm_ptr {
  void *ptr;
  list_t listp;
} scm_pcm_ptr_t;

int cleanup_scm_pcm_ptr_list(list_t *scm_pcm_ptr_list);

typedef struct profile_conn_map {
  ihash_t default_pid;
  /*
   * client-server ports, ID for the specific socket conn
   */
  conn_t sec_conn_id;
  /*
   * flag to indicate whether ssl handshake profile has been
   * set or not. we want to install a single profile for all
   * incoming messages during ssl handshake.
   */
  uint8_t is_req_prof_set;
  /*
   * flag to indicate if log written to shared ring buffer
   */
  uint8_t written_proflog;
  /*
   * session marker from app indicating from when
   * to start logging incoming and outgoing packets
   */
  uint8_t rcvd_sess_marker;
  /*
   * states of TCP state machine
   */
  uint8_t state;
  /*
   * cpuid of the conn for which this pcm is used.
   * to identify RFS hash function.
   */
  int8_t cpuid;
  /*
   * timer for when pcm should be deleted after
   * it goes into TCP_TIME_WAIT state
   */
  uint64_t tw_timeout;
  /*
   * pointer to the dev of the skb
   */
  struct net_device *sme_dev;
  /*
   * read-only pointer to an entry in the profile_map array
   */
  profile_t *p;
  /*
   * read-only pointer to parent scm
   */
  struct sme_channel_map *s;
  /*
   * pointer to the list head in pcm hashtable
   */
  list_t *pcm_list_head;
  /*
   * pointer to link into list of profile_conn_map elements
   */
  list_t pcm_listp;
  /*
   * list to hold pointers to pcm(s) linked to this pcm
   */
  list_t pcm_ptr_list;
  /*
   * log queue for profiling
   */
  log_t prof_log;
  /*
   * elem holding pskb for last pkt of a req (for profile logging)
   */
  partial_sk_buff_t last_req_pkt;
  /*
   * this is the current version of the last_req_pkt, that keeps
   * getting updated continuously. on receiving the marker we copy
   * latest value of rec_req_pkt into last_req_pkt and use that for
   * logging. after this last_req_pkt is not updated until a new
   * marker arrives in the next request.
   */
  partial_sk_buff_t rec_req_pkt;
  
#if CONFIG_PROF_MINI_LOG
  //TODO: possibly combine with last_send_pkt
  /*
   * elem holding pskb for last pkt of a rsp (for profile logging)
   */
  partial_sk_buff_t last_rsp_pkt;
  /*
   * this is the current version of the last_rsp_pkt, that keeps
   * getting updated continuously. on FIN of a connection or
   * receiving a new request we copy the latest value of rec_rsp_pkt
   * into last_rsq_pkt and use that for logging.
   * After this last_rsq_pkt is not updated until a new
   * response is sent by the application in the next request.
   */
  partial_sk_buff_t rec_rsp_pkt;

  /* Number of packets out for the current request response */
  uint32_t count_pkts_out;
  /* Saving state of whether first response message was looged or not) */
  uint8_t logged_first_rsp_pkt;

#endif
  /*
   * to make sure that pcm cannot be freed if anyone else is using it.
   */
  atomic_t refcount;
  /*
   * holds last seen seq,ack# for pcm state change
   */
  partial_sk_buff_t last_send_pkt;
  /*
   * public and private input hashes sent by application
   */
  void *marker_data;
  int marker_data_len;
  /*
   * index in profq where default profile added corresponding to this conn.
   */
  int64_t profq_idx;
  /*
   * hypace ID to which flow is mapped
   */
  int32_t hypace_idx;
#if CONFIG_PROF_FREE == PROF_FREE_TCP
  /*
   * move profq_idx to prev_profq_idx on new request.
   */
  int64_t prev_profq_idx;
  list_t profq_idx_list;
#endif
  /*
   * copy the value of snd_cwnd from tcp. we need this to set the
   * cwnd_watermark in new profiles on the connection.
   */
  int32_t tcp_cwnd;
#if CONFIG_DUMMY == DUMMY_OOB
  uint32_t *shared_seq_p;
  uint32_t *shared_dummy_out_p;
  uint32_t *shared_dummy_lsndtime_p;
#endif
  uint32_t *packets_out_p;
  uint32_t *retrans_out_p;
  uint32_t *sacked_out_p;
  uint32_t *lost_out_p;
  struct sock *tcp_sock;
  /*
   * session-level last real seq numbers. to use across two requests.
   * TODO: does this need to be atomic as well?
   */
  uint32_t last_real_ack;
  /*
   * socket value of rcv_wnd to be set in tcp header of dummies
   */
  uint16_t tcp_rcv_wnd;
  uint16_t tcp_rcv_wscale;
  /*
   * copy the value of tcp_sk->segs_out. we need this to set the
   * cwnd_watermark in new profiles on the connection.
   */
  int32_t tcp_segs_out;
  /*
   * count number of occurrences of scenarios where N == CWM == P, and
   * we first extend profile by adding a rexmit slot (P => P+1), and then
   * set CWM => CWM+1 as part of update_cwnd.
   *
   * if this count is non-zero, do not append a rexmit slot in tx_incr_profile.
   */
  int num_loss_extn;
  /*
   * connection stage, to determine the type of default profile to install
   * on the next incoming packet. currently, we recognize only end-of-SSL.
   */
  int stage;
  /*
   * nicq header for per-flow queue
   */
  nicq_hdr_t pcm_nicq;
  /*
   * MAC addresses for msges intercepted right near the NIC
   * direction w.r.t. sending a msg from the host, i.e.
   * src_mac = mac of the host NIC, dst_mac = mac of the gateway
   */
  char src_mac[ETH_HEADER_LEN];
  char dst_mac[ETH_HEADER_LEN];
  /*
   * Remember TCP allowance and initial sequence number to
   * fix the CWM computation in TCP_CA_Recovery state
   */
  int rem_allowance;
  uint32_t rem_initial_seq;
} profile_conn_map_t;

typedef struct sme_channel_map {
  /*
   * client-server pair ID, identifier for the scm module
   */
  conn_t conn_id;
  /*
   * SCM type, 3 on frontend host, 1 on backend host
   */
  uint8_t sme_type;
  /*
   * to make the profile id namespace local to the conn id
   * this is the static profile database -- populated during
   * profiling, and read during enforcement
   */
  list_t pmap_list;
  sme_htable_t pmap_htbl;
  /*
   * hash table of profile_conn_map elements
   * since port numbers may change during runtime, map the
   * profiles to port numbers when information provided by
   * the application
   */
  sme_htable_t pcm_htbl;
  /*
   * list to hold dep scm(s) linked to this scm
   */
  list_t dep_scm_list;
  /*
   * pointer into list of scm elements in netback driver of dom0
   */
  list_t scm_listp;
  /*
   * pointer into dep_scm_list of another scm
   */
  list_t dep_scm_listp;
  /*
   * read-only pointer to client scm, valid only in crosstier scm
   */
  struct sme_channel_map *parent_scm;
} sme_channel_map_t;

void init_sme_channel_map_list(list_t *scm_list);

/*
 * ================
 * helper functions
 * ================
 */
typedef int (*scm_filter_fn) (sme_channel_map_t *scm);
typedef int (*pcm_filter_fn) (profile_conn_map_t *pcm);

/*
 * =========================
 * sme channel map functions
 * =========================
 */
// allocate scm, add to global scm_list
int init_sme_channel_map(sme_channel_map_t *scm, conn_t *conn_id);
// check global list for duplicate and set scm in the list
int set_sme_channel_in_global_list(list_t *scm_list, sme_channel_map_t *scm);
// lookup scm in scm_list
sme_channel_map_t *lookup_sme_channel_map(list_t *scm_list, conn_t *conn_id,
    int compare_type);
// iterate scm_list and get next valid scm based on filter function
sme_channel_map_t *get_next_scm(list_t *scm_list_head, list_t **start_scm,
    scm_filter_fn sfn);
void cleanup_sme_channel_map(sme_channel_map_t *scm);
// free scm datastructure only
int free_sme_channel_map(sme_channel_map_t **scm_p);
// remove all scm from scm_list and free
int free_all_sme_channel_map(list_t *scm_list);

/*
 * =====================
 * profile map functions
 * =====================
 */
int set_profile_in_pmap_list(list_t *pmap_list, profile_t *p);
int free_profile_in_pmap_list(list_t *pmap_list);

/*
 * ==========================
 * profile conn map functions 
 * ==========================
 */
// set sconn and profile pointer in pcm
int init_profile_conn_map(profile_conn_map_t *pcm, conn_t *sconn_id, profile_t *p,
    struct net_device *dev, unsigned char *src_mac, unsigned char *dst_mac, int8_t cpuid);
// add pcm to scm.pcm_list
//int set_profile_conn_map_on_channel(sme_channel_map_t *scm, profile_conn_map_t *pcm);

// add pcm to scm.pcm_list without checking for duplicates
int set_profile_conn_map_on_channel_nocheck(sme_channel_map_t *scm,
    profile_conn_map_t *pcm);
// lookup in a given scm by sconn
profile_conn_map_t *lookup_profile_conn_map_by_sec_conn(sme_channel_map_t *scm,
    conn_t *sconn_id, int update, int is_send_side, sme_sk_buff_t *sskb);
// iterate scm_list and get next valid scm based on filter function
profile_conn_map_t *get_next_pcm(list_t *pcm_list_head, list_t **start_pcm,
    pcm_filter_fn pfn);
profile_conn_map_t *get_one_pcm(sme_channel_map_t *scm, pcm_filter_fn pfn);
// free pcm
int free_profile_conn_map(profile_conn_map_t **pcm_p, int is_rmmod);
// remove all pcm's on any list, and free each of them
int free_all_pcm_on_list(list_t *pcm_list);

uint32_t pcm_hash(void *key, int klen, int modulo);

/*
 * =================
 * profile functions
 * =================
 */
// allocate and init internal structure of profile
int init_profile(profile_t *p, uint16_t id, ihash_t id_ihash,
    uint16_t num_out_reqs, uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *spacing, uint64_t *latency);
#if 0
int init_profile_hashes(profile_t *p, uint32_t num_pub, uint32_t num_priv,
    ihash_t *pubhash, ihash_t *privhash, uint16_t num_out_reqs,
    uint16_t *num_extra_slots, uint64_t *num_out_frames,
    uint64_t *spacing, uint64_t *latency);
#endif
// default profile for every conn unless explicitly specified
void init_default_profile(profile_t *prof);
void init_one_profile(profile_t *prof, ihash_t pid);
void init_ihashes(void);
void init_profiles(sme_htable_t *pmap_htbl);
// lookup profile in pmap list
profile_t *lookup_profile_list_by_inp_hash(list_t *pmap_list, uint32_t num_pub,
    uint32_t num_priv, ihash_t *pubhash, ihash_t *privhash);
// free internal structure of the profile
void reset_profile(profile_t *p);
// accessor functions
int get_req_frame_idx_from_timer_idx(profile_t *p, uint16_t timer_idx,
    int *req_idx, int *frame_idx);
int16_t get_num_out_reqs(profile_t *p);
int64_t get_num_out_frames_for_req(profile_t *p, int req_idx);
int16_t get_num_extra_slots_for_req(profile_t *p, int req_idx);
int64_t get_latency_for_req(profile_t *p, int req_idx);
int64_t get_spacing_for_req(profile_t *p, int req_idx);
int64_t get_total_frames(profile_t *p);
int64_t get_last_latency(profile_t *p);
int64_t get_last_resp_timestamp(profile_t *p, uint64_t ref_ts);
uint64_t get_profile_timer_at_idx(profile_t *p, uint64_t ref_ts, uint16_t timer_idx);

/*
 * ================
 * parse ctrl msges
 * ================
 */
// parse all profile conn ctrl messages from application
int parse_profile_conn_buf(list_t *scm_list, char *buf, int buf_len);
// parse only the connection info from the profile ctrl messages
int parse_conn_from_profile_buf(char *buf, int buf_len, conn_t *conn_id);
// parse profile buf, assign to profile
int parse_profile_from_buf(profile_t *p, char *buf, int buf_len);
int parse_profile_from_buf_ihash(profile_t *p, char *buf, int buf_len);
// parse IP pair in the buf, init scm, set pmap in the scm
int parse_profile_list_from_buf_ihash(list_t *scm_list, char *buf, int buf_len);
int parse_profile_marker_buf(list_t *scm_list, char *buf, int buf_len);

/*
 * ===============
 * print functions
 * ===============
 */
void print_sme_channel_map(sme_channel_map_t *scm);
void print_profile_conn_map(profile_conn_map_t *pcm);
void print_profile(profile_t *p);

void print_profile_conn_buf(char *buf, int buf_len);

#endif /* __PROFILE_MAP_H__ */
