#include "sme_spinlock.h"

#include <asm/errno.h>
//#include <asm-generic/atomic.h>
#include <linux/bottom_half.h>

int
init_tkt_lock(tkt_lock_t *lock)
{
  if (!lock)
    return -EINVAL;

  atomic_set(&lock->next_tkt, 0);
  atomic_set(&lock->now_serving, 0);

  return 0;
}

void
acquire_tkt_lock(tkt_lock_t *lock)
{
  int my_tkt = 0;

  if (!lock)
    return;

  my_tkt = atomic_fetch_inc(&lock->next_tkt);
  while (my_tkt != atomic_read(&lock->now_serving));

  if (my_tkt == atomic_read(&lock->now_serving))
    return;
}

void
release_tkt_lock(tkt_lock_t *lock)
{
  if (!lock)
    return;

  atomic_fetch_inc(&lock->now_serving);
}

void
acquire_tkt_lock_bh(tkt_lock_t *lock)
{
  int my_tkt = 0;

  if (!lock)
    return;

  __local_bh_disable_ip(_RET_IP_, SOFTIRQ_LOCK_OFFSET);
  my_tkt = atomic_fetch_inc(&lock->next_tkt);
  while (my_tkt != atomic_read(&lock->now_serving));

  if (my_tkt == atomic_read(&lock->now_serving))
    return;
}

void
release_tkt_lock_bh(tkt_lock_t *lock)
{
  if (!lock)
    return;

  atomic_fetch_inc(&lock->now_serving);
  __local_bh_enable_ip(_RET_IP_, SOFTIRQ_LOCK_OFFSET);
}

