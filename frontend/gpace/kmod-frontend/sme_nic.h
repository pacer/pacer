/*
 * sme_nic.h
 *
 * created on: Nov 18, 2018
 * author: aasthakm
 *
 * datastructures related to sharing nic buffers
 * between guest and hypace
 */

#ifndef __SME_NIC_H__
#define __SME_NIC_H__

#include "mem_desc.h"
#include <linux/skbuff.h>

typedef struct nic_txdata {
  uint64_t tx_tsc; // key for sorted insertion in lockfree priority queue
  uint64_t skb_maddr;
  atomic_t next;
  __le32 pbd2_parsing_data;
  __le32 txbd_addr_lo;
  __le32 txbd_addr_hi;
  __le16 txbd_nbytes; // skb_headlen(skb)
  __le16 txbd_vlan_or_ethertype;
  u8 txbd_flags;
  u8 txbd_general_data;
  u8 txbuf_flags;
  u8 s_mac[ETH_ALEN];
  u8 d_mac[ETH_ALEN];
} nic_txdata_t;

typedef struct global_nicq_hdr {
  int qsize;
#define MAX_NICQ_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_NICQ_FRAGS];
  nic_txdata_t *queue;
  nic_txdata_t **queue_p;
} global_nicq_hdr_t;

typedef struct nicq_hdr {
  int qsize;
  atomic_t head;
  atomic_t tail;
} nicq_hdr_t;

typedef struct nic_fl_elem {
  int32_t offset;
} nic_fl_elem_t;

typedef struct nic_freelist {
  int fl_size;
  uint32_t prod_idx;
  uint32_t cons_idx;
#define MAX_NICFL_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_NICFL_FRAGS];
  nic_fl_elem_t *nfl_arr;
  spinlock_t lock;
} nic_freelist_t;

extern nic_freelist_t *global_nic_freelist[MAX_HP_ARGS];

int init_global_nicq_hdr(global_nicq_hdr_t *global_queue, int qsize);
void cleanup_global_nicq_hdr(global_nicq_hdr_t *global_queue);

int nicq_search(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_nicq, uint64_t key,
    void *pcm);
int nicq_insert(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_nicq, int ins_idx,
    void *pcm);
int nicq_remove(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_nicq, uint64_t key,
    void *pcm);

int init_nicq_hdr(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_queue,
    nic_freelist_t *nfl);
int init_nicq_hdr_missing(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_queue,
    nic_freelist_t *nfl);
int cleanup_nicq_hdr(nicq_hdr_t *nicq_hdr, global_nicq_hdr_t *global_queue,
    nic_freelist_t *nfl, void *pcm);

int init_nic_freelist(nic_freelist_t *nfl, int total_size, int pcm_slab_size);
void cleanup_nic_freelist(nic_freelist_t *nfl);
int get_one_nic_freelist(nic_freelist_t *nfl, nic_fl_elem_t *nelem);
int put_one_nic_freelist(nic_freelist_t *nfl, nic_fl_elem_t *nelem);

/*
 * ==========
 * nic txintq
 * ==========
 */

typedef struct nic_txint_elem {
  /*
   * physical addr of skb to free up completion of NIC transfer
   */
  uint64_t skb_maddr;
  uint8_t is_real;
} nic_txint_elem_t;

typedef struct nic_txintq {
  int qsize;
#define MAX_TXINTQ_FRAGS 4
  uint16_t n_md;
  mem_desc_t md_arr[MAX_TXINTQ_FRAGS];
  nic_txint_elem_t *queue;
  nic_txint_elem_t **queue_p;
} nic_txintq_t;

int init_nic_txintq(nic_txintq_t *ntq, int qsize);
void cleanup_nic_txintq(nic_txintq_t *ntq);

#endif /*  __SME_NIC_H__ */
