/*
 * sme_stats_decl.h
 *
 * created on: Apr 14, 2019
 * author: aasthakm
 */

#ifndef __SME_STATS_DECL_H__
#define __SME_STATS_DECL_H__

SME_DECL_STAT_ARR_EXTERN(init_pcm_cnt, NUM_PACERS);
SME_DECL_STAT_ARR_EXTERN(gc_pcm_cnt, NUM_PACERS);

SME_DECL_STAT_ARR_EXTERN(rx_ooo_cnt, NUM_PACERS);
SME_DECL_STAT_ARR_EXTERN(dma_lat, RX_NCPUS);

SME_DECL_STAT_ARR_EXTERN(prev_pkt_cnt, RX_NCPUS);
SME_DECL_STAT_ARR_EXTERN(prev_unused_cnt, RX_NCPUS);

SME_DECL_STAT_ARR_EXTERN(defprof_install, RX_NCPUS);
SME_DECL_STAT_ARR_EXTERN(ack_cwnd_update, RX_NCPUS);
SME_DECL_STAT_ARR_EXTERN(loss_cwnd_update, RX_NCPUS);
SME_DECL_STAT_ARR_EXTERN(rtx_prof_extend, RX_NCPUS);

SME_DECL_STAT_ARR_EXTERN(phash_dist, RX_NCPUS);

SME_DECL_STAT_ARR_EXTERN(pcm_sl, RX_NCPUS);
SME_DECL_STAT_ARR_EXTERN(nicq_ins, RX_NCPUS);

SME_DECL_STAT_ARR_EXTERN(dummy_per_irq, RX_NCPUS);
SME_DECL_STAT_ARR_EXTERN(real_per_irq, RX_NCPUS);

#endif /* __SME_STATS_DECL_H__ */
