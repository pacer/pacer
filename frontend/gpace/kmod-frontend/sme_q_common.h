/*
 * sme_q_common.h
 *
 * created on: May 22, 2017
 * author: aasthakm
 *
 * queue for sskb
 */

#ifndef __SME_Q_COMMON_H__
#define __SME_Q_COMMON_H__

#include "sme_config.h"
#include "sme_skb.h"
#include "sme_debug.h"

enum {
  IDX_PROD = 0,
  IDX_CONS,
};

/*
 * ============================================
 * sme_q: queues for TS, ctrl and data pkts
 * shared between IRQ handlers and pacer thread
 * ============================================
 */
enum {
  SME_Q_IDX_NOT_READY = 0,
  SME_Q_IDX_READY,
};

enum {
  PCPU_QT_IRQ = 0,
  PCPU_QT_PACER,
  PCPU_QT_PROFILE,
};

// TODO put prod,cons idx and n_sskb into union
typedef struct sme_q {
  uint32_t prod_idx;
  uint32_t cons_idx;
  int max_sskb;
  uint8_t is_init;
  uint8_t cpuid;
  sme_sk_buff_t *sskb_list;
} sme_q_t;

static inline int
sme_q_empty(sme_q_t *q)
{
  return (q->prod_idx == q->cons_idx);
}

static inline int
sme_q_full(sme_q_t *q)
{
  int max_sskb = q->max_sskb;
  int qmask = max_sskb - 1;
  return (((q->prod_idx+1) & qmask) == q->cons_idx);
}

// caller must hold appropriate locks
static inline int
enqueue_sme_q(sme_q_t *q, sme_sk_buff_t *sskb)
{
  sme_sk_buff_t *qelem = NULL;
  int qmask = 0;

  if (!sskb || !q || !q->sskb_list)
    return -EINVAL;

  if (sme_q_full(q))
    return -ENOMEM;

  qmask = q->max_sskb - 1;

  qelem = &q->sskb_list[q->prod_idx];
  clone_sme_skb(qelem, sskb, 1);

  q->prod_idx = (q->prod_idx + 1) & qmask;
  iprint(LVL_DBG, "cpu: %d, prod: %d cons %d (ro) type %u skb %p"
    , q->cpuid, q->prod_idx, q->cons_idx, sskb->type, sskb->skb);
  return 0;
}

// caller must hold appropriate locks
static inline int
dequeue_sme_q(sme_q_t *q, sme_sk_buff_t *sskb)
{
  sme_sk_buff_t *qelem = NULL;
  int qmask = 0;

  if (!sskb || !q || !q->sskb_list)
    return -EINVAL;

  if (sme_q_empty(q))
    return -ENOENT;

  qmask = q->max_sskb - 1;

  qelem = &q->sskb_list[q->cons_idx];
  clone_sme_skb(sskb, qelem, 1);

  q->cons_idx = (q->cons_idx + 1) & qmask;
  iprint(LVL_DBG, "cpu: %u, cons: %d sskb type %u %u"
      , q->cpuid, q->cons_idx, sskb->type, qelem->type);
  return 0;
}

void init_sme_q(sme_q_t *q, int qsize);
void cleanup_sme_q(sme_q_t *q);
void print_sme_q(sme_q_t *q);
int sme_q_num_elem(sme_q_t *q);

/*
 * =========================================
 * sme_log_q: queue for profile log
 * shared between sme and userspace profiler
 * =========================================
 */
typedef struct qbuf {
  char buf[MAX_QBUF_SIZE];
} qbuf_t;

typedef struct sme_log_q {
  /*
   * size of ring buffer in terms of # slots
   */
  uint64_t max_qbuf;
  /*
   * generation # indicating number of times
   * producer has wrapped around the queue
   */
  uint64_t gen_id;
  /*
   * idx modified by kernel
   */
  uint64_t prod_idx;
  /*
   * idx pointing to head of ring buffer
   * from where kernel can overwrite entries.
   * userspace must ensure that cons_idx is
   * ahead of head, else it must read from
   * head onwards.
   */
  uint64_t head_idx;
} sme_log_q_t;

sme_log_q_t *alloc_init_sme_log_q(int num_qbufs);
void cleanup_sme_log_q(sme_log_q_t **q);
void enqueue_sme_log_q(sme_log_q_t *q, char *buf, int buf_len);
int sme_log_q_full_head(sme_log_q_t *q, int buf_len);

/*
 * =================================================
 * pcm_q: queue for sharing pcm's with logger thread
 * =================================================
 */
typedef struct pcm_q_elem {
  uint64_t pcm_ptr;
} pcm_q_elem_t;

typedef struct pcm_q {
  spinlock_t prod_lock;
  uint32_t prod_idx;
  uint32_t cons_idx;
  uint32_t max_pcms;
  pcm_q_elem_t *pcm_ptr_list;
} pcm_q_t;

void init_pcm_q(pcm_q_t *pcm_q, int num_pcms);
void cleanup_pcm_q(pcm_q_t *pcm_q);
int enqueue_pcm_q(pcm_q_t *pcm_q, void *pcm);
int dequeue_pcm_q(pcm_q_t *pcm_q, void **pcm);

#endif /* __SME_Q_COMMON_H__ */
