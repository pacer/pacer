/*
 * sme_debug.h
 *
 * created on: Feb 21, 2017
 * author: aasthakm
 * 
 * debug macros
 */

#ifndef __SME_DEBUG_H__
#define __SME_DEBUG_H__

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/kern_levels.h>
#include <linux/sched.h>
#include <linux/smp.h>

#include "sme_config.h"

#define PFX "SME"

#define LVL_DBG 0
#define LVL_INFO 1
#define LVL_ERR 2
#define LVL_EXP 3

#define itrace  \
  do {  \
    if (SME_DEBUG_LVL <= LVL_EXP) { \
      printk(KERN_INFO PFX " (%d:%.16s,core:%d) %s:%d\n"  \
          , task_pid_nr(current), current->comm, smp_processor_id() \
          , __func__, __LINE__);  \
    } \
  } while (0)

#define iprintk(LVL, F) \
  do {  \
    if (SME_DEBUG_LVL <= LVL) { \
      printk(KERN_INFO PFX " (%d:%.16s,core:%d) %s:%d " F "\n"  \
          , task_pid_nr(current), current->comm, smp_processor_id() \
          , __func__, __LINE__);  \
    } \
  } while (0)

#define iprintk2(LVL, F)  \
  do {  \
    if (SME_DEBUG_LVL <= LVL) { \
      printk(KERN_INFO PFX " (%d:%.16s,core:%d) " F "\n"  \
          , task_pid_nr(current), current->comm, smp_processor_id()); \
    } \
  } while (0)

#define iprint(LVL, F, A...)  \
  do {  \
    if (SME_DEBUG_LVL <= LVL) { \
      printk(KERN_INFO PFX " (%d:%.16s,core:%d) %s:%d " F "\n"  \
          , task_pid_nr(current), current->comm, smp_processor_id() \
          , __func__, __LINE__, A); \
    } \
  } while (0)

#define iprint2(LVL, F, A...) \
  do {  \
    if (SME_DEBUG_LVL <= LVL) { \
      printk(KERN_INFO PFX " (%d:%.16s,core:%d) " F "\n"  \
          , task_pid_nr(current), current->comm, smp_processor_id(), A);  \
    } \
  } while (0)

#define iprint3(LVL, F, A...) \
  do {  \
    if (SME_DEBUG_LVL <= LVL) { \
      printk(KERN_INFO F "\n", A);  \
    } \
  } while (0)

static inline void
dbg_prt(char *buf, int buf_len)
{
  int it = 0;
  int s_it = 0;
  char sbuf[MAX_DBG_BUF_LEN];
  memset(sbuf, 0, MAX_DBG_BUF_LEN);
  for (it = 0; it < buf_len; it++) {
    sprintf(sbuf+s_it, "%02X", *(unsigned char *) (buf+it));
    s_it += 2;
  }
  //sprintf(sbuf+s_it, "'\0'");
  iprint(LVL_DBG, "%d: %s\n", buf_len, sbuf);
}


static inline void
prt_hash(char *hbuf, int hlen, char *out)
{ 
  int i;

  if (!hbuf || !hlen || !out)
    return;

  for (i = 0; i < hlen; i++) {
    sprintf(out + (i*2), "%02x", *(unsigned char *) (hbuf + i));
  }
}

#endif /* __SME_DEBUG_H__ */
