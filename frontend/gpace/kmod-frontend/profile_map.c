/*
 * profile_map.c
 *
 * created on: Feb 21, 2017
 * author: aasthakm
 * 
 * functions to manage in kernel profile database
 */

#include "sme_debug.h"
#include "sme_skb.h"
#include "sme_time.h"
#include "sme_helper.h"
#include "profile_map.h"
#include "sme_hashtable.h"
#include "logger.h"
#include "sme_statistics.h"
#include "sme_time.h"
#include "sme_nic.h"
#include "sme_stats_decl.h"
#include <linux/slab.h>
#include <linux/etherdevice.h>

#include <net/tcp.h> // for TCP_INIT_CWND
#include <linux/jhash.h>

extern uint32_t my_ip_int;
extern uint16_t my_port_int;
extern uint32_t bknd_ip_int[MAX_BKND_IP];
extern uint16_t bknd_port_int;
#if CONFIG_XEN_PACER
extern global_nicq_hdr_t *global_nicq[MAX_HP_ARGS];
extern nic_freelist_t *global_nic_freelist[MAX_HP_ARGS];
#endif

int
get_profile_idx(int profile_id)
{
  return profile_id;
}

int
get_profile_idx_from_hash(uint32_t num_pub, uint32_t num_priv,
    ihash_t *pubhash, ihash_t *privhash)
{
  int idx = 0;
  // all cannot be 0 at the same time
  if (!num_pub && !num_priv && !pubhash && !privhash)
    return -EINVAL;

  if (num_pub > 0 && pubhash) {
    idx = ((int *) pubhash[0].byte)[0];
  }

  return 0;
}

int
check_profile_match_by_hash(profile_t *p, uint32_t num_pub, uint32_t num_priv,
    ihash_t *pubhash, ihash_t *privhash)
{
  if (!p || (num_pub && !pubhash) || (num_priv && !privhash))
    return -1;

  if (memcmp((void *) &pubhash[0], (void *) &p->id_ihash, sizeof(ihash_t)) == 0)
    return 1;

  return 0;
}

/*
 * =============================
 * global scm list init function
 * =============================
 */
void
init_sme_channel_map_list(list_t *scm_list)
{
  INIT_LIST_HEAD(scm_list);
}

/*
 * =========================
 * sme channel map functions
 * =========================
 */
int
init_sme_channel_map(sme_channel_map_t *scm, conn_t *conn_id)
{
  int ret = 0;
  if (!conn_id || !scm)
    return -EINVAL;

  memcpy((char *) &scm->conn_id, (char *) conn_id, sizeof(conn_t));
  INIT_LIST_HEAD(&scm->pmap_list);
  INIT_LIST_HEAD(&scm->dep_scm_list);
  INIT_LIST_HEAD(&scm->scm_listp);
  INIT_LIST_HEAD(&scm->dep_scm_listp);
  ret = init_htable(&scm->pmap_htbl, PMAP_HTBL_SIZE, &pmap_hash_fn, 0);
  ret = init_htable(&scm->pcm_htbl, PCM_HTBL_SIZE, &pcm_hash_fn, 1);
  return 0;
}

int
set_sme_channel_in_global_list(list_t *scm_list, sme_channel_map_t *scm)
{
  sme_channel_map_t *scm2 = NULL;
  if (!scm)
    return -EINVAL;

  scm2 = lookup_sme_channel_map(scm_list, &scm->conn_id, COMPARE_CLIENT_SCM);
  if (scm2)
    return -EEXIST;

  list_add_tail(&scm->scm_listp, scm_list);

  return 0;
}

sme_channel_map_t *
lookup_sme_channel_map(list_t *scm_list, conn_t *conn_id, int compare_type)
{
  int ret = 0;
  sme_channel_map_t *scm = NULL;

  if (list_empty(scm_list)) {
    iprintk(LVL_INFO, "Empty scm list");
    return NULL;
  }

  list_for_each_entry(scm, scm_list, scm_listp) {
    ret = compare_conn_generic(&scm->conn_id, conn_id, compare_type);
    if (ret == 1) {
      return scm;
    }
  }

  return NULL;
}

sme_channel_map_t *
get_next_scm(list_t *scm_list_head, list_t **to_check_scm, scm_filter_fn sfn)
{
  sme_channel_map_t *scm = NULL;
  list_t *scm_iter = NULL;

  if (!to_check_scm || !(*to_check_scm))
    return NULL;

  scm_iter = *to_check_scm;
  while (scm_iter != scm_list_head) {
    scm = list_entry(scm_iter, sme_channel_map_t, dep_scm_listp);
    iprint(LVL_DBG, "scm [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
        , (void *) &scm->conn_id.src_ip, scm->conn_id.src_port
        , (void *) &scm->conn_id.in_ip, scm->conn_id.in_port
        , (void *) &scm->conn_id.out_ip, scm->conn_id.out_port
        , (void *) &scm->conn_id.dst_ip, scm->conn_id.dst_port);
    if (!sfn || sfn(scm) == 0) {
      *to_check_scm = scm->dep_scm_listp.next;
      return scm;
    }

    scm_iter = scm_iter->next;
  }

  scm_iter = scm_iter->next;
  while (scm_iter != *to_check_scm) {
    scm = list_entry(scm_iter, sme_channel_map_t, dep_scm_listp);
    iprint(LVL_DBG, "scm [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
        , (void *) &scm->conn_id.src_ip, scm->conn_id.src_port
        , (void *) &scm->conn_id.in_ip, scm->conn_id.in_port
        , (void *) &scm->conn_id.out_ip, scm->conn_id.out_port
        , (void *) &scm->conn_id.dst_ip, scm->conn_id.dst_port);
    if (!sfn || sfn(scm) == 0) {
      *to_check_scm = scm->dep_scm_listp.next;
      return scm;
    }

    scm_iter = scm_iter->next;
  }

  return NULL;
}

void
cleanup_sme_channel_map(sme_channel_map_t *scm)
{
  list_t *scm_it_head = NULL;
  list_t *scm_it_head2 = NULL;
  sme_channel_map_t *dep_scm = NULL;

  if (!scm)
    return;

  free_profile_in_pmap_list(&scm->pmap_list);
  cleanup_htable(&scm->pmap_htbl);
  cleanup_htable(&scm->pcm_htbl);
  list_for_each_safe(scm_it_head, scm_it_head2, &scm->dep_scm_list) {
    dep_scm = list_entry(scm_it_head, sme_channel_map_t, dep_scm_listp);
    list_del_init(&dep_scm->dep_scm_listp);
    free_sme_channel_map(&dep_scm);
  }
}

int
free_sme_channel_map(sme_channel_map_t **scm_p)
{
  sme_channel_map_t *scm = NULL;
  if (!scm_p || !(*scm_p))
    return -EINVAL;

  scm = *scm_p;

  cleanup_sme_channel_map(scm);
  kfree(scm);

  *scm_p = NULL;
  return 0;
}

int
free_all_sme_channel_map(list_t *scm_list)
{
  int cnt = 0;
  sme_channel_map_t *scm = NULL;
  list_t *scm_it_head = NULL;
  list_t *scm_it_head2 = NULL;

  list_for_each_safe(scm_it_head, scm_it_head2, scm_list) {
    scm = list_entry(scm_it_head, sme_channel_map_t, scm_listp);
    list_del_init(&scm->scm_listp);
    print_sme_channel_map(scm);
    free_sme_channel_map(&scm);
    cnt++;
  }
  iprint(LVL_INFO, "deleted %d SCMs", cnt);
  return 0;
}

/*
 * =====================
 * profile map functions
 * =====================
 */
int
set_profile_in_pmap_list(list_t *pmap_list, profile_t *p)
{
  if (!pmap_list || !p)
    return -EINVAL;

  list_add_tail(&p->profile_listp, pmap_list);
  return 0;
}

int
free_profile_in_pmap_list(list_t *pmap_list)
{
  list_t *pmap_it_head = NULL;
  list_t *pmap_it_head2 = NULL;

  profile_t *p = NULL;
  if (!pmap_list)
    return -EINVAL;

  list_for_each_safe(pmap_it_head, pmap_it_head2, pmap_list) {
    p = list_entry(pmap_it_head, profile_t, profile_listp);
    list_del_init(&p->profile_listp);
    if (p) {
      reset_profile(p);
      kfree(p);
    }
    p = NULL;
  }

  return 0;
}

/*
 * =====================
 * scm_pcm_ptr functions
 * =====================
 */

int
cleanup_scm_pcm_ptr_list(list_t *scm_pcm_ptr_list)
{
  scm_pcm_ptr_t *scm_pcm_ptr = NULL;
  list_t *list_head = NULL;
  list_t *list_head2 = NULL;

  if (!scm_pcm_ptr_list)
    return -EINVAL;

  list_for_each_safe(list_head, list_head2, scm_pcm_ptr_list) {
    scm_pcm_ptr = list_entry(list_head, scm_pcm_ptr_t, listp);
    list_del_init(&scm_pcm_ptr->listp);
    kfree(scm_pcm_ptr);
    scm_pcm_ptr = NULL;
  }

  return 0;
}

/*
 * ==========================
 * profile conn map functions 
 * ==========================
 */

int
init_profile_conn_map(profile_conn_map_t *pcm, conn_t *sconn_id, profile_t *p,
  struct net_device *dev, unsigned char *src_mac, unsigned char *dst_mac, int8_t cpuid)
{
  ipport_t key;
  uint32_t phash = 0;
  if (!sconn_id || !pcm)
    return -EINVAL;

  memcpy(&pcm->sec_conn_id, sconn_id, sizeof(conn_t));
  pcm->p = p;
  if (!pcm->p)
    pcm->p = &default_profile;

  pcm->stage = -1;
  pcm->sme_dev = dev;
  pcm->cpuid = cpuid;
  pcm->tcp_cwnd = TCP_INIT_CWND;
#if CONFIG_DUMMY == DUMMY_OOB
  pcm->shared_seq_p = NULL;
  pcm->shared_dummy_out_p = NULL;
  pcm->shared_dummy_lsndtime_p = NULL;
#endif

  key.src_ip = pcm->sec_conn_id.out_ip;
  key.src_port = pcm->sec_conn_id.out_port;
  key.dst_ip = pcm->sec_conn_id.dst_ip;
  key.dst_port = pcm->sec_conn_id.dst_port;
  phash = pcm_hash((void *) &key, sizeof(ipport_t), 0);
  pcm->hypace_idx = phash % N_HYPACE;
  iprint(LVL_DBG, "PORT %u hash %0x HP %d"
      , pcm->sec_conn_id.dst_port, phash, pcm->hypace_idx
      );

#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB && CONFIG_NIC_QUEUE == CONFIG_NQ_DYNAMIC
  init_nicq_hdr(&pcm->pcm_nicq, global_nicq[pcm->hypace_idx],
      global_nic_freelist[pcm->hypace_idx]);
#endif

  pcm->tcp_segs_out = 0;
  pcm->num_loss_extn = 0;
#if CONFIG_PROF_MINI_LOG
  pcm->count_pkts_out = 0;
#endif
  INIT_LIST_HEAD(&pcm->pcm_listp);
  INIT_LIST_HEAD(&pcm->pcm_ptr_list);

  eth_zero_addr((u8 *) pcm->src_mac);
  eth_zero_addr((u8 *) pcm->dst_mac);
  if (src_mac)
    ether_addr_copy((u8 *) pcm->src_mac, (u8 *) src_mac);
  if (dst_mac)
    ether_addr_copy((u8 *) pcm->dst_mac, (u8 *) dst_mac);

  set_pskb_cpuid(&pcm->last_req_pkt, -1);
  set_pskb_cpuid(&pcm->rec_req_pkt, -1);
#if CONFIG_PROF_MINI_LOG
  set_pskb_cpuid(&pcm->last_rsp_pkt, -1);
  set_pskb_cpuid(&pcm->rec_rsp_pkt, -1);
#endif
  atomic_set(&pcm->refcount, 1);

  set_pskb_cpuid(&pcm->last_send_pkt, -1);

  //skb_queue_head_init(&pcm->dma_q);

  init_log(&pcm->prof_log, &log_binary_fn);
  iprint(LVL_DBG, "Init profile log on [%pI4 %u, %pI4 %u]"
      , (void *) &sconn_id->out_ip, sconn_id->out_port
      , (void *) &sconn_id->dst_ip, sconn_id->dst_port);

  return 0;
}

#if 0
int
set_profile_conn_map_on_channel(sme_channel_map_t *scm, profile_conn_map_t *pcm)
{
  ipport_t key;
  int keylen;
  uint32_t idx;
  int compare_type;
  profile_conn_map_t *pcm2 = NULL;

  if (!pcm || !scm) {
    iprint(LVL_DBG, "SET PCM ON SCM scm %p pcm %p", scm, pcm);
    return -EINVAL;
  }

  if (scm->sme_type == SME_FE_CLIENT || scm->sme_type == SME_BE_CLIENT)
    compare_type = COMPARE_CLIENT_SCM;
  else
    compare_type = COMPARE_CROSSTIER_SCM;

  pcm2 =
    lookup_profile_conn_map_by_sec_conn(scm, &pcm->sec_conn_id);
  if (!pcm2) {
    // add conn key fields that may be known in scm but not in pcm
    inherit_key_fields(&scm->conn_id, &pcm->sec_conn_id);
    key.src_ip = pcm->sec_conn_id.out_ip;
    key.src_port = pcm->sec_conn_id.out_port;
    key.dst_ip = pcm->sec_conn_id.dst_ip;
    key.dst_port = pcm->sec_conn_id.dst_port;
    keylen = sizeof(ipport_t);
    //idx = pcm_hash((void *) &key, keylen, scm->pcm_htbl.size);
    idx = htable_key_idx(&scm->pcm_htbl, (void *) &key, keylen);
    htable_insert(&scm->pcm_htbl, (void *) &key, keylen, &pcm->pcm_listp);
    pcm->pcm_list_head = &scm->pcm_htbl.pcm_list[idx];
    pcm->s = scm;
  } else if (pcm2->p == pcm->p)
    return -EEXIST;
  else
    return -EINVAL;

  return 0;
}
#endif

int
set_profile_conn_map_on_channel_nocheck(sme_channel_map_t *scm,
    profile_conn_map_t *pcm)
{
  ipport_t key;
  int keylen;
  int idx;

  if (!pcm || !scm) {
    iprint(LVL_DBG, "SET PCM ON SCM scm %p pcm %p", scm, pcm);
    return -EINVAL;
  }

  // add conn key fields that may be known in scm but not in pcm
//  inherit_key_fields(&scm->conn_id, &pcm->sec_conn_id);
  key.src_ip = pcm->sec_conn_id.out_ip;
  key.src_port = pcm->sec_conn_id.out_port;
  key.dst_ip = pcm->sec_conn_id.dst_ip;
  key.dst_port = pcm->sec_conn_id.dst_port;
  keylen = sizeof(ipport_t);
  htable_insert(&scm->pcm_htbl, (void *) &key, keylen, &pcm->pcm_listp);
  //idx = pcm_hash((void *) &key, keylen, scm->pcm_htbl.size);
  idx = htable_key_idx(&scm->pcm_htbl, (void *) &key, keylen);
  pcm->pcm_list_head = &scm->pcm_htbl.pcm_list[idx];
  pcm->s = scm;

  return 0;
}

profile_conn_map_t *
lookup_profile_conn_map_by_sec_conn(sme_channel_map_t *scm, conn_t *sconn_id,
    int update, int send, sme_sk_buff_t *sskb)
{
  ipport_t key;
  int keylen = 0;
  int idx = 0;
  uint64_t now = 0;
  profile_conn_map_t *pcm = NULL;
  SME_INIT_TIMER(pcm_sl);

  if (!scm || !sconn_id)
    return NULL;

  key.src_ip = sconn_id->src_ip;
  key.src_port = sconn_id->src_port;
  key.dst_ip = sconn_id->dst_ip;
  key.dst_port = sconn_id->dst_port;
  keylen = sizeof(ipport_t);

  idx = htable_key_idx(&scm->pcm_htbl, (void *) &key, keylen);

  SME_START_TIMER(pcm_sl);

  spin_lock_bh(&(scm->pcm_htbl.lock[idx]));

  SME_END_TIMER_ARR(pcm_sl, smp_processor_id());

  now = get_current_time(SCALE_NS);

  pcm = htable_lookup(&scm->pcm_htbl, (void *) &key, keylen);

  if (!pcm || pcm->written_proflog == 1) {
    spin_unlock_bh(&(scm->pcm_htbl.lock[idx]));
    return NULL;
  }

  if (!update) {
    spin_unlock_bh(&(scm->pcm_htbl.lock[idx]));
    return pcm;
  }

  iprint(LVL_DBG, "pcm %p proflog %d state %d idx %d refcount %d"
      , pcm, pcm->written_proflog, pcm->state, idx, atomic_read(&pcm->refcount));

  atomic_inc(&pcm->refcount);

  if (send)
    update_pcm_state_send(pcm, sskb);
  else
    update_pcm_state_recv(pcm, sskb);

  if (pcm->state == TCP_CLOSE
      || (pcm->state == TCP_TIME_WAIT && pcm->tw_timeout > 0
          && ((int64_t) (pcm->tw_timeout - now) <= PCM_EXPIRE_THRESHOLD))) {
    pcm->written_proflog = 1;
  }

  iprint(LVL_DBG, "UPDATED pcm %p proflog %d state %d idx %d refcount %d"
      , pcm, pcm->written_proflog, pcm->state, idx, atomic_read(&pcm->refcount));

  spin_unlock_bh(&(scm->pcm_htbl.lock[idx]));

  return pcm;
}

profile_conn_map_t *
get_next_pcm(list_t *pcm_list_head, list_t **to_check_pcm, pcm_filter_fn pfn)
{
  profile_conn_map_t *pcm = NULL;
  list_t *pcm_iter = NULL;
  int cnt = 0;

  if (!to_check_pcm || !(*to_check_pcm))
    return NULL;

  pcm_iter = *to_check_pcm;
  while (pcm_iter != pcm_list_head) {
    pcm = list_entry(pcm_iter, profile_conn_map_t, pcm_listp);
    if (!pfn || pfn(pcm) == 0) {
      iprint(LVL_DBG, "list head: %p, to_check: %p, iter: %p, pcm: %p\n"
          "curr: [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u], JUMP: %d"
          , pcm_list_head, *to_check_pcm, pcm_iter, pcm
          , (void *) &pcm->sec_conn_id.src_ip, pcm->sec_conn_id.src_port
          , (void *) &pcm->sec_conn_id.in_ip, pcm->sec_conn_id.in_port
          , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
          , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port, cnt);
      *to_check_pcm = pcm->pcm_listp.next;
      return pcm;
    }

    pcm_iter = pcm_iter->next;
    cnt++;
  }

  pcm_iter = pcm_iter->next;
  while (pcm_iter != *to_check_pcm) {
    pcm = list_entry(pcm_iter, profile_conn_map_t, pcm_listp);
    if (!pfn || pfn(pcm) == 0) {
      iprint(LVL_INFO, "curr: [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u], JUMP: %d"
          , (void *) &pcm->sec_conn_id.src_ip, pcm->sec_conn_id.src_port
          , (void *) &pcm->sec_conn_id.in_ip, pcm->sec_conn_id.in_port
          , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
          , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port, cnt);
      *to_check_pcm = pcm->pcm_listp.next;
      return pcm;
    }

    pcm_iter = pcm_iter->next;
    cnt++;
  }

  return NULL;
}

profile_conn_map_t *
get_one_pcm(sme_channel_map_t *scm, pcm_filter_fn pfn)
{
  profile_conn_map_t *pcm = NULL;
  list_t *pcm_list = NULL;
  list_t *to_check_pcm = NULL;
  int i;
  for (i = 0; i < scm->pcm_htbl.size; i++) {
    pcm_list = &scm->pcm_htbl.pcm_list[i];
    to_check_pcm = pcm_list->next;
    pcm = get_next_pcm(pcm_list, &to_check_pcm, pfn);
    if (pcm) {
      iprint(LVL_DBG, "KEY %d, list head: %p, to_check: %p, iter: %p, pcm: %p\n"
          "curr: [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
          , i, pcm_list, pcm_list->next, to_check_pcm, pcm
          , (void *) &pcm->sec_conn_id.src_ip, pcm->sec_conn_id.src_port
          , (void *) &pcm->sec_conn_id.in_ip, pcm->sec_conn_id.in_port
          , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
          , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port);
      return pcm;
    }
  }

  return NULL;
}

int
free_profile_conn_map(profile_conn_map_t **pcm_p, int is_rmmod)
{
  profile_conn_map_t *pcm = NULL;
  int dma_cnt = 0;
  int ret = 0;

  if (!pcm_p || !(*pcm_p))
    return -EINVAL;

  pcm = *pcm_p;

  ret = is_pcm_ready_for_logging(pcm);
  if (ret == 0) {
    SME_ADD_COUNT_POINT_ARR(gc_pcm_cnt, 1, 0);
  } else if (ret != -EEXIST) {
    iprint(LVL_ERR, "NOT READY pcm\n[%pI4 %u, %pI4 %u] state %d logged %d"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->state, pcm->written_proflog);

  }

  // enable just this print to capture RFS mapping of flows to cpus
  iprint(LVL_DBG, "[%pI4 %u, %pI4 %u] => %u"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->cpuid);

  if (!list_empty(&pcm->pcm_ptr_list))
    cleanup_scm_pcm_ptr_list(&pcm->pcm_ptr_list);

  iprint(LVL_DBG, "Del %d dma io, cleanup plog [%pI4 %u, %pI4 %u]"
      , dma_cnt
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port);
  //write_log(&pcm->prof_log, &pcm->sec_conn_id);
  cleanup_log(&pcm->prof_log);

  iprint(LVL_DBG, "FREE MARKER DATA %p LEN %d", pcm->marker_data, pcm->marker_data_len);
  if (pcm->marker_data && pcm->marker_data_len) {
    kfree(pcm->marker_data);
    pcm->marker_data_len = 0;
  }

#if CONFIG_PROF_MINI_LOG
  pcm->count_pkts_out = 0;
  pcm->logged_first_rsp_pkt = 0;
#endif

  if (!is_rmmod) {
    // wait for hypace to finish freeing last of profile slots
    msleep(200);
  }

#if CONFIG_XEN_PACER && CONFIG_XEN_PACER_DB && CONFIG_NIC_QUEUE == CONFIG_NQ_DYNAMIC
  ret = cleanup_nicq_hdr(&pcm->pcm_nicq, global_nicq[pcm->hypace_idx],
      global_nic_freelist[pcm->hypace_idx], pcm);
#endif

  iprint(LVL_INFO, "[%pI4 %u, %pI4 %u] state %d delta %lld logged %d"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->state
      , (pcm->state == TCP_TIME_WAIT ?
          (int64_t) (get_current_time(SCALE_NS) - pcm->tw_timeout) : 0)
      , pcm->written_proflog
      );
  kfree(pcm);

  return 0;
}

int
free_all_pcm_on_list(list_t *pcm_list)
{
  profile_conn_map_t *pcm = NULL;
  list_t *pcm_it_head = NULL;
  list_t *pcm_it_head2 = NULL;
  if (!pcm_list)
    return -EINVAL;

  list_for_each_safe(pcm_it_head, pcm_it_head2, pcm_list) {
    pcm = list_entry(pcm_it_head, profile_conn_map_t, pcm_listp);
    iprint(LVL_DBG, "Should be freeing pcm %p [%pI4 %u, %pI4 %u] => %u"
      , pcm, (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->cpuid);
    list_del_init(&pcm->pcm_listp);
    free_profile_conn_map(&pcm, 1);
  }

  INIT_LIST_HEAD(pcm_list);

  return 0;
}

uint32_t
pcm_hash(void *key, int klen, int seed)
{
  u32 jh = jhash(key, klen, seed);
  return jh;
}

int
pcm_lookup(list_t *pcm_list, void *key, int klen, void **entry)
{
  profile_conn_map_t *pcm = NULL;
  conn_t *p_conn = NULL;
  ipport_t *ipp = (ipport_t *) key;
  list_for_each_entry(pcm, pcm_list, pcm_listp) {
    p_conn = &pcm->sec_conn_id;
    // hard coded comparison function.
    if (p_conn->out_ip == ipp->src_ip && p_conn->out_port == ipp->src_port
        && p_conn->dst_ip == ipp->dst_ip && p_conn->dst_port == ipp->dst_port) {
      *entry = pcm;
      BUG_ON(!pcm);
      return 0;
    }
  }

  *entry = NULL;
  return -EINVAL;
}

hash_fn_t pcm_hash_fn = {
  .hash = pcm_hash,
  .lookup = pcm_lookup,
  .free = free_all_pcm_on_list
};

/*
 * =================
 * profile functions
 * =================
 */
profile_t *
lookup_profile_list_by_inp_hash(list_t *pmap_list, uint32_t num_pub,
    uint32_t num_priv, ihash_t *pubhash, ihash_t *privhash)
{
//  int idx;
  profile_t *p = NULL;

  if (!pmap_list) {
    iprintk(LVL_ERR, "Error invalid profile map");
    return NULL;
  }

//  idx = get_profile_idx_from_hash(num_pub, num_priv, pubhash, privhash);
//  // TODO
//  idx = 0;
  list_for_each_entry(p, pmap_list, profile_listp) {
    if (check_profile_match_by_hash(p, num_pub, num_priv, pubhash, privhash))
      return p;
  }

  return NULL;
}

uint32_t
pmap_hash(void *key, int klen, int seed)
{
  u32 h = 0;
  ihash_t *hkey = (ihash_t *) key;
  memcpy((void *) &h, hkey->byte, sizeof(u32));
  return h;
}

int
pmap_lookup(list_t *pmap_list, void *key, int klen, void **entry)
{
  profile_t *p = NULL;
  ihash_t *ikey = (ihash_t *) key;
  list_for_each_entry(p, pmap_list, profile_listp) {
    if (check_profile_match_by_hash(p, 1, 0, ikey, NULL)) {
      *entry = p;
      return 0;
    }
  }

  *entry = NULL;
  return -EINVAL;
}

hash_fn_t pmap_hash_fn = {
  .hash = pmap_hash,
  .lookup = pmap_lookup,
  .free = free_profile_in_pmap_list,
};

/*
 * ===============
 * print functions
 * ===============
 */

void
print_profile_conn_map(profile_conn_map_t *pcm)
{
  if (!pcm)
    return;

  iprint(LVL_DBG, "PCM(%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u), PROF: %p",
      (void *) &pcm->sec_conn_id.src_ip, pcm->sec_conn_id.src_port,
      (void *) &pcm->sec_conn_id.in_ip, pcm->sec_conn_id.in_port,
      (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port,
      (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port,
      pcm->p);
}

void
print_profile_list(list_t *pmap_list)
{
  int cnt = 0;
  profile_t *p = NULL;
  list_for_each_entry(p, pmap_list, profile_listp) {
    cnt++;
  }
  iprint(LVL_DBG, "PLIST[%d]", cnt);
  list_for_each_entry(p, pmap_list, profile_listp) {
    print_profile(p);
  }
}

void
print_sme_channel_map(sme_channel_map_t *scm)
{
  int i;
  profile_conn_map_t *pcm = NULL;
  if (!scm)
    return;

  iprint(LVL_DBG, "SCM(%pI4, %pI4, %pI4, %pI4)",
      (void *) &scm->conn_id.src_ip, (void *) &scm->conn_id.in_ip,
      (void *) &scm->conn_id.out_ip, (void *) &scm->conn_id.dst_ip);
  //print_profile_map(&scm->pmap);
  print_profile_list(&scm->pmap_list);

  for (i = 0; i < scm->pcm_htbl.size; i++) {
    list_for_each_entry(pcm, &scm->pcm_htbl.pcm_list[i], pcm_listp) {
      print_profile_conn_map(pcm);
    }
  }
}

