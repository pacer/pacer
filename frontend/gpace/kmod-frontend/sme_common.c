/*
 * sme_common.c
 *
 * created on: May 13, 2017
 * author: aasthakm
 *
 * common functions between sme_core, sme_skb, pacer
 */

#include "sme_config.h"
#include "sme_debug.h"

#include "sme_common.h"
#include "profile_map.h"

#include <net/ip.h>
#include <net/tcp.h>
#include <net/inet_connection_sock.h>

#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/phy.h>
#include <linux/ktime.h>

#include <linux/jhash.h>

#include "../linux-frontend-4.9.5/drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"

#define DBG_BUF_SIZE 1024
#define DBG_DATA_LEN 80

extern uint32_t my_ip_int;
extern uint16_t my_port_int;
extern uint32_t bknd_ip_int[MAX_BKND_IP];
extern uint16_t bknd_port_int;
extern uint16_t dummy_client_port;
extern uint16_t dummy_server_port;

int
check_valid_protocol(unsigned int protocol)
{
  return ((protocol == IPPROTO_TCP) || (protocol == IPPROTO_UDP));
}

int
is_sender_ip_me(uint32_t ip)
{
  return (ip == my_ip_int);
}

int
is_destination_ip_me(uint32_t ip)
{
  return (ip == my_ip_int);
}

int
check_relevant_ports(int s_port, int d_port)
{
#if 0
  if (s_port == 80 || d_port == 80)
    return 1;

  if (s_port == 443 || d_port == 443)
    return 1;
#endif

  // exclude ssh/scp traffic from enforcement
  if (s_port == 22 || d_port == 22)
    return 0;

  if (s_port == my_port_int || d_port == my_port_int)
    return 1;

  if (s_port == bknd_port_int || d_port == bknd_port_int)
    return 1;

  if (s_port == dummy_server_port || d_port == dummy_server_port)
    return 1;

  if (s_port == dummy_client_port || d_port == dummy_client_port)
    return 1;

  return 0;
}

int
is_destination_port_dummy_server(int d_port)
{
  if (d_port == dummy_server_port)
    return 1;

  return 0;
}

void
init_mac_addr(unsigned char *mac, int mac_len)
{
  int i;
  if (!mac || !mac_len)
    return;

  for (i = 0; i < mac_len; i++) {
    mac[0] = '0';
  }

  //memset(mac, '0', mac_len);
}

void
print_mac_addr(unsigned char *mac, unsigned char *buf, int buflen)
{
#if SME_DEBUG_LVL <= LVL_INFO
  int i;
  char c;
  int off = 0;

  memset(buf, 0, buflen);

  for (i = 0; i < ETH_HEADER_LEN; i++) {
    c = mac[i];
    if (i == 0)
      sprintf(buf + off, "%02x", (uint8_t) c);
    else
      sprintf(buf + off, ":%02x", (uint8_t) c);
    off = strlen(buf);
  }
#endif
}

void
get_mac_from_string(char *buf, int buflen, unsigned char *mac)
{
  int i;
  unsigned int c;
  int off = 0;
  char dbg_mac[ETH_PRINT_HDR_LEN];

  for (i = 0; i < ETH_HEADER_LEN; i++) {
    if (off >= buflen)
      break;

    if (i == 0) {
      sscanf(buf + off, "%02x", &c);
      mac[i] = (unsigned char) c;
    } else {
      off += 1;
      sscanf(buf + off, "%02x", &c);
      mac[i] = (unsigned char) c;
    }
    off += 2;
  }

  memset(dbg_mac, 0, ETH_PRINT_HDR_LEN);
  print_mac_addr(mac, dbg_mac, ETH_PRINT_HDR_LEN);
  iprint(LVL_INFO, "orig mac %s dbg mac %s", buf, dbg_mac);
}

void
__print_skb_dev(struct sk_buff *skb, char *skb_dev_buf)
{
  struct Qdisc *q;
  struct netdev_queue *txq;
  int queue_index = -1;
  struct net_device *next_dev = NULL;
  struct net_device *first_dev = NULL;
  int ptype_list_empty = 0;
  char mac_addr_buf[ETH_PRINT_HDR_LEN];
  struct phy_device *phydev = NULL;

  if (!skb)
    return;

  if (skb->dev) {
    print_mac_addr(skb->dev->perm_addr, mac_addr_buf, skb->dev->addr_len+1);
    queue_index = __skb_tx_hash(skb->dev, skb, skb->dev->real_num_tx_queues);
    txq = &((skb->dev)->_tx[queue_index]);
    q = rcu_dereference_bh(txq->qdisc);

    next_dev = next_net_device(skb->dev);
    first_dev = first_net_device(dev_net(skb->dev));
    ptype_list_empty = list_empty(&skb->dev->ptype_all);
    phydev = skb->dev->phydev;

    sprintf(skb_dev_buf, "dev: %s, mtu: %d, addr(%d): %s"
//        ", addr assign type: %d"
//        ", phydev: %p, drv: %p, rxtstamp: %p, txtstamp: %p"
//        ", rx handler: %p, rx data: %p, ptype list empty: %d"
//        ", next dev: %s, first dev: %s\n"
//        ", netdev ops: %p"
        ", #TX queues: %d, queue index: %d, %d, txq: %p, qdisc: %s"
        , skb->dev->name, skb->dev->mtu, skb->dev->addr_len, mac_addr_buf
//        , skb->dev->addr_assign_type
//        , phydev, (phydev ? phydev->drv : NULL)
//        , (phydev ? phydev->drv->rxtstamp : NULL)
//        , (phydev ? phydev->drv->txtstamp : NULL)
//        , skb->dev->rx_handler, skb->dev->rx_handler_data, ptype_list_empty
//        , (next_dev ? next_dev->name: "empty")
//        , (first_dev ? first_dev->name : "empty")
//        , skb->dev->netdev_ops
        , skb->dev->real_num_tx_queues, skb->queue_mapping, queue_index, txq
        , (q->ops ? q->ops->id : "nops")
        );
  } else
    sprintf(skb_dev_buf, "SKB DEV NULL");
}

void
__print_bp_dev_info(struct sk_buff *skb, char *skb_bp_dev_buf)
{
  struct net_device *dev = NULL;
  struct bnx2x *bp = NULL;
  struct skb_shared_hwtstamps *shwts = NULL;
  struct skb_shared_info *shinfo = NULL;
  if (!skb || !skb_bp_dev_buf)
    return;

  dev = skb->dev;
  if (!dev)
    return;

  bp = netdev_priv(dev);
  if (!bp)
    return;

  shwts = skb_hwtstamps(skb);
  shinfo = skb_shinfo(skb);
  if (!shwts || !shinfo)
    return;

  sprintf(skb_bp_dev_buf,
      "chip ID: %d chip_is_e3: %d, chip_is_e3b0: %d, chip_is_e3a0: %d"
      ", bp->flags: %x, tx_type: %d, rx_filter: %d\n"
      ", igu_dsb_id: %d, igu_base_sb: %d, igu_sb_cnt: %d, igu_base_addr: %x"
      ", hwts_ioc_called: %d, PFID: %d, ptp clock: %p\n"
      ", SKB HWTS: %llu, SKB TS: %llu, SKB SH TXFLAGS: %x"
      , CHIP_ID(bp), CHIP_IS_E3(bp), CHIP_IS_E3B0(bp), CHIP_IS_E3A0(bp)
      , bp->flags, bp->tx_type, bp->rx_filter
      , bp->igu_dsb_id, bp->igu_base_sb, bp->igu_sb_cnt, bp->igu_base_addr
      , bp->hwtstamp_ioctl_called ? 1 : 0, bp->pfid, bp->ptp_clock
      , shwts->hwtstamp.tv64, skb->tstamp.tv64, shinfo->tx_flags
      );
}

void
__mod_print_udp_skb(struct sk_buff *skb, char *extra_dbg_string)
{
#if SME_DEBUG_LVL <= LVL_DBG
  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct udphdr *udph = NULL;

  unsigned char smac[ETH_HEADER_LEN], dmac[ETH_HEADER_LEN];
  unsigned char print_s_mac[ETH_PRINT_HDR_LEN], print_d_mac[ETH_PRINT_HDR_LEN];
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  uint8_t protocol = 0;
  uint16_t transport_hdr_len = 0;

  int tot_hdr_len = 0;
  char *user_data = NULL;
  uint32_t user_data_len = 0;

  char *user_data2 = NULL;

  uint8_t first_byte = 0;
  uint8_t second_byte = 0;
  uint8_t third_byte = 0;
  uint8_t fourth_byte = 0;
  uint8_t fifth_byte = 0;
  uint8_t sixth_byte = 0;
  uint8_t seventh_byte = 0;
  uint8_t eighth_byte = 0;
  char paged_data[512], paged_data2[512];

  char skb_dev_buf[DBG_BUF_SIZE];
  int ret;

  char dbg_string[64];
  memset(dbg_string, 0, 64);
  sprintf(dbg_string, "%s:%d", __func__, __LINE__);
  if (!extra_dbg_string)
    extra_dbg_string = dbg_string;

  memset(paged_data, 0, 512);
  memset(paged_data2, 0, 512);

  if (!skb)
    return;

  memset(skb_dev_buf, 0, DBG_BUF_SIZE);
  __print_skb_dev(skb, skb_dev_buf);

  transport_hdr_len = sizeof(struct udphdr);

  ret = parse_skb_headers(skb, smac, dmac, &s_addr, &d_addr, &s_port, &d_port,
      &protocol, &tot_hdr_len, NULL, &user_data_len, &user_data,
      (void *) &eth, (void *) &iph, NULL, (void *) &udph);

  if (ret == -EINVAL) {
    iprintk(LVL_EXP, "UDP RETURNED INVAL!!!!!");
    return;
  }

  print_mac_addr(smac, print_s_mac, ETH_PRINT_HDR_LEN);
  print_mac_addr(dmac, print_d_mac, ETH_PRINT_HDR_LEN);

  iprint2(LVL_INFO, "%s -- PROTO: %d, SKB PROT: %d\n"
      ", src: %pI4 %u, dst: %pI4 %u, len: %d, DELTA: %d, ip ID: %d\n"
      ", q %d txq %d, skb hash 0x%x l4h %u swh %u"
      , extra_dbg_string, iph->protocol, skb->protocol
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port, skb->len
      , user_data_len, ntohs(iph->id)
      , skb->queue_mapping, (skb->sk ? skb->sk->sk_tx_queue_mapping : -2)
      , skb->hash, skb->l4_hash, skb->sw_hash
      );

  return;

  if (ret == -EIO) {
    iprint2(LVL_DBG, "%s:%d %s -- PROTO: %d, SKB PROT %d\n"
        "src: %pI4, %d, dst: %pI4, %d SMAC: %s, DMAC: %s, PKT TYPE: %d\n"
        ", len: %d, dlen: %d, hlen: %d, udp hlen: %d, udp->len: %d"
        ", nh: %d, hn: %d data hlen: %d"
        ", ip hlen: %d, mac hlen: %d\n"
        ", mh: %d, ih: %d, th: %d, headroom: %d, tailroom: %d, tail: %d"
        ", ip ID: %d, frag: %d\n"
        ", skb ip sum: %d, skb csum: %d, skb csum start: %d, skb csum off: %d, ip check: %d, udp check: %d\n"
        ", [DELTA]: %d, FRAGS: %d IP frag_max_size: %d"
        ", gso(type, segs, size): %d, %d, %d\n"
        ", %s"
        , __func__, __LINE__, extra_dbg_string, iph->protocol, skb->protocol
        , (void *) &s_addr, s_port, (void *) &d_addr, d_port, print_s_mac, print_d_mac
        , skb->pkt_type
        , skb->len, skb->data_len, skb->hdr_len, transport_hdr_len
        , udph->len, ntohs(udph->len), htons(udph->len)
        , tot_hdr_len
        , skb_network_header_len(skb), (skb->network_header - skb->mac_header)
        , skb->mac_header, skb->network_header, skb->transport_header
        , skb_headroom(skb), skb_tailroom(skb), skb->tail
        , ntohs(iph->id), ntohs(iph->frag_off)
        , skb->ip_summed, skb->csum, skb->csum_start, skb->csum_offset, iph->check, udph->check
        , user_data_len, skb_shinfo(skb)->nr_frags, IPCB(skb)->frag_max_size
        , skb_shinfo(skb)->gso_type, skb_shinfo(skb)->gso_segs, skb_shinfo(skb)->gso_size
        , skb_dev_buf
        );
    return;
  }

  if (!user_data || ret == -EIO) {
    iprint2(LVL_DBG, "%s:%d %s -- small data! PROTO: %d, SKB PROT %d\n"
        "src: %pI4, %d, dst: %pI4, %d SMAC: %s, DMAC: %s, PKT TYPE: %d\n"
        ", len: %d, dlen: %d, hlen: %d, udp hlen: %d, udp->len: %d, nh: %d, hn: %d, data hlen: %d"
        ", ip hlen: %d, mac hlen: %d\n"
        ", mh: %d, ih: %d, th: %d, headroom: %d, tailroom: %d, tail: %d"
        ", ip ID: %d, frag: %d\n"
        ", skb ip sum: %d, skb csum: %d, skb csum start: %d, skb csum off: %d, ip check: %d, udp check: %d\n"
        ", [DELTA]: %d, FRAGS: %d IP frag_max_size: %d"
        ", gso(type, segs, size): %d, %d, %d\n"
        ", %s"
        , __func__, __LINE__, extra_dbg_string, iph->protocol, skb->protocol
        , (void *) &s_addr, s_port, (void *) &d_addr, d_port, print_s_mac, print_d_mac
        , skb->pkt_type
        , skb->len, skb->data_len, skb->hdr_len, transport_hdr_len
        , udph->len, ntohs(udph->len), htons(udph->len)
        , tot_hdr_len
        , skb_network_header_len(skb), (skb->network_header - skb->mac_header)
        , skb->mac_header, skb->network_header, skb->transport_header
        , skb_headroom(skb), skb_tailroom(skb), skb->tail
        , ntohs(iph->id), ntohs(iph->frag_off)
        , skb->ip_summed, skb->csum, skb->csum_start, skb->csum_offset, iph->check, udph->check
        , user_data_len, skb_shinfo(skb)->nr_frags, IPCB(skb)->frag_max_size
        , skb_shinfo(skb)->gso_type, skb_shinfo(skb)->gso_segs, skb_shinfo(skb)->gso_size
        , skb_dev_buf
        );
    return;
  }

  first_byte = ((uint8_t *) user_data)[0];
  second_byte = ((uint8_t *) user_data)[1];
  third_byte = ((uint8_t *) user_data)[2];
  fourth_byte = ((uint8_t *) user_data)[3];
  fifth_byte = ((uint8_t *) user_data)[4];
  sixth_byte = ((uint8_t *) user_data)[5];
  seventh_byte = ((uint8_t *) user_data)[6];
  eighth_byte = ((uint8_t *) user_data)[7];

  iprint2(LVL_DBG, "%s:%d %s -- PROTO: %d, SKB PROT %d\n"
      "src: %pI4, %d, dst: %pI4, %d, SMAC: %s, DMAC: %s, PKT TYPE: %d\n"
      ", len: %d, dlen: %d, hlen: %d, udp hlen: %d, udp->len: %d, nh: %d, hn: %d, data hlen: %d\n"
      ", ip hlen: %d, mac hlen: %d"
      ", ip ID: %d, frag: %d, headroom: %d, tailroom: %d\n"
      ", skb ip sum: %d, skb csum: %d, skb csum start: %d, skb csum off: %d, ip check: %d, udp check: %d\n"
      ", [0-7]: %d %d %d %d %d %d %d %d"
      ", [DELTA]: %d, FRAGS: %d [%s], IP frag_max_size: %d"
      ", gso(type, segs, size): %d, %d, %d\n"
      ", %s"
      , __func__, __LINE__, extra_dbg_string, iph->protocol, skb->protocol
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port, print_s_mac, print_d_mac
      , skb->pkt_type
      , skb->len, skb->data_len, skb->hdr_len, transport_hdr_len
      , udph->len, ntohs(udph->len), htons(udph->len)
      , tot_hdr_len
      , skb_network_header_len(skb), (skb->network_header - skb->mac_header)
      , ntohs(iph->id), ntohs(iph->frag_off)
      , skb_headroom(skb), skb_tailroom(skb)
      , skb->ip_summed, skb->csum, skb->csum_start, skb->csum_offset, iph->check, udph->check
      , first_byte, second_byte, third_byte, fourth_byte
      , fifth_byte, sixth_byte, seventh_byte, eighth_byte
      , user_data_len, skb_shinfo(skb)->nr_frags, paged_data
      , IPCB(skb)->frag_max_size
      , skb_shinfo(skb)->gso_type, skb_shinfo(skb)->gso_segs, skb_shinfo(skb)->gso_size
      , skb_dev_buf
      );

  user_data2 = skb_header_pointer(skb, 0, skb->len, paged_data2);
  if (!user_data2)
    return;

  //print_pkt_payload(user_data2, tot_hdr_len, extra_dbg_string);
#endif
}

void
__mod_print_tcp_skb(struct sk_buff *skb, char *extra_dbg_string)
{
#if SME_DEBUG_LVL <= LVL_DBG
  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  struct bnx2x *bp;

  unsigned char smac[ETH_HEADER_LEN], dmac[ETH_HEADER_LEN];
  unsigned char print_s_mac[ETH_PRINT_HDR_LEN], print_d_mac[ETH_PRINT_HDR_LEN];
  uint32_t s_addr = 0, d_addr = 0, seqno = 0, seqack = 0;
  uint16_t s_port = 0, d_port = 0;
  uint8_t protocol = 0;
  char *user_data = NULL;
  uint32_t user_data_len = 0;
  uint32_t delta_len = 0;
  int transport_hdr_len = 0;
  int tot_hdr_len = 0;
  uint8_t first_byte = 0;
  uint8_t second_byte = 0;
  uint8_t third_byte = 0;
  uint8_t fourth_byte = 0;
  uint8_t fifth_byte = 0;
  uint8_t sixth_byte = 0;
  uint8_t seventh_byte = 0;
  uint8_t eighth_byte = 0;
  struct inet_sock *inet_sock = NULL;
  struct inet_connection_sock *inet_c_sock = NULL;
  struct tcp_sock *tcp_sock = NULL;

  int ret = 0;

  char dbg_string[32];
  memset(dbg_string, 0, 32);
  if (!extra_dbg_string)
    extra_dbg_string = dbg_string;

  if (!skb) {
    return;
  }

  ret = parse_skb_headers(skb, smac, dmac, &s_addr, &d_addr, &s_port, &d_port,
      &protocol, &tot_hdr_len, &delta_len, &user_data_len, &user_data,
      (void *) &eth, (void *) &iph, (void *) &tcph, NULL);

  if (ret == -EINVAL) {
    iprintk(LVL_EXP, "TCP RETURNED INVAL!!!!!");
    return;
  }

  if (!tcph || !iph)
    return;

  seqno = htonl(tcph->seq);
  seqack = htonl(tcph->ack_seq);

  bp = netdev_priv(skb->dev);

  if (skb->sk) {
    inet_sock = inet_sk(skb->sk);
    inet_c_sock = inet_csk(skb->sk);
    tcp_sock = tcp_sk(skb->sk);
  }

//  if (strncmp(extra_dbg_string, "bnx2x_start_xmit", strlen("bnx2x_start_xmit")) == 0)
//    return;

  iprint(LVL_INFO, "%s\n"
//      "TS %llu head %p data %p tcph %p th %d diff %d\n"
      "[%pI4 %u, %pI4 %u] seq (%u:%u) "
//      "orig (%u:%u)\n"
//      "ntohl of seq (%u:%u) ntohl of orig (%u:%u) htonl of seq (%u:%u)\n"
      "PAYLOAD %d len %d nlen %d sacked 0x%0x"
//      "tcp timestamp %u"
      "\nip ID %d ttl %d "
      "[%d %d %d %d %d %d %d %d] "
      "urg ptr %d window %u %u rcv_wscale %u"
      "\n[TCP_SOCK] snd_ssthresh %d snd_cwnd %d snd_cwnd_cnt %d "
      "snd_cwnd_used %d pkts %d max pkts %d max_window %d"
      "\nprior_cwnd %d rcv_wnd %d snd_wnd %d is_cwnd_limited %d app_limited %d "
      "sk state %d CA %d delivered %d segs %u data_segs %u"
      "\nbytes_acked %lld icsk_pending %d snd_cwnd_stamp %d "
      "snd_una %u snd_nxt %u inflight %d"
//      "\n[SK_SOCK] sk_sndbuf %d wmem_queued %d wmem_alloc %d prot_mem_alloc %ld "
//      "sk_pacing_rate %d tsq_flags %ld\n"
      , extra_dbg_string
//      , rdtsc(), skb->head, skb->data, tcph, skb->transport_header
//      , (int) ((unsigned long) tcph - (unsigned long) skb->head)
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port, seqno, seqack
//      , tcph->seq, tcph->ack_seq, ntohl(seqno), ntohl(seqack)
//      , ntohl(tcph->seq), ntohl(tcph->ack_seq)
//      , htonl(seqno), htonl(seqack)
      , delta_len, skb->len, user_data_len
      , TCP_SKB_CB(skb)->sacked
//      , tcp_time_stamp
      , ntohs(iph->id), iph->ttl, tcph->fin, tcph->syn, tcph->rst, tcph->psh
      , tcph->ack, tcph->urg, tcph->ece, tcph->cwr
      , ntohs(tcph->urg_ptr), tcph->window, ntohs(tcph->window)
      , (tcp_sock ? tcp_sock->rx_opt.rcv_wscale : -1)
       ,(tcp_sock ? tcp_sock->snd_ssthresh : -1)
       ,(tcp_sock ? tcp_sock->snd_cwnd : -1)
       ,(tcp_sock ? tcp_sock->snd_cwnd_cnt : -1)
       ,(tcp_sock ? tcp_sock->snd_cwnd_used : -1)
       ,(tcp_sock ? tcp_sock->packets_out : -1)
       ,(tcp_sock ? tcp_sock->max_packets_out : -1)
       ,(tcp_sock ? tcp_sock->max_window : -1)
       ,(tcp_sock ? tcp_sock->prior_cwnd : -1)
       ,(tcp_sock ? tcp_sock->rcv_wnd : -1)
       ,(tcp_sock ? tcp_sock->snd_wnd : -1)
       ,(tcp_sock ? tcp_sock->is_cwnd_limited : -1)
       ,(tcp_sock ? tcp_sock->app_limited : -1)
       ,skb->sk->sk_state
       ,(inet_c_sock ? inet_c_sock->icsk_ca_state : -1)
       ,(tcp_sock ? tcp_sock->delivered : -1)
       ,(tcp_sock ? tcp_sock->segs_out : -1)
       ,(tcp_sock ? tcp_sock->data_segs_out : -1)
       ,(tcp_sock ? tcp_sock->bytes_acked : -1)
       ,(inet_c_sock ? inet_c_sock->icsk_pending : -1)
       ,(tcp_sock ? tcp_sock->snd_cwnd_stamp : -1)
       ,(tcp_sock ? tcp_sock->snd_una : -1)
       ,(tcp_sock ? tcp_sock->snd_nxt: -1)
       ,(tcp_sock ? tcp_packets_in_flight(tcp_sock) : -1)
//       ,(skb->sk ? skb->sk->sk_sndbuf : -1)
//       ,(skb->sk ? skb->sk->sk_wmem_queued : -1)
//       ,(skb->sk ? sk_wmem_alloc_get(skb->sk) : -1)
//       ,(skb->sk ? (skb->sk->sk_prot ?
//             atomic_long_read(skb->sk->sk_prot->memory_allocated) : -1) : -1)
//       ,(skb->sk ? skb->sk->sk_pacing_rate : -1)
//       ,(tcp_sock ? tcp_sock->tsq_flags : -1)
      );

  return;

  if (ret == -EIO) {
    iprint2(LVL_DBG, "%s:%d %s -- PROTO: %d, SKB PROT %d\n"
        ", skb: %p, sk state: %x\n"
        ", src: %pI4, %d, dst: %pI4, %d, SMAC: %s, DMAC: %s, PKT TYPE: %d\n"
        ", seq: %u, seqack: %u"
        ", flags[FIN,SYN,RST,PSH,ACK,URG,ECE,CWR]: %d %d %d %d %d %d %d %d\n"
        ", len: %d, dlen: %d, hlen: %d, tcp hlen: %d, data hlen: %d"
        ", ip hlen: %d, ip tot len: %d, mac hlen: %d\n"
        ", mh: %d, ih: %d, th: %d, headroom: %d, tailroom: %d"
        ", ip ID: %d, frag: %d\n"
        ", skb ip sum: %d, skb csum: %d, skb csum start: %d, skb csum off: %d"
        ", ip check: %d, tcp check: %d\n"
        ", [DELTA]: %d, FRAGS: %d TCP window: %d IP frag_max_size: %d"
        ", gso(type, segs, size): %d, %d, %d\n"
        , __func__, __LINE__, extra_dbg_string, iph->protocol, skb->protocol
        , skb, (skb->sk ? skb->sk->sk_state : 0)
        , (void *) &s_addr, s_port, (void *) &d_addr, d_port
        , print_s_mac, print_d_mac, skb->pkt_type, seqno, seqack
        , tcph->fin, tcph->syn, tcph->rst, tcph->psh
        , tcph->ack, tcph->urg, tcph->ece, tcph->cwr
        , skb->len, skb->data_len, skb->hdr_len, tcph->doff*4, tot_hdr_len
        , iph->ihl, ntohs(iph->tot_len), (skb->network_header - skb->mac_header)
        , skb->mac_header, skb->network_header, skb->transport_header
        , skb_headroom(skb), skb_tailroom(skb)
        , ntohs(iph->id), ntohs(iph->frag_off)
        , skb->ip_summed, skb->csum, skb->csum_start
        , skb->csum_offset, iph->check, tcph->check
        , user_data_len, skb_shinfo(skb)->nr_frags, tcph->window
        , IPCB(skb)->frag_max_size
        , skb_shinfo(skb)->gso_type, skb_shinfo(skb)->gso_segs
        , skb_shinfo(skb)->gso_size
        );
    return;
  }

  if (!user_data || ret == -EIO) {
    iprint2(LVL_DBG, "%s:%d %s -- small data! PROTO: %d, SK PROT %d sk state: %x\n"
      ", src: %pI4, %d, dst: %pI4, %d, SMAC: %s, DMAC: %s, PKT TYPE: %d\n"
      ", seq: %u, seqack: %u"
      ", flags[FIN,SYN,RST,PSH,ACK,URG,ECE,CWR]: %d %d %d %d %d %d %d %d\n"
      ", len: %d, dlen: %d, hlen: %d, tcp hlen: %d, data hlen: %d"
      ", ip hlen: %d, ip tot len: %d, mac hlen: %d\n"
      ", mh: %d, ih: %d, th: %d, headroom: %d, tailroom: %d"
      ", ip ID: %d, frag: %d\n"
      ", skb ip sum: %d, skb csum: %d, skb csum start: %d, skb csum off: %d"
      ", ip check: %d, tcp check: %d\n"
      ", [DELTA]: %d, FRAGS: %d, TCP window: %d IP frag_max_size: %d"
      ", gso(type, segs, size): %d, %d, %d\n"
      , __func__, __LINE__, extra_dbg_string, iph->protocol, skb->protocol
      , (skb->sk ? skb->sk->sk_state : 0)
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port
      , print_s_mac, print_d_mac, skb->pkt_type, seqno, seqack
      , tcph->fin, tcph->syn, tcph->rst, tcph->psh
      , tcph->ack, tcph->urg, tcph->ece, tcph->cwr
      , skb->len, skb->data_len, skb->hdr_len, transport_hdr_len, tot_hdr_len
      , skb_network_header_len(skb), ntohs(iph->tot_len)
      , (skb->network_header - skb->mac_header)
      , skb->mac_header, skb->network_header, skb->transport_header
      , skb_headroom(skb), skb_tailroom(skb)
      , ntohs(iph->id), ntohs(iph->frag_off), skb->ip_summed, skb->csum
      , skb->csum_start, skb->csum_offset, iph->check, tcph->check
      , user_data_len, skb_shinfo(skb)->nr_frags
      , tcph->window, IPCB(skb)->frag_max_size
      , skb_shinfo(skb)->gso_type, skb_shinfo(skb)->gso_segs
      , skb_shinfo(skb)->gso_size
      );
    return;
  }

  first_byte = ((uint8_t *) user_data)[0];
  second_byte = ((uint8_t *) user_data)[1];
  third_byte = ((uint8_t *) user_data)[2];
  fourth_byte = ((uint8_t *) user_data)[3];
  fifth_byte = ((uint8_t *) user_data)[4];
  sixth_byte = ((uint8_t *) user_data)[5];
  seventh_byte = ((uint8_t *) user_data)[6];
  eighth_byte = ((uint8_t *) user_data)[7];

  iprint2(LVL_DBG, "%s:%d %s -- PROTO: %d, SKB PROT %d\n"
      ", skb: %p, sk state: %x\n"
      ", src: %pI4, %d, dst: %pI4, %d, SMAC: %s, DMAC: %s, PKT TYPE: %d\n"
      ", seq: %u, seqack: %u"
      ", flags[FIN,SYN,RST,PSH,ACK,URG,ECE,CWR]: %d %d %d %d %d %d %d %d\n"
      ", len: %d, dlen: %d, hlen: %d, tcp hlen: %d, data hlen: %d"
      ", ip hlen: %d, ip tot len: %d, mac hlen: %d\n"
      ", mh: %d, ih: %d, th: %d, headroom: %d, tailroom: %d"
      ", ip ID: %d, frag: %d\n"
      ", skb ip sum: %d, skb csum: %d, skb csum start: %d, csum off: %d, ip check: %d, tcp check: %d\n"
      ", [0-7]: %d %d %d %d %d %d %d %d"
      ", [DELTA]: %d, FRAGS: %d, TCP window: %d IP frag_max_size: %d"
      ", gso(type, segs, size): %d, %d, %d\n"
      , __func__, __LINE__, extra_dbg_string, iph->protocol, skb->protocol
      , skb, (skb->sk ? skb->sk->sk_state : 0)
      , (void *) &s_addr, s_port, (void *) &d_addr, d_port, print_s_mac, print_d_mac
      , skb->pkt_type
      , seqno, seqack
      , tcph->fin, tcph->syn, tcph->rst, tcph->psh, tcph->ack, tcph->urg, tcph->ece, tcph->cwr
      , skb->len, skb->data_len, skb->hdr_len, transport_hdr_len, tot_hdr_len
      , iph->ihl, ntohs(iph->tot_len), (skb->network_header - skb->mac_header)
      , skb->mac_header, skb->network_header, skb->transport_header
      , skb_headroom(skb), skb_tailroom(skb)
      , ntohs(iph->id), ntohs(iph->frag_off)
      , skb->ip_summed, skb->csum, skb->csum_start, skb->csum_offset, iph->check, tcph->check
      , first_byte, second_byte, third_byte, fourth_byte
      , fifth_byte, sixth_byte, seventh_byte, eighth_byte
      , user_data_len, skb_shinfo(skb)->nr_frags
      , tcph->window, IPCB(skb)->frag_max_size
      , skb_shinfo(skb)->gso_type, skb_shinfo(skb)->gso_segs, skb_shinfo(skb)->gso_size
      );

  //print_pkt_payload(user_data2, tot_hdr_len, extra_dbg_string);
#endif
}

void
print_pkt_payload(char *data, int data_len, char *extra_dbg_string)
{
  int i;
  char *ptr = NULL;
  char print_buf[DBG_BUF_SIZE];
  int print_off = 0, print_len = 0;

  char dbg_string[2];

  if (!data)
    return;

  if (!extra_dbg_string) {
    memset(dbg_string, 0, 2);
    extra_dbg_string = dbg_string;
    sprintf(dbg_string, " ");
  }

  memset(print_buf, 0, DBG_BUF_SIZE);
  print_len = data_len;
  if (data_len > DBG_DATA_LEN)
    print_len = DBG_DATA_LEN;

  i = 0;
  for ( ; i < print_len; ) {
    ptr = data + i;
    sprintf(print_buf+print_off, "%d %d %d %d %d %d %d %d\n"
        , ((uint8_t *) ptr)[0], ((uint8_t *) ptr)[1], ((uint8_t *) ptr)[2], ((uint8_t *) ptr)[3]
        , ((uint8_t *) ptr)[4], ((uint8_t *) ptr)[5], ((uint8_t *) ptr)[6], ((uint8_t *) ptr)[7]);
    i += 8;
    print_off = strlen(print_buf);
  }
  iprint(LVL_DBG, "%s --\n%s", extra_dbg_string, print_buf);
}

void
__mod_print_sk_buff_int(struct sk_buff *skb, char *extra_dbg_string)
{
#if SME_DEBUG_LVL <= LVL_ERR
  struct iphdr *iph = NULL;
  if (!skb) {
    iprintk(LVL_EXP, "==== NULL SKB ====");
    return;
  }

  iph = (struct iphdr *) skb_network_header(skb);
  if (!iph)
    return;

  if (iph->protocol == IPPROTO_TCP) {
    __mod_print_tcp_skb(skb, extra_dbg_string);
  } else {
    __mod_print_udp_skb(skb, extra_dbg_string);
  }
#endif
}

int
parse_skb_headers(struct sk_buff *skb,
    unsigned char *src_mac, unsigned char *dst_mac,
    uint32_t *src_ip, uint32_t *dst_ip, uint16_t *src_port, uint16_t *dst_port,
    uint8_t *protocol, uint32_t *tot_hdr_len_p, uint32_t *delta_len_p,
    uint32_t *user_data_len_p, char **user_data_p,
    void **eth_p, void **iph_p, void **tcph_p, void **udph_p)
{
  int ret;
  uint32_t s_addr, d_addr;
  uint16_t s_port, d_port;
  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  struct udphdr *udph = NULL;
  unsigned int mac_hdr_len, network_hdr_len, transport_hdr_len, tot_hdr_len;
  int ip_tot_len, ip_ihl;
  int delta;
  int user_data_off, user_data_len;
  char *user_data = NULL;
  char page[DBG_BUF_SIZE];

  if (!skb || !skb->head)
    return -EINVAL;

  eth = eth_hdr(skb);
  if ((unsigned char *) eth == skb->head)
    return -EINVAL;

  if (eth_p)
    *eth_p = (void *) eth;

  iph = ip_hdr(skb);
  if ((unsigned char *) iph == skb->head)
    return -EINVAL;

  if (iph_p)
    *iph_p = (void *) iph;

  ret = check_valid_protocol(iph->protocol);
  if (!ret)
    return -EINVAL;

  if (iph->protocol == IPPROTO_TCP) {
    tcph = tcp_hdr(skb);
    if ((unsigned char *) tcph == skb->head)
      return -EINVAL;
    s_port = ntohs((unsigned short int) tcph->source);
    d_port = ntohs((unsigned short int) tcph->dest);
    transport_hdr_len = tcph->doff * 4;
    if (tcph_p)
      *tcph_p = (void *) tcph;
  } else {
    udph = udp_hdr(skb);
    if ((unsigned char *) udph == skb->head)
      return -EINVAL;
    s_port = ntohs((unsigned short int) udph->source);
    d_port = ntohs((unsigned short int) udph->dest);
    transport_hdr_len = sizeof(struct udphdr);
    if (udph_p)
      *udph_p = (void *) udph;
  }
  ret = check_relevant_ports(s_port, d_port);
  if (!ret) {
    iprint(LVL_DBG, "src %pI4 %u, dst %pI4 %u txq %d caller %pS"
        , (void *) &iph->saddr, s_port, (void *) &iph->daddr, d_port
        , skb_get_queue_mapping(skb)
        , __builtin_return_address(0));
    return -EINVAL;
  }

  s_addr = iph->saddr;
  d_addr = iph->daddr;

  network_hdr_len = skb->transport_header - skb->network_header;
  mac_hdr_len = skb->network_header - skb->mac_header;
  if (is_destination_ip_me(d_addr))
    mac_hdr_len = 0;
  tot_hdr_len = transport_hdr_len + network_hdr_len + mac_hdr_len;

  if (src_mac) {
    eth_zero_addr((u8 *) src_mac);
    ether_addr_copy((u8 *) src_mac, (u8 *) eth->h_source);
    //memset(src_mac, 0, ETH_HEADER_LEN);
    //memcpy(src_mac, eth->h_source, ETH_HEADER_LEN);
  }
  if (dst_mac) {
    eth_zero_addr((u8 *) dst_mac);
    ether_addr_copy((u8 *) dst_mac, (u8 *) eth->h_dest);
    //memset(src_mac, 0, ETH_HEADER_LEN);
    //memcpy(src_mac, eth->h_source, ETH_HEADER_LEN);
  }
  if (src_ip)
    *src_ip = s_addr;
  if (dst_ip)
    *dst_ip = d_addr;
  if (src_port)
    *src_port = s_port;
  if (dst_port)
    *dst_port = d_port;
  if (protocol)
    *protocol = iph->protocol;
  if (tot_hdr_len_p)
    *tot_hdr_len_p = tot_hdr_len;

  ip_tot_len = ntohs(iph->tot_len);
  ip_ihl = (unsigned int) (((uint8_t) (((char *) iph)[0]) & 0x0f) << 2);
  delta = ((ip_tot_len - ip_ihl) - transport_hdr_len);

  user_data_len = skb->len - tot_hdr_len;
  user_data_off = tot_hdr_len;

  if (delta_len_p)
    *delta_len_p = delta;

  if (user_data_len_p)
    *user_data_len_p = user_data_len;

  if (delta <= 0)
    return -EIO;

  if (is_destination_port_dummy_server(d_port)) {
    memset(page, 0, DBG_BUF_SIZE);
    user_data = skb_header_pointer(skb, user_data_off, user_data_len, page);
    if (!user_data)
      return -EINVAL;
  }

  if (user_data_p)
    *user_data_p = user_data;

  return 0;
}

int
parse_raw_skb_headers(char *data, int data_len,
    unsigned char *src_mac, unsigned char *dst_mac,
    uint32_t *src_ip, uint32_t *dst_ip, uint16_t *src_port, uint16_t *dst_port,
    uint8_t *protocol, uint32_t *tot_hdr_len_p, uint32_t *user_data_len_p,
    char **user_data_p, uint32_t *mac_hdr_off, uint32_t *ip_hdr_off,
    uint32_t *trans_hdr_off, uint32_t *trans_hdr_len)
{
  int ret;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  struct ethhdr *eth = NULL;
  struct iphdr *iph = NULL;
  struct tcphdr *tcph = NULL;
  struct udphdr *udph = NULL;
  int mac_hdr_len = ETH_HLEN;
  int ip_hdr_len = 0, transport_hdr_len = 0;
  uint32_t network_hdr_off = 0, transport_hdr_off = 0;
  uint32_t min_network_hdr_len = 0, min_transport_hdr_len = 0;
  uint32_t tot_hdr_len = 0;

  if (!data || !data_len)
    return -EINVAL;

  eth = (struct ethhdr *) data;

  network_hdr_off = mac_hdr_len;
  min_network_hdr_len = sizeof(struct iphdr);
  if (data_len < (network_hdr_off + min_network_hdr_len)) {
    iprint2(LVL_DBG, "v small data %d", data_len);
    return -EIO;
  }

  iph = (struct iphdr *) (data + network_hdr_off);
  ret = check_valid_protocol(iph->protocol);
  if (!ret)
    return -EINVAL;

  if (protocol)
    *protocol = iph->protocol;

  s_addr = iph->saddr;
  d_addr = iph->daddr;
  ip_hdr_len = iph->ihl * 4;
  transport_hdr_off = network_hdr_off + ip_hdr_len;
  if (iph->protocol == IPPROTO_TCP) {
    min_transport_hdr_len = sizeof(struct tcphdr);
    if (data_len < (transport_hdr_off + min_transport_hdr_len))
      return -EIO;

    tcph = (struct tcphdr *) (data + transport_hdr_off);
    s_port = ntohs(tcph->source);
    d_port = ntohs(tcph->dest);
    transport_hdr_len = tcph->doff * 4;
  } else {
    min_transport_hdr_len = sizeof(struct udphdr);
    if (data_len < (transport_hdr_off + min_transport_hdr_len))
      return -EIO;

    udph = (struct udphdr *) (data + transport_hdr_off);
    s_port = ntohs(udph->source);
    d_port = ntohs(udph->dest);
    transport_hdr_len = min_transport_hdr_len;
  }

  ret = check_relevant_ports(s_port, d_port);
  if (!ret)
    return -EINVAL;

  tot_hdr_len = mac_hdr_len + ip_hdr_len + transport_hdr_len;

  if (src_mac) {
    eth_zero_addr((u8 *) src_mac);
    ether_addr_copy((u8 *) src_mac, (u8 *) eth->h_source);
    //memset(src_mac, 0, ETH_HEADER_LEN);
    //memcpy(src_mac, eth->h_source, ETH_HEADER_LEN);
  }
  if (dst_mac) {
    eth_zero_addr((u8 *) dst_mac);
    ether_addr_copy((u8 *) dst_mac, (u8 *) eth->h_dest);
    //memset(dst_mac, 0, ETH_HEADER_LEN);
    //memcpy(dst_mac, eth->h_dest, ETH_HEADER_LEN);
  }
  if (src_ip)
    *src_ip = s_addr;
  if (dst_ip)
    *dst_ip = d_addr;
  if (src_port)
    *src_port = s_port;
  if (dst_port)
    *dst_port = d_port;
  if (tot_hdr_len_p)
    *tot_hdr_len_p = tot_hdr_len;
  if (user_data_len_p)
    *user_data_len_p = (data_len - tot_hdr_len);
  if (user_data_p)
    *user_data_p = data + tot_hdr_len;
  if (mac_hdr_off)
    *mac_hdr_off = 0;
  if (ip_hdr_off)
    *ip_hdr_off = network_hdr_off;
  if (trans_hdr_off)
    *trans_hdr_off = transport_hdr_off;
  if (trans_hdr_len)
    *trans_hdr_len = transport_hdr_len;
  return 0;
}

