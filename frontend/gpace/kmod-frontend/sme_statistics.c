/*
 * sme_statistics.c
 *
 * created on: Sep 27, 2017
 * author: aasthakm
 */

#ifndef SME_STATISTICS_C_
#define SME_STATISTICS_C_

#include "sme_statistics.h"
#include "sme_debug.h"
#include "sme_config.h"

#include <linux/gfp.h>
#include <linux/slab.h>
//#include <linux/random.h>
#include <asm-generic/div64.h>

int
init_sme_stats (sme_stat *stat, char *name, long start, long end, long step)
{
#if SME_CONFIG_STAT_DETAIL == 1
  int size;
#endif

  memcpy (stat->name, name, strlen (name));
  stat->start = start;
  stat->end = end;
  stat->step = step;

  stat->count = 0;
  stat->min = 99999999999;
  stat->max = 0;

#if SME_CONFIG_STAT_DETAIL == 1
  size = (end - start) / step + 1;
  stat->dist = kzalloc (size * sizeof(unsigned int), GFP_KERNEL);

  if (!stat->dist)
    return -ENOMEM;
#endif

  return 0;
}

int
init_sme_stats_arr(sme_stat *stat, long start, long end, long step,
    char name[], ...)
{
#if SME_CONFIG_STAT_DETAIL == 1
  int size;
#endif
  va_list args;

  va_start(args, name);
  vsnprintf(stat->name, sizeof(stat->name), name, args);
  va_end(args);
  //memcpy (stat->name, name, strlen (name));
  stat->start = start;
  stat->end = end;
  stat->step = step;

  stat->count = 0;
  stat->min = 99999999999;
  stat->max = 0;

#if SME_CONFIG_STAT_DETAIL == 1
  size = (end - start) / step + 1;
  stat->dist = kzalloc (size * sizeof(unsigned int), GFP_KERNEL);

  if (!stat->dist)
    return -ENOMEM;
#endif

  return 0;
}

void inline
add_point (sme_stat *stat, long value)
{
  int index;

  if (value >= stat->max)
    stat->max = value;

  if (value < stat->min)
    stat->min = value;

  index = (value - stat->start) / stat->step;

  if (value >= stat->end) {
    //append to the last bucket
    index = ((stat->end - stat->start) / stat->step) - 1;
  } else if (value < stat->start) {
    //append to the first bucket
    index = 0;
  }

#if SME_CONFIG_STAT_DETAIL == 1
  stat->dist[index]++;
#endif
  stat->count++;
  stat->sum += value;
}

void inline
add_custom_point (sme_stat *stat, int index, long value)
{
  int max_index = (stat->end - stat->start) / stat->step;

  if (value >= stat->max)
    stat->max = value;

  if (value < stat->min)
    stat->min = value;

  if (index < 0)
    index = stat->start;
  else if (index > max_index)
    index = max_index;

#if 0
  index = (value - stat->start) / stat->step;

  if (value >= stat->end) {
    //append to the last bucket
    index = ((stat->end - stat->start) / stat->step) - 1;
  } else if (value < stat->start) {
    //append to the first bucket
    index = 0;
  }
#endif

#if SME_CONFIG_STAT_DETAIL == 1
  stat->dist[index]++;
#endif
  stat->count++;
  stat->sum += value;
}

void
print_distribution (sme_stat *stat)
{

#if SME_CONFIG_STAT_DETAIL == 1
  int i;
  long bucket;
  int size = (stat->end - stat->start) / stat->step;
#endif

  //double avg = 0;
  long avg_div = 0;
  long avg_mod = 0;
  if (stat->count != 0) {
    avg_div = stat->sum;
    avg_mod = do_div(avg_div, stat->count);
    //avg_div = (stat->sum / (uint64_t) stat->count);
  }

  iprintk2(LVL_EXP, "");
  iprintk2(LVL_EXP, "-----------------------------------");
  iprint2(LVL_EXP, "%s", stat->name);

  if(stat->count == 0)
    return;

  iprint2(LVL_EXP, "%s Total time: %llu", stat->name, stat->sum);
  iprint2(LVL_EXP, "%s Total count: %ld", stat->name, stat->count);

  if (stat->count > 0) {
    iprint2(LVL_EXP, "%s avg: %lu [%lu, %lu]", stat->name, avg_div,
        stat->min, stat->max);
    //iprint2(LVL_EXP, "%s avg: %lu %lu [%lu, %lu]", stat->name, avg_div,
    //    avg_mod, stat->min, stat->max);

#if SME_CONFIG_STAT_DETAIL == 1
    for (i = 0; i < size; i++) {
      if (stat->dist[i] == 0)
        continue;

      bucket = stat->start + i * stat->step;
      iprint2(LVL_EXP, "%-*lu\t%-d", 7, bucket, stat->dist[i]);
    }
#endif

  }
}

void
serialize_distribution (sme_stat *stat, char *ser, int len)
{
  char *pivot;
  int written = 0;
  int size;
#if SME_CONFIG_STAT_DETAIL == 1
  int i;
#endif

  if (stat == NULL) {
    ((int *) ser)[0] = (-1);
    return;
  }

  //reserve for index
  written += sizeof(int);

  pivot = ser + written;
  memcpy (pivot, stat, sizeof(sme_stat));
  written += sizeof(sme_stat);

  pivot = ser + written;
  size = (stat->end - stat->start) / stat->step;
#if SME_CONFIG_STAT_DETAIL == 1
  for (i = 0; i < size; i++) {
    if (written + sizeof(unsigned int) > len)
      break;

    ((unsigned int *) pivot)[i] = stat->dist[i];
  }

  ((int *) ser)[0] = i;
#endif
}

void
free_sme_stats (sme_stat *stat)
{
#if SME_CONFIG_STAT_DETAIL == 1
  kfree (stat->dist);
#endif
}

#endif /* SME_STATISTICS_C_ */
