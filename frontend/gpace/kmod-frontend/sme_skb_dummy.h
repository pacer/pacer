/*
 * sme_skb_dummy.h
 *
 * created on: Apr 25, 2017
 * author: aasthakm
 *
 * API to create dummy skb
 */

#ifndef __SME_SKB_DUMMY_H__
#define __SME_SKB_DUMMY_H__

#include <linux/skbuff.h>
#include <linux/netdevice.h>

#include "../linux-frontend-4.9.5/drivers/net/ethernet/broadcom/bnx2x/sme/ptcp_hooks.h"

enum {
  SKB_T_SEND = 0,
  SKB_T_RECV,
};

enum {
  TCP_STREAM_ALLOC = 0,
  TCP_DUMMY_ALLOC,
};

void build_dummy_skb_with_data(struct sk_buff **dummy_skb_p, char *payload,
    int plen_in_hdr, uint32_t s_addr, uint32_t d_addr, uint16_t s_port,
    uint16_t d_port, uint32_t seqno, uint32_t seqack, uint8_t protocol,
    char *s_mac, char *d_mac, struct net_device *dev, int coreid);
void build_dummy_tcp_skb(struct sk_buff **dummy_skb_p, struct sock *sk,
    int alloc_type, uint32_t seqno, uint32_t seqack);
void free_dummy_skb(struct sk_buff **dummy_skb_p);

#endif /* __SME_SKB_DUMMY_H__ */
