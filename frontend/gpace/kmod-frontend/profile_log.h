/*
 * profile_log.h
 *
 * created on: Jun 22, 2017
 * author: aasthakm
 *
 * profile log datastructure
 */

#ifndef __PROFILE_LOG_H__
#define __PROFILE_LOG_H__

#include <linux/timerqueue.h>
#include "sme_config.h"
#include "sme_skb.h"
#include "conn.h"
#include "list.h"

typedef struct log {
  struct log_func *fn;
  list_t log_list;
} log_t;

typedef struct log_elem {
  /*
   * gives the TS, payload len, seq, ack and I/O direction
   */
  partial_sk_buff_t pskb;
  /*
   * hold the marker data
   */
  void *optional_data;
  int optional_data_len;
  list_t log_listp;

#if CONFIG_PROF_MINI_LOG
  /* This is the total count of response packets so far
   */
  uint32_t count;
#endif

} log_elem_t;

typedef struct log_func {
  void (*create_log_record) (void *buf, log_elem_t *e, conn_t *c, int scm_type);
} log_func_t;

extern log_t profile_log;
extern log_func_t log_ascii_fn;
extern log_func_t log_binary_fn;

void alloc_init_log_elem(log_elem_t **le, partial_sk_buff_t *pskb,
    void *optional_data, int optional_data_len);
void add_log_elem(log_t *p_log, log_elem_t *le);
int get_sizeof_log_elem(log_elem_t *le);

int init_log(log_t *p_log, log_func_t *fn);
void cleanup_log(log_t *p_log);
void generate_log(log_t *p_log, conn_t *c, int scm_type, char *wbuf);
void write_log(log_t *p_log, conn_t *c, int scm_type);
int count_log_entries_size(log_t *p_log, int *e_count, int *e_size);

#endif /* __PROFILE_LOG_H__ */
