/*
 * logger.h
 *
 * created on: Dec 05, 2017
 * author: aasthakm
 *
 * datastructures and functions to log packets
 * for profiling.
 */

#ifndef __LOGGER_H__
#define __LOGGER_H__

#include "sme_config.h"
#include "sme_debug.h"

#include "profile_map.h"
#include "../../include/log_format.h"
#include "sme_q_common.h"
#include "sme_q_single.h"

/*
 * scm[0]: holds frontend pcm's
 * scm[1]: holds backend pcm's
 */

int is_pcm_ready_for_logging(profile_conn_map_t *pcm);
int gc_pcm_list(list_t *pcm_gc_list, int idx);
int gc_pcm_q(pcm_q_t *pcm_gcq, int idx);
int move_pcm_to_gc_list_pacer(list_t *pcm_gc_list, profile_conn_map_t *pcm);
int move_to_gc_list_pacer_all(list_t *scm_list, list_t *pcm_gc_list);
int do_enflog_one_pcm(profile_conn_map_t *pcm);

int setup_logger_thread(void *data);
void remove_logger_thread(void *data);

int setup_logger2_thread(void *data);
void remove_logger2_thread(void *data);

#endif /* __LOGGER_H__ */
