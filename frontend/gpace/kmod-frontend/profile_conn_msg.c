/*
 * profile_conn_msg.c
 *
 * created on: Feb 21, 2017
 * author: aasthakm
 *
 * functions to parse the profile control messages from
 * the userspace, and update in kernel profile datastructure
 */

#include <linux/slab.h>
#include <linux/byteorder/generic.h>

#include "../../include/msg_hdr.h"
#include "../../include/profile_conn_format.h"
#include "sme_debug.h"
#include "sme_common.h"
#include "sme_time.h"
#include "profile_map.h"
#include "sme_helper.h"

// forward declarations
void print_profile_from_buf(char *s_buf, int *s_buf_len, char *buf, int buf_len);
void print_profile_from_buf_ihash(char *s_buf, int *s_buf_len,
    char *buf, int buf_len);
void print_profile_list_from_buf(char *s_buf, int *s_buf_len,
    char *buf, int buf_len);
void print_profile_list_from_buf_ihash(char *s_buf, int *s_buf_len,
    char *buf, int buf_len);
void print_profile_id_conn_from_buf(char *s_buf, int *s_buf_len,
    char *buf, int buf_len);
void print_profile_marker_buf(char *s_buf, int *s_buf_len, char *buf, int buf_len);

  int
parse_profile_from_buf_ihash(profile_t *p, char *buf, int buf_len)
{
  int ret = 0;
  int i;
  uint16_t off = 0;
  ihash_t prof_id_ihash;
  uint16_t prof_id;
  uint16_t num_out_reqs;
  uint16_t *num_extra_slots = NULL;
  uint64_t *num_out_frames = NULL;
  uint64_t *spacing = NULL;
  uint64_t *latency = NULL;
  char hbuf[64];
  int hbuf_len = 64;

  if (!p)
    return -EINVAL;

  prof_id_ihash = ((ihash_t *) (buf + off))[0];
  off += sizeof(ihash_t);

  num_out_reqs = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  num_extra_slots = (uint16_t *) kzalloc(sizeof(uint16_t) * num_out_reqs, GFP_KERNEL);
  num_out_frames = (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  spacing = (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  latency = (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  if (!num_extra_slots || !num_out_frames || !spacing || !latency) {
    ret = -ENOMEM;
    goto error;
  }
  for (i = 0; i < num_out_reqs; i++) {
    num_extra_slots[i] = ((uint16_t *) (buf + off))[0];
    off += sizeof(uint16_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    num_out_frames[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    spacing[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    latency[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  //SME_IHASH
  prof_id = 0;
  ret = init_profile(p, prof_id, prof_id_ihash, num_out_reqs, num_extra_slots,
      num_out_frames, spacing, latency);

  memset(hbuf, 0, hbuf_len);
  prt_hash(prof_id_ihash.byte, sizeof(ihash_t), hbuf );

  iprint(LVL_DBG, "INIT PROFILE %s %d, ret: %d", hbuf, num_out_reqs, ret);
  if (ret < 0)
    goto error;

  return 0;

error:
  if (num_extra_slots)
    kfree(num_extra_slots);
  if (num_out_frames)
    kfree(num_out_frames);
  if (spacing)
    kfree(spacing);
  if (latency)
    kfree(latency);
  return ret;
}


int
parse_profile_from_buf(profile_t *p, char *buf, int buf_len)
{
  int ret = 0;
  int i;
  uint16_t off = 0;
  uint16_t prof_id;
  uint16_t num_out_reqs;
  uint16_t *num_extra_slots = NULL;
  uint64_t *num_out_frames = NULL;
  uint64_t *spacing = NULL;
  uint64_t *latency = NULL;

  //SME_IHASH
  //ihash_t prof_id_ihash = { DEFAULT_PROFILE_ID };
  ihash_t prof_id_ihash;
  uint16_t def_prof_id = DEFAULT_PROFILE_ID;
  memset(prof_id_ihash.byte, 0, sizeof(ihash_t));
  memcpy(prof_id_ihash.byte, &def_prof_id, sizeof(uint16_t));

  if (!p)
    return -EINVAL;

  prof_id = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  num_out_reqs = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  num_extra_slots = (uint16_t *) kzalloc(sizeof(uint16_t) * num_out_reqs, GFP_KERNEL);
  num_out_frames = (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  spacing = (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  latency = (uint64_t *) kzalloc(sizeof(uint64_t) * num_out_reqs, GFP_KERNEL);
  if (!num_extra_slots || !num_out_frames || !spacing || !latency) {
    ret = -ENOMEM;
    goto error;
  }

  for (i = 0; i < num_out_reqs; i++) {
    num_extra_slots[i] = ((uint16_t *) (buf + off))[0];
    off += sizeof(uint16_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    num_out_frames[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    spacing[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  for (i = 0; i < num_out_reqs; i++) {
    latency[i] = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
  }

  //SME_IHASH
  ret = init_profile(p, prof_id, prof_id_ihash, num_out_reqs, num_extra_slots,
      num_out_frames, spacing, latency);
  iprint(LVL_INFO, "INIT PROFILE %d %d, ret: %d", prof_id, num_out_reqs, ret);
  if (ret < 0)
    goto error;

  return 0;

error:
  if (num_extra_slots)
    kfree(num_extra_slots);
  if (num_out_frames)
    kfree(num_out_frames);
  if (spacing)
    kfree(spacing);
  if (latency)
    kfree(latency);
  return ret;
}

int
_parse_conn_from_profile_buf(char *buf, int buf_len, conn_t *conn_id, int *offset)
{
  int off = 0;
  if (!conn_id || !offset)
    return -EINVAL;

  conn_id->src_ip = ((uint32_t *) (buf + off))[0];
  conn_id->in_ip = ((uint32_t *) (buf + off))[1];
  conn_id->out_ip = ((uint32_t *) (buf + off))[2];
  conn_id->dst_ip = ((uint32_t *) (buf + off))[3];
  off += PROF_CONN_IP_HDR_LEN;

  conn_id->src_port = ntohs(((uint16_t *) (buf + off))[0]);
  conn_id->in_port = ntohs(((uint16_t *) (buf + off))[1]);
  conn_id->out_port = ntohs(((uint16_t *) (buf + off))[2]);
  conn_id->dst_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += PROF_CONN_PORT_HDR_LEN;

  *offset = off;
  iprint(LVL_DBG, "src %pI4 %u, in %pI4 %u, out %pI4 %u, dst %pI4 %u",
      (void *) &conn_id->src_ip, conn_id->src_port,
      (void *) &conn_id->in_ip, conn_id->in_port,
      (void *) &conn_id->out_ip, conn_id->out_port,
      (void *) &conn_id->dst_ip, conn_id->dst_port);
  return 0;
}

int
parse_profile_list_from_buf_ihash(list_t *scm_list, char *buf, int buf_len)
{
  int ret = 0;
  int i;
  int off = 0;
  int prof_len_off = 0;
  uint16_t n_prof;
  uint64_t prof_off;
  char *prof_buf = NULL;
  sme_channel_map_t *scm = NULL;
  sme_channel_map_t *parent_scm = NULL;
  sme_channel_map_t *dep_scm = NULL;
  profile_t *p = NULL;
  conn_t conn_id;
  conn_t tmp_conn_id;
  memset(&conn_id, 0, sizeof(conn_t));

  ret = _parse_conn_from_profile_buf(buf, buf_len, &conn_id, &off);
  if (ret < 0)
    return ret;

  memset(&tmp_conn_id, 0, sizeof(conn_t));
  tmp_conn_id.in_ip = conn_id.in_ip;
  tmp_conn_id.in_port = conn_id.in_port;
  tmp_conn_id.out_ip = conn_id.in_ip;
  tmp_conn_id.out_port = conn_id.in_port;
  parent_scm = lookup_sme_channel_map(scm_list, &tmp_conn_id, COMPARE_CLIENT_SCM);
  if (!parent_scm)
    return -EINVAL;

  scm = parent_scm;

  /*
   * msg for cross-tier interface
   */
  ret = 1;
  if (conn_id.in_ip == conn_id.out_ip && conn_id.in_port != conn_id.out_port) {
    ret = 0;
    list_for_each_entry(dep_scm, &parent_scm->dep_scm_list, dep_scm_listp) {
      ret = compare_conn_generic(&dep_scm->conn_id, &conn_id,
          COMPARE_CROSSTIER_SCM);
      if (ret == 1) {
        scm = dep_scm;
        break;
      }
    }
  }

  if (ret <= 0)
    return -EINVAL;

  iprint(LVL_DBG, "SCM: %p\n[%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
      , scm, (void *) &conn_id.src_ip, conn_id.src_port
      , (void *) &conn_id.in_ip, conn_id.in_port
      , (void *) &conn_id.out_ip, conn_id.out_port
      , (void *) &conn_id.dst_ip, conn_id.dst_port);

  n_prof = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  prof_len_off = off;
  for (i = 0; i < n_prof; i++) {
    prof_off = ((uint64_t *) (buf + prof_len_off))[i];
    prof_buf = buf + prof_off;
    p = (profile_t *) kzalloc(sizeof(profile_t), GFP_KERNEL);
    if (!p) {
      ret = -ENOMEM;
      goto error;
    }

    ret = parse_profile_from_buf_ihash(p, prof_buf, buf_len);
    htable_insert(&scm->pmap_htbl, (void *) &p->id_ihash, sizeof(ihash_t),
        &p->profile_listp);

    if (ret < 0)
      break;
  }

  iprint(LVL_DBG, "ready to set pmap: %d", ret);
  if (ret < 0)
    goto error;

  return 0;

error:
  free_profile_in_pmap_list(&scm->pmap_list);
  return ret;
}


int
parse_profile_marker_buf(list_t *scm_list, char *buf, int buf_len)
{
  int ret = 0;
  int off = 0;
  uint32_t num_pub, num_priv;
  ihash_t *pubhash = NULL, *privhash = NULL;
  int hash_len = sizeof(ihash_t);
  sme_channel_map_t *scm = NULL;
  sme_channel_map_t *parent_scm = NULL;
  sme_channel_map_t *dep_scm = NULL;
  profile_conn_map_t *pcm = NULL;
  profile_conn_map_t *pcm2 = NULL;
  profile_t *p = NULL;

  char *marker_data_ptr = NULL;
#if CONFIG_PROF_LOG
  char *timestamp_ptr = NULL;
#endif

  conn_t conn_id;
  conn_t tmp_conn_id;
  scm_pcm_ptr_t *pcm_ptr = NULL;

  ret = _parse_conn_from_profile_buf(buf, buf_len, &conn_id, &off);
  if (ret < 0)
    return ret;

  memset(&tmp_conn_id, 0, sizeof(conn_t));
  tmp_conn_id.in_ip = conn_id.in_ip;
  tmp_conn_id.in_port = conn_id.in_port;
  tmp_conn_id.out_ip = conn_id.in_ip;
  tmp_conn_id.out_port = conn_id.in_port;
  parent_scm = lookup_sme_channel_map(scm_list, &tmp_conn_id, COMPARE_CLIENT_SCM);
  if (!parent_scm)
    return -EINVAL;

  scm = parent_scm;

  /*
   * msg for cross-tier interface
   */
  ret = 1;
  if (conn_id.in_ip == conn_id.out_ip && conn_id.in_port != conn_id.out_port) {
    ret = 0;
    list_for_each_entry(dep_scm, &parent_scm->dep_scm_list, dep_scm_listp) {
      ret = compare_conn_generic(&dep_scm->conn_id, &conn_id,
          COMPARE_CROSSTIER_SCM);
      if (ret == 1) {
        scm = dep_scm;
        break;
      }
    }
  }

  if (ret <= 0)
    return -EINVAL;

  ret = 0;
  iprint(LVL_DBG, "SCM: %p dep %d ret %d\n [%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]"
      , scm, ((scm == parent_scm) ? 0 : 1), ret
      , (void *) &conn_id.src_ip, conn_id.src_port
      , (void *) &conn_id.in_ip, conn_id.in_port
      , (void *) &conn_id.out_ip, conn_id.out_port
      , (void *) &conn_id.dst_ip, conn_id.dst_port);

  if (!scm)
    return -EINVAL;

  marker_data_ptr = buf + off;
  num_pub = ((uint32_t *) (buf + off))[0];
  off += sizeof(uint32_t);
  num_priv = ((uint32_t *) (buf + off))[0];
  off += sizeof(uint32_t);

  // skip the req ID and timestamp fields
  off += hash_len;
  off += sizeof(uint64_t);

  pubhash = (ihash_t *) (buf + off);
  off += sizeof(ihash_t)*num_pub;
  privhash = (ihash_t *) (buf + off);
  off += sizeof(ihash_t)*num_priv;

#if 0
  p = lookup_profile_list_by_inp_hash(&scm->pmap_list, num_pub, num_priv,
      pubhash, privhash);
#endif
  p = htable_lookup(&scm->pmap_htbl, (void *) &pubhash[0], sizeof(ihash_t));

  memset(&tmp_conn_id, 0, sizeof(conn_t));
  tmp_conn_id.src_ip = conn_id.out_ip;
  tmp_conn_id.src_port = conn_id.out_port;
  tmp_conn_id.dst_ip = conn_id.dst_ip;
  tmp_conn_id.dst_port = conn_id.dst_port;
  pcm = lookup_profile_conn_map_by_sec_conn(scm, &tmp_conn_id, 0, 0, NULL);
  if (!pcm)
    return -EINVAL;

  inherit_key_fields(&conn_id, &pcm->sec_conn_id);

  pcm->p = p;
  if (!pcm->p)
    pcm->p = &default_profile;

#if CONFIG_PROF_LOG
  pcm->marker_data_len = (2*sizeof(uint32_t)) /* #pub + #priv */
    + sizeof(uint64_t) /* timestamp field */
    + ((1+num_pub+num_priv)*hash_len); /* reqID, #pub, #priv hash fields */

  // free previous version of the marker data before allocating new one
  if (pcm->marker_data)
    kfree(pcm->marker_data);

  pcm->marker_data = kzalloc(pcm->marker_data_len, GFP_KERNEL);
  memcpy(pcm->marker_data, marker_data_ptr, pcm->marker_data_len);
  /*
   * replace timestamp field in marker data with kernel timestamp
   * we do this in order to profile the delay until marker processing in kernel.
   */
  timestamp_ptr = pcm->marker_data + 2*sizeof(uint32_t) + sizeof(ihash_t);
  *(uint64_t *) timestamp_ptr = get_current_time(SCALE_NS);
#endif

  iprint(LVL_DBG, "#pub: %u, #priv: %u, profile %p, default %p marker data len: %d, %d"
      , num_pub, num_priv, pcm->p, &default_profile, pcm->marker_data_len
      , (int) (((char *) buf + buf_len) - (char *) marker_data_ptr));

  /*
   * on FE, link FE-Client and FE-BE connections
   * using the FE-BE marker sent by app.
   */
  if (conn_id.in_port == conn_id.out_port
      && conn_id.src_ip == conn_id.dst_ip
      && conn_id.src_port == conn_id.dst_port)
    return ret;

  memset(&tmp_conn_id, 0, sizeof(conn_t));
  tmp_conn_id.src_ip = conn_id.in_ip;
  tmp_conn_id.src_port = conn_id.in_port;
  tmp_conn_id.dst_ip = conn_id.src_ip;
  tmp_conn_id.dst_port = conn_id.src_port;
  pcm2 = lookup_profile_conn_map_by_sec_conn(parent_scm, &tmp_conn_id, 0, 0, NULL);
  if (!pcm2)
    return -EINVAL;

  pcm_ptr = (scm_pcm_ptr_t *) kzalloc(sizeof(scm_pcm_ptr_t), GFP_KERNEL);
  INIT_LIST_HEAD(&pcm_ptr->listp);
  pcm_ptr->ptr = pcm2;
  list_add_tail(&pcm_ptr->listp, &pcm->pcm_ptr_list);

  pcm_ptr = (scm_pcm_ptr_t *) kzalloc(sizeof(scm_pcm_ptr_t), GFP_KERNEL);
  INIT_LIST_HEAD(&pcm_ptr->listp);
  pcm_ptr->ptr = pcm;
  list_add_tail(&pcm_ptr->listp, &pcm2->pcm_ptr_list);

  return ret;
}

int
parse_ssl_handshake_buf(list_t *scm_list, char *buf, int buf_len)
{
  int ret = 0;
  int off = 0;
  sme_channel_map_t *scm = NULL;
  profile_conn_map_t *pcm = NULL;
  uint32_t src_ip = 0, dst_ip = 0;
  uint16_t src_port = 0, dst_port = 0;

  dst_ip = ((uint32_t *) buf)[0];
  src_ip = ((uint32_t *) buf)[1];

  off = 2 * sizeof(uint32_t);
  dst_port = ((uint16_t *) (buf + off))[0];
  src_port = ((uint16_t *) (buf + off))[1];

  ret = get_profile_conn_info_from_skb_header(src_ip, dst_ip, src_port, dst_port,
      scm_list, &scm, &pcm, 0, 0, 0, NULL);
  if (ret < 0 || !scm || !pcm)
    return -EINVAL;

  pcm->stage = PCM_STAGE_SSL_END;
  memcpy(&pcm->default_pid, &DEFAULT_MW_PROF_IHASH, sizeof(ihash_t));
  pcm->is_req_prof_set = 0;
  iprint(LVL_INFO, "pcm [%pI4 %u, %pI4 %u] state %d"
      , (void *) &pcm->sec_conn_id.out_ip, pcm->sec_conn_id.out_port
      , (void *) &pcm->sec_conn_id.dst_ip, pcm->sec_conn_id.dst_port
      , pcm->stage);

  return 0;
}


int
parse_profile_conn_buf(list_t *scm_list, char *buf, int buf_len)
{
  int ret = 0;
  uint64_t msg_type;
  uint64_t msg_len;
  int s_buf_len = 0;
  char s_buf[MAX_DBG_BUF_LEN];
  memset(s_buf, 0, MAX_DBG_BUF_LEN);

  if (buf_len < MSG_HDR_LEN)
    return -EINVAL;

  get_msg_cmd(buf, msg_type);
  get_msg_args_len(buf, msg_len);
  iprint(LVL_DBG, "msg type: %lld, len: %lld, buf len: %d"
      , msg_type, msg_len, buf_len);

  if (msg_len < 0 || msg_len < (buf_len - MSG_HDR_LEN))
    return -EINVAL;

  switch (msg_type) {
    case MSG_T_PMAP_IHASH:
#if SME_DEBUG_LVL <= LVL_INFO
      print_profile_list_from_buf_ihash(s_buf, &s_buf_len,
          buf+MSG_HDR_LEN, buf_len-MSG_HDR_LEN);
#endif
      ret = parse_profile_list_from_buf_ihash(scm_list,
          buf+MSG_HDR_LEN, buf_len-MSG_HDR_LEN);
      if (s_buf_len > 0)
        iprint(LVL_INFO, "[%lld]: ret %d len %lld\n%s"
            , msg_type, ret, msg_len, s_buf);
      break;
    case MSG_T_MARKER:
#if SME_DEBUG_LVL <= LVL_INFO
      print_profile_marker_buf(s_buf, &s_buf_len,
          buf+MSG_HDR_LEN, buf_len-MSG_HDR_LEN);
#endif
      ret = parse_profile_marker_buf(scm_list,
          buf+MSG_HDR_LEN, buf_len-MSG_HDR_LEN);
      if (s_buf_len > 0)
        iprint(LVL_INFO, "[%lld]: ret %d len %d\n%s"
            , msg_type, ret, s_buf_len, s_buf);
      break;
    case MSG_T_SSL_HANDSHAKE:
      ret = parse_ssl_handshake_buf(scm_list, buf+MSG_HDR_LEN,
          buf_len-MSG_HDR_LEN);
      break;
    default:
      ret = -EBADF;
  }

  return ret;
}

int
parse_conn_from_profile_buf(char *buf, int buf_len, conn_t *conn_id)
{
  int ret = 0;
  int off = 0;
  uint64_t msg_type;
  uint64_t msg_len;

  if (buf_len < MSG_HDR_LEN)
    return -EINVAL;

  get_msg_cmd(buf, msg_type);
  get_msg_args_len(buf, msg_len);
  iprint(LVL_DBG, "GET CONN cmd %lld, len %lld", msg_type, msg_len);
  if (msg_len < 0 || msg_len < (buf_len - MSG_HDR_LEN))
    return -EINVAL;

  switch (msg_type) {
    case MSG_T_PMAP:
    case MSG_T_PMAP_IHASH:
    case MSG_T_PROF_ID:
    case MSG_T_MARKER:
      ret = _parse_conn_from_profile_buf(buf+MSG_HDR_LEN, buf_len-MSG_HDR_LEN,
          conn_id, &off);
      break;
    default:
      ret = -EBADF;
  }

  return ret;
}

void
print_profile_from_buf_ihash(char *s_buf, int *s_buf_len, char *buf, int buf_len)
{
  int i;
  uint16_t off = 0;
  ihash_t prof_id_ihash;
  uint16_t num_out_reqs;
  uint16_t num_extra_slots = 0;
  uint64_t num_out_frames = 0;
  uint64_t spacing = 0;
  uint64_t latency = 0;
  int s_buf_off = 0;
  char hbuf[64];
  int hbuf_len = 64;

  if (!s_buf || !s_buf_len || !buf || !buf_len)
    return;

  prof_id_ihash = ((ihash_t *) (buf + off))[0];
  off += sizeof(ihash_t);

  num_out_reqs = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  memset(hbuf, 0, hbuf_len);
  prt_hash(prof_id_ihash.byte, sizeof(ihash_t), hbuf );

  sprintf(s_buf + s_buf_off, "prof ID (Hex): %s, # out reqs: %d\n",
      hbuf, num_out_reqs);
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "extra slots --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    num_extra_slots = ((uint16_t *) (buf + off))[0];
    off += sizeof(uint16_t);
    sprintf(s_buf + s_buf_off, "%d ", num_extra_slots);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "out frames --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    num_out_frames = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%llu ", num_out_frames);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "spacing --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    spacing = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%llu ", spacing);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "latency --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    latency = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%llu ", latency);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  *s_buf_len = s_buf_off;
}


void
print_profile_from_buf(char *s_buf, int *s_buf_len, char *buf, int buf_len)
{
#if 0
  int i;
  uint16_t off = 0;
  uint16_t prof_id;
  uint16_t num_out_reqs;
  uint16_t num_extra_slots = 0;
  uint64_t num_out_frames = 0;
  uint64_t spacing = 0;
  uint64_t latency = 0;
  int s_buf_off = 0;

  if (!s_buf || !s_buf_len || !buf || !buf_len)
    return;

  prof_id = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  num_out_reqs = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  sprintf(s_buf + s_buf_off, "prof ID: %d, # out reqs: %d\n",
      prof_id, num_out_reqs);
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "extra slots --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    num_extra_slots = ((uint16_t *) (buf + off))[0];
    off += sizeof(uint16_t);
    sprintf(s_buf + s_buf_off, "%d ", num_extra_slots);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "out frames --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    num_out_frames = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%d ", num_out_frames);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "spacing --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    spacing = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%llu ", spacing);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "latency --- ");
  s_buf_off = strlen(s_buf);
  for (i = 0; i < num_out_reqs; i++) {
    latency = ((uint64_t *) (buf + off))[0];
    off += sizeof(uint64_t);
    sprintf(s_buf + s_buf_off, "%llu ", latency);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  *s_buf_len = s_buf_off;
#endif
}

void
print_profile_list_from_buf_ihash(char *s_buf, int *s_buf_len,
    char *buf, int buf_len)
{
#if 0
  int i;
  int off = 0;
  int prof_len_off = 0;
  int s_buf_off = 0, s_buf_off2 = 0;
  uint16_t n_prof, prof_off;
  char *prof_buf = NULL;
  conn_t conn_id;
  memset(&conn_id, 0, sizeof(conn_t));

  if (!s_buf || !s_buf_len || !buf || buf_len <= 0)
    return;

  _parse_conn_from_profile_buf(buf, buf_len, &conn_id, &off);

  n_prof = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  sprintf(s_buf + s_buf_off,
      "[%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u], # profiles: %d\n",
      (void *) &conn_id.src_ip, conn_id.src_port,
      (void *) &conn_id.in_ip, conn_id.in_port,
      (void *) &conn_id.out_ip, conn_id.out_port,
      (void *) &conn_id.dst_ip, conn_id.dst_port, n_prof);
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "profile offsets --- ");
  s_buf_off = strlen(s_buf);
  prof_len_off = off;
  for (i = 0; i < n_prof; i++) {
    prof_off = ((uint16_t *) (buf + prof_len_off))[i];
    sprintf(s_buf + s_buf_off, "%d ", prof_off);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "PROFILES ----\n");
  s_buf_off = strlen(s_buf);
  prof_len_off = off;
  for (i = 0; i < n_prof; i++) {
    prof_off = ((uint16_t *) (buf + prof_len_off))[i];
    prof_buf = buf + prof_off;
    print_profile_from_buf_ihash(s_buf + s_buf_off, &s_buf_off2, prof_buf, buf_len);
    s_buf_off += s_buf_off2;
  }

  *s_buf_len = s_buf_off;
#endif
}



void
print_profile_list_from_buf(char *s_buf, int *s_buf_len, char *buf, int buf_len)
{
#if 0
  int i;
  int off = 0;
  int prof_len_off = 0;
  int s_buf_off = 0, s_buf_off2 = 0;
  uint16_t n_prof, prof_off;
  char *prof_buf = NULL;
  conn_t conn_id;
  memset(&conn_id, 0, sizeof(conn_t));

  if (!s_buf || !s_buf_len || !buf || buf_len <= 0)
    return;

  _parse_conn_from_profile_buf(buf, buf_len, &conn_id, &off);

  n_prof = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  sprintf(s_buf + s_buf_off,
      "[%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u], # profiles: %d\n",
      (void *) &conn_id.src_ip, conn_id.src_port,
      (void *) &conn_id.in_ip, conn_id.in_port,
      (void *) &conn_id.out_ip, conn_id.out_port,
      (void *) &conn_id.dst_ip, conn_id.dst_port, n_prof);
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "profile offsets --- ");
  s_buf_off = strlen(s_buf);
  prof_len_off = off;
  for (i = 0; i < n_prof; i++) {
    prof_off = ((uint16_t *) (buf + prof_len_off))[i];
    sprintf(s_buf + s_buf_off, "%d ", prof_off);
    s_buf_off = strlen(s_buf);
  }
  sprintf(s_buf + s_buf_off, "\n");
  s_buf_off = strlen(s_buf);

  sprintf(s_buf + s_buf_off, "PROFILES ----\n");
  s_buf_off = strlen(s_buf);
  prof_len_off = off;
  for (i = 0; i < n_prof; i++) {
    prof_off = ((uint16_t *) (buf + prof_len_off))[i];
    prof_buf = buf + prof_off;
    print_profile_from_buf(s_buf + s_buf_off, &s_buf_off2, prof_buf, buf_len);
    s_buf_off += s_buf_off2;
  }
//  sprintf(s_buf + s_buf_off, "\n");
//  s_buf_off = strlen(s_buf);
  *s_buf_len = s_buf_off;
#endif
}

void
print_profile_id_conn_from_buf(char *s_buf, int *s_buf_len, char *buf, int buf_len)
{
#if 0
  int off = 0;
  int s_buf_off = 0;
  uint16_t prof_id = 0;
  conn_t conn_id;
  memset(&conn_id, 0, sizeof(conn_t));

  if (!s_buf || !s_buf_len || !buf || !buf_len)
    return;

  conn_id.src_ip = ((uint32_t *) (buf + off))[0];
  conn_id.in_ip = ((uint32_t *) (buf + off))[1];
  conn_id.out_ip = ((uint32_t *) (buf + off))[2];
  conn_id.dst_ip = ((uint32_t *) (buf + off))[3];
  off += PROF_CONN_IP_HDR_LEN;

  conn_id.src_port = ntohs(((uint16_t *) (buf + off))[0]);
  conn_id.in_port = ntohs(((uint16_t *) (buf + off))[1]);
  conn_id.out_port = ntohs(((uint16_t *) (buf + off))[2]);
  conn_id.dst_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += PROF_CONN_PORT_HDR_LEN;

  prof_id = ((uint16_t *) (buf + off))[0];
  off += sizeof(uint16_t);

  sprintf(s_buf + s_buf_off,
      "[%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u], PROF ID: %d"
      , (void *) &conn_id.src_ip, conn_id.src_port
      , (void *) &conn_id.in_ip, conn_id.in_port
      , (void *) &conn_id.out_ip, conn_id.out_port
      , (void *) &conn_id.dst_ip, conn_id.dst_port
      , prof_id);
  s_buf_off = strlen(s_buf);
  *s_buf_len = s_buf_off;
#endif
}

void
print_profile_marker_buf(char *s_buf, int *s_buf_len, char *buf, int buf_len)
{
#if 0
  int off = 0;
  int s_buf_off = 0;
  uint32_t num_pub, num_priv;
  ihash_t reqid;
  char rhash[64];
  uint64_t marker_ts = 0;
  char *marker_data_ptr = NULL;
  ihash_t *pubhash = NULL, *privhash = NULL;
  int hash_len = sizeof(ihash_t);
  conn_t conn_id;

  memset(rhash, 0, 64);
  memset(&conn_id, 0, sizeof(conn_t));

  if (!s_buf || !s_buf_len || !buf || !buf_len)
    return;

  conn_id.src_ip = ((uint32_t *) (buf + off))[0];
  conn_id.in_ip = ((uint32_t *) (buf + off))[1];
  conn_id.out_ip = ((uint32_t *) (buf + off))[2];
  conn_id.dst_ip = ((uint32_t *) (buf + off))[3];
  off += PROF_CONN_IP_HDR_LEN;

  conn_id.src_port = ntohs(((uint16_t *) (buf + off))[0]);
  conn_id.in_port = ntohs(((uint16_t *) (buf + off))[1]);
  conn_id.out_port = ntohs(((uint16_t *) (buf + off))[2]);
  conn_id.dst_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += PROF_CONN_PORT_HDR_LEN;

  marker_data_ptr = buf + off;
  num_pub = ((uint32_t *) (buf + off))[0];
  off += sizeof(uint32_t);
  num_priv = ((uint32_t *) (buf + off))[0];
  off += sizeof(uint32_t);

  memset(&reqid, 0, sizeof(ihash_t));
  memcpy(reqid.byte, buf+off, hash_len);
  off += hash_len;

  marker_ts = ((uint64_t *) (buf + off))[0];
  off += sizeof(uint64_t);

  pubhash = (ihash_t *) (buf + off);
  prt_hash(pubhash[0].byte, sizeof(ihash_t), rhash);
  privhash = NULL;
//  prt_hash(reqid.byte, sizeof(ihash_t), rhash);

  sprintf(s_buf + s_buf_off,
      "[%pI4 %u, %pI4 %u, %pI4 %u, %pI4 %u]\nmarker data len: %d, "
      "TS: %llu, #pub: %u, #priv: %u pub0 %s"
      , (void *) &conn_id.src_ip, conn_id.src_port
      , (void *) &conn_id.in_ip, conn_id.in_port
      , (void *) &conn_id.out_ip, conn_id.out_port
      , (void *) &conn_id.dst_ip, conn_id.dst_port
      , (int) (((char *) buf + buf_len) - (char *) marker_data_ptr)
      , marker_ts, num_pub, num_priv, rhash);
  s_buf_off = strlen(s_buf);

  *s_buf_len = s_buf_off;

#if 0
  int i;
  if (num_pub > 0)
    pubhash = (ihash_t *) kzalloc(sizeof(ihash_t)*num_pub, GFP_KERNEL);
  if (num_priv > 0)
    privhash = (ihash_t *) kzalloc(sizeof(ihash_t)*num_priv, GFP_KERNEL);

  for (i = 0; i < num_pub; i++) {
    memcpy(pubhash[i].byte, buf+off, hash_len);
    off += hash_len;
  }

  for (i = 0; i < num_priv; i++) {
    memcpy(privhash[i].byte, buf+off, hash_len);
    off += hash_len;
  }

  if (pubhash)
    kfree(pubhash);
  if (privhash)
    kfree(privhash);
#endif
#endif
}

void
print_profile_conn_buf(char *buf, int buf_len)
{
#if 0
  uint64_t msg_type;
  uint64_t msg_len;
  int s_buf_len = 0;
  char s_buf[MAX_DBG_BUF_LEN];
  conn_t conn_id;
  int off = 0;
  memset(s_buf, 0, MAX_DBG_BUF_LEN);
  memset(&conn_id, 0, sizeof(conn_t));

  if (buf_len < MSG_HDR_LEN)
    return;

  get_msg_cmd(buf, msg_type);
  get_msg_args_len(buf, msg_len);
  iprint(LVL_DBG, "msg type: %lld, len: %lld, buf len: %d", msg_type, msg_len, buf_len);
  if (msg_len < 0 || msg_len < (buf_len - MSG_HDR_LEN))
    return;

  _parse_conn_from_profile_buf(buf+MSG_HDR_LEN, buf_len-MSG_HDR_LEN, &conn_id, &off);
  switch (msg_type) {
    case MSG_T_PMAP_IHASH:
      print_profile_list_from_buf_ihash(s_buf, &s_buf_len,
          buf+MSG_HDR_LEN, buf_len-MSG_HDR_LEN);
      if (s_buf_len > 0)
        iprint(LVL_DBG, "[%d]: %s", s_buf_len, s_buf);
      break;
    case MSG_T_MARKER:
      print_profile_marker_buf(s_buf, &s_buf_len,
          buf+MSG_HDR_LEN, buf_len-MSG_HDR_LEN);
      if (s_buf_len > 0)
        iprint(LVL_DBG, "[%d]: %s", s_buf_len, s_buf);
      break;
    default:
      iprint(LVL_DBG, "INVALID CTRL MSG type %lld len %lld", msg_type, msg_len);
  }

  return;
#endif
}
