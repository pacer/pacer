/*
 * spinlock.h
 *
 * created on: May 08, 2018
 * author: aasthakm
 */

#ifndef __SPINLOCK_H__
#define __SPINLOCK_H__

#include <linux/types.h>

typedef struct tkt_lock {
  atomic_t next_tkt;
  atomic_t now_serving;
} tkt_lock_t;

int init_tkt_lock(tkt_lock_t *lock);
void acquire_tkt_lock(tkt_lock_t *lock);
void release_tkt_lock(tkt_lock_t *lock);
void acquire_tkt_lock_bh(tkt_lock_t *lock);
void release_tkt_lock_bh(tkt_lock_t *lock);

#endif /* __SPINLOCK_H__ */
