/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.12
 *
 * This file is not intended to be easily readable and contains a number of
 * coding conventions designed to improve portability and efficiency. Do not make
 * changes to this file unless you know what you are doing--modify the SWIG
 * interface file instead.
 * ----------------------------------------------------------------------------- */

#ifndef PHP_PHPIOCTL_H
#define PHP_PHPIOCTL_H

extern zend_module_entry phpioctl_module_entry;
#define phpext_phpioctl_ptr &phpioctl_module_entry

#ifdef PHP_WIN32
# define PHP_PHPIOCTL_API __declspec(dllexport)
#else
# define PHP_PHPIOCTL_API
#endif

ZEND_NAMED_FUNCTION(_wrap_dev_open);
ZEND_NAMED_FUNCTION(_wrap_dev_close);
ZEND_NAMED_FUNCTION(_wrap_dev_ioctl);
PHP_MINIT_FUNCTION(phpioctl);

#endif /* PHP_PHPIOCTL_H */
