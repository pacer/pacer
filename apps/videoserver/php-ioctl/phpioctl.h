/*
 * phpioctl.h
 *
 * created on: Mar 23, 2018
 * author: aasthakm
 *
 * wrapper around device calls
 */

#ifndef __PHPIOCTL_H__
#define __PHPIOCTL_H__

int dev_open(char *devname);
int dev_close(int fd);
int dev_ioctl(int fd, int cmd, char *buf, int len);

#endif /* __PHPIOCTL_H__ */
