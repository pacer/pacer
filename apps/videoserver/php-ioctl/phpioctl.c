/*
 * phioctl.c
 *
 * created on: Mar 23, 2018
 * author: aasthakm
 *
 * wrapper implementation to be used in swig generated file
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define SME_IOCTL _IO('S', 0)

int
dev_open(char *devname)
{
  int fd = open(devname, O_RDWR);
  return fd;
}

int
dev_close(int fd)
{
  close(fd);
}

int
dev_ioctl(int fd, int cmd, char *buf, int len)
{
  int ret = ioctl(fd, SME_IOCTL, buf);
  return ret;
}
