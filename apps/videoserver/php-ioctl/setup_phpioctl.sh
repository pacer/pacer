#!/bin/bash

## script to compile and install libphpioctl.so extension for apache
## dependencies: php-dev


## Compile ##
make

extdir=`php-config --extension-dir`
extname="libphpioctl.so"
inifile="/etc/php/7.0/apache2/php.ini"

## Copy .so to extensions directory ##
echo "cp $extname $extdir/"
cp $extname $extdir/

## Setup php.ini ##
## We only need to add the extension to php.ini once
## Avoid appending multiple times by checking for the entry
ext=`cat $inifile | grep "extension=$extdir/$extname"`
if [[ "x$ext" == "x" ]]; then
  echo "echo \"extension=$extdir/$extname\" >> $inifile"
  echo "extension=$extdir/$extname" >> $inifile
fi

#sed -i "/; Dynamic Extensions.*\n.*\n.*\n/a extension=$extdir/$extname" $inifile
