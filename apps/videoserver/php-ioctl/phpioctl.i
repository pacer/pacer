%module phpioctl

extern int dev_open(char *devname);
extern int dev_close(int fd);
extern int dev_ioctl(int fd, int cmd, char *buf, int len);
