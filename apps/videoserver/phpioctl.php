<?php

/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.12
 *
 * This file is not intended to be easily readable and contains a number of
 * coding conventions designed to improve portability and efficiency. Do not make
 * changes to this file unless you know what you are doing--modify the SWIG
 * interface file instead.
 * ----------------------------------------------------------------------------- */

/*
 * Date: 31 July 2019
 * Author: aasthakm
 *
 * Disabled this snippet, once we moved to php-fpm config with apache,
 * since dl() is disabled in php-fpm from v7.0.0.
 * https://www.php.net/manual/en/function.dl.php
 *
 * Instead of loading the library from here, I just specified libphpioctl.so
 * as an extension to be loaded always with php-fpm server.
 * see /etc/php/7.0/fpm/php.ini
 */

/*
// Try to load our extension if it's not already loaded.
if (!extension_loaded('phpioctl')) {
  if (strtolower(substr(PHP_OS, 0, 3)) === 'win') {
    if (!dl('php_phpioctl.dll')) return;
  } else {
    // PHP_SHLIB_SUFFIX gives 'dylib' on MacOS X but modules are 'so'.
    if (PHP_SHLIB_SUFFIX === 'dylib') {
      if (!dl('phpioctl.so')) return;
    } else {
      if (!dl('phpioctl.'.PHP_SHLIB_SUFFIX)) return;
    }
  }
}
 */



abstract class phpioctl {
	static function dev_open($devname) {
		return dev_open($devname);
	}

	static function dev_close($fd) {
		return dev_close($fd);
	}

	static function dev_ioctl($fd,$cmd,$buf,$len) {
		return dev_ioctl($fd,$cmd,$buf,$len);
	}
}

/* PHP Proxy Classes */

?>
