#!/usr/bin/python

# Contains information about a single client request and its related response

class Observation:
    # URL of the requested file ("f" URL encoded parameter in PHP script)
    url = ""
    # ID of the segment requested (from 0 to 3005, -1 if MPD file)
    segid = 0
    # HTTP response status code
    status = 0
    # Size of the segment requested (bytes)
    rlen = 0
    # Timestamp immediately before sending the request (ms)
    reqtime = 0.0
    # Round-trip time (request_time - response_time) (ms)
    restime = 0.0
    # Number of retry attempts
    retry_cnt = 0

    def __init__(self, url="", segid=0, status=0, size=0, reqtime=0.0, rtt=0.0,
            retry_cnt=0):
        self.url = url
        self.segid = segid
        self.status = status
        self.size = size
        self.reqtime = reqtime
        self.rtt = rtt
        self.retry_cnt = retry_cnt

    ## Getters

    def get_url(self):
        return self.url

    def get_segid(self):
        return self.segid

    def get_status(self):
        return self.status

    def get_size(self):
        return self.size

    def get_reqtime(self):
        return self.reqtime

    def get_rtt(self):
        return self.rtt

    def get_retry_cnt(self):
        return self.retry_cnt

    ## Setters

    def set_url(self, url):
        self.url = url

    def set_segid(self, segid):
        self.segid = segid

    def set_status(self, status):
        self.status = status

    def set_size(self, size):
        self.size = size

    def set_reqtime(self, reqtime):
        self.reqtime = reqtime

    def set_rtt(self, rtt):
        self.rtt = rtt

    def set_retry_cnt(self, retry_cnt):
        self.retry_cnt = retry_cnt

    # Debug: print attributes
    def print_observation(self):
        print("\n[Observation]\nURL: "+self.url+ \
        "\nsegid: "+str(self.segid)+ \
        "\nstatus: "+str(self.status)+ \
        "\nsize: "+str(self.size)+ \
        " rtt: "+str(self.rtt)+   \
        "\nreqtime: "+str(self.reqtime)+ \
        "\nretry: "+str(self.retry_cnt))

    def store_observation(self, filename):
        with open(filename, "a") as f:
            f.write("\n[Observation]\nURL: "+self.url+ \
                    "\nsegid: "+str(self.segid)+ \
                    "\nstatus: "+str(self.status)+ \
                    "\nsize: "+str(self.size)+ \
                    " rtt: "+str(self.rtt)+    \
                    "\nreqtime: "+str(self.reqtime)+ \
                    "\nretry: "+str(self.retry_cnt))

