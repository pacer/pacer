# Requirements: BeautifulSoup (pip install bs4), IsoDate (pip install isodate)
# Note: written to parse MPDs created with Bento4 mp4-dash.py (VERSION=1.8.0-620) with the following options enabled: --use-segment-list --use-segment-timeline
# Get the URL of every segment of a given video resolution

import csv
import os
import sys
import isodate

from bs4 import BeautifulSoup

def parseMPD(soup, videoQuality):

    # Initialize URL list
    segmentURLs = []

    # Select video resolution
    representation = soup.findAll("Representation", {"height" : str(videoQuality)})

    # Navigate the segment list
    for segmentList in representation:
        # Get URL of initialization segment
        for initSegment in segmentList.select("Initialization"):
            segmentURLs.append(initSegment["sourceURL"])
            # Get URL of segments
            for segment in segmentList.findAll("SegmentURL"):
                segmentURLs.append(segment["media"])

    return segmentURLs

def getMinBufferTime(soup):
    minBufferTime = isodate.parse_duration(soup.MPD['minBufferTime']).total_seconds()
    return minBufferTime

def readFile(xmlfile):
    # Parse XML file
    _this_dir = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(_this_dir, xmlfile)) as fhandle:
        soup = BeautifulSoup(fhandle.read(), 'xml')
    return soup

def readXML(xml):
    soup = BeautifulSoup(xml, 'xml')
    return soup

if __name__ == '__main__':
    soup = readFile(sys.argv[1])
    getMinBufferTime(soup)
    #segmentURLs = parseMPD(soup, sys.argv[2])
    # print(segmentURLs)
