#!/usr/bin/python
import os
import sys
import argparse

# If used as remote client
sys.path.append("/usr/local/lib/python2.7/dist-packages/")

# Handle HTTP requests
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
# Retrying requests
from requests.adapters import HTTPAdapter

# MPD parsing
from bs4 import BeautifulSoup
from mpd_parse import *

# Multiprocessing and sync
import multiprocessing
from multiprocessing import Event

# Statistics
from observations_streaming import Observation
from stats import *
import time
from uptime import uptime
import datetime
from functools import partial
import collections
import random
import numpy

import inspect

# Map video tag with cluster
from collections import defaultdict

# Disable warning about unverified HTTPS connection
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Output for failed requests
not_found_requests = "/local/sme/logs/not_found_requests.txt"

# Segment duration (s)
# Typically, one would set play_sleep and segment_duration to same value.
# To speed up experiment, ideally change both values.
# However, you can set the values differently just for fun ;-)
segment_duration = 5
# Cluster ID
clusterid = "-1"

# Flag for failed requests
failed_request = False

# Config variables
#sys.stdout = os.fdopen(sys.stdout.fileno(), 'wb', 0)

#inter_request_gap = 500.0
#inter_request_gap = 1000.0
#inter_request_gap = 2000.0
#timeout_delay_ms = 2500.0
#timeout_delay_ms = 120000.0

## used for latest attack experiment and video xput experiment
#inter_request_gap = 0.0
inter_request_gap = 1000.0
timeout_delay_ms = 5000.0
timeout_delay_s = (timeout_delay_ms)/1000.0

RETRY_ITERS = 2

def lineno():
    return inspect.currentframe().f_back.f_lineno

# Issue GET request with the given URL
def send_request(index, url, session):
    global timeout_delay_ms, timeout_delay_s
    response = None
    retry = RETRY_ITERS
    retry_iter = 0
    # If this is the first request, create session
    if not session:
        print("Creating session.")
        session = requests.session()

    while retry > 0:
        try:
            starttime = gettime()
            response = session.get(url, verify=False, timeout=timeout_delay_s)
            # If success, return
            break
        except Exception as e:
            print(("%6.6f [Buffer:%d] %s url %s" % (datetime.datetime.now().timestamp(), os.getpid(), e, url)))

        retry -= 1
        retry_iter += 1
        print(("%6.6f Remaining retry attempts %d" % (datetime.datetime.now().timestamp(), retry)))

        # Sleep time between requests (e.g., 500 ms)
        sleep_s = (starttime + inter_request_gap - gettime())/1000.0
        if sleep_s > 0:
            #print("Sleeping for %d s" % sleep_s)
            time.sleep(sleep_s)

    """
    TO DO. PHP file_get_contents returns WARNING when it fails to open a stream.
    The $buf sent on the socket will be null, but the response is 200 OK.
    Need to check for null value on response.content, instead of using a raise_for_status.
    Also, add warning handles in PHP file.
    """

    if response and "200" not in str(response):
        with open(not_found_requests, "a+") as f:
            print("Response error: " + str(index))
            f.write(str(lineno())+": "+str(index)+", "+str(url)+", rsp: "+str(response)+"\n")
        response.raise_for_status()

    return response, retry_iter

# Issue GET request and store latency information
def process_request(obs, index, url, session):
    global failed_request
    # Get URL
    url = url + "&i=" + str(clusterid) + "-" + str(index)
    print("CLNT URL: %s" % url)
    """
    # Debug mode - request always same segment
    if index >= 1:
        url="https://139.19.171.103/benchVideo.php?c=0&d=0&f=./videos/yt-0n2k6N6odKI-frag/video/avc1/1/seg-1.m4s"
    """
    # Send request and store request latency
    starttime = gettime()
    response, retry_iter = send_request(index, url, session)
    endtime = gettime()
    rtt = endtime - starttime
    if response:
        rsp = Observation(url, index, response.status_code, len(response.content),
                starttime, rtt, retry_iter)
    else:
        rsp = Observation(url, index, "No response", 0, starttime, rtt,
                retry_iter)
    obs.append(rsp)
    # If send_request fails despite retries
    if retry_iter == RETRY_ITERS:
        failed_request = True

    # Sleep time between requests (e.g., 500 ms)
    sleep_s = (starttime + inter_request_gap - gettime())/1000.0
    if sleep_s > 0:
        #print("Sleeping for %d s" % sleep_s)
        time.sleep(sleep_s)

    return response

# Request MPD file and store latency information
def get_mpd_file(obs, mpdurl, session):
    mpd = None
    response = process_request(obs, -1, mpdurl, session)
    if response != None:
        mpd = response.content
    return mpd

# Parse MPD file and get segment URL list for a given video quality
def get_segment_list(mpd, hqvideos):
    # Check for hq videos
    if hqvideos == 1:
        quality = 1080
    elif hqvideos == 2:
        quality = 720
    elif hqvideos == "20M":
        ## custom video segs resolution created for indirect attack test with 10gbps
        quality = "20M"
    else:
        quality = 240
    soup = readXML(mpd)
    segment_list = parseMPD(soup, quality)
    n_segments = len(segment_list)
    try:
        min_buffer_time = getMinBufferTime(soup)
    except:
        print(len(str(mpd)))
        print(mpd)
        min_buffer_time = None
    return segment_list, n_segments, min_buffer_time

# Get clusterid from mapping videotag,clusterid
def load_cluster_map_from_file(filename='videotag_clusterid_20clusters.csv'):
    _this_dir = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(_this_dir, filename)) as f:
        lines = f.readlines()
        cluster_map = defaultdict(list)
        for _, line in enumerate(lines):
            [video, cluster] = line.strip('\n').strip('\r').split(',')
            cluster_map[video] = cluster
        return cluster_map

def get_cluster_id(videotag, n_clusters, hqvideos):
    if n_clusters == 0:
        return 0
    if hqvideos == 1:
        filename = 'videotag_clusterid_'+str(n_clusters)+'clusters_hq.csv'
    else:
        filename = 'videotag_clusterid_'+str(n_clusters)+'clusters.csv'
    cluster_map = load_cluster_map_from_file(filename)
    return int(cluster_map[videotag])

# Request all segments as warm-up phase
def warm_up():
    global failed_request, requested_segments
    global observations, session

    stime = time.time()

    while (requested_segments < n_segments):
        response = process_request(observations, requested_segments,
            url["base"]+segment_list[requested_segments], session)
        if failed_request == True:
            print(("%6.6f [WarmUp:%d] Buffering failed!" % (datetime.datetime.now().timestamp(), os.getpid())))
            break
        else:
            print(("%6.6f [WarmUp:%d] Got %s%s" % (datetime.datetime.now().timestamp(), os.getpid(),
                url["base"], segment_list[requested_segments])))
            requested_segments += 1

    etime = time.time() - stime
    print(("%6.6f [WarmUp:%d] Requested %d/%d segments in %6.10f s"
            % (datetime.datetime.now().timestamp(), os.getpid(), requested_segments, n_segments, etime)))

    return etime

# Buffering logic
def buffer_segments():
    global failed_request, requested_segments, buffer_size, lock_buffer_size
    global observations, session, rebuffering_goal, rebuffering_goal_is_reached
    global buffering_goal

    start_buffering = time.time()
    first_buffering = True
    time_first_buffering = 0

    # Until all the segments have been requested
    while (requested_segments < n_segments):
        # If the buffer is already full, wait
        buffer_is_not_full.wait()
        # Request a segment
        #print("%6.6f [Buffer:%d] %d %d" %
        #    (datetime.datetime.now().timestamp(), os.getpid(), buffer_size.value, buffering_goal))
        #print("%6.6f [Buffer:%d] Requesting segment #%d" %
        #    (datetime.datetime.now().timestamp(), os.getpid(), requested_segments))
        response = process_request(observations, requested_segments,
            url["base"]+segment_list[requested_segments], session)
        # Check if the request has failed
        if failed_request == True:
            print(("%6.6f [Buffer:%d] Buffering failed!" %
                (datetime.datetime.now().timestamp(), os.getpid())))
            break
        else:
            print(("%6.6f [Buffer:%d] Got %s%s" %
                (datetime.datetime.now().timestamp(), os.getpid(),
                url["base"], segment_list[requested_segments])))

            requested_segments += 1

            with lock_buffer_size:
                buffer_size.value += 1
                #print "Buffer %d buffer_size" % buffer_size.value

            # If the rebuffering goal is met, set event
            if buffer_size.value >= rebuffering_goal:
                if first_buffering == True:
                    time_first_buffering = time.time() - start_buffering
                    first_buffering = False
                rebuffering_goal_is_reached.set()

            # If the buffering goal is met, clear event (the buffer is full)
            if buffer_size.value >= buffering_goal:
                buffer_is_not_full.clear()

#            # If it is the first time the buffering goal is reached, take timestamp
#            if first_buffering == True and requested_segments == buffering_goal + 1:
#                first_buffering = False
#                time_first_buffering = time.time() - start_buffering
#                print("Time for first buffering (s): %6.10f" % time_first_buffering)

    return time_first_buffering

# Read list of strings
def load_file(filepath):
    _this_dir = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(_this_dir, filepath)) as fhandle:
        datalist = fhandle.read().splitlines()
    return datalist

# Select n random video from the original videolist (sorted in the same order of videolist)
def select_n_random_videos(videolist, nvideos):
    if nvideos >= len(videolist):
        return videolist

    rnd_videolist = [ videolist[i] for i in
            sorted(numpy.random.choice(range(len(videolist)), nvideos)) ]
    return rnd_videolist

# select first n videos from the original videolist (sorted in the same order of videolist)
def select_n_video_id(videolist, seed, nvideos):
    if (seed >= len(videolist)):
        return None

    if (seed+nvideos < len(videolist)):
        sel_videolist = [ videolist[i] for i in
                range(seed, (nvideos+seed)) ]
    else:
        len1 = len(videolist) - seed
        len2 = nvideos - len1
        sel_videolist = [ videolist[i] for i in range(seed, len(videolist)) ]
        for i in range(len2):
            sel_videolist.append(i)
    return sel_videolist

# Generate MPD URL list from the video Ids list
# VideoMap: videoIds -> {Base URL, MPD URL}
def get_mpd_url_list(video_list, protocol="https", server_ip="139.19.171.103",
        base_dir="benchVideo.php?c=1&d=0&f=./videos", n_clusters='20'):
    videomap = collections.OrderedDict()
    for video in video_list:
        urls = {}
        urls["base"] = protocol+"://"+server_ip+"/"+base_dir+"/yt-"+video+"-frag/"
        urls["mpd"] = protocol+"://"+server_ip+"/"+base_dir+"/yt-"+video+"-frag/mpd-"+video+".mpd"
        if n_clusters != 0:
            urls["mpd"] = protocol+"://"+server_ip+"/"+base_dir+"/yt-"+video+"-frag/n"+str(n_clusters)+"-mpd-"+video+".mpd"
        videomap[video] = urls
    return videomap

# Sleep in seconds (with a ms granularity)
def wait_rnd_start_time(mint=1, maxt=5000):
    s = float(numpy.random.uniform(mint, maxt)/1000)
    time.sleep(s)

# Request MPD, parse MPD, request segments
def play_video_with_aggressive_buffering(videourls, allObs=None):
    obs = []
    mpd = get_mpd_file(obs, videourls["mpd"])
    segment_list, n_segments = get_segment_list(mpd)
    obsv = get_segments_with_aggressive_buffering(obs, segment_list, videourls["base"])
    return obsv

# Print statistics
def print_observations(observations, missed_segments, exptime_ms):
    totalxfer = 0
    totalrtt = 0
    totalretry = 0
    totalreq = len(observations)

    for o in observations:
        totalxfer += o.size
        totalrtt += o.rtt
        totalretry += o.retry_cnt

    avg_size = totalxfer/totalreq
    avg_rtt = totalrtt/totalreq
    totaltime_s = exptime_ms/1000.0
    xput = totalreq/totaltime_s
    xput_kbps = (totalxfer/1024)/totaltime_s

    print("")
    print("%#-25s %#-6.3f seconds" % ("Time taken for tests:", totaltime_s))
    print("%#-25s %d" % ("Completed requests:", totalreq))
    print("%#-25s %d" % ("Skipped segments:", len(missed_segments)))
    print("%#-25s %d" % ("Total retries:", totalretry))
    print("\nList of skipped segments:")
    print(missed_segments)
    print("")

    print("%#-25s %d" % ("Total bytes transferred:", totalxfer))
    print("%#-25s %#-6.3f" % ("Requests per second:", xput))
    print("%#-25s %#-6.3f [ms] (mean)" \
        % ("Time per request:", avg_rtt))
    print("%#-25s %#-6.3f [Kbytes/sec] received"   \
        % ("Transfer rate:", xput_kbps))
    print("")

    rtime_arr = [o.rtt for o in observations]
    avg_rtt = numpy.mean(rtime_arr)
    std_rtt = numpy.std(rtime_arr)
    p50_rtt = numpy.percentile(rtime_arr, 50)
    p90_rtt = numpy.percentile(rtime_arr, 90)
    p95_rtt = numpy.percentile(rtime_arr, 95)
    p99_rtt = numpy.percentile(rtime_arr, 99)
    max_rtt = numpy.amax(rtime_arr)
    min_rtt = numpy.amin(rtime_arr)
    print("Latency statistics (ms)")
    print("-----------------------")
    print("RTT avg:\t%#-6.3f" % avg_rtt)
    print("RTT 50p:\t%#-6.3f" % p50_rtt)
    print("RTT 90p:\t%#-6.3f" % p90_rtt)
    print("RTT 95p:\t%#-6.3f" % p95_rtt)
    print("RTT 99p:\t%#-6.3f" % p99_rtt)
    print("RTT max:\t%#-6.3f, req#: %d" % (max_rtt, rtime_arr.index(max_rtt)))
    print("RTT min:\t%#-6.3f, req#: %d" % (min_rtt, rtime_arr.index(min_rtt)))

    rlen_arr = [o.size for o in observations]
    avg_rlen = numpy.mean(rlen_arr)
    std_rlen = numpy.std(rlen_arr)
    p50_rlen = numpy.percentile(rlen_arr, 50)
    p90_rlen = numpy.percentile(rlen_arr, 90)
    p95_rlen = numpy.percentile(rlen_arr, 95)
    p99_rlen = numpy.percentile(rlen_arr, 99)
    max_rlen = numpy.amax(rlen_arr)
    min_rlen = numpy.amin(rlen_arr)

    print("")
    print("Size statistics (bytes)")
    print("-----------------------")
    print("RSIZE avg:\t%#-6.3f" % avg_rlen)
    print("RSIZE 50p:\t%#-6.3f" % p50_rlen)
    print("RSIZE 90p:\t%#-6.3f" % p90_rlen)
    print("RSIZE 95p:\t%#-6.3f" % p95_rlen)
    print("RSIZE 99p:\t%#-6.3f" % p99_rlen)
    print("RSIZE max:\t%#-6.3f, req#: %d" % (max_rlen, rlen_arr.index(max_rlen)))
    print("RSIZE min:\t%#-6.3f, req#: %d" % (min_rlen, rlen_arr.index(min_rlen)))
    print("")

# Store statistics in file
def store_observations(observations, missed_segments, exptime_ms, filehandle):
    totalxfer = 0
    totalrtt = 0
    totalreq = len(observations)

    for o in observations:
        totalxfer += o.size
        totalrtt += o.rtt

    avg_size = totalxfer/totalreq
    avg_rtt = totalrtt/totalreq
    totaltime_s = exptime_ms/1000.0
    xput = totalreq/totaltime_s
    xput_kbps = (totalxfer/1024)/totaltime_s

    f.write("\n")
    f.write("\n%#-25s %#-6.3f seconds" % ("Time taken for tests:", totaltime_s))
    f.write("\n%#-25s %d" % ("Completed requests:", totalreq))
    f.write("\n%#-25s %d" % ("Skipped segments:", len(missed_segments)))
    f.write("\n\nList of skipped segments:")
    for key, val in list(missed_segments.items()):
        f.write("\nSegment " + str(key) + ": [Time] " + str(val[0]) + ", [Delay] " + str(val[1]))
    f.write("\n")

    f.write("\n%#-25s %d" % ("Total bytes transferred:", totalxfer))
    f.write("\n%#-25s %#-6.3f" % ("Requests per second:", xput))
    f.write("\n%#-25s %#-6.3f [ms] (mean)" \
        % ("\nTime per request:", avg_rtt))
    f.write("\n%#-25s %#-6.3f [Kbytes/sec] received"   \
        % ("\nTransfer rate:", xput_kbps))
    f.write("\n")

    rtime_arr = [o.rtt for o in observations]
    avg_rtt = numpy.mean(rtime_arr)
    std_rtt = numpy.std(rtime_arr)
    p50_rtt = numpy.percentile(rtime_arr, 50)
    p90_rtt = numpy.percentile(rtime_arr, 90)
    p95_rtt = numpy.percentile(rtime_arr, 95)
    p99_rtt = numpy.percentile(rtime_arr, 99)
    max_rtt = numpy.amax(rtime_arr)
    min_rtt = numpy.amin(rtime_arr)
    f.write("\nLatency statistics (ms)")
    f.write("\n-----------------------")
    f.write("\nRTT avg:\t%#-6.3f" % avg_rtt)
    f.write("\nRTT 50p:\t%#-6.3f" % p50_rtt)
    f.write("\nRTT 90p:\t%#-6.3f" % p90_rtt)
    f.write("\nRTT 95p:\t%#-6.3f" % p95_rtt)
    f.write("\nRTT 99p:\t%#-6.3f" % p99_rtt)
    f.write("\nRTT max:\t%#-6.3f, req#: %d" % (max_rtt, rtime_arr.index(max_rtt)))
    f.write("\nRTT min:\t%#-6.3f, req#: %d" % (min_rtt, rtime_arr.index(min_rtt)))

    rlen_arr = [o.size for o in observations]
    avg_rlen = numpy.mean(rlen_arr)
    std_rlen = numpy.std(rlen_arr)
    p50_rlen = numpy.percentile(rlen_arr, 50)
    p90_rlen = numpy.percentile(rlen_arr, 90)
    p95_rlen = numpy.percentile(rlen_arr, 95)
    p99_rlen = numpy.percentile(rlen_arr, 99)
    max_rlen = numpy.amax(rlen_arr)
    min_rlen = numpy.amin(rlen_arr)

    f.write("\n")
    f.write("\nSize statistics (bytes)")
    f.write("\n-----------------------")
    f.write("\nRSIZE avg:\t%#-6.3f" % avg_rlen)
    f.write("\nRSIZE 50p:\t%#-6.3f" % p50_rlen)
    f.write("\nRSIZE 90p:\t%#-6.3f" % p90_rlen)
    f.write("\nRSIZE 95p:\t%#-6.3f" % p95_rlen)
    f.write("\nRSIZE 99p:\t%#-6.3f" % p99_rlen)
    f.write("\nRSIZE max:\t%#-6.3f, req#: %d" % (max_rlen, rlen_arr.index(max_rlen)))
    f.write("\nRSIZE min:\t%#-6.3f, req#: %d" % (min_rlen, rlen_arr.index(min_rlen)))
    f.write("\n")

# Update latencies' information
def update_latencies(latencies, observations):
    rtt_times = [o.rtt for o in observations]
    latencies.append(rtt_times)
    return latencies

# Print overall latencies
def print_latencies(latencies):

    # Remove stats of first video (exclude warm-up effects)
    latencies.pop(0)

    # Flatten array of latencies
    rtime_arr = [item for sublist in latencies for item in sublist]

    # Get and print stats
    avg_rtt = numpy.mean(rtime_arr)
    std_rtt = numpy.std(rtime_arr)
    p50_rtt = numpy.percentile(rtime_arr, 50)
    p90_rtt = numpy.percentile(rtime_arr, 90)
    p95_rtt = numpy.percentile(rtime_arr, 95)
    p99_rtt = numpy.percentile(rtime_arr, 99)
    max_rtt = numpy.amax(rtime_arr)
    min_rtt = numpy.amin(rtime_arr)

    print("")
    print("Overall statistics")
    print("-----------------------")
    print("")

    print("Aggregate latency statistics (ms) - after cache warm-up")
    print("-----------------------")
    print("RTT avg:\t%#-6.3f" % avg_rtt)
    print("RTT 50p:\t%#-6.3f" % p50_rtt)
    print("RTT 90p:\t%#-6.3f" % p90_rtt)
    print("RTT 95p:\t%#-6.3f" % p95_rtt)
    print("RTT 99p:\t%#-6.3f" % p99_rtt)
    print("RTT max:\t%#-6.3f" % (max_rtt))
    print("RTT min:\t%#-6.3f" % (min_rtt))
    print("")

# Simulate video playing
class Player(multiprocessing.Process):

    def __init__(self):
        multiprocessing.Process.__init__(self)

    def run(self):
        # Ignore init segment (#0)
        played_segments = 1
        starttime_skip = 0
        endtime_skip = 0

        # Wait at start-up
        # time.sleep(7)
        print("%6.6f [Player] Waiting buffering at start-up." % datetime.datetime.now().timestamp())
        rebuffering_goal_is_reached.wait()
        print("%6.6f [Player] Waking up after start-up." % datetime.datetime.now().timestamp())

        # Until the buffering is not done
        # (no need to keep playing otherwise)
        while buffer_done.value == 0:
            # If there are segments to play
            if buffer_size.value > 0:
                # If the buffer was full, now it is not
                with lock_buffer_size:
                    buffer_size.value -= 1
                buffer_is_not_full.set()
                # Play one segment
                #print("%6.6f [Player] Playing segment #%d" % (datetime.datetime.now().timestamp(), played_segments))
                time.sleep(play_sleep)
                played_segments += 1
            # If the buffer is empty
            else:
                print("%6.6f [Player] Not enough segments in the buffer (%d). Pause." % (
                    datetime.datetime.now().timestamp(), buffer_size.value))
                # Store timestamp when skipping
                starttime_skip = time.time()
                # Clear the rebuffering goal condition
                rebuffering_goal_is_reached.clear()
                # Wait until the rebuffering goal is met
                rebuffering_goal_is_reached.wait()
                # Store skipping information
                endtime_skip = time.time()
                missed_segments[played_segments] = [starttime_skip, endtime_skip - starttime_skip]

        # When the buffering is done, close the player
        print(("%6.6f [Player] Done. Played %d of %d, buffer_done %d" %
            (datetime.datetime.now().timestamp(), played_segments, n_segments, buffer_done.value)))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Video streaming client")
    parser.add_argument("-I", "--serverip", dest="serverip", action="store",
        help="IP address of the VM hosting video server")
    parser.add_argument("-p", "--proto", dest="proto", action="store",
        default="https", help="request protocol (http/https)")
    parser.add_argument("-u", "--baseurl", dest="baseurl", action="store",
        help="base URL to request. e.g. benchVideo.php?c=0&d=0&f=./video")
    parser.add_argument("-f", "--videolist", dest="videolist", action="store",
        help="list of videos to request")
    parser.add_argument("-b", "--bandwidth", dest="bw", required=False,
        default="10G", help="client bandwidth configuration")
    parser.add_argument("-s", "--seed", dest="seed", required=False,
        default=0, type=int, help="random seed")
    parser.add_argument("-c", "--clustering", dest="n_clusters", required=False,
        default=0, type=int, help="number of clusters (0, 20, 50)")
    parser.add_argument("-ps", "--play_sleep", dest="play_sleep", required=False,
        default=5, type=float, help="seconds to sleep to simulate playing")
    parser.add_argument("-q", "--quality", dest="hqvideos", required=False,
        default=0, help="1 for 1080p, 2 for 720p, 20M for 20M, 0 for 240p (default 0)")
    parser.add_argument("-w", "--warmup", dest="warmup",
        default=0, type=int, help="1 if running in warm-up mode, 0 otherwise")
    parser.add_argument("-nv", "--nvideos", dest="n_videos_to_play",
        default=1, type=int, help="number of videos to play from videolist (default 1)")
    parser.add_argument("-dur", "--duration", dest="max_seconds",
        default=0, type=int, help=("max duration (s) for each video stream "
                    "(default 0, implies each video will be streamed till end)."))
    parser.add_argument("-bgs", "--buffering_goal", dest="buffering_goal_s",
        default=60, type=float, help=("buffering goal (in s) before playback starts"))
    parser.add_argument("-rbgs", "--rebuffering_goal", dest="rebuffering_goal_s",
        default=15, type=float, help=("buffering goal (in s) before playback starts"))

    args = parser.parse_args()

    serverip = args.serverip
    protocol = args.proto
    baseurl = args.baseurl
    videolist = args.videolist
    bw = args.bw
    seed = int(args.seed)
    n_clusters = int(args.n_clusters)
    play_sleep = float(args.play_sleep)

    # Number of videos to play
    n_videos_to_play = int(args.n_videos_to_play)
    hqvideos = args.hqvideos
    warmup = int(args.warmup)

    # Limit the duration of a video to max_minutes
    max_seconds = int(args.max_seconds)

    segment_duration = play_sleep

    max_n_segments = max_seconds/segment_duration

    # Number of buffered segments not yet played
    #buffer_size = 0

    # Amount of content we try to buffer (in seconds)
    buffering_goal_s = args.buffering_goal_s
#    buffering_goal_s = 30
#    buffering_goal_s = play_sleep

    # Amount of content we try to buffer (in number of segments)
    buffering_goal = numpy.floor(buffering_goal_s/segment_duration)

    # Amount of content we have to have buffered before we can play (in seconds)
    rebuffering_goal_s = args.rebuffering_goal_s
#    rebuffering_goal_s = 15
#    rebuffering_goal_s = play_sleep

    # Number of segments requested (by the Buffer thread)
    #requested_segments = 0
    # Number of segments played (by the Player thread)
    #played_segments = 0
    # IDs of the skipped segments and skipping information
    # ID -> [skipping time, skipping delay]
    #missed_segments = {}
    # list of observations per video
    observations_list = []
    time_first_buffering_list = []
    # Client session
    session = None
    # Threads
    player = None

    # Get list of videos on the server
    allvideolist = load_file(videolist)

    # Seed
    numpy.random.seed(seed=seed)
    # Select a subset of the original dataset (debug: one)
    videolist = select_n_random_videos(allvideolist, n_videos_to_play)
#    videolist = select_n_video_id(allvideolist, seed, n_videos_to_play)
    # For each video, store array of segment latencies
    latencies = []

    # Generate the URLs of the videos in the subset
    videomap = get_mpd_url_list(videolist, protocol=protocol, server_ip=serverip,
            base_dir=baseurl, n_clusters=n_clusters)
    # print(videomap['08IZonH0tAs']['mpd'])
    print("%f [Client:%d] Request %d videos seed %d vidlist %s" %
        (datetime.datetime.now().timestamp(), os.getpid(), n_videos_to_play,
        seed, str(list(videomap.keys()))))

#    print((list(videomap.keys())))

    # For each video to be played
    # for i in range(len(videomap.keys())):
    for i in range(len(videolist)):

        failed_request = False

        buffer_done = multiprocessing.Value('b', 0)
        buffer_size = multiprocessing.Value('d', 0)
        lock_buffer_size = multiprocessing.Lock()
        rebuffering_goal_is_reached = multiprocessing.Event()
        buffer_is_not_full = multiprocessing.Event()
        buffer_is_not_full.set()
        manager = multiprocessing.Manager()
        missed_segments = manager.dict()

        requested_segments = 0

        session = requests.session()

        # Get video information
        video_tag = videolist[i]
        url = videomap[video_tag]
        clusterid = get_cluster_id(video_tag, n_clusters, hqvideos)
        print("%6.6f [Buffer:%d] Processing video[%d]: %s clusterid: %d hq: %s url: %s" %
            (datetime.datetime.now().timestamp(), os.getpid(), i, video_tag, clusterid,
                hqvideos, url))

        # Wait random start time
        # comment only for attack experiment with video streams
        wait_rnd_start_time()

        # Initialize observation list
        observations = []

        # Mark beginning of the whole run
        starttime = gettime()

        # Request MPD file
        print("%6.6f [Buffer:%d] Requesting MPD file... %s" %
            (datetime.datetime.now().timestamp(), os.getpid(), url["mpd"]))
        mpd = get_mpd_file(observations, url["mpd"], session)
        if failed_request == True:
            print("Error - Failed to get MPD file!")
            sys.exit(1)
        print("%6.6f [Buffer:%d] Got MPD file: %s" %
            (datetime.datetime.now().timestamp(), os.getpid(), url["mpd"]))

        # Parse URL segment list and get total number of segments and minimum buffer time
        segment_list, n_segments, min_buffer_time = get_segment_list(mpd, hqvideos)

        # Check MPD error
        if min_buffer_time is None:
            print("MPD error!")
            with open("/local/sme/workspace/side-channels/profiling/mpderrors.txt",'a+') as f:
                f.write("\n")
                f.write(url["mpd"])
        try:
            # Select rebuffering_goal according to MPD information
            rebuffering_goal = numpy.floor(rebuffering_goal_s/segment_duration)

#            if int(min_buffer_time) > rebuffering_goal_s:
#                rebuffering_goal = numpy.floor(min_buffer_time/segment_duration)
#            else:
#                rebuffering_goal = numpy.floor(rebuffering_goal_s/segment_duration)

            # Potentially adjust buffering_goal
            if rebuffering_goal >= buffering_goal:
                buffering_goal = rebuffering_goal * 2

            # Limit the duration of a video
            vid_segments = n_segments
            if max_seconds > 0:
                if n_segments > max_n_segments:
                    n_segments = max_n_segments

            print("%6.6f [Buffer:%d] Vid segments %d #segments %d min_buffer_time %.4f "    \
                "buf goal s %.4f n %d adjusted %d rebuf goal s %.4f n %d adjusted %d" %
                (datetime.datetime.now().timestamp(), os.getpid(),
                vid_segments, n_segments, min_buffer_time,
                buffering_goal_s, numpy.floor(buffering_goal_s/segment_duration), buffering_goal,
                rebuffering_goal_s, numpy.floor(rebuffering_goal_s/segment_duration),
                rebuffering_goal))

            """
            We no longer need this code fragment, since we can
            configure the debug configuration of n_segments = 1
            by setting max_seconds = 5 via commandline.
            # Debug
            #n_segments = 1
            if n_segments == 1:
                rebuffering_goal = 1
                buffering_goal = 1
            """

            # Get initialization segment
#            print(("%6.6f [Buffer:%d] Requesting segment #%d" %
#                (datetime.datetime.now().timestamp(), os.getpid(), requested_segments)))
            response = process_request(observations, requested_segments,
                    url["base"]+segment_list[requested_segments], session)
            # If response is None or failed_request == True:
            if failed_request == True:
                print("Error - Failed to get initialization segment!")
                sys.exit(1)

            print("%6.6f [Buffer:%d] Got %s%s" %
                    (datetime.datetime.now().timestamp(), os.getpid(),
                    url["base"], segment_list[requested_segments]))
            requested_segments += 1

            # If warm up phase
            if warmup == 1:
                print("Starting warm-up phase...")
                time_first_buffering = warm_up()

            # If video playing
            else:
                # Initialize Player
                player = Player()

                # Start Player process
                print("%6.6f [Player] Starting ..." % (datetime.datetime.now().timestamp()))
                player.start()

                # Start buffering
                time_first_buffering = buffer_segments()
                buffer_done.value = 1
                print("%6.6f [Buffer:%d] Done (%d)." %
                    (datetime.datetime.now().timestamp(), os.getpid(), buffer_done.value))

                # Wake up player in case it is waiting
                rebuffering_goal_is_reached.set()
                # Join process when buffering is done
                player.join()
                print("Player joined.")

            # Sleep to ensure all profile has been played out
            # comment only for attack experiment with video streams
            time.sleep(20)

        except Exception as e:
            print("Error while requesting segments!")
            print(e)

            # Sleep to ensure all profile has been played out
            # comment only for attack experiment with video streams
            time.sleep(20)

            # Close session
            session.close()
            session = None

            # Close player
            if player != None:
                # Wake up player in case it is waiting
                rebuffering_goal_is_reached.set()
                # Join player process
                buffer_done.value = 1
                player.join()
            #pass

        finally:
            # Mark end of the video playing
            endtime = gettime()
            exptime_ms = (endtime-starttime)

        # Print per-segment statistics
        for o in observations:
            o.print_observation()

        # Reset global buffer_done variable
        buffer_done.value = 0

        # Print video statistics
        print_observations(observations, missed_segments, exptime_ms)
        print("\nTime for first buffering and play start (s): %6.9f" % time_first_buffering)
        print("")

        # If the buffering has failed, print a timeout
        if failed_request == True:
            print("\n################\n")
            print("Error: timeout!")
            print("\n################\n")
            session.close()
            session = None
            sys.exit(1)

        """
        # Save statistics to file
        outdir = "/local/sme/workspace/side-channels/profiling/stats/"
        filename = outdir + "obs-bw-" + str(bw) + "-" + video_tag + "-" + str(time.time()) + ".out"
        for o in observations:
            o.store_observation(filename)
        filename = outdir + "stats-bw-" + str(bw) + "-" + video_tag + "-" + str(time.time()) + ".out"
        with open(filename, "w") as f:
            store_observations(observations, missed_segments, exptime_ms, f)
        """
        # Add the stats and details to the observations' list
        observations_list.append([observations, missed_segments, exptime_ms])
        time_first_buffering_list.append(time_first_buffering)

        # Update latencies' information
        update_latencies(latencies, observations)

        # Close session
        # session.close()
        # session = None

    # If there is more than one video, get overall latency statistics
    if len(videolist) > 1:
        print_latencies(latencies)
        time_first_buffering_list.pop(0)
        print("\nAverage time for first buffering (s): %6.10f" % numpy.mean(time_first_buffering_list))
        print("")
