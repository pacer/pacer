## Applications

Frontend VM:
- openssl: 1.1.0g
- apache: httpd-2.4.33
- mediawiki: 1.27.1 (install vanilla mysql server on frontend VM)
- videoserver (can connect with memcached in backend VM)

Backend VM:
- memcached: 1.6.9

Client:
- wrk2 (used with mediawiki)
- videoclient (used with videoserver)


## Installation instructions

### OpenSSL
```
cd openssl-1.1.0g
./config --prefix=/opt/openssl --openssldir=/usr/local/ssl
make && make install
```

Load libraries using one of the following methods:
- At each reboot: `export LD_LIBRARY_PATH=/opt/openssl/lib` && `ldconfig`
- One time: Add a file `/etc/ld.so.conf.d/openssl.conf` with the following line in the file: `/opt/openssl/lib`

### Apache
```
cd httpd-2.4.33/
./configure --enable-mpms-shared=all --enable-vhost-alias --enable-rewrite --enable-so --enable-ssl --with-included-apr --with-included-apr-util --with-ssl=/opt/openssl
make && make install
```

Generate baseline `mod_ssl.so`:
- In `include/ap_config.h`: set `SME_ENF` to 1.
- `make && make install`
- cp /usr/local/apache2/modules/mod_ssl.so /usr/local/apache2/modules/mod_ssl_base.so

Generate Pacer-enabled `mod_ssl.so`:
- In `include/ap_config.h`: set `SME_ENF` to 1.
- `make && make install`
- cp /usr/local/apache2/modules/mod_ssl.so /usr/local/apache2/modules/mod_ssl_enf.so

Default web server path: `/var/www/html`

### Mediawiki
- Dependencies: php7.0-mbstring php7.0-xml php7.0-curl php7.0-mcrypt php7.0-curl mysql-server liblua5.1-dev php-dev
- Some reference instructions: https://www.mediawiki.org/wiki/Manual:Running_MediaWiki_on_Debian_or_Ubuntu
- CLI installation:
```
cd mediawiki-1.27.1
php maintenance/install.php --confpath /home/pacer/workspace/mediawiki-1.27.1 --dbuser root --dbpass <password> --dbserver localhost --dbtype mysql --dbname <dbname> --pass <password> --scriptpath /mediawiki --server https://localhost --globals <dbname> root
php maintenance/update.php
```

### Video server
- Compile the php ioctl code generator from [php-ioctl](https://gitlab.mpi-sws.org/pacer/pacer/-/tree/main/apps/videoserver/php-ioctl)
- Configure path of generated in `libphpioctl.so` in appropriate `php.ini` file.
- Copy `benchVideo.php` and `phpioct.php` to `/var/www/html/`

### Memcache
- Dependencies: php-memcache php-memcached
- Get repo from https://gitlab.mpi-sws.org/pacer/memcached-1.6.9
- Installation steps:
```
cd memcached-1.6.9
./configure
make
make test
make install
```

### wrk2
- Get repo from https://gitlab.mpi-sws.org/pacer/wrk2
- Just compile using `make`

